#Use Intellij Plugin GLSL Support for highlighting

#type vertex
#version 330 core

layout (location=0) in vec3 aPos;
layout (location=1) in vec4 aColour;
layout (location=2) in vec2 aTextureCoordinates;

uniform mat4 uProjection;
uniform mat4 uView;

out vec4 fragmentColour;
out vec2 fTextureCoordinates;

void main() {
    fragmentColour = aColour;
    fTextureCoordinates = aTextureCoordinates;
    gl_Position = uProjection * uView * vec4(aPos, 1.0);
}

#type fragment
#version 330 core

uniform sampler2D TEXTURE_SAMPLER;

in vec4 fragmentColour;
in vec2 fTextureCoordinates;

out vec4 colour;

void main() {
    colour = texture(TEXTURE_SAMPLER, fTextureCoordinates);
}
