package de.arnonym.strategiespiel.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import de.arnonym.strategiespiel.Strategiespiel;
import de.arnonym.strategiespiel.spielSchirm.balancing.Konstanten;
import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenBalance;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();

		//SELBST HINZUGEFUEGT
		config.title = "Strategiespiel pre Alpha";
		config.width = 1280;
		config.height = 720;
		config.vSyncEnabled = false; // Setting to false disables vertical sync
		if (Konstanten.DEVELOPER_VISUELL_NUR_10_FPS) {
			config.foregroundFPS = 10;
			config.backgroundFPS = 10;
		} else {
			config.foregroundFPS = 0; // Setting to 0 disables foreground fps throttling
			config.backgroundFPS = 0; // Setting to 0 disables background fps throttling
		}
		// SELBST HINZUGEFUEGT

		new LwjglApplication(new Strategiespiel(), config);

	}
}
