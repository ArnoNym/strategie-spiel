<?xml version="1.0" encoding="UTF-8"?>
<tileset name="tileset_Spawnen" tilewidth="64" tileheight="32" tilecount="18" columns="3">
 <image source="tileset_Spawnen.png" width="192" height="192"/>
 <tile id="0">
  <properties>
   <property name="baum" value=""/>
  </properties>
 </tile>
 <tile id="1">
  <properties>
   <property name="beeren" value=""/>
  </properties>
 </tile>
</tileset>
