package de.arnonym.strategiespiel.spielSchirm.ecs.framework.verwaltung.manager;

import com.badlogic.gdx.utils.IntMap;

import java.util.ArrayList;
import java.util.List;

import de.arnonym.strategiespiel.spielSchirm.balancing.Konstanten;
import de.arnonym.strategiespiel.spielSchirm.ecs.framework.verwaltung.connect.Connectable;
import de.arnonym.strategiespiel.spielSchirm.ecs.framework.verwaltung.connect.Connector;
import de.arnonym.strategiespiel.spielSchirm.ecs.framework.verwaltung.visit.Visitable;
import de.arnonym.strategiespiel.spielSchirm.ecs.framework.verwaltung.visit.Visitor;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.Component;
import de.arnonym.strategiespiel.framework.werkzeuge.collections.hashMaps.mehrereValuesOKeys.CoupleValueMap;
import de.arnonym.strategiespiel.framework.werkzeuge.collections.hashMaps.mehrereValuesOKeys.CoupleMap;

public class Manager {
    private final IntMap<Visits> visitsIntMap = new IntMap<>();
    private final IntMap<Connections> connectionsIntMap = new IntMap<>();

    /** Aus Effizienzgruenden. Ansonsten muss beim loeschen eines Visitables (also auch einer
     *  Componente) jeder einzelne Eintrag durchlaufen werden */
    private final IntMap<List<Visitor>> visitableIntMap = new IntMap<>();
    private final IntMap<List<Connector>> connectableIntMap = new IntMap<>();

    private final List<Visitor> saveDeleteList = new ArrayList<>();

    // IDS ////////////////////////////////////////////////////////////////////////////////////////

    private List<Integer> freeIds = new ArrayList<>();
    private int id = -1;

    public int createId() {
        if (freeIds.isEmpty()) {
            id += 1;
            return id;
        }
        return freeIds.remove(freeIds.size() - 1);
    }

    public void freeId(int id) {
        freeIds.add(id);
    }

    // VISITS /////////////////////////////////////////////////////////////////////////////////////

    public void visit(Visitor visitor, Visitable visitable) {
        if (Konstanten.DEVELOPER_EXCEPTIONS
                && visitable instanceof Visitor
                && visitor instanceof Visitable) {
            List<Visitor> visitorList = visitableIntMap.get(visitor.getId());
            if (visitorList != null && visitorList.contains(visitable)) {
                throw new IllegalStateException("Es duerfen nicht beide gegenseitig voneinander "
                        + "abhaengig sein. Fuehrt beim loeschen zu Zirkelbezug! "
                        + visitor.getClass().getSimpleName() + " und "
                        + visitable.getClass().getSimpleName());
            }
        }

        int entitiyId = visitor.getEntityId();
        Visits visits = visitsIntMap.get(entitiyId);
        if (visits == null) {
            visits = new Visits();
            visitsIntMap.put(entitiyId, visits);
        }
        visits.put(visitor, visitable);

        int id = visitable.getId();
        List<Visitor> visitorList = visitableIntMap.get(id);
        if (visitorList == null) {
            visitorList = new ArrayList<>();
            visitableIntMap.put(id, visitorList);
        }
        visitorList.add(visitor);
        if (Konstanten.DEVELOPER_VISUELL_MANAGER_OUT) {
            java.lang.System.out.println("Manager ::: new visit, visitableIntMap.size == "
                    + visitableIntMap.size + ", "
                    + connectableIntMap.size + ", "
                    + visitsIntMap.size + ", "
                    + connectionsIntMap.size + ", "
                    + visits.size() + ", "
                    + visitorList.size() + ", id == " + id);
        }
    }

    public void unvisit(Visitor visitor, Visitable visitable) {
        int entityId = visitor.getEntityId();
        Visits visits = visitsIntMap.get(entityId);
        visits.remove(visitor, visitable);
        if (visits.isEmpty()) {
            visitsIntMap.remove(entityId);
        }

        int id = visitable.getId();
        List<Visitor> visitorList = visitableIntMap.get(id);
        visitorList.remove(visitor);
        if (visitorList.isEmpty()) {
            visitableIntMap.remove(id);
        }
    }

    public void unvisitVisitor(Visitor visitor) {
        //java.lang.System.out.println("Manager ::: unvisitVisitor");
        int entityId = visitor.getEntityId();
        Visits visits = visitsIntMap.get(entityId);
        if (visits == null) return; // Siehe Begruendung bei disconnectConnector()
        List<Visitable> visitableList = visits.removeK1(visitor);
        //java.lang.System.out.println("Manager ::: visitableList.size == "+visitableList.size());
        if (visits.isEmpty()) {
            visitsIntMap.remove(entityId);
        }

        for (Visitable visitable : visitableList) {
            int id = visitable.getId();
            List<Visitor> visitorList = visitableIntMap.get(id);
            visitorList.remove(visitor);
            if (visitorList.isEmpty()) {
                visitableIntMap.remove(id);
            }
        }
    }

    public void unvisitVisitable(Visitable visitable) {
        List<Visitor> visitorList = visitableIntMap.get(visitable.getId());
        if (visitorList == null) {
            return;
        }
        for (Visitor visitor : visitorList) {
            addSaveDelete(visitor);
        }
        saveDelete();
    }

    // SAVE DELETE //

    private void addSaveDelete(Visitor visitor) {
        if (saveDeleteList.contains(visitor)) return;
        saveDeleteList.add(visitor);
    }

    private void saveDelete() {
        while (!saveDeleteList.isEmpty()) {
            Visitor visitor = saveDeleteList.remove(0);
            if (visitor == null) return;
            visitor.delete();
        }
    }

    // CONNECTIONS ////////////////////////////////////////////////////////////////////////////////

    public void connect(Connector connector, Connectable connectable, boolean activ) {
        int entityId = connector.getEntityId();
        Connections connections = connectionsIntMap.get(entityId);
        if (connections == null) {
            connections = new Connections();
            connectionsIntMap.put(entityId, connections);
        } else if (connections.contains(connector, connectable)) {
            return;
        }
        connections.put(connector, connectable, new MyBoolean(activ));

        int connectableId = connectable.getId();
        List<Connector> connectorList = connectableIntMap.get(connectableId);
        if (connectorList == null) {
            connectorList = new ArrayList<>();
            connectableIntMap.put(connectableId, connectorList);
        }
        connectorList.add(connector);

        if (activ) {
            connectable.activate(connector);
        }
    }

    public void disconnect(Connector connector, Connectable connectable) {
        int entityId = connector.getEntityId();
        Connections connections = connectionsIntMap.get(entityId);
        boolean activ = connections.getValue(connector, connectable).get();
        connections.remove(connector, connectable);
        if (connections.isEmpty()) {
            connectionsIntMap.remove(entityId);
        }

        int id = connectable.getId();
        List<Connector> connectorList = connectableIntMap.get(id);
        connectorList.remove(connector);
        if (connectorList.isEmpty()) {
            connectableIntMap.remove(id);
        }

        if (activ) {
            connectable.deactivate(connector);
        }

    }

    public void disconnectConnector(Connector connector) {
        int entityId = connector.getEntityId();
        Connections connections = connectionsIntMap.get(entityId);
        if (connections == null) return; /* Beim loeschen Einer Entity sind manche geloschte
        Connectors die Connectables anderer Connectors. Das fuehrt dazu, dass 'Connections' leer
        ist und geloescht wird waehrend die Entity noch Connectors loeschen mochte. Fuer diesen
        Fall diese Abfrage */
        List<Connectable> connectableList = connections.getK2s(connector);
        for (Connectable connectable : connectableList) {
            boolean activ = connections.getValue(connector, connectable).get();

            connections.remove(connector, connectable);

            int id = connectable.getId();
            List<Connector> connectorList = connectableIntMap.get(id);
            connectorList.remove(connector);
            if (connectorList.isEmpty()) {
                connectableIntMap.remove(id);
            }

            if (activ) {
                connectable.deactivate(connector);
            }
        }
        if (connections.isEmpty()) {
            connectionsIntMap.remove(entityId);
        }
    }

    public void disconnectConnectable(Connectable connectable) {
        List<Connector> connectorList = connectableIntMap.get(connectable.getId());

        if (connectorList == null) {
            return;
        }

        List<Connector> disconnectList = new ArrayList<>();

        for (Connector connector : connectableIntMap.get(connectable.getId())) {
            disconnectList.add(connector);
        }

        for (Connector c : disconnectList) {
            disconnect(c, connectable);
        }
    }

    // ACTIVATE //

    public void setActiv(Connector connector, Connectable connectable, boolean activ) {
        Connections connections = connectionsIntMap.get(connector.getEntityId());
        setActiv(connections, connector, connectable, activ);
    }

    public void setActiv(Connector connector, boolean activ) {
        Connections connections = connectionsIntMap.get(connector.getEntityId());
        for (Connectable connectable : connections.getK2s(connector)) {
            setActiv(connections, connector, connectable, activ);
        }
    }

    public void setActiv(Connectable connectable, boolean activ) {
        for (Connector connector : connectableIntMap.get(connectable.getId())) {
            setActiv(connector, connectable, activ);
        }
    }

    private void setActiv(Connections connections, Connector connector, Connectable connectable,
                          boolean activ) {
        MyBoolean myBoolean = connections.getValue(connector, connectable);
        if (activ) {
            if (myBoolean.get()) {
                return;
            }
            myBoolean.set(true);
            connectable.activate(connector);
        } else {
            if (!myBoolean.get()) {
                return;
            }
            myBoolean.set(false);
            connectable.deactivate(connector);
        }
    }

    // BITTE WENIG NUTZEN /////////////////////////////////////////////////////////////////////////

    public List<Component> gettComponents(int entityId) {
        List<Component> componentList = new ArrayList<>();
        for (Connector connector : connectionsIntMap.get(entityId).key1List) {
            if (connector instanceof Component) {
                componentList.add((Component) connector);
            }
        }
        return componentList;
    }

    public List<Visitor> gettVisitors(int entityId) {
        return new ArrayList<>(visitsIntMap.get(entityId).getK1s());
    }

    public List<Connector> gettConnectors(int entityId) {
        return new ArrayList<>(connectionsIntMap.get(entityId).key1List);
    }

    public Component gettComponent(int entityId, Class<? extends Component> clazz) {
        for (Connector connector : connectionsIntMap.get(entityId).key1List) {
            if (clazz.isInstance(connector)) {
                return (Component) connector;
            }
        }
        return null;
    }

    public void setActiv(int entityId, boolean activ) {
        Connections connections = connectionsIntMap.get(entityId);
        for (Connector connector : connections.key1List) {
            if (connector instanceof Component) {
                setActiv(connector, activ);
            }
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////

    private class MyBoolean {
        private boolean value;

        public MyBoolean() {
            this(false);
        }

        public MyBoolean(boolean value) {
            this.value = value;
        }

        public boolean get() {
            return value;
        }

        public void set(boolean truth) {
            this.value = truth;
        }
    }

    public class Visits extends CoupleMap<Visitor, Visitable> {
        private Visits() { // Ist kuerzer. Fuer Uebersichtlichkeit
        }
    }

    public class Connections extends CoupleValueMap<Connector, Connectable, MyBoolean> {
        private Connections() { // Ist kuerzer. Fuer Uebersichtlichkeit
        }
    }
}
