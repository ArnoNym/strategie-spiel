package de.arnonym.strategiespiel.spielSchirm.objekte;

import com.badlogic.gdx.math.Vector2;

import de.arnonym.strategiespiel.spielSchirm.Dorf;
import de.arnonym.strategiespiel.spielSchirm.Level;
import de.arnonym.strategiespiel.spielSchirm.objekte.kollisionsobjekte.BodenStapel;
import de.arnonym.strategiespiel.spielSchirm.objekte.kollisionsobjekte.gebaude.Gebaude;
import de.arnonym.strategiespiel.spielSchirm.objekte.kollisionsobjekte.gebaude.gebBeruf.GebBerufFarm;
import de.arnonym.strategiespiel.spielSchirm.objekte.kollisionsobjekte.gebaude.gebBeruf.GebBerufHerstFabrik;
import de.arnonym.strategiespiel.spielSchirm.objekte.kollisionsobjekte.gebaude.gebBeruf.GebBerufHerstSchule;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterOnKlick.FensterOnKlickGebBerufSchule;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterOnKlick.FensterOnKlickGebBerufFabrik;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterOnKlick.FensterOnKlickGebBerufFarm;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.GegListe;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.MusterGeg;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.GegenstandListe;
import de.arnonym.strategiespiel.spielSchirm.objekte.dorfbewohnerAi.Dorfbewohner;
import de.arnonym.strategiespiel.spielSchirm.objekte.kollisionsobjekte.gebaude.Baugrube;
import de.arnonym.strategiespiel.spielSchirm.objekte.kollisionsobjekte.gebaude.gebBeruf.GebBeruf;
import de.arnonym.strategiespiel.spielSchirm.objekte.kollisionsobjekte.gebaude.GebaudeHaus;
import de.arnonym.strategiespiel.spielSchirm.objekte.kollisionsobjekte.gebaude.GebaudeLager;
import de.arnonym.strategiespiel.spielSchirm.objekte.kollisionsobjekte.ressourcen.Ressource;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterOnKlick.FensterOnKlick;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterOnKlick.FensterOnKlickBaugrube;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterOnKlick.FensterOnKlickDorfbewohner;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterOnKlick.FensterOnKlickHaus;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterOnKlick.FensterOnKlickMaterialLager;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterOnKlick.FensterOnKlickNahrungLager;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterOnKlick.FensterOnKlickRessource;
import de.arnonym.strategiespiel.spielSchirm.worldMap.RenderArrayList;

/**
 * Created by LeonPB on 01.09.2018.
 */

public abstract class WorldObjekt extends WorldActor implements LoeschenObjekt {
    private RenderArrayList renderArrayList; //la = loeschenAus : Die Liste wird nur gespeichert damit das objekt beim leoschen wierder aus ihr entfern werden kann

    private GegListe<MusterGeg> musterGegListe_nl; // Wird beim initialiseren durch GegListe ersetzt
    private GegenstandListe gegenstandListe;
    private String name; //TODO Nur Dbs haben Namen!?! Oder auch Gebauden zufaellige Namen generieren?? Eher nicht
    private boolean ausgewaehlt = false;
    private transient FensterOnKlick onKlickFenster = null;

    // BESCHREIBEN //
    public WorldObjekt(float width, float height, GegListe<MusterGeg> musterGegListe_nl) {
        super(width, height);
        this.musterGegListe_nl = musterGegListe_nl;
    }

    // KOPIEREN //
    protected WorldObjekt(WorldObjekt worldObjekt) {
        super(worldObjekt);
        this.musterGegListe_nl = worldObjekt.musterGegListe_nl;
    }

    // INITIALISIEREN //
    protected void init_worldObjekt(Level level, RenderArrayList renderArrayList,
                                    boolean pruefeRenderIndex,
                        Vector2 position) {
        init_worldActor(level, position);
        this.renderArrayList = renderArrayList;
        renderArrayList.add(this, pruefeRenderIndex);

        if (this.musterGegListe_nl != null) {
            this.gegenstandListe = new GegenstandListe(
                    level.getDorf().getForschungGegenstandListe(), musterGegListe_nl);
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public void gotHit() {
        onKlickFenster(true);
    }

    // LOESCHEN ///////////////////////////////////////////////////////////////////////////////////

    public void loesche() {
        // Entferne OnKlickFenster
        if (onKlickFenster != null) { //TODO Sollte am Ende bei nichts mehr null sein
            onKlickFenster.remove();
        }
        onKlickFenster = null; /* Damit keine Verlinkung mehr besteht und der Garbage Colletor es
        einsammelt */

        // Loesche kein Pfad gefunden (Nochmal weiter unten)
        Dorf dorf = getLevel().getDorf();
        boolean loescheKeinPfad = !(this instanceof Ressource
                && ((Ressource) this).getZelle().isUmringtVonKollisionLaufen());
        if (loescheKeinPfad) {
            for (Gebaude g : dorf.getGebaudeListe()) {
                g.setKeinPfad(false);
            }
            for (BodenStapel bs : dorf.getBodenStapelListe()) {
                bs.setKeinPfad(false);
            }
            for (Ressource r : getLevel().getRessourcenListe()) { // TODO Moeglicher performance hit
                r.setKeinPfad(false);
            }
        }

        // Loesche aus Steps
        for (Dorfbewohner d : dorf.gettDorfbewohnerListe()) {
            d.getStackFiniteStepMachine().loescheZiel(this);

            // Nochmal loesche kein Pfad gefunden (aus Performance Gruenden hier)
            if (loescheKeinPfad) {
                d.getKeinPfadListe().clear();
            }
        }

        // Entferne aus diversen Listen
        renderArrayList.remove(this);
        loescheAusSpeziellenListen();
    }

    protected abstract void loescheAusSpeziellenListen();

    // ON KLICK FENSTER ///////////////////////////////////////////////////////////////////////////

    public void onKlickFenster(boolean aktivieren) { //TODO Ueberarbeiten. Alle zB Baume sollen sich ein Fenster teilen?
        if (onKlickFenster == null) {
            erstelleOnKlickFenster();
        }
        if (aktivieren) {
            onKlickFenster.oeffnen(true);
        } else {
            onKlickFenster.schliessen();
        }
    }

    public void erstelleOnKlickFenster() { //TODO Loeschen wenn besseren Weg Fenster zu initialisieren
        if (this instanceof Dorfbewohner) {
            onKlickFenster = new FensterOnKlickDorfbewohner(getLevel(), this, name);
        } else if (this instanceof GebBeruf) {
            if (this instanceof GebBerufFarm) {
                onKlickFenster = new FensterOnKlickGebBerufFarm(getLevel(), (GebBerufFarm) this, name);
            } else if (this instanceof GebBerufHerstSchule) {
                onKlickFenster = new FensterOnKlickGebBerufSchule(getLevel(), (GebBerufHerstSchule) this);
            } else {
                onKlickFenster = new FensterOnKlickGebBerufFabrik(getLevel(), (GebBerufHerstFabrik) this, name);
            }
        } else if (this instanceof GebaudeLager) {
            if (((GebaudeLager)this).isLagerNahrung()) {
                onKlickFenster = new FensterOnKlickNahrungLager(getLevel(), this, name);
            } else {
                onKlickFenster = new FensterOnKlickMaterialLager(getLevel(), (GebaudeLager) this, name);
            }
        } else if (this instanceof GebaudeHaus) {
            onKlickFenster = new FensterOnKlickHaus(getLevel(), this, name);
        } else if (this instanceof Baugrube) {
            onKlickFenster = new FensterOnKlickBaugrube(getLevel(), (Baugrube) this, name);
        } else if (this instanceof Ressource) {
            onKlickFenster = new FensterOnKlickRessource(getLevel(), this, name);
        } else {
            throw new IllegalArgumentException();
        }
        onKlickFenster.settPosition(getPosition(), 0, 0);
    }

    public void onKlickFensterUmschalten() {
        boolean aktivieren;
        if (onKlickFenster == null) {
            aktivieren = true;
        } else {
            aktivieren = !onKlickFenster.isVisible();
        }
        onKlickFenster(aktivieren);
    }

    // POSITIONEN /////////////////////////////////////////////////////////////////////////////////

    @Override
    public void settPosition(Vector2 position) {
        super.settPosition(position);
        if (renderArrayList != null) {
            renderArrayList.pruefeIndex(this);
        }
    }

    @Override
    public void settPositionMitte(Vector2 positionMitte) {
        super.settPositionMitte(positionMitte);
        if (renderArrayList != null) {
            renderArrayList.pruefeIndex(this);
        }
    }

    // OVERRIDE ///////////////////////////////////////////////////////////////////////////////////

    public void mengenVeraenderung() { //Um zB Ressourcen zu verkleinern oder zu loeschen_familieNichtDBs ohne andauernd alle Ressourcen ueberpruefen zu muessen (Spart 20FPS)

    }

    // EINFACHE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public GegenstandListe getGegenstandListe() {
        return gegenstandListe;
    }

    public boolean isAusgewaehlt() {
        return ausgewaehlt;
    }

    public void setAusgewaehlt(boolean ausgewaehlt) {
        this.ausgewaehlt = ausgewaehlt;
    }

    public FensterOnKlick getOnKlickFenster() {
        return onKlickFenster;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public RenderArrayList getRenderArrayList() {
        return renderArrayList;
    }

    public void setRenderArrayList(RenderArrayList renderArrayList) {
        this.renderArrayList = renderArrayList;
    }

    public void setOnKlickFenster(FensterOnKlick onKlickFenster) {
        this.onKlickFenster = onKlickFenster;
    }

    public void setGegenstandListe(GegenstandListe gegenstandListe) {
        this.gegenstandListe = gegenstandListe;
    }
}
