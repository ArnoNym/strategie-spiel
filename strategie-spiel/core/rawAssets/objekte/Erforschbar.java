package de.arnonym.strategiespiel.spielSchirm.objekte;

import com.badlogic.gdx.graphics.g2d.TextureRegion;

import de.arnonym.strategiespiel.spielSchirm.objekte.kollisionsobjekte.gebaude.gebBeruf.GebBerufHerstSchule;
import de.arnonym.strategiespiel.framework.werkzeuge.Enums;

public interface Erforschbar {
    Enums.EnumErforscht getEnumErforscht();
    void setEnumErforscht(Enums.EnumErforscht enumErforscht);
    GebBerufHerstSchule getWirdErforschtVonGebaude();
    void setWirdErforschtVonGebaude(GebBerufHerstSchule wirdErforschtVonGebaude);
    float getForschungsdauer();
    TextureRegion gettIconTextureRegion();

    int getId();
}
