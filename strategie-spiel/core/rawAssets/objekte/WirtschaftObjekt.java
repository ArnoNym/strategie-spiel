package de.arnonym.strategiespiel.spielSchirm.objekte;

import com.badlogic.gdx.math.Vector2;

import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.Geld;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.GegenstandListe;

public interface WirtschaftObjekt {

    Geld getGeld();

    Vector2 getPositionMitte();

    GegenstandListe getGegenstandListe();


}
