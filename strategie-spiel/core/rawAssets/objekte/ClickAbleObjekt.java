package de.arnonym.strategiespiel.spielSchirm.objekte;

import com.badlogic.gdx.math.Vector2;

public interface ClickAbleObjekt {
    boolean issHit(Vector2 clickWorldPosition);
    void gotHit();
}
