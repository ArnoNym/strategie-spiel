package de.arnonym.strategiespiel.spielSchirm.objekte;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import de.arnonym.strategiespiel.spielSchirm.objekte.kollisionsobjekte.Eigentum;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.Geld;

public abstract class Eigentuemer implements Serializable {
    private Geld geld;
    private List<Eigentum> eigentumListe = new ArrayList<>();

    public Eigentuemer(int initialeGeldmenge) {
        this.geld = new Geld(initialeGeldmenge);
    }

    public Geld getGeld() {
        return geld;
    }

    public List<Eigentum> getEigentumListe() {
        return eigentumListe;
    }
}
