package de.arnonym.strategiespiel.spielSchirm.objekte.kollisionsobjekte.gebaude;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

import java.util.List;

import de.arnonym.strategiespiel.spielSchirm.Dorf;
import de.arnonym.strategiespiel.spielSchirm.Level;
import de.arnonym.strategiespiel.spielSchirm.objekte.Eigentuemer;
import de.arnonym.strategiespiel.spielSchirm.objekte.Erforschbar;
import de.arnonym.strategiespiel.spielSchirm.objekte.kollisionsobjekte.gebaude.gebBeruf.GebBerufHerstSchule;
import de.arnonym.strategiespiel.spielSchirm.ui.StatischesSymbol;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.GegListe;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.MusterGeg;
import de.arnonym.strategiespiel.framework.werkzeuge.collections.hashMaps.IntIntHashMap;
import de.arnonym.strategiespiel.spielSchirm.worldMap.RenderArrayList;
import de.arnonym.strategiespiel.framework.werkzeuge.Enums;
import de.arnonym.strategiespiel.framework.werkzeuge.PauseDelta;
import de.arnonym.strategiespiel.spielSchirm.balancing.IdsGebaude;
import de.arnonym.strategiespiel.spielSchirm.balancing.IdsSymbole;
import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenOptik;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.TextureUtils;

/**
 * Created by LeonPB on 07.04.2018.
 */

public abstract class Gebaude extends ZivilObjekt implements Erforschbar {
    private Dorf dorf;
    private List<Gebaude> gebaudeListe;

    private IntIntHashMap noetigesBaumaterial;
    private transient TextureRegion blaupauseTextureRegion; //Nicht direkt aus dem Array weil es bei einem einzelnen baugrube den Unterschied zwischen 152 und 174 FPS ausgemacht hat
    private transient TextureRegion tuerTextureRegion;
    private PauseDelta tuerWirdVerwendetPause = new PauseDelta(KonstantenOptik.TUER_OFFEN_DAUER,
            Enums.EnumPauseStart.VORBEI);

    private Enums.EnumErforscht enumErforscht;
    private GebBerufHerstSchule wirdErforschtVonGebaude = null;
    private boolean genugGeld = true;

    // BESCHREIBEN //
    public Gebaude(int id, boolean erforscht, GegListe<MusterGeg> musterGegListe_nl,
                   Vector2 blaupausePositionsMod, int[][] noetigesBaumaterial) {
        super(id, musterGegListe_nl, blaupausePositionsMod);
        this.noetigesBaumaterial = new IntIntHashMap();
        for (int[] intArray : noetigesBaumaterial) {
            this.noetigesBaumaterial.put(intArray[0], intArray[1]);
        }
        if (erforscht) {
            enumErforscht = Enums.EnumErforscht.JA;
        } else {
            enumErforscht = Enums.EnumErforscht.NEIN;
        }
    }

    // KOPIEREN //
    protected Gebaude(Gebaude gebaude) {
        super(gebaude);
        this.noetigesBaumaterial = gebaude.noetigesBaumaterial;
        this.enumErforscht = gebaude.enumErforscht;
    }

    // INITIALISIEREN //
    protected void init_gebaude(Level level, Dorf dorf, RenderArrayList renderArrayList,
                                List<Gebaude> gebaudeListe, Eigentuemer eigentuemer,
                                Vector2 position, boolean erstelleWirtschaftsdatenmanager) {
        init_zivilObjekt(level, renderArrayList, eigentuemer, position,
                erstelleWirtschaftsdatenmanager);
        this.dorf = dorf;
        this.gebaudeListe = gebaudeListe;
        this.gebaudeListe.add(this);
    }

    // CREATE //
    protected abstract Gebaude create(Level level, Dorf dorf,
                                      RenderArrayList renderArrayList, List<Gebaude> gebaudeListe,
                                      Eigentuemer eigentuemer, Vector2 position);

    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public void update (float delta) {
        super.update(delta);
        tuerWirdVerwendetPause.issVorbei(delta, false);
    }

    @Override
    public void renderOverride(SpriteBatch spriteBatch, List<StatischesSymbol> symbolListe) {
        super.renderOverride(spriteBatch, symbolListe);
        if (!genugGeld) {
            symbolListe.add(IdsSymbole.SYMBOL_NICHT_GENUG_GELD);
        }
        TextureUtils.zeichneTexturRegion(spriteBatch, blaupauseTextureRegion,  getPosition(),
                false);
        if (!tuerWirdVerwendetPause.issVorbei() && tuerTextureRegion != null) {
            TextureUtils.zeichneTexturRegion(spriteBatch, tuerTextureRegion,  getPosition(), false);
        }
    }

    public void tuerVerwenden() {
        tuerWirdVerwendetPause.settStartzeitJetzt();
    }

    // OVERRIDE ///////////////////////////////////////////////////////////////////////////////////

    @Override
    public void ladeTextureRegion_setBounds() {
        TextureRegion[] textureRegionArray = IdsGebaude.gettTexture(getId());
        blaupauseTextureRegion = textureRegionArray[0];
        settBounds(getPosition(), blaupauseTextureRegion.getRegionWidth(), blaupauseTextureRegion
                .getRegionHeight());
        if (textureRegionArray.length > 1) {//TODO Die length preufung sollte nicht mehr noetig sein wenn am Ende alle Tueren haben
            tuerTextureRegion = textureRegionArray[1];
        }
    }

    @Override
    protected void loescheAusSpeziellenListen() {
        gebaudeListe.remove(this);
    }

    // POSITIONEN /////////////////////////////////////////////////////////////////////////////////

    @Override
    protected Vector2 positionZuPositionMitte(Vector2 position) {
        return new Vector2( //EcsDokumentation-002
                position.x + KonstantenOptik.TILE_ORTO_UND_ISOXACHSE_DIAGONAL / 2
                        * (getFundamentGroesse().x + getFundamentGroesse().y) / 2,
                position.y + KonstantenOptik.TILE_ISOYACHSE_DIAGONAL / 2
                        * (getFundamentGroesse().x + getFundamentGroesse().y) / 2
        );
    }

    @Override
    protected Vector2 positionMitteZuPosition(Vector2 positionMitte) {
        return new Vector2( //EcsDokumentation-002
                positionMitte.x - KonstantenOptik.TILE_ORTO_UND_ISOXACHSE_DIAGONAL / 2
                        * (getFundamentGroesse().x + getFundamentGroesse().y) / 2,
                positionMitte.y - KonstantenOptik.TILE_ISOYACHSE_DIAGONAL / 2
                        * (getFundamentGroesse().x + getFundamentGroesse().y) / 2
        );
    }

    // ERFORSCHEN /////////////////////////////////////////////////////////////////////////////////

    @Override
    public GebBerufHerstSchule getWirdErforschtVonGebaude() {
        return wirdErforschtVonGebaude;
    }

    @Override
    public float getForschungsdauer() {
        return 0;
    }

    @Override
    public Enums.EnumErforscht getEnumErforscht() {
        return enumErforscht;
    }

    @Override
    public void setEnumErforscht(Enums.EnumErforscht enumErforscht) {
        this.enumErforscht = enumErforscht;
    }

    @Override
    public void setWirdErforschtVonGebaude(GebBerufHerstSchule wirdErforschtVonGebaude) {
        this.wirdErforschtVonGebaude = wirdErforschtVonGebaude;
    }

    @Override
    public TextureRegion gettIconTextureRegion() {
        return IdsGebaude.gettTexture(getId())[0];
    }

    // EINFACHE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public TextureRegion getBlaupauseTextureRegion() {
        return blaupauseTextureRegion;
    }

    public Dorf getDorf() {
        return dorf;
    }

    public IntIntHashMap getNoetigesBaumaterial() {
        return noetigesBaumaterial;
    }

    public void setGenugGeld(boolean genugGeld) {
        this.genugGeld = genugGeld;
    }
}
