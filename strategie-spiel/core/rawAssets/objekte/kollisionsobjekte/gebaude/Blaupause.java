package de.arnonym.strategiespiel.spielSchirm.objekte.kollisionsobjekte.gebaude;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

import java.util.List;

import de.arnonym.strategiespiel.spielSchirm.IngameCursorManager;
import de.arnonym.strategiespiel.spielSchirm.Level;
import de.arnonym.strategiespiel.spielSchirm.objekte.kollisionsobjekte.StatischesObjekt;
import de.arnonym.strategiespiel.spielSchirm.worldMap.Zelle;
import de.arnonym.strategiespiel.framework.werkzeuge.Assets;
import de.arnonym.strategiespiel.framework.werkzeuge.Enums;
import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenBalancing;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.GeometryUtils;
import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenOptik;

/**
 * Created by LeonPB on 09.04.2018.
 */

public class Blaupause {
    private Level level;
    private IngameCursorManager ingameCursorManager;

    private int gebaudeId;
    private Gebaude gebaude;
    private boolean aktiv = false;

    public Blaupause(Level level, IngameCursorManager ingameCursorManager) {
        this.level = level;
        this.ingameCursorManager = ingameCursorManager;
    }

    public void update(float delta) {
        if (aktiv) {
            gebaude.settPosition(blaupausePosition());
            flaecheIstFrei(gebaude.getPosition(), gebaude.getFundamentGroesse(), true);
        }
    }

    public void render(SpriteBatch spriteBatch) {
        if (aktiv) {
            gebaude.render(spriteBatch);
        }
    }

    // POSITION MODIFIKATOREN /////////////////////////////////////////////////////////////////////

    private Vector2 blaupausePosition() {
        return GeometryUtils.ganzerTile(
                applyMausModifikator(
                        level.getViewport().unproject(
                                new Vector2(Gdx.input.getX(), Gdx.input.getY())),
                        gebaude.getFundamentGroesse()),
                gebaude.getFundamentGroesse());
    }

    public Vector2 applyMausModifikator(Vector2 position, Vector2 blaubpausenMod) {
        return position; //TODO. (Hat keine Auswirkungen auf die Kollision beim bauen
    }

    // PLANEN UND BAUEN ///////////////////////////////////////////////////////////////////////////

    public void planen(int gebaudeId) {
        aktiv = true;
        this.gebaudeId = gebaudeId;
        gebaude = level.getDorf().getForschungGebaudeListe().gettDurchId(gebaudeId);
        gebaude.settPosition(blaupausePosition());
        gebaude.lade();
        level.getWorldMap().setGridZeichnen(true);
        ingameCursorManager.setCursorStatus(Enums.EnumIngameWorldCursorStatus.NICHTS);
        ingameCursorManager.getFensterInfo().aktiviereBlaupause(gebaude);
    }

    public void planenBeenden() {
        aktiv = false;
        gebaude = null;
        level.getWorldMap().setGridZeichnen(false);
        ingameCursorManager.setCursorStatus(Enums.EnumIngameWorldCursorStatus.ZELLE_UND_GEBAUDE_UND_CURSOR);
        ingameCursorManager.getFensterInfo().deaktiviereBlaupause();
    }

    public void bauen(List<Gebaude> gebaudeListe) {
        if (!flaecheIstFrei(gebaude.getPosition(), gebaude.getFundamentGroesse(), false)) {
            return;
        }

        StatischesObjekt neuesKollObjekt;
        if (KonstantenBalancing.DEVELOPER_GAMEPLAY_IMMER_SOFORT_GEBAUT ||
                KonstantenBalancing.DEVELOPER_GAMEPLAY_ERSTES_GEBAUDE_SOFORT_GEBAUT
                        && level.getDorf().getGebaudeListe().isEmpty()) {
            Gebaude gebautesGebaude = gebaude.create(level, level.getDorf(), level
                    .getRenderArrayList(), level.getDorf().getGebaudeListe(), level.getDorf(),
                    new Vector2(gebaude.getPosition()));
            neuesKollObjekt = gebautesGebaude;
        } else {
            Baugrube baugrube = new Baugrube(level, level.getDorf(), level.getRenderArrayList(),
                    level.getDorf(), gebaude);
            level.getDorf().getBaugrubenListe().add(baugrube);
            neuesKollObjekt = baugrube;
        }

        for (Vector2 v : GeometryUtils.gettIndizesListe(gebaude.getPosition(),
                gebaude.getFundamentGroesse())) {
            level.getWorldMap().gett((int)v.x, (int)v.y).settStatischesObjekt(neuesKollObjekt);
        }
    }

    // ZELLEN /////////////////////////////////////////////////////////////////////////////////////

    private boolean flaecheIstFrei(Vector2 position, Vector2 blaubpausenMod, boolean
            markierenObFrei) {
        boolean flaecheIstFrei = true;
        for (Vector2 v : GeometryUtils.gettIndizesListe(position, blaubpausenMod)) {
            Zelle zelle = level.getWorldMap().gett((int) v.x, (int) v.y);
            if (zelle == null || zelle.isKollisionBauen()) {
                if (markierenObFrei) {
                    ingameCursorManager.getZellenMarkierungRenderer().addPosition(GeometryUtils.ortoToIso(new Vector2(
                            v.x * KonstantenOptik.TILE_ORTO_WIDTH_AND_HEIGHT,
                            v.y * KonstantenOptik.TILE_ORTO_WIDTH_AND_HEIGHT)), Assets.instance
                            .tiledMapAssets.durchsichtigRot, true);
                    flaecheIstFrei = false;
                } else {
                    return false;
                }
            } else if (markierenObFrei) {
                /*zellenMarkierer.addWeissPosition(GeometryUtils.ortoToIso(new Vector2( //TODO Entscheidung treffen
                        v.x * KonstantenOptik.TILE_ORTO_WIDTH_AND_HEIGHT,
                        v.y * KonstantenOptik.TILE_ORTO_WIDTH_AND_HEIGHT)));*/
            }
        }
        return flaecheIstFrei;
    }

    // EINFACHE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public boolean getAktiv () {
        return aktiv;
    }

    public Gebaude getGebaude() {
        return gebaude;
    }

    public IngameCursorManager getIngameCursorManager() {
        return ingameCursorManager;
    }
}
