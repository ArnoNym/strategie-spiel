package de.arnonym.strategiespiel.spielSchirm.objekte.kollisionsobjekte.gebaude.gebBeruf;

import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;
import java.util.List;

import de.arnonym.strategiespiel.spielSchirm.Dorf;
import de.arnonym.strategiespiel.spielSchirm.Level;
import de.arnonym.strategiespiel.spielSchirm.objekte.Eigentuemer;
import de.arnonym.strategiespiel.spielSchirm.objekte.dorfbewohnerAi.berufe.BerufListe;
import de.arnonym.strategiespiel.spielSchirm.objekte.kollisionsobjekte.gebaude.Gebaude;
import de.arnonym.strategiespiel.spielSchirm.worldMap.RenderArrayList;
import de.arnonym.strategiespiel.spielSchirm.worldMap.Zelle;

public class GebBerufFarm extends GebBeruf {
    private int[] ressourcenIDs;
    private int maxBewirtschafteteZellen;

    private int aktuelleRessourcenId;
    private List<Zelle> bewirtschafteteZellenListe = new ArrayList<>();

    // BESCHREIBEN //
    public GebBerufFarm(int id, boolean erforscht, Vector2 blaupausePositionMod,
                        int[][] noetigesBaumaterial, BerufListe berufeListe, int[] ressourcenIDs,
                        int maxBewirtschafteteZellen) {
        super(id, erforscht, null, blaupausePositionMod, noetigesBaumaterial,
                berufeListe);
        this.ressourcenIDs = ressourcenIDs;
        this.maxBewirtschafteteZellen = maxBewirtschafteteZellen;
    }

    // KOPIEREN //
    private GebBerufFarm(GebBerufFarm gebBerufFarm) {
        super(gebBerufFarm);
        this.ressourcenIDs = gebBerufFarm.ressourcenIDs;
        this.maxBewirtschafteteZellen = gebBerufFarm.maxBewirtschafteteZellen;
    }

    // INITIALISIEREN //
    private void init_gebBerufFarm(Level level, Dorf dorf, RenderArrayList renderArrayList,
                      List<Gebaude> gebaudeListe, Eigentuemer eigentuemer, Vector2 position) {
        init_gebBeruf(level, dorf, renderArrayList, gebaudeListe, eigentuemer, position, false);
        this.aktuelleRessourcenId = ressourcenIDs[0]; //TODO Auswahlmenue oeffnen
    }

    // CREATE //
    @Override
    protected GebBerufFarm create(Level level, Dorf dorf, RenderArrayList renderArrayList,
                             List<Gebaude> gebaudeListe, Eigentuemer eigentuemer,
                             Vector2 position) {
        GebBerufFarm gebBerufFarm = new GebBerufFarm(this);
        gebBerufFarm.init_gebBerufFarm(level, dorf, renderArrayList, gebaudeListe, eigentuemer,
                position);
        return gebBerufFarm;
    }

    /*@Override
    protected Gebaude create(Level level, Dorf dorf, RenderArrayList renderArrayList,
                             List<Gebaude> gebaudeListe, Eigentuemer eigentuemer,
                             Vector2 position) {
        GebBerufFarm gebBerufFarm = new GebBerufFarm(gettId(),
                getEnumErforscht() == Enums.EnumErforscht.JA, getMusterGegListe(),
                getFundamentGroesse(), getNoetigesBaumaterial(), getGesuchtListe(), ressourcenIDs,
                maxBewirtschafteteZellen);
        gebBerufFarm.init_gebBeruf(level, dorf, renderArrayList, gebaudeListe, eigentuemer,
                position, false);
        gebBerufFarm.aktuelleRessourcenId = ressourcenIDs[0]; //TODO Auswahlmenue oeffnen
        return gebBerufFarm;
    }*/

    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public void loesche() {
        super.loesche();
        for (Zelle z : bewirtschafteteZellenListe) {
            z.settBesitzer(null);
        }
    }

    public void addUndPruefeLand(List<Zelle> zellenListe) {
        if (!zellenListe.isEmpty() && getBewirtschafteteZellenListe().contains(zellenListe.get(0))) {
            for (Zelle z : zellenListe) {
                if (bewirtschafteteZellenListe.contains(z)) {
                    bewirtschafteteZellenListe.remove(z);
                    z.settBesitzer(null);//TODO Soll auch Eigentuemer uebertragen werden?? Wenn ja: Kaufpreis erhalten
                }
            }
        } else {
            for (Zelle z : zellenListe) {
                if (bewirtschafteteZellenListe.size() >= maxBewirtschafteteZellen) {
                    return;
                }
                if (!gettBesitzListe().contains(z)) { //BesitzListe inkludiert Land auf dem das ZivilObjekt steht
                    bewirtschafteteZellenListe.add(z);
                    z.settBesitzer(this);//TODO Soll auch Eigentuemer uebertragen werden?? Wenn ja: Kaufpreis zahlen
                }
            }
        }
    }

    // KOMPLEXE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    @Override
    public List<Zelle> gettBesitzListe() {
        List<Zelle> besitzListe = new ArrayList<>(getAufZellenListe());
        besitzListe.addAll(getBewirtschafteteZellenListe());
        return besitzListe;
    }

    // EINFACHE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public List<Zelle> getBewirtschafteteZellenListe() {
        return bewirtschafteteZellenListe;
    }

    public int getAktuelleRessourcenId() {
        return aktuelleRessourcenId;
    }

    public int getMaxBewirtschafteteZellen() {
        return maxBewirtschafteteZellen;
    }
}
