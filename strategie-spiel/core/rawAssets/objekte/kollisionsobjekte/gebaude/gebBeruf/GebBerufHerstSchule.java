package de.arnonym.strategiespiel.spielSchirm.objekte.kollisionsobjekte.gebaude.gebBeruf;

import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import de.arnonym.strategiespiel.spielSchirm.Dorf;
import de.arnonym.strategiespiel.spielSchirm.Level;
import de.arnonym.strategiespiel.spielSchirm.objekte.Eigentuemer;
import de.arnonym.strategiespiel.spielSchirm.objekte.Erforschbar;
import de.arnonym.strategiespiel.spielSchirm.objekte.dorfbewohnerAi.Dorfbewohner;
import de.arnonym.strategiespiel.spielSchirm.objekte.dorfbewohnerAi.berufe.Beruf;
import de.arnonym.strategiespiel.spielSchirm.objekte.dorfbewohnerAi.berufe.BerufListe;
import de.arnonym.strategiespiel.spielSchirm.objekte.kollisionsobjekte.gebaude.Gebaude;
import de.arnonym.strategiespiel.spielSchirm.worldMap.RenderArrayList;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.GegenstandListe;
import de.arnonym.strategiespiel.framework.werkzeuge.Enums;

public class GebBerufHerstSchule extends GebBerufHerst {
    // Schueler
    private final Enums.EnumBildungsgrad enumBildungsgrad;
    private final int schuelerMaxAlter;
    private final int schuelerMax;
    private final int schuelerProLehrerMax;
    //
    private List<Dorfbewohner> schuelerListe = new ArrayList<>(); // Die in einem laengeren Zeitraum die Schule besuchen

    // Forschung
    private final int[] kannErforschenIds;
    //
    private Erforschbar erforschbar;
    private float erforschenRestDauer;

    // BESCHREIBEN //
    public GebBerufHerstSchule(int id, boolean erforscht, GegenstandListe kopierenGegenstandListe,
                               Vector2 blaupausePositionMod, int[][] noetigesBaumaterial,
                               BerufListe berufeListe, Enums.EnumBildungsgrad enumBildungsgrad,
                               int schuelerMaxAlter, int schuelerMax, int schuelerProLehrerMax,
                               int... kannErforschenIds) {
        super(id, erforscht, null, blaupausePositionMod, noetigesBaumaterial,
                berufeListe);
        this.enumBildungsgrad = enumBildungsgrad;
        this.schuelerMaxAlter = schuelerMaxAlter;
        this.schuelerMax = schuelerMax;
        this.schuelerProLehrerMax = schuelerProLehrerMax;
        this.kannErforschenIds = kannErforschenIds; // TODO Input fuer Forschung ermoeglichen. Auch in GegListe einfuegen!
    }

    // KOPIEREN //
    public GebBerufHerstSchule(GebBerufHerstSchule gebBerufHerstSchule) {
        super(gebBerufHerstSchule);
        this.enumBildungsgrad = gebBerufHerstSchule.enumBildungsgrad;
        this.schuelerMaxAlter = gebBerufHerstSchule.schuelerMaxAlter;
        this.schuelerMax = gebBerufHerstSchule.schuelerMax;
        this.schuelerProLehrerMax = gebBerufHerstSchule.schuelerProLehrerMax;
        this.kannErforschenIds = gebBerufHerstSchule.kannErforschenIds;
    }

    // INITIALISIEREN //
    public void init_GebBerufHerstSchule(Level level, Dorf dorf, RenderArrayList renderArrayList,
                     List<Gebaude> gebaudeListe, Eigentuemer eigentuemer, Vector2 position) {
        init_gebBerufHerst(level, dorf, renderArrayList, gebaudeListe, eigentuemer, position,
                false);
    }

    // CREATE //
    @Override
    protected GebBerufHerstSchule create(Level level, Dorf dorf, RenderArrayList renderArrayList,
                                         List<Gebaude> gebaudeListe, Eigentuemer eigentuemer,
                                         Vector2 position) {
        GebBerufHerstSchule gebBerufHerstSchule = new GebBerufHerstSchule(this);
        gebBerufHerstSchule.init_GebBerufHerstSchule(level, dorf, renderArrayList, gebaudeListe,
                eigentuemer, position);
        return gebBerufHerstSchule;
    }

    /*@Override
    protected GebBerufHerstSchule create(Level level, Dorf dorf, RenderArrayList renderArrayList,
                                         List<Gebaude> gebaudeListe, Eigentuemer eigentuemer, Vector2 position) {
        GebBerufHerstSchule neueGebBerufHerstSchule = new GebBerufHerstSchule(gettId(),
                getEnumErforscht() == Enums.EnumErforscht.JA, getMusterGegListe(),
                getFundamentGroesse(), getNoetigesBaumaterial(), getGesuchtListe(), enumBildungsgrad,
                schuelerMaxAlter, schuelerMax, schuelerProLehrerMax, kannErforschenIds);
        neueGebBerufHerstSchule.init_gebBeruf(level, dorf, renderArrayList, gebaudeListe,
                eigentuemer, position, false);
        return neueGebBerufHerstSchule;
    }*/

    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////

    // ARBEITEN ///////////////////////////////////////////////////////////////////////////////////

    @Override
    public void addArbeitszeit(Beruf beruf, float arbeitszeit) {
        int anzahlSchueler = schuelerListe.size();
        if (anzahlSchueler > 0) {
            float vermittelteBildung = 0.001f * arbeitszeit; //TODO Konstanten
            float vermittelteBildungProSchueler = vermittelteBildung / anzahlSchueler;
            for (Dorfbewohner db : schuelerListe) {
                db.addBildung(enumBildungsgrad, vermittelteBildungProSchueler);
            }
        }

        if (isHerstellen()) {
            erforschenRestDauer -= arbeitszeit;
            if (erforschenRestDauer <= 0) {
                erforschbar.setEnumErforscht(Enums.EnumErforscht.JA);
                setHerstellen(false);
                erforschbar = null;
            }
        }
    }

    @Override
    public void kuendigen(Dorfbewohner dorfbewohner) {
        super.kuendigen(dorfbewohner);

        int schueler = schuelerListe.size();
        int maxSchueler = schuelerProLehrerMax * getAngestellteListe().size();
        for (int i = schueler-1; i > maxSchueler; i--) {
            schuelerListe.remove(i);
        }
    }

    // FORSCHUNG //////////////////////////////////////////////////////////////////////////////////

    @Override
    public void update(float delta) {
        super.update(delta);
    }

    public void settErforschen(Erforschbar erforschbar_NL) {
        if (erforschbar == erforschbar_NL) {
            throw new IllegalArgumentException();
        }
        if (erforschbar_NL != null) {
            if (!IntStream.of(kannErforschenIds).anyMatch(id -> id == erforschbar_NL.getId())) {
                throw new IllegalArgumentException();
            }

            setHerstellen(true);
            erforschenRestDauer = erforschbar_NL.getForschungsdauer();
            erforschbar_NL.setEnumErforscht(Enums.EnumErforscht.DABEI);
            erforschbar_NL.setWirdErforschtVonGebaude(this);
        } else {
            setHerstellen(false);
        }
        setHergestelltProzent(0);
        if (erforschbar != null && erforschbar.getEnumErforscht() == Enums.EnumErforscht.DABEI) {
            erforschbar.setEnumErforscht(Enums.EnumErforscht.NEIN);
        }
        this.erforschbar = erforschbar_NL;
    }

    @Override
    public boolean genugOutputPlatz() {
        return true;
    }

    @Override
    public boolean genugInput() {
        if (schuelerListe.size() > 0) {
            return true;
        }
        if (erforschbar != null) {
            return true;
        }
        return false; //TODo Material fuer die Forschung. Oder Geld fuer die Forschung?!?
    }

    // SCHUELER ///////////////////////////////////////////////////////////////////////////////////

    public boolean issPassendeSchule(Dorfbewohner dorfbewohner) {
        //TODO Alter, Bildungsgrad usw
        if (dorfbewohner.gettAlter() > schuelerMaxAlter) {
            return false;
        }
        if (schuelerListe.size() >= schuelerMax
                || schuelerListe.size() >= schuelerProLehrerMax * getAngestellteListe().size()) {
            return false;
        }
        return true;
    }

    public void addNeuerSchueler(Dorfbewohner dorfbewohner) {
        schuelerListe.add(dorfbewohner);
    }

    public void beendeSchule(Dorfbewohner schueler) {
        if (!schuelerListe.contains(schueler)) {
            throw new IllegalArgumentException("DB ist kein Schueler dieser Schule!");
        }
        schuelerListe.remove(schueler);
    }

    public int gettMaxSchueler() {
        return Math.min(schuelerMax, schuelerProLehrerMax * gettAnzahlAngestellte());
    }

    public int gettAnzahlSchueler() {
        return schuelerListe.size();
    }

    // EINFACHE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public Enums.EnumBildungsgrad getEnumBildungsgrad() {
        return enumBildungsgrad;
    }

    public int getSchuelerMaxAlter() {
        return schuelerMaxAlter;
    }

    public Erforschbar getErforschbar() {
        return erforschbar;
    }

    public float getErforschenRestDauer() {
        return erforschenRestDauer;
    }

    public int[] getKannErforschenIds() {
        return kannErforschenIds;
    }
}
