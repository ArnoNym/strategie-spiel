package de.arnonym.strategiespiel.spielSchirm.objekte.kollisionsobjekte.gebaude.gebBeruf;

import com.badlogic.gdx.math.Vector2;

import java.util.List;
import java.util.Map;

import de.arnonym.strategiespiel.spielSchirm.Dorf;
import de.arnonym.strategiespiel.spielSchirm.Level;
import de.arnonym.strategiespiel.spielSchirm.objekte.Eigentuemer;
import de.arnonym.strategiespiel.spielSchirm.ecsErgaenzung.stp.steps.StepHerstellen;
import de.arnonym.strategiespiel.spielSchirm.objekte.dorfbewohnerAi.berufe.Beruf;
import de.arnonym.strategiespiel.spielSchirm.objekte.dorfbewohnerAi.berufe.BerufListe;
import de.arnonym.strategiespiel.spielSchirm.objekte.kollisionsobjekte.gebaude.Gebaude;
import de.arnonym.strategiespiel.spielSchirm.worldMap.RenderArrayList;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.InfoGeg;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.GegListe;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.MusterGeg;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.Gegenstand;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.GegenstandListe;
import de.arnonym.strategiespiel.framework.werkzeuge.collections.hashMaps.IntIntHashMap;
import de.arnonym.strategiespiel.framework.werkzeuge.Enums;
import de.arnonym.strategiespiel.spielSchirm.balancing.Konstanten;
import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenBalancing;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.MathUtils;

public class GebBerufHerstFabrik extends GebBerufHerst {
    private int[] produktIDs;

    private int produktId; //Kan ResId im Falle einer Farm oder GegId im Falle einer Fabrik sein

    // BESCHREIBEN //
    public GebBerufHerstFabrik(int id, boolean erforscht, int platzMax,
                               Vector2 blaupausePositionMod, int[][] noetigesBaumaterial,
                               BerufListe berufeListe, int[] produktIDs) {
        super(id, erforscht, new GegListe<>(platzMax), blaupausePositionMod, noetigesBaumaterial, berufeListe);
        this.produktIDs = produktIDs;
    }

    // KOPIEREN //
    public GebBerufHerstFabrik(GebBerufHerstFabrik gebBerufHerstFabrik) {
        super(gebBerufHerstFabrik);
        this.produktIDs = gebBerufHerstFabrik.produktIDs;
    }

    // INITIALISIEREN //
    private void init_GebBerufHerstFabrik(Level level, Dorf dorf, RenderArrayList renderArrayList,
                      List<Gebaude> gebaudeListe, Eigentuemer eigentuemer, Vector2 position) {
        InfoGegListe infoGegListe = dorf.getForschungGegenstandListe();
        for (int produktId : produktIDs) {
            getGegenstandListe().addGegenstand(infoGegListe, produktId, 0,
                    KonstantenBalancing.GEB_BERUF_HERST_FABRIK_PRODUKT_MAX_MENGE, -1); // TODO Vielleicht im Wert spreichern wieviel ein Geg aktuell fuer eine Fabrik wert ist
            for (int inputId : infoGegListe.gettDurchId(produktId).getInputHashMap().keySet()) {
                if (getGegenstandListe().getDurchId(inputId) == null) {
                    getGegenstandListe().addGegenstand(inputId, 0,
                            KonstantenBalancing.GEB_BERUF_HERST_FABRIK_PRODUKT_MAX_MENGE);
                }
            }
        }
        init_gebBerufHerst(level, dorf, renderArrayList, gebaudeListe, eigentuemer, position, true);
        setGegenstandListe(erstelleGegenstandListe(infoGegListe, getGegenstandListe().getMaxPlatz(),
                produktIDs)); /* Nach create damit gegListe erstellt ist und vor settHerstellen,
                damit neuberechnung des GegMaxPlatzes funktioniert */
        settHerstellen(produktIDs[0]); //TODO Menue oeffnen wo Spieler das auswaehlt. Vorher sollen keine Stellen angeboten werden
    }

    // CREATE //
    @Override
    protected GebBerufHerstFabrik create(Level level, Dorf dorf, RenderArrayList renderArrayList,
                             List<Gebaude> gebaudeListe, Eigentuemer eigentuemer,
                             Vector2 position) {
        GebBerufHerstFabrik gebBerufHerstFabrik = new GebBerufHerstFabrik(this);
        gebBerufHerstFabrik.init_GebBerufHerstFabrik(level, dorf, renderArrayList, gebaudeListe,
                eigentuemer, position);
        return gebBerufHerstFabrik;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public void update(float delta) {
        float hergestelltProzent = getHergestelltProzent();

        if (hergestelltProzent > 0 && !isHerstellen()) {
            if (!genugInput()) throw new IllegalStateException();

            IntIntHashMap herstellenHashmap = gettHerstellen().gettInputHashMap();
            for (Map.Entry<Integer, Integer> entry : herstellenHashmap.entrySet()) {
                Gegenstand g = getGegenstandListe().getDurchId(entry.getKey());
                g.mengeVerringern(entry.getValue(), false);
            }

            gettHerstellen().reservierenLegen(gettHerstellen().gettEntstehendeMenge());

            setHerstellen(true);
        }
        if (hergestelltProzent >= 1 && isHerstellen()) {
            getGegenstandListe().getDurchId(getProduktId()).mengeErhoehen(gettHerstellen()
                    .gettEntstehendeMenge(), true);

            setHergestelltProzent(hergestelltProzent - 1);
            setHerstellen(false);

            for (StepHerstellen sh : getStepHerstellenListe()) {
                sh.setStepStatus(Enums.EnumStepStatus.ERLEDIGT); // TODO Pruefen ob erledigt
            }
        }

        super.update(delta);
    }

    // GEGENSTAND LISTE ///////////////////////////////////////////////////////////////////////////

    private GegenstandListe erstelleGegenstandListe(GegListe<InfoGeg> infoGegListe, int maxPlatz,
                                                    int[] produktIDs) {
        GegListe<MusterGeg> musterGegListe = new GegListe<>(maxPlatz);
        for (int id : produktIDs) {
            musterGegListe.add(true, new MusterGeg(id, 0, null, 0));
            for (int inputId : infoGegListe.gettDurchId(id).getInputHashMap().keySet()) {
                musterGegListe.add(true, new MusterGeg(id, 0, null, 0));
            }
        }
        return new GegenstandListe(infoGegListe, musterGegListe);
    }

    public void errechnePlatzFuerGebBeruf(int produktId) { /* In das Gebaude sollen
        unterschiedliche Mengen von unterschiedlich grossen Outputs passen. In jedem Fall sollen
        jedoch genug Einheiten der Inputs Platz finden um die zuvor errechnete Menge an Outputs zu
        erstellen. */
        GegenstandListe gl = getGegenstandListe();
        Gegenstand aktuellesProdukt = gl.getDurchId(produktId);

        int mengeProdukt = MathUtils.myIntDiv(
                gl.maxPlatzUrspruenglich,
                aktuellesProdukt.gettPlatzVerbrauch());
        aktuellesProdukt.setMengeMaximal(mengeProdukt);
        int platzMultiplikator = aktuellesProdukt.gettPlatzVerbrauch();

        IntIntHashMap herstellenHashmap = aktuellesProdukt.gettInputHashMap();

        for (Map.Entry<Integer, Integer> entry : herstellenHashmap.entrySet()) {
            int key = entry.getKey();
            int value = entry.getValue();

            int maximaleMenge = mengeProdukt * value / Konstanten.NACHKOMMASTELLEN;
            gl.getDurchId(key).setMengeMaximal(maximaleMenge);
            platzMultiplikator += value * gl.gegInfoListe.gettDurchId(key).getPlatzVerbrauch()
                    / Konstanten.NACHKOMMASTELLEN;
        }

        gl.settMaxPlatz(mengeProdukt * platzMultiplikator / Konstanten.NACHKOMMASTELLEN);
    }

    // PRUEFE IN- UND OUTPUT //////////////////////////////////////////////////////////////////////

    public boolean genugOutputPlatz() {
        return gettHerstellen().gettEntstehendeMenge() <= gettHerstellen().getMengeBisVoll();
    }

    public boolean genugInput() {
        IntIntHashMap herstellenHashMap = gettHerstellen().gettInputHashMap();
        for (Integer key : herstellenHashMap.keySet()) {
            if (herstellenHashMap.get(key) > getGegenstandListe().getDurchId(key)
                    .getMengeVerfuegbar()) {
                return false;
            }
        }
        return true;
    }

    // KOMPLEXE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public void settHerstellen(int id) {
        produktId = id;
        setHergestelltProzent(0);
        getGegenstandListe().errechnePlatzFuerGebBeruf(id);
        getBerufeListe().aenderungAktuellesProdukt(gettHerstellen());
    }

    public Gegenstand gettHerstellen() {
        return getGegenstandListe().getDurchId(produktId);
    }

    public void addArbeitszeit(Beruf beruf, float arbeitszeit) {
        float hergProz = arbeitszeit / gettHerstellen().gettProduktidauer()
                * beruf.gettProduktivitaet();
        setHergestelltProzent(getHergestelltProzent() + hergProz);
    }

    // EINFACHE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public int getProduktId() {
        return produktId;
    }

    public int[] getProduktIDs() {
        return produktIDs;
    }
}
