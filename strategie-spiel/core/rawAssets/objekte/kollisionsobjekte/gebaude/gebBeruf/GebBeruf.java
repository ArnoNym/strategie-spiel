package de.arnonym.strategiespiel.spielSchirm.objekte.kollisionsobjekte.gebaude.gebBeruf;

import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;
import java.util.List;

import de.arnonym.strategiespiel.spielSchirm.Dorf;
import de.arnonym.strategiespiel.spielSchirm.Level;
import de.arnonym.strategiespiel.spielSchirm.objekte.Eigentuemer;
import de.arnonym.strategiespiel.spielSchirm.objekte.dorfbewohnerAi.Dorfbewohner;
import de.arnonym.strategiespiel.spielSchirm.objekte.dorfbewohnerAi.berufe.BerufListe;
import de.arnonym.strategiespiel.spielSchirm.objekte.kollisionsobjekte.gebaude.Gebaude;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.GegListe;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.MusterGeg;
import de.arnonym.strategiespiel.spielSchirm.worldMap.RenderArrayList;


/**
 * Created by LeonPB on 12.04.2018.
 */


public abstract class GebBeruf extends Gebaude {
    private BerufListe berufeListe;

    private List<Dorfbewohner> angestellteListe = new ArrayList();

    // BESCHREIBEN //
    public GebBeruf(int id, boolean erforscht, GegListe<MusterGeg> musterGegListe,
                    Vector2 blaupausePositionMod, int[][] noetigesBaumaterial,
                    BerufListe berufeListe) {
        super(id, erforscht, musterGegListe, blaupausePositionMod, noetigesBaumaterial);
        this.berufeListe = berufeListe;
    }

    // KOPIEREN //
    public GebBeruf(GebBeruf gebBeruf) {
        super(gebBeruf);
        this.berufeListe = gebBeruf.berufeListe;
    }

    // INITIALISIEREN //
    protected void init_gebBeruf(Level level, Dorf dorf, RenderArrayList renderArrayList,
                        List<Gebaude> gebaudeListe, Eigentuemer eigentuemer,
                        Vector2 position, boolean erstelleWirtschaftsdatenmanage) {
        init_gebaude(level, dorf, renderArrayList, gebaudeListe, eigentuemer, position,
                erstelleWirtschaftsdatenmanage);
        this.berufeListe = berufeListe.create(dorf, this);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public void loesche() {
        super.loesche();
        kuendigeAlle();
    }

    @Override
    public void update(float delta) {
        getBerufeListe().update(delta);
    }

    // MITARBEITER ////////////////////////////////////////////////////////////////////////////////

    public void kuendigeAlle() {
        for (Dorfbewohner d : angestellteListe) {
            d.setGebBeruf(null);
        }
    }

    public void einstellen(Dorfbewohner dorfbewohner) {
        angestellteListe.add(dorfbewohner);
    }

    public void kuendigen(Dorfbewohner dorfbewohner) { //Wird in Schule erweitert
        angestellteListe.remove(dorfbewohner);
    }

    // KOMPLEXE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public int gettAnzahlAngestellte() {
        return getAngestellteListe().size();
    }

    public int gettMaxAngestellte() {
        return berufeListe.size();
    }

    // PROTECTED GETTER UND SETTER ////////////////////////////////////////////////////////////////

    protected List<Dorfbewohner> getAngestellteListe() {
        return angestellteListe;
    }

    // EINFACHE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public BerufListe getBerufeListe() {
        return berufeListe;
    }
}
