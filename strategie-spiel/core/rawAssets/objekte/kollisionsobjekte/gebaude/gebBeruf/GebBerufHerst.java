package de.arnonym.strategiespiel.spielSchirm.objekte.kollisionsobjekte.gebaude.gebBeruf;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;
import java.util.List;

import de.arnonym.strategiespiel.spielSchirm.Dorf;
import de.arnonym.strategiespiel.spielSchirm.Level;
import de.arnonym.strategiespiel.spielSchirm.objekte.Eigentuemer;
import de.arnonym.strategiespiel.spielSchirm.ecsErgaenzung.stp.steps.StepHerstellen;
import de.arnonym.strategiespiel.spielSchirm.objekte.dorfbewohnerAi.berufe.Beruf;
import de.arnonym.strategiespiel.spielSchirm.objekte.dorfbewohnerAi.berufe.BerufListe;
import de.arnonym.strategiespiel.spielSchirm.objekte.kollisionsobjekte.gebaude.Gebaude;
import de.arnonym.strategiespiel.spielSchirm.ui.StatischesSymbol;
import de.arnonym.strategiespiel.spielSchirm.worldMap.RenderArrayList;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.GegListe;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.MusterGeg;
import de.arnonym.strategiespiel.spielSchirm.balancing.IdsSymbole;

public abstract class GebBerufHerst extends GebBeruf {
    private boolean herstellen = false;
    private float hergestelltProzent = 0;
    private List<StepHerstellen> stepHerstellenListe = new ArrayList();

    private boolean genugInput = true;
    private boolean genugOutputKaeufer = true;

    // BESCHREIBEN //
    protected GebBerufHerst(int id, boolean erforscht, GegListe<MusterGeg> musterGegListe,
                            Vector2 blaupausePositionMod, int[][] noetigesBaumaterial,
                            BerufListe berufeListe) {
        super(id, erforscht, musterGegListe, blaupausePositionMod, noetigesBaumaterial,
                berufeListe);
    }

    // KOPIEREN //
    protected GebBerufHerst(GebBerufHerst gebBerufHerst) {
        super(gebBerufHerst);
    }

    // INITIALISIEREN //
    protected void init_gebBerufHerst(Level level, Dorf dorf, RenderArrayList renderArrayList,
                     List<Gebaude> gebaudeListe, Eigentuemer eigentuemer, Vector2 position,
                     boolean erstelleWirtschaftsdatenmanage) {
        init_gebBeruf(level, dorf, renderArrayList, gebaudeListe, eigentuemer, position,
                erstelleWirtschaftsdatenmanage);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public void renderOverride(SpriteBatch spriteBatch, List<StatischesSymbol> symbolListe) {
        super.renderOverride(spriteBatch, symbolListe);
        if (!genugInput) {
            symbolListe.add(IdsSymbole.SYMBOL_NICHT_GENUG_INPUT);
        }
        if (!genugOutputKaeufer) {
            symbolListe.add(IdsSymbole.SYMBOL_NICHT_GENUG_OUTPUT_KAEUFER);
        }
    }

    // HERSTELLEN /////////////////////////////////////////////////////////////////////////////////

    public abstract boolean genugOutputPlatz();

    public abstract boolean genugInput();

    public abstract void addArbeitszeit(Beruf beruf, float arbeitszeit);

    // EINFACHE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public boolean isHerstellen() {
        return herstellen;
    }

    public void setHerstellen(boolean herstellen) {
        this.herstellen = herstellen;
    }

    public float getHergestelltProzent() {
        return hergestelltProzent;
    }

    public void setHergestelltProzent(float hergestelltProzent) {
        this.hergestelltProzent = hergestelltProzent;
    }

    public List<StepHerstellen> getStepHerstellenListe() {
        return stepHerstellenListe;
    }

    public void setGenugInput(boolean genugInput) {
        this.genugInput = genugInput;
    }

    public void setGenugOutputKaeufer(boolean genugOutputKaeufer) {
        this.genugOutputKaeufer = genugOutputKaeufer;
    }
}
