package de.arnonym.strategiespiel.spielSchirm.objekte.kollisionsobjekte.gebaude;

import com.badlogic.gdx.math.Vector2;

import java.util.List;

import de.arnonym.strategiespiel.spielSchirm.Dorf;
import de.arnonym.strategiespiel.spielSchirm.Level;
import de.arnonym.strategiespiel.spielSchirm.objekte.Eigentuemer;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.GegListe;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.MusterGeg;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.Gegenstand;
import de.arnonym.strategiespiel.spielSchirm.worldMap.RenderArrayList;
import de.arnonym.strategiespiel.framework.werkzeuge.Enums;
import de.arnonym.strategiespiel.framework.werkzeuge.PauseDelta;
import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenBalancing;

/**
 * Created by LeonPB on 08.04.2018.
 */

public class GebaudeLager extends Gebaude {
    private boolean lagerNahrung;

    private PauseDelta updatePreisePause;

    // BESCHREIBEN //
    public GebaudeLager(int id, boolean erforscht, GegListe<MusterGeg> musterGegListe,
                        boolean lagerNahrung, Vector2 blaupausePositionMod,
                        int[][]  noetigesBaumaterial) {
        super(id, erforscht, musterGegListe, blaupausePositionMod, noetigesBaumaterial);
        this.lagerNahrung = lagerNahrung;
    }

    // KOPIEREN //
    public GebaudeLager(GebaudeLager gebaudeLager) {
        super(gebaudeLager);
        this.lagerNahrung = gebaudeLager.lagerNahrung;
    }

    // INITIALISIEREN //
    private void init_gebaudeLager(Level level, Dorf dorf, RenderArrayList renderArrayList,
                     List<Gebaude> gebaudeListe, Eigentuemer eigentuemer, Vector2 position) {
        for (Gegenstand g : getGegenstandListe()) {
            int marktpreis = dorf.gettDurchschnMarktpreis(g.gettId());
            if (marktpreis > 0) {
                g.setWert(marktpreis);
            }
            if (KonstantenBalancing.DEVELOPER_GAMEPLAY_GEBAUDE_STARTMENGEN) {
                g.mengeErhoehen(1000, false);
            }
        }
        init_gebaude(level, dorf, renderArrayList, gebaudeListe, eigentuemer, position, true);
        updatePreisePause = new PauseDelta(KonstantenBalancing
                .PREISE_UPDATE_PAUSE_UND_NEUER_DATENSATZ_PAUSE_jahr
                * KonstantenBalancing.SEKUNDEN_PRO_JAHR,
                Enums.EnumPauseStart.JETZT_ZUFAELLIGE_RESTDAUER);
    }

    // CREATE //
    @Override
    public GebaudeLager create(Level level, Dorf dorf, RenderArrayList renderArrayList,
                               List<Gebaude> gebaudeListe, Eigentuemer eigentuemer,
                               Vector2 position) {
        GebaudeLager gebaudeLager = new GebaudeLager(this);
        gebaudeLager.init_gebaudeLager(level, dorf, renderArrayList, gebaudeListe, eigentuemer,
                position);
        return gebaudeLager;
    }

    /*// KOPIEREN //
    @Override
    public GebaudeLager create(Level level, Dorf dorf, RenderArrayList renderArrayList,
                               List<Gebaude> gebaudeListe, Eigentuemer eigentuemer,
                               Vector2 position) {
        GebaudeLager neuesGebaudeLager = new GebaudeLager(gettId(),
                getEnumErforscht() == Enums.EnumErforscht.JA, getMusterGegListe(),
                lagerNahrung, getFundamentGroesse(), getNoetigesBaumaterial());
        if (lagerNahrung) { //TODO In IdsGebaude verschieben
            neuesGebaudeLager.getMusterGegListe().addGegenstandPruefeReihenfolgeMengeMaximalUndWert(IdsGegenstaende.NAH_BEEREN_ID, 500, 1000); //TODO Konstanten
        } else {
            neuesGebaudeLager.getMusterGegListe().addGegenstandPruefeReihenfolgeMengeMaximalUndWert(IdsGegenstaende.MAT_HOLZ_ID, 10000, 1000);
            neuesGebaudeLager.getMusterGegListe().addGegenstandPruefeReihenfolgeMengeMaximalUndWert(IdsGegenstaende.MAT_PROD_WERKZEUG_EINFACH_ID, 200, 1000);
        }
        neuesGebaudeLager.init_Gebaude(level, dorf, renderArrayList, gebaudeListe, eigentuemer,
                position, true);
        return neuesGebaudeLager;
    }*/

    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public void update (float delta) {
        updatePreise(delta);
        super.update(delta);
    }

    private void updatePreise(float delta) {
        if (!updatePreisePause.issVorbei(delta, true)) {
            return;
        }
        for (Gegenstand g : getGegenstandListe()) {
            int eingang = Math.max(1, getWirtschaftsdatenManager().gettSummeEingang(g.gettId(),
                    KonstantenBalancing.ZIVIL_OBJEKT_BETRACHTE_PERIODEN));
            int ausgang = Math.max(1, getWirtschaftsdatenManager().gettSummeAusgang(g.gettId(),
                    KonstantenBalancing.ZIVIL_OBJEKT_BETRACHTE_PERIODEN));

            float eingangProzentVonAusgang = Math.min(
                    KonstantenBalancing.PREISE_VERAENDERUNG_MAX_PROZENT_jahr,
                    Math.max(
                            KonstantenBalancing.PREISE_VERAENDERUNG_MIN_PROZENT_jahr,
                            ausgang / eingang
                                    - KonstantenBalancing.PREISE_STILLSTAND_PROZENT_ABZUG_jahr));

            g.setWert(Math.max(1, (int) (g.getWert() * eingangProzentVonAusgang)));
        }
        getWirtschaftsdatenManager().neueDatenmenge();
    }

    public int einkaufspreis(int id) { //Zu welchem Preis von den Herstellern gekauft wird //TODO Verwenden??
        return (int) (getGegenstandListe().getDurchId(id).getWert() * 0.9f); //TODO Konstante
    }

    // EINFACHE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public boolean isLagerNahrung() {
        return lagerNahrung;
    }
}