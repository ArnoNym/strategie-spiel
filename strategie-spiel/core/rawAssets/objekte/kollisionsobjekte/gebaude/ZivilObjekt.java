package de.arnonym.strategiespiel.spielSchirm.objekte.kollisionsobjekte.gebaude;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;
import java.util.List;

import de.arnonym.strategiespiel.spielSchirm.Level;
import de.arnonym.strategiespiel.spielSchirm.objekte.Eigentuemer;
import de.arnonym.strategiespiel.spielSchirm.objekte.WirtschaftObjekt;
import de.arnonym.strategiespiel.spielSchirm.objekte.dorfbewohnerAi.Dorfbewohner;
import de.arnonym.strategiespiel.spielSchirm.objekte.kollisionsobjekte.Eigentum;
import de.arnonym.strategiespiel.spielSchirm.objekte.kollisionsobjekte.StatischesObjekt;
import de.arnonym.strategiespiel.spielSchirm.ui.StatischesSymbol;
import de.arnonym.strategiespiel.spielSchirm.worldMap.RenderArrayList;
import de.arnonym.strategiespiel.spielSchirm.worldMap.Zelle;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.Geld;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.GegListe;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.MusterGeg;
import de.arnonym.strategiespiel.framework.werkzeuge.collections.hashMaps.IntEintragHashMap;
import de.arnonym.strategiespiel.spielSchirm.zahlen.wirtschaftsdaten.WirtschaftsdatenManager;
import de.arnonym.strategiespiel.framework.werkzeuge.Assets;
import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenBalancing;
import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenOptik;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.GeometryUtils;

public abstract class ZivilObjekt extends StatischesObjekt implements Eigentum, WirtschaftObjekt {
    private Eigentuemer eigentuemer;
    private Geld geld;
    private WirtschaftsdatenManager wirtschaftsdatenManager = null;
    private List<Zelle> aufZellenListe = new ArrayList<>();

    // BESCHREIBEN //
    public ZivilObjekt(int id, GegListe<MusterGeg> musterGegListe_nl,
                       Vector2 blaupausePositionsMod) {
        super(id, musterGegListe_nl, blaupausePositionsMod, true, -1);
    }

    // KOPIEREN //
    protected ZivilObjekt(ZivilObjekt zivilObjekt) {
        super(zivilObjekt);
    }

    // INITIALISIEREN //
    protected void init_zivilObjekt(Level level, RenderArrayList renderArrayList,
                                    Eigentuemer eigentuemer, Vector2 position,
                                    boolean erstelleWirtschaftsdatenmanager) {
        super.init_statischesObjekt(level, renderArrayList, true, position);
        this.eigentuemer = eigentuemer;

        this.aufZellenListe = level.getWorldMap().gettBetroffeneZellenListe(position,
                getFundamentGroesse());
        for (Zelle z : aufZellenListe) {
            z.settStatischesObjekt(this);
            z.settBesitzer(this);
        }

        if (KonstantenBalancing.DEVELOPER_GAMEPLAY_ALLE_AUSSER_BAUGRUBE_EIGENES_GELD
                && !(this instanceof Baugrube)) {
            geld = new Geld(10000);
        } else {
            geld = eigentuemer.getGeld();
        }

        if (erstelleWirtschaftsdatenmanager) {
            wirtschaftsdatenManager = new WirtschaftsdatenManager(geld, getGegenstandListe());
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    protected void loescheAusZellen() {
        Vector2 koordinatenPositionInIsoUntereEckeDesHausesIso = GeometryUtils.applyBlaupausenMod(
                getPosition(), getFundamentGroesse());
        Vector2 koordinatenPositionInIsoUntereEckeDesHausesOrto = GeometryUtils.isoToOrto(
                koordinatenPositionInIsoUntereEckeDesHausesIso);
        Vector2 tilesInIsoUntereEckeDesHauses = new Vector2(
                koordinatenPositionInIsoUntereEckeDesHausesOrto.x
                        / KonstantenOptik.TILE_ORTO_WIDTH_AND_HEIGHT - 1,
                koordinatenPositionInIsoUntereEckeDesHausesOrto.y
                        / KonstantenOptik.TILE_ORTO_WIDTH_AND_HEIGHT);
        for (int j = 0; j > (int) -getFundamentGroesse().x; j--) {
            for (int i = 0; i < getFundamentGroesse().y; i++) {
                getLevel().getWorldMap().gett(
                        (int)tilesInIsoUntereEckeDesHauses.x + j,
                        (int)tilesInIsoUntereEckeDesHauses.y + i)
                        .settStatischesObjekt(null);
            }
        }
    }

    // UPDATE UND RENDER //////////////////////////////////////////////////////////////////////////

    @Override
    public void update(float delta) {
        if (wirtschaftsdatenManager != null) { //Wird ggf nicht initialisiert (zB GebHaus) //TODO etwas unschoen gff anders loesen?
            wirtschaftsdatenManager.update(delta);
        }
    }

    @Override
    public void renderOverride(SpriteBatch spriteBatch, List<StatischesSymbol> symbolListe) {
        super.renderOverride(spriteBatch, symbolListe);
        if (isAusgewaehlt()) {
            getLevel().getZellenMarkierungRenderer().addPositionen(gettBesitzListe(), Assets.instance
                    .tiledMapAssets.durchsichtigWeiss, false);
        }
    }

    // LIEFERN ////////////////////////////////////////////////////////////////////////////////////

    public void bezahleLieferanten(Dorfbewohner lieferant, double laufzeit,
                                   IntEintragHashMap geliefertHashMap) {
        int sollteKostenLieferant = (int) (lieferant.einkommen() * laufzeit);
        wirtschaftsdatenManager.updateDurchschnKosten(sollteKostenLieferant, geliefertHashMap);
        int tatsaechlichKostenLieferant = Math.min(geld.getMengeVerfuegbar(), sollteKostenLieferant);
        this.geld.mengeVerringern(tatsaechlichKostenLieferant, false);
        lieferant.getGeld().mengeErhoehen(tatsaechlichKostenLieferant, false);
    }

    // KOMPLEXE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public void settEigentuemer(Eigentuemer eigentuemer) {
        this.eigentuemer = eigentuemer;
        eigentuemer.getEigentumListe().add(this);
        this.geld = eigentuemer.getGeld();
    }

    public List<Zelle> gettBesitzListe() { //Wird in GebBeruf ueberschrieben
        return aufZellenListe;
    }

    // EINFACHE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public Geld getGeld() {
        return geld;
    }

    public WirtschaftsdatenManager getWirtschaftsdatenManager() {
        return wirtschaftsdatenManager;
    }

    @Override
    public Eigentuemer gettEigentuemer() {
        return eigentuemer;
    }

    public List<Zelle> getAufZellenListe() {
        return aufZellenListe;
    }
}
