package de.arnonym.strategiespiel.spielSchirm.objekte.kollisionsobjekte.gebaude;

import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;
import java.util.List;

import de.arnonym.strategiespiel.spielSchirm.Dorf;
import de.arnonym.strategiespiel.spielSchirm.Level;
import de.arnonym.strategiespiel.spielSchirm.objekte.Eigentuemer;
import de.arnonym.strategiespiel.spielSchirm.objekte.dorfbewohnerAi.Dorfbewohner;
import de.arnonym.strategiespiel.spielSchirm.objekte.dorfbewohnerAi.Familie;
import de.arnonym.strategiespiel.spielSchirm.worldMap.RenderArrayList;
import de.arnonym.strategiespiel.framework.werkzeuge.Enums;
import de.arnonym.strategiespiel.framework.werkzeuge.PauseDelta;
import de.arnonym.strategiespiel.spielSchirm.balancing.Konstanten;

/**
 * Created by LeonPB on 12.04.2018.
 */

public class GebaudeHaus extends Gebaude {
    private int maximaleBewohner;
    private int wertWohnen; //Was ein mieter erhaelt (und mit seinem Nutzen Exponenten in Nutzen umrechnet)

    private int preisMiete; //Was ein Mieter zahlen muss
    private PauseDelta pauseMiete;
    private List<Familie> familienBewohnerListe = new ArrayList<>();
    private List<Dorfbewohner> bewohnerListe = new ArrayList<>(); //Damit in Waisenheimen etc nicht nur eine Familie leben kann

    // BESCHREIBUNG //
    public GebaudeHaus(int id, boolean erforscht, Vector2 blaupausePositionMod,
                       int[][] noetigesBaumaterial, int wertWohnen, int maximaleBewohner) {
        super(id, erforscht, null, blaupausePositionMod, noetigesBaumaterial);
        this.wertWohnen = wertWohnen;
        this.maximaleBewohner = maximaleBewohner;
    }

    // KOPIEREN //
    private GebaudeHaus(GebaudeHaus gebaudeHaus) {
        super(gebaudeHaus);
        this.wertWohnen = gebaudeHaus.wertWohnen;
        this.maximaleBewohner = gebaudeHaus.maximaleBewohner;
    }

    // INITIALISIEREN //
    private void init_gebaudeHaus(Level level, Dorf dorf, RenderArrayList renderArrayList,
                      List<Gebaude> gebaudeListe, Eigentuemer eigentuemer, Vector2 position) {
        init_gebaude(level, dorf, renderArrayList, gebaudeListe, eigentuemer, position, false);
        this.preisMiete = this.wertWohnen;
        this.pauseMiete = new PauseDelta(2, Enums.EnumPauseStart.JETZT_ZUFAELLIGE_RESTDAUER);
    }

    // CREATE //
    @Override
    protected GebaudeHaus create(Level level, Dorf dorf, RenderArrayList renderArrayList,
                                 List<Gebaude> gebaudeListe, Eigentuemer eigentuemer,
                                 Vector2 position) {
        GebaudeHaus gebaudeHaus = new GebaudeHaus(this);
        gebaudeHaus.init_gebaudeHaus(level, dorf, renderArrayList, gebaudeListe, eigentuemer,
                position);
        return gebaudeHaus;
        /*GebaudeHaus neuesGebaudeHaus = new GebaudeHaus(gettId(),
                getEnumErforscht() == Enums.EnumErforscht.JA, getFundamentGroesse(),
                getNoetigesBaumaterial(), wertWohnen, maximaleBewohner);
        neuesGebaudeHaus.init_Gebaude(level, dorf, renderArrayList, gebaudeListe, eigentuemer,
                position, false);
        neuesGebaudeHaus.preisMiete = wertWohnen;
        neuesGebaudeHaus.pauseMiete = new PauseDelta(2, true, true); //TODO Konstanten
        return neuesGebaudeHaus;*/
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public void update (float delta) {
        super.update(delta);
        if (pauseMiete.issVorbei(delta, true)) {
            int anzahlMieter = bewohnerListe.size();
            for (Dorfbewohner d : bewohnerListe) {
                int gezahlteMiete = Math.min(preisMiete / anzahlMieter, d.getGeld().getMengeVerfuegbar());
                d.getGeld().mengeVerringern(gezahlteMiete, false); //TODO Mieter kuendigen wenn Miete nicht gezahlt wird und verhindenr, dass sie direkt wieder einziehen. Wie sollen die DBs das Geld fuer die Miete sparen??
                getGeld().mengeErhoehen(gezahlteMiete, false);
            }

            if (getDorf().gettAnzahlObdachlose() <= 0) { //Keine Obdachlosen
                if (!bewohnerListe.isEmpty()) { //Hat Mieter
                    preisMiete = (int) (preisMiete * (1 + 0.001f)); //TODO Konstante
                }
            } else { //Obdachlose
                if (bewohnerListe.isEmpty()) { //Hat keine Mieter
                    preisMiete = (int) (preisMiete * (1 - 0.05f)); //TODO Konstanten
                } else { //Hat Mieter
                    preisMiete = (int) (preisMiete * (1 + 0.01f)); //TODO Konstanten
                }
            }
        }
    }

    // WERTE FUER DORFBEWOHNER ////////////////////////////////////////////////////////////////////

    public int bruttoNutzen(float familieNutzenWohnenExponent) {
        return bruttoNutzen(familieNutzenWohnenExponent, bewohnerListe.size());
    }
    public int bruttoNutzen(float familieNutzenWohnenExponent, int anzahlBewohner) {
        float bruttoNutzenFloat = (float) Math.pow(wertWohnen / Konstanten.NACHKOMMASTELLEN, familieNutzenWohnenExponent);
        if (anzahlBewohner > maximaleBewohner) {
            bruttoNutzenFloat *= maximaleBewohner/anzahlBewohner;
        }
        return (int) bruttoNutzenFloat;
    }

    public float produktivitaet() {
        float produktivitaet = wertWohnen / Konstanten.NACHKOMMASTELLEN / 10; //TODO Konstanten
        int anzahlBewohner = bewohnerListe.size();
        if (anzahlBewohner > maximaleBewohner) {
            produktivitaet *= maximaleBewohner/anzahlBewohner;
        }
        //gettProduktivitaet *= (1 - 0.05 * anzahlKinder); //TODO Konstanten //TODO Soll ich das einfuegen?

        return Math.min(produktivitaet, 1);
    }

    public float gesundheit() {
        return gesundheit(bewohnerListe.size());
    }
    public float gesundheit(int anzahlBewohner) {
        float produktivitaet = wertWohnen / Konstanten.NACHKOMMASTELLEN / 10; //TODO Konstanten
        if (anzahlBewohner > maximaleBewohner) {
            produktivitaet *= maximaleBewohner/anzahlBewohner;
        }
        return Math.min(produktivitaet, 1);
    }

    // EINZIEHEN UND AUSZIEHEN/////////////////////////////////////////////////////////////////////

    public void einziehen(Familie familie) {
        familienBewohnerListe.add(familie);
        bewohnerListe.addAll(familie.getFamilienmitglieder());
    }

    public void ausziehen(Familie familie) {
        familienBewohnerListe.remove(familie);
        bewohnerListe.removeAll(familie.getFamilienmitglieder());
    }

    // SONSTIGES //////////////////////////////////////////////////////////////////////////////////

    public boolean verfuegbar() {
        return bewohnerListe.isEmpty();
    }

    // EINFACHE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public int getPreisMiete() {
        return preisMiete;
    }

    public int getWertWohnen() {
        return wertWohnen;
    }

    public List<Familie> getBewohnerFamilienListe() {
        return familienBewohnerListe;
    }
}