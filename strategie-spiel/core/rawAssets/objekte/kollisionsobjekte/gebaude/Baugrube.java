package de.arnonym.strategiespiel.spielSchirm.objekte.kollisionsobjekte.gebaude;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

import java.util.List;
import java.util.Map;

import de.arnonym.strategiespiel.spielSchirm.Dorf;
import de.arnonym.strategiespiel.spielSchirm.Level;
import de.arnonym.strategiespiel.spielSchirm.objekte.Eigentuemer;
import de.arnonym.strategiespiel.spielSchirm.objekte.dorfbewohnerAi.Dorfbewohner;
import de.arnonym.strategiespiel.spielSchirm.ui.StatischesSymbol;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.Gegenstand;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.GegenstandListe;
import de.arnonym.strategiespiel.spielSchirm.worldMap.RenderArrayList;
import de.arnonym.strategiespiel.framework.werkzeuge.Enums;
import de.arnonym.strategiespiel.spielSchirm.balancing.Konstanten;
import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenBalancing;
import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenOptik;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.MathUtils;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.TextureUtils;

public class Baugrube extends ZivilObjekt {
    private Dorf dorf;

    private Gebaude gebaude;
    private transient TextureRegion textureRegion;

    private int noetigeMaterialPlatzMenge;

    public Baugrube(Level level, Dorf dorf, RenderArrayList renderArrayList,
                    Eigentuemer eigentuemer, Gebaude gebaude) {
        super(-1, new GegenstandListe(Integer.MAX_VALUE), gebaude.getFundamentGroesse());
        this.gebaude = gebaude;
        noetigeMaterialPlatzMenge = 0;
        for (Map.Entry<Integer, Integer> entry : gebaude.getNoetigesBaumaterial().entrySet()) {
            int key = entry.getKey();
            int value = entry.getValue();

            Gegenstand g = getGegenstandListe().addGegenstand(key, 0, 0);
            g.setMengeMaximal(value);

            noetigeMaterialPlatzMenge += MathUtils.myIntMult(value, getGegenstandListe()
                    .getDurchId(key).gettPlatzVerbrauch());
        }
        init_zivilObjekt(level, renderArrayList, eigentuemer, gebaude.getPosition(), true);
        this.dorf = dorf;

        settArbeitszeit(noetigeMaterialPlatzMenge / Konstanten.NACHKOMMASTELLEN
                * KonstantenBalancing.BAUEN_PRO_MENGENGEWICHT_DAUER);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public void update(float delta) {

    }

    @Override
    public void renderOverride(SpriteBatch spriteBatch, List<StatischesSymbol> symbolListe) {
        super.renderOverride(spriteBatch, symbolListe);
        TextureUtils.zeichneTexturRegion(spriteBatch, textureRegion,  getPosition(), false);
    }

    @Override
    public void ladeTextureRegion_setBounds() {
        textureRegion = KonstantenOptik.baugrubeTextureRegionAuswaehlen(
                gebaude.getFundamentGroesse());
        settBounds(getPosition(), textureRegion.getRegionWidth(), textureRegion.getRegionHeight());
    }

    @Override
    protected void loescheAusSpeziellenListen() {
        dorf.getBaugrubenListe().remove(this);
    }

    // ARBEITSZEIT ////////////////////////////////////////////////////////////////////////////////

    @Override
    protected float gettArbeitszeitPlanen_or(Dorfbewohner dorfbewohner) {
        float maxErledigtProzent = getGegenstandListe().gettPlatzMalMengen(true, false, false)
                / (float) noetigeMaterialPlatzMenge;
        float materialMaxArbeitszeit = (maxErledigtProzent - gettErledigtUndGeplantProzent())
                * getArbeitszeit();

        float bezahlungArbeitszeitMax = (float) getGeld().getMengeVerfuegbar()
                / dorfbewohner.einkommen(Enums.EnumZeit.REAL_SEKUNDEN);

        return MathUtils.min(getRestZuPlanenArbeitszeit(), KonstantenBalancing.BAUEN_AM_STUECK_MAX,
                materialMaxArbeitszeit, bezahlungArbeitszeitMax);
    }

    @Override
    protected void arbeitErledigt() {
        loesche();
        gebaude.create(getLevel(), dorf, getRenderArrayList(), dorf.getGebaudeListe(), dorf,
                new Vector2(getPosition()));
    }

    // POSITIONEN /////////////////////////////////////////////////////////////////////////////////

    @Override
    protected Vector2 positionZuPositionMitte(Vector2 position) {
        Vector2 positionBlaupauseMod = gebaude.getFundamentGroesse();
        return new Vector2( //EcsDokumentation-002
                position.x + KonstantenOptik.TILE_ORTO_UND_ISOXACHSE_DIAGONAL / 2
                        * (positionBlaupauseMod.x + positionBlaupauseMod.y) / 2,
                position.y + KonstantenOptik.TILE_ISOYACHSE_DIAGONAL / 2
                        * (positionBlaupauseMod.x + positionBlaupauseMod.y) / 2
        );
    }

    @Override
    protected Vector2 positionMitteZuPosition(Vector2 positionMitte) {
        Vector2 positionBlaupauseMod = gebaude.getFundamentGroesse();
        return new Vector2( //EcsDokumentation-002
                positionMitte.x - KonstantenOptik.TILE_ORTO_UND_ISOXACHSE_DIAGONAL / 2
                        * (positionBlaupauseMod.x + positionBlaupauseMod.y) / 2,
                positionMitte.y - KonstantenOptik.TILE_ISOYACHSE_DIAGONAL / 2
                        * (positionBlaupauseMod.x + positionBlaupauseMod.y) / 2
        );
    }

    // EINFACHE GETTER UND SETTER /////////////////////////////////////////////////////////////////
}