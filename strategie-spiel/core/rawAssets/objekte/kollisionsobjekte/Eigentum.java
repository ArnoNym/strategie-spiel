package de.arnonym.strategiespiel.spielSchirm.objekte.kollisionsobjekte;

import de.arnonym.strategiespiel.spielSchirm.objekte.Eigentuemer;

public interface Eigentum {
    /* Beides mit doppel t weil es teilweise nicht nur als einfacher Getter verwendet wird */
    Eigentuemer gettEigentuemer();
    void settEigentuemer(Eigentuemer eigentuemer);
    /* {
        this.besitzer = besitzer;
        besitzer.getBesitzListe.addFrei(this);
    } */
}
