package de.arnonym.strategiespiel.spielSchirm.objekte.kollisionsobjekte;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

import java.util.List;

import de.arnonym.strategiespiel.spielSchirm.Level;
import de.arnonym.strategiespiel.spielSchirm.objekte.WorldObjekt;
import de.arnonym.strategiespiel.spielSchirm.objekte.dorfbewohnerAi.Dorfbewohner;
import de.arnonym.strategiespiel.spielSchirm.ui.StatischesSymbol;
import de.arnonym.strategiespiel.spielSchirm.worldMap.RenderArrayList;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.GegListe;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.MusterGeg;
import de.arnonym.strategiespiel.spielSchirm.balancing.IdsSymbole;
import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenFehlerAusgleich;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.MathUtils;

/**
 * Created by LeonPB on 07.04.2018.
 */

public abstract class StatischesObjekt extends WorldObjekt {
    private final int id;
    private final Vector2 fundamentGroesse;
    private final boolean kollisionBauen;
    private final float kollisionLaufen; // -1 = unpassierbar, ansonsten groesser = schwieriger

    private boolean keinPfad = false; // Mindestens ein DB kann StatOb nicht erreichen

    private float arbeitszeit;
    private float restZuPlanenArbeitszeit;
    private float restZuErledigenArbeitszeit;

    // BESCHREIBEN //
    public StatischesObjekt(int id, GegListe<MusterGeg> musterGegListe_nl,
                            Vector2 fundamentGroesse, boolean kollisionBauen,
                            float kollisionLaufen) {
        super(-1, -1, musterGegListe_nl);
        this.id = id;
        this.fundamentGroesse = fundamentGroesse;
        this.kollisionBauen = kollisionBauen;
        this.kollisionLaufen = kollisionLaufen;
    }

    // KOPIEREN //
    protected StatischesObjekt(StatischesObjekt statischesObjekt) {
        super(statischesObjekt);
        this.id = statischesObjekt.id;
        this.fundamentGroesse = statischesObjekt.fundamentGroesse;
        this.kollisionBauen = statischesObjekt.kollisionBauen;
        this.kollisionLaufen = statischesObjekt.kollisionLaufen;
    }

    // INITIALISIEREN //
    protected void init_statischesObjekt(Level level, RenderArrayList renderArrayList,
                                         boolean pruefeRenderIndex, Vector2 position) {
        init_worldObjekt(level, renderArrayList, pruefeRenderIndex, position);
        ladeTextureRegion_setBounds();
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    protected void renderOverride(SpriteBatch spriteBatch, List<StatischesSymbol> symbolListe) {
        if (keinPfad) {
            symbolListe.add(IdsSymbole.SYMBOL_KEIN_PFAD);
        }
    }

    @Override
    public void loesche() {
        super.loesche();

        // Loesche aus Zellen
        loescheAusZellen();
    }

    protected abstract void loescheAusZellen();

    // LADEN //////////////////////////////////////////////////////////////////////////////////////

    public abstract void ladeTextureRegion_setBounds();

    public void lade() {
        setAusgewaehlt(false);
        setOnKlickFenster(null);
        ladeTextureRegion_setBounds();
    }

    // ARBEITSZEIT ////////////////////////////////////////////////////////////////////////////////

    public float gettArbeitszeitPlanen(Dorfbewohner dorfbewohner) {
        if (restZuPlanenArbeitszeit == 0) {
            return 0;
        }

        float arbeitszeit = gettArbeitszeitPlanen_or(dorfbewohner);

        if (arbeitszeit <= 0) {
            return 0;
        }
        this.restZuPlanenArbeitszeit -= arbeitszeit;
        return arbeitszeit;
    }

    protected abstract float gettArbeitszeitPlanen_or(Dorfbewohner dorfbewohner);

    public void arbeitszeitPlanenRueckgaenig(float arbeitszeit) {
        restZuPlanenArbeitszeit += arbeitszeit;
    }

    public void addArbeitszeitErledigt(float arbeitszeit) {
        restZuErledigenArbeitszeit -= arbeitszeit;

        if (restZuErledigenArbeitszeit < 0) {
            throw new IllegalStateException("restZuErledigenArbeitszeit: "+ restZuErledigenArbeitszeit);
        } else if (MathUtils.runden(restZuErledigenArbeitszeit,
                KonstantenFehlerAusgleich.SEKUNDEN_RUNDEN_NACHKOMMASTELLEN) == 0) {
            loesche();
            arbeitErledigt();
        }
    }

    protected abstract void arbeitErledigt();

    public float gettErledigtProzent() {
        return 1 - restZuErledigenArbeitszeit / arbeitszeit;
    }

    public float gettErledigtUndGeplantProzent() {
        return 1 - restZuPlanenArbeitszeit / arbeitszeit;
    }

    protected void settArbeitszeit(float arbeitszeit) {
        this.arbeitszeit = arbeitszeit;
        this.restZuErledigenArbeitszeit = arbeitszeit;
        this.restZuPlanenArbeitszeit = arbeitszeit;
    }

    protected float getArbeitszeit() {
        return arbeitszeit;
    }

    protected float getRestZuPlanenArbeitszeit() {
        return restZuPlanenArbeitszeit;
    }

    protected float getRestZuErledigenArbeitszeit() {
        return restZuErledigenArbeitszeit;
    }

    // KOMPLEXE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public boolean issKollisionLaufen() {
        return kollisionLaufen == -1;
    }

    // PROTECTED GETTER UND SETTER ////////////////////////////////////////////////////////////////

    // EINFACHE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public int getId() {
        return id;
    }

    public Vector2 getFundamentGroesse() {
        return fundamentGroesse;
    }

    public boolean isKollisionBauen() {
        return kollisionBauen;
    }

    public float getKollisionLaufen() {
        return kollisionLaufen;
    }

    public void setKeinPfad(boolean keinPfad) {
        this.keinPfad = keinPfad;
    }
}
