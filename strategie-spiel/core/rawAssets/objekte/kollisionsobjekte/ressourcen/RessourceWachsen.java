package de.arnonym.strategiespiel.spielSchirm.objekte.kollisionsobjekte.ressourcen;

import com.badlogic.gdx.graphics.g2d.TextureRegion;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import de.arnonym.strategiespiel.spielSchirm.Level;
import de.arnonym.strategiespiel.spielSchirm.worldMap.RenderArrayList;
import de.arnonym.strategiespiel.spielSchirm.worldMap.Zelle;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.GegListe;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.MusterGeg;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.Gegenstand;
import de.arnonym.strategiespiel.framework.werkzeuge.Enums;
import de.arnonym.strategiespiel.framework.werkzeuge.PauseDelta;
import de.arnonym.strategiespiel.framework.werkzeuge.RessourcenAssetContainer;
import de.arnonym.strategiespiel.spielSchirm.balancing.IdsGegenstaende;
import de.arnonym.strategiespiel.spielSchirm.balancing.IdsRessourcenWachsen;
import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenBalancing;

public class RessourceWachsen extends Ressource {
    private RessourceWachsen_core core; // Kein Getter!!!
    // Alt
    private PauseDelta vermehrenPause;
    private float fruechteWachstumProzent_sekunde = 0;
    private PauseDelta fruechteWachstumPause;
    // Andere
    private PauseDelta naechsteStufePause;

    private float pflege = 1;
    private boolean tod = false;

    // BESCHREIBEN //
    protected RessourceWachsen(RessourceWachsen_core core, Level level,
                               List<Ressource> ressourceListe,
                               RenderArrayList renderArrayList, boolean pruefenRenderIndex,
                               Zelle zelle, Enums.EnumAbbauen enumAbbauen,
                               boolean zufaelligAlt) {
        super(core.getId(), core.getMusterGegListe(),
                core.isKollisionBauen(), core.getKollisionLaufen());
        this.core = core;
        for (Gegenstand g : getGegenstandListe()) {
            if (g.gettId() != IdsGegenstaende.MAT_HOLZ_ID) {
                g.mengeErhoehen((int) (g.getMengeMaximal()
                        * ThreadLocalRandom.current().nextFloat()), false);
            }
        }
        init_Ressource(level, ressourceListe, renderArrayList, pruefenRenderIndex, zelle,
                enumAbbauen);
        pausenErstellen(zufaelligAlt, zelle.getFruchtbarkeit());
    }

    // NEUE STUFE //
    protected void neuerCore(RessourceWachsen_core neuerCore) {
        this.core = neuerCore;
        erweitereGegenstandListe(neuerCore.getMusterGegListe());
        pausenErstellen(false, getZelle().getFruchtbarkeit());
    }

    private void erweitereGegenstandListe(GegListe<MusterGeg> erweitereUmMusterGegListe) {
        getGegenstandListe().settMaxPlatzUndResetPlatz(erweitereUmMusterGegListe.getMaxPlatz());
        for (MusterGeg nachfolgerResMusterGeg : erweitereUmMusterGegListe) {
            Gegenstand thisGeg = getGegenstandListe()
                    .getDurchId(nachfolgerResMusterGeg.gettId());
            if (thisGeg == null) {
                getGegenstandListe().addGegenstand(nachfolgerResMusterGeg.gettId(),
                        nachfolgerResMusterGeg.mengeVerfuegbar,
                        nachfolgerResMusterGeg.mengeMax,
                        nachfolgerResMusterGeg.wert);
            } else {
                thisGeg.mengeErhoehen(nachfolgerResMusterGeg.mengeVerfuegbar
                        - thisGeg.getMengeVerfuegbar(), true);
                thisGeg.setMengeMaximal(nachfolgerResMusterGeg.mengeMax);
            }
        }
        mengenVeraenderung();
    }

    private void pausenErstellen(boolean zufaelligAlt, float zelleFruchtbarkeit) {
        Enums.EnumPauseStart enumPauseStart;
        if (zufaelligAlt) enumPauseStart = Enums.EnumPauseStart.JETZT_ZUFAELLIGE_RESTDAUER;
        else enumPauseStart = Enums.EnumPauseStart.JETZT;
        if (core.issAusgewachsen()) {
            vermehrenPause = new PauseDelta(core.gettVermehrenPauseDauer(zelleFruchtbarkeit),
                    enumPauseStart);
            fruechteWachstumProzent_sekunde = core.gettFruchteWachstumProzent_sekunde();
            if (fruechteWachstumProzent_sekunde > 0) {
                fruechteWachstumPause = new PauseDelta(core.gettFruchteWachstumPauseDauer(
                        getGegenstandListe(), fruechteWachstumProzent_sekunde), enumPauseStart);
            }
            naechsteStufePause = null;
        } else {
            vermehrenPause = null;
            fruechteWachstumProzent_sekunde = 0;
            fruechteWachstumPause = null;
            naechsteStufePause = new PauseDelta(core.gettNaechsteStufePauseDauer(), enumPauseStart);
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public void update(float delta) {
        // Res ist tod und vergammelt langsam
        if (tod) {
            vergammeln(delta, 1);
            return;
        }

        float deltaPflege = delta * pflege;
        pflege = Math.max(pflege - KonstantenBalancing.RES_PFLEGE_ABBAUEN_PROZENT_sekunde * delta, 1);

        core.vermehren(this, deltaPflege);

        Gegenstand thisGegenstandHolz = getGegenstandListe().getDurchId(IdsGegenstaende.MAT_HOLZ_ID);

        // Res hat immer eine Chance zu strerben
        if (ThreadLocalRandom.current().nextFloat() < KonstantenBalancing
                .RESSOURCEN_STERBEN_WAHRSCHEINLICHKEIT_PRO_JAHR
                / KonstantenBalancing.SEKUNDEN_PRO_JAHR * delta) {
            tod = true; // Vor vergammeln
            vergammeln(delta, 1);
            return;
        }

        // Res ist tod wenn es eine HolzRes (zB Baum) ist und davon eine Menge entfern wurde
        if (core.isStamm() && thisGegenstandHolz.getMengeVerfuegbar()
                < thisGegenstandHolz.getMengeMaximal()) {
            tod = true;
            return;
        }

        core.fruechte(this, deltaPflege);
        core.naechsteStufe(this, deltaPflege);
    }

    // RENDER ETC /////////////////////////////////////////////////////////////////////////////////

    @Override
    protected RessourcenAssetContainer ermittleAssetContainer() {
        if (core.getEnumStufe() == null) {
            return null;
        }
        return IdsRessourcenWachsen.getAssetContainer(getId(), core.getEnumStufe());
    }

    @Override
    public void ladeTextureRegion_setBounds() {
        RessourcenAssetContainer assetContainer = ermittleAssetContainer();
        setAssetContainer(assetContainer);
        TextureRegion textureRegion = assetContainer.get(getGegenstandListe(), core.isStamm(), tod);
        setTextureRegion(textureRegion);
        settBounds(getPosition(), textureRegion.getRegionWidth(), textureRegion.getRegionHeight());
    }

    // ERNTEN /////////////////////////////////////////////////////////////////////////////////////

    public void entferneErnten() {
        List<Ressource> abzubauendeRessourcenListe = getLevel().getAbzubauendeRessourcenListe();
        if (getEnumAbbauen() == Enums.EnumAbbauen.NUR_ERNTEN && !issErntenMoeglich()
                && abzubauendeRessourcenListe.contains(this)) {
            abzubauendeRessourcenListe.remove(this);
            setEnumAbbauen(Enums.EnumAbbauen.NEIN);
        }
    }

    // PFLEGEN ////////////////////////////////////////////////////////////////////////////////////

    public void pflegen(float dauer, float produktivitaet) {
        pflege += KonstantenBalancing.RES_PFLEGE_AUFBAUEN_PROZENT_sekunde * dauer * produktivitaet;
    }

    // VERMEHREN //////////////////////////////////////////////////////////////////////////////////

    public void vermehrenDurchfuehren() {
        if (getZelle().isUmringtVonKollisionBauen()) {
            return;
        }
        List<Zelle> freieZellenListe = new ArrayList<>();
        for (Zelle z : getLevel().getWorldMap().gett(getPosition()).gettAngrenzendeZellen()) {
            if (z != null && !z.isKollisionBauen()) {
                freieZellenListe.add(z);
            }
        }
        if (freieZellenListe.isEmpty()) {
            return;
        }
        int randomIndex = (int) (ThreadLocalRandom.current().nextFloat() * freieZellenListe.size());
        Zelle betroffeneZelle = freieZellenListe.get(randomIndex);
        IdsRessourcenWachsen.get(core.getId(), Enums.EnumResNwStufe.JUNG).create(getLevel(),
                getLevel().getRessourcenListe(), getRenderArrayList(), true, betroffeneZelle,
                getEnumAbbauen(), false);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    // IMMITIERE CORE /////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////

    public boolean issErntenMoeglich() {
        return core.issErntenMoeglich(getGegenstandListe());
    }

    public void updateFruchtbarkeit(float zelleFruchtbarkeit) {
        core.updateFruchtbarkeit(this, zelleFruchtbarkeit);
    }

    public boolean issAusgewachsen() {
        return core.issAusgewachsen();
    }

    public boolean isFruechte() {
        return core.isFruechte();
    }

    public Enums.EnumResNwStufe getEnumStufe() {
        return core.getEnumStufe();
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    // PROTECTED SETTER UND GETTER ////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////

    protected float getPflege() {
        return pflege;
    }

    protected PauseDelta getVermehrenPause() {
        return vermehrenPause;
    }

    protected float getFruechteWachstumProzent_sekunde() {
        return fruechteWachstumProzent_sekunde;
    }

    protected PauseDelta getFruechteWachstumPause() {
        return fruechteWachstumPause;
    }

    protected PauseDelta getNaechsteStufePause() {
        return naechsteStufePause;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    // EINFACHE GETTER UND SETTER /////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////

    public boolean isTod() {
        return tod;
    }
}
