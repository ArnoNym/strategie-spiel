package de.arnonym.strategiespiel.spielSchirm.objekte.kollisionsobjekte.ressourcen;

import java.util.List;

import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.Gegenstand;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.GegenstandListe;
import de.arnonym.strategiespiel.framework.werkzeuge.Enums;
import de.arnonym.strategiespiel.framework.werkzeuge.PauseDelta;
import de.arnonym.strategiespiel.spielSchirm.balancing.IdsGegenstaende;
import de.arnonym.strategiespiel.spielSchirm.balancing.Konstanten;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.MathUtils;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.MyTimeUtils;

public class RessourceWachsen_coreAlt extends RessourceWachsen_core {
    private final float vermehrenDauer_jahr;
    private final float fruechteWachstumProzent_jahr;

    // BESCHREIBEN //
    public RessourceWachsen_coreAlt(int id, GegenstandListe kopierenGegenstandListe,
                                    boolean kollisionBauen, float kollisionLaufen,
                                    RessourceWachsen_core nachfolgerRessource,
                                    float vermehrenDauer_jahr,
                                    float fruechteWachstumProzent_jahr) {
        super(id, Enums.EnumResNwStufe.ALT, kopierenGegenstandListe, kollisionBauen,
                kollisionLaufen, nachfolgerRessource);
        this.vermehrenDauer_jahr = vermehrenDauer_jahr;
        this.fruechteWachstumProzent_jahr = fruechteWachstumProzent_jahr;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public float gettVermehrenPauseDauer(float zellenFruchtbarkeit) {
        return gettVermehrenDauer_sek(MyTimeUtils.perIngameYearToPerSec(vermehrenDauer_jahr,
                Enums.EnumValue.NUMBER), zellenFruchtbarkeit);
    }

    @Override
    public float gettFruchteWachstumProzent_sekunde() {
        return MyTimeUtils.perIngameYearToPerSec(fruechteWachstumProzent_jahr,
                Enums.EnumValue.PERCENT);
    }

    @Override
    public float gettFruchteWachstumPauseDauer(GegenstandListe gegenstandListe, float fruechteWachstumProzent_sekunde) {
        /* FruechteWachstumPause festlegen (sodass mindestens eine kleinste Einheit jedes
        Gegenstandes heranwaechst und nicht weggerundet wird) */
        int kleinsteMengeMax = Integer.MAX_VALUE;
        Gegenstand kleinsteMengeMaxGeg = null;
        for (Gegenstand g : gegenstandListe) {
            if (g.getMengeMaximal() < kleinsteMengeMax) {
                kleinsteMengeMax = g.getMengeMaximal();
                kleinsteMengeMaxGeg = g;
            }
        }
        if (kleinsteMengeMaxGeg == null) throw new IllegalStateException();
        float pauseDauer;
        if (kleinsteMengeMaxGeg.isTeilbar()) {
            pauseDauer = 1 / (kleinsteMengeMax * fruechteWachstumProzent_sekunde);
        } else {
            pauseDauer = Konstanten.NACHKOMMASTELLEN
                    / (kleinsteMengeMax *  fruechteWachstumProzent_sekunde);
        }
        return pauseDauer;
    }

    @Override
    public float gettNaechsteStufePauseDauer() {
        return 0;
    }

    // UPDATE /////////////////////////////////////////////////////////////////////////////////////

    @Override
    protected void vermehren(RessourceWachsen shell, float deltaPflege) {
        if (shell.getVermehrenPause().issVorbei(deltaPflege, true)) {
            List<RessourceWachsen> ressourcenVermehrenListe = shell.getLevel()
                    .getRessourcenVermehrenListe();
            if (ressourcenVermehrenListe.contains(shell)) throw new IllegalStateException();
            ressourcenVermehrenListe.add(shell);
        }
    }

    @Override
    protected void fruechte(RessourceWachsen shell, float deltaPflege) {
        PauseDelta fruechteWachstumPause = shell.getFruechteWachstumPause();
        if (isFruechte() && fruechteWachstumPause.issVorbei(deltaPflege, false)) {
            for (Gegenstand g : shell.getGegenstandListe()) {
                if (!isStamm() || g.gettId() != IdsGegenstaende.MAT_HOLZ_ID) {
                    int mengeErhoehenUm = MathUtils.min(g.getMengeBisVoll(),
                            (int) (g.getMengeMaximal() * shell.getFruechteWachstumProzent_sekunde()
                                    * fruechteWachstumPause.gettZeitSeitStartzeit()));
                    /*if (mengeErhoehenUm <= 0 && gettId() == IdsRessourcenWachsen.STRAUCH_BEEREN_ID) throw  new IllegalStateException(
                            "ResId: " + gettId() + ", stufe: " + getEnumStufe().name()
                                    + ", mengeErhoehenUm: " + mengeErhoehenUm + ", shell.getFruechteWachstumProzent_sekunde(): "
                                    + shell.getFruechteWachstumProzent_sekunde()
                                    + ", fruechteWachstumPause.gettZeitSeitStartzeit(): "
                                    + fruechteWachstumPause.gettZeitSeitStartzeit() + ", gegLite Platz: " + shell.getMusterGegListe().getMaxPlatz()); // TODO LOEschen*/
                    g.mengeErhoehen(mengeErhoehenUm, false);
                }
            }
            shell.mengenVeraenderung();
            fruechteWachstumPause.settStartzeitJetzt();
        }
    }

    @Override
    protected void naechsteStufe(RessourceWachsen shell, float deltaPflege) {

    }

    // FRUCHTBARKEIT //////////////////////////////////////////////////////////////////////////////

    @Override
    public void updateFruchtbarkeit(RessourceWachsen shell, float zelleFruchtbarkeit) {
        PauseDelta vermehrenPause = shell.getVermehrenPause();
        if (gettVermehrenDauer_sek(vermehrenPause.getDauer(),
                zelleFruchtbarkeit) <= 0) throw new IllegalStateException(); // TODO LOEschen
        vermehrenPause.settDauer(gettVermehrenDauer_sek(vermehrenPause.getDauer(),
                zelleFruchtbarkeit));
        // TODO Auch fruechte Wachstum prozent anpassen
    }

    // KOMPLEXE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    private float gettVermehrenDauer_sek(float alteVermehrenDauer_sek, float zelleFruchtbarkeit) {
        return alteVermehrenDauer_sek * (1 - zelleFruchtbarkeit);
    }

    @Override
    public boolean issErntenMoeglich(GegenstandListe gegenstandListe) {
        for (Gegenstand g : gegenstandListe) {
            if (g.gettId() != IdsGegenstaende.MAT_HOLZ_ID && g.getMengeVerfuegbar() > 0) {
                return true;
            }
        }
        return false;
    }
}