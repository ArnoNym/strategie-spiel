package de.arnonym.strategiespiel.spielSchirm.objekte.kollisionsobjekte.ressourcen;

import java.util.List;

import de.arnonym.strategiespiel.spielSchirm.Level;
import de.arnonym.strategiespiel.spielSchirm.worldMap.RenderArrayList;
import de.arnonym.strategiespiel.spielSchirm.worldMap.Zelle;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.GegListe;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.MusterGeg;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.GegenstandListe;
import de.arnonym.strategiespiel.framework.werkzeuge.Enums;
import de.arnonym.strategiespiel.spielSchirm.balancing.IdsGegenstaende;

public abstract class RessourceWachsen_core {
    private final int id;
    private final Enums.EnumResNwStufe enumStufe;
    private final GegListe<MusterGeg> musterGegListe;
    private final boolean kollisionBauen;
    private final float kollisionLaufen;
    private RessourceWachsen_core nachfolgerRessource; // Entweder naechste Stufe oder Nachkommen //TODO Finalize

    private final boolean stamm; /* Also Baeume etc die erst einen Stamm brauchen um Fruechte tragen zu
    koennen und die aufhoeren Freuchte zu tragen sobald dieser bechaedigt wird */
    private final boolean fruechte; /* Kann Fruechte tragen. Es lohnt sich also im ausgewachsenen
    Zustand zu pflegen */

    // BESCHREIBEN //
    public RessourceWachsen_core(int id, Enums.EnumResNwStufe enumStufe,
                                 GegListe<MusterGeg> musterGegListe,
                                 boolean kollisionBauen, float kollisionLaufen,
                                 RessourceWachsen_core nachfolgerRessource) {
        this.id = id;
        this.enumStufe = enumStufe;
        this.musterGegListe = musterGegListe;
        this.kollisionBauen = kollisionBauen;
        this.kollisionLaufen = kollisionLaufen;
        this.nachfolgerRessource = nachfolgerRessource;

        MusterGeg musterGegHolz = musterGegListe.gettDurchId(IdsGegenstaende.MAT_HOLZ_ID);
        this.stamm = musterGegHolz != null;
        int musterGegListeSize = musterGegListe.size();
        this.fruechte = musterGegListeSize > 1 || (musterGegHolz == null && musterGegListeSize > 0);
    }

    // CREATE NEW SHELL //
    public RessourceWachsen create(Level level, List<Ressource> ressourceListe,
                                   RenderArrayList renderArrayList, boolean pruefenRenderIndex,
                                   Zelle zelle, Enums.EnumAbbauen enumAbbauen,
                                   boolean zufaelligAlt) {
        return new RessourceWachsen(this, level, ressourceListe, renderArrayList,
                pruefenRenderIndex, zelle, enumAbbauen, zufaelligAlt);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////

    public abstract float gettVermehrenPauseDauer(float zellenFruchtbarkeit);

    public abstract float gettFruchteWachstumProzent_sekunde();

    public abstract float gettFruchteWachstumPauseDauer(GegenstandListe gegenstandListe,
                                                        float fruechteWachstumProzent_sekunde);

    public abstract float gettNaechsteStufePauseDauer();

    // UPDATE//////////////////////////////////////////////////////////////////////////////////////

    protected abstract void vermehren(RessourceWachsen shell, float deltaPflege);

    protected abstract void fruechte(RessourceWachsen shell, float deltaPflege);

    protected abstract void naechsteStufe(RessourceWachsen shell, float deltaPflege);

    // FRUCHTBARKEIT //////////////////////////////////////////////////////////////////////////////

    public abstract void updateFruchtbarkeit(RessourceWachsen shell, float zelleFruchtbarkeit);

    // KOMPLEXE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public abstract boolean issErntenMoeglich(GegenstandListe gegenstandListe);

    public boolean issAusgewachsen() {
        return enumStufe == Enums.EnumResNwStufe.ALT;
    }

    // EINFACHE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public Enums.EnumResNwStufe getEnumStufe() {
        return enumStufe;
    }

    public boolean isStamm() {
        return stamm;
    }

    public RessourceWachsen_core getNachfolgerRessource() {
        return nachfolgerRessource;
    }

    public int getId() {
        return id;
    }

    public GegListe<MusterGeg> getMusterGegListe() {
        return musterGegListe;
    }

    public boolean isKollisionBauen() {
        return kollisionBauen;
    }

    public float getKollisionLaufen() {
        return kollisionLaufen;
    }

    public void setNachfolgerRessource(RessourceWachsen_core nachfolgerRessource) {
        this.nachfolgerRessource = nachfolgerRessource;
    }

    public boolean isFruechte() {
        return fruechte;
    }
}
