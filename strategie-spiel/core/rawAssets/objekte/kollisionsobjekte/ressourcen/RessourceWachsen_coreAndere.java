package de.arnonym.strategiespiel.spielSchirm.objekte.kollisionsobjekte.ressourcen;

import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.GegenstandListe;
import de.arnonym.strategiespiel.framework.werkzeuge.Enums;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.MyTimeUtils;

public class RessourceWachsen_coreAndere extends RessourceWachsen_core {
    private final float naechsteStufeDauer_jahr;

    // BESCHREIBEN //
    public RessourceWachsen_coreAndere(int id, Enums.EnumResNwStufe enumStufe,
                                       GegenstandListe kopierenGegenstandListe,
                                       boolean kollisionBauen, float kollisionLaufen,
                                       RessourceWachsen_core nachfolgerRessource,
                                       float naechsteStufeDauer_jahr) {
        super(id, enumStufe, kopierenGegenstandListe, kollisionBauen, kollisionLaufen,
                nachfolgerRessource);
        this.naechsteStufeDauer_jahr = naechsteStufeDauer_jahr;
        if (enumStufe == Enums.EnumResNwStufe.ALT) {
            throw new IllegalArgumentException("Verwende 'RessourceWachsen_coreAusgewachsen'!");
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public float gettVermehrenPauseDauer(float zellenFruchtbarkeit) {
        return 0;
    }

    @Override
    public float gettFruchteWachstumProzent_sekunde() {
        return 0;
    }

    @Override
    public float gettFruchteWachstumPauseDauer(GegenstandListe gegenstandListe,
                                               float fruechteWachstumProzent_sekunde) {
        return 0;
    }

    public float gettNaechsteStufePauseDauer() {
        return MyTimeUtils.perIngameYearToPerSec(naechsteStufeDauer_jahr, Enums.EnumValue.NUMBER);
    }

    // UPDATE /////////////////////////////////////////////////////////////////////////////////////


    @Override
    protected void vermehren(RessourceWachsen shell, float deltaPflege) {

    }

    @Override
    protected void fruechte(RessourceWachsen shell, float deltaPflege) {

    }

    @Override
    protected void naechsteStufe(RessourceWachsen shell, float deltaPflege) {
        if (shell.getNaechsteStufePause().issVorbei(deltaPflege, false)) {
            shell.neuerCore(getNachfolgerRessource());
        }
    }

    // FRUCHTBARKEIT //////////////////////////////////////////////////////////////////////////////

    @Override
    public void updateFruchtbarkeit(RessourceWachsen shell, float zelleFruchtbarkeit) {
        // TODO Auf fruchtbarem Boden schneller wachsen lasse?? Eher nein, also leer lassen
    }

    // KOMPLEXE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    @Override
    public boolean issErntenMoeglich(GegenstandListe gegenstandListe) {
        return false;
    }

    // EINFACHE GETTER UND SETTER /////////////////////////////////////////////////////////////////
}
