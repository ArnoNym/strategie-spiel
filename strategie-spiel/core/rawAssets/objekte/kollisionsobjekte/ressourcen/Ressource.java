package de.arnonym.strategiespiel.spielSchirm.objekte.kollisionsobjekte.ressourcen;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

import java.util.List;

import de.arnonym.strategiespiel.spielSchirm.Level;
import de.arnonym.strategiespiel.spielSchirm.objekte.kollisionsobjekte.AbbaubaresObjekt;
import de.arnonym.strategiespiel.spielSchirm.ui.StatischesSymbol;
import de.arnonym.strategiespiel.spielSchirm.worldMap.Zelle;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.GegListe;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.MusterGeg;
import de.arnonym.strategiespiel.spielSchirm.worldMap.RenderArrayList;
import de.arnonym.strategiespiel.framework.werkzeuge.Enums;
import de.arnonym.strategiespiel.framework.werkzeuge.RessourcenAssetContainer;
import de.arnonym.strategiespiel.spielSchirm.balancing.IdsRessourcenWachsen;
import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenOptik;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.TextureUtils;

/**
 * Created by LeonPB on 07.04.2018.
 */

public abstract class Ressource extends AbbaubaresObjekt {
    private transient RessourcenAssetContainer assetContainer;
    private transient TextureRegion textureRegion;

    // BESCHREIBEN //
    public Ressource(int id, GegListe<MusterGeg> musterGegListe_nl, boolean kollisionBauen,
                     float kollisionLaufen) {
        super(id, musterGegListe_nl, new Vector2(1, 1), kollisionBauen, kollisionLaufen);
    }

    // KOPIEREN //
    protected Ressource(Ressource ressource) {
        super(ressource);
    }

    // INITIALISIEREN //
    protected void init_Ressource(Level level, List<Ressource> ressourceListe,
                                  RenderArrayList renderArrayList, boolean pruefenRenderIndex,
                                  Zelle zelle, Enums.EnumAbbauen enumAbbauen) {
        init_AbbaubaresObjekt(level, renderArrayList, pruefenRenderIndex, zelle, enumAbbauen);
        ressourceListe.add(this);
        ladeTextureRegion_setBounds();
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public void renderOverride(SpriteBatch spriteBatch, List<StatischesSymbol> symbolListe) {
        super.renderOverride(spriteBatch, symbolListe);
        TextureUtils.zeichneTexturRegion(spriteBatch, textureRegion, getPosition().x,
                getPosition().y - KonstantenOptik.TILE_ISO_HEIGHT/2, 1, 1, false);
    }

    @Override
    public void ladeTextureRegion_setBounds() {
        assetContainer = ermittleAssetContainer();
        textureRegion = assetContainer.get(getGegenstandListe());
        settBounds(getPosition(), textureRegion.getRegionWidth(), textureRegion.getRegionHeight());
    }

    protected RessourcenAssetContainer ermittleAssetContainer() { //Wird in Res Nachwachsen ueberschrieben
        return IdsRessourcenWachsen.getAssetContainer(getId(), Enums.EnumResNwStufe.ALT); //TODO Loeschen wenn Resosurcen ohne Nachwachsen implementiert
    }

    @Override
    protected void loescheAusSpeziellenListen() {
        getLevel().getAbzubauendeRessourcenListe().remove(this);
        getLevel().getRessourcenListe().remove(this);
    }

    @Override
    public void mengenVeraenderung() {
        if (!getGegenstandListe().issMenge(true, true, true)
                || !((this instanceof RessourceWachsen) || ((RessourceWachsen) this).isTod())) {
            getLevel().getWorldObjektLoeschenListe().add(this);
            return;
        }
        ladeTextureRegion_setBounds();
    }

    // POSITIONEN /////////////////////////////////////////////////////////////////////////////////

    @Override
    protected Vector2 positionZuPositionMitte(Vector2 position) {
        return new Vector2(position.x + KonstantenOptik.TILE_ISO_WIDTH / 2, position.y);
    }

    @Override
    protected Vector2 positionMitteZuPosition(Vector2 positionMitte) {
        return new Vector2(positionMitte.x - KonstantenOptik.TILE_ISO_WIDTH / 2, positionMitte.y);
    }

    // EINFACHE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public void setAssetContainer(RessourcenAssetContainer assetContainer) {
        this.assetContainer = assetContainer;
    }

    public void setTextureRegion(TextureRegion textureRegion) {
        this.textureRegion = textureRegion;
    }
}
