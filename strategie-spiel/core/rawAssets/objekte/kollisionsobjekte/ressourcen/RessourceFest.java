package de.arnonym.strategiespiel.spielSchirm.objekte.kollisionsobjekte.ressourcen;

import java.util.List;

import de.arnonym.strategiespiel.spielSchirm.Level;
import de.arnonym.strategiespiel.spielSchirm.worldMap.RenderArrayList;
import de.arnonym.strategiespiel.spielSchirm.worldMap.Zelle;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.GegenstandListe;
import de.arnonym.strategiespiel.framework.werkzeuge.Enums;
import de.arnonym.strategiespiel.framework.werkzeuge.RessourcenAssetContainer;
import de.arnonym.strategiespiel.spielSchirm.balancing.IdsRessourcenWachsen;

public class RessourceFest extends Ressource {
    // BESCHREIBEN //
    public RessourceFest(int id, GegenstandListe kopierenGegenstandListe_nl,
                     boolean kollisionBauen, float kollisionLaufen) {
        super(id, kopierenGegenstandListe_nl, kollisionBauen, kollisionLaufen);
    }

    // KOPIEREN //
    protected RessourceFest(RessourceFest ressourceFest) {
        super(ressourceFest);
    }

    // INITIALISIEREN //
    protected void init_Ressource(Level level, List<Ressource> ressourceListe,
                                  RenderArrayList renderArrayList, boolean pruefenRenderIndex,
                                  Zelle zelle, Enums.EnumAbbauen enumAbbauen) {
        init_AbbaubaresObjekt(level, renderArrayList, pruefenRenderIndex, zelle, enumAbbauen);
        ressourceListe.add(this);
        ladeTextureRegion_setBounds();
    }

    // CREATE //
    public RessourceFest create(Level level, List<Ressource> ressourceListe,
                            RenderArrayList renderArrayList, Zelle zelle) {
        RessourceFest ressourceFest = new RessourceFest(this);
        ressourceFest.init_Ressource(level, ressourceListe, renderArrayList, false, zelle,
                Enums.EnumAbbauen.NEIN);
        return ressourceFest;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public void update(float delta) {
    }

    protected RessourcenAssetContainer ermittleAssetContainer() { //Wird in Res Nachwachsen ueberschrieben
        return IdsRessourcenWachsen.getAssetContainer(getId(), Enums.EnumResNwStufe.ALT); //TODO Loeschen wenn Resosurcen ohne Nachwachsen implementiert
    }

    // EINFACHE GETTER UND SETTER /////////////////////////////////////////////////////////////////
}
