package de.arnonym.strategiespiel.spielSchirm.objekte.kollisionsobjekte;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

import java.util.List;

import de.arnonym.strategiespiel.spielSchirm.Level;
import de.arnonym.strategiespiel.spielSchirm.objekte.Eigentuemer;
import de.arnonym.strategiespiel.spielSchirm.ui.StatischesSymbol;
import de.arnonym.strategiespiel.spielSchirm.worldMap.Zelle;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.GegenstandListe;
import de.arnonym.strategiespiel.spielSchirm.worldMap.RenderArrayList;
import de.arnonym.strategiespiel.framework.werkzeuge.Assets;
import de.arnonym.strategiespiel.framework.werkzeuge.Enums;
import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenBalancing;
import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenOptik;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.TextureUtils;

public class BodenStapel extends AbbaubaresObjekt implements Eigentum {
    private Eigentuemer eigentuemer;
    private static TextureRegion textureRegion;
    private List<BodenStapel> bodenStapelListe;

    public BodenStapel(Level level, RenderArrayList renderArrayList,
                       List<BodenStapel> abzubauendeStatischeObjekteListe, int inhaltGegenstandId,
                       Eigentuemer eigentuemer, Zelle zelle) {
        super(-1, new GegenstandListe(KonstantenBalancing.BODEN_STAPEL_MAX_PLATZ),
                new Vector2(1, 1), true, 2); // TODO Konstanten (die 2 bei laufen Kollision)
        init_AbbaubaresObjekt(level, renderArrayList, true, zelle,
                Enums.EnumAbbauen.ABBAUEN_VERERBEN);
        this.eigentuemer = eigentuemer;
        this.bodenStapelListe = abzubauendeStatischeObjekteListe;
        abzubauendeStatischeObjekteListe.add(this);
        getGegenstandListe().addGegenstand(inhaltGegenstandId, 0, 0);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public void update(float delta) {
        if (getGegenstandListe().gettMenge(true, true, true) <= 0) {
            getLevel().getWorldObjektLoeschenListe().add(this);
        }
    }

    @Override
    protected void renderOverride(SpriteBatch spriteBatch, List<StatischesSymbol> symbolListe) {
        super.renderOverride(spriteBatch, symbolListe);
        TextureUtils.zeichneTexturRegion(spriteBatch, textureRegion, getPosition().x,
                getPosition().y - KonstantenOptik.TILE_ISO_HEIGHT/2, 1, 1, false); //TODO Position ist dann die gleiche wie die der Zelle (das ist auch be Ressource so) macht das so Sinn??
    }

    @Override
    public void ladeTextureRegion_setBounds() {
        textureRegion = Assets.instance.gebaudeAssets.bodenStapel;
        settBounds(getPosition(), textureRegion.getRegionWidth(), textureRegion.getRegionHeight());
    }

    @Override
    protected void loescheAusSpeziellenListen() {
        bodenStapelListe.remove(this);
    }

    @Override
    public void mengenVeraenderung() {
        super.mengenVeraenderung();
        if (getGegenstandListe().gettMenge(true, true, true) <= 0) {
            loesche();
        }
    }

    // POSITIONEN /////////////////////////////////////////////////////////////////////////////////

    @Override
    protected Vector2 positionMitteZuPosition(Vector2 positionMitte) {
        return positionMitte;
    }

    @Override
    protected Vector2 positionZuPositionMitte(Vector2 position) {
        return position;
    }

    @Override
    public Eigentuemer gettEigentuemer() {
        return eigentuemer;
    }

    @Override
    public void settEigentuemer(Eigentuemer eigentuemer) {
        this.eigentuemer = eigentuemer;
        eigentuemer.getEigentumListe().add(this);
    }
}
