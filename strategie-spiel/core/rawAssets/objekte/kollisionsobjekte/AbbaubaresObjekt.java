package de.arnonym.strategiespiel.spielSchirm.objekte.kollisionsobjekte;

import com.badlogic.gdx.math.Vector2;

import de.arnonym.strategiespiel.spielSchirm.Level;
import de.arnonym.strategiespiel.spielSchirm.objekte.Eigentuemer;
import de.arnonym.strategiespiel.spielSchirm.objekte.kollisionsobjekte.ressourcen.Ressource;
import de.arnonym.strategiespiel.spielSchirm.worldMap.RenderArrayList;
import de.arnonym.strategiespiel.spielSchirm.worldMap.Zelle;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.GegListe;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.MusterGeg;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.Gegenstand;
import de.arnonym.strategiespiel.framework.werkzeuge.Enums;

public abstract class AbbaubaresObjekt extends StatischesObjekt {
    private Enums.EnumAbbauen enumAbbauen;
    private Zelle zelle;

    // BESCHREIBEN //
    public AbbaubaresObjekt(int id, GegListe<MusterGeg> musterGegListe_nl,
                            Vector2 blaupausePositionsMod, boolean bauenKollision,
                            float laufenKollision) {
        super(id, musterGegListe_nl, blaupausePositionsMod, bauenKollision,
                laufenKollision);
    }

    // KOPIEREN //
    public AbbaubaresObjekt(AbbaubaresObjekt abbaubaresObjekt) {
        super(abbaubaresObjekt);
    }

    // INITIALISIEREN //
    public void init_AbbaubaresObjekt(Level level, RenderArrayList renderArrayList, boolean pruefeRenderIndex,
                     Zelle zelle, Enums.EnumAbbauen enumAbbauen) {
        init_statischesObjekt(level, renderArrayList, pruefeRenderIndex, zelle.getPosition());
        this.zelle = zelle;
        zelle.settStatischesObjekt(this);
        if (enumAbbauen == Enums.EnumAbbauen.ABBAUEN_VERERBEN) {
            this.enumAbbauen = Enums.EnumAbbauen.ABBAUEN_VERERBEN;
            if (this instanceof Ressource) {
                getLevel().getAbzubauendeRessourcenListe().add((Ressource) this);
            }
        } else {
            this.enumAbbauen = Enums.EnumAbbauen.NEIN;
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    protected void loescheAusZellen() {
        zelle.settStatischesObjekt(null);
    }

    protected void vergammeln(float delta, float vergammelnMod) {
        for (Gegenstand g : getGegenstandListe()) {
            g.vergammeln(delta, vergammelnMod);
        }
        mengenVeraenderung();
    }

    // KOMPLEXE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public Eigentuemer gettBesitzer() {
        return zelle.gettEigentuemer();
    }

    // EINFACHE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public Enums.EnumAbbauen getEnumAbbauen() {
        return enumAbbauen;
    }

    public void setEnumAbbauen(Enums.EnumAbbauen enumAbbauen) {
        this.enumAbbauen = enumAbbauen;
    }

    public Zelle getZelle() {
        return zelle;
    }
}
