package de.arnonym.strategiespiel.spielSchirm.objekte.kollisionsobjekte;

import com.badlogic.gdx.math.Vector2;

import de.arnonym.strategiespiel.spielSchirm.objekte.dorfbewohnerAi.Dorfbewohner;
import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenBalancing;
import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenFehlerAusgleich;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.MathUtils;

public interface ArbeitObjekt {

    float arbeitszeitPlanen(Dorfbewohner dorfbewohner);

    void arbeitszeitPlanenRueckgaenig(float arbeitszeit);

    void addArbeitszeitErledigt(float arbeitszeit);

    float gettErledigtProzent();

    float gettErledigtUndGeplantProzent();
}
