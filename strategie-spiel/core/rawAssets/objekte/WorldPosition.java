package de.arnonym.strategiespiel.spielSchirm.objekte;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

import java.util.List;

import de.arnonym.strategiespiel.spielSchirm.ui.StatischesSymbol;

public class WorldPosition extends WorldObjekt {

    public WorldPosition(Vector2 position) {
        super(1, 1, null);
        init_worldActor(null, position);
    }

    @Override
    public void update(float delta) {

    }

    @Override
    public void renderOverride(SpriteBatch spriteBatch, List<StatischesSymbol> symbolListe) {

    }

    @Override
    protected Vector2 positionMitteZuPosition(Vector2 positionMitte) {
        return getPosition();
    }

    @Override
    protected Vector2 positionZuPositionMitte(Vector2 position) {
        return getPosition();
    }

    @Override
    protected void loescheAusSpeziellenListen() {

    }
}
