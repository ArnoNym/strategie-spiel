package de.arnonym.strategiespiel.spielSchirm.objekte;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import de.arnonym.strategiespiel.spielSchirm.Level;
import de.arnonym.strategiespiel.spielSchirm.ui.StatischesSymbol;

public abstract class WorldActor implements Serializable {
    private Level level;
    private Vector2 position;
    private Vector2 positionMitte;
    private float width;
    private float height;
    private boolean visible = true;

    // BESCHREIBEN //
    public WorldActor(float width, float height) {
        this.width = width;
        this.height = height;
    }

    // KOPIEREN //
    protected WorldActor(WorldActor worldActor) {
        this.width = worldActor.width;
        this.height = worldActor.height;
    }

    // INITIALISIEREN //
    protected void init_worldActor(Level level, Vector2 position) {
        this.level = level;
        settPosition(position);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////

    public void render(SpriteBatch spriteBatch) {
        if (!isVisible()) return;
        final ArrayList<StatischesSymbol> symbolListe = new ArrayList<>(); //Wird jeden Tick neu gefuellt, dem statSymbManager im level hinzugefuegt und dort gerendert
        renderOverride(spriteBatch, symbolListe);
        if (level != null) { //Weil die Blaupause sonst eine null pointer Exception schmeisst
            level.getSchwebendeSymboleManager().add(positionMitte, getHeight(), symbolListe);
        }
    }

    // VISIBLE ////////////////////////////////////////////////////////////////////////////////////

    public void preufeVisible(float kameraX, float kameraY, float kameraWidth, float kameraHeight) {
        visible = (!(kameraX >= getRight() || kameraX + kameraWidth <= position.x
                || kameraY >= getTop() || kameraY + kameraHeight <= position.y));
    }

    public void settBounds(Vector2 position, float width, float height) {
        this.position = position;
        this.width = width;
        this.height = height;
    }

    // POSITIONEN /////////////////////////////////////////////////////////////////////////////////

    public void settPosition(Vector2 position) {
        this.position = position;
        this.positionMitte = positionZuPositionMitte(position);
    }

    public void settPositionMitte(Vector2 positionMitte) {
        this.positionMitte = positionMitte;
        this.position = positionMitteZuPosition(positionMitte);
    }

    public float getRight() {
        return position.x + width;
    }

    public float getTop() {
        return position.y + height;
    }

    protected abstract Vector2 positionMitteZuPosition(Vector2 positionMitte);

    protected abstract Vector2 positionZuPositionMitte(Vector2 position);

    // OVERRIDE ///////////////////////////////////////////////////////////////////////////////////

    public abstract void update(float delta);

    protected abstract void renderOverride(SpriteBatch spriteBatch, List<StatischesSymbol> symbolListe);

    public abstract void gotHit();

    // EINFACHE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public Level getLevel() {
        return level;
    }

    public Vector2 getPosition() {
        return position;
    }

    public Vector2 getPositionMitte() {
        return positionMitte;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public float getWidth() {
        return width;
    }

    public float getHeight() {
        return height;
    }
}