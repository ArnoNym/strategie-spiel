Fpackage de.arnonym.strategiespiel.spielSchirm.objekte.dorfbewohnerAi;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.TimeUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import de.arnonym.strategiespiel.spielSchirm.Level;
import de.arnonym.strategiespiel.spielSchirm.Dorf;
import de.arnonym.strategiespiel.spielSchirm.objekte.Eigentuemer;
import de.arnonym.strategiespiel.spielSchirm.objekte.WirtschaftObjekt;
import de.arnonym.strategiespiel.spielSchirm.objekte.WorldActor;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.ai.ai.todoai.arbeit.TaskBerufFarmen;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.ai.ai.todoai.arbeit.TaskBerufLehrer;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.ai.ai.todoai.arbeit.TaskBodenStapelAufraeumen;
import de.arnonym.strategiespiel.spielSchirm.objekte.dorfbewohnerAi.berufe.BerufFabrikArbeiter;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dna.DnaStrang;
import de.arnonym.strategiespiel.spielSchirm.objekte.kollisionsobjekte.BodenStapel;
import de.arnonym.strategiespiel.spielSchirm.objekte.ClickAbleObjekt;
import de.arnonym.strategiespiel.spielSchirm.objekte.WorldObjekt;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.ai.ai.tasks.Task;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.ai.ai.todoai.arbeit.TaskBerufFabrik;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.ai.ai.todoai.arbeit.TaskBauen;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.ai.ai.todoai.privat.TaskRetteVorVerhungern;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.ai.ai.todoai.arbeit.TaskRessourcenAbbauen;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.ai.ai.todoai.privat.TaskSchlafen;
import de.arnonym.strategiespiel.spielSchirm.objekte.dorfbewohnerAi.berufe.Beruf;
import de.arnonym.strategiespiel.spielSchirm.objekte.kollisionsobjekte.gebaude.gebBeruf.GebBerufFarm;
import de.arnonym.strategiespiel.spielSchirm.objekte.kollisionsobjekte.gebaude.gebBeruf.GebBerufHerstSchule;
import de.arnonym.strategiespiel.spielSchirm.ui.MyInputProcessor;
import de.arnonym.strategiespiel.spielSchirm.ui.StatischesSymbol;
import de.arnonym.strategiespiel.spielSchirm.worldMap.Zelle;
import de.arnonym.strategiespiel.spielSchirm.zahlen.Schaden;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.Gegenstand;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.GegenstandListe;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.Geld;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.taschen.MedizinTasche;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.taschen.NahrungTasche;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.taschen.WerkzeugTasche;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.taschen.SpielzeugTasche;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.ai.ai.StackFiniteStepMachine;
import de.arnonym.strategiespiel.spielSchirm.objekte.kollisionsobjekte.gebaude.gebBeruf.GebBeruf;
import de.arnonym.strategiespiel.spielSchirm.objekte.kollisionsobjekte.gebaude.Gebaude;
import de.arnonym.strategiespiel.spielSchirm.objekte.kollisionsobjekte.gebaude.GebaudeHaus;
import de.arnonym.strategiespiel.spielSchirm.worldMap.RenderArrayList;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.taschen.Tasche;
import de.arnonym.strategiespiel.framework.werkzeuge.Assets;
import de.arnonym.strategiespiel.framework.werkzeuge.Enums;
import de.arnonym.strategiespiel.framework.werkzeuge.IngameKamera;
import de.arnonym.strategiespiel.spielSchirm.balancing.IdsSymbole;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.GeometryUtils;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.MyTimeUtils;
import de.arnonym.strategiespiel.framework.werkzeuge.PauseDelta;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.SpecialUtils;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.TextureUtils;
import de.arnonym.strategiespiel.spielSchirm.balancing.IdsGegenstaende;
import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenBalancing;
import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenOptik;


/**
 * Created by LeonPB on 07.04.2018.
 */

public class Dorfbewohner extends WorldObjekt implements ClickAbleObjekt, WirtschaftObjekt {
    private Dorf dorf;
    private Geld geld;
    private List<WorldActor> keinPfadListe = new ArrayList<>();

    private Enums.EnumDbStates enumDbStates;
    private StackFiniteStepMachine stackFiniteStepMachine;
    private PauseDelta einkaufenPause;
    private Enums.EnumLaufen enumLaufen = Enums.EnumLaufen.STEHEN;
    private Enums.EnumRichtung enumRichtung = Enums.EnumRichtung.UNTEN;
    private long laufenStartZeit;
    private Gebaude befindetSichInGebaude = null;
    private boolean freeze = false; // Testzwecke

    private GebBeruf gebBeruf; // Fuer Schueler die Schule
    private Beruf beruf = null;

    private final boolean geschlechtFemale;
    private final DnaStrang dnaStrang;
    private float schlaf;
    private int schlafMax;

    private float bildung = 1; // Angeeignet durch Schulen
    private float fitness = 1; // Angeeignet durch Arbeiten TODO alles noch erstellen. Auch zB durch gelaufene Wege die Fitness modifizieren
    private float streckeGelaufen;
    private float produktivitaetKopf; // Produktivitaet fuer Arbeit mit dem Kopf
    private float produktivitaetKoerper; // Produktivitaet fuer Arbeit mit dem Koerper
    private Float gesundheit = 100f;
    private float alter_sekunden = 0;
    private Dorfbewohner schwangerMitDB = null;
    private PauseDelta schwangerschaftPause = new PauseDelta(
            KonstantenBalancing.SCHWANGERSCHAFT_DAUER, Enums.EnumPauseStart.VORBEI);

    private boolean verhungert = false;

    private String vorname;
    private Familie familie;
    private NahrungTasche nahrungsTasche;
    private SpielzeugTasche spielzeugTasche;
    private WerkzeugTasche werkzeugTasche;
    private MedizinTasche medizinTasche;
    private GebaudeHaus zuhause;

    public Dorfbewohner(String vorname, boolean geschlechtFemale, float alter_jahre, float dnaProduktivtiaetKopf,
                        float dnaProduktivitaetKoerper, float konstantenNutzenExponentNahrung,
                        float konstantenNutzenExponentSpielzeugtasche,
                        float konstantenNutzenExponentWohnen,
                        int konstantenBewegungsgeschwindigkeit,
                        int konstantenSchlafAuffuellGeschwindigkeit) {
        super(Assets.instance.dorfbewohnerAAssets.dorfbewohnerStehen.getRegionWidth(),
                Assets.instance.dorfbewohnerAAssets.dorfbewohnerStehen.getRegionHeight(),
                new GegenstandListe<Gegenstand>(new IdsGegenstaende().gettGegenstandListe(), //TODO Width und Height
                KonstantenBalancing.GEGENSTAND_LISTE_STANDARD_PLATZ));
        this.vorname = vorname;
        this.geschlechtFemale = geschlechtFemale;
        this.alter_sekunden = alter_jahre * KonstantenBalancing.SEKUNDEN_PRO_JAHR;
        this.gesundheit -= gettAlter();
        this.dnaStrang = new DnaStrang(dnaProduktivtiaetKopf, dnaProduktivitaetKoerper,
                konstantenNutzenExponentNahrung, konstantenNutzenExponentSpielzeugtasche,
                konstantenNutzenExponentWohnen, konstantenBewegungsgeschwindigkeit,
                konstantenSchlafAuffuellGeschwindigkeit);
        this.dnaProduktivtiaetKopf = dnaProduktivtiaetKopf; //Nicht aus Dna kreieren, da es genau die stats sein sollen, die in IdsFamilien definiert wurden
        this.dnaProduktivitaetKoerper = dnaProduktivitaetKoerper;
        this.konstantenNutzenExponentNahrung = konstantenNutzenExponentNahrung;
        this.konstantenNutzenExponentSpielzeugtasche = konstantenNutzenExponentSpielzeugtasche;
        this.konstantenNutzenExponentWohnen = konstantenNutzenExponentWohnen;
        this.konstantenBewegungsgeschwindigkeit = konstantenBewegungsgeschwindigkeit;
        this.konstantenSchlafAuffuellGeschwindigkeit = konstantenSchlafAuffuellGeschwindigkeit;
    }

    public Dorfbewohner(DnaStrang motherDnaStrang, DnaStrang fatherDnaStrang) {
        super(Assets.instance.dorfbewohnerAAssets.dorfbewohnerStehen.getRegionWidth(),
                Assets.instance.dorfbewohnerAAssets.dorfbewohnerStehen.getRegionHeight(),
                new DorfbewohnerGegenstandListe(new IdsGegenstaende().gettGegenstandListe(),
                KonstantenBalancing.GEGENSTAND_LISTE_STANDARD_PLATZ));
        this.vorname = "Billy"; //TODO
        this.geschlechtFemale = ThreadLocalRandom.current().nextBoolean();
        this.dnaStrang = new DnaStrang(motherDnaStrang, fatherDnaStrang);
        this.dnaProduktivtiaetKopf = dnaStrang.gettSingleValue("dnaProduktivtiaetKopf");
        this.dnaProduktivitaetKoerper = dnaStrang.gettSingleValue("dnaProduktivitaetKoerper");
        this.konstantenNutzenExponentNahrung = dnaStrang.gettSingleValue("konstantenNutzenExponentNahrung");
        this.konstantenNutzenExponentSpielzeugtasche = dnaStrang.gettSingleValue("konstantenNutzenExponentSpielzeugtasche");
        this.konstantenNutzenExponentWohnen = dnaStrang.gettSingleValue("konstantenNutzenExponentWohnen");
        this.konstantenBewegungsgeschwindigkeit = (int) dnaStrang.gettSingleValue("konstantenBewegungsgeschwindigkeit");
        this.konstantenSchlafAuffuellGeschwindigkeit = (int) dnaStrang.gettSingleValue("konstantenSchlafAuffuellGeschwindigkeit");
    }

    public void init(Level level, Dorf dorf, RenderArrayList renderArrayList, MyInputProcessor
            worldInputProcessor, Familie familie, Vector2 position) {
        init_worldObjekt(level, renderArrayList, true, position);
        this.dorf = dorf;
        this.familie = familie;
        setName(vorname + " " + familie.getNachname());
        worldInputProcessor.add(this);
        schlaf = schlafMax = 10; //TODO Konstanten
        enumDbStates = Enums.EnumDbStates.IDLE;
        stackFiniteStepMachine = new StackFiniteStepMachine();
        einkaufenPause = new PauseDelta(10, Enums.EnumPauseStart.JETZT_ZUFAELLIGE_RESTDAUER); //TODO Konstante
        //getMusterGegListe().addGegenstandPruefeReihenfolgeMengeMaximalUndWert(IdsGegenstaende.NAH_BEEREN_ID, 0, 30 * Konstanten.NACHKOMMASTELLEN);
        //getMusterGegListe().addGegenstandPruefeReihenfolgeMengeMaximalUndWert(IdsGegenstaende.MAT_HOLZ_ID, 0, 0 * Konstanten.NACHKOMMASTELLEN);
        //getMusterGegListe().addGegenstandPruefeReihenfolgeMengeMaximalUndWert(IdsGegenstaende.MAT_PROD_WERKZEUG_EINFACH_ID, 0, 30 * Konstanten.NACHKOMMASTELLEN);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public void update(float delta) { } //TODO schoen machen

    public void update(float delta, boolean pauseVorbei, float pauseDauer) { // TODO Pause bei Familienwechsel...
        if (freeze) {
            return;
        }

        if (pauseVorbei) {
            schlaf = Math.max(0, schlaf - pauseDauer);

            // Produktivitaet
            float produktivitaetNahrung = nahrungsTasche.gettProduktivitaet();
            //
            produktivitaetKopf = 0.5f + 0.5f * produktivitaetNahrung * dnaProduktivtiaetKopf * bildung;
            //
            float produktivitaetWerkzeug = werkzeugTasche.gettProduktivitaet();
            fitness = updateFitness(fitness);
            produktivitaetKoerper = 0.5f + 0.5f * produktivitaetWerkzeug * produktivitaetNahrung
                    * dnaProduktivitaetKoerper * fitness;

            // Gesundheit
            gesundheit += medizinTasche.verbauchen(1);
            //if (gesundheit.isNaN()) gesundheit = 0.5f;
        }

        alter_sekunden += delta;
        gesundheit -= delta / fitness / nahrungsTasche.gettGesundheit(false);

        if (gesundheit <= 0) {
            getLevel().getFensterChat().addMessage(getName() + " passed away!");
            loesche();
            return;
        }

        updatePartnerKinderUndSchwangerschaft(delta);
        entscheiden(delta, false);

        stackFiniteStepMachine.update(delta);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    //region ENTSCHEIDEN
    ///////////////////////////////////////////////////////////////////////////////////////////////

    private void entscheiden(float delta, boolean langeweile) {
        einkaufenPause.issVorbei(delta, false);

        if (!stackFiniteStepMachine.isEmpty()) {
            return;
        }

        /*if (langeweile || einkaufenPause.issVorbei()) {
            TaskEinkaufenLuxus taskKaufenPrivatLuxus = new TaskEinkaufenLuxus(this);
            if (taskKaufenPrivatLuxus.versuchen() == Enums.EnumStepStatus.OK) {
                stackFiniteStepMachine.addTask(taskKaufenPrivatLuxus, true);
                return;
            }
            einkaufenPause.settStartzeitJetzt();
        }

        float nahrungMenge = familie.getNahrungsTasche().gettMenge(true, true, false);
        if (nahrungMenge <= KonstantenBalancing.DORFBEWOHNER_NAHRUNG_GRUNDBEDARF_SUCHEN_AB) {
            if (essen()) {
                enumDbStates = Enums.EnumDbStates.ESSEN;
                verhungert = false;
                return;
            }
            if (nahrungMenge <= KonstantenBalancing.DORFBEWOHNER_NAHRUNG_GRUNDBEDARF_VERHUNGERN) {
                verhungert = true;
            }
        }

        if (schlaf <= 0 || langeweile) {
            if (schlafen()) {
                enumDbStates = Enums.EnumDbStates.SCHLAFEN;
                return;
            }
        }

        if (gettAlter() <= 14) {
            if (gebBeruf != null || sucheSchule()) {
                TaskSchueler taskSchueler = new TaskSchueler(this);
                if (taskSchueler.versuchen() == Enums.EnumStepStatus.OK) {
                    stackFiniteStepMachine.addTask(taskSchueler, true);
                    return;
                }
            }
        } else*/ if (arbeiten()) {
            return; //enumDbStates wird in arbeit() gesetzt
        }

        if (!langeweile) {
            enumDbStates = Enums.EnumDbStates.IDLE;
            //entscheiden(delta, true); TODO Fuer Bug fixes eingeklammern und DB berechnbarer zu machen
        }
    }

    // ESSEN //////////////////////////////////////////////////////////////////////////////////////

    private boolean essen() {
        TaskRetteVorVerhungern taskKaufenPrivatVerhungern = new TaskRetteVorVerhungern(this);
        if (taskKaufenPrivatVerhungern.versuchen() == Enums.EnumStepStatus.OK) {
            stackFiniteStepMachine.addTask(taskKaufenPrivatVerhungern, true);
            return true;
        }
        return false;
    }

    // SCHLAFEN ///////////////////////////////////////////////////////////////////////////////////

    private boolean schlafen() {
        TaskSchlafen taskSchlafen = new TaskSchlafen(this);
        if (taskSchlafen.versuchen() == Enums.EnumStepStatus.OK) {
            stackFiniteStepMachine.addTask(taskSchlafen, true);
            return true;
        }
        return false;
    }

    // ARBEITEN ///////////////////////////////////////////////////////////////////////////////////

    private boolean arbeiten() {
        /*if (beruf == null) {
            if (suche()) { //TODO Nur alle paar Sekunden pruefenUndSuchen lassen
                return arbeit();
            }
        } else if (arbeitenBeruf()) {
            enumDbStates = Enums.EnumDbStates.ARBEITEN_BERUF;
            return true;
        }*/
        enumDbStates = Enums.EnumDbStates.ARBEITEN_FREI;
        return arbeitenFrei();
    }

    // ARBEITEN BERUF //

    private boolean arbeitenBeruf() {
        if (gebBeruf instanceof GebBerufFarm) {
            TaskBerufFarmen taskBerufFarmen = new TaskBerufFarmen(this, (GebBerufFarm) gebBeruf);
            if (taskBerufFarmen.versuchen() == Enums.EnumStepStatus.OK) {
                stackFiniteStepMachine.addTask(taskBerufFarmen, true);
                return true;
            }
            return false;
        }
        if (beruf instanceof BerufFabrikArbeiter) {
            TaskBerufFabrik taskBerufFabrik = new TaskBerufFabrik(this);
            if (taskBerufFabrik.versuchen() == Enums.EnumStepStatus.OK) {
                stackFiniteStepMachine.addTask(taskBerufFabrik, true);
                return true;
            }
            return false;
        }
        if (gebBeruf instanceof GebBerufHerstSchule) {
            TaskBerufLehrer taskBerufLehrer = new TaskBerufLehrer(this);
            if (taskBerufLehrer.versuchen() == Enums.EnumStepStatus.OK) {
                stackFiniteStepMachine.addTask(taskBerufLehrer, true);
                return true;
            }
            return false;
        }
        return false;
    }

    // ARBEITEN FREI //

    private boolean arbeitenFrei() {
        Task task = new TaskBauen(this);
        if (task.versuchen() == Enums.EnumStepStatus.OK) {
            stackFiniteStepMachine.addTask(task, true);
            return true;
        }
        task = new TaskBodenStapelAufraeumen(this);
        if (task.versuchen() == Enums.EnumStepStatus.OK) {
            stackFiniteStepMachine.addTask(task, true);
            System.out.println("Dorfbewohner ::: 000");
            return true;
        }
        task = new TaskRessourcenAbbauen(this);
        if (task.versuchen() == Enums.EnumStepStatus.OK) {
            stackFiniteStepMachine.addTask(task, true);
            return true;
        }
        return false;
    }

    // IDLE ///////////////////////////////////////////////////////////////////////////////////////

    private void idle() {

    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    //endregion
    ///////////////////////////////////////////////////////////////////////////////////////////////

    ///////////////////////////////////////////////////////////////////////////////////////////////
    //region BILDUNG
    ///////////////////////////////////////////////////////////////////////////////////////////////

    public void addBildung(Enums.EnumBildungsgrad unterrichtsniveau, float addBildung) {
        //TODO Maximum fuer utnerschiedliche Bildungsgrade
        this.bildung += addBildung;
    }

    private boolean sucheSchule() {
        GebBerufHerstSchule minDistanzSchule = null;
        float minDistanz = Float.MAX_VALUE;
        for (Gebaude g : dorf.getGebaudeListe()) {
            if (g instanceof GebBerufHerstSchule && ((GebBerufHerstSchule) g).issPassendeSchule(this)) {
                float distanz = GeometryUtils.hypothenuse(getPositionMitte(), g.getPositionMitte());
                if (distanz < minDistanz) {
                    minDistanz = distanz;
                    minDistanzSchule = (GebBerufHerstSchule) g;
                }
            }
        }
        gebBeruf = minDistanzSchule;
        if (minDistanzSchule == null) {
            return false;
        } else {
            minDistanzSchule.addNeuerSchueler(this);
            return true;
        }
    }

    private float grenzBildung(Enums.EnumBildungsgrad unterrichtsniveau, float bildung) {
        return bildung; // TODO Hatte noch keine gute Idee. Man beacht die Funktionen in Dorfbewohner Utils
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    //endregion
    ///////////////////////////////////////////////////////////////////////////////////////////////

    ///////////////////////////////////////////////////////////////////////////////////////////////
    //region BERUF
    ///////////////////////////////////////////////////////////////////////////////////////////////

    public boolean sucheBeruf() {
        //TODO Um die Distanz vom Wohnort zum Arbeitsplatz erweitern
        int maxGehalt = dorf.getMindestLohn();
        Beruf maxGehaltBeruf = null;
        for (Gebaude g : dorf.getGebaudeListe()) {
            if (g instanceof GebBeruf) {
                for (Beruf b : ((GebBeruf) g).getBerufeListe()) {
                    if (b.getDorfbewohner() == null || b == beruf) { //Also wenn keiner die Stelle hat oder der suchende die Stelle hat
                        int gehalt = b.gehalt(this);

                        int nutzen = gehalt;
                        if (b != beruf) {
                            nutzen *= KonstantenBalancing.DB_BERUF_WECHSEL_KOSTEN;
                        }
                        if (zuhause != null) {
                            //nutzen -=
                        }

                        if (nutzen > maxGehalt) {
                            maxGehalt = gehalt;
                            maxGehaltBeruf = b;
                        }
                    }
                }
            }
        }
        if (maxGehaltBeruf != null) {
            berufAnnehmen(maxGehaltBeruf, maxGehalt);
            return true;
        } else {
            if (beruf != null) {
                berufKuendigen();
            }
            return false;
        }
    }

    private void berufAnnehmen(Beruf annehmenBeruf, int gehalt) {
        if (beruf == annehmenBeruf) {
            return;
        }
        if (beruf != null) {
            berufKuendigen();
        }
        beruf = annehmenBeruf;
        gebBeruf = beruf.getGebaudeBeruf();
        beruf.annehmen(this, gehalt);
    }

    public void berufKuendigen() {
        beruf.kuendigen();
        gebBeruf = null;
        beruf = null;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    //endregion
    ///////////////////////////////////////////////////////////////////////////////////////////////

    // FAMIELE VERGOESSERN ////////////////////////////////////////////////////////////////////////

    private void updatePartnerKinderUndSchwangerschaft(float delta) {
        if (!isFemale()) {
            return;
        }
        if (schwangerschaftPause.issVorbei(delta, false) && schwangerMitDB != null) { //Reihenfolge wichtig, da die Pause das Delta benoetigt um abzulaufen (zwischen zwei Schwangerschaften)
            dorf.getDorfbewohnerWollengebaerenListe().add(this);
            return;
        }
        if (issParent() && familie.issHatZweiParents()) {
            if (familie.getFamilienmitglieder().size() < 5) { //TODO Loeschen wenn eine Schwangerschaft angemessen lange braucht
                if (versuchenKindZeugen()) {
                    getLevel().getFensterChat().addMessage(getName() + " is pregnant.");
                }
            }
            return;
        }
        if (suchtEinenPartner() && suchePartner()) {
            getLevel().getFensterChat().addMessage(getName() + " and "
                    + familie.gettPartner(this).getName() + " got married."); //TODO frueherer Name einfuegen
            return;
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    //region PARTNER UND BEZIEHUNGEN
    ///////////////////////////////////////////////////////////////////////////////////////////////

    private boolean suchePartner() {
        List<Dorfbewohner> potentiellePartnerListe = new ArrayList<>();
        for (Dorfbewohner d : dorf.gettDorfbewohnerListe()) {
            if (geschlechtFemale != d.isFemale()
                    && d.suchtEinenPartner()
                    && GeometryUtils.hypothenuse(getPosition(), d.getPosition())
                        < KonstantenBalancing.DB_MAX_DISTANZ_ZU_ANDEREM_DB_FUER_VERLIEBEN_UND_KINDER
                    && familie != d.getFamilie()
                    && Math.random() > KonstantenBalancing.DB_PARTNER_KOMMT_IN_FRAGE_CHANCE) {
                potentiellePartnerListe.add(d);
            }
        }
        if (potentiellePartnerListe.size() > 0) {
            dorf.getDorfbewohnerNeuePartnerHashMap().put(this, potentiellePartnerListe.get(0)); //TODO Wie genau aussuchen?
            return true;
        }
        return false;
    }

    private boolean suchtEinenPartner() {
        return alter_sekunden / KonstantenBalancing.SEKUNDEN_PRO_JAHR
                > KonstantenBalancing.DB_MINDESTALTER_HEIRATEN_UND_KINDER
                && issSingle()
                && !dorf.getDorfbewohnerNeuePartnerHashMap().containsKey(this)
                && !dorf.getDorfbewohnerNeuePartnerHashMap().containsValue(this);
    }

    public void neuerPartner(Dorfbewohner neuerPartner) {
        //Alle Tasks aller Familienmitglieder loeschen_familieNichtDBs da es sonst Probleme mit den gegenstaende etc geben koennte
        for (Dorfbewohner d : this.familie.getFamilienmitglieder()) {
            d.stackFiniteStepMachine.loescheAlleTasks();
        }
        for (Dorfbewohner d : neuerPartner.familie.getFamilienmitglieder()) {
            d.stackFiniteStepMachine.loescheAlleTasks();
        }

        boolean thisIsParent = issParent();
        boolean neuerPartnerIsParent = neuerPartner.issParent();
        if (thisIsParent && neuerPartnerIsParent) {
            if (!geschlechtFemale) { //Also wenn dieser DB ein Mann ist
                familie.zusammenfuegen(neuerPartner, neuerPartner.familie);
            } else { //Wenn der Partner ein Mann ist
                neuerPartner.familie.zusammenfuegen(this, familie);
            }
        } else if (thisIsParent) {
            int erbe = neuerPartner.familie.remove(neuerPartner, true, familie);
            familie.getGeld().mengeErhoehen(erbe, false);
            familie.addFamilienmitglied(neuerPartner, true);
        } else if (neuerPartnerIsParent) {
            int erbe = familie.remove(this, true, neuerPartner.familie);
            neuerPartner.familie.getGeld().mengeErhoehen(erbe, false);
            neuerPartner.familie.addFamilienmitglied(neuerPartner, true);
        } else {
            int erbe = familie.remove(this, true, neuerPartner.familie);
            erbe += neuerPartner.familie.remove(neuerPartner, true, familie);

            Familie neueFamilie = new Familie(getLevel(), dorf, erbe);
            neueFamilie.addFamilienmitglied(this, true);
            neueFamilie.addFamilienmitglied(neuerPartner, true);
            dorf.getFamilienListe().add(neueFamilie);

            familie = neueFamilie;
            neuerPartner.familie = neueFamilie;
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    //endregion
    ///////////////////////////////////////////////////////////////////////////////////////////////

    ///////////////////////////////////////////////////////////////////////////////////////////////
    //region SCHWANGERSCHAFTEN UND KINDER
    ///////////////////////////////////////////////////////////////////////////////////////////////

    private boolean versuchenKindZeugen() {
        Dorfbewohner partner = familie.gettPartner(this);
        if (geschlechtFemale && schwangerMitDB == null
                && schwangerschaftPause.issVorbei()
                && alter_sekunden / KonstantenBalancing.SEKUNDEN_PRO_JAHR
                > KonstantenBalancing.DB_MINDESTALTER_HEIRATEN_UND_KINDER
                && GeometryUtils.hypothenuse(getPosition(), partner.getPosition())
                < KonstantenBalancing.DB_MAX_DISTANZ_ZU_ANDEREM_DB_FUER_VERLIEBEN_UND_KINDER
                && Math.random() > KonstantenBalancing.DB_KIND_WIRD_GEZEUGT_CHANCE) {
            schwangerMitDB = new Dorfbewohner(this.dnaStrang, partner.dnaStrang);
            schwangerschaftPause.settDauer(KonstantenBalancing.SCHWANGERSCHAFT_DAUER,
                    Enums.EnumPauseStart.JETZT);
            return true;
        }
        return false;
    }

    public void kindGebaeren() {
        schwangerMitDB.init(getLevel(), getDorf(), getRenderArrayList(), getLevel()
                .getWorldInputProcessor(), familie, new Vector2(getPosition()));
        familie.addFamilienmitglied(schwangerMitDB, false);
        String text = getName() + " has born a little ";
        if (schwangerMitDB.isFemale()) {
            text += "girl ";
        } else {
            text += "boy ";
        }
        getLevel().getFensterChat().addMessage(text + "named " + schwangerMitDB.getName() + ".");
        schwangerMitDB = null;
        schwangerschaftPause.settDauer(KonstantenBalancing
                .SCHWANGERSCHAFT_BIS_KANN_WIEDER_SCHWANGER_WERDEN_DAUER,
                Enums.EnumPauseStart.JETZT);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    //endregion
    ///////////////////////////////////////////////////////////////////////////////////////////////

    // RENDER UND SONSTIGES ///////////////////////////////////////////////////////////////////////

    @Override
    public void renderOverride(SpriteBatch spriteBatch, List<StatischesSymbol> symbolListe) {
        if (verhungert) {
            symbolListe.add(IdsSymbole.SYMBOL_VERHUNGERT);
        }
        if (zuhause == null) {
            symbolListe.add(IdsSymbole.SYMBOL_OBDACHLOS);
        }

        if (isAusgewaehlt()) {
            TextureUtils.zeichneTexturRegion(spriteBatch,
                    Assets.instance.tiledMapAssets.durchsichtigWeiss, getPosition(), false);
        }

        float vergangeneLaufenZeit = MyTimeUtils.secondsSince(laufenStartZeit);
        TextureRegion region;
        boolean zeichneGespiegelt = false;
        if (enumLaufen == Enums.EnumLaufen.LAUFEN) {
            if (enumRichtung == Enums.EnumRichtung.OBEN) {
                region = (TextureRegion) Assets.instance.dorfbewohnerAAssets
                        .obenLaufenAnimation.getKeyFrame(vergangeneLaufenZeit);
            } else if (enumRichtung == Enums.EnumRichtung.LINKS
                    || enumRichtung == Enums.EnumRichtung.LINKS_OBEN
                    || enumRichtung == Enums.EnumRichtung.LINKS_UNTEN) {
                region = (TextureRegion) Assets.instance.dorfbewohnerAAssets
                        .seiteLaufenAnimation.getKeyFrame(vergangeneLaufenZeit);
                zeichneGespiegelt = true;
            } else if (enumRichtung == Enums.EnumRichtung.RECHTS
                    || enumRichtung == Enums.EnumRichtung.RECHTS_OBEN
                    || enumRichtung == Enums.EnumRichtung.RECHTS_UNTEN) {
                region = (TextureRegion) Assets.instance.dorfbewohnerAAssets
                        .seiteLaufenAnimation.getKeyFrame(vergangeneLaufenZeit);
            } else {
                region = (TextureRegion) Assets.instance.dorfbewohnerAAssets
                        .untenLaufenAnimation.getKeyFrame(vergangeneLaufenZeit);
            }
        } else {
            region = Assets.instance.dorfbewohnerAAssets.dorfbewohnerStehen;
        }
        TextureUtils.zeichneTexturRegion(spriteBatch, region, getPosition(), zeichneGespiegelt);
    }

    @Override
    public void loesche() {
        super.loesche();
        berufKuendigen();
    }

    @Override
    protected void loescheAusSpeziellenListen() {
        getLevel().getWorldInputProcessor().remove(this);

        if (familie.getFamilienmitglieder().size() == 1) {
            int erbe = familie.remove(this, true, null);
            dorf.getGeld().mengeErhoehen(erbe, false);
        } else {
            familie.remove(this, false, null);
        }

        Eigentuemer eigentuemer;
        if (familie.getFamilienmitglieder().isEmpty()) {
            eigentuemer = dorf;
        } else {
            eigentuemer = familie; //TODO Familie als Besitzer testen
        }
        for (Gegenstand g : getGegenstandListe()) {
            BodenStapel bs = new BodenStapel(getLevel(), getRenderArrayList(),
                    getDorf().getBodenStapelListe(), g.gettId(), eigentuemer,
                    getLevel().getWorldMap().findeFreieZelle(getPositionMitte(), true, false));
            bs.getGegenstandListe().get(0).mengeErhoehen(g.gettMenge(true, false, true), false);
            getDorf().getBodenStapelListe().add(bs);
        }
    }

    @Override
    public boolean issHit(Vector2 clickWorldPosition) {
        if (!isVisible()) {
            return false;
        }
        float x = clickWorldPosition.x;
        float y = clickWorldPosition.y;
        return x >= getPosition().x && x < getRight() && y >= getPosition().y && y < getTop();
    }

    private float updateFitness(float fitness) {
        if (beruf != null) {
            float berufFitness = beruf.getOptimaleDBProduktivitaetKoerper();
            fitness += (berufFitness - fitness) / 100; //TODO Konstanten
        }
        fitness += 0.01f * (Math.pow(streckeGelaufen, (1 - fitness))
                / (konstantenBewegungsgeschwindigkeit
                * KonstantenBalancing.DB_FITNESS_PROZENT_LEBENSZEIT_LAUFEN
                * KonstantenBalancing.DORFBEWOHNER_PAUSE_KONSUMIEREN) //TODO Was soll das hier????
                - 1);
        fitness = Math.max(Math.min(fitness, 1), 0);
        streckeGelaufen = 0;
        return fitness;
    }

    // EINKOMMEN //////////////////////////////////////////////////////////////////////////////////

    public int einkommen(Enums.EnumZeit enumZeit) {
        if (beruf != null) {
            int gehalt_sekunde = beruf.getGehalt();
            switch (enumZeit) {
                case REAL_SEKUNDEN:
                    return gehalt_sekunde;
                case INGAME_MONAT:
                    return (int) (gehalt_sekunde * KonstantenBalancing.SEKUNDEN_PRO_MONAT);
                case INGAME_JAHR:
                    return (int) (gehalt_sekunde * KonstantenBalancing.SEKUNDEN_PRO_JAHR);
            }
            throw new IllegalStateException();
        } else {
            return 1000; //TODO Arbeitslosengeld und Geld fuer gelaufene Wege berechnen
        }
    }

    public int kostenLaufen(Vector2 startPosition, Vector2 zielPosition) {
        return (int) (GeometryUtils.hypothenuseIsoStrecke(startPosition, zielPosition)
                / konstantenBewegungsgeschwindigkeit * einkommen());
        // TODO Gehalt ueberall in selber zeiteinheit angeben. Ich glaube es sind aktuell Sekunden??!
        // TODO Im Fall fuer Beruf anpassen, da der Arbeitgebwer weniger zahlt als der DB bekommt
    }

    // POSIIONEN //////////////////////////////////////////////////////////////////////////////////

    @Override
    protected Vector2 positionZuPositionMitte(Vector2 position) {
        return new Vector2(position.x + KonstantenOptik.TILE_ISO_WIDTH / 2,
                position.y + KonstantenOptik.TILE_ISO_HEIGHT  / 2);
    }

    @Override
    protected Vector2 positionMitteZuPosition(Vector2 positionMitte) {
        return new Vector2(positionMitte.x - KonstantenOptik.TILE_ISO_WIDTH  / 2,
                positionMitte.y - KonstantenOptik.TILE_ISO_HEIGHT / 2);
    }

    // LAUFEN /////////////////////////////////////////////////////////////////////////////////////

    public void erhoehStreckeGelaufen(float strecke) {
        streckeGelaufen += strecke;
    }

    public void laufenAnfangen() {
        laufenStartZeit = TimeUtils.nanoTime();
    }

    public void laufenBeenden() {
        laufenStartZeit = TimeUtils.nanoTime();
        this.enumLaufen = Enums.EnumLaufen.STEHEN;
    }

    public void settEnumRichtung(Enums.EnumRichtung enumRichtung) {
        this.enumLaufen = Enums.EnumLaufen.LAUFEN;
        this.enumRichtung = enumRichtung;
    }

    // SCHADEN ////////////////////////////////////////////////////////////////////////////////////

    public void schadenZufuegen(float sekunden, float[] schadenArray) {
        schadenZufuegen(new Schaden(sekunden * schadenArray[0], schadenArray[1], schadenArray[2],
                schadenArray[3], schadenArray[4], schadenArray[5]));
    }

    public void schadenZufuegen(Schaden schaden) {
        gesundheit -= werkzeugTasche.schuetzen(schaden);
    }

    // GEBAUDE BETRETEN UND VERLASSEN /////////////////////////////////////////////////////////////

    public void preufeVisible() {
        IngameKamera ingameKamera = getLevel().getIngameKamera(); //TODO irgendwie getten
        ingameKamera.preufeVisible(this);
    }

    public void gebaudeBetreten(Gebaude gebaudeBetreten) {
        if (!KonstantenBalancing.DEVELOPER) {
            befindetSichInGebaude = gebaudeBetreten;
            getRenderArrayList().remove(this); //visible wuerde wieder auf true gesetzt wenn sich die Kamera bewegt
            befindetSichInGebaude.tuerVerwenden();
        }
    }

    public void gebaudeVerlassen() {
        if (!KonstantenBalancing.DEVELOPER) {
            if (befindetSichInGebaude != null) {
                getRenderArrayList().add(this, true);
                befindetSichInGebaude.tuerVerwenden();
            }
            befindetSichInGebaude = null;
        }
    }

    // TASCHEN UND NUTZEN /////////////////////////////////////////////////////////////////////////

    public int gettNutzen(Gegenstand gegenstand) {
        int preis = gegenstand.getWert();
        Tasche tasche = SpecialUtils.passendeTasche(this, gegenstand);
        int grenznutzen = tasche.gettGrenznutzen(gegenstand.gettId());
        return grenznutzen - preis;
    }

    // KOMPLEXE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    private boolean issSingle() {
        return (!issParent() || (familie.getParent1() == this && familie.getParent2() == null)
                || (familie.getParent2() == this && familie.getParent1() == null));
    }

    private boolean issParent() {
        return (familie.getParent1() == this || familie.getParent2() == this);
    }

    public float gettAlter() {
        return alter_sekunden / KonstantenBalancing.SEKUNDEN_PRO_JAHR;
    }

    protected void settNachname(String nachname) {
        setName(vorname + " " + nachname);
    }

    public Zelle gettZelle() {
        Zelle zelle = getLevel().getWorldMap().gett(getPositionMitte());
        if (zelle == null) {
            throw new IllegalArgumentException("x: "+getPositionMitte().x
                    +", y: "+getPositionMitte().y);
        }
        return zelle;
    }

    @Override
    public DorfbewohnerGegenstandListe getGegenstandListe() {
        return (DorfbewohnerGegenstandListe) super.getGegenstandListe();
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    //region EINFACHE GETTER UND SETTER
    ///////////////////////////////////////////////////////////////////////////////////////////////

    public void setFamilie(Familie familie) {
        this.familie = familie;
    }

    public float getSchlaf() {
        return schlaf;
    }

    public void setGebBeruf(GebBeruf beruf) {
        this.gebBeruf = beruf;
    }

    public Dorf getDorf() {
        return dorf;
    }

    public void setDorf(Dorf dorf) {
        this.dorf = dorf;
    }

    public void setSchlaf(int schlaf) {
        this.schlaf = schlaf;
    }

    public Enums.EnumDbStates getEnumDbStates() {
        return enumDbStates;
    }

    public GebBeruf getGebBeruf() {
        return gebBeruf;
    }

    public StackFiniteStepMachine getStackFiniteStepMachine() {
        return stackFiniteStepMachine;
    }

    public int getSchlafMax() {
        return schlafMax;
    }

    public int getKonstantenSchlafAuffuellGeschwindigkeit() {
        return konstantenSchlafAuffuellGeschwindigkeit;
    }

    public float getProduktivitaetKopf() {
        return produktivitaetKopf;
    }

    public float getProduktivitaetKoerper() {
        return produktivitaetKoerper;
    }

    public Beruf getBeruf() {
        return beruf;
    }

    public void setBeruf(Beruf beruf) {
        this.beruf = beruf;
    }

    public Familie getFamilie() {
        return familie;
    }

    public boolean isFemale() {
        return geschlechtFemale;
    }

    public float getKonstantenNutzenExponentNahrung() {
        return konstantenNutzenExponentNahrung;
    }

    public float getKonstantenNutzenExponentSpielzeugtasche() {
        return konstantenNutzenExponentSpielzeugtasche;
    }

    public float getKonstantenNutzenExponentWohnen() {
        return konstantenNutzenExponentWohnen;
    }

    public float getStreckeGelaufen() {
        return streckeGelaufen;
    }

    public float getBildung() {
        return bildung;
    }

    public float getFitness() {
        return fitness;
    }

    public Float getGesundheit() {
        return gesundheit;
    }

    public NahrungTasche getNahrungsTasche() {
        return nahrungsTasche;
    }

    public void setNahrungsTasche(NahrungTasche nahrungsTasche) {
        this.nahrungsTasche = nahrungsTasche;
    }

    public SpielzeugTasche getSpielzeugTasche() {
        return spielzeugTasche;
    }

    public void setSpielzeugTasche(SpielzeugTasche spielzeugTasche) {
        this.spielzeugTasche = spielzeugTasche;
    }

    public GebaudeHaus getZuhause() {
        return zuhause;
    }

    public void setZuhause(GebaudeHaus zuhause) {
        this.zuhause = zuhause;
    }

    public float getDnaProduktivtiaetKopf() {
        return dnaProduktivtiaetKopf;
    }

    public float getDnaProduktivitaetKoerper() {
        return dnaProduktivitaetKoerper;
    }

    public String getVorname() {
        return vorname;
    }

    public List<WorldActor> getKeinPfadListe() {
        return keinPfadListe;
    }

    public boolean isFreeze() {
        return freeze;
    }

    public void setFreeze(boolean freeze) {
        this.freeze = freeze;
    }

    public WerkzeugTasche getWerkzeugTasche() {
        return werkzeugTasche;
    }

    public void setWerkzeugTasche(WerkzeugTasche werkzeugTasche) {
        this.werkzeugTasche = werkzeugTasche;
    }

    public MedizinTasche getMedizinTasche() {
        return medizinTasche;
    }

    public void setMedizinTasche(MedizinTasche medizinTasche) {
        this.medizinTasche = medizinTasche;
    }

    public int getKonstantenBewegungsgeschwindigkeit() {
        return konstantenBewegungsgeschwindigkeit;
    }

    public Geld getGeld() {
        return geld;
    }

    public void setGeld(Geld geld) {
        this.geld = geld;
    }

    public Gebaude getBefindetSichInGebaude() {
        return befindetSichInGebaude;
    }
    //endregion
}
