package de.arnonym.strategiespiel.spielSchirm.objekte.dorfbewohnerAi.berufe;

import java.io.Serializable;

import de.arnonym.strategiespiel.spielSchirm.Dorf;
import de.arnonym.strategiespiel.spielSchirm.objekte.dorfbewohnerAi.Dorfbewohner;
import de.arnonym.strategiespiel.spielSchirm.objekte.kollisionsobjekte.gebaude.gebBeruf.GebBeruf;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.MathUtils;
import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenBalancing;

public abstract class Beruf implements Serializable {
    private Dorf dorf;
    private GebBeruf gebaudeBeruf;
    private BerufListe berufListe;

    private final float optimaleDBProduktivitaetKopf; //DB = Dorfbewohner //Intelligenz wird nicht durch den Beruf verringert oder erhoeht sondenr durch die Schule. TODO koennte man ueberdenken
    private final float optimaleDBProduktivitaetKoerper; //Koerper, also fitness wird durch Beruf veraendert und passt sich diesem langfristig an (nur fitness. Nicht die gesamte physische Verpasseung
    private Float produktivitaet;

    private Dorfbewohner dorfbewohner = null;
    private int gehalt = 0;

    /* WICHTIG!! Bei jeder Produktivitaet muss von 1 DB mit 1 Gehalt,
    in (gezahlter) hoehe von G, 1 Einheit erzeugt werden können.
    Die Einzelteile muessen fuer E gekauft werden koennen.
    Das Produkt muss fuer Kosten von L geliefert und fuer (G+L+K)*(1+gewinn) verkauft werden
    koennen WICHTIG!! */

    // BESCHREIBEN //
    public Beruf(float optimaleDBProduktivitaetKopf, float optimaleDBProduktivitaetKoerper) {
        this.optimaleDBProduktivitaetKopf = optimaleDBProduktivitaetKopf;
        this.optimaleDBProduktivitaetKoerper = optimaleDBProduktivitaetKoerper;
    }

    // KOPIEREN //
    protected Beruf(Beruf beruf) {
        this.optimaleDBProduktivitaetKopf = beruf.optimaleDBProduktivitaetKopf;
        this.optimaleDBProduktivitaetKoerper = beruf.optimaleDBProduktivitaetKoerper;
    }

    // INITIALISIEREN //
    protected void init_Beruf(Dorf dorf, GebBeruf gebaudeBeruf, BerufListe berufListe) {
        this.dorf = dorf;
        this.gebaudeBeruf = gebaudeBeruf;
        this.berufListe = berufListe;
    }

    // CREATE //
    public abstract Beruf create(Dorf dorf, GebBeruf gebaudeBeruf, BerufListe berufListe);

    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////

    protected void update() {
        produktivitaet = null; //Wir hier resettet und nur errechnet wenn noetig
        if (dorfbewohner != null) {
            gehalt = gehalt();
            dorfbewohner.sucheBeruf();
        }
    }

    // GEHALT /////////////////////////////////////////////////////////////////////////////////////

    public int getGehalt() {
        return gehalt;
    }

    private int gehalt() {
        return gehalt(dorfbewohner);
    }

    public abstract int gehalt(Dorfbewohner dorfbewohner);

    public void zahleGehalt(float arbeitszeit) {
        int gehaltZahlung = MathUtils.min((int) (gehalt * arbeitszeit),
                gebaudeBeruf.getGeld().getMengeVerfuegbar());
        gebaudeBeruf.getGeld().mengeVerringern(gehaltZahlung, false);
        dorfbewohner.getGeld().mengeErhoehen((int)(gehaltZahlung
                * KonstantenBalancing.GEHALT_GELD_ERSCHAFFEN_MULTIPLIKATOR), false);
    }

    // PRODUKTIVITAET /////////////////////////////////////////////////////////////////////////////

    public float gettProduktivitaet() {
        return gettProduktivitaet(dorfbewohner);
    }

    public float gettProduktivitaet(Dorfbewohner dorfbewohner) {
        if (this.dorfbewohner == dorfbewohner && produktivitaet != null) {
            return produktivitaet;
        }

        float dbProdKopf = dorfbewohner.getProduktivitaetKopf();
        float dbProdKoerper = dorfbewohner.getProduktivitaetKoerper();
        float prodKopf, prodKoerper;
        if (dbProdKopf > optimaleDBProduktivitaetKopf) {
            prodKopf = (float) Math.pow(optimaleDBProduktivitaetKopf / dbProdKopf, 0.5f); //TODO Konstante //Ueberqualifikation soll sich nicht so stark auswirken
        } else {
            prodKopf = dbProdKopf / optimaleDBProduktivitaetKopf;
        }
        if (dbProdKoerper > optimaleDBProduktivitaetKopf) {
            prodKoerper = (float) Math.pow(optimaleDBProduktivitaetKoerper / dbProdKoerper, 0.5f); //TODO Konstante //Ueberqualifikation soll sich nicht so stark auswirken
        } else {
            prodKoerper = dbProdKoerper / optimaleDBProduktivitaetKoerper;
        }

        if (this.dorfbewohner == dorfbewohner) {
            return produktivitaet = prodKopf * prodKoerper;
        }
        return prodKopf * prodKoerper;
    }

    // ANNEHMEN UND KUENDIGEN /////////////////////////////////////////////////////////////////////

    public void annehmen(Dorfbewohner dorfbewohner, int gehalt) {
        if (this.dorfbewohner != dorfbewohner) {
            this.dorfbewohner = dorfbewohner;
            gebaudeBeruf.einstellen(dorfbewohner);
        }
        this.gehalt = gehalt; //Gehalt wurde bei der Suche nach dem Beruf bereits ausgerechnet
    }

    public void kuendigen() {
        gebaudeBeruf.kuendigen(dorfbewohner);
        this.dorfbewohner = null;
    }

    // PROTECTED GETTER UND SETTER ////////////////////////////////////////////////////////////////

    protected Dorf getDorf() {
        return dorf;
    }

    protected BerufListe getBerufListe() {
        return berufListe;
    }

    // EINFACHE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public Dorfbewohner getDorfbewohner() {
        return dorfbewohner;
    }
    public void setDorfbewohner(Dorfbewohner dorfbewohner) {
        this.dorfbewohner = dorfbewohner;
    }

    public float getOptimaleDBProduktivitaetKoerper() {
        return optimaleDBProduktivitaetKoerper;
    }

    public GebBeruf getGebaudeBeruf() {
        return gebaudeBeruf;
    }

    public float getOptimaleDBProduktivitaetKopf() {
        return optimaleDBProduktivitaetKopf;
    }
}
