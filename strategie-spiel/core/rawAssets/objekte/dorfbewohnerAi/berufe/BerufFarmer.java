package de.arnonym.strategiespiel.spielSchirm.objekte.dorfbewohnerAi.berufe;

import de.arnonym.strategiespiel.spielSchirm.Dorf;
import de.arnonym.strategiespiel.spielSchirm.objekte.dorfbewohnerAi.Dorfbewohner;
import de.arnonym.strategiespiel.spielSchirm.objekte.kollisionsobjekte.gebaude.gebBeruf.GebBeruf;
import de.arnonym.strategiespiel.spielSchirm.zahlen.wirtschaftsdaten.WirtschaftsdatenManager;

public class BerufFarmer extends Beruf {
    // BESCHREIBEN //
    public BerufFarmer(float optimaleDBProduktivitaetKopf, float optimaleDBProduktivitaetKoerper) {
        super(optimaleDBProduktivitaetKopf, optimaleDBProduktivitaetKoerper);
    }

    // KOPIEREN //
    private BerufFarmer(BerufFarmer berufFarmer) {
        super(berufFarmer);
    }

    // INITIALISIEREN //
    private void init_BerufFarmer(Dorf dorf, GebBeruf gebaudeBeruf, BerufListe berufListe) {
        init_Beruf(dorf, gebaudeBeruf, berufListe);
    }

    // CREATE //
    @Override
    public BerufFarmer create(Dorf dorf, GebBeruf gebaudeBeruf, BerufListe berufListe) {
        BerufFarmer berufFarmer = new BerufFarmer(this);
        berufFarmer.init_BerufFarmer(dorf, gebaudeBeruf, berufListe);
        return berufFarmer;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public int gehalt(Dorfbewohner dorfbewohner) {
        return 1000; //TODO Mit WirtDatManager aus Einnahmen und Ausgaben der letzten Perioden errechnen
    }
}
