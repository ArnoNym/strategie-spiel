package de.arnonym.strategiespiel.spielSchirm.objekte.dorfbewohnerAi.berufe;

import java.util.Map;

import de.arnonym.strategiespiel.spielSchirm.Dorf;
import de.arnonym.strategiespiel.spielSchirm.objekte.dorfbewohnerAi.Dorfbewohner;
import de.arnonym.strategiespiel.spielSchirm.objekte.kollisionsobjekte.gebaude.Gebaude;
import de.arnonym.strategiespiel.spielSchirm.objekte.kollisionsobjekte.gebaude.GebaudeLager;
import de.arnonym.strategiespiel.spielSchirm.objekte.kollisionsobjekte.gebaude.gebBeruf.GebBeruf;
import de.arnonym.strategiespiel.spielSchirm.objekte.kollisionsobjekte.gebaude.gebBeruf.GebBerufHerstFabrik;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.Gegenstand;
import de.arnonym.strategiespiel.spielSchirm.zahlen.wirtschaftsdaten.WirtschaftsdatenManager;
import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenBalancing;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.MathUtils;

public class BerufFabrikArbeiter extends Beruf {
    // BESCHREIBEN //
    public BerufFabrikArbeiter(float optimaleDBProduktivitaetKopf,
                               float optimaleDBProduktivitaetKoerper) {
        super(optimaleDBProduktivitaetKopf, optimaleDBProduktivitaetKoerper);
    }

    // KOPIEREN //
    private BerufFabrikArbeiter(BerufFabrikArbeiter berufFabrikArbeiter) {
        super(berufFabrikArbeiter);
    }

    // INITIALISIEREN //
    private void init_BerufFabrikArbeiter(Dorf dorf, GebBeruf gebaudeBeruf, BerufListe berufListe) {
        init_Beruf(dorf, gebaudeBeruf, berufListe);
    }

    // CREATE //
    @Override
    public Beruf create(Dorf dorf, GebBeruf gebaudeBeruf, BerufListe berufListe) {
        BerufFabrikArbeiter berufFabrikArbeiter = new BerufFabrikArbeiter(this);
        berufFabrikArbeiter.init_BerufFabrikArbeiter(dorf, gebaudeBeruf, berufListe);
        return berufFabrikArbeiter;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////

    public int gehalt(Dorfbewohner dorfbewohner) {
        GebBerufHerstFabrik gebBeruf = (GebBerufHerstFabrik) getGebaudeBeruf();
        int prodId = gebBeruf.getProduktId();
        Gegenstand prod = gebBeruf.gettHerstellen();

        // Verkauf (Gewinne durch Verkaufshandlung)
        int maxVerkaufGewinn = -Integer.MAX_VALUE;
        int ppEerloes = 0; // pp = ProProdukt, Ges = Gesamt
        int ppAuslieferKosten = 0;
        for (Gebaude g : getDorf().getGebaudeListe()) {
            if (g instanceof GebaudeLager) {
                int e = g.getGegenstandListe().getDurchId(prodId).getWert();
                int ak = g.getWirtschaftsdatenManager().gettDurchschnLieferKosten(
                        prodId, KonstantenBalancing.ZIVIL_OBJEKT_BETRACHTE_PERIODEN);
                int verkaufGewinn = e - ak;
                if (verkaufGewinn > maxVerkaufGewinn) {
                    maxVerkaufGewinn = verkaufGewinn;
                    ppEerloes = e;
                    ppAuslieferKosten = ak;
                }
            }
        }

        if (ppEerloes - ppAuslieferKosten
                * KonstantenBalancing.ZIVIL_OBJEKT_MIN_GEWINN_MULT <= 0) {
            return 0;
        }

        // Material AnschaffungsKosten (Liefern und Kaufen)
        int gesMaterialAnschaffungKosten = 0; // pp = ProProdukt, Ges = Gesamt
        WirtschaftsdatenManager wirtDatManager = gebBeruf.getWirtschaftsdatenManager();
        for (Map.Entry<Integer, Integer> entry : prod.gettInputHashMap().entrySet()) {
            int id = entry.getKey();
            int menge = entry.getValue();

            int durchschnMatAnschKost = wirtDatManager.gettDurchschnKaufKosten(id,
                    KonstantenBalancing.ZIVIL_OBJEKT_BETRACHTE_PERIODEN)
                    + wirtDatManager.gettDurchschnLieferKosten(id,
                    KonstantenBalancing.ZIVIL_OBJEKT_BETRACHTE_PERIODEN);
            gesMaterialAnschaffungKosten += MathUtils.myIntMult(durchschnMatAnschKost, menge);
        }

        int ppMaxGehalt = (int) ((ppEerloes - (ppAuslieferKosten + gesMaterialAnschaffungKosten)
                * KonstantenBalancing.ZIVIL_OBJEKT_MIN_GEWINN_MULT)
                / getBerufListe().getGehaltMarktlageMult());
        if (ppMaxGehalt <= 0) {
            return 0;
        }

        float ppProduktionsdauer = getBerufListe().getStandardProduktionsdauer()
                / gettProduktivitaet(dorfbewohner);

        int maxGehalt = Math.min((int) (ppMaxGehalt / ppProduktionsdauer),
                gebBeruf.getGeld().getMengeVerfuegbar());

        return maxGehalt; //TODO Beruf zahlt nicht mehr als er muss?!?
    }
}
