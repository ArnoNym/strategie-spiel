package de.arnonym.strategiespiel.spielSchirm.objekte.dorfbewohnerAi.berufe;

import de.arnonym.strategiespiel.spielSchirm.Dorf;
import de.arnonym.strategiespiel.spielSchirm.objekte.dorfbewohnerAi.Dorfbewohner;
import de.arnonym.strategiespiel.spielSchirm.objekte.kollisionsobjekte.gebaude.gebBeruf.GebBeruf;

public class BerufLehrer extends Beruf {
    // BESCHREIBEN //
    public BerufLehrer(float optimaleDBProduktivitaetKopf, float optimaleDBProduktivitaetKoerper) {
        super(optimaleDBProduktivitaetKopf, optimaleDBProduktivitaetKoerper);
    }

    // KOPIEREN //
    private BerufLehrer(BerufLehrer berufLehrer) {
        super(berufLehrer);
    }

    // INITIALISIEREN //
    private void init_BerufLehrer(Dorf dorf, GebBeruf gebaudeBeruf, BerufListe berufListe) {
        init_Beruf(dorf, gebaudeBeruf, berufListe);
    }

    // CREATE //
    @Override
    public BerufLehrer create(Dorf dorf, GebBeruf gebaudeBeruf, BerufListe berufListe) {
        BerufLehrer berufLehrer = new BerufLehrer(this);
        berufLehrer.init_BerufLehrer(dorf, gebaudeBeruf, berufListe);
        return berufLehrer;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public int gehalt(Dorfbewohner dorfbewohner) {
        return 1000; //TODO
    }
}
