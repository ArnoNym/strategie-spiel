package de.arnonym.strategiespiel.spielSchirm.objekte.dorfbewohnerAi.berufe;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import de.arnonym.strategiespiel.spielSchirm.Dorf;
import de.arnonym.strategiespiel.spielSchirm.objekte.kollisionsobjekte.gebaude.gebBeruf.GebBeruf;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.Gegenstand;
import de.arnonym.strategiespiel.framework.werkzeuge.Enums;
import de.arnonym.strategiespiel.framework.werkzeuge.PauseDelta;
import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenBalancing;

public class BerufListe extends ArrayList<Beruf> {
    private List<Beruf> berufeSauberListe = new ArrayList<>();
    private Dorf dorf;
    private GebBeruf gebaudeBeruf;

    private float gehaltMarktlageMult = 1;
    private float standardProduktionsdauer;
    private PauseDelta updatePause = new PauseDelta(KonstantenBalancing.GROSSE_UPDATE_PAUSE,
            Enums.EnumPauseStart.JETZT_ZUFAELLIGE_RESTDAUER);
    private int auslastung = 1; //Bedeutet 1 mal alle Berufe in der berufeSauberListe

    // BESCHREIBEN //
    public BerufListe(Beruf... berufe) {
        berufeSauberListe.addAll(Arrays.asList(berufe)); //Berufe zu this werden in neueBerufe() hinzugefuegt
        neuerBerufe();
    }

    // CREATE //
    public BerufListe create(Dorf dorf, GebBeruf gebaudeBeruf) {
        BerufListe neueBerufListe = new BerufListe();
        for (Beruf b : this) {
            neueBerufListe.add(b.create(dorf, gebaudeBeruf, neueBerufListe));
        }
        neueBerufListe.dorf = dorf;
        neueBerufListe.gebaudeBeruf = gebaudeBeruf;
        return neueBerufListe;
    }

    public void update(float delta_NL) {
        if (!updatePause.issVorbei(delta_NL, true)) {
            return;
        }
        gehaltMarktlageMult = gettGehaltMarktlageMult(gehaltMarktlageMult);
        for (Beruf b : this) {
            b.update();
        }
    }

    // SONSTIGES //////////////////////////////////////////////////////////////////////////////////

    public void aenderungAktuellesProdukt(Gegenstand neuesProdukt) {
        standardProduktionsdauer = neuesProdukt.gettProduktidauer();
        updatePause.settVorbei();
        update(0);
    }

    // ANZAHL BERUFE //////////////////////////////////////////////////////////////////////////////

    public void neuerBerufe() {
        for (Beruf b : berufeSauberListe) {
            this.add(b.create(dorf, gebaudeBeruf, this));
        }
    }

    // KOMPLEXE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    private int gettFreieStellen() {
        int freieStellen = 0;
        for (Beruf b : this) {
            if (b.getDorfbewohner() == null) {
                freieStellen += 1;
            }
        }
        return freieStellen;
    }

    private float gettGehaltMarktlageMult(float gehaltMarktlageMult) {
        // Verhaeltnis von Gehalt zu Produktivitaet zu Preis des produzierten Gutes muss stimmen
        int arbeitslose = dorf.gettAnzahlArbeitslose();
        int freieStellen = gettFreieStellen();

        if (arbeitslose <= 0) { //Keine Arbeitslosen
            if (freieStellen <= 0) { //Keine freien Stellen
                gehaltMarktlageMult -= 0.0001; //TODO Konstante //Leichte verringerung des Gehalts bis die Mitarbeiter keinen Bock mehr haben
            }
        } else { //Arbeitslose
            if (freieStellen > 0) { //Freie Stellen
                gehaltMarktlageMult += 0.01; //TODO Konstante //Gehalt erhoehen bis jemand den Job annimmt
            } else { //Keine freien Stellen
                gehaltMarktlageMult -= 0.1 * arbeitslose/dorf.gettAnzahlDorfbewohner(); //TODO Konstante //Gehalt staerker verringern, solange sich aus den Arbeitslosen immer neue Mitarbeiter rekrutieren lassen
            }
        }
        return Math.min(1, gehaltMarktlageMult); //Damit das Unternhemen keine Verluste macht
    }

    // PROTECTED GETTER UND SETTER ////////////////////////////////////////////////////////////////

    protected float getStandardProduktionsdauer() {
        return standardProduktionsdauer;
    }

    protected float getGehaltMarktlageMult() {
        return gehaltMarktlageMult;
    }

    // EINFACHE GETTER UND SETTER /////////////////////////////////////////////////////////////////
}
