package de.arnonym.strategiespiel.spielSchirm.objekte.dorfbewohnerAi;

import com.badlogic.gdx.math.Vector2;

import java.util.List;

import de.arnonym.strategiespiel.spielSchirm.objekte.WorldActor;
import de.arnonym.strategiespiel.spielSchirm.objekte.kollisionsobjekte.StatischesObjekt;
import de.arnonym.strategiespiel.framework.werkzeuge.Enums;
import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenBalancing;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.GeometryUtils;

public class DorfbewohnerUtils {

    public static float gettMaxBildung(Enums.EnumBildungsgrad bildungsgrad) {
        switch (bildungsgrad) {
            case UNGEBILDET: return KonstantenBalancing.BILDUNG_UNGEBILDET;
            case GRUNDSCHULE: return KonstantenBalancing.BILDUNG_GRUNDSCHULE;
            case WEITERFUEHRENDE_SCHULE: return KonstantenBalancing.BILDUNG_WEITERFUEHRENDE_SCHULE;
            case UNIVERSITAET: return KonstantenBalancing.BILDUNG_UNIVERSITAET;
        }
        throw new IllegalStateException("Funktion um neuen Bildungsgrad erweitern!");
    }

    public static Enums.EnumBildungsgrad gettBildungsgrad(float bildung) {
        if (bildung <= KonstantenBalancing.BILDUNG_UNGEBILDET) {
            return Enums.EnumBildungsgrad.UNGEBILDET;
        } else if (bildung <= KonstantenBalancing.BILDUNG_GRUNDSCHULE) {
            return Enums.EnumBildungsgrad.GRUNDSCHULE;
        } else if (bildung <= KonstantenBalancing.BILDUNG_WEITERFUEHRENDE_SCHULE) {
            return Enums.EnumBildungsgrad.WEITERFUEHRENDE_SCHULE;
        } else {
            return Enums.EnumBildungsgrad.UNIVERSITAET;
        }
    }
}
