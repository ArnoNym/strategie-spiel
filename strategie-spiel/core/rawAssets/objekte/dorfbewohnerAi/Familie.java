package de.arnonym.strategiespiel.spielSchirm.objekte.dorfbewohnerAi;

import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import de.arnonym.strategiespiel.spielSchirm.Dorf;
import de.arnonym.strategiespiel.spielSchirm.Level;
import de.arnonym.strategiespiel.spielSchirm.objekte.Eigentuemer;
import de.arnonym.strategiespiel.spielSchirm.ecsErgaenzung.stp.steps.Step;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dorfbewohner.tauschen.step.StepTauschen;
import de.arnonym.strategiespiel.spielSchirm.objekte.kollisionsobjekte.Eigentum;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.Gegenstand;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.GegenstandListe;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.Geld;
import de.arnonym.strategiespiel.framework.werkzeuge.collections.hashMaps.IntIntHashMap;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.taschen.MedizinTasche;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.taschen.NahrungTasche;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.taschen.WerkzeugTasche;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.taschen.SpielzeugTasche;
import de.arnonym.strategiespiel.spielSchirm.objekte.kollisionsobjekte.gebaude.gebBeruf.GebBeruf;
import de.arnonym.strategiespiel.spielSchirm.objekte.kollisionsobjekte.gebaude.GebaudeHaus;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterChild.FensterChildFamilie;
import de.arnonym.strategiespiel.framework.werkzeuge.Enums;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.GeometryUtils;
import de.arnonym.strategiespiel.framework.werkzeuge.PauseDelta;
import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenBalancing;

public class Familie extends Eigentuemer {
    private Dorf dorf;

    private Dorfbewohner parent1;
    private Dorfbewohner parent2;
    private List<Dorfbewohner> familienmitglieder = new ArrayList<>();
    private List<Dorfbewohner> kinder = new ArrayList<>();

    private GebaudeHaus zuhause = null;
    private final NahrungTasche nahrungsTasche;
    private final SpielzeugTasche spielzeugTasche;
    private final WerkzeugTasche werkzeugTasche;
    private final MedizinTasche medizinTasche;
    private float nutzen;
    private float produktivitaet;
    private float gesundheit;
    private PauseDelta pauseKonsumieren = new PauseDelta(
            KonstantenBalancing.DORFBEWOHNER_PAUSE_KONSUMIEREN,
            Enums.EnumPauseStart.JETZT_ZUFAELLIGE_RESTDAUER);

    private float nutzenExponentWohnen = 0;

    private String nachname = "null";

    private transient FensterChildFamilie ausklappenFenster;

    // Erstellen von Familien bei Beginn eines neuen Spiels
    public Familie(Level level, Dorf dorf, int initialeGeldmenge, String nachname,
                   List<Dorfbewohner> familienmitgliederListe) {
        this(dorf, initialeGeldmenge);
        this.nachname = nachname;

        for (Dorfbewohner d : familienmitgliederListe) {
            if (parent1 == null) {
                parent1 = d;
            } else if (parent2 == null) {
                parent2 = d;
            } else {
                kinder.add(d);
            }
            familienmitglieder.add(d);
            d.init(level, dorf, level.getRenderArrayList(), level.getWorldInputProcessor(), this,
                    new Vector2(500, 100));
        }

        updateTaschenUndNutzen();
        for (Dorfbewohner d : familienmitglieder) {
            d.setGeld(getGeld());
            d.setNahrungsTasche(nahrungsTasche);
            d.setSpielzeugTasche(spielzeugTasche);
            d.setWerkzeugTasche(werkzeugTasche);
            d.setMedizinTasche(medizinTasche);
        }
    }

    // Erstellen von neuen Familien im laufenden Spiel
    public Familie(Level level, Dorf dorf, int initialeGeldmenge) {
        this(dorf, initialeGeldmenge);
    }

    // Gemeinsamer COnstructor
    private Familie(Dorf dorf, int initialeGeldmenge) {
        super(initialeGeldmenge);
        this.dorf = dorf;

        GegenstandListe<Gegenstand> forschungGegenstandListe = dorf.getForschungGegenstandListe();
        this.nahrungsTasche = new NahrungTasche(forschungGegenstandListe);
        this.spielzeugTasche = new SpielzeugTasche(forschungGegenstandListe);
        this.werkzeugTasche = new WerkzeugTasche(forschungGegenstandListe, this);
        this.medizinTasche = new MedizinTasche(forschungGegenstandListe, this);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////


    public void update(float delta) {
        //Nahrung und Spielzeuge verbauchen
        if (pauseKonsumieren.issVorbei(delta, true)) {
            nahrungsTasche.verbauchen(familienmitglieder.size() - kinder.size()/2); //TODO Konstante. Kinder essen aktuell nur halb so viel wie ein Erwachsener
            spielzeugTasche.verbauchen(familienmitglieder.size() - kinder.size()/2);
            neuesZuhauseSuchen();
        }

        for (Dorfbewohner d : familienmitglieder) {
            boolean konsumPauseVorbei = pauseKonsumieren.issVorbei();

            d.update(delta, konsumPauseVorbei);

            if (konsumPauseVorbei) {
                nutzen = nahrungsTasche.gettNutzen() + spielzeugTasche.gettNutzen()
                        + medizinTasche.gettNutzen() + werkzeugTasche.gettNutzen();
                if (zuhause != null) {
                    nutzen += zuhause.bruttoNutzen(nutzenExponentWohnen);
                }
            }
        }
    }

    // TASCHEN ////////////////////////////////////////////////////////////////////////////////////

    private void updateTaschenUndNutzen() {
        int familieSize = familienmitglieder.size();

        // Taschen Max
        int platz = familieSize * KonstantenBalancing.TASCHE_PLATZ;
        nahrungsTasche.settMaxPlatz(platz);
        spielzeugTasche.settMaxPlatz(platz);
        werkzeugTasche.settMaxPlatz(platz);
        medizinTasche.settMaxPlatz(platz);

        // Nutzen Exponenten
        float nExpNahrung, nExpSpielzeug, nExpWerkzeug, nExpMedizin, nExpWohnen;
        nExpNahrung = nExpSpielzeug = nExpWerkzeug = nExpMedizin = nExpWohnen = 0;
        for (Dorfbewohner d : familienmitglieder) {
            DorfbewohnerGegenstandListe dbGegListe = d.getGegenstandListe();
            nExpNahrung = dbGegListe.getNutzenExponentNahrung();
            nExpSpielzeug = dbGegListe.getNutzenExponentSpielzeug();
            nExpWerkzeug = dbGegListe.getNutzenExponentWerkzeug();
            nExpMedizin = dbGegListe.getNutzenExponentMedizin();
            nExpWohnen = dbGegListe.getNutzenExponentWohnen();
        }
        nExpNahrung = nExpNahrung / familieSize;
        nExpSpielzeug = nExpSpielzeug / familieSize;
        nExpWerkzeug = nExpWerkzeug / familieSize;
        nExpMedizin = nExpMedizin / familieSize;
        nutzenExponentWohnen = nExpWohnen / familieSize;
        nahrungsTasche.setNutzenExponent(nExpNahrung);
        spielzeugTasche.setNutzenExponent(nExpSpielzeug);
        werkzeugTasche.setNutzenExponent(nExpWerkzeug);
        medizinTasche.setNutzenExponent(nExpMedizin);

        // Nutzen der einzelnen Gegenstaende
        IntIntHashMap familienNahrungNutzenHashMap = new IntIntHashMap();
        IntIntHashMap familienSpielzeugNutzenHashMap = new IntIntHashMap();
        for (int id : familienmitglieder.get(0).getGegenstandListe().getIds()) {
            int gegenstandFamilientNutzen = 0;
            for (Dorfbewohner d : familienmitglieder) {
                gegenstandFamilientNutzen += d.getGegenstandListe().getDurchId(id).getWert();
            }
            Gegenstand g = familienmitglieder.get(0).getGegenstandListe().getDurchId(id);
            if (g instanceof GegenstandNahrung) {
                familienNahrungNutzenHashMap.put(id, gegenstandFamilientNutzen / familieSize);
            } else if (!(g instanceof GegenstandWerkzeug || g instanceof GegenstandMedizin)){
                familienSpielzeugNutzenHashMap.put(id, gegenstandFamilientNutzen / familieSize);
            }
        }
        nahrungsTasche.settNutzenHashMap(familienNahrungNutzenHashMap);
        spielzeugTasche.settNutzenHashMap(familienSpielzeugNutzenHashMap);
    }

    // DB HINZUFUEGEN UND ENTFERNEN ///////////////////////////////////////////////////////////////

    public void zusammenfuegen(Dorfbewohner neuerPartner, Familie hinzukommendeFamilie) {
        GebaudeHaus merkeZuhause = zuhause;
        if (zuhause != null) {
            this.ausziehen();
        }
        addFamilienmitglied(neuerPartner, true);
        for (Dorfbewohner d : hinzukommendeFamilie.getKinder()) {
            addFamilienmitglied(d, false); //Updated auch Taschen etc
        }
        hinzukommendeFamilie.loeschen_familieNichtDBs(this);
        if (merkeZuhause != null) {
            this.einziehen(merkeZuhause);
        }
    }

    public void addFamilienmitglied(Dorfbewohner dorfbewohner, boolean parent) {
        dorfbewohner.setFamilie(this);
        dorfbewohner.settNachname(nachname);
        familienmitglieder.add(dorfbewohner);
        if (parent && parent1 == null) {
            parent1 = dorfbewohner;
        } else if (parent && parent2 == null) {
            parent2 = dorfbewohner;
        } else {
            kinder.add(dorfbewohner);
        }
        updateTaschenUndNutzen();
        dorfbewohner.setGeld(getGeld());
        dorfbewohner.setNahrungsTasche(nahrungsTasche);
        dorfbewohner.setSpielzeugTasche(spielzeugTasche);
        dorfbewohner.setWerkzeugTasche(werkzeugTasche);
        dorfbewohner.setMedizinTasche(medizinTasche);
        if (zuhause != null) {
            dorfbewohner.setZuhause(zuhause);
        }
    }

    public int remove(Dorfbewohner familienmitglied, boolean returnErbe, Familie neueFamilie_NL) {
        int geld = this.getGeld().getMengeVerfuegbar(); // TODO Geld Legen und Geld Nehmen!!!!??
        int erbe;
        if (familienmitglieder.size() > 1) {
            int erbeAnteile = 0;
            if (parent1 != null) erbeAnteile += 5; //TODO Konstante
            if (parent2 != null) erbeAnteile += 5; //TODO Konstante
            erbeAnteile += kinder.size() * 1; //TODO Konstante

            if (familienmitglied == parent1) {
                parent1 = null;
                erbe = geld * 5 / erbeAnteile; //TODO Konstante
            } else if (familienmitglied == parent2) {
                parent2 = null;
                erbe = geld * 5 / erbeAnteile; //TODO Konstante
            } else {
                kinder.remove(familienmitglied);
                erbe = geld * 1 / erbeAnteile; //TODO Konstante
            }
        } else {
            erbe = geld; //TODO Auch besitzt vererben!?
            dorf.getFamilienListe().remove(this);
        }

        familienmitglieder.remove(familienmitglied);
        if (familienmitglieder.isEmpty()) {
            loeschen_familieNichtDBs(neueFamilie_NL);
        }

        if (returnErbe) {
            this.getGeld().mengeVerringern(erbe, false);
            return erbe;
        }
        return 0;
    }

    // LOESCHEN ///////////////////////////////////////////////////////////////////////////////////

    public void loeschen_familieNichtDBs(Eigentuemer neuerBesizter_NL) {
        //Immer ausfuehren
        dorf.getFamilienListe().remove(this);
        if (zuhause != null) {
            this.ausziehen();
        }

        //In Interaktion mit neuem Besitzer ausfuehren
        ausziehen();
        if (neuerBesizter_NL == null) {
            neuerBesizter_NL = dorf;
        }
        Geld neuerBesizterGeld = neuerBesizter_NL.getGeld(); //Geld in den Steps austauschen (Ob fuer Gebaude die der Familie gehoeren oder ihren FamMitgliedern

        neuerBesizterGeld.add(getGeld());

        for (Eigentum b : getEigentumListe()) {
            b.settEigentuemer(neuerBesizter_NL);
            neuerBesizter_NL.getEigentumListe().add(b);
        }

        for (Step s : dorf.gettAlleSteps()) {
            if (s instanceof StepTauschen) {
                StepTauschen sgm = ((StepTauschen)s);
                if (sgm.getNehmenAusGeld() == getGeld()) {
                    sgm.setNehmenAusGeld(neuerBesizterGeld); //reservierenNehmen und reservierenLegen wird durch das uebergeben des geldes uebertragen
                } else if (sgm.getLegenInGeld() == getGeld()) {
                    sgm.setLegenInGeld(neuerBesizterGeld);
                }
            }
        }

        //In Interaktion mit neuem besitzer, der eine Familie ist ausfuehren
        if (!(neuerBesizter_NL instanceof Familie)) {
            return;
        }
        Familie neueFamilie = (Familie) neuerBesizter_NL;
        nahrungsTasche.mengeErhoehen(neueFamilie.nahrungsTasche);
        spielzeugTasche.mengeErhoehen(neueFamilie.spielzeugTasche);
    }

    // DURCHSCHNITTSWERTE ERRECHNEN ///////////////////////////////////////////////////////////////

    public int gettDurchschnGehalt(Enums.EnumZeit enumZeit) {
        int summeGehalt = 0;
        int anzahlArbeitenderPersonen = 0;
        for (Dorfbewohner d : familienmitglieder) {
            int einkommen = d.einkommen(enumZeit);
            if (einkommen > 0) { //Einkommen bedeutet, dass auch Arbeitslose ein Gehalt haben koennen. So soll es sein, da Werkzeuge auch zum abbauen etc verwendet werden
                summeGehalt += einkommen;
                anzahlArbeitenderPersonen += 1;
            }
        }
        return summeGehalt / anzahlArbeitenderPersonen;
    }

    // HAUSER UND WOHNEN //////////////////////////////////////////////////////////////////////////

    public void einziehen(GebaudeHaus haus) {
        haus.einziehen(this);
        zuhause = haus;
        for (Dorfbewohner d : familienmitglieder) {
            d.setZuhause(haus);
        }
    }

    public void ausziehen() {
        if (zuhause == null) return;
        zuhause.ausziehen(this);
        zuhause = null;
        for (Dorfbewohner d : familienmitglieder) {
            d.setZuhause(null);
        }
    }

    private GebaudeHaus maxNutzenZuhause() {
        List<GebaudeHaus> hauserListe = (List) dorf.getGebaudeListe().stream()
                .filter(g -> g instanceof GebaudeHaus && ((GebaudeHaus)g).verfuegbar())
                .collect(Collectors.toList());

        float maxNutzen = 0;
        GebaudeHaus maxNutzenHaus = null;

        for (GebaudeHaus haus : hauserListe) {
            float laufwege = 0;
            for (Dorfbewohner d : familienmitglieder) {
                GebBeruf gebaudeBeruf = d.getGebBeruf();
                if (gebaudeBeruf != null) {
                    laufwege += GeometryUtils.hypothenuseIsoStrecke(haus.getPosition(), gebaudeBeruf.getPosition());
                }
            }

            float intrinisscherNutzen = haus.bruttoNutzen(nutzenExponentWohnen, familienmitglieder.size())
                    - haus.getPreisMiete();
            float nutzen = intrinisscherNutzen - laufwege;

            if (zuhause != null && haus != zuhause) {
                nutzen *= KonstantenBalancing.DB_UMZUGSKOSTEN;
            }

            if (intrinisscherNutzen > 0 && nutzen > maxNutzen) {
                maxNutzen = nutzen;
                maxNutzenHaus = haus;
            }
        }
        return maxNutzenHaus;
    }

    private void neuesZuhauseSuchen() {
        GebaudeHaus neuesZuhause = maxNutzenZuhause();
        if (neuesZuhause == zuhause || neuesZuhause == null) {
            return;
        }
        if (zuhause != null) {
            ausziehen();
        }
        einziehen(neuesZuhause);
    }

    // KOMPLEXE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public Dorfbewohner gettPartner(Dorfbewohner dorfbewohner) {
        if (dorfbewohner == parent1) {
            return parent2;
        }
        if (dorfbewohner == parent2) {
            return parent1;
        }
        return null;
    }

    public boolean issHatZweiParents() {
        return parent1 != null && parent2 != null;
    }

    public FensterChildFamilie gettAusklappenFenster() {
        if (ausklappenFenster == null) {
            ausklappenFenster = new FensterChildFamilie(familienmitglieder.get(0).getLevel(), this);
        }
        return ausklappenFenster;
    }

    // EINFACHE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public GebaudeHaus getZuhause() {
        return zuhause;
    }

    public void setZuhause(GebaudeHaus zuhause) {
        this.zuhause = zuhause;
    }

    public NahrungTasche getNahrungsTasche() {
        return nahrungsTasche;
    }

    public SpielzeugTasche getSpielzeugTasche() {
        return spielzeugTasche;
    }

    public List<Dorfbewohner> getFamilienmitglieder() {
        return familienmitglieder;
    }

    public Dorfbewohner getParent1() {
        return parent1;
    }

    public Dorfbewohner getParent2() {
        return parent2;
    }

    public float getNutzen() {
        return nutzen;
    }

    public float getProduktivitaet() {
        return produktivitaet;
    }

    public float getGesundheit() {
        return gesundheit;
    }

    public String getNachname() {
        return nachname;
    }

    public WerkzeugTasche getWerkzeugTasche() {
        return werkzeugTasche;
    }

    public MedizinTasche getMedizinTasche() {
        return medizinTasche;
    }

    public List<Dorfbewohner> getKinder() {
        return kinder;
    }
}
