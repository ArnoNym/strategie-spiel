package de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterOnKlick;

import com.badlogic.gdx.scenes.scene2d.ui.Label;

import de.arnonym.strategiespiel.spielSchirm.Level;
import de.arnonym.strategiespiel.spielSchirm.objekte.WorldObjekt;
import de.arnonym.strategiespiel.spielSchirm.objekte.kollisionsobjekte.gebaude.GebaudeLager;
import de.arnonym.strategiespiel.spielSchirm.balancing.IdsGegenstaende;

public class FensterOnKlickNahrungLager extends FensterOnKlick {
    private Label label;

    public FensterOnKlickNahrungLager(Level level, WorldObjekt objekt, String gebaudeBezeichnung) {
        super(level, objekt, 250, 175, gebaudeBezeichnung);

        label = new Label(aktualisierteInformationen(), getSkin());
        getContentTable().add(label).expand().left().top();
    }

    @Override
    public void act(float delta) {
        super.act(delta);

        label.setText(aktualisierteInformationen());
    }

    private String aktualisierteInformationen() {
        GebaudeLager g = (GebaudeLager)getObjekt();

        String text = "...";
        if (!isVisible()) {
            return text;
        }
        if (g.isLagerNahrung()) {
            text = ""+g.getGegenstandListe().getDurchId(IdsGegenstaende.NAH_BEEREN_ID).getWert();
        }

        String returnString = "MengeVerfuegbar: "+ g.getGegenstandListe().gettMenge(true, false, false)
                +"\n"
                +"\nPreis fuer Beeren: "+ text
                +"\n"
                +"\nGeld: "+g.getGeld().getMengeVerfuegbar();
        return returnString;
    }
}
