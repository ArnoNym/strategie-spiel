package de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterOnKlick;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import java.util.List;

import de.arnonym.strategiespiel.spielSchirm.Level;
import de.arnonym.strategiespiel.spielSchirm.objekte.WorldObjekt;
import de.arnonym.strategiespiel.spielSchirm.objekte.dorfbewohnerAi.Familie;
import de.arnonym.strategiespiel.spielSchirm.objekte.kollisionsobjekte.gebaude.GebaudeHaus;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.labels.UpdateLabel;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.tables.UpdateTableMod;

public class FensterOnKlickHaus extends FensterOnKlick {

    public FensterOnKlickHaus(Level level, WorldObjekt objekt, String gebaudeBezeichnung) {
        super(level, objekt, 250, 175, gebaudeBezeichnung);

        Label label;
        UpdateLabel updateLabel;

        label = new Label("rental fee: ", getSkin());
        getContentTable().add(label).expand().left();
        updateLabel = new UpdateLabel(getSkin()) {
            @Override
            public String aktualisiereInformationenOverride() {
                return ""+((GebaudeHaus)objekt).getPreisMiete();
            }
        };
        getUpdateLabelList().add(updateLabel);
        getContentTable().add(updateLabel).right();

        getContentTable().row();

        UpdateTableMod bewohnerTable = initWohnendeFamilienTable();
        getUpdateTableList().add(bewohnerTable);
        getContentTable().add(bewohnerTable).expand().fill().colspan(100);
    }

    private UpdateTableMod initWohnendeFamilienTable() {
        return new UpdateTableMod() {
            @Override
            public void aktualisiereInformationenOverride() {
                Skin skin = FensterOnKlickHaus.this.getSkin();
                Label label;

                List<Familie> familienListe = ((GebaudeHaus)FensterOnKlickHaus.this.getObjekt()).getBewohnerFamilienListe();
                if (familienListe.isEmpty()) {
                    label = new Label("uninhabited", skin);
                    add(label).left();
                } else {
                    label = new Label("Families living in this building: ", skin);
                    add(label).expand().left().colspan(100);
                    row();
                    label = new Label("Name", skin);
                    add(label).expandX().left();
                    label = new Label("Family Members", skin);
                    add(label).expandX().left();
                    row();

                    for (Familie f : familienListe) {
                        label = new Label(f.getNachname(), skin);
                        add(label).left();
                        label = new Label("" + f.getFamilienmitglieder().size(), skin);
                        add(label).left();
                        label = new Label("visit", skin);
                        Button button = new Button(label, skin);
                        button.addListener(new ClickListener() {
                            @Override
                            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                                FensterOnKlickHaus.this.childAusklappen(f.gettAusklappenFenster());
                                return super.touchDown(event, x, y, pointer, button);
                            }
                        });
                        add(button).right();
                        row();
                    }
                }
            }
        };
    }
}
