package de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterOnKlick;

import com.badlogic.gdx.scenes.scene2d.ui.Label;

import de.arnonym.strategiespiel.spielSchirm.Level;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.Gegenstand;
import de.arnonym.strategiespiel.spielSchirm.objekte.kollisionsobjekte.gebaude.GebaudeLager;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.labels.UpdateLabel;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.tables.UpdateTableMod;
import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenBalancing;

public class FensterOnKlickMaterialLager extends FensterOnKlick {


    public FensterOnKlickMaterialLager(Level level, GebaudeLager gebaudeLager,
                                       String fensterTitel) {
        super(level, gebaudeLager, 600, 300, fensterTitel);
        Label label;
        UpdateLabel updateLabel;

        label = new Label("money: ", getSkin());
        getContentTable().add(label).expand().left();
        updateLabel = new UpdateLabel(getSkin()) {
            @Override
            public String aktualisiereInformationenOverride() {
                return ""+gebaudeLager.getGeld().getMengeVerfuegbar();
            }
        };
        getUpdateLabelList().add(updateLabel);
        getContentTable().add(updateLabel).right();

        getContentTable().row();

        label = new Label("platz: ", getSkin());
        getContentTable().add(label).expand().left();
        updateLabel = new UpdateLabel(getSkin()) {
            @Override
            public String aktualisiereInformationenOverride() {
                return ""+gebaudeLager.getGegenstandListe().getPlatz();
            }
        };
        getUpdateLabelList().add(updateLabel);
        getContentTable().add(updateLabel).right();

        getContentTable().row();

        UpdateTableMod produkteTable = initProdukteTable();
        getUpdateTableList().add(produkteTable);
        getContentTable().add(produkteTable).expand().fill().colspan(100);
    }

    private UpdateTableMod initProdukteTable() {
        return new UpdateTableMod(getSkin()) {
            @Override
            public void aktualisiereInformationenOverride() {
                Label label;

                label = new Label("products: ", getSkin());
                add(label).expand().left().colspan(100);
                row();
                label = new Label("name /", getSkin());
                add(label).expandX().left();
                label = new Label("amount /", getSkin());
                add(label).expandX().left();
                label = new Label("avg purchasing costs /", getSkin());
                add(label).expandX().left();
                label = new Label("avg delivery costs /", getSkin());
                add(label).expandX().left();
                label = new Label("price per unit", getSkin());
                add(label).expandX().left();
                row();

                for (Gegenstand g : getObjekt().getGegenstandListe()) {
                    label = new Label(g.gettName(), getSkin());
                    add(label).left();
                    label = new Label("" + g.getMengeVerfuegbar(), getSkin());
                    add(label).left();
                    label = new Label("" + ((GebaudeLager)getObjekt()).getWirtschaftsdatenManager()
                            .gettDurchschnKaufKosten(g.gettId(),
                                    KonstantenBalancing.ZIVIL_OBJEKT_BETRACHTE_PERIODEN),
                            getSkin());
                    add(label).left();
                    label = new Label("" + ((GebaudeLager)getObjekt()).getWirtschaftsdatenManager()
                            .gettDurchschnLieferKosten(g.gettId(),
                                    KonstantenBalancing.ZIVIL_OBJEKT_BETRACHTE_PERIODEN),
                            getSkin());
                    add(label).left();
                    label = new Label("" + g.getWert(), getSkin());
                    add(label).left();
                    row();
                }
            }
        };
    }
}