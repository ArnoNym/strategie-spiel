package de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterOnKlick;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import de.arnonym.strategiespiel.spielSchirm.Level;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.Gegenstand;
import de.arnonym.strategiespiel.spielSchirm.objekte.WorldObjekt;
import de.arnonym.strategiespiel.spielSchirm.objekte.dorfbewohnerAi.Dorfbewohner;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.labels.UpdateLabel;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.MathUtils;

public class FensterOnKlickDorfbewohner extends FensterOnKlick {

    private Dorfbewohner dorfbewohner;
    private final float buttonPadLeft = 10f;

    public FensterOnKlickDorfbewohner(Level level, WorldObjekt objekt, String name) {
        super(level, objekt, 500/*336*/, 500/*296*/, "Dorfbewohner "+name);

        this.dorfbewohner = (Dorfbewohner)getObjekt();

        initFamilieInformationen(dorfbewohner);

        getContentTable().row();

        Label label = new Label("age: ", getSkin());
        getContentTable().add(label).left();
        UpdateLabel updateLabel = new UpdateLabel(getSkin()) {
            @Override
            public String aktualisiereInformationenOverride() {
                return ""+MathUtils.runden(dorfbewohner.gettAlter(), 2);
            }
        };
        getUpdateLabelList().add(updateLabel);
        getContentTable().add(updateLabel).expandX().right();
        getContentTable().row();

        getContentTable().row();

        label = new Label("sleep: ", getSkin());
        getContentTable().add(label).left();
        updateLabel = new UpdateLabel(getSkin()) {
            @Override
            public String aktualisiereInformationenOverride() {
                return ""+MathUtils.runden(dorfbewohner.getSchlaf(), 2);
            }
        };
        getUpdateLabelList().add(updateLabel);
        getContentTable().add(updateLabel).expandX().right();
        getContentTable().row();

        initInnereTabellePerson(dorfbewohner);

        getContentTable().row();

        initArbeitenUndBerufInformationen(dorfbewohner);

        getContentTable().row();

        label = new Label("avaiable amount: ", getSkin());
        getContentTable().add(label).left();
        updateLabel = new UpdateLabel(getSkin()) {
            @Override
            public String aktualisiereInformationenOverride() {
                return ""+dorfbewohner.getGegenstandListe().gettMenge(true, false, false);
            }
        };
        getUpdateLabelList().add(updateLabel);
        getContentTable().add(updateLabel).expand().right();
        getContentTable().row();
        label = new Label("reserviertNehmen amount: ", getSkin());
        getContentTable().add(label).left();
        updateLabel = new UpdateLabel(getSkin()) {
            @Override
            public String aktualisiereInformationenOverride() {
                int i = 0;
                for (Gegenstand g : dorfbewohner.getGegenstandListe()) {
                    i += g.getMengeNehmenReserviert();
                }
                return ""+i;
            }
        };
        getUpdateLabelList().add(updateLabel);
        getContentTable().add(updateLabel).expand().right();
        getContentTable().row();
        label = new Label("reserviertLegen amount: ", getSkin());
        getContentTable().add(label).left();
        updateLabel = new UpdateLabel(getSkin()) {
            @Override
            public String aktualisiereInformationenOverride() {
                int i = 0;
                for (Gegenstand g : dorfbewohner.getGegenstandListe()) {
                    i += g.getMengeLegenReserviert();
                }
                return ""+i;
            }
        };
        getUpdateLabelList().add(updateLabel);
        getContentTable().add(updateLabel).expand().right();
        getContentTable().row();
        label = new Label("transported amount (Verfuegbar): ", getSkin());
        getContentTable().add(label).left();
        updateLabel = new UpdateLabel(getSkin()) {
            @Override
            public String aktualisiereInformationenOverride() {
                return ""+dorfbewohner.getGegenstandListe().gettMenge(true, false, false);
            }
        };
        getUpdateLabelList().add(updateLabel);
        getContentTable().add(updateLabel).expand().right();
        getContentTable().row();
        label = new Label("transported amount (Legen): ", getSkin());
        getContentTable().add(label).left();
        updateLabel = new UpdateLabel(getSkin()) {
            @Override
            public String aktualisiereInformationenOverride() {
                return ""+dorfbewohner.getGegenstandListe().gettMenge(false, true, false);
            }
        };
        getUpdateLabelList().add(updateLabel);
        getContentTable().add(updateLabel).expand().right();
        getContentTable().row();
        label = new Label("transported amount (Nehmen): ", getSkin());
        getContentTable().add(label).left();
        updateLabel = new UpdateLabel(getSkin()) {
            @Override
            public String aktualisiereInformationenOverride() {
                return ""+dorfbewohner.getGegenstandListe().gettMenge(false, true, false);
            }
        };
        getUpdateLabelList().add(updateLabel);
        getContentTable().add(updateLabel).expand().right();
        getContentTable().row();
        label = new Label("transported space: ", getSkin());
        getContentTable().add(label).left();
        updateLabel = new UpdateLabel(getSkin()) {
            @Override
            public String aktualisiereInformationenOverride() {
                return dorfbewohner.getGegenstandListe().gettPlatzMalMengen(true, false, true)
                        + " / " + dorfbewohner.getGegenstandListe().getMaxPlatz();
            }
        };
        getUpdateLabelList().add(updateLabel);
        getContentTable().add(updateLabel).expand().right();
        getContentTable().row();
        label = new Label("stepMachineSize: ", getSkin());
        getContentTable().add(label).left();
        updateLabel = new UpdateLabel(getSkin()) {
            @Override
            public String aktualisiereInformationenOverride() {
                return ""+dorfbewohner.getStackFiniteStepMachine().countSteps();
            }
        };
        getUpdateLabelList().add(updateLabel);
        getContentTable().add(updateLabel).expand().right();

        setStandardposition(false);
    }

    @Override
    public void act(float delta) {
        setFensterTitel("Dorfbewohner "+dorfbewohner.getName());
        super.act(delta);
    }

    private void initFamilieInformationen(Dorfbewohner dorfbewohner) {
        Table innerTable = new Table();
        getContentTable().add(innerTable).expand().fill().colspan(2);
        Label label;
        UpdateLabel updateLabel;

        label = new Label("health: ", getSkin());
        innerTable.add(label).left();
        updateLabel = new UpdateLabel(getSkin()) {
            @Override
            public String aktualisiereInformationenOverride() {
                return ""+MathUtils.runden(dorfbewohner.getGesundheit(), 2);
            }
        };
        getUpdateLabelList().add(updateLabel);
        innerTable.add(updateLabel).expandX().right();

        Button button = new Button(new Label(" Visit Family ", getSkin()) ,getSkin());
        button.addListener(new ClickListener(){
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                childAusklappen(dorfbewohner.getFamilie().gettAusklappenFenster());
                return super.touchDown(event, x, y, pointer, button);
            }
        });
        getContentTable().add(button).fillX().padLeft(buttonPadLeft);
    }

    private void initArbeitenUndBerufInformationen(Dorfbewohner dorfbewohner) {
        Table innerTable = new Table();
        getContentTable().add(innerTable).expand().fill().colspan(2);
        Label label;
        UpdateLabel updateLabel;

        label = new Label("Workplace: ", getSkin());
        innerTable.add(label).left();
        updateLabel = new UpdateLabel(getSkin()) {
            @Override
            public String aktualisiereInformationenOverride() {
                String text = "Unemployed";
                if (dorfbewohner.getGebBeruf() != null) {
                    text = ""+dorfbewohner.getGebBeruf().getClass().getSimpleName();
                }
                return text;
            }
        };
        getUpdateLabelList().add(updateLabel);
        innerTable.add(updateLabel).expandX().right();

        innerTable.row();

        label = new Label("Salary: ", getSkin());
        innerTable.add(label).left();
        updateLabel = new UpdateLabel(getSkin()) {
            @Override
            public String aktualisiereInformationenOverride() {
                if (dorfbewohner.getBeruf() != null) {
                    return ""+ MathUtils.runden(dorfbewohner.getBeruf().gehalt(), 2);
                }
                return " - ";
            }
        };
        getUpdateLabelList().add(updateLabel);
        innerTable.add(updateLabel).expandX().right();

        Button button = new Button(new Label(" Visit Workplace ", getSkin()),getSkin());
        button.addListener(new ClickListener(){
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if (dorfbewohner.getGebBeruf() != null) {
                    dorfbewohner.getGebBeruf().onKlickFenster(true);
                }
                return super.touchDown(event, x, y, pointer, button);
            }
        });
        getContentTable().add(button).fillX().padLeft(buttonPadLeft);
    }

    private void initInnereTabellePerson(Dorfbewohner dorfbewohner) {
        Table innerTable = new Table();
        getContentTable().add(innerTable).expand().fill().colspan(100);

        Label label;
        UpdateLabel updateLabel;
        float padding = 7f;

        label = new Label("", getSkin());
        innerTable.add(label).expandX().left().pad(padding);
        label = new Label("Genetically", getSkin());
        innerTable.add(label).pad(padding);
        label = new Label("Skilled", getSkin());
        innerTable.add(label).pad(padding);
        label = new Label("Productivity", getSkin());
        innerTable.add(label).pad(padding);

        innerTable.row();

        label = new Label("Body: ", getSkin());
        innerTable.add(label).expandX().left().padLeft(0).padRight(padding);
        label = new Label(""+dorfbewohner.getDnaProduktivitaetKoerper(), getSkin());
        innerTable.add(label).padLeft(padding).padRight(padding);
        updateLabel = new UpdateLabel(getSkin()){
            @Override
            public String aktualisiereInformationenOverride() {
                return ""+MathUtils.runden(dorfbewohner.getFitness(), 2);
            }
        };
        getUpdateLabelList().add(updateLabel);
        innerTable.add(updateLabel).padLeft(padding).padRight(padding);
        updateLabel = new UpdateLabel(getSkin()){
            @Override
            public String aktualisiereInformationenOverride() {
                return ""+MathUtils.runden(dorfbewohner.getProduktivitaetKoerper(), 2);
            }
        };
        getUpdateLabelList().add(updateLabel);
        innerTable.add(updateLabel).padLeft(padding).padRight(padding);

        innerTable.row();

        label = new Label("Mind: ", getSkin());
        innerTable.add(label).expandX().left().padLeft(0).padRight(padding);
        label = new Label(""+dorfbewohner.getDnaProduktivtiaetKopf(), getSkin());
        innerTable.add(label).padLeft(padding).padRight(padding);
        updateLabel = new UpdateLabel(getSkin()){
            @Override
            public String aktualisiereInformationenOverride() {
                return ""+MathUtils.runden(dorfbewohner.getBildung(), 2);
            }
        };
        getUpdateLabelList().add(updateLabel);
        innerTable.add(updateLabel).padLeft(padding).padRight(padding);
        updateLabel = new UpdateLabel(getSkin()){
            @Override
            public String aktualisiereInformationenOverride() {
                return ""+MathUtils.runden(dorfbewohner.getProduktivitaetKopf(), 2);
            }
        };
        getUpdateLabelList().add(updateLabel);
        innerTable.add(updateLabel).padLeft(padding).padRight(padding);

        innerTable.row();

        label = new Label("movement speed: ", getSkin());
        innerTable.add(label).expandX().left().padLeft(0).padRight(padding);
        updateLabel = new UpdateLabel(getSkin()){
            @Override
            public String aktualisiereInformationenOverride() {
                return ""+MathUtils.runden(dorfbewohner.getKonstantenBewegungsgeschwindigkeit(), 2);
            }
        };
        getUpdateLabelList().add(updateLabel);
        innerTable.add(updateLabel).padLeft(padding).padRight(padding);

        innerTable.row();

        label = new Label("catch up sleep: ", getSkin());
        innerTable.add(label).expandX().left().padLeft(0).padRight(padding);
        updateLabel = new UpdateLabel(getSkin()){
            @Override
            public String aktualisiereInformationenOverride() {
                return ""+MathUtils.runden(dorfbewohner.getKonstantenSchlafAuffuellGeschwindigkeit(), 2);
            }
        };
        getUpdateLabelList().add(updateLabel);
        innerTable.add(updateLabel).padLeft(padding).padRight(padding);
    }
}
