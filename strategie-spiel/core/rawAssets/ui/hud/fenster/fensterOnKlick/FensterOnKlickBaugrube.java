package de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterOnKlick;

import com.badlogic.gdx.scenes.scene2d.ui.Label;

import de.arnonym.strategiespiel.spielSchirm.Level;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.Gegenstand;
import de.arnonym.strategiespiel.spielSchirm.objekte.kollisionsobjekte.gebaude.Baugrube;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.labels.UpdateLabel;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.tables.UpdateTableMod;

public class FensterOnKlickBaugrube extends FensterOnKlick {

    public FensterOnKlickBaugrube(Level level, Baugrube baugrube, String gebaudeBezeichnung) {
        super(level, baugrube, 450, 175, gebaudeBezeichnung);

        Label label;
        UpdateLabel updateLabel;

        label = new Label("progress: ", getSkin());
        getContentTable().add(label).expandX().left();
        updateLabel = new UpdateLabel(getSkin()) {
            @Override
            public String aktualisiereInformationenOverride() {
                return ((int) (baugrube.gettErledigtProzent() * 100)) + "%";
            }
        };
        getUpdateLabelList().add(updateLabel);
        getContentTable().add(updateLabel).expandX().right();

        getContentTable().row();

        label = new Label("planned progress: ", getSkin());
        getContentTable().add(label).expandX().left();
        updateLabel = new UpdateLabel(getSkin()) {
            @Override
            public String aktualisiereInformationenOverride() {
                return ((int) (baugrube.gettErledigtUndGeplantProzent() * 100)) + "%";
            }
        };
        getUpdateLabelList().add(updateLabel);
        getContentTable().add(updateLabel).expandX().right();

        getContentTable().row();

        label = new Label("platz: ", getSkin());
        getContentTable().add(label).expand().left();
        updateLabel = new UpdateLabel(getSkin()) {
            @Override
            public String aktualisiereInformationenOverride() {
                return ""+baugrube.getGegenstandListe().getPlatz();
            }
        };
        getUpdateLabelList().add(updateLabel);
        getContentTable().add(updateLabel).right();

        getContentTable().row();

        UpdateTableMod produkteTable = initProdukteTable();
        getUpdateTableList().add(produkteTable);
        getContentTable().add(produkteTable).expand().fill().colspan(100);
    }

    private UpdateTableMod initProdukteTable() {
        return new UpdateTableMod(getSkin()) {
            @Override
            public void aktualisiereInformationenOverride() {
                Label label;

                label = new Label("products: ", getSkin());
                add(label).expand().left().colspan(100);
                row();
                label = new Label("name /", getSkin());
                add(label).expandX().left();
                label = new Label("existing amount /", getSkin());
                add(label).expandX().left();
                label = new Label("required amount", getSkin());
                add(label).expandX().left();
                row();

                for (Gegenstand g : getObjekt().getGegenstandListe()) {
                    label = new Label(g.gettName(), getSkin());
                    add(label).left();
                    label = new Label("" + g.getMengeVerfuegbar(), getSkin());
                    add(label).left();
                    label = new Label("" + (g.getMengeMaximal()-g.getMengeNehmenReserviert()),
                            getSkin()); /* Nicht mengeMax weil es mit der zum Bau verwendeten Menge
                            sinken soll */
                    add(label).left();
                    row();
                }
            }
        };
    }
}
