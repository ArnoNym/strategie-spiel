package de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterOnKlick;

import com.badlogic.gdx.scenes.scene2d.ui.Label;

import de.arnonym.strategiespiel.spielSchirm.Level;
import de.arnonym.strategiespiel.spielSchirm.objekte.WorldObjekt;
import de.arnonym.strategiespiel.spielSchirm.objekte.kollisionsobjekte.ressourcen.RessourceWachsen;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.labels.UpdateLabel;

public class FensterOnKlickRessource extends FensterOnKlick {

    public FensterOnKlickRessource(Level level, WorldObjekt objekt, String fensterTitel) {
        super(level, objekt, 500, 300, fensterTitel);
        Label label;
        UpdateLabel updateLabel;

        label = new Label("amount: ", getSkin());
        getContentTable().add(label).expand().left();
        updateLabel = new UpdateLabel(getSkin()) {
            @Override
            public String aktualisiereInformationenOverride() {
                return ""+objekt.getGegenstandListe().gettMenge(true, true, true);
            }
        };
        getUpdateLabelList().add(updateLabel);
        getContentTable().add(updateLabel);

        getContentTable().row();

        label = new Label("mature: ", getSkin());
        getContentTable().add(label).expand().left();
        updateLabel = new UpdateLabel(getSkin()) {
            @Override
            public String aktualisiereInformationenOverride() {
                return ""+((RessourceWachsen) objekt).issAusgewachsen();
            }
        };
        getUpdateLabelList().add(updateLabel);
        getContentTable().add(updateLabel);

        getContentTable().row();

        label = new Label("age: ", getSkin());
        getContentTable().add(label).expand().left();
        updateLabel = new UpdateLabel(getSkin()) {
            @Override
            public String aktualisiereInformationenOverride() {
                return ""+((RessourceWachsen) objekt).getEnumStufe().name();
            }
        };
        getUpdateLabelList().add(updateLabel);
        getContentTable().add(updateLabel);
    }
}
