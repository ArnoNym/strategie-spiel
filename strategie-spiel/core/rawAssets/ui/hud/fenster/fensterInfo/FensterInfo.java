package de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterInfo;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;

import java.util.Map;

import de.arnonym.strategiespiel.Strategiespiel;
import de.arnonym.strategiespiel.spielSchirm.Dorf;
import de.arnonym.strategiespiel.spielSchirm.ui.IngameCursorManager;
import de.arnonym.strategiespiel.spielSchirm.Level;
import de.arnonym.strategiespiel.spielSchirm.objekte.kollisionsobjekte.gebaude.Gebaude;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.MoveableGroup;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.tables.UpdateTable;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.Gegenstand;
import de.arnonym.strategiespiel.framework.werkzeuge.collections.hashMaps.IntIntHashMap;
import de.arnonym.strategiespiel.framework.werkzeuge.Assets;
import de.arnonym.strategiespiel.spielSchirm.balancing.IdsGegenstaende;
import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenOptik;

public class FensterInfo extends MoveableGroup {
    private final Level level;
    private NinePatchDrawable ninePatchDrawable;

    private boolean hovertUeberIcon;
    private Gebaude blaupausenGebaude;
    private Table contentTable;

    public FensterInfo(Level level, IngameCursorManager ingameCursorManager) {
        super(level.getGuiStage(), level.getOverlayViewport(), level.getViewport(),
                ingameCursorManager, 1, 1);
        this.level = level;
        this.ninePatchDrawable = new NinePatchDrawable(new NinePatch(
                Assets.instance.levelGuiAssets.auswahlKastenA,
                KonstantenOptik.AUSWAHLKASTEN_ECKEN_BREITE, //TODO
                KonstantenOptik.AUSWAHLKASTEN_ECKEN_BREITE,
                KonstantenOptik.AUSWAHLKASTEN_ECKEN_HOEHE,
                KonstantenOptik.AUSWAHLKASTEN_ECKEN_HOEHE));
        setVisible(false);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public void act(float delta) {
        if (!isVisible()) return;

        if (contentTable instanceof UpdateTable) {
            ((UpdateTable) contentTable).aktualisiereInformationen();
        }

        if (hovertUeberIcon) {
            if (getParentFenster() != null) {
                settPositionAnParent();
            } else {
                settPositionAnMaus();
            }
        } else if (blaupausenGebaude != null) {
            settPositionAnBlaupause();
            toBack(); // Nicht ueber Fenstern wenn Maus auf Fenster bewegt wird
        } else {
            schliessen();
        }
        super.act(delta);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
    }

    // AKTIVIEREN UND DEAKTIVIEREN ////////////////////////////////////////////////////////////////

    public void aktiviereHover(int id, MoveableGroup parentFenster) {
        setParentFenster(parentFenster);
        aktiviereHover(id);
    }

    public void aktiviereHover(int id) {
        hovertUeberIcon = true;
        settContent(gettInfos(id));
        aktiviere(gettInfos(id));
    }

    public void deaktiviereHover() {
        setParentFenster(null);
        hovertUeberIcon = false;
        deaktiviere();
    }

    public void aktiviereBlaupause(Gebaude blaupausenGebaude) {
        this.blaupausenGebaude = blaupausenGebaude;
        aktiviere(gettInfos(blaupausenGebaude));
    }

    public void deaktiviereBlaupause() {
        blaupausenGebaude = null;
        deaktiviere();
    }

    private void aktiviere(Table table) {
        toFront();
        oeffnen(false);
        settContent(table);
    }

    private void deaktiviere() {

    }

    // CONTENT ////////////////////////////////////////////////////////////////////////////////////

    private void settContent(Table table) {
        clearChildren();
        this.contentTable = table;
        contentTable.setBackground(ninePatchDrawable);
        contentTable.pack();
        addActor(contentTable);
        setWidth(contentTable.getWidth());
        setHeight(contentTable.getHeight());
    }

    private Table gettInfos(int id) {
        Dorf dorf = level.getDorf();
        Gegenstand gegenstand = dorf.getForschungGegenstandListe().getDurchId(id);
        if (gegenstand != null) return gettInfos(gegenstand);
        Gebaude gebaude = dorf.getForschungGebaudeListe().gettDurchId(id);
        if (gebaude != null) return gettInfos(gebaude);
        throw new IllegalArgumentException();
    }

    private static Table gettInfos(Gegenstand gegenstand) {
        Table table = new Table(Strategiespiel.skin);
        table.add("name: ").left();
        table.add(gegenstand.gettName()).left();
        table.row();
        table.add("description: ").left();
        table.add(gegenstand.gettBeschreibung()).left();
        table.row();
        table.add("");
        table.row();
        table.add("input: ").left();
        table.row();
        for (Map.Entry<Integer, Integer> entry : gegenstand.gettInputHashMap().entrySet()) {
            int inputId = entry.getKey();
            int menge = entry.getValue();
            table.add(IdsGegenstaende.gett(inputId).gettName()+", "+menge).left();
            table.row();
        }
        return table;
    }

    private Table gettInfos(Gebaude blaupausenGebaude) {
        Table contentTable = new Table(getSkin());

        contentTable.add(new Label("building name: ", getSkin()));
        contentTable.add(new Label("" + blaupausenGebaude.getClass().getSimpleName(), getSkin()));

        contentTable.row();
        contentTable.add("").row();

        UpdateTable updateTable = new UpdateTable(getSkin()) { // Weil ich erstelleHeader() nutzen moechte
            @Override
            public void aktualisiereInformationen() {
                erstelleHeader("required building material: ", true, "name", "avaiable",
                        " / purchasable", " / needet");
                row();

                IntIntHashMap alleGegenstaendeHashMap = level.getDorf().gettAlleGegenstaendeHashMap(true, false, true);
                for (Map.Entry<Integer, Integer> entry : blaupausenGebaude.getNoetigesBaumaterial().entrySet()) {
                    add(new Label(IdsGegenstaende.gett(entry.getKey()).gettName(), getSkin())).left();

                    int id = entry.getKey();
                    Integer vorhandenGegenstandMenge = alleGegenstaendeHashMap.get(id);
                    if (vorhandenGegenstandMenge == null) {
                        vorhandenGegenstandMenge = 0;
                    }
                    add(new Label(""+vorhandenGegenstandMenge, getSkin())).left();

                    add(new Label(" / 0", getSkin())).left(); //TODO Zum kauf verfuegbare Menge anpassen wenn Gebaude verkauft werden koennen oder Handel eingefuegt wird etc

                    int zumBauBenoetigteMenge = entry.getValue();
                    add(new Label(" / "+zumBauBenoetigteMenge, getSkin())).left();

                    row();
                }
            }
        };
        updateTable.aktualisiereInformationen();
        contentTable.add(updateTable);

        return contentTable;
    }

    // KOMPLEXE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    private void settPositionAnBlaupause() {
        float x, y, width, height;
        Vector2 blaupausePosition = blaupausenGebaude.getPosition();
        x = blaupausePosition.x;
        y = blaupausePosition.y;
        TextureRegion textureRegion = blaupausenGebaude.getBlaupauseTextureRegion();
        width = textureRegion.getRegionWidth();
        height = textureRegion.getRegionHeight();
        settPosition(new Vector2(x + width, y + height), 0, - getHeight());
    }

    /*private void settPositionAnParent() {
        float x = getParentFenster().getX();
        float y = getParentFenster().getY();
        float width = getParentFenster().getWidth();
        float height = getParentFenster().getHeight();
        /*if (0==0) throw new IllegalStateException("contentTable.getHeight(): "+contentTable.getHeight()
                + "contentTable.getPrefHeight(): "+contentTable.getPrefHeight()
                + "getHeight(): "+getHeight());
        settPosition(new Vector2(x + width, y + height - contentTable.getHeight()));
    }*/

    private void settPositionAnMaus() {
        Vector2 position = getIngameCursorManager().gettMausOverlayPosition();
        position.x += 50; // TODO Konstante
        settPosition(position);
    }

    // PROTECTED GETTER UND SETTER ////////////////////////////////////////////////////////////////

    protected boolean isHovertUeberIcon() {
        return hovertUeberIcon;
    }

    protected Table getContentTable() {
        return contentTable;
    }

    // EINFACHE GETTER UND SETTER /////////////////////////////////////////////////////////////////
}
