package de.arnonym.strategiespiel.spielSchirm.ui.auswahlkaesten.deprecated;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.viewport.Viewport;

import de.arnonym.strategiespiel.spielSchirm.ui.IngameCursorManager;
import de.arnonym.strategiespiel.spielSchirm.Level;
import de.arnonym.strategiespiel.spielSchirm.ui.MyInputProcessor;
import de.arnonym.strategiespiel.spielSchirm.ui.ZellenMarkierungRenderer;
import de.arnonym.strategiespiel.framework.werkzeuge.Enums;

public abstract class Auswahlkasten {
    private Level level;
    private Viewport viewport;
    private MyInputProcessor myInputProcessor;
    private IngameCursorManager ingameCursorManager;
    private ZellenMarkierungRenderer zellenMarkierungRenderer;

    private Vector2 positionAnfang;
    private Vector2 position;
    private Enums.EnumAktiv enumAktiv;

    public Auswahlkasten(Level level, Viewport worldViewport, MyInputProcessor myInputProcessor,
                         IngameCursorManager ingameCursorManager) {
        this.level = level;
        this.viewport = worldViewport;
        this.myInputProcessor = myInputProcessor;
        this.ingameCursorManager = ingameCursorManager;
        zellenMarkierungRenderer = ingameCursorManager.getZellenMarkierungRenderer();
        enumAktiv = Enums.EnumAktiv.INAKTIV;
    }

    public abstract void update();

    // AKTIVIEREN ETC /////////////////////////////////////////////////////////////////////////////

    public void aktivieren() {
        this.enumAktiv = Enums.EnumAktiv.AKTIV;
        myInputProcessor.setAktiv(false);
        ingameCursorManager.setCursorStatus(Enums.EnumIngameWorldCursorStatus.ZELLE);
    }

    protected void vorschlagen() {
        this.enumAktiv = Enums.EnumAktiv.VORSCHLAGEN;
        myInputProcessor.setAktiv(false);
        ingameCursorManager.setCursorStatus(Enums.EnumIngameWorldCursorStatus.NICHTS);
    }

    public void auswaehlen() {
        ingameCursorManager.setCursorStatus(Enums.EnumIngameWorldCursorStatus.ZELLE);
    }

    public void deaktivieren() {
        this.enumAktiv = Enums.EnumAktiv.INAKTIV;
        myInputProcessor.setAktiv(true);
        ingameCursorManager.setCursorStatus(
                Enums.EnumIngameWorldCursorStatus.ZELLE_UND_GEBAUDE_UND_CURSOR);
    }

    // PROTECTED GETTER UND SETTER ////////////////////////////////////////////////////////////////

    protected Level getLevel() {
        return level;
    }

    protected Viewport getViewport() {
        return viewport;
    }

    protected MyInputProcessor getMyInputProcessor() {
        return myInputProcessor;
    }

    protected Vector2 getPositionAnfang() {
        return positionAnfang;
    }

    protected Vector2 getPosition() {
        return position;
    }

    protected void setPositionAnfang(Vector2 positionAnfang) {
        this.positionAnfang = positionAnfang;
    }

    protected void setPosition(Vector2 position) {
        this.position = position;
    }

    protected void setEnumAktiv(Enums.EnumAktiv enumAktiv) {
        this.enumAktiv = enumAktiv;
    }

    // EINFACHE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public Enums.EnumAktiv getEnumAktiv() {
        return enumAktiv;
    }

    public ZellenMarkierungRenderer getZellenMarkierungRenderer() {
        return zellenMarkierungRenderer;
    }

    public IngameCursorManager getIngameCursorManager() {
        return ingameCursorManager;
    }
}
