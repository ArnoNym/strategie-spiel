package de.arnonym.strategiespiel.spielSchirm.ui.auswahlkaesten.deprecated;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.viewport.Viewport;

import java.util.ArrayList;
import java.util.List;

import de.arnonym.strategiespiel.spielSchirm.ui.IngameCursorManager;
import de.arnonym.strategiespiel.spielSchirm.Level;
import de.arnonym.strategiespiel.spielSchirm.ui.MyInputProcessor;
import de.arnonym.strategiespiel.spielSchirm.worldMap.Zelle;
import de.arnonym.strategiespiel.framework.werkzeuge.Assets;
import de.arnonym.strategiespiel.framework.werkzeuge.Enums;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.GeometryUtils;

public class AuswahlkastenZellenLandUebergeben {
    /*private GebBerufFarm gebBerufFarm;

    public AuswahlkastenZellenLandUebergeben(Level level, Viewport worldViewport,
                                             MyInputProcessor myInputProcessor,
                                             IngameCursorManager ingameCursorManager) {
        super(level, worldViewport, myInputProcessor, ingameCursorManager);
    }

    @Override
    public void update() {
        if (getEnumAktiv() != Enums.EnumAktiv.INAKTIV) {
            for (Zelle z : gebBerufFarm.gettBesitzListe()) {
                getZellenMarkierungRenderer().addPosition(z.getPosition(),
                        Assets.instance.tiledMapAssets.durchsichtigGruen, true);
            }
        }

        if (getEnumAktiv() == Enums.EnumAktiv.VORSCHLAGEN) {
            setPosition(GeometryUtils.gettIndizes(
                    getViewport().unproject(new Vector2(Gdx.input.getX(), Gdx.input.getY()))));
            int restGruen = gebBerufFarm.getMaxBewirtschafteteZellen()
                    - gebBerufFarm.getBewirtschafteteZellenListe().size();
            List<Zelle> zellenListe = gettZellenListe(getPosition(), getPositionAnfang());
            boolean loeschen = !zellenListe.isEmpty()
                    && gebBerufFarm.getBewirtschafteteZellenListe().contains(zellenListe.get(0));
            for (Zelle z : zellenListe) {
                TextureRegion textureRegion;
                if (loeschen) {
                    textureRegion = Assets.instance.tiledMapAssets.entfernenMarkierung;
                } else if (z.getBesitzer() == null) {
                    if (restGruen <= 0) {
                        textureRegion = Assets.instance.tiledMapAssets.durchsichtigRot;
                    } else {
                        textureRegion = Assets.instance.tiledMapAssets.durchsichtigGruen;
                        restGruen -= 1;
                    }
                } else {
                    if (z.getBesitzer() == gebBerufFarm) {
                        textureRegion = Assets.instance.tiledMapAssets.durchsichtigGruen;
                    } else {
                        textureRegion = Assets.instance.tiledMapAssets.durchsichtigRot;
                    }
                }
                getZellenMarkierungRenderer().addPosition(z.getPosition(), textureRegion, true);
            }
        }
    }

    private List<Zelle> gettPassendeZellenListe(List<Zelle> zellenListe, boolean loeschen) {
        List<Zelle> addZellenListe = new ArrayList<>();

        setPosition(GeometryUtils.gettIndizes(
                getViewport().unproject(
                        new Vector2(Gdx.input.getX(), Gdx.input.getY()))));
        if (loeschen) {
            addZellenListe.addAll(zellenListe);
        } else {
            int restGruen = gebBerufFarm.getMaxBewirtschafteteZellen()
                    - gebBerufFarm.getBewirtschafteteZellenListe().size();
            for (Zelle z : zellenListe) {
                if (z.getBesitzer() == null && restGruen > 0) {
                    addZellenListe.add(z);
                    restGruen -= 1;
                }
            }
        }

        return addZellenListe;
    }

    @Deprecated
    @Override
    public void aktivieren() {
        super.aktivieren();
    }
    public void auswaehlen(GebBerufFarm gebBerufFarm) {
        super.aktivieren();
        this.gebBerufFarm = gebBerufFarm;
    }

    @Override
    public void auswaehlen() {
        super.auswaehlen();
        setEnumAktiv(Enums.EnumAktiv.AKTIV);

        // Damit Fehler durch sehr niedrige Update-Raten unmoeglich werden
        List<Zelle> zellenListe = gettZellenListe(getPosition(), getPositionAnfang());
        boolean loeschen = !zellenListe.isEmpty()
                && gebBerufFarm.getBewirtschafteteZellenListe().contains(zellenListe.get(0));
        gebBerufFarm.addUndPruefeLand(gettPassendeZellenListe(zellenListe, loeschen));
    }*/
}