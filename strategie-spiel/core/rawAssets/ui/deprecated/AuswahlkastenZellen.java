package de.arnonym.strategiespiel.spielSchirm.ui.auswahlkaesten.deprecated;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.viewport.Viewport;

import java.util.ArrayList;
import java.util.List;

import de.arnonym.strategiespiel.spielSchirm.ui.IngameCursorManager;
import de.arnonym.strategiespiel.spielSchirm.Level;
import de.arnonym.strategiespiel.spielSchirm.ui.MyInputProcessor;
import de.arnonym.strategiespiel.spielSchirm.worldMap.Zelle;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.GeometryUtils;

public abstract class AuswahlkastenZellen extends Auswahlkasten {

    public AuswahlkastenZellen(Level level, Viewport worldViewport,
                               MyInputProcessor myInputProcessor,
                               IngameCursorManager ingameCursorManager) {
        super(level, worldViewport, myInputProcessor, ingameCursorManager);
    }

    protected List<Zelle> gettZellenListe(Vector2 position, Vector2 positionAnfang) {
        List<Zelle> zellenListe = new ArrayList<>();
        int width = (int) (position.x - positionAnfang.x);
        boolean anfangLinks = width > 0;
        width = Math.abs(width);
        int height = (int) (position.y - positionAnfang.y);
        boolean anfangUnten = height > 0;
        height = Math.abs(height);

        int xCount = (int) positionAnfang.x;
        do {
            int yCount = (int) positionAnfang.y;
            do {
                Zelle z = getLevel().getWorldMap().gett(xCount, yCount);
                if (z != null) {
                    zellenListe.add(z);
                }

                if (anfangUnten && yCount < positionAnfang.y + height) {
                    yCount += 1;
                } else if (!anfangUnten && yCount > positionAnfang.y - height) {
                    yCount -= 1;
                } else {
                    break;
                }
            } while (true);

            if (anfangLinks && xCount < positionAnfang.x + width) {
                xCount += 1;
            } else if (!anfangLinks && xCount > positionAnfang.x - width) {
                xCount -= 1;
            } else {
                break;
            }
        } while (true);
        return zellenListe;
    }

    public void aktivieren(Vector2 positionAnfang) {
        super.vorschlagen();
        setPositionAnfang(GeometryUtils.gettIndizes(positionAnfang));
    }
}
