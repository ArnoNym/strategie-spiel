package de.arnonym.strategiespiel.spielSchirm.ui.auswahlkaesten.deprecated;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.viewport.Viewport;

import java.util.ArrayList;
import java.util.List;

import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.ausbeuten.CompAbbaubar;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.lists.listenSysteme.SysAbbaubar;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.lists.listenSysteme.SysFreuchteWachsen;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.SysRender;
import de.arnonym.strategiespiel.spielSchirm.ui.IngameCursorManager;
import de.arnonym.strategiespiel.spielSchirm.Level;
import de.arnonym.strategiespiel.spielSchirm.ui.MyInputProcessor;
import de.arnonym.strategiespiel.spielSchirm.worldMap.Zelle;
import de.arnonym.strategiespiel.framework.werkzeuge.Assets;
import de.arnonym.strategiespiel.framework.werkzeuge.Enums;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.GeometryUtils;

public class AuswahlkastenZellenAbbauen {
    /*private final SysAbbaubar sysAbbaubar;
    private final SysRender sysRender;
    private final SysDorf sysSpielerDorf;
    private final SysFreuchteWachsen sysFreuchteWachsen;

    private Enums.EnumAbbauen enumAbbauen; /* Legt fest "welcher" Auswahlkasten aktiviert sei soll.
    Der zum ernten oder der zum faellen
    private boolean loeschen; /* Legt fest ob abzubauen entfernt oder entsprechend dem enum abbauen
    hinzugefuegt werden soll. Unterscheidung, da man nicht immer zum entfernen Auswahlkasten
    wechseln muessen soll
    private List<CompAbbaubar> ausgewaehlteRessourcenListe;

    public AuswahlkastenZellenAbbauen(Level level, Viewport worldViewport,
                                      MyInputProcessor myInputProcessor,
                                      IngameCursorManager ingameCursorManager,
                                      SysAbbaubar sysAbbaubar,
                                      SysFreuchteWachsen sysFreuchteWachsen) {
        super(level, worldViewport, myInputProcessor, ingameCursorManager);
        this.sysAbbaubar = sysAbbaubar;
        this.sysFreuchteWachsen = sysFreuchteWachsen;
    }

    @Override
    public void update() {
        if (getEnumAktiv() == Enums.EnumAktiv.INAKTIV) {
            return;
        }

        // Zeichne alle bereits zum Abbau markierten Zellen
        switch (enumAbbauen) {
            case ABBAUEN_VERERBEN:
                for (CompAbbaubar compAbbaubar : sysSpielerDorf.compAbbaubarList) {
                    getZellenMarkierungRenderer().add(compAbbaubar.getCompPosition().getPosition(),
                            Assets.instance.tiledMapAssets.abbauenAlles, false);
                }
                break;
            case NUR_ERNTEN:
                for (CompAbbaubar compAbbaubar : sysSpielerDorf.compAbbauNurErntenList) {
                    getZellenMarkierungRenderer().add(compAbbaubar.getCompPosition().getPosition(),
                            Assets.instance.tiledMapAssets.abbauenErnten, false);
                }
                break;
        }

        if (getEnumAktiv() == Enums.EnumAktiv.VORSCHLAGEN) {
            ausgewaehlteRessourcenListe = new ArrayList<>(); //Vor dem ersten return

            setPosition(GeometryUtils.gettIndizes(
                    getViewport().unproject(new Vector2(Gdx.input.getX(), Gdx.input.getY()))));

            List<Zelle> zellenListe = gettZellenListe(getPosition(), getPositionAnfang());
            if (zellenListe.isEmpty()) {
                return;
            }

            for (Zelle z : zellenListe) {
                TextureRegion textureRegion = gettMarkierenTextureRegion(z);
                getZellenMarkierungRenderer().addPosition(z.getPosition(), textureRegion, true);
            }
        }
    }

    private TextureRegion gettMarkierenTextureRegion(Zelle z) {
        StatischesObjekt so = z.getStatischesObjekt();
        if (so == null || !(so instanceof Ressource)) {
            return Assets.instance.tiledMapAssets.durchsichtigWeiss;
        }

        Ressource res = (Ressource) so;
        if (loeschen) {
            if (res.getEnumAbbauen() == Enums.EnumAbbauen.NEIN) {
                return Assets.instance.tiledMapAssets.durchsichtigWeiss;
            } else {
                //ausgewaehlteRessourcenListe.add(res);
                return Assets.instance.tiledMapAssets.entfernenMarkierung;
            }
        }

        Enums.EnumAbbauen resEnumAbbauen = res.getEnumAbbauen();
        if (resEnumAbbauen == enumAbbauen) {
            return pickDefaultTextureRegion(resEnumAbbauen);
        }

        switch (enumAbbauen) {
            case ABBAUEN_VERERBEN:
                //ausgewaehlteRessourcenListe.add(res);
                return Assets.instance.tiledMapAssets.abbauenAlles;
            case NUR_ERNTEN:
                if (res instanceof RessourceWachsen
                        && ((RessourceWachsen) res).issErntenMoeglich()) {
                    //ausgewaehlteRessourcenListe.add(res);
                    return Assets.instance.tiledMapAssets.abbauenErnten;
                }
            default:
                return pickDefaultTextureRegion(resEnumAbbauen);
        }
    }

    private TextureRegion pickDefaultTextureRegion(Enums.EnumAbbauen resEnumAbbauen) {
        switch (resEnumAbbauen) {
            case ABBAUEN_VERERBEN: return Assets.instance.tiledMapAssets.abbauenAlles;
            case NUR_ERNTEN: return Assets.instance.tiledMapAssets.abbauenErnten;
            default: return Assets.instance.tiledMapAssets.durchsichtigWeiss;
        }
    }

    // AKTIVIEREN ETC /////////////////////////////////////////////////////////////////////////////

    @Deprecated
    @Override
    public void aktivieren() {
        super.aktivieren();
    }
    public void auswaehlen(Enums.EnumAbbauen enumAbbauen) {
        super.aktivieren();
        this.enumAbbauen = enumAbbauen;


        StatischesObjekt so = zellenListe.get(0).getStatischesObjekt();
        loeschen = so != null && so instanceof Ressource
                && ((Ressource) so).getEnumAbbauen() != Enums.EnumAbbauen.NEIN
                && ((Ressource) so).getEnumAbbauen() == enumAbbauen;
    }

    @Override
    public void auswaehlen() {
        super.auswaehlen();

        setEnumAktiv(Enums.EnumAktiv.AKTIV);

        List<Ressource> abzubauendeRessourcenListe = getLevel().getAbzubauendeRessourcenListe();
        for (Ressource r : ausgewaehlteRessourcenListe) {
            if (loeschen || enumAbbauen == Enums.EnumAbbauen.NEIN) {
                r.setEnumAbbauen(Enums.EnumAbbauen.NEIN);
                abzubauendeRessourcenListe.remove(r);
            } else {
                r.setEnumAbbauen(enumAbbauen);
                if (!abzubauendeRessourcenListe.contains(r)) {
                    abzubauendeRessourcenListe.add(r);
                }
            }

            /*if (r.getPosition().x >= positionUntenLinks.x
                    && r.getPosition().y >= positionUntenLinks.y
                    && r.getPosition().x <= positionObenRechts.x
                    && r.getPosition().y <= positionObenRechts.y) {
                if (getLevel().getAbzubauendeRessourcenListe().contains(r)) {
                    r.setEnumAbbauen(enumAbbauen);
                } else if (enumAbbauen == Enums.EnumAbbauen.NUR_ERNTEN) {
                    if (r instanceof RessourceNachwachsen && ((RessourceNachwachsen) r).issErntenMoeglich()) {
                        getLevel().getAbzubauendeRessourcenListe().addFrei(r);
                        r.setEnumAbbauen(enumAbbauen);
                    }
                } else {
                    getLevel().getAbzubauendeRessourcenListe().addFrei(r);
                    r.setEnumAbbauen(enumAbbauen);
                }
            }
        }
    }*/
}
