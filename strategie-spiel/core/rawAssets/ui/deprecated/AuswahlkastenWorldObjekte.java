package de.arnonym.strategiespiel.spielSchirm.ui.auswahlkaesten.deprecated;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.viewport.Viewport;

import de.arnonym.strategiespiel.spielSchirm.ui.IngameCursorManager;
import de.arnonym.strategiespiel.spielSchirm.Level;
import de.arnonym.strategiespiel.spielSchirm.ui.MyInputProcessor;
import de.arnonym.strategiespiel.framework.werkzeuge.Assets;
import de.arnonym.strategiespiel.framework.werkzeuge.Enums;
import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenOptik;

/**
 * Created by LeonPB on 08.04.2018.
 */

public class AuswahlkastenWorldObjekte extends Auswahlkasten {
    private Vector2 positionUntenLinks;
    private Vector2 positionObenRechts;
    private NinePatch ninePatch;
    private Enums.EnumAbbauen enumAbbauen;

    public AuswahlkastenWorldObjekte(Level level, Viewport worldViewport,
                                     MyInputProcessor myInputProcessor,
                                     IngameCursorManager ingameCursorManager) {
        super(level, worldViewport, myInputProcessor, ingameCursorManager);
        ninePatch = new NinePatch(Assets.instance.levelGuiAssets.auswahlKastenA,
                KonstantenOptik.AUSWAHLKASTEN_ECKEN_BREITE,
                KonstantenOptik.AUSWAHLKASTEN_ECKEN_BREITE,
                KonstantenOptik.AUSWAHLKASTEN_ECKEN_HOEHE,
                KonstantenOptik.AUSWAHLKASTEN_ECKEN_HOEHE);
    }

    @Override
    public void update() { //Weil das Update keine Auswirkung auf die Spielwelt hat geht das so
        if (getEnumAktiv() == Enums.EnumAktiv.VORSCHLAGEN) {
            setPosition(getViewport().unproject(new Vector2(Gdx.input.getX(), Gdx.input.getY())));
            float x = Math.min(getPositionAnfang().x, getPosition().x);
            float y = Math.min(getPositionAnfang().y, getPosition().y);
            float width = Math.abs(getPositionAnfang().x - getPosition().x);
            float height = Math.abs(getPositionAnfang().y - getPosition().y);
            positionUntenLinks = (new Vector2(x, y));
            positionObenRechts = (new Vector2(x+width, y+height));

            if (width > 1 && height > 1) {
                throw new IllegalStateException("!! FUNKTIONIER OHNE SPRITEBACTH NICHT !!");
                // !!!!!!!!!!!!!!! FUNKTIONIER OHNE SPRITEBACTH NICHT !!!!!!!!!!!! ninePatch.draw(spriteBatch, x, y, width, height);
            }
        }
    }

    // AKTIVIEREN ETC /////////////////////////////////////////////////////////////////////////////

    @Deprecated
    @Override
    public void aktivieren() {
        super.aktivieren();
    }
    public void auswaehlen(Enums.EnumAbbauen enumAbbauen) {
        super.aktivieren();
        this.enumAbbauen = enumAbbauen;
        getIngameCursorManager().setCursorStatus(Enums.EnumIngameWorldCursorStatus.NICHTS);
    }

    public void aktivieren(Vector2 positionAnfang) {
        super.vorschlagen();
        setPositionAnfang(positionAnfang);
    }

    @Override
    public void auswaehlen() {
        super.auswaehlen();

       setEnumAktiv(Enums.EnumAktiv.AKTIV);

        /* FUNKTIONSUNFAEHIG
        for (Ressource r : getLevel().getRessourcenListe()) {
            if (r.getPosition().x >= positionUntenLinks.x
                    && r.getPosition().y >= positionUntenLinks.y
                    && r.getPosition().x <= positionObenRechts.x
                    && r.getPosition().y <= positionObenRechts.y) {
                if (getLevel().getAbzubauendeRessourcenListe().contains(r)) {
                    r.setEnumAbbauen(enumAbbauen);
                } else if (enumAbbauen == Enums.EnumAbbauen.NUR_ERNTEN) {
                    if (r instanceof RessourceWachsen && ((RessourceWachsen) r).issErntenMoeglich()) {
                        getLevel().getAbzubauendeRessourcenListe().add(r);
                        r.setEnumAbbauen(enumAbbauen);
                    }
                } else {
                    getLevel().getAbzubauendeRessourcenListe().add(r);
                    r.setEnumAbbauen(enumAbbauen);
                }
            }
        }*/
    }
}
