package de.arnonym.strategiespiel.spielSchirm.ui;

import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.viewport.Viewport;

import java.io.Serializable;
import java.util.ArrayList;

import de.arnonym.strategiespiel.spielSchirm.objekte.ClickAbleObjekt;

/* Wird verwendet um DBs und Zellen klick zu machen */
public class MyInputProcessor extends ArrayList<ClickAbleObjekt> implements InputProcessor,
        Serializable {
    private transient Viewport worldViewport;
    private boolean aktiv = true;

    public MyInputProcessor(Viewport worldViewport) {
        lade(worldViewport);
    }

    public void lade(Viewport worldViewport) {
        this.worldViewport = worldViewport;
    }

    // OVERRIDE INPUT_PROCESSOR ///////////////////////////////////////////////////////////////////

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        if (!aktiv) {
            return false;
        }
        Vector2 clickWorldPosition = worldViewport.unproject(new Vector2(screenX, screenY));
        for (ClickAbleObjekt ca : this) {
            if (ca.issHit(clickWorldPosition)) {
                ca.gotHit();
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }

    // EINFACHE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public void setAktiv(boolean aktiv) {
        this.aktiv = aktiv;
    }
}
