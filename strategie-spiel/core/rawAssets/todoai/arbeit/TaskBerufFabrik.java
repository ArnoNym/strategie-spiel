package de.arnonym.strategiespiel.spielSchirm.ecsErgaenzung.stp.tasks.arbeit;

import de.arnonym.strategiespiel.spielSchirm.ecsErgaenzung.stp.plaene.PlanTauschen.nehmen.PlanNehmenBeruf;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.ai.ai.StepStack;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.ai.ai.tasks.Task;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.ai.ai.tasks.TaskWeglegen;
import de.arnonym.strategiespiel.spielSchirm.objekte.kollisionsobjekte.gebaude.gebBeruf.GebBerufHerstFabrik;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.Gegenstand;
import de.arnonym.strategiespiel.framework.werkzeuge.collections.hashMaps.IntIntHashMap;
import de.arnonym.strategiespiel.spielSchirm.objekte.dorfbewohnerAi.Dorfbewohner;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.ai.ai.todoai.StepHerstellen;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dorfbewohner.tauschen.step.StepNehmen;
import de.arnonym.strategiespiel.framework.werkzeuge.Enums;
import de.arnonym.strategiespiel.spielSchirm.balancing.IdsGegenstaende;

public class TaskBerufFabrik extends Task {
    public TaskBerufFabrik(Dorfbewohner dorfbewohner) {
        super(dorfbewohner);
    }

    @Override
    public StepStack planeAlleSteps(StepStack stepStack) {
        GebBerufHerstFabrik gebBerufHerstFabrik = (GebBerufHerstFabrik) getDorfbewohner().getGebBeruf();

        if (getDorfbewohner().getKeinPfadListe().contains(gebBerufHerstFabrik)) {
            return stepStack;
        }

        StepHerstellen stepHerstellen = new StepHerstellen(getDorfbewohner(), null, gebBerufHerstFabrik, false);
        Enums.EnumStepStatus stepStatus = stepHerstellen.versuchen();
        if (stepStatus == Enums.EnumStepStatus.OK) {
            gebBerufHerstFabrik.setGenugInput(true);
            gebBerufHerstFabrik.setGenugOutputKaeufer(true);
            stepStack.add(stepHerstellen, true);
            System.out.println("TaskBerufFabrik ::: ENTSCHEIDUNG === Herstellen)");
            return stepStack;
        }

        StepStack ausliefernStepStack = ausliefern(stepStack, gebBerufHerstFabrik);
        if (ausliefernStepStack == null) {
            gebBerufHerstFabrik.setGenugOutputKaeufer(true);
        } else {
            stepStack = ausliefernStepStack;
            if (stepStack.isEmpty()) {
                gebBerufHerstFabrik.setGenugOutputKaeufer(false);
            } else {
                gebBerufHerstFabrik.setGenugOutputKaeufer(true);
                System.out.println("TaskBerufFabrik ::: ENTSCHEIDUNG === Ausliefern)");
                return stepStack;
            }
        }

        if (!stepHerstellen.versuchenPruefeInput()) { //Nicht genug Input
            System.out.println("TaskBerufFabrik ::: ENTSCHEIDUNG === Einkaufen)");
            gebBerufHerstFabrik.setGenugInput(false);
            stepStack.addAll(new PlanNehmenBeruf(getDorfbewohner(), getDorfbewohner()
                    .getLevel().getWorldMap().gett(getDorfbewohner().getPositionMitte()),
                    gebBerufHerstFabrik));
        }

        return stepStack;
    }

    private StepStack ausliefern(StepStack stepStack, GebBerufHerstFabrik gebBerufHerstFabrik) {
        // Alles ausliefern, dass kein Input fuer das aktuelle Produkt ist
        IntIntHashMap nehmenAusHashMap = new IntIntHashMap();
        IntIntHashMap aktuelllesProduktInputHashMap = IdsGegenstaende.gett(gebBerufHerstFabrik
                .getProduktId()).gettInputHashMap();
        for (Gegenstand g : gebBerufHerstFabrik.getGegenstandListe()) {
            int mengeVerfuegbar = g.getMengeVerfuegbar();
            if (mengeVerfuegbar > 0 && !aktuelllesProduktInputHashMap.containsKey(g.gettId())) {
                int gId = g.gettId();
                int mengeNehmen = Math.min(getDorfbewohner().getGegenstandListe().getDurchId(gId)
                        .getMengeBisVoll(), mengeVerfuegbar);
                nehmenAusHashMap.put(g.gettId(), mengeNehmen);
            }
        }

        if (nehmenAusHashMap.isEmpty()) {
            return null; // Es muss nichts weggebracht werden
        }

        StepNehmen stepNehmen = new StepNehmen(getDorfbewohner(), null,
                gebBerufHerstFabrik, getDorfbewohner().getGegenstandListe(), nehmenAusHashMap,
                Enums.EnumReservieren.JETZT_RESERVIEREN);

        if (stepNehmen.versuchen() == Enums.EnumStepStatus.OK) {
            Task task = new TaskWeglegen(getDorfbewohner(), gebBerufHerstFabrik.gettEigentuemer(), false);
            if (task.versuchen() == Enums.EnumStepStatus.OK) {
                stepStack.addAll(task, true);
                stepStack.add(stepNehmen, true);
            } else {
                stepNehmen.failNachVersuchenVorAnfangen();
            }
        }
        return stepStack;
    }

    @Override
    public StepStack planeNaechstenStep(StepStack stepStack) {
        return stepStack;
    }
}
