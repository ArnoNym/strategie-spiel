package de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.ai.ai.todoai;

import de.arnonym.strategiespiel.spielSchirm.objekte.dorfbewohnerAi.Dorfbewohner;
import de.arnonym.strategiespiel.spielSchirm.objekte.dorfbewohnerAi.berufe.Beruf;
import de.arnonym.strategiespiel.spielSchirm.objekte.kollisionsobjekte.gebaude.gebBeruf.GebBerufHerst;
import de.arnonym.strategiespiel.spielSchirm.objekte.kollisionsobjekte.gebaude.gebBeruf.GebBerufHerstFabrik;
import de.arnonym.strategiespiel.spielSchirm.worldMap.Zelle;
import de.arnonym.strategiespiel.framework.werkzeuge.Enums;

/**
 * Created by LeonPB on 22.08.2018.
 */

public class StepHerstellen extends StepArbeit {
    private final Beruf beruf;

    public StepHerstellen(Dorfbewohner dorfbewohner, Zelle start_nl, GebBerufHerst ziel) {
        super(dorfbewohner, start_nl, ziel, ziel.getGeld());
        beruf = dorfbewohner.getBeruf();
    }

    @Override
    protected Enums.EnumStepStatus versuchen_or_or() {
        Enums.EnumStepStatus superStepStatus = super.versuchen_or_or();
        if (superStepStatus != Enums.EnumStepStatus.OK) {
            return superStepStatus;
        }

        if (versuchenPruefeInput() && versuchenPruefeOutput()) {
            return Enums.EnumStepStatus.OK;
        }
        return Enums.EnumStepStatus.FAIL;
    }

    public boolean versuchenPruefeInput() {
        return((GebBerufHerst) getZiel()).genugInput();
    }

    public boolean versuchenPruefeOutput() {
        return ((GebBerufHerst) getZiel()).genugOutputPlatz();
    }

    @Override
    protected float gettPassendeProduktivitaet() {
        return beruf.gettProduktivitaet();
    }

    @Override
    public boolean pruefenObFail() {
        return super.pruefenObFail() || getDorfbewohner().getBeruf() != beruf;
    }

    @Override
    protected void schadenZufuegen() {
        if (getZiel() instanceof GebBerufHerstFabrik) {
            getDorfbewohner().schadenZufuegen(gettArbeitszeit(),
                    ((GebBerufHerstFabrik) getZiel()).gettHerstellen().gettHerstellenSchadenArray());
        }
    }
}
