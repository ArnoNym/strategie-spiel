package de.arnonym.strategiespiel;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

import java.util.logging.Level;
import java.util.logging.Logger;

import de.arnonym.strategiespiel.framework.werkzeuge.CursorManger;
import de.arnonym.strategiespiel.spielSchirm.SpielSchirm;
import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenOptik;

public class Strategiespiel extends Game {
	public static final Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
	public static Skin skin;
	public static CursorManger cursorManger;
	public static BitmapFont bitmapFont;

	@Override
	public void create(){
		skin = new Skin(Gdx.files.internal(KonstantenOptik.SKIN_PFAD));
		cursorManger = new CursorManger();

		logger.setLevel(Level.FINEST);
		logger.finer("tetsttstst");

		bitmapFont = new BitmapFont();
		bitmapFont.getRegion().getTexture().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
		setScreen(new SpielSchirm());
	}
}