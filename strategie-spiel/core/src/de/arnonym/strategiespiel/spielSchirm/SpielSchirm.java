package de.arnonym.strategiespiel.spielSchirm;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenOptik;

/**
 * Created by LeonPB on 07.04.2018.
 */

public class SpielSchirm extends ScreenAdapter {

    private SpriteBatch spriteBatch;
    private World world;

    @Override
    public void show() {
        AssetManager assetManager = new AssetManager();
        Assets.instance.init(assetManager);

        spriteBatch = new SpriteBatch();
        world = new World(this, spriteBatch);
    }

    @Override
    public void render(float delta) {
        world.update(delta);

        Gdx.gl.glClearColor(KonstantenOptik.HINTERGRUND_FARBE.r,
                KonstantenOptik.HINTERGRUND_FARBE.g, KonstantenOptik.HINTERGRUND_FARBE.b,
                KonstantenOptik.HINTERGRUND_FARBE.a);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        //Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT | (Gdx.graphics.getBufferFormat().coverageSampling?GL20.GL_COVERAGE_BUFFER_BIT_NV:0));

        world.render(spriteBatch, delta);
    }

    @Override
    public void resize(int width, int height) {
        world.resize(width, height);
    }

    @Override
    public void hide() {
        super.hide();
    }

    @Override
    public void pause() {
        super.pause();
    }

    @Override
    public void resume() {
        super.resume();
    }

    @Override
    public void dispose() {
        super.dispose();
    }

    // EINFACHE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public World getWorld() {
        return world;
    }

    public void setWorld(World world) {
        this.world = world;
    }
}
