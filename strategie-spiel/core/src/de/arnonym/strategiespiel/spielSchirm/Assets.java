package de.arnonym.strategiespiel.spielSchirm;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetErrorListener;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;

import java.io.Serializable;
import java.util.Map;
import java.util.TreeMap;

import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenOptik;

/**
 * Created by LeonPB on 07.04.2018.
 */

public class Assets implements Disposable, AssetErrorListener, Serializable {

    public static final String LOGGING_TAG = Assets.class.getName();
    public static final Assets instance = new Assets();
    private AssetManager assetManager;

    private static final TreeMap<Integer, TextureRegion> idTextureRegionHashMap = new TreeMap<>();
    private static final TreeMap<Integer, Animation> idAnimationHashMap = new TreeMap<>();

    //Hier Assetklassen von neuen Objekten einfuegen
    public DorfbewohnerAAssets dorfbewohnerAAssets;
    public GuiAssets guiAssets;
    public LevelGuiAssets levelGuiAssets;
    public IconAssets iconAssets;
    public PfeilAssets pfeilAssets;
    public BauerAAssets bauerAAssets;
    public BildschirmtastenAssets bildschirmtastenAssets;
    public GebaudeAssets gebaudeAssets;
    public RessourcenAssets ressourcenAssets;
    public TiledMapAssets tiledMapAssets;
    public SpielweltSymboleAssets spielweltSymboleAssets;

    private Assets(){
    }

    public void init(AssetManager assetManager){
        this.assetManager = assetManager;
        assetManager.setErrorListener(this);
        assetManager.load(KonstantenOptik.TEXTUREN_ATLAS, TextureAtlas.class);
        assetManager.finishLoading();

        TextureAtlas atlas = assetManager.get(KonstantenOptik.TEXTUREN_ATLAS);
        //Hier Assetklassen von neuen Objekten deklarieren
        guiAssets = new GuiAssets(atlas);
        levelGuiAssets = new LevelGuiAssets(atlas);
        iconAssets = new IconAssets(atlas);
        pfeilAssets = new PfeilAssets(atlas);
        bauerAAssets = new BauerAAssets(atlas);
        bildschirmtastenAssets = new BildschirmtastenAssets(atlas);
        dorfbewohnerAAssets = new DorfbewohnerAAssets(atlas);
        gebaudeAssets = new GebaudeAssets(atlas);
        ressourcenAssets = new RessourcenAssets(atlas);
        tiledMapAssets = new TiledMapAssets(atlas);
        spielweltSymboleAssets = new SpielweltSymboleAssets(atlas);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public void error(AssetDescriptor asset, Throwable throwable) {
        Gdx.app.error(LOGGING_TAG, "Couldn't load asset: " + asset.fileName, throwable);
    }

    @Override
    public void dispose() {
        assetManager.dispose();
    }

    // IDS ////////////////////////////////////////////////////////////////////////////////////////

    public int getId(TextureRegion textureRegion) {
        for (Map.Entry<Integer, TextureRegion> entry : idTextureRegionHashMap.entrySet()) {
            if (entry.getValue() == textureRegion) {
                return entry.getKey();
            }
        }
        throw new IllegalArgumentException();
    }

    public int getId(Animation animation) {
        for (Map.Entry<Integer, Animation> entry : idAnimationHashMap.entrySet()) {
            if (entry.getValue() == animation) {
                return entry.getKey();
            }
        }
        throw new IllegalArgumentException();
    }

    public TextureRegion getTextureRegion(int id) {
        return idTextureRegionHashMap.get(id);
    }

    public Animation getAnimation(int id) {
        return idAnimationHashMap.get(id);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////

    public class GuiAssets {

        public GuiAssets(TextureAtlas atlas) {
        }
    }

    public class LevelGuiAssets {
        public final TextureAtlas.AtlasRegion auswahlKastenA;
        public final TextureAtlas.AtlasRegion gebaudeLeisteA;
        public final TextureAtlas.AtlasRegion fensterDickerRand;
        public final TextureAtlas.AtlasRegion fensterDuennerRand;
        public final TextureAtlas.AtlasRegion fensterPin;
        public final TextureAtlas.AtlasRegion fensterPinDeaktiv;
        public final TextureAtlas.AtlasRegion fensterButtonMoveable;
        public final TextureAtlas.AtlasRegion fensterButtonNotMoveable;
        public final TextureAtlas.AtlasRegion fensterSchliessenKreuz;

        public LevelGuiAssets(TextureAtlas atlas) {
            auswahlKastenA = atlas.findRegion("auswahlKastenA");
            idTextureRegionHashMap.put(0, auswahlKastenA);
            gebaudeLeisteA = atlas.findRegion("ingameHudA1");
            idTextureRegionHashMap.put(1, gebaudeLeisteA);
            fensterDickerRand = atlas.findRegion("fensterA1");
            idTextureRegionHashMap.put(2, fensterDickerRand);
            fensterDuennerRand = atlas.findRegion("fensterA2");
            idTextureRegionHashMap.put(3, fensterDuennerRand);
            fensterButtonMoveable = atlas.findRegion("ingameGuiA1");
            idTextureRegionHashMap.put(4, fensterButtonMoveable);
            fensterButtonNotMoveable = atlas.findRegion("ingameGuiA2");
            idTextureRegionHashMap.put(5, fensterButtonNotMoveable);
            fensterPin = atlas.findRegion("ingameGuiA3");
            idTextureRegionHashMap.put(6, fensterPin);
            fensterPinDeaktiv = atlas.findRegion("ingameGuiA4");
            idTextureRegionHashMap.put(7, fensterPinDeaktiv);
            fensterSchliessenKreuz = atlas.findRegion("ingameGuiA5");
            idTextureRegionHashMap.put(8, fensterSchliessenKreuz);
        }
    }

    public class IconAssets {
        public final TextureAtlas.AtlasRegion iconBlank;
        public final TextureAtlas.AtlasRegion iconRahmenHaken;
        public final TextureAtlas.AtlasRegion iconRahmenKreuz;
        public final TextureAtlas.AtlasRegion iconRahmenKannKlicken;
        public final TextureAtlas.AtlasRegion iconRahmenKlick;

        public final TextureAtlas.AtlasRegion iconLager;
        public final TextureAtlas.AtlasRegion iconLagerMaterialEinfach;
        public final TextureAtlas.AtlasRegion iconLagerNahrungEinfach;
        public final TextureAtlas.AtlasRegion iconHaus;
        public final TextureAtlas.AtlasRegion iconFabrik;
        public final TextureAtlas.AtlasRegion iconFarm;
        public final TextureAtlas.AtlasRegion iconWissenschaft;
        public final TextureAtlas.AtlasRegion iconFreizeit;

        public final TextureAtlas.AtlasRegion iconGegenstandHolt;
        public final TextureAtlas.AtlasRegion iconGegenstandWerkzeugeEinfach;
        public final TextureAtlas.AtlasRegion iconGegenstandBeeren;
        public final TextureAtlas.AtlasRegion iconGegenstandEisen;
        public final TextureAtlas.AtlasRegion iconGegenstandKleidungEinfach;

        public IconAssets(TextureAtlas atlas) {
            iconBlank = atlas.findRegion("iconMitRahmenA1");
            idTextureRegionHashMap.put(9, iconBlank);

            iconRahmenHaken = atlas.findRegion("iconMitRahmenA2");
            idTextureRegionHashMap.put(10, iconRahmenHaken);
            iconRahmenKreuz = atlas.findRegion("iconMitRahmenA3");
            idTextureRegionHashMap.put(11, iconRahmenKreuz);
            iconRahmenKannKlicken = atlas.findRegion("iconMitRahmenA4");
            idTextureRegionHashMap.put(12, iconRahmenKannKlicken);
            iconRahmenKlick = atlas.findRegion("iconMitRahmenA5");
            idTextureRegionHashMap.put(13, iconRahmenKlick);

            iconLager = atlas.findRegion("iconA6");
            idTextureRegionHashMap.put(14, iconLager);
            iconLagerMaterialEinfach = atlas.findRegion("iconA7");
            idTextureRegionHashMap.put(15, iconLagerMaterialEinfach);
            iconLagerNahrungEinfach = atlas.findRegion("iconA8");
            idTextureRegionHashMap.put(17, iconLagerNahrungEinfach);
            iconHaus = atlas.findRegion("iconA9");
            idTextureRegionHashMap.put(18, iconHaus);
            iconFabrik = atlas.findRegion("iconA10");
            idTextureRegionHashMap.put(19, iconFabrik);
            iconFarm = atlas.findRegion("iconA11");
            idTextureRegionHashMap.put(20, iconFarm);
            iconWissenschaft = atlas.findRegion("iconA12");
            idTextureRegionHashMap.put(21, iconWissenschaft);
            iconFreizeit = atlas.findRegion("iconA13");
            idTextureRegionHashMap.put(22, iconFreizeit);

            iconGegenstandHolt = atlas.findRegion("iconGegenstandA1");
            idTextureRegionHashMap.put(23, iconGegenstandHolt);
            iconGegenstandWerkzeugeEinfach = atlas.findRegion("iconGegenstandA2");
            idTextureRegionHashMap.put(24, iconGegenstandWerkzeugeEinfach);
            iconGegenstandBeeren = atlas.findRegion("iconGegenstandA3");
            idTextureRegionHashMap.put(25, iconGegenstandBeeren);
            iconGegenstandEisen = atlas.findRegion("iconGegenstandA4");
            idTextureRegionHashMap.put(26, iconGegenstandEisen);
            iconGegenstandKleidungEinfach = atlas.findRegion("iconGegenstandA5");
            idTextureRegionHashMap.put(27, iconGegenstandKleidungEinfach);
        }
    }

    public class SpielweltSymboleAssets {
        public final TextureAtlas.AtlasRegion schwebeGeld;
        public final TextureAtlas.AtlasRegion schwebeGueter;

        public final TextureAtlas.AtlasRegion statischKeinInput;
        public final TextureAtlas.AtlasRegion statischKeinOutput;
        public final TextureAtlas.AtlasRegion statischKeinEssen;
        public final TextureAtlas.AtlasRegion statischKeinGeld;
        public final TextureAtlas.AtlasRegion statischKeinZuhause;
        public final TextureAtlas.AtlasRegion statischKeinPfad;

        public SpielweltSymboleAssets(TextureAtlas atlas) {
            schwebeGeld = atlas.findRegion("schwebeIconsA1");
            idTextureRegionHashMap.put(28, schwebeGeld);
            schwebeGueter = atlas.findRegion("schwebeIconsA2");
            idTextureRegionHashMap.put(29, schwebeGueter);

            statischKeinInput = atlas.findRegion("statischeSymboleA1");
            idTextureRegionHashMap.put(30, statischKeinInput);
            statischKeinOutput = atlas.findRegion("statischeSymboleA2");
            idTextureRegionHashMap.put(31, statischKeinOutput);
            statischKeinEssen = atlas.findRegion("statischeSymboleA3");
            idTextureRegionHashMap.put(32, statischKeinEssen);
            statischKeinGeld = atlas.findRegion("statischeSymboleA4");
            idTextureRegionHashMap.put(33, statischKeinGeld);
            statischKeinZuhause = atlas.findRegion("statischeSymboleA5");
            idTextureRegionHashMap.put(34, statischKeinZuhause);
            statischKeinPfad = atlas.findRegion("statischeSymboleA6");
            idTextureRegionHashMap.put(35, statischKeinPfad);
        }
    }

    public class DorfbewohnerAAssets {
        public final AtlasRegion dorfbewohnerStehen;
        public final AtlasRegion dorfbewohnerStehenAusgewaehlt;
        public final Animation untenLaufenAnimation;
        public final Animation obenLaufenAnimation;
        public final Animation seiteLaufenAnimation;
        public DorfbewohnerAAssets(TextureAtlas atlas) {
            dorfbewohnerStehen = atlas.findRegion("dorfbewohnerC1");
            idTextureRegionHashMap.put(36, dorfbewohnerStehen);
            dorfbewohnerStehenAusgewaehlt = atlas.findRegion("dorfbewohnerA2");
            idTextureRegionHashMap.put(37, dorfbewohnerStehenAusgewaehlt);

            Array<AtlasRegion> untenLaufenAnimationArray = new Array<>();
            untenLaufenAnimationArray.add(atlas.findRegion("dorfbewohnerB1"));
            untenLaufenAnimationArray.add(atlas.findRegion("dorfbewohnerB2"));
            untenLaufenAnimationArray.add(atlas.findRegion("dorfbewohnerB3"));
            untenLaufenAnimationArray.add(atlas.findRegion("dorfbewohnerB4"));
            untenLaufenAnimation = new Animation(0.2f, untenLaufenAnimationArray, Animation.PlayMode.LOOP);
            idAnimationHashMap.put(38, untenLaufenAnimation);

            Array<AtlasRegion>seiteLaufenAnimationArray = new Array<>();
            seiteLaufenAnimationArray.add(atlas.findRegion("dorfbewohnerB5"));
            seiteLaufenAnimationArray.add(atlas.findRegion("dorfbewohnerB6"));
            seiteLaufenAnimationArray.add(atlas.findRegion("dorfbewohnerB7"));
            seiteLaufenAnimationArray.add(atlas.findRegion("dorfbewohnerB8"));
            seiteLaufenAnimation = new Animation(0.2f, seiteLaufenAnimationArray, Animation.PlayMode.LOOP);
            idAnimationHashMap.put(39, seiteLaufenAnimation);

            Array<AtlasRegion> obenLaufenAnimationArray = new Array<>();
            obenLaufenAnimationArray.add(atlas.findRegion("dorfbewohnerB9"));
            obenLaufenAnimationArray.add(atlas.findRegion("dorfbewohnerB10"));
            obenLaufenAnimationArray.add(atlas.findRegion("dorfbewohnerB11"));
            obenLaufenAnimationArray.add(atlas.findRegion("dorfbewohnerB12"));
            obenLaufenAnimation = new Animation(0.2f, obenLaufenAnimationArray, Animation.PlayMode.LOOP);
            idAnimationHashMap.put(40, obenLaufenAnimation);
        }
    }

    public class GebaudeAssets {
        public final TextureAtlas.AtlasRegion bodenStapel;
        public final TextureAtlas.AtlasRegion lagerMaterial;
        public final TextureAtlas.AtlasRegion lagerMaterialTuer;
        public final TextureAtlas.AtlasRegion lagerNahrung;
        public final TextureAtlas.AtlasRegion hausEinfach;
        public final TextureAtlas.AtlasRegion berufSchmied;
        public final TextureAtlas.AtlasRegion berufFarmGross;
        public final TextureAtlas.AtlasRegion kircheKlein;
        public final TextureAtlas.AtlasRegion schuleKlein;

        public final TextureAtlas.AtlasRegion baugrube2x2;
        public final TextureAtlas.AtlasRegion baugrube3x3;
        public final TextureAtlas.AtlasRegion baugrube4x4;
        public final TextureAtlas.AtlasRegion baugrube5x5;
        public GebaudeAssets(TextureAtlas atlas) {
            bodenStapel = atlas.findRegion("bodenStapelA1");
            idTextureRegionHashMap.put(41, bodenStapel);
            lagerNahrung = atlas.findRegion("lagerA1");
            idTextureRegionHashMap.put(42, lagerNahrung);
            lagerMaterial = atlas.findRegion("lagerA2");
            idTextureRegionHashMap.put(43, lagerMaterial);
            lagerMaterialTuer = atlas.findRegion("lagerA3");
            idTextureRegionHashMap.put(44, lagerMaterialTuer);
            hausEinfach = atlas.findRegion("hausA1");
            idTextureRegionHashMap.put(45, hausEinfach);
            berufSchmied = atlas.findRegion("lagerA4");
            idTextureRegionHashMap.put(46, berufSchmied);
            berufFarmGross = atlas.findRegion("farmA1");
            idTextureRegionHashMap.put(47, berufFarmGross);
            kircheKlein = atlas.findRegion("kircheA1");
            idTextureRegionHashMap.put(48, kircheKlein);
            schuleKlein = atlas.findRegion("schuleA1");
            idTextureRegionHashMap.put(49, schuleKlein);

            baugrube2x2 = atlas.findRegion("baugrube2x2A1");
            idTextureRegionHashMap.put(50, baugrube2x2);
            baugrube3x3 = atlas.findRegion("baugrube3x3A1");
            idTextureRegionHashMap.put(51, baugrube3x3);
            baugrube4x4 = atlas.findRegion("baugrube4x4A1");
            idTextureRegionHashMap.put(52, baugrube4x4);
            baugrube5x5 = atlas.findRegion("baugrube5x5A1");
            idTextureRegionHashMap.put(53, baugrube5x5);
        }
    }

    public class RessourcenAssets {
        public final TextureAtlas.AtlasRegion tanneJung;
        public final TextureAtlas.AtlasRegion tanneMittel;
        public final TextureAtlas.AtlasRegion tanneAltGesund;
        public final TextureAtlas.AtlasRegion tanneAltGekippt;
        public final TextureAtlas.AtlasRegion tanneAltVergammeln;
        public final TextureAtlas.AtlasRegion beerenJung;
        public final TextureAtlas.AtlasRegion beerenMittel;
        public final TextureAtlas.AtlasRegion beerenAltLeer;
        public final TextureAtlas.AtlasRegion beerenAltHalbvoll;
        public final TextureAtlas.AtlasRegion beerenAltVoll;
        public RessourcenAssets(TextureAtlas atlas) {
            // Tanne //
            tanneJung = atlas.findRegion("baumA6");
            idTextureRegionHashMap.put(54, tanneJung);

            tanneMittel = atlas.findRegion("baumA5");
            idTextureRegionHashMap.put(55, tanneMittel);

            tanneAltGesund = atlas.findRegion("baumA1");
            idTextureRegionHashMap.put(56, tanneAltGesund);
            tanneAltGekippt = atlas.findRegion("baumA2");
            idTextureRegionHashMap.put(57, tanneAltGekippt);
            tanneAltVergammeln = atlas.findRegion("baumA3");
            idTextureRegionHashMap.put(58, tanneAltVergammeln);

            // Beeren //
            beerenJung = atlas.findRegion("beerenBuschA1");
            idTextureRegionHashMap.put(59, beerenJung);

            beerenMittel = atlas.findRegion("beerenBuschA2");
            idTextureRegionHashMap.put(60, beerenMittel);

            beerenAltLeer = atlas.findRegion("beerenBuschA3");
            idTextureRegionHashMap.put(61, beerenAltLeer);
            beerenAltHalbvoll = atlas.findRegion("beerenBuschA4");
            idTextureRegionHashMap.put(63, beerenAltHalbvoll);
            beerenAltVoll = atlas.findRegion("beerenBuschA5");
            idTextureRegionHashMap.put(64, beerenAltVoll);
        }
    }

    public class TiledMapAssets {
        public final TextureAtlas.AtlasRegion grid;
        public final TextureAtlas.AtlasRegion durchsichtigWeiss;
        public final TextureAtlas.AtlasRegion durchsichtigRot;
        public final TextureAtlas.AtlasRegion durchsichtigGruen;
        public final TextureAtlas.AtlasRegion entfernenMarkierung;

        public final TextureAtlas.AtlasRegion abbauen;
        public final TextureAtlas.AtlasRegion ernten;
        public TiledMapAssets(TextureAtlas atlas) {
            grid = atlas.findRegion("gridA1");
            idTextureRegionHashMap.put(65, grid);
            durchsichtigWeiss = atlas.findRegion("gridA2");
            idTextureRegionHashMap.put(66, durchsichtigWeiss);
            durchsichtigRot = atlas.findRegion("gridA3");
            idTextureRegionHashMap.put(67, durchsichtigRot);
            durchsichtigGruen = atlas.findRegion("gridA4");
            idTextureRegionHashMap.put(68, durchsichtigGruen);
            entfernenMarkierung = atlas.findRegion("entfernenMarkierungA1");
            idTextureRegionHashMap.put(69, entfernenMarkierung);

            abbauen = atlas.findRegion("abbauenMarkierungA1");
            idTextureRegionHashMap.put(70, abbauen);
            ernten = atlas.findRegion("abbauenMarkierungA2");
            idTextureRegionHashMap.put(71, ernten);
        }
    }

    public class BauerAAssets {
        /* public final Animation linksStehenAnimation;
         public final Animation linksLaufenAnimation;
         public final Animation linksSchlagenAnimation;
         public final Animation linksStuermenAnimation;
         public final Animation linksSterbenAnimation;
 */
        public BauerAAssets(TextureAtlas atlas) {/*
            Array<AtlasRegion> linksStehenAnimationArray = new Array<AtlasRegion>();
            linksStehenAnimationArray.addFrei(atlas.findRegion("bauerA1"));
            linksStehenAnimationArray.addFrei(atlas.findRegion("bauerA2"));
            linksStehenAnimationArray.addFrei(atlas.findRegion("bauerA3"));
            linksStehenAnimation = new Animation(KonstantenAssets.BAUER_A_STEHEN_ANIMATIONSDAUER_EINES_FRAMES, linksStehenAnimationArray, Animation.PlayMode.LOOP);

            Array<AtlasRegion> linksLaufenAnimationArray = new Array<AtlasRegion>();
            linksLaufenAnimationArray.addFrei(atlas.findRegion("bauerA4"));
            linksLaufenAnimationArray.addFrei(atlas.findRegion("bauerA5"));
            linksLaufenAnimationArray.addFrei(atlas.findRegion("bauerA6"));
            linksLaufenAnimationArray.addFrei(atlas.findRegion("bauerA7"));
            linksLaufenAnimationArray.addFrei(atlas.findRegion("bauerA8"));
            linksLaufenAnimationArray.addFrei(atlas.findRegion("bauerA9"));
            linksLaufenAnimation = new Animation(KonstantenAssets.BAUER_A_LAUFEN_ANIMATIONSDAUER_EINES_FRAMES, linksLaufenAnimationArray, Animation.PlayMode.LOOP);

            Array<AtlasRegion>linksSchlagenAnimationArray = new Array<AtlasRegion>();
            linksSchlagenAnimationArray.addFrei(atlas.findRegion("bauerA10"));
            linksSchlagenAnimationArray.addFrei(atlas.findRegion("bauerA11"));
            linksSchlagenAnimationArray.addFrei(atlas.findRegion("bauerA12"));
            linksSchlagenAnimationArray.addFrei(atlas.findRegion("bauerA13"));
            linksSchlagenAnimationArray.addFrei(atlas.findRegion("bauerA14"));
            linksSchlagenAnimationArray.addFrei(atlas.findRegion("bauerA15"));
            linksSchlagenAnimationArray.addFrei(atlas.findRegion("bauerA16"));
            linksSchlagenAnimation = new Animation(KonstantenAssets.BAUER_A_SCHLAGEN_ANIMATIONSDAUER_EINES_FRAMES, linksSchlagenAnimationArray, Animation.PlayMode.LOOP);

            Array<AtlasRegion>linksStuermenAnimationArray = new Array<AtlasRegion>();
            linksStuermenAnimationArray.addFrei(atlas.findRegion("bauerA17"));
            linksStuermenAnimationArray.addFrei(atlas.findRegion("bauerA18"));
            linksStuermenAnimationArray.addFrei(atlas.findRegion("bauerA19"));
            linksStuermenAnimationArray.addFrei(atlas.findRegion("bauerA20"));
            linksStuermenAnimationArray.addFrei(atlas.findRegion("bauerA21"));
            linksStuermenAnimationArray.addFrei(atlas.findRegion("bauerA22"));
            linksStuermenAnimation = new Animation(KonstantenAssets.BAUER_A_STUERMEN_ANIMATIONSDAUER_EINES_FRAMES, linksStuermenAnimationArray, Animation.PlayMode.LOOP);

            Array<AtlasRegion>linksSterbenAnimationArray = new Array<AtlasRegion>();
            linksSterbenAnimationArray.addFrei(atlas.findRegion("bauerA23"));
            linksSterbenAnimationArray.addFrei(atlas.findRegion("bauerA24"));
            linksSterbenAnimationArray.addFrei(atlas.findRegion("bauerA25"));
            linksSterbenAnimationArray.addFrei(atlas.findRegion("bauerA26"));
            linksSterbenAnimationArray.addFrei(atlas.findRegion("bauerA27"));
            linksSterbenAnimationArray.addFrei(atlas.findRegion("bauerA28"));
            linksSterbenAnimationArray.addFrei(atlas.findRegion("bauerA29"));
            linksSterbenAnimation = new Animation(KonstantenAssets.BAUER_A_STERBEN_ANIMATIONSDAUER_EINES_FRAMES, linksSterbenAnimationArray, Animation.PlayMode.LOOP);*/
        }
    }

    public class PfeilAssets {
        public final TextureAtlas.AtlasRegion pfeil;
        public PfeilAssets(TextureAtlas atlas) {
            pfeil = atlas.findRegion("pfeilA");
        }
    }

    public class BildschirmtastenAssets {
        public final TextureAtlas.AtlasRegion links;
        public final TextureAtlas.AtlasRegion rechts;
        public final TextureAtlas.AtlasRegion oben;
        public final TextureAtlas.AtlasRegion unten;

        public BildschirmtastenAssets(TextureAtlas atlas) {
            links = atlas.findRegion("bildschirmtasten1");
            rechts = atlas.findRegion("bildschirmtasten2");
            oben = atlas.findRegion("bildschirmtasten3");
            unten = atlas.findRegion("bildschirmtasten4");
        }
    }
}
