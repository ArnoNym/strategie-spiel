package de.arnonym.strategiespiel.spielSchirm.forschungUndWissen.schule;

import de.arnonym.strategiespiel.framework.stp.Ai;
import de.arnonym.strategiespiel.framework.ecs.component.Comp;
import de.arnonym.strategiespiel.framework.stp.StpComp;
import de.arnonym.strategiespiel.framework.stp.Task;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.departments.bildung.TeachDepartmentComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.bewegung.BewegungComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.EinkommenComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.werte.BildungComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.werte.HabitalwerteComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.werte.ProduktivitaetComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.rendern.RenderComp;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.framework.stp.StpMachine;

public class AiSchueler extends Ai {
    public final BewegungComp bewegungComp;
    public final BildungComp bildungComp;
    public final EinkommenComp einkommenComp;
    public final HabitalwerteComp habitalwerteComp;
    public final ProduktivitaetComp produktivitaetComp;
    private TeachDepartmentComp schuleArbeitgeberComp;

    public AiSchueler(Entity entity, RenderComp renderComp, StpComp stpComp,
                      BewegungComp bewegungComp, BildungComp bildungComp, EinkommenComp einkommenComp,
                      HabitalwerteComp habitalwerteComp, ProduktivitaetComp produktivitaetComp,
                      int prioritaetId) {
        super(entity, renderComp, stpComp, new Comp[]{bildungComp, einkommenComp,
                habitalwerteComp, produktivitaetComp}, prioritaetId);
        this.bewegungComp = bewegungComp;
        this.bildungComp = bildungComp;
        this.einkommenComp = einkommenComp;
        this.habitalwerteComp = habitalwerteComp;
        this.produktivitaetComp = produktivitaetComp;
    }

    @Override
    public Task versuchen() {
        if (schuleArbeitgeberComp == null) {
            return null;
        }
        return new TaskSchueler(stpComp, bewegungComp, bildungComp,
                schuleArbeitgeberComp, null);
    }

    public TeachDepartmentComp getSchuleArbeitgeberComp() {
        return schuleArbeitgeberComp;
    }

    public void setSchuleArbeitgeberComp(TeachDepartmentComp schuleArbeitgeberComp) {
        this.schuleArbeitgeberComp = schuleArbeitgeberComp;
    }
}
