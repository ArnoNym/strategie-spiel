package de.arnonym.strategiespiel.spielSchirm.forschungUndWissen.schule;

import de.arnonym.strategiespiel.framework.stp.StpComp;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.departments.bildung.TeachDepartmentComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.bewegung.BewegungComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.EinkommenComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.werte.HabitalwerteComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.werte.ProduktivitaetComp;
import de.arnonym.strategiespiel.framework.stp.stacks.StepStack;
import de.arnonym.strategiespiel.spielSchirm.arbeit.step.schule.StepLehren;
import de.arnonym.strategiespiel.framework.stp.Task;

public class TaskBerufLehrer extends Task {
    public final TeachDepartmentComp schuleArbeitgeberComp;
    public final BewegungComp bewegungComp;
    public final EinkommenComp einkommenComp;
    public final HabitalwerteComp habitalwerteComp;
    public final ProduktivitaetComp produktivitaetComp;

    public TaskBerufLehrer(TeachDepartmentComp schuleArbeitgeberComp,
                           StpComp stpComp, BewegungComp bewegungComp,
                           EinkommenComp einkommenComp, HabitalwerteComp habitalwerteComp,
                           ProduktivitaetComp produktivitaetComp) {
        super(stpComp, schuleArbeitgeberComp, einkommenComp, habitalwerteComp,
                produktivitaetComp);
        this.schuleArbeitgeberComp = schuleArbeitgeberComp;
        this.bewegungComp = bewegungComp;
        this.einkommenComp = einkommenComp;
        this.habitalwerteComp = habitalwerteComp;
        this.produktivitaetComp = produktivitaetComp;
    }

    @Override
    public void planeAlleSteps(StepStack stepStack) {
        stepStack.handle(
                new StepLehren(schuleArbeitgeberComp, bewegungComp, einkommenComp, habitalwerteComp,
                        produktivitaetComp, null),
                true);
    }

    @Override
    public void planeNaechstenStep(StepStack stepStack) {

    }
}
