package de.arnonym.strategiespiel.spielSchirm.forschungUndWissen.labor;

import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;
import java.util.List;

import de.arnonym.strategiespiel.spielSchirm.Dorf;
import de.arnonym.strategiespiel.spielSchirm.World;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.EmployerComp;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.VarEmployerComp;
import de.arnonym.strategiespiel.spielSchirm.balancing.IdsGebaude;
import de.arnonym.strategiespiel.spielSchirm.balancing.IdsGegenstaende;
import de.arnonym.strategiespiel.framework.ecs.component.Comp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.gegenstaende.GegenstaendeComp1;
import de.arnonym.strategiespiel.spielSchirm.gebaude.Gebaude;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.vorstufe.EntityGegInfoList;
import de.arnonym.strategiespiel.spielSchirm.info.muster.Info;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterOnKlick.OnKlickFenster;

public class Labor extends Gebaude<LaborInfo> {
    public Labor(World world, Dorf dorf, LaborInfo laborInfofo, Vector2 position) {
        super(world, dorf, laborInfofo, position);
    }

    @Override
    protected OnKlickFenster definiereGebaude(List<Comp> activateCompList,
                                              List<Comp> loescheCompList) {
        GegenstaendeComp1 compGegenstaende1 = new GegenstaendeComp1(this, compRender,
                new EntityGegInfoList(info.maxPlatz, false).addAll());
        activateCompList.add(compGegenstaende1);

        List<Info> infoList = new ArrayList<>(info.arbeitenAnIds.length);
        for (int id : info.arbeitenAnIds) {
            if (IdsGegenstaende.contains(id)) {
                infoList.add(IdsGegenstaende.get(id));
            } else {
                infoList.add(IdsGebaude.get(id));
            }
        }
        EmployerComp<Info> forschungEmployerComp = new VarEmployerComp<>(dorf.employmentMatcher, this, bewegungszielComp,
                eigentuemerComp, infoList.toArray(new Info[infoList.size()]), null,
                info.maxStellen);
        activateCompList.add(forschungEmployerComp);

        return null;
    }
}
