package de.arnonym.strategiespiel.spielSchirm.forschungUndWissen.schule;

import com.badlogic.gdx.math.Vector2;

import de.arnonym.strategiespiel.framework.stp.StpComp;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.departments.bildung.TeachDepartmentComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.bewegung.BewegungComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.werte.BildungComp;
import de.arnonym.strategiespiel.framework.stp.stacks.StepStack;
import de.arnonym.strategiespiel.spielSchirm.arbeit.step.schule.StepSchueler;
import de.arnonym.strategiespiel.framework.stp.Task;

public class TaskSchueler extends Task {
    public final BewegungComp bewegungComp;
    public final BildungComp bildungComp;
    public final TeachDepartmentComp schuleArbeitgeberComp;
    private final Vector2 start_nl;

    public TaskSchueler(StpComp stpComp, BewegungComp bewegungComp,
                        BildungComp bildungComp,
                        TeachDepartmentComp schuleArbeitgeberComp, Vector2 start_nl) {
        super(stpComp, bewegungComp, bildungComp, schuleArbeitgeberComp);
        this.bewegungComp = bewegungComp;
        this.bildungComp = bildungComp;
        this.schuleArbeitgeberComp = schuleArbeitgeberComp;
        this.start_nl = start_nl;
    }

    @Override
    public void planeAlleSteps(StepStack stepStack) {
        StepSchueler step = new StepSchueler(schuleArbeitgeberComp, bewegungComp, bildungComp,
                start_nl);
        stepStack.handle(step, true);
    }

    @Override
    public void planeNaechstenStep(StepStack stepStack) {

    }
}
