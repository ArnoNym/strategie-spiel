package de.arnonym.strategiespiel.spielSchirm.forschungUndWissen.schule;

import com.badlogic.gdx.math.Vector2;

import de.arnonym.strategiespiel.spielSchirm.Dorf;
import de.arnonym.strategiespiel.spielSchirm.World;
import de.arnonym.strategiespiel.spielSchirm.gebaude.Gebaude;
import de.arnonym.strategiespiel.spielSchirm.gebaude.GebaudeFactory;

public class SchuleFactory extends GebaudeFactory<SchuleInfo> {
    public SchuleFactory(World world, Dorf dorf, SchuleInfo info) {
        super(world, dorf, info);
    }

    @Override
    public Gebaude create(Vector2 position) {
        return new SchuleMitLabor(world, dorf, info, position);
    }
}
