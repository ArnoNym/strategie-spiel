package de.arnonym.strategiespiel.spielSchirm.forschungUndWissen.schule;

import com.badlogic.gdx.math.Vector2;

import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.departments.bildung.BildungInfo;
import de.arnonym.strategiespiel.spielSchirm.balancing.IdsBildung;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.departments.bildung.ForschungAssignment;
import de.arnonym.strategiespiel.spielSchirm.forschungUndWissen.labor.LaborInfo;
import de.arnonym.strategiespiel.framework.werkzeuge.textures.TextureHashMap;

public class SchuleInfo extends LaborInfo {
    public final int maxLehrer;
    public final int maxSchueler;
    public final BildungInfo bildungInfo;

    public SchuleInfo(int id, String name, String beschreibung, TextureHashMap textureHashMap, Vector2 fundament, int[][] bauMaterial, float bauZeit, ForschungAssignment forschungAssignment_nl, int maxForscher, int[] arbeitenAnIds, int maxPlatz, int maxLehrer, int maxSchueler, BildungInfo bildungInfo) {
        super(id, name, beschreibung, textureHashMap, fundament, bauMaterial, bauZeit, forschungAssignment_nl, maxForscher, arbeitenAnIds, maxPlatz);
        this.maxLehrer = maxLehrer;
        this.maxSchueler = maxSchueler;
        this.bildungInfo = bildungInfo;
    }
}
