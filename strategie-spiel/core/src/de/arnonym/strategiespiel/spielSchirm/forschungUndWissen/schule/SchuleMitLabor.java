package de.arnonym.strategiespiel.spielSchirm.forschungUndWissen.schule;

import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;
import java.util.List;

import de.arnonym.strategiespiel.Strategiespiel;
import de.arnonym.strategiespiel.spielSchirm.Dorf;
import de.arnonym.strategiespiel.spielSchirm.World;
import de.arnonym.strategiespiel.framework.ecs.component.Comp;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.VarEmployerComp;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.departments.bildung.ForschungDepartmentComp;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.departments.bildung.TeachDepartmentComp;
import de.arnonym.strategiespiel.spielSchirm.balancing.IdsGebaude;
import de.arnonym.strategiespiel.spielSchirm.balancing.IdsGegenstaende;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.gegenstaende.GegenstaendeComp1;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.name.NameComp;
import de.arnonym.strategiespiel.spielSchirm.gebaude.Gebaude;
import de.arnonym.strategiespiel.spielSchirm.info.muster.Info;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterOnKlick.OnKlickFenster;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterOnKlick.SchuleOnKlickFenster;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.vorstufe.EntityGegInfoList;

public class SchuleMitLabor extends Gebaude<SchuleInfo> {
    public SchuleMitLabor(World world, Dorf dorf, SchuleInfo info, Vector2 position) {
        super(world, dorf, info, position);
    }

    @Override
    protected OnKlickFenster definiereGebaude(List<Comp> activateCompList,
                                              List<Comp> loescheCompList) {
        GegenstaendeComp1 compGegenstaende1 = new GegenstaendeComp1(this, compRender,
                new EntityGegInfoList(info.maxPlatz, false).addAll());
        activateCompList.add(compGegenstaende1);

        List<Info> infoList = new ArrayList<>(info.arbeitenAnIds.length);
        for (int id : info.arbeitenAnIds) {
            if (IdsGegenstaende.contains(id)) {
                infoList.add(IdsGegenstaende.get(id));
            } else {
                infoList.add(IdsGebaude.get(id));
            }
        }

        VarEmployerComp<Info> employerComp = new VarEmployerComp<>(dorf.employmentMatcher, this,
                bewegungszielComp, eigentuemerComp, infoList.toArray(new Info[infoList.size()]),
                null, info.maxStellen);
        TeachDepartmentComp teachDepartmentComp = new TeachDepartmentComp(this, employerComp,
                info.maxSchueler, info.bildungInfo);
        new ForschungDepartmentComp(dorf.forschungsstandList, this, employerComp, compGegenstaende1);
        activateCompList.add(employerComp);

        NameComp nameComp = new NameComp(this, "elementary school");

        return new SchuleOnKlickFenster(world, Strategiespiel.skin,
                employerComp, teachDepartmentComp, compGegenstaende1, nameComp, compRender);
    }
}
