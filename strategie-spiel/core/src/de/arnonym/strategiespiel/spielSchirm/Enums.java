package de.arnonym.strategiespiel.spielSchirm;

import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenBalance;
import de.arnonym.strategiespiel.framework.werkzeuge.numbers.unit.Unit;

/**
 * Created by LeonPB on 07.04.2018.
 */

public class Enums {

    // PROPERTY_CHANGE ////////////////////////////////////////////////////////////////////////////

    public static final String PROBERTY_CHANGE_DEPARTMENT = "department";
    public static final String PROBERTY_CHANGE_PRODUCT = "product";
    public static final String PROBERTY_CHANGE_ZELLE = "zelle";
    public static final String PC_EVENT_NAME_GELD = "geld";
    public static final String PC_EVENT_NAME_MASTER_TASCHE = "masterTasche";
    public static final String PC_EVENT_ADD = "update";
    public static final String PC_EVENT_REMOVE = "remove";

    ///////////////////////////////////////////////////////////////////////////////////////////////

    public enum EnumDopplung {
        IGNORE,
        EXCEPTION,
        ALLOW
    }
    public enum EnumNull {
        IGNORE,
        EXCEPTION,
        ALLOW
    }

    public enum EnumAlter {
        JUNG,
        MITTEL,
        ALT
    }
    public enum EnumAddTexture {
        ZUSATZ,
        ERSETZE_WENN_UNGLEICH, // Die Dauer laeuft smit weiter
        ERSETZE_IMMER
    }
    public enum EnumAktion {
        STANDARD_PRIMAERMENGE_VOLL,
        PRIMAERMENGE_MITTEL,
        PRIMAERMENGE_WENIG,
        PRIMAERMENGE_LEER,

        SEKUNDAERMENGE_VOLL,
        SEKUNDAERMENGE_MITTEL,
        SEKUNDAERMENGE_WENIG,

        GEHEN_OBEN,
        GEHEN_OBEN_RECHTS,
        GEHEN_RECHTS,
        GEHEN_RECHTS_UNTEN,
        GEHEN_UNTEN,
        GEHEN_UNTEN_LINKS,
        GEHEN_LINKS,
        GEHEN_LINKS_OBEN,

        ABBAUEN,
        WEGLEGEN,
        NEHMEN,

        EINTRETEN_IN_THIS,
        AUSTRETEN_AUS_THIS,
        THIS_TRITT_EIN,
        THIS_TRITT_AUS,
    }
    public enum EnumStatusInFamilie {
        KIND,
        ELTERN
    }
    public enum EnumGegenstandKlasse {
        NAHRUNG,
        SPIELZEUG,
        WERKZEUG,
        MEDIZIN
    }
    public enum EnumMengen {
        LEGEN,
        VERFUEGBAR,
        NEHMEN
    }
    public enum EnumKoerperteil {
        HAENDE,
        FUESSE,
        LUNGE,
        KOPF,
        SONSTIGES
    }
    public enum EnumHandeln {
        NAECHSTER_GEGENSTAND,
        NAECHSTES_GEBAUDE,
        PLANEN_GEFAILT
    }
    public enum EnumInfoFenster {
        MAUS,
        PARENT,
        BLAUPAUSE
    }
    public enum EnumValue {
        NUMBER, // zB 131
        MULT, // zB 1.1 ( = 1 + PERCENT)
        PERCENT // zB 0.1
    }
    public enum EnumPauseStart {
        JETZT,
        JETZT_ZUFAELLIGE_RESTDAUER,
        VORBEI
    }
    public enum EnumErforscht {
        JA,
        NEIN,
        DABEI
    }
    public enum EnumIngameWorldCursorStatus {
        ZELLE_UND_GEBAUDE_UND_CURSOR,
        ZELLE_UND_FLACHEN,
        ZELLE,
        NICHTS
    }
    public enum EnumCursor {
        PFEIL,
        UNSICHTBAR
    }
    public enum EnumResNwHolz {
        KEIN_HOLZ,
        NUR_HOLZ,
        FREUCHTE_WENN_HOLZ
    }
    public enum EnumResNwStufe {
        JUNG,
        MITTEL,
        ALT
    }
    public enum EnumGebBeruf {
        SCHMIED,
        FARM
    }
    public enum EnumZellenMarkiererFarbe {
        ROT,
        WEISS
    }
    public enum EnumBeruf {
        BUERO, //Machen nur Bueroarbeit im Gebaude selbst. Versuchen: Buero -> Lieferant -> Handwerk
        HANDWERK, //Machen Arbeit im Gebaude selbst (zB Schmieden) und Arbeit ausserhalb (zB Ressourcen abbauen). Versuchen: Handwerk -> Lieferant -> Buero
        LIEFERANT //Beschafft neue Gegenstaende und liefert produzierte Ware aus. Versuchen: Lieferant -> Handwerk -> Buero
    }
    public enum EnumAktiv {
        AKTIV,
        VORSCHLAGEN,
        INAKTIV
    }
    public enum EnumAbbauen {
        ABBAUEN_VERERBEN,
        NUR_ERNTEN,
        NEIN
    }
    public enum EnumDnaStatus {
        STAT,
        MULT,
        NUTZEN
    }
    public enum EnumReservieren {
        BEREITS_RESERVIERT,
        JETZT_RESERVIEREN,
        SPAETER_RESERVIEREN
    }
    public enum EnumStpStatus {
        FAIL_BEI_GEHE_ZU,
        ERLEDIGT,
        FAIL,
        OK
    }
    public enum EnumOnKlickFensterStatus {
        INAKTIV,
        AKTIV_FREI,
        AKTIV_LOCKED_ON_MYOBJEKT
    }
    public enum EnumGegenstand {
        EIWEISS,
        VITAMINE,
        KOHLENHYDRATE_GESUND,
        KOHLENHYDRATE_UNGESUND,
        MATERIAL
    }
    public enum EnumKnopfreiheRichtung {
        NACH_LINKS,
        NACH_RECHTS,
        NACH_OBEN,
        NACH_UNTEN
    }
    public enum EnumPositionen {
        OBEN_LINKS,
        OBEN_RECHTS,
        UNTEN_LINKS,
        UNTEN_RECHTS
    }
    public enum EnumKnopfStatus {
        VERSTECKT_UND_INAKTIV,
        INAKTIV,
        AKTIV,
        GEDRUECKT
    }
    public enum EnumDbStates {
        ESSEN,
        SCHLAFEN,
        ARBEITEN_FREI,
        ARBEITEN_BERUF,
        IDLE
    }
    public enum EnumAusgewaehlt {
        NICHT_AUSGEWAEHLT,
        AUSGEWAEHLT,
        AUFGABE_BEENDEN
    }
    public enum EnumPrioritaet {
        NIEDRIG,
        MITTEL,
        HOCH,
    }
    public enum EnumProdukt {
        WERKZEUG
    }

    public enum EnumLaufen {
        LAUFEN,
        STEHEN
    }
    public enum EnumEinfacheRichtung { //Springen und laufen seperat damit man auch in der Luft noch die Richtung ändern kann
        LINKS,
        RECHTS,
        OBEN,
        UNTEN
    }
    public enum EnumRichtung { //Springen und laufen seperat damit man auch in der Luft noch die Richtung ändern kann
        LINKS,
        LINKS_OBEN,
        LINKS_UNTEN,
        RECHTS,
        RECHTS_OBEN,
        RECHTS_UNTEN,
        OBEN,
        UNTEN
    }

    public enum EnumAngreifen {
        ANGREIFEN, NICHT_ANGREIFEN, BLOCKEN
    }
    public enum EnumAngreifenRichtung {
        LINKS, RECHTS, MITTE
    }

    public enum EnumSpringen {
        SPRINGEND, FALLEND, BODEN,
        STUERZEND //Damit man eine zusätzliche Haltung hat bei der man keine Kontrolle mehr hat
    }

    public enum EnumSieht {
        JA_LINKS, JA_RECHTS, NEIN
    }

    public enum EnumHintergruendeFriedhofHoehe {
        MITTE, UNTEN
    }

    /*
    public enum EnumHintergruendeFriedhof {
        HF1, HF2, HF3, HF4;
        public static EnumHintergruendeFriedhof Zufaellig(){
            int z = MathUtils.random(5); //Damit die normale Fläche öfter drankommt
            if(z==1){return HF1;}
            else if(z==2){return HF2;}
            else if(z==4){return HF4;}
            else{return HF3;}
        }
    }
    public static EnumHintergruendeFriedhof Zufaellig(){
        int z = MathUtils.random(4);
        if(z==1){return EnumHintergruendeFriedhof.HF1;}
        else if(z==2){return EnumHintergruendeFriedhof.HF2;}
        else if(z==3){return EnumHintergruendeFriedhof.HF3;}
        else{return EnumHintergruendeFriedhof.HF4;}
    }
    */
}
