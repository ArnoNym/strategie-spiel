package de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.departments.bildung;

import de.arnonym.strategiespiel.spielSchirm.arbeit.auftrag.AssignmentMaterial;
import de.arnonym.strategiespiel.spielSchirm.zahlen.schaden.MusterSchaden;

public class ForschungAssignment extends AssignmentMaterial {
    public ForschungAssignment(int id, float optimaleProduktivitaetKopf, float optimaleProduktivitaetKoerper, float arbeitszeit, int[][] idMengenArray) {
        super(id, optimaleProduktivitaetKopf, optimaleProduktivitaetKoerper, arbeitszeit, idMengenArray);
    }

    public ForschungAssignment(int id, float optimaleProduktivitaetKopf, float optimaleProduktivitaetKoerper, float arbeitszeit, MusterSchaden musterSchaden, int[][] idMengenArray) {
        super(id, optimaleProduktivitaetKopf, optimaleProduktivitaetKoerper, arbeitszeit, musterSchaden, idMengenArray);
    }
}
