package de.arnonym.strategiespiel.spielSchirm.arbeit.task.liefernArbeitszeit.ausbeuten;

import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import de.arnonym.strategiespiel.framework.listener.StoryTeller;
import de.arnonym.strategiespiel.framework.stp.Step;
import de.arnonym.strategiespiel.framework.stp.StpComp;
import de.arnonym.strategiespiel.framework.werkzeuge.collections.Collector;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.bewegung.BewegungComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.gegenstaende.GegenstaendeComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.GeldComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.herrschaft.EigentuemerComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.werte.HabitalwerteComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.werte.ProduktivitaetComp;
import de.arnonym.strategiespiel.framework.stp.stacks.StepStack;
import de.arnonym.strategiespiel.spielSchirm.arbeit.task.liefernArbeitszeit.TaskLoswerden;
import de.arnonym.strategiespiel.spielSchirm.arbeit.step.ausbeuten.StepAusbeuten;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dorfbewohner.tauschen.step.StepLegeIn;
import de.arnonym.strategiespiel.spielSchirm.bodenStapel.BodenStapelFactory;
import de.arnonym.strategiespiel.spielSchirm.ui.aufsteigendeSymbole.AufsteigendesSymbolFactory;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.listSystems.privateListSystems.SysLager;
import de.arnonym.strategiespiel.spielSchirm.arbeit.task.liefernArbeitszeit.TransportArbeitszeitTask;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.GeometryUtils;

public abstract class TaskAusbeuten<AusbeutbarComp extends de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.ausbeuten.AusbeutbarComp>
        extends TransportArbeitszeitTask {

    public final AufsteigendesSymbolFactory aufsteigendesSymbolFactory;
    public final BodenStapelFactory bodenStapelFactory;
    public final SysLager sysLager;

    public final Collection<AusbeutbarComp> ausbeutbarComps;

    public final EigentuemerComp neuerEigentuemerComp; /** Wenn nicht null ueberschreibt es den
    Eigentuemer der Ressource! */

    public final BewegungComp aBewegungComp;
    public final GegenstaendeComp aGegenstaendeComp;
    public final HabitalwerteComp aHabitalwerteComp;
    public final ProduktivitaetComp aProduktivitaetComp;

    public TaskAusbeuten(AufsteigendesSymbolFactory aufsteigendesSymbolFactory,
                         BodenStapelFactory bodenStapelFactory,

                         SysLager sysLager,

                         Collection<AusbeutbarComp> zAusbeutbarComps,

                         EigentuemerComp neuerEigentuemerComp, boolean overrideEigentuemer,

                         GeldComp gGeldComp, int salary,

                         StpComp aStpComp, BewegungComp aBewegungComp,
                         GeldComp aGeldComp,
                         GegenstaendeComp aGegenstaendeComp, HabitalwerteComp aHabitalwerteComp,
                         ProduktivitaetComp aProduktivitaetComp) {
        super(gGeldComp, salary, aGeldComp, aStpComp,
                new Collector<StoryTeller>(aBewegungComp, aGegenstaendeComp, aHabitalwerteComp,
                        aProduktivitaetComp)
                        .addMayNull(neuerEigentuemerComp)
                        .toArray(new StoryTeller[]{}));
        this.aufsteigendesSymbolFactory = aufsteigendesSymbolFactory;
        this.bodenStapelFactory = bodenStapelFactory;
        this.sysLager = sysLager;
        this.ausbeutbarComps = zAusbeutbarComps;
        this.neuerEigentuemerComp = neuerEigentuemerComp;
        this.aBewegungComp = aBewegungComp;
        this.aGegenstaendeComp = aGegenstaendeComp;
        this.aHabitalwerteComp = aHabitalwerteComp;
        this.aProduktivitaetComp = aProduktivitaetComp;


        /*todo


        // Decide Eigentuemer
        if (ueberschreibeEigentuemer) {
            this.eigentuemerComp = neuerEigentuemerComp;
        } else {
            EigentuemerComp ec = zAusbeutbarComp.eigentumComp.getEigentuemer();
            if (ec == null) {
                if (neuerEigentuemerComp == null) {
                    throw new IllegalArgumentException();
                }
                ec = neuerEigentuemerComp;
            }
            this.eigentuemerComp = ec;
        }

        // Decide Geld
        GeldComp gGeldComp;
        if (gGeldComp_nl != null) {
            gGeldComp = gGeldComp_nl;
        } else {
            gGeldComp = eigentuemerComp.geldComp;
        }*/
    }

    @Override
    public final void planeAlleSteps(StepStack stepStack) {

    }

    @Override
    public final void planeNaechstenStep(StepStack stepStack) {
        if (!stepStack.isEmpty()) {
            return;
        }
        Step currentStep = getCurrentStep();
        if (currentStep instanceof StepLegeIn) {
            return;
        }

        if (ausbeuten(stepStack)) {
            return;
        }

        if (aGegenstaendeComp.gegenstandListe.issMenge(true, true, false)) {
            if (!(currentStep instanceof StepAusbeuten)) {
                throw new IllegalStateException();
            }
            //EigentuemerComp eigentuemerComp = ((StepAusbeuten) currentStep).eigentuemerComp;
            stepStack.handle(new TaskLoswerden(aufsteigendesSymbolFactory, bodenStapelFactory,
                    sysLager, neuerEigentuemerComp, gGeldComp, salary, stpComp,
                    aBewegungComp, aGeldComp, aGegenstaendeComp,
                    null, false, null));
        }
    }

    private boolean ausbeuten(StepStack stepStack) {
        List<de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.ausbeuten.AusbeutbarComp> ausbeutbarList = new ArrayList<>();
        for (de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.ausbeuten.AusbeutbarComp ca : ausbeutbarComps) {
            if (!aBewegungComp.unerreichbarList.contains(ca.bewegungszielComp)) {
                ausbeutbarList.add(ca);
            }
        }

        while (!ausbeutbarList.isEmpty()) {
            int size = ausbeutbarList.size();
            Vector2[] zielPositions = new Vector2[size];
            for (int i = 0; i < size; i++) {
                zielPositions[i] = ausbeutbarList.get(i).bewegungszielComp.compKollisionLaufen
                        .positionComp.position;
            }
            de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.ausbeuten.AusbeutbarComp ausbeutbarComp = ausbeutbarList.get(GeometryUtils.minDistanzZiel(
                    aBewegungComp.positionComp.position, zielPositions));

            StepAusbeuten step = gettStep(ausbeutbarComp);
            switch (step.versuchen()) {
                case OK:
                    stepStack.handle(step, true);
                    return true;
                case ERLEDIGT:
                    return false;
                case FAIL:
                case FAIL_BEI_GEHE_ZU:
                    ausbeutbarList.remove(ausbeutbarComp);
                    break;
            }
        }

        return false;
    }

    protected abstract StepAusbeuten gettStep(de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.ausbeuten.AusbeutbarComp ausbeutbarComp);
}
