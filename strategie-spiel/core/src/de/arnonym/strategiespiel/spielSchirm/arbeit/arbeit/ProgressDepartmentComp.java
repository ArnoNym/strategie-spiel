package de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit;

import de.arnonym.strategiespiel.framework.ecs.component.Comp;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.framework.werkzeuge.collections.Collector;
import de.arnonym.strategiespiel.spielSchirm.arbeit.TimeClock;
import de.arnonym.strategiespiel.spielSchirm.arbeit.auftrag.Assignment;
import de.arnonym.strategiespiel.spielSchirm.info.muster.Info;

public abstract class ProgressDepartmentComp<ProductInfo extends Info, A extends Assignment> extends VarDepartmentComp<ProductInfo, A> {
    public final TimeClock timeClock = new TimeClock(-1);

    public ProgressDepartmentComp(Entity entity,

                                  Collector<Comp> comps,
                                  VarEmployerComp<? extends ProductInfo> employerComp) {
        super(new Collector<>(), entity, employerComp, comps);
    }

    public void resetWorkingHours() {
        if (!hasAssignment()) return;
        timeClock.settArbeitszeit(getAssignment_nl().arbeitszeit);
    }

    @Override
    protected void newProduct(ProductInfo productInfo) {
        resetWorkingHours();
    }
}
