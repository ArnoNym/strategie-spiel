package de.arnonym.strategiespiel.spielSchirm.arbeit.step.arbeitszeit;

import com.badlogic.gdx.math.Vector2;

import de.arnonym.strategiespiel.framework.ecs.component.Comp;
import de.arnonym.strategiespiel.framework.werkzeuge.collections.Collector;
import de.arnonym.strategiespiel.spielSchirm.arbeit.TimeClock;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.bewegung.BewegungComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.bewegung.BewegungszielComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.EinkommenComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.GeldComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.werte.HabitalwerteComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.werte.ProduktivitaetComp;
import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenFehlerAusgleich;
import de.arnonym.strategiespiel.spielSchirm.arbeit.step.StepArbeit;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.MathUtils;

public abstract class StepArbeitszeit<Assignment extends de.arnonym.strategiespiel.spielSchirm.arbeit.auftrag.Assignment> extends StepArbeit<Assignment> {
    public final TimeClock zTimeClock;

    public StepArbeitszeit(Collector<Comp> comps,

                           TimeClock zTimeClock,
                           BewegungszielComp zBewegungszielComp,
                           Assignment assignment,

                           GeldComp gGeldComp, int salary,

                           BewegungComp bewegungComp, GeldComp aGeldComp,
                           HabitalwerteComp habitalwerteComp, ProduktivitaetComp produktivitaetComp,

                           Vector2 start_nl) {
        super(comps, zBewegungszielComp, gGeldComp, salary, assignment,
                bewegungComp, aGeldComp, habitalwerteComp, produktivitaetComp, start_nl);
        this.zTimeClock = zTimeClock;
    }

    @Override
    protected void reservieren(float arbeitszeit) {
        super.reservieren(arbeitszeit);
        zTimeClock.addRestZuPlanenArbeitszeit( - arbeitszeit);
    }

    @Override
    protected void reservierenRueckgaengig_or() {
        super.reservierenRueckgaengig_or();
        zTimeClock.addRestZuPlanenArbeitszeit(gettArbeitszeit());
    }

    @Override
    public void erledigt_or() {
        super.erledigt_or();

        zTimeClock.addRestZuErledigenArbeitszeit( - gettArbeitszeit());
        float restArbeitszeit = zTimeClock.getRestZuErledigenArbeitszeit();

        if (restArbeitszeit < 0) {
            throw new IllegalStateException("restZuErledigenArbeitszeit: " + restArbeitszeit);
        } else if (MathUtils.runden(restArbeitszeit,
                KonstantenFehlerAusgleich.SEKUNDEN_RUNDEN_NACHKOMMASTELLEN) == 0) {
            auftragErledigt(); // Beinhaltet erschaffe von Gebauden etc
        }
    }

    protected abstract void auftragErledigt();
}
