package de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.departments.bildung;

import de.arnonym.strategiespiel.framework.werkzeuge.collections.Collector;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.EmployerComp;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.ArbeitnehmerAi;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.VarEmployerComp;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.departments.material.MaterialDepartmentComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.gegenstaende.GegenstaendeComp;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.framework.stp.Task;
import de.arnonym.strategiespiel.spielSchirm.arbeit.task.liefernArbeitszeit.anliefern.ForschenTask;
import de.arnonym.strategiespiel.spielSchirm.info.forschung.Forschungsstand;
import de.arnonym.strategiespiel.spielSchirm.info.forschung.ForschungsstandList;
import de.arnonym.strategiespiel.spielSchirm.info.muster.Info;

public class ForschungDepartmentComp extends MaterialDepartmentComp<Info, ForschungAssignment> {
    private final ForschungsstandList forschungsstandList;

    public ForschungDepartmentComp(ForschungsstandList forschungsstandList,

                                   Entity entity,

                                   VarEmployerComp<? extends Info> employerComp, GegenstaendeComp gegenstaendeComp) {
        super(entity, new Collector<>(), employerComp, gegenstaendeComp);
        this.forschungsstandList = forschungsstandList;
    }

    @Override
    public Task createTask_nl(ArbeitnehmerAi arbeitnehmerAi) {
        return new ForschenTask(arbeitnehmerAi.aufsteigendesSymbolFactory, arbeitnehmerAi.sysLager,

                this,

                arbeitnehmerAi.stpComp, arbeitnehmerAi.bewegungComp,
                arbeitnehmerAi.einkommenComp, arbeitnehmerAi.gegenstaendeComp,
                arbeitnehmerAi.habitalwerteComp, arbeitnehmerAi.produktivitaetComp);
    }

    @Override
    protected void newProduct(Info info) {
        ForschungAssignment newAssignment_nl = getAssignment(info);
        if (newAssignment_nl == null) {
            return;
        }

        Forschungsstand forschungsstand = forschungsstandList.gettDurchId(newAssignment_nl.id);
        if (forschungsstand.isErforscht()) {
            throw new IllegalArgumentException();
        }
        forschungsstand.addForscher(this);

        gegenstaendeComp.settMaxMengen(newAssignment_nl, true);
    }

    /*@Override
    public boolean setAssignment(ForschungAssignment newAssignment_nl) {
        ForschungAssignment oldAssignment = getAssignment_nl();
        Forschungsstand forschungsstand;

        if (oldAssignment != null) {
            forschungsstand = forschungsstandList.gettDurchId(oldAssignment.id);

            forschungsstand.setForscher(null);
        }

        if (newAssignment_nl == null) {
            return super.setAssignment(null);
        }

        forschungsstand = forschungsstandList.gettDurchId(newAssignment_nl.id);

        if (forschungsstand.isErforscht()) {
            throw new IllegalArgumentException();
        }

        ForschungDepartmentComp oldForschungDepartmentComp = forschungsstand.getForscher();
        if (oldForschungDepartmentComp != null) {
            oldForschungDepartmentComp.setAssignment(null);
        }

        forschungsstand.setForscher(this);

        gegenstaendeComp.settMaxMengen(forschungsstand.getForschungAssignment(), true);

        return super.setAssignment(newAssignment_nl);
    }*/

    public Forschungsstand forschungsstand() {
        return forschungsstandList.gettDurchId(getAssignment_nl().id);
    }

    @Override
    protected ForschungAssignment getAssignment(Info info) {
        return info.getForschungAssignment();
    }
}
