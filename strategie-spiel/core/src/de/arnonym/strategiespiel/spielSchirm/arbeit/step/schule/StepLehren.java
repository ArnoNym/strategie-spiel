package de.arnonym.strategiespiel.spielSchirm.arbeit.step.schule;

import com.badlogic.gdx.math.Vector2;

import de.arnonym.strategiespiel.framework.ecs.component.Comp;
import de.arnonym.strategiespiel.framework.werkzeuge.collections.Collector;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.departments.bildung.TeachDepartmentComp;
import de.arnonym.strategiespiel.spielSchirm.arbeit.auftrag.Assignment;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.bewegung.BewegungComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.EinkommenComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.werte.HabitalwerteComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.werte.ProduktivitaetComp;
import de.arnonym.strategiespiel.spielSchirm.arbeit.step.StepArbeit;
import de.arnonym.strategiespiel.spielSchirm.Enums;

public class StepLehren extends StepArbeit<Assignment> {
    public final TeachDepartmentComp teachDepartmentComp;

    public StepLehren(TeachDepartmentComp teachDepartmentComp,

                      BewegungComp bewegungComp, EinkommenComp einkommenComp,
                      HabitalwerteComp habitalwerteComp, ProduktivitaetComp produktivitaetComp,

                      Vector2 start_nl) {
        super(new Collector<Comp>(teachDepartmentComp), teachDepartmentComp.employerComp.bewegungszielComp,
                teachDepartmentComp.employerComp.eigentuemerComp.geldComp, einkommenComp.getEinkommen(),
                teachDepartmentComp.bildenInfo.bildenAuftrag, bewegungComp, einkommenComp.geldComp,
                habitalwerteComp, produktivitaetComp, start_nl);
        this.teachDepartmentComp = teachDepartmentComp;
    }

    @Override
    protected Enums.EnumStpStatus versuchen_or_or_or() {
        if (teachDepartmentComp.issMaxVorbereitung()) {
            return Enums.EnumStpStatus.FAIL; // todo erledigt_or wiedergeben??????
        }
        return Enums.EnumStpStatus.OK;
    }

    @Override
    public void erledigt_or() {
        teachDepartmentComp.addVorbereitung();
        super.erledigt_or();
    }
}
