package de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.departments.material;

import com.badlogic.gdx.math.Vector2;

import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.departments.bildung.ForschungAssignment;
import de.arnonym.strategiespiel.spielSchirm.gebaude.GebaudeInfo;
import de.arnonym.strategiespiel.framework.werkzeuge.textures.TextureHashMap;

public abstract class MaterialGebaudeInfo extends GebaudeInfo {
    public final int maxStellen;
    public final int maxPlatz;
    public final int[] arbeitenAnIds;

    public MaterialGebaudeInfo(int id, String name, String beschreibung,
                               TextureHashMap textureHashMap, Vector2 fundament,
                               int[][] bauMaterial, float bauZeit,
                               ForschungAssignment forschungAuftrag_nl, int maxStellen,
                               int[] arbeitenAnIds, int maxPlatz) {
        super(id, name, beschreibung, textureHashMap, fundament, bauMaterial, bauZeit,
                forschungAuftrag_nl);
        this.maxStellen = maxStellen;
        this.arbeitenAnIds = arbeitenAnIds;
        this.maxPlatz = maxPlatz;
    }
}
