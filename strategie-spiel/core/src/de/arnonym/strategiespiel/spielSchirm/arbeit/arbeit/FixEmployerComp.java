package de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit;

import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.bewegung.BewegungszielComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.herrschaft.EigentuemerComp;

public class FixEmployerComp extends EmployerComp {
    public FixEmployerComp(EmploymentMatcher employmentMatcher, Entity entity, BewegungszielComp bewegungszielComp, EigentuemerComp eigentuemerComp, int maxStellen) {
        super(employmentMatcher, entity, bewegungszielComp, eigentuemerComp, maxStellen);
        super.placeholderAssignment = new PlaceholderAssignment(null);
    }
}
