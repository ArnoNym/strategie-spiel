package de.arnonym.strategiespiel.spielSchirm.arbeit.step.schule;

import com.badlogic.gdx.math.Vector2;

import de.arnonym.strategiespiel.framework.listener.StoryTeller;
import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenBalance;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.departments.bildung.TeachDepartmentComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.bewegung.BewegungComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.werte.BildungComp;
import de.arnonym.strategiespiel.spielSchirm.StepNGZ;
import de.arnonym.strategiespiel.framework.werkzeuge.pause.DeltaPause;
import de.arnonym.strategiespiel.spielSchirm.Enums;

public class StepSchueler extends StepNGZ {
    public final TeachDepartmentComp schuleArbeitgeberComp;
    public final BildungComp bildungComp;
    private final DeltaPause pause;

    public StepSchueler(TeachDepartmentComp zSchuleArbeitgeberComp,

                        BewegungComp aBewegungComp, BildungComp aBildungComp,

                        Vector2 start_nl) {
        super(zSchuleArbeitgeberComp.employerComp.bewegungszielComp,
                new StoryTeller[]{zSchuleArbeitgeberComp}, aBewegungComp, start_nl);
        this.bildungComp = aBildungComp;
        this.schuleArbeitgeberComp = zSchuleArbeitgeberComp;

        this.pause = new DeltaPause(KonstantenBalance.DORFBEWOHNER_LERNEN_AM_STUECK,
                Enums.EnumPauseStart.VORBEI);
    }

    @Override
    protected Enums.EnumStpStatus versuchen_or_or() {
        if (schuleArbeitgeberComp.anzahlLehrerInGebaude() == 0) {
            /* Eigentlich nicht wirklich noetig, da schueler immer lernen und besodners viel wenn
            sie 'Vorbereitung' (Siehe CompArbeitgeberSchule) verbauchen. Alleine schon damit
            Schueler nicht umsonst zur Schuele laufen. Diese Bedingung bleibt trotzdem bestehen, da
            es so viesuell schoener ist. */
            return Enums.EnumStpStatus.FAIL;
        }
        return Enums.EnumStpStatus.OK;
    }

    @Override
    protected void anfangen_or() {
        pause.settStartzeitJetzt();
    }

    @Override
    protected void durchfuehren_or(float delta) {
        pause.issVorbei(delta, false);
    }

    @Override
    public boolean pruefenObFail() {
        return false;
    }

    @Override
    public boolean pruefenObErledigt() {
        if (pause != null && pause.issVorbei()) {
            bildungComp.addBildung(pause.gettZeitSeitStartzeit()
                    / schuleArbeitgeberComp.bildenInfo.bildenAuftrag.arbeitszeit);
            return true;
        }
        return false;
    }

    @Override
    protected void failNachVersuchenVorAnfangen_or() {

    }

    @Override
    protected void failNachAnfangenVorDurchfuehren_or() {

    }

    @Override
    protected void failNachDurchfuehren_or() {

    }

    @Override
    public void erledigt_or() {
    }

    @Override
    protected void reservierenRueckgaengig_or() {

    }
}
