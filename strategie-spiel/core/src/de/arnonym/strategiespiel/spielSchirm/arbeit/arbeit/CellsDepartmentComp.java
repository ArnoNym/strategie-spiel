package de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit;

import java.beans.PropertyChangeEvent;

import de.arnonym.strategiespiel.framework.ecs.collections.System;
import de.arnonym.strategiespiel.framework.ecs.component.Comp;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.framework.werkzeuge.collections.Collector;
import de.arnonym.strategiespiel.spielSchirm.Enums;
import de.arnonym.strategiespiel.spielSchirm.arbeit.auftrag.Assignment;
import de.arnonym.strategiespiel.spielSchirm.info.muster.Info;

public abstract class CellsDepartmentComp<ProductInfo extends Info, A extends Assignment> extends VarDepartmentComp<ProductInfo, A> {
    public final CellsComp cellsComp;

    public CellsDepartmentComp(Collector<System> systems,

                               Entity entity,

                               Collector<Comp> comps,
                               CellsComp cellsComp, VarEmployerComp<? extends ProductInfo> employerComp) {
        super(systems, entity, employerComp, comps.add(cellsComp));
        this.cellsComp = cellsComp;

        cellsComp.addListener(this);
    }

    @Override
    public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
        if (propertyChangeEvent.getPropertyName().equals(Enums.PROBERTY_CHANGE_ZELLE)) {
            support.firePropertyChange(propertyChangeEvent); // Fuer Steps
        }
        super.propertyChange(propertyChangeEvent);
    }
}
