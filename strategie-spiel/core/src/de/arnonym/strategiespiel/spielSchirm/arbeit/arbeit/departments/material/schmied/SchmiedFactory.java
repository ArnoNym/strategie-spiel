package de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.departments.material.schmied;

import com.badlogic.gdx.math.Vector2;

import de.arnonym.strategiespiel.spielSchirm.Dorf;
import de.arnonym.strategiespiel.spielSchirm.World;
import de.arnonym.strategiespiel.spielSchirm.gebaude.Gebaude;
import de.arnonym.strategiespiel.spielSchirm.gebaude.GebaudeFactory;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.departments.material.MaterialGebaudeInfo;

public class SchmiedFactory extends GebaudeFactory<MaterialGebaudeInfo> {
    public SchmiedFactory(World world, Dorf dorf, MaterialGebaudeInfo info) {
        super(world, dorf, info);
    }

    @Override
    public Gebaude create(Vector2 position) {
        return new Schmied(world, dorf, info, position);
    }
}
