package de.arnonym.strategiespiel.spielSchirm.arbeit.auftrag;

import de.arnonym.strategiespiel.spielSchirm.zahlen.schaden.MusterSchaden;

public class AssignmentHerstellen extends AssignmentMaterial {
    public final int entstehendeMenge;

    public AssignmentHerstellen(int id, float optimaleProduktivitaetKopf,
                                float optimaleProduktivitaetKoerper, float arbeitszeit,
                                MusterSchaden musterSchaden, int[][] idMengenArray,
                                int entstehendeMenge) {
        super(id, optimaleProduktivitaetKopf, optimaleProduktivitaetKoerper, arbeitszeit,
                musterSchaden, idMengenArray);
        this.entstehendeMenge = entstehendeMenge;
    }
}
