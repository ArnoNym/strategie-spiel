package de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.departments.bildung;

import de.arnonym.strategiespiel.framework.werkzeuge.textures.IdTextureRegion;
import de.arnonym.strategiespiel.framework.werkzeuge.textures.Texture;
import de.arnonym.strategiespiel.spielSchirm.Assets;
import de.arnonym.strategiespiel.spielSchirm.balancing.IdsBildung;
import de.arnonym.strategiespiel.spielSchirm.info.muster.Info;

public class BildungInfo extends Info {
    public final IdsBildung.EnumBildung enumBildung;
    public final BildungAssignment bildenAuftrag;

    public BildungInfo(int id, String name, String beschreibung,
                       IdsBildung.EnumBildung enumBildung,
                       BildungAssignment bildenAuftrag) {
        super(id, name, beschreibung);
        this.enumBildung = enumBildung;
        this.bildenAuftrag = bildenAuftrag;
    }

    public BildungInfo(int id, String name, String beschreibung,
                       ForschungAssignment forschungAssignment,
                       IdsBildung.EnumBildung enumBildung,
                       BildungAssignment bildenAuftrag) {
        super(id, name, beschreibung, forschungAssignment);
        this.enumBildung = enumBildung;
        this.bildenAuftrag = bildenAuftrag;
    }

    @Override
    public Texture getIconTexture() {
        return new IdTextureRegion(Assets.instance.iconAssets.iconFreizeit); // TODO
    }
}
