package de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.departments.bildung;

import de.arnonym.strategiespiel.spielSchirm.arbeit.auftrag.Assignment;
import de.arnonym.strategiespiel.spielSchirm.zahlen.schaden.MusterSchaden;

public class BildungAssignment extends Assignment {
    public final float minBildung;
    public final float maxBildung;

    public BildungAssignment(float optimaleProduktivitaetKopf, float optimaleProduktivitaetKoerper, float arbeitszeit, float minBildung, float maxBildung) {
        super(optimaleProduktivitaetKopf, optimaleProduktivitaetKoerper, arbeitszeit);
        this.minBildung = minBildung;
        this.maxBildung = maxBildung;
    }
}
