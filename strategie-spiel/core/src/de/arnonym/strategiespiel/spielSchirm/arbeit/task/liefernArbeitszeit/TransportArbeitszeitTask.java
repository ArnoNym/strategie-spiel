package de.arnonym.strategiespiel.spielSchirm.arbeit.task.liefernArbeitszeit;

import de.arnonym.strategiespiel.framework.stp.StpComp;
import de.arnonym.strategiespiel.framework.listener.StoryTeller;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.gegenstaende.CompWirtschaftsdaten;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.EinkommenComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.GeldComp;
import de.arnonym.strategiespiel.framework.stp.Step;
import de.arnonym.strategiespiel.spielSchirm.arbeit.step.ausbeuten.StepAbbauen;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.bewegen.StepGeheZu;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dorfbewohner.tauschen.step.StepLegeIn;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dorfbewohner.tauschen.step.StepNehmen;
import de.arnonym.strategiespiel.framework.stp.Task;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.undPreis.IntEintragHashMap;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.CollectionUtils;

public abstract class TransportArbeitszeitTask extends Task {
    /** Bezahlt jede Laufzeit ab dem ersten Step der kein StepGeheZu ist. Ausgezahlt wird entweder
     * wenn ein StepLegenIn ausgefuehrt wurde oder der Task beendet wird! */

    public final GeldComp gGeldComp; // Wer bezahlt den Transport
    public final int salary;
    public final GeldComp aGeldComp;

    private boolean trackStarted = false;
    private float trackLaufzeit = 0;
    private IntEintragHashMap genommenHashmap = new IntEintragHashMap();

    public TransportArbeitszeitTask(GeldComp gGeldComp, int salary,

                                    GeldComp aGeldComp, StpComp aStpComp,

                                    StoryTeller... storyTellers) {
        super(aStpComp, CollectionUtils.combine(storyTellers, aGeldComp, aGeldComp));
        this.gGeldComp = gGeldComp;
        this.salary = salary;
        this.aGeldComp = aGeldComp;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    protected void stepAngefangen(Step step) {
        if (!trackStarted && !(step instanceof StepGeheZu)) {
            trackStarted = true;
        }
    }

    @Override
    protected void stepBeendet(Step step) {
        if (!trackStarted) return;

        if (step instanceof StepGeheZu) {
            trackLaufzeit += ((StepGeheZu) step).gettGesamtLaufZeit();
        } else if (step instanceof StepNehmen) {
            genommenHashmap.addUpEntrys(((StepNehmen) step).getGenommenHashMap());
        } else if (step instanceof StepAbbauen) {
            genommenHashmap.addUpEntrys(((StepAbbauen) step).getGenommenHashMap());
        } else if (step instanceof StepLegeIn) {
            StepLegeIn stepLegeIn = ((StepLegeIn) step);

            int lieferKosten = bezahleTransporteur(stepLegeIn);

            CompWirtschaftsdaten compWirtschaftsdaten = stepLegeIn.getCompWirtschaftsdaten();
            if (compWirtschaftsdaten != null) {
                compWirtschaftsdaten.gegenstandManager.addLieferKosten(lieferKosten, genommenHashmap);
            }

            trackLaufzeit = 0;
            genommenHashmap = new IntEintragHashMap();
        }
    }

    @Override
    protected void beenden() {
        if (!trackStarted) return;

        Step step = getCurrentStep();

        if (step instanceof StepLegeIn) {
            StepLegeIn stepLegeIn = (StepLegeIn) step;
            int lieferKosten = bezahleTransporteur(stepLegeIn);
            CompWirtschaftsdaten compWirtschaftsdaten = stepLegeIn.getCompWirtschaftsdaten();
            if (compWirtschaftsdaten != null) {
                compWirtschaftsdaten.gegenstandManager.addLieferKosten(lieferKosten, genommenHashmap);
            }
        } else {
            bezahleTransporteur(null);
        }

        trackLaufzeit = 0;
        genommenHashmap = new IntEintragHashMap();

        super.beenden();
    }

    private int bezahleTransporteur(StepLegeIn stepLegeIn_nl) {
        GeldComp nehmenVonGeldComp;
        if (stepLegeIn_nl != null) {
            nehmenVonGeldComp = stepLegeIn_nl.bezahleLieferantenCompGeld;
        } else {
            nehmenVonGeldComp = this.gGeldComp;
            if (nehmenVonGeldComp == null) {
                return 0;
            }
        }
        int lieferKosten = (int) (trackLaufzeit * salary);
        int lieferBezahlung = Math.min(lieferKosten, nehmenVonGeldComp.getGeld().getMengeVerfuegbar()); // todo schulden
        nehmenVonGeldComp.mengeVerringern(lieferBezahlung, false);
        aGeldComp.mengeErhoehen(lieferBezahlung, false);
        return lieferKosten;
    }
}
