package de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.departments.gebiet.farm;

import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;
import java.util.List;

import de.arnonym.strategiespiel.Strategiespiel;
import de.arnonym.strategiespiel.spielSchirm.Dorf;
import de.arnonym.strategiespiel.spielSchirm.World;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.CellsComp;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.VarEmployerComp;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.departments.gebiet.AbbauenDepartmentComp;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.departments.gebiet.AnpflanzenDepartmentComp;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.departments.gebiet.ErntenDepartmentComp;
import de.arnonym.strategiespiel.spielSchirm.balancing.IdsRessourcen;
import de.arnonym.strategiespiel.framework.ecs.component.Comp;
import de.arnonym.strategiespiel.spielSchirm.gebaude.Gebaude;
import de.arnonym.strategiespiel.spielSchirm.umwelt.ressourcen.stufen.WachstumRessourceInfo;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterOnKlick.OnKlickFenster;

public class Farm extends Gebaude<FarmInfo> {
    public Farm(World world, Dorf dorf, FarmInfo info, Vector2 position) {
        super(world, dorf, info, position);
    }

    @Override
    protected OnKlickFenster definiereGebaude(List<Comp> activateCompList,
                                              List<Comp> loescheCompList) {
        List<WachstumRessourceInfo> stufeResOutInfoList = new ArrayList<>();
        for (int id : info.anpflanzenIds) {
            try {
                stufeResOutInfoList.add((WachstumRessourceInfo) IdsRessourcen.get(id));
            } catch (ClassCastException e) {
                throw new IllegalArgumentException("Ressource mit id: "+id+" kann nicht auf"
                        + " einer Farm angebaut werden!");
            }
        }
        VarEmployerComp<WachstumRessourceInfo> employerComp = new VarEmployerComp<>(
                dorf.employmentMatcher, this, bewegungszielComp, eigentuemerComp,
                stufeResOutInfoList.toArray(new WachstumRessourceInfo[stufeResOutInfoList.size()]),
                null,
                info.abbauenMaxStellen + info.anpflanzenMaxStellen + info.erntenMaxStellen + info.pflegenMaxStellen);
        CellsComp cellsComp = new CellsComp(this);
        new AnpflanzenDepartmentComp(this, cellsComp, employerComp);
        new ErntenDepartmentComp(this, cellsComp, employerComp);
        new AbbauenDepartmentComp(this, cellsComp, employerComp);
        activateCompList.add(employerComp);

        return new FarmOnKlickFenster(world, cellsComp, employerComp, nameComp, compRender,
                Strategiespiel.skin);
    }
}
