package de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.departments.material.schmied;

import com.badlogic.gdx.math.Vector2;

import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.departments.bildung.ForschungAssignment;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.departments.material.MaterialGebaudeInfo;
import de.arnonym.strategiespiel.framework.werkzeuge.textures.TextureHashMap;

public class SchmiedInfo extends MaterialGebaudeInfo {
    public SchmiedInfo(int id, String name, String beschreibung, TextureHashMap textureHashMap, Vector2 fundament, int[][] bauMaterial, float bauZeit, ForschungAssignment forschungAssignment_nl, int maxStellen, int[] arbeitenAnIds, int maxPlatz) {
        super(id, name, beschreibung, textureHashMap, fundament, bauMaterial, bauZeit, forschungAssignment_nl, maxStellen, arbeitenAnIds, maxPlatz);
    }
}
