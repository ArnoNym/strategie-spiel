package de.arnonym.strategiespiel.spielSchirm.arbeit.step.ausbeuten;

import com.badlogic.gdx.math.Vector2;

import de.arnonym.strategiespiel.framework.werkzeuge.collections.Collector;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.ausbeuten.FruechteComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.bewegung.BewegungComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.gegenstaende.GegenstaendeComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.EinkommenComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.GeldComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.herrschaft.EigentuemerComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.werte.HabitalwerteComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.werte.ProduktivitaetComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.lists.ausbeuten.ErntenEcsList;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.endstufe.Gegenstand;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.endstufe.GegenstandList;

public class StepErnten extends StepAusbeuten {
    public final FruechteComp compFruechte;

    public StepErnten(EigentuemerComp eigentuemerComp,

                      FruechteComp zFruechteComp,

                      GeldComp gGeldComp, int salary,

                      BewegungComp bewegungComp, GeldComp aGeldComp,
                      HabitalwerteComp habitalwerteComp, ProduktivitaetComp produktivitaetComp,
                      GegenstaendeComp gegenstaendeComp,

                      Vector2 start_nl) {
        super(new Collector<>(), eigentuemerComp, zFruechteComp, gGeldComp, salary, bewegungComp,
                aGeldComp, habitalwerteComp, produktivitaetComp, gegenstaendeComp, start_nl);
        this.compFruechte = zFruechteComp;
    }

    @Override
    protected Gegenstand gettAusbeutenGegenstand(GegenstandList aGegList, GegenstandList zGegList) {
        for (Gegenstand gegenstand : zGegList) {
            if (gegenstand.gegMuster.isNachwachsbar() && pruefeGegenstand(gegenstand)) {
                return gegenstand;
            }
        }
        return null;
    }
}
