package de.arnonym.strategiespiel.spielSchirm.arbeit.task.arbeitszeit;

import de.arnonym.strategiespiel.framework.stp.StpComp;
import de.arnonym.strategiespiel.framework.listener.StoryTeller;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.EinkommenComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.GeldComp;
import de.arnonym.strategiespiel.framework.stp.Step;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.bewegen.StepGeheZu;
import de.arnonym.strategiespiel.framework.stp.Task;
import de.arnonym.strategiespiel.spielSchirm.Enums;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.CollectionUtils;

public abstract class ArbeitszeitTask extends Task {
    public final GeldComp gGeldComp;
    public final int salary;
    public final GeldComp aGeldComp;

    private boolean started = false;
    private float unbezahlteZeit = 0;
    private int reserviertesGeld = 0;
    // TODO mit liefern abreizzeit velnüpfen??
    public ArbeitszeitTask(GeldComp gGeldComp, int salary,
                           GeldComp aGeldComp, StpComp aStpComp, StoryTeller... storyTellers) {
        super(aStpComp, CollectionUtils.combine(storyTellers,
                gGeldComp, aGeldComp));
        this.gGeldComp = gGeldComp;
        this.salary = salary;
        this.aGeldComp = aGeldComp;
    }

    @Override
    protected Enums.EnumStpStatus versuchen_or() {
        if (gGeldComp.getGeld().getMengeVerfuegbar() == 0) {
            return Enums.EnumStpStatus.FAIL;
        }
        return super.versuchen_or();
    }

    @Override
    protected void durchfuehren_or(float delta) {
        if (started) {
            unbezahlteZeit += delta;
        }
        super.durchfuehren_or(delta);
    }

    @Override
    protected void stepAngefangen(Step step) {
        if (!started && !(step instanceof StepGeheZu)) {
            started = true;
        }
    }

    @Override
    protected void stepBeendet(Step step) {
        if (started) {
            float zeit = this.unbezahlteZeit;
            int geld = (int) (zeit * salary);
            int nehmenGeld = gGeldComp.getGeld().getMengeVerfuegbar();
            if (geld > nehmenGeld) {
                geld = Math.min(geld, nehmenGeld);
                setStatus(Enums.EnumStpStatus.FAIL);
            }
            gGeldComp.getGeld().reservierenNehmen(geld); // todo getGeld entfernen
            aGeldComp.getGeld().reservierenLegen(geld);
            reserviertesGeld += geld;
        }
    }

    @Override
    public void beenden() {
        gGeldComp.mengeVerringern(reserviertesGeld, true);
        aGeldComp.mengeErhoehen(reserviertesGeld, true);
        super.beenden();
    }
}
