package de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.departments.gebiet.farm;

import com.badlogic.gdx.math.Vector2;

import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.departments.bildung.ForschungAssignment;
import de.arnonym.strategiespiel.spielSchirm.gebaude.GebaudeInfo;
import de.arnonym.strategiespiel.framework.werkzeuge.textures.TextureHashMap;

public class FarmInfo extends GebaudeInfo {
    public final int abbauenMaxStellen;
    public final int anpflanzenMaxStellen;
    public final int erntenMaxStellen;
    public final int pflegenMaxStellen;
    public final int gesamtMaxStellen; // TODO Hat aktuell keine Auswirkungen
    public final int[] anpflanzenIds;

    public FarmInfo(int id, String name, String beschreibung,
                    TextureHashMap textureHashMap, Vector2 fundament,
                    int[][] bauMaterial, float arbeitszeit,
                    ForschungAssignment forschungAuftrag_nl, int abbauenMaxStellen,
                    int anpflanzenMaxStellen, int erntenMaxStellen, int pflegenMaxStellen,
                    int gesamtMaxStellen, int[] anpflanzenIds) {
        super(id, name, beschreibung, textureHashMap, fundament, bauMaterial, arbeitszeit,
                forschungAuftrag_nl);
        this.abbauenMaxStellen = abbauenMaxStellen;
        this.anpflanzenMaxStellen = anpflanzenMaxStellen;
        this.erntenMaxStellen = erntenMaxStellen;
        this.pflegenMaxStellen = pflegenMaxStellen;
        this.gesamtMaxStellen = gesamtMaxStellen;
        this.anpflanzenIds = anpflanzenIds;
    }
}
