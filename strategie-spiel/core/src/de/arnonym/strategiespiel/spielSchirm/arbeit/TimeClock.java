package de.arnonym.strategiespiel.spielSchirm.arbeit;

public class TimeClock {
    private float arbeitszeit;
    private float restZuPlanenArbeitszeit;
    private float restZuErledigenArbeitszeit;

    public TimeClock(float arbeitszeit) {
        settArbeitszeit(arbeitszeit);
    }

    public void addRestZuPlanenArbeitszeit(float zeit) {
        restZuPlanenArbeitszeit += zeit;
    }

    public void addRestZuErledigenArbeitszeit(float zeit) {
        restZuErledigenArbeitszeit += zeit;
    }

    public void settArbeitszeit(float arbeitszeit) {
        this.arbeitszeit = restZuPlanenArbeitszeit = restZuErledigenArbeitszeit = arbeitszeit;
    }

    // EINFACHE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public float getArbeitszeit() {
        return arbeitszeit;
    }

    public float getRestZuPlanenArbeitszeit() {
        return restZuPlanenArbeitszeit;
    }

    public float getRestZuErledigenArbeitszeit() {
        return restZuErledigenArbeitszeit;
    }
}
