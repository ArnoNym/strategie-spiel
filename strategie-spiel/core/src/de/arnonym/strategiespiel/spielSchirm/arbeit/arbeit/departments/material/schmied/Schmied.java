package de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.departments.material.schmied;

import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;
import java.util.List;

import de.arnonym.strategiespiel.Strategiespiel;
import de.arnonym.strategiespiel.spielSchirm.Dorf;
import de.arnonym.strategiespiel.spielSchirm.World;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.VarEmployerComp;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.departments.material.HerstellerDepartmentComp;
import de.arnonym.strategiespiel.spielSchirm.balancing.IdsGegenstaende;
import de.arnonym.strategiespiel.framework.ecs.component.Comp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.gegenstaende.GegenstaendeComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.gegenstaende.GegenstaendeComp1;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.gegenstaende.CompWirtschaftsdaten;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.name.NameComp;
import de.arnonym.strategiespiel.spielSchirm.gebaude.Gebaude;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.departments.material.MaterialGebaudeInfo;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterOnKlick.OnKlickFenster;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterOnKlick.HerstellerOnKlickFenster;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.vorstufe.EntityGegInfoList;
import de.arnonym.strategiespiel.spielSchirm.info.muster.GegInfo;

public class Schmied extends Gebaude<MaterialGebaudeInfo> {
    public Schmied(World world, Dorf dorf, MaterialGebaudeInfo info, Vector2 position) {
        super(world, dorf, info, position);
    }

    @Override
    protected OnKlickFenster definiereGebaude(List<Comp> activateCompList,
                                              List<Comp> loescheCompList) {
        GegenstaendeComp gegenstaendeComp = new GegenstaendeComp1(this, compRender,
                new EntityGegInfoList(info.maxPlatz, false).addAll());
        activateCompList.add(gegenstaendeComp);
        gegenstaendeComp.gegenstandListe.gettDurchId(IdsGegenstaende.MAT_PROD_WERKZEUG_EINFACH_ID).mengeErhoehen(200, false); // todo loeschen
        gegenstaendeComp.gegenstandListe.gettDurchId(IdsGegenstaende.MAT_HOLZ_ID).mengeErhoehen(600, false); // todo loeschen

        CompWirtschaftsdaten compWirtschaftsdaten = new CompWirtschaftsdaten(this,
                world.sysWirtschaftsdatenManager, gegenstaendeComp);
        activateCompList.add(compWirtschaftsdaten);

        List<GegInfo> herstellbarGegInfoList = new ArrayList<>();
        for (int id : info.arbeitenAnIds) {
            herstellbarGegInfoList.add(IdsGegenstaende.get(id));
        }
        VarEmployerComp<GegInfo> employerComp = new VarEmployerComp<>(
                dorf.employmentMatcher, this, bewegungszielComp, eigentuemerComp,
                herstellbarGegInfoList.toArray(new GegInfo[herstellbarGegInfoList.size()]), null,
                info.maxStellen);
        HerstellerDepartmentComp herstellerDepartmentComp = new HerstellerDepartmentComp(this,
                employerComp, gegenstaendeComp);
        activateCompList.add(employerComp);

        NameComp nameComp = new NameComp(this, info.name);
        activateCompList.add(nameComp);

        return new HerstellerOnKlickFenster(world, employerComp, herstellerDepartmentComp,
                variabelGeldComp, nameComp, compRender, compWirtschaftsdaten,
                dorf.forschungsstandList.gettDurchId((int) id), Strategiespiel.skin);
    }
}
