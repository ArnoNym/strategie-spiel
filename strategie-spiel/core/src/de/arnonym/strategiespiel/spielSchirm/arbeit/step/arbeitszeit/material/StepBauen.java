package de.arnonym.strategiespiel.spielSchirm.arbeit.step.arbeitszeit.material;

import com.badlogic.gdx.math.Vector2;

import de.arnonym.strategiespiel.framework.ecs.component.Comp;
import de.arnonym.strategiespiel.framework.werkzeuge.collections.Collector;
import de.arnonym.strategiespiel.spielSchirm.arbeit.TimeClock;
import de.arnonym.strategiespiel.spielSchirm.arbeit.auftrag.AssignmentMaterial;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.bewegung.BewegungComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.bewegung.BewegungszielComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.gegenstaende.GegenstaendeComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.EinkommenComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.GeldComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.herrschaft.EigentumComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.werte.HabitalwerteComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.werte.ProduktivitaetComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.transformieren.TransComp;

public class StepBauen extends StepMaterial<AssignmentMaterial> {
    public final TransComp zTransComp;

    public StepBauen(TimeClock zTimeClock, BewegungszielComp zBewegungszielComp,
                     EigentumComp zEigentumComp, GegenstaendeComp zGegenstaendeComp,
                     TransComp zTransComp,

                     AssignmentMaterial assignmentMaterial,

                     GeldComp gGeldComp, int salary,

                     BewegungComp bewegungComp, GeldComp aGeldComp,
                     HabitalwerteComp habitalwerteComp, ProduktivitaetComp produktivitaetComp,

                     Vector2 start_nl) {
        super(new Collector<>(zTransComp), zTimeClock, zBewegungszielComp, zEigentumComp,
                zGegenstaendeComp, assignmentMaterial, gGeldComp, salary, bewegungComp, aGeldComp,
                habitalwerteComp, produktivitaetComp, start_nl);
        this.zTransComp = zTransComp;
    }

    @Override
    protected void auftragErledigt_or() {
        zTransComp.trans();
    }
}
