package de.arnonym.strategiespiel.spielSchirm.arbeit.task;

/*
public class TaskFarmen extends Task {
    public final IdsRessourcenFactory idsRessourcenFactory;
    public final AufsteigendesSymbolFactory aufsteigendesSymbolFactory;
    public final BodenStapelFactory bodenStapelFactory;
    public final SysLager sysLager;
    public final BewegungComp aBewegungComp;
    public final EinkommenComp aGeldComp;
    public final HabitalwerteComp aHabitalwerteComp;
    public final ProduktivitaetComp aProduktivitaetComp;
    public final GegenstaendeComp aGegenstaendeComp;

    private final FarmComp gebietComp;
    private final GeldComp arbeitgeberGeldComp;

    public TaskFarmen(IdsRessourcenFactory idsRessourcenFactory,
                      AufsteigendesSymbolFactory aufsteigendesSymbolFactory,
                      BodenStapelFactory bodenStapelFactory, SysLager sysLager,

                      FarmComp gebietComp,

                      StpComp stpComp, BewegungComp aBewegungComp,
                      EinkommenComp aGeldComp, HabitalwerteComp aHabitalwerteComp,
                      ProduktivitaetComp aProduktivitaetComp, GegenstaendeComp aGegenstaendeComp) {
        super(stpComp, aBewegungComp, aGeldComp, aHabitalwerteComp, aProduktivitaetComp,
                aGegenstaendeComp, gebietComp);
        this.idsRessourcenFactory = idsRessourcenFactory;
        this.aufsteigendesSymbolFactory = aufsteigendesSymbolFactory;
        this.bodenStapelFactory = bodenStapelFactory;
        this.sysLager = sysLager;
        this.gebietComp = gebietComp;
        this.aBewegungComp = aBewegungComp;
        this.aGeldComp = aGeldComp;
        this.aHabitalwerteComp = aHabitalwerteComp;
        this.aProduktivitaetComp = aProduktivitaetComp;
        this.aGegenstaendeComp = aGegenstaendeComp;

        this.arbeitgeberGeldComp = gebietComp.erntenDepartmentComp.geldComp; // Ist immer fuer alle CompArbeitgeber gleich
    }

    @Override
    public void planeAlleSteps(StepStack stepStack) {
        //todo pflanzen auf fruchtbarstem Boden beguenstigen? Die Pflege sollte auf alle den gleichne Einfluss haben. Beim Anpflanzen die fruchtbareren beguenstigen;

        // Ernten wenn Ressource ausgewachsen und voll
        TaskErnten taskErnten = new TaskErnten(aufsteigendesSymbolFactory, bodenStapelFactory,
                sysLager, gebietComp.erntenDepartmentComp.liste, stpComp, aBewegungComp,
                aGeldComp, aGegenstaendeComp, aHabitalwerteComp, aProduktivitaetComp);
        stepStack.handle(taskErnten);
        if (!stepStack.isEmpty()) {
            if (Konstanten.DEVELOPER_VISUELL_STEP_SYSTEM_OUT) {
                IdentityConnectable.out.println("TaskFarmen ::: Ernten");
            }
            return;
        }

        // Neue Ressource anpflanzen
        List<Zelle> gebietList = gebietComp.anpflanzenDepartmentComp.anpflanzenAufZellenList;
        Zelle zelle = gebietList.toList((int) (gebietList.size()
                * ThreadLocalRandom.current().nextFloat()));
        StepNGZ step = new StepAnpflanzen(idsRessourcenFactory, gebietComp.anpflanzenDepartmentComp,
                zelle, gebietComp.getResInfo().id, aBewegungComp, aGeldComp,
                aHabitalwerteComp, aProduktivitaetComp, null);
        if (step.versuchen() == Enums.EnumStpStatus.OK) {
            stepStack.handle(step, true);
            if (Konstanten.DEVELOPER_VISUELL_STEP_SYSTEM_OUT) {
                IdentityConnectable.out.println("TaskFarmen ::: Anpflanzen");
            }
            return;
        }

        // Sachen von Land entfernen die nicht angebaut werden
        TaskAbbauen taskAbbauen = new TaskAbbauen(aufsteigendesSymbolFactory, bodenStapelFactory,
                sysLager, gebietComp.compArbeitgeberAbbauen.liste, stpComp, aBewegungComp,
                aGeldComp, aGegenstaendeComp, aHabitalwerteComp, aProduktivitaetComp);
        stepStack.handle(taskAbbauen);
        if (!stepStack.isEmpty()) {
            if (Konstanten.DEVELOPER_VISUELL_STEP_SYSTEM_OUT) {
                IdentityConnectable.out.println("TaskFarmen ::: Abbauen");
            }
            return;
        }

        // Pflanzen gepflegt
        PflegenTask taskPflegen = new PflegenTask(gebietComp.pflegenArbeitComp.liste,
                stpComp, aBewegungComp, aGeldComp, aHabitalwerteComp, aProduktivitaetComp,
                arbeitgeberGeldComp);
        stepStack.handle(taskPflegen);
        if (!stepStack.isEmpty()) {
            if (Konstanten.DEVELOPER_VISUELL_STEP_SYSTEM_OUT) {
                IdentityConnectable.out.println("TaskFarmen ::: Pflegen");
            }
            return;
        }
    }

    @Override
    public void planeNaechstenStep(StepStack stepStack) {

    }
}*/
