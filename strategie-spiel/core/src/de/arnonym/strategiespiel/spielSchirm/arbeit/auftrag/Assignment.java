package de.arnonym.strategiespiel.spielSchirm.arbeit.auftrag;

import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.werte.ProduktivitaetComp;
import de.arnonym.strategiespiel.spielSchirm.zahlen.schaden.MusterSchaden;

public class Assignment {
    private static final MusterSchaden ZERO_MUSTER_SCHADEN = new MusterSchaden(0, false, 0, 0, 0, 0, 1);
    public final float optimaleProduktivitaetKopf;
    public final float optimaleProduktivitaetKoerper;
    public final float arbeitszeit;
    public final MusterSchaden musterSchaden;

    public Assignment(float optimaleProduktivitaetKopf, float optimaleProduktivitaetKoerper,
                      float arbeitszeit) {
        this(optimaleProduktivitaetKopf, optimaleProduktivitaetKoerper, arbeitszeit,
                ZERO_MUSTER_SCHADEN);
    }

    public Assignment(float optimaleProduktivitaetKopf, float optimaleProduktivitaetKoerper,
                      float arbeitszeit, MusterSchaden musterSchaden) {
        this.optimaleProduktivitaetKopf = optimaleProduktivitaetKopf;
        this.optimaleProduktivitaetKoerper = optimaleProduktivitaetKoerper;
        this.arbeitszeit = arbeitszeit;
        this.musterSchaden = musterSchaden;
    }
}
