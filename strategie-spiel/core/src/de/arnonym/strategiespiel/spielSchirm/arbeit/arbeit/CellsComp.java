package de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.function.BooleanSupplier;

import de.arnonym.strategiespiel.framework.ecs.component.Comp;
import de.arnonym.strategiespiel.framework.ecs.component.Component;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.framework.werkzeuge.functionalInterfaces.ValueToBoolean;
import de.arnonym.strategiespiel.spielSchirm.Enums;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.kollision.KollisionComp;
import de.arnonym.strategiespiel.spielSchirm.worldMap.Zelle;

public class CellsComp extends Comp implements Iterable<Zelle> {
    private final Set<Zelle> cells = new HashSet<>();

    public CellsComp(Entity entity) {
        super(entity);
    }

    public boolean addCell(Zelle zelle) {
        if (cells.add(zelle)) {
            support.firePropertyChange(Enums.PROBERTY_CHANGE_ZELLE, null, zelle);
            return true;
        }
        return false;
    }

    public boolean removeCell(Zelle zelle) {
        if (cells.remove(zelle)) {
            support.firePropertyChange(Enums.PROBERTY_CHANGE_ZELLE, zelle, null);
            return true;
        }
        return false;
    }

    public int size() {
        return cells.size();
    }

    public boolean isEmpty() {
        return size() == 0;
    }

    public Set<Zelle> cells() {
        return new HashSet<>(cells);
    }

    public Set<Zelle> cells(ValueToBoolean<Zelle> valueToBoolean) {
        Set<Zelle> cells = new HashSet<>();
        for (Zelle cell : this.cells) {
            if (valueToBoolean.to(cell)) {
                cells.add(cell);
            }
        }
        return cells;
    }

    public <C extends Component> Set<C> comps(Class<C> clazz) {
        Set<C> cs = new HashSet<>();
        for (Zelle cell : cells) {
            for (Entity entity : cell.onCellEntities()) {
                C c = entity.gettComponent(clazz);
                if (c != null) {
                    cs.add(c);
                }
            }
        }
        return cs;
    }

    @Override
    public Iterator<Zelle> iterator() {
        return cells.iterator();
    }
}
