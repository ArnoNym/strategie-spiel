package de.arnonym.strategiespiel.spielSchirm.arbeit.step;

import com.badlogic.gdx.math.Vector2;

import java.util.Collection;

import de.arnonym.strategiespiel.framework.listener.StoryTeller;
import de.arnonym.strategiespiel.framework.werkzeuge.collections.Collector;
import de.arnonym.strategiespiel.spielSchirm.balancing.IdsRessourcen;
import de.arnonym.strategiespiel.spielSchirm.umwelt.ressourcen.RessourcenFactory;
import de.arnonym.strategiespiel.spielSchirm.arbeit.auftrag.Assignment;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.bewegung.BewegungComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.EinkommenComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.GeldComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.werte.HabitalwerteComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.werte.ProduktivitaetComp;
import de.arnonym.strategiespiel.framework.werkzeuge.collections.PropertyChangeList;
import de.arnonym.strategiespiel.spielSchirm.worldMap.Zelle;
import de.arnonym.strategiespiel.spielSchirm.Enums;

public class StepAnpflanzen extends StepArbeit<Assignment> {
    public final RessourcenFactory ressourcenFactory;

    public final Zelle zelle;
    private final int anpflanzenId;

    public StepAnpflanzen(RessourcenFactory ressourcenFactory,

                          GeldComp gGeldComp, int salary,

                          BewegungComp bewegungComp, GeldComp aGeldComp,
                          HabitalwerteComp habitalwerteComp, ProduktivitaetComp produktivitaetComp,

                          Zelle zelle, int anpflanzenId, Vector2 start_nl) {
        super(new Collector<>(), zelle.bewegungszielComp, gGeldComp, salary,
                IdsRessourcen.ANPFLANZEN_MUSTER_AUFTRAG, bewegungComp, aGeldComp,
                habitalwerteComp, produktivitaetComp, start_nl);
        this.ressourcenFactory = ressourcenFactory;
        this.zelle = zelle;
        this.anpflanzenId = anpflanzenId;
    }

    @Override
    protected Enums.EnumStpStatus versuchen_or_or_or() {
        if (!zelle.reservierer.issFrei() || zelle.isKollisionBauen()) {
            return Enums.EnumStpStatus.FAIL;
        }
        return Enums.EnumStpStatus.OK;
    }

    @Override
    protected void reservieren(float arbeitszeit) {
        super.reservieren(arbeitszeit);
        zelle.reservierer.reservieren();
    }

    @Override
    protected void reservierenRueckgaengig_or() {
        super.reservierenRueckgaengig_or();
        zelle.reservierer.freigeben();
    }

    @Override
    public void erledigt_or() {
        super.erledigt_or();

        ressourcenFactory.createNeu(anpflanzenId, zelle.position);

        zelle.reservierer.freigeben();
    }

    /*@Override
    protected Enums.EnumStepStatus versuchen_or_or() {
        return pruefenObBeenden();
    }

    @Override
    protected void anfangen_or() {
        pflanzenPause = new PauseDelta(KonstantenBalancing.RESSOURCEN_ANBAUEN_DAUER_sekunden,
                Enums.EnumPauseStart.JETZT);
    }

    @Override
    public boolean pruefenObErledigt() {
        StatischesObjekt so = ((Zelle) getZiel()).getStatischesObjekt();
        if (so != null && so.id() == pflanzenRessourceId) {
            return true;
        }
        return false;
    }

    @Override
    public boolean pruefenObFail() {
        StatischesObjekt so = ((Zelle) getZiel()).getStatischesObjekt();
        if (so != null && so.id() != pflanzenRessourceId) {
            return true;
        }
        return false;
    }

    @Override
    protected void durchfuehren_or(float delta) {
        if (!pflanzenPause.issVorbei(delta, false)) {
            return;
        }
        ((Zelle) getZiel()).settStatischesObjekt(
                (IdsRessourcenWachsen.toList(pflanzenRessourceId, Enums.EnumResNwStufe.JUNG))
                        .finialize(getDorfbewohner().getLevel(),
                                getDorfbewohner().getLevel().getRessourcenListe(),
                                getDorfbewohner().getLevel().getRenderArrayList(), true,
                                (Zelle) getZiel(), Enums.EnumAbbauen.NEIN, false));
        setStatus(Enums.EnumStepStatus.ERLEDIGT);
    }

    @Override
    protected void failNachVersuchenVorAnfangen_or() {

    }

    @Override
    protected void failNachAnfangenVorDurchfuehren_or() {

    }

    @Override
    protected void failNachDurchfuehren_or() {

    }

    @Override
    public void erledigt_or() {

    }*/
}
