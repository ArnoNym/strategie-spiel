package de.arnonym.strategiespiel.spielSchirm.arbeit.step.arbeitszeit.material;

import com.badlogic.gdx.math.Vector2;

import java.util.Map;

import de.arnonym.strategiespiel.framework.ecs.component.Comp;
import de.arnonym.strategiespiel.framework.werkzeuge.collections.Collector;
import de.arnonym.strategiespiel.spielSchirm.balancing.Konstanten;
import de.arnonym.strategiespiel.spielSchirm.arbeit.TimeClock;
import de.arnonym.strategiespiel.spielSchirm.arbeit.auftrag.AssignmentMaterial;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.bewegung.BewegungComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.bewegung.BewegungszielComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.gegenstaende.GegenstaendeComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.EinkommenComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.GeldComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.herrschaft.EigentumComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.werte.HabitalwerteComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.werte.ProduktivitaetComp;
import de.arnonym.strategiespiel.spielSchirm.arbeit.step.arbeitszeit.StepArbeitszeit;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.endstufe.Gegenstand;

public abstract class StepMaterial<Assignment extends AssignmentMaterial> extends StepArbeitszeit<Assignment> {
    public final EigentumComp zEigentumComp;
    public final GegenstaendeComp zGegenstaendeComp;

    public StepMaterial(Collector<Comp> comps, TimeClock zTimeClock, BewegungszielComp zBewegungszielComp,
                        EigentumComp zEigentumComp, GegenstaendeComp zGegenstaendeComp,

                        Assignment assignmentMaterial,

                        GeldComp gGeldComp, int salary,

                        BewegungComp aBewegungComp, GeldComp aGeldComp,
                        HabitalwerteComp aHabitalwerteComp, ProduktivitaetComp aProduktivitaetComp,

                        Vector2 start_nl) {
        super(comps.add(zEigentumComp, zGegenstaendeComp), zTimeClock, zBewegungszielComp,
                assignmentMaterial, gGeldComp, salary, aBewegungComp, aGeldComp, aHabitalwerteComp,
                aProduktivitaetComp, start_nl);
        this.zEigentumComp = zEigentumComp;
        this.zGegenstaendeComp = zGegenstaendeComp;
    }

    // ERLEDIGT ///////////////////////////////////////////////////////////////////////////////////

    @Override
    protected final void auftragErledigt() {
        for (Map.Entry<Integer, Integer> entry : assignment.idMengeHashMap.entrySet()) {
            zGegenstaendeComp.gegenstandListe.gettDurchId(entry.getKey())
                    .mengeVerringern(entry.getValue(), false);
        }
        auftragErledigt_or();
    }

    protected abstract void auftragErledigt_or();

    // ARBEITSZEIT ////////////////////////////////////////////////////////////////////////////////

    @Override
    protected float gettZuPlanenArbeitsZeit() {
        int hatSummeMenge = 0;
        int sollSummeMenge = 0;

        for (Map.Entry<Integer, Integer> entry : assignment.idMengeHashMap.entrySet()) {
            Gegenstand geg = zGegenstaendeComp.gegenstandListe.gettDurchId(entry.getKey());
            hatSummeMenge += geg.gettMenge(true, false, false);
            sollSummeMenge += entry.getValue();
        }

        if (sollSummeMenge == 0) {
            if (Konstanten.DEVELOPER_VISUELL_STEP_SYSTEM_OUT) {
                // Kein Fehler aber ungewoehnlich
                System.out.println("StepMaterial ::: sollSummeMenge == 0");
            }
            return Float.MAX_VALUE;
        }

        float darfVerplant_prozent = Math.min(1, (float) hatSummeMenge / sollSummeMenge);
        float arbeitszeit = zTimeClock.getArbeitszeit();
        float restZuPlanenArbeitszeit = zTimeClock.getRestZuPlanenArbeitszeit();
        float istVerplant_prozent = (arbeitszeit - restZuPlanenArbeitszeit) / arbeitszeit;

        float zuPlanenArbeitszeit = (darfVerplant_prozent - istVerplant_prozent) * arbeitszeit;
        if (Konstanten.DEVELOPER_VISUELL_STEP_SYSTEM_OUT && zuPlanenArbeitszeit == 0) {
            System.out.println("StepMaterial ::: darfVerplant_prozent: " + darfVerplant_prozent + ", arbeitszeit: "
                    + arbeitszeit + ", restZuPlanenArbeitszeit: " + restZuPlanenArbeitszeit
                    + ", istVerplant_prozent: " + istVerplant_prozent);
        }

        return zuPlanenArbeitszeit;
    }
}
