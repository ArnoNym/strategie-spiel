package de.arnonym.strategiespiel.spielSchirm.arbeit.task.liefernArbeitszeit.anliefern;

import de.arnonym.strategiespiel.framework.stp.StpComp;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.departments.material.HerstellerDepartmentComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.bewegung.BewegungComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.gegenstaende.GegenstaendeComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.EinkommenComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.werte.HabitalwerteComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.werte.ProduktivitaetComp;
import de.arnonym.strategiespiel.spielSchirm.ui.aufsteigendeSymbole.AufsteigendesSymbolFactory;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.listSystems.privateListSystems.SysLager;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dorfbewohner.tauschen.plan.legen.BerufLegenPlan;
import de.arnonym.strategiespiel.framework.stp.stacks.StepStack;
import de.arnonym.strategiespiel.spielSchirm.arbeit.step.arbeitszeit.material.StepHerstellen;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.endstufe.Gegenstand;
import de.arnonym.strategiespiel.framework.werkzeuge.collections.hashMaps.IntIntHashMap;

public class HerstellenTask extends AnliefernTask {
    public final HerstellerDepartmentComp herstellerArbeitgeberComp;

    private boolean verkaufen = false;

    private boolean todoloeschentest = false; // todo loeschen

    public HerstellenTask(AufsteigendesSymbolFactory aufsteigendesSymbolFactory,

                          SysLager sysLager,

                          HerstellerDepartmentComp herstellerArbeitgeberComp,

                          StpComp stpComp, BewegungComp bewegungComp,
                          EinkommenComp einkommenComp,
                          GegenstaendeComp gegenstaendeComp, HabitalwerteComp habitalwerteComp,
                          ProduktivitaetComp produktivitaetComp) {
        super(aufsteigendesSymbolFactory, sysLager,
                herstellerArbeitgeberComp.employerComp.eigentuemerComp.geldComp,
                stpComp, bewegungComp, einkommenComp, gegenstaendeComp, habitalwerteComp,
                produktivitaetComp);
        this.herstellerArbeitgeberComp = herstellerArbeitgeberComp;
    }

    @Override
    public void planeAlleSteps(StepStack stepStack) {
        if (todoloeschentest) throw new IllegalArgumentException();
        todoloeschentest = true;

        StepHerstellen stepHerstellen = new StepHerstellen(herstellerArbeitgeberComp, aBewegungComp,
                aGeldComp, aHabitalwerteComp, aProduktivitaetComp, null);

        switch (stepStack.handle(stepHerstellen, true)) {
            case OK:
            case FAIL_BEI_GEHE_ZU:
                return;
        }

        if (verkaufen = verkaufen(stepStack)) {
            return;
        }

        einkaufen(stepStack, stepHerstellen);
    }

    @Override
    public void planeNaechstenStep(StepStack stepStack) {
        /* todo ausklammern
        if (verkaufen && getCurrentStep() instanceof StepLegeIn) {
            einkaufen(stepStack, herstellerArbeitgeberComp.bewegungszielComp,
                    herstellerArbeitgeberComp.aGegenstaendeComp, herstellerArbeitgeberComp.geldComp,
                    herstellerArbeitgeberComp.eigentuemerComp,
                    herstellerArbeitgeberComp.getAssignment_nl().idMengeHashMap,
                    aBewegungComp.compPosition.position);
        }*/
    }

    private boolean verkaufen(StepStack stepStack) {
        GegenstaendeComp gGegenstaendeComp = herstellerArbeitgeberComp.gegenstaendeComp;
        IntIntHashMap inputHashMap = herstellerArbeitgeberComp.getAssignment_nl().idMengeHashMap;
        IntIntHashMap zuLegenHashMap = new IntIntHashMap();

        for (Gegenstand g : gGegenstaendeComp.gegenstandListe) {
            int id = g.gegMuster.id;
            int mengeVerfuegbar = g.getMengeVerfuegbar();
            if (mengeVerfuegbar > 0 && !inputHashMap.containsKey(id)) {
                zuLegenHashMap.put(id, mengeVerfuegbar);
            }
        }

        stepStack.handle(new BerufLegenPlan(aufsteigendesSymbolFactory, sysLager,

                herstellerArbeitgeberComp.employerComp.bewegungszielComp, gGegenstaendeComp,
                herstellerArbeitgeberComp.employerComp.eigentuemerComp.geldComp,

                aBewegungComp, aGeldComp, aGegenstaendeComp,

                zuLegenHashMap, null, false,
                aBewegungComp.positionComp.position));
        return !stepStack.isEmpty();
    }
}
