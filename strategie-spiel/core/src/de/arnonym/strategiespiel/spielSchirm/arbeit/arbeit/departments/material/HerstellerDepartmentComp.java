package de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.departments.material;

import java.util.Map;

import de.arnonym.strategiespiel.framework.werkzeuge.collections.Collector;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.EmployerComp;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.VarEmployerComp;
import de.arnonym.strategiespiel.spielSchirm.balancing.IdsGegenstaende;
import de.arnonym.strategiespiel.spielSchirm.balancing.Konstanten;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.ArbeitnehmerAi;
import de.arnonym.strategiespiel.spielSchirm.arbeit.auftrag.AssignmentHerstellen;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.gegenstaende.GegenstaendeComp;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.framework.stp.Task;
import de.arnonym.strategiespiel.spielSchirm.arbeit.task.liefernArbeitszeit.anliefern.HerstellenTask;
import de.arnonym.strategiespiel.spielSchirm.info.muster.GegInfo;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.endstufe.Gegenstand;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.endstufe.GegenstandList;
import de.arnonym.strategiespiel.framework.werkzeuge.collections.hashMaps.IntIntHashMap;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.MathUtils;

public class HerstellerDepartmentComp extends MaterialDepartmentComp<GegInfo, AssignmentHerstellen> {
    public HerstellerDepartmentComp(Entity entity,

                                    VarEmployerComp<GegInfo> employerComp,
                                    GegenstaendeComp gegenstaendeComp) {
        super(entity, new Collector<>(), employerComp, gegenstaendeComp);
    }

    @Override
    public Task createTask_nl(ArbeitnehmerAi arbeitnehmerAi) {
        return new HerstellenTask(
                arbeitnehmerAi.aufsteigendesSymbolFactory, arbeitnehmerAi.sysLager,

                this,

                arbeitnehmerAi.stpComp, arbeitnehmerAi.bewegungComp,
                arbeitnehmerAi.einkommenComp, arbeitnehmerAi.gegenstaendeComp,
                arbeitnehmerAi.habitalwerteComp, arbeitnehmerAi.produktivitaetComp);
    }

    @Override
    protected AssignmentHerstellen getAssignment(GegInfo gegInfo) {
        return gegInfo.getMusterAuftragHerstellen();
    }

    @Override
    protected void newProduct(GegInfo gegInfo) {
        calcMaxMengen(getAssignment_nl());
        super.newProduct(gegInfo);
    }

    /** In das Gebaude sollen unterschiedliche Mengen von unterschiedlich grossen Outputs passen.
    In jedem Fall sollen jedoch genug Einheiten der Inputs Platz finden um die zuvor errechnete
    Menge an Outputs zu erstellen. */
    private void calcMaxMengen(AssignmentHerstellen newAssignment_nl) {
        GegenstandList gl = gegenstaendeComp.gegenstandListe;

        if (newAssignment_nl == null) {
            for (Gegenstand g : gl) {
                g.setMengeMaximal(0);
            }
            gl.settMaxPlatz(0, false, false);
            return;
        }
        int produktId_code = newAssignment_nl.id;

        Gegenstand output = gl.gettDurchId(produktId_code);

        int mengeOutput = MathUtils.myIntDiv(
                gl.getMaxPlatzUrspruenglich(),
                output.gegMuster.platzVerbrauch);
        output.setMengeMaximal(mengeOutput);

        int proOutputMitInputPlatz = output.gegMuster.platzVerbrauch;
        IntIntHashMap inputHashMap = output.gegMuster.getMusterAuftragHerstellen().idMengeHashMap;
        for (Map.Entry<Integer, Integer> entry : inputHashMap.entrySet()) {
            int key = entry.getKey();
            int value = entry.getValue();

            int maximaleMenge = mengeOutput * value / Konstanten.NACHKOMMASTELLEN;
            gl.gettDurchId(key).setMengeMaximal(maximaleMenge);
            proOutputMitInputPlatz += value * IdsGegenstaende.get(key).platzVerbrauch
                    / Konstanten.NACHKOMMASTELLEN;
        } //todo verwende myIndiv und mult??

        gl.settMaxPlatz(mengeOutput * proOutputMitInputPlatz / Konstanten.NACHKOMMASTELLEN,
                false, false);
    }
}
