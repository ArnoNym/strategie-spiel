package de.arnonym.strategiespiel.spielSchirm.arbeit.task.liefernArbeitszeit;

import com.badlogic.gdx.math.Vector2;

import de.arnonym.strategiespiel.spielSchirm.balancing.Konstanten;
import de.arnonym.strategiespiel.framework.stp.StpComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.bewegung.BewegungComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.gegenstaende.GegenstaendeComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.EinkommenComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.GeldComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.herrschaft.EigentuemerComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.lager.LagerComp;
import de.arnonym.strategiespiel.spielSchirm.bodenStapel.BodenStapel;
import de.arnonym.strategiespiel.spielSchirm.bodenStapel.BodenStapelFactory;
import de.arnonym.strategiespiel.spielSchirm.ui.aufsteigendeSymbole.AufsteigendesSymbolFactory;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.listSystems.privateListSystems.SysLager;
import de.arnonym.strategiespiel.framework.stp.stacks.StepStack;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dorfbewohner.tauschen.plan.legen.PrivatLegenPlan;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dorfbewohner.tauschen.step.StepLegeIn;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.endstufe.Gegenstand;
import de.arnonym.strategiespiel.spielSchirm.Enums;

public class TaskLoswerden extends TransportArbeitszeitTask {
    private final AufsteigendesSymbolFactory aufsteigendesSymbolFactory;
    private final BodenStapelFactory bodenStapelFactory;
    private final SysLager sysLager;
    private final BewegungComp bewegungComp;
    private final GegenstaendeComp gegenstaendeComp;
    private final EigentuemerComp eigentuemerComp;
    private final LagerComp zuvorGenommenAusLager_Comp_nl;
    private final boolean mussLohnen;
    private final Vector2 startPosition;

    public TaskLoswerden(AufsteigendesSymbolFactory aufsteigendesSymbolFactory,
                         BodenStapelFactory bodenStapelFactory,

                         SysLager sysLager,

                         EigentuemerComp gEigentuemerComp,
                         GeldComp gGeldComp, int salary,

                         StpComp aStpComp, BewegungComp bewegungComp,
                         GeldComp aGeldComp, GegenstaendeComp gegenstaendeComp,

                         LagerComp zuvorGenommenAusLager_Comp_nl, boolean mussLohnen,

                         Vector2 start_nl) {
        super(gGeldComp, salary, aGeldComp, aStpComp, bewegungComp,
                gEigentuemerComp, gegenstaendeComp);
        this.aufsteigendesSymbolFactory = aufsteigendesSymbolFactory;
        this.bodenStapelFactory = bodenStapelFactory;
        this.sysLager = sysLager;
        this.bewegungComp = bewegungComp;
        this.gegenstaendeComp = gegenstaendeComp;
        this.eigentuemerComp = gEigentuemerComp;
        this.zuvorGenommenAusLager_Comp_nl = zuvorGenommenAusLager_Comp_nl;
        this.mussLohnen = mussLohnen;
        if (start_nl == null) {
            this.startPosition = bewegungComp.positionComp.getPosition();
        } else {
            this.startPosition = start_nl;
        }
    }

    public TaskLoswerden(AufsteigendesSymbolFactory aufsteigendesSymbolFactory,
                         BodenStapelFactory bodenStapelFactory,

                         SysLager sysLager,

                         EigentuemerComp aEigentuemerComp,
                         StpComp aStpComp, BewegungComp bewegungComp,
                         EinkommenComp aEinkommenComp, GegenstaendeComp gegenstaendeComp,

                         LagerComp zuvorGenommenAusLager_Comp_nl, boolean mussLohnen,

                         Vector2 start_nl) {
        this(aufsteigendesSymbolFactory, bodenStapelFactory, sysLager, aEigentuemerComp,
                aEinkommenComp.geldComp, aEinkommenComp.getEinkommen(), aStpComp, bewegungComp,
                aEinkommenComp.geldComp, gegenstaendeComp, zuvorGenommenAusLager_Comp_nl,
                mussLohnen, start_nl);
    }

    @Override
    public void planeAlleSteps(StepStack stepStack) {
        // Pruefen ob ueberhaupt weggelegt werden muss
        if (!gegenstaendeComp.gegenstandListe.issMenge(true, true, false)) {
            return;
        }

        // Verkaufe an bestes Lager oder lege in besten BodenStapel
        stepStack.handle(new PrivatLegenPlan(aufsteigendesSymbolFactory, sysLager, bewegungComp,
                aGeldComp, gegenstaendeComp, eigentuemerComp.geldComp, salary,
                zuvorGenommenAusLager_Comp_nl, mussLohnen, startPosition));todo nur privat??
        if (!stepStack.isEmpty()) {
            return;
        }

        if (Konstanten.DEVELOPER_VISUELL_STEP_SYSTEM_OUT) {
            System.out.println("- TaskLoswerden ::: Neuer BodenStapel");
        }

        // Neuen BodenStapel erstellen
        int bodenStapelGegId = -1;
        for (Gegenstand g : gegenstaendeComp.gegenstandListe) {
            if (g.getMengeVerfuegbar() > 0) {
                bodenStapelGegId = g.gettId();
                break;
            }
        }
        if (bodenStapelGegId == -1) {
            throw new IllegalStateException();
        }
        BodenStapel bodenStapel = bodenStapelFactory.create(eigentuemerComp, bodenStapelGegId, 0,
                bewegungComp.positionComp.getPosition());
        StepLegeIn stepLegeIn = new StepLegeIn(aufsteigendesSymbolFactory,
                bewegungComp,
                bodenStapel.getBewegungszielComp(),
                gegenstaendeComp,
                bodenStapel.getGegenstaendeComp(),
                gGeldComp,
                null);

        Enums.EnumStpStatus stepStatus = stepLegeIn.versuchen();
        if (stepStatus == Enums.EnumStpStatus.FAIL_BEI_GEHE_ZU){
            throw new IllegalStateException("TODO Machen, dass Stapel nur auf erreichbaren Zellen erstellt werden");
        }
        stepStack.handle(stepLegeIn, true);
    }

    @Override
    public void planeNaechstenStep(StepStack stepStack) {

    }
}
