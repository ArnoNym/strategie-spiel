package de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.framework.stp.Task;
import de.arnonym.strategiespiel.spielSchirm.Enums;
import de.arnonym.strategiespiel.spielSchirm.arbeit.auftrag.Assignment;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.bewegung.BewegungszielComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.herrschaft.EigentuemerComp;
import de.arnonym.strategiespiel.spielSchirm.info.muster.Info;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.werte.ProduktivitaetComp;

public class VarEmployerComp<ProductInfo extends Info> extends EmployerComp<ProductInfo> {
    private final Set<VarDepartmentComp> varDepartmentComps = new HashSet<>();

    public final ProductInfo[] possibleProducts;
    private ProductInfo product;

    public final EmploymentMatcher employmentMatcher;
    private final Set<ArbeitnehmerAi> arbeitnehmers = new HashSet<>();

    private int gehalt_sekunde = -1; /* ... pro Sekunde! Arbeitnehmer koennen sich die Arbeit suchen fuer die
    sie am meisten Geld pro Sekunde bekommen = am wenigsten Zeit benoetigen = am Produktivsten
    sind */

    public VarEmployerComp(EmploymentMatcher employmentMatcher,

                        Entity entity,

                        BewegungszielComp bewegungszielComp, EigentuemerComp eigentuemerComp,

                        ProductInfo[] possibleProducts, ProductInfo product_nl, int maxStellen) {
        super(employmentMatcher, entity, bewegungszielComp, eigentuemerComp, maxStellen);
        this.employmentMatcher = employmentMatcher;
        this.possibleProducts = possibleProducts;
        if (product_nl != null) {
            setProduct(product_nl);
        }
        super.placeholderAssignment = new PlaceholderAssignment(varDepartmentComps);
    }

    public Task createTask(ArbeitnehmerAi arbeitnehmerAi) {
        for (VarDepartmentComp varDepartmentComp : varDepartmentComps) {
            return varDepartmentComp.createTask_nl(arbeitnehmerAi);
        }
        return null;
    }

    public void abortAi() {
        for (ArbeitnehmerAi arbeitnehmerAi : arbeitnehmers) {
            arbeitnehmerAi.beenden();
        }
    }

    // DEPARTMENTS ////////////////////////////////////////////////////////////////////////////////

    @Override
    protected EmployerComp<ProductInfo> addDepartment(DepartmentComp departmentComp) {
        if (departmentComp instanceof VarDepartmentComp) {
            varDepartmentComps.add((VarDepartmentComp) departmentComp);
        }
        return super.addDepartment(departmentComp);
    }


    // ANGESTELLTE ////////////////////////////////////////////////////////////////////////////////

    public void addArbeitnehmer(ArbeitnehmerAi arbeitnehmerAi) {
        arbeitnehmers.add(arbeitnehmerAi);
    }

    public void removeArbeitnehmer(ArbeitnehmerAi arbeitnehmerAi) {
        arbeitnehmers.remove(arbeitnehmerAi);
    }

    public int freieStellen() {
        return maxStellen - arbeitnehmers.size();
    }

    public boolean hasFreieStellen() {
        return freieStellen() == 0;
    }

    public int getMaxStellen() {
        return maxStellen;
    }

    public int getAnzahlAngestellte() {
        return arbeitnehmers.size();
    }

    public ArbeitnehmerAi[] arbeitnehmerArray() {
        return arbeitnehmers.toArray(new ArbeitnehmerAi[arbeitnehmers.size()]);
    }

    public <COLL extends Collection<ArbeitnehmerAi>> COLL arbeitnehmers(COLL coll) {
        coll.addAll(arbeitnehmers);
        return coll;
    }

    // GEHALT /////////////////////////////////////////////////////////////////////////////////////

    private void calculateGehalt() {
        float sumProdKopf = 0;
        float sumProdKoerper = 0;
        int count = 0;
        for (VarDepartmentComp varDepartmentComp : varDepartmentComps) {
            if (varDepartmentComp.hasAssignment()) {
                Assignment assignment = varDepartmentComp.getAssignment_nl();
                sumProdKopf += assignment.optimaleProduktivitaetKopf;
                sumProdKoerper += assignment.optimaleProduktivitaetKoerper;
                count++;
            }
        }
        float avgProdKopf = sumProdKopf / count;
        float avgProdKoerper = sumProdKoerper / count;

        this.gehalt_sekunde = Math.max(1, employmentMatcher.schaetzeGehalt(avgProdKopf, avgProdKoerper,
                bewegungszielComp.compKollisionLaufen.positionComp.position));
    }

    public int getGehalt_sekunde() {
        return gehalt_sekunde;
    }

    public int getMaxGehalt() {
        throw new IllegalStateException(); // todo
    }

    public int realGehalt_sekunde(ProduktivitaetComp produktivitaetComp) {
        return realGehalt_sekunde(produktivitaetComp.realProduktivitaet(
                placeholderAssignment.getProdKopf(), placeholderAssignment.getProdKoerper()));
    }

    public int realGehalt_sekunde(float realProduktivitaet) {
        return (int) (gehalt_sekunde / realProduktivitaet);
    }

    // PRODUCT ////////////////////////////////////////////////////////////////////////////////////

    public void setProduct(ProductInfo product) {
        abortAi();

        support.firePropertyChange(Enums.PROBERTY_CHANGE_PRODUCT, this.product, product);
        this.product = product;

        calculateGehalt();
    }

    public ProductInfo getProduct() {
        return product;
    }

    /*
    // FINDE ARBEIT FUER ABTEILUNGEN //////////////////////////////////////////////////////////////

    protected boolean checkForWork(Zelle z) {
        boolean gefunden = false;
        List<Entity> entityIdList = new ArrayList<>(); // Da kollision Laufen und Bauen
        for (KollisionComp kollisionComp : z.onCellKollisionComps()) {
            Entity entity = kollisionComp.entity;
            if (!entityIdList.contains(entity)) {
                gefunden = gefunden | checkForWork(entity);
                entityIdList.add(entity);
            }
        }
        return gefunden;
    }

    private boolean checkForWork(Entity e) {
        return e.gettComponents(new ArrayList<>(),
                s -> (s instanceof Component && checkForWork(e, (Component) s))).size() > 0;
    }

    protected boolean checkForWork(Entity e, Component c) {
        if (c instanceof AbbaubarComp) {
            abbauenDepartmentComp.assignmentObjecs.add((AbbaubarComp) c);
            return true;
        }
        return false;
    }
    */
}
