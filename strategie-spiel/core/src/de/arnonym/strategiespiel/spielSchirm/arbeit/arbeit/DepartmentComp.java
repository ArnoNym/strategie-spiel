package de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.HashSet;
import java.util.Set;

import de.arnonym.strategiespiel.framework.ecs.collections.System;
import de.arnonym.strategiespiel.framework.ecs.component.Comp;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.framework.stp.Task;
import de.arnonym.strategiespiel.framework.werkzeuge.collections.Collector;
import de.arnonym.strategiespiel.spielSchirm.arbeit.auftrag.Assignment;

public abstract class DepartmentComp<E extends EmployerComp, A extends Assignment> extends Comp implements PropertyChangeListener {
    public final E employerComp;

    protected DepartmentComp(Collector<System> systems,
                          Entity entity,
                          Collector<Comp> comps,
                          E employerComp) {
        super(systems.toArray(new System[]{}), entity,
                new Collector<>(comps, employerComp).getSet().toArray(new Comp[]{}));
        this.employerComp = employerComp;
        employerComp.addDepartment(this);
    }

    public abstract Task createTask_nl(ArbeitnehmerAi arbeitnehmerAi);

    @Override
    public void propertyChange(PropertyChangeEvent propertyChangeEvent) {

    }

    public abstract A getAssignment_nl();
}
