package de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.departments.gebiet;

import de.arnonym.strategiespiel.framework.werkzeuge.collections.Collector;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.CellsComp;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.CellsDepartmentComp;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.VarEmployerComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.ausbeuten.FruechteComp;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.ArbeitnehmerAi;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.framework.stp.Task;
import de.arnonym.strategiespiel.spielSchirm.arbeit.task.liefernArbeitszeit.ausbeuten.TaskErnten;
import de.arnonym.strategiespiel.spielSchirm.umwelt.ressourcen.RessourceInfo;
import de.arnonym.strategiespiel.spielSchirm.umwelt.ressourcen.stufen.sekundaer.SWachstumRessourceInfo;

public class ErntenDepartmentComp extends CellsDepartmentComp<RessourceInfo, RessourcenAssignment> {

    public ErntenDepartmentComp(Entity entity, CellsComp cellsComp,
                                VarEmployerComp<? extends RessourceInfo> employerComp) {
        super(new Collector<>(), entity, new Collector<>(), cellsComp, employerComp);
    }

    @Override
    protected RessourcenAssignment getAssignment(RessourceInfo ressourceInfo) {
        if (ressourceInfo instanceof SWachstumRessourceInfo) {
            return ((SWachstumRessourceInfo) ressourceInfo).erntenAssignment;
        }
        return null;
    }

    @Override
    public Task createTask_nl(ArbeitnehmerAi arbeitnehmerAi) {
        RessourcenAssignment assignment = getAssignment_nl();
        if (assignment == null) {
            return null;
        }

        return new TaskErnten(arbeitnehmerAi.aufsteigendesSymbolFactory,
                arbeitnehmerAi.bodenStapelFactory, arbeitnehmerAi.sysLager,

                cellsComp.comps(FruechteComp.class),

                employerComp.eigentuemerComp, employerComp.eigentuemerComp.geldComp,

                arbeitnehmerAi.stpComp, arbeitnehmerAi.bewegungComp,
                arbeitnehmerAi.einkommenComp, arbeitnehmerAi.gegenstaendeComp,
                arbeitnehmerAi.habitalwerteComp, arbeitnehmerAi.produktivitaetComp);
    }
}
