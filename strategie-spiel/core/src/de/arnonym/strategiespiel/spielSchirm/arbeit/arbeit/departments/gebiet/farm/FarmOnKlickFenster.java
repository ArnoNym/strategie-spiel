package de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.departments.gebiet.farm;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import java.util.ArrayList;
import java.util.List;

import de.arnonym.strategiespiel.spielSchirm.World;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.CellsComp;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.EmployerComp;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.VarEmployerComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.name.NameComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.rendern.HashMapRenderComp;
import de.arnonym.strategiespiel.spielSchirm.info.muster.Info;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.anzeige.RessourceAnzeige;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.buttons.icons.ProductIcon;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterOnKlick.OnKlickFenster;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterOnKlick.content.LandGebenButton;
import de.arnonym.strategiespiel.spielSchirm.umwelt.ressourcen.stufen.WachstumRessourceInfo;
import de.arnonym.strategiespiel.spielSchirm.ui.DiverseInputs;
import de.arnonym.strategiespiel.spielSchirm.ui.auswahlkaesten.AuswahlkastenLandUebergeben;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterOnKlick.content.IconTable;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.SpecialUtils;

public class FarmOnKlickFenster extends OnKlickFenster {
    public FarmOnKlickFenster(World world,

                              CellsComp cellsComp,
                              VarEmployerComp<WachstumRessourceInfo> employerComp,
                              NameComp nameComp, HashMapRenderComp hashMapRenderComp,

                              Skin skin) {
        super(world, skin, 500, 800, nameComp, hashMapRenderComp);
        DiverseInputs diverseInputs = world.getDiverseInputs();

        Button button = new LandGebenButton(world, cellsComp, skin);
        getContentTable().add(button);

        SpecialUtils.leerzeile(getContentTable());

        List<ProductIcon<WachstumRessourceInfo>> iconList = new ArrayList<>();
        for (WachstumRessourceInfo wachstumRessourceInfo : employerComp.possibleProducts) {
            iconList.add(new ProductIcon<>(ingameCursorManager, this,
                    new RessourceAnzeige(wachstumRessourceInfo), employerComp, wachstumRessourceInfo,
                    world.spielerDorf.forschungsstandList.gettDurchId(wachstumRessourceInfo.id)));
        }
        IconTable iconTable = new IconTable(skin, 5, 5, 5,
                iconList.toArray(new ProductIcon[iconList.size()]));
        getContentTable().add(iconTable).expandX().fillX().left();
        getUpdateTableList().add(iconTable);
    }
}
