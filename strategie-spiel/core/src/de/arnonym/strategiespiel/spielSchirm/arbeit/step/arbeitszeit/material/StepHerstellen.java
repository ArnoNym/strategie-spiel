package de.arnonym.strategiespiel.spielSchirm.arbeit.step.arbeitszeit.material;

import com.badlogic.gdx.math.Vector2;

import java.beans.PropertyChangeEvent;

import de.arnonym.strategiespiel.framework.ecs.component.Comp;
import de.arnonym.strategiespiel.framework.werkzeuge.collections.Collector;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.departments.material.HerstellerDepartmentComp;
import de.arnonym.strategiespiel.spielSchirm.arbeit.auftrag.AssignmentHerstellen;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.bewegung.BewegungComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.EinkommenComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.werte.HabitalwerteComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.werte.ProduktivitaetComp;
import de.arnonym.strategiespiel.spielSchirm.Enums;

public class StepHerstellen extends StepMaterial<AssignmentHerstellen> {
    public final HerstellerDepartmentComp herstellerDepartmentComp;

    public StepHerstellen(HerstellerDepartmentComp herstellerDepartmentComp,

                          BewegungComp aBewegungComp, EinkommenComp aEinkommenComp,
                          HabitalwerteComp aHabitalwerteComp,
                          ProduktivitaetComp aProduktivitaetComp,

                          Vector2 start_nl) {
        super(new Collector<>(herstellerDepartmentComp),
                herstellerDepartmentComp.timeClock,
                herstellerDepartmentComp.employerComp.bewegungszielComp,
                herstellerDepartmentComp.employerComp.eigentuemerComp,
                herstellerDepartmentComp.gegenstaendeComp,
                herstellerDepartmentComp.getAssignment_nl(),
                herstellerDepartmentComp.employerComp.eigentuemerComp.geldComp,
                aEinkommenComp.getEinkommen(),
                aBewegungComp,
                aEinkommenComp.geldComp,
                aHabitalwerteComp,
                aProduktivitaetComp, start_nl);
        this.herstellerDepartmentComp = herstellerDepartmentComp;
    }

    @Override
    protected void auftragErledigt_or() {
        zGegenstaendeComp.gegenstandListe.gettDurchId(assignment.id)
                .mengeErhoehen(assignment.entstehendeMenge, false);

        herstellerDepartmentComp.resetWorkingHours();
    }

    // PROPERTY CHANGE ////////////////////////////////////////////////////////////////////////////

    @Override
    public boolean propertyChange_or_or(PropertyChangeEvent propertyChangeEvent) {
        if (propertyChangeEvent.getPropertyName().equals(Enums.PROBERTY_CHANGE_PRODUCT)) {
            return true;
        }
        return super.propertyChange_or_or(propertyChangeEvent);
    }

    @Override
    protected void addPropertyChangeListeners() {
        herstellerDepartmentComp.addPropertyChangeListener(this);
        super.addPropertyChangeListeners();
    }

    @Override
    protected void removePropertyChangeListeners() {
        herstellerDepartmentComp.removePropertyChangeListener(this);
        super.removePropertyChangeListeners();
    }
}
