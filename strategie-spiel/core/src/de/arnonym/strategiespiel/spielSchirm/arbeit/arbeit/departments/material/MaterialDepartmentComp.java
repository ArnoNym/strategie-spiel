package de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.departments.material;

import de.arnonym.strategiespiel.framework.ecs.component.Comp;
import de.arnonym.strategiespiel.framework.werkzeuge.collections.Collector;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.EmployerComp;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.ProgressDepartmentComp;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.VarEmployerComp;
import de.arnonym.strategiespiel.spielSchirm.arbeit.auftrag.AssignmentMaterial;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.gegenstaende.GegenstaendeComp;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.spielSchirm.info.muster.Info;

public abstract class MaterialDepartmentComp<ProductInfo extends Info, A extends AssignmentMaterial> extends ProgressDepartmentComp<ProductInfo, A> {
    public final GegenstaendeComp gegenstaendeComp;

    public MaterialDepartmentComp(Entity entity, Collector<Comp> comps,
                                  VarEmployerComp<? extends ProductInfo> employerComp,
                                  GegenstaendeComp gegenstaendeComp) {
        super(entity, comps.add(gegenstaendeComp), employerComp);
        this.gegenstaendeComp = gegenstaendeComp;
    }
}
