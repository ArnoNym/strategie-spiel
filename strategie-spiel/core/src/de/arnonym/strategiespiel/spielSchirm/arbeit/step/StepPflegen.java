package de.arnonym.strategiespiel.spielSchirm.arbeit.step;

import com.badlogic.gdx.math.Vector2;

import de.arnonym.strategiespiel.framework.listener.StoryTeller;
import de.arnonym.strategiespiel.framework.werkzeuge.collections.Collector;
import de.arnonym.strategiespiel.spielSchirm.arbeit.auftrag.Assignment;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.bewegung.BewegungComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.EinkommenComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.GeldComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.werte.HabitalwerteComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.werte.ProduktivitaetComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.ressourcen.PflegenComp;

public class StepPflegen extends StepArbeit<Assignment> {
    public final PflegenComp pflegenComp;

    public StepPflegen(PflegenComp zPflegenComp,

                       GeldComp gGeldComp, int salary,

                       BewegungComp bewegungComp, GeldComp aGeldComp,
                       HabitalwerteComp habitalwerteComp, ProduktivitaetComp produktivitaetComp,

                       Vector2 start_nl) {
        super(new Collector<>(zPflegenComp), zPflegenComp.bewegungszielComp, gGeldComp, salary,
                zPflegenComp.getAssignment(), bewegungComp, aGeldComp, habitalwerteComp,
                produktivitaetComp, start_nl);
        this.pflegenComp = zPflegenComp;
    }

    @Override
    public void erledigt_or() {
        pflegenComp.gepflegt(aProduktivitaetComp);
        super.erledigt_or();
    }

    /*private PauseDelta pflegenPause;
    private Beruf dbBeruf;

    public StepPflegen(Dorfbewohner dorfbewohner, Zelle start_nl, RessourceWachsen ziel) {
        super(dorfbewohner, start_nl, ziel);
        this.dbBeruf = dorfbewohner.getBeruf();
    }

    @Override
    protected Enums.EnumStepStatus versuchen_or_or() {
        return pruefenObBeenden();
    }

    @Override
    public boolean pruefenObErledigt() {
        return false;
    }

    @Override
    public boolean pruefenObFail() {
        if (getDorfbewohner() != dbBeruf.getDorfbewohner()) {
            return true;
        }
        return false;
    }

    @Override
    protected void anfangen_or() {
        pflegenPause = new PauseDelta(3, Enums.EnumPauseStart.JETZT); //TODO Konstanten
    }

    @Override
    protected void durchfuehren_or(float delta) {
        if (!pflegenPause.issVorbei(delta, false)) {
            return;
        }
        ((RessourceWachsen)getZiel()).gepflegt(pflegenPause.gettZeitSeitStartzeit(),
                dbBeruf.gettProduktivitaet());
        setStatus(Enums.EnumStepStatus.ERLEDIGT);
    }

    @Override
    protected void failNachVersuchenVorAnfangen_or() {

    }

    @Override
    protected void failNachAnfangenVorDurchfuehren_or() {

    }

    @Override
    protected void failNachDurchfuehren_or() {

    }

    @Override
    public void erledigt_or() {

    }*/
}
