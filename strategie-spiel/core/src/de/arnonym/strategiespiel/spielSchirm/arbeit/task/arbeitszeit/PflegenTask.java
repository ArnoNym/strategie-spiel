package de.arnonym.strategiespiel.spielSchirm.arbeit.task.arbeitszeit;

import java.util.Collection;

import de.arnonym.strategiespiel.framework.stp.Step;
import de.arnonym.strategiespiel.framework.stp.StpComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.bewegung.BewegungComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.EinkommenComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.GeldComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.werte.HabitalwerteComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.werte.ProduktivitaetComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.ressourcen.PflegenComp;
import de.arnonym.strategiespiel.framework.stp.stacks.StepStack;
import de.arnonym.strategiespiel.spielSchirm.arbeit.step.StepPflegen;
import de.arnonym.strategiespiel.spielSchirm.Enums;

public class PflegenTask extends ArbeitszeitTask {
    public final Collection<PflegenComp> pflegenComps;
    public final BewegungComp bewegungComp;
    public final HabitalwerteComp habitalwerteComp;
    public final ProduktivitaetComp produktivitaetComp;

    public PflegenTask(Collection<PflegenComp> pflegenComps,

                       GeldComp gGeldComp, int salary,

                       GeldComp aGeldComp, StpComp stpComp, BewegungComp bewegungComp,
                       HabitalwerteComp habitalwerteComp,
                       ProduktivitaetComp produktivitaetComp) {
        super(gGeldComp, salary, aGeldComp, stpComp, bewegungComp, habitalwerteComp,
                produktivitaetComp);
        this.pflegenComps = pflegenComps;
        this.bewegungComp = bewegungComp;
        this.habitalwerteComp = habitalwerteComp;
        this.produktivitaetComp = produktivitaetComp;
    }

    @Override
    public void planeAlleSteps(StepStack stepStack) {
        for (PflegenComp pflegenComp : pflegenComps) {
            StepPflegen stepPflegen = new StepPflegen(pflegenComp, gGeldComp, salary, bewegungComp,
                    aGeldComp, habitalwerteComp, produktivitaetComp, null);
            if (stepStack.handle(stepPflegen, true) == Enums.EnumStpStatus.OK) {
                return;
            }
        }
    }

    @Override
    public void planeNaechstenStep(StepStack stepStack) {

    }

    @Override
    protected void stepBeendet(Step step) {
        if (step instanceof StepPflegen && step.isStatus(Enums.EnumStpStatus.OK)) {
            pflegenComps.remove(((StepPflegen) step).pflegenComp);
        }
        super.stepBeendet(step);
    }
}
