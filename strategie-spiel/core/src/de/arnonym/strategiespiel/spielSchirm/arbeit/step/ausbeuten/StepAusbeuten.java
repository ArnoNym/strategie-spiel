package de.arnonym.strategiespiel.spielSchirm.arbeit.step.ausbeuten;

import com.badlogic.gdx.math.Vector2;

import java.beans.PropertyChangeEvent;
import java.util.List;

import de.arnonym.strategiespiel.framework.ecs.component.Comp;
import de.arnonym.strategiespiel.framework.ecs.component.Component;
import de.arnonym.strategiespiel.framework.werkzeuge.collections.Collector;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.MathUtils;
import de.arnonym.strategiespiel.spielSchirm.arbeit.auftrag.Assignment;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.ausbeuten.AusbeutbarComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.bewegung.BewegungComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.gegenstaende.GegenstaendeComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.EinkommenComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.GeldComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.herrschaft.EigentuemerComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.werte.HabitalwerteComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.werte.ProduktivitaetComp;
import de.arnonym.strategiespiel.spielSchirm.arbeit.step.StepArbeit;
import de.arnonym.strategiespiel.spielSchirm.worldMap.Zelle;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.undPreis.IntEintragHashMap;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.endstufe.Gegenstand;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.endstufe.GegenstandList;
import de.arnonym.strategiespiel.spielSchirm.Enums;
import de.arnonym.strategiespiel.spielSchirm.balancing.Konstanten;

public abstract class StepAusbeuten extends StepArbeit<Assignment> {
    private final GegenstandList zGegList;
    private final GegenstandList aGegList;

    private final IntEintragHashMap genommenHashMap = new IntEintragHashMap();

    public final EigentuemerComp eigentuemerComp;
    private Gegenstand zGegenstand;
    private Gegenstand aGegenstand;
    private int menge;

    public StepAusbeuten(Collector<Comp> comps,

                         EigentuemerComp neuerEigentuemerComp,

                         AusbeutbarComp zAusbeutbarComp,

                         GeldComp gGeldComp, int salary,

                         BewegungComp aBewegungComp, GeldComp aGeldComp,
                         HabitalwerteComp aHabitalwerteComp, ProduktivitaetComp aProduktivitaetComp,
                         GegenstaendeComp aGegenstaendeComp,

                         Vector2 start_nl) {
        super(comps.add(neuerEigentuemerComp, zAusbeutbarComp, aGegenstaendeComp),
                zAusbeutbarComp.bewegungszielComp, gGeldComp, salary, zAusbeutbarComp.getAssignment(),
                aBewegungComp, aGeldComp, aHabitalwerteComp, aProduktivitaetComp, start_nl);
        this.eigentuemerComp = neuerEigentuemerComp;
        this.zGegList = zAusbeutbarComp.gegenstaendeComp.gegenstandListe;
        this.aGegList = aGegenstaendeComp.gegenstandListe;
    }

    @Override
    protected Enums.EnumStpStatus versuchen_or_or_or() {
        zGegenstand = gettAusbeutenGegenstand(aGegList, zGegList);

        if (zGegenstand == null) {
            return Enums.EnumStpStatus.FAIL;
        }

        aGegenstand = aGegList.gettDurchId(zGegenstand.gegMuster.id);

        return Enums.EnumStpStatus.OK;
    }

    protected abstract Gegenstand gettAusbeutenGegenstand(GegenstandList aGegList,
                                                          GegenstandList zGegList);

    protected final boolean pruefeGegenstand(Gegenstand gegenstand) {
        return aGegList.issPlatzFuerEtwas(gegenstand) && gegenstand.issMenge(true, false, false);
    }

    @Override
    protected void reservieren(float arbeitszeit) {
        super.reservieren(arbeitszeit);
        menge = MathUtils.min(aGegenstand.getMengeBisVoll(), zGegenstand.getMengeVerfuegbar(),
                Konstanten.NACHKOMMASTELLEN);
        aGegenstand.reservierenLegen(menge);
        zGegenstand.reservierenNehmen(menge);
    }

    @Override
    protected void reservierenRueckgaengig_or() {
        super.reservierenRueckgaengig();
        aGegenstand.reservierenLegenRueckgaengig(menge);
        zGegenstand.reservierenNehmenRueckgaengig(menge);
    }

    @Override
    public void erledigt_or() {
        aGegenstand.mengeErhoehen(menge, true);
        zGegenstand.mengeVerringern(menge, true);
        genommenHashMap.addUpEntry(zGegenstand.gettId(), menge, getSummeGehalt());
        super.erledigt_or();
    }

    @Override
    public boolean propertyChange_or_or(PropertyChangeEvent propertyChangeEvent) {
        if (propertyChangeEvent.getPropertyName().equals(Enums.PROBERTY_CHANGE_ZELLE)) {
            Zelle cell = (Zelle) propertyChangeEvent.getOldValue();
            List<Zelle> cells = zBewegungszielComp.compKollisionLaufen.getBetrifftZellenListe_nl();
            if (cells == null) {
                throw new IllegalStateException();
            }
            if (cells.contains(cell)) {
                setStatus(Enums.EnumStpStatus.FAIL);
                return true;
            }
            return false;
        }
        return super.propertyChange_or_or(propertyChangeEvent);
    }

    // EINFACHE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public IntEintragHashMap getGenommenHashMap() {
        return genommenHashMap;
    }
}

