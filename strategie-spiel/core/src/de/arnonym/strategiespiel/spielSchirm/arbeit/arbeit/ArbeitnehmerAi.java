package de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit;

import de.arnonym.strategiespiel.framework.stp.Ai;
import de.arnonym.strategiespiel.framework.ecs.component.Comp;
import de.arnonym.strategiespiel.framework.stp.StpComp;
import de.arnonym.strategiespiel.framework.stp.Task;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.bewegung.BewegungComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.EinkommenComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.werte.HabitalwerteComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.rendern.RenderComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.gegenstaende.GegenstaendeComp;
import de.arnonym.strategiespiel.spielSchirm.bodenStapel.BodenStapelFactory;
import de.arnonym.strategiespiel.spielSchirm.ui.aufsteigendeSymbole.AufsteigendesSymbolFactory;
import de.arnonym.strategiespiel.spielSchirm.umwelt.ressourcen.RessourcenFactory;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.listSystems.privateListSystems.SysLager;
import de.arnonym.strategiespiel.framework.ecs.collections.Matcher;
import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenAiPrioritaet;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.werte.ProduktivitaetComp;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;

public class ArbeitnehmerAi extends Ai {
    public final AufsteigendesSymbolFactory aufsteigendesSymbolFactory;
    public final BodenStapelFactory bodenStapelFactory;
    public final RessourcenFactory ressourcenFactory;
    public final SysLager sysLager;
    public final EmploymentMatcher employmentMatcher;
    public final Entity entity;
    public final BewegungComp bewegungComp;
    public final EinkommenComp einkommenComp;
    public final GegenstaendeComp gegenstaendeComp;
    public final HabitalwerteComp habitalwerteComp;
    public final ProduktivitaetComp produktivitaetComp;

    private EmployerComp employer;
    private int gehalt;

    public ArbeitnehmerAi(AufsteigendesSymbolFactory aufsteigendesSymbolFactory,
                          BodenStapelFactory bodenStapelFactory,
                          RessourcenFactory ressourcenFactory,

                          Entity entity,

                          SysLager sysLager, EmploymentMatcher employmentMatcher,

                          RenderComp renderComp, StpComp stpComp,
                          BewegungComp bewegungComp, GegenstaendeComp gegenstaendeComp,
                          HabitalwerteComp habitalwerteComp,
                          EinkommenComp einkommenComp, ProduktivitaetComp produktivitaetComp) {
        super(new Matcher[]{employmentMatcher}, entity, stpComp, renderComp,
                new Comp[]{bewegungComp, einkommenComp, produktivitaetComp},
                KonstantenAiPrioritaet.ARBEITNEHMER);
        this.entity = entity;
        this.aufsteigendesSymbolFactory = aufsteigendesSymbolFactory;
        this.bodenStapelFactory = bodenStapelFactory;
        this.ressourcenFactory = ressourcenFactory;
        this.sysLager = sysLager;
        this.employmentMatcher = employmentMatcher;
        this.bewegungComp = bewegungComp;
        this.gegenstaendeComp = gegenstaendeComp;
        this.habitalwerteComp = habitalwerteComp;
        this.einkommenComp = einkommenComp;
        this.produktivitaetComp = produktivitaetComp;
    }

    @Override
    public Task versuchen() {
        if (employer == null) {
            return null;
        }
        return employer.createTask(this);
    }

    // GEHALT /////////////////////////////////////////////////////////////////////////////////////

    public int getGehalt() {
        if (!isEmployed()) {
            throw new IllegalStateException();
        }
        return gehalt;
    }

    // ARBEITGEBER ////////////////////////////////////////////////////////////////////////////////

    public void setEmployer(EmployerComp employer, int gehalt) {
        if (employer == null) {
            if (getStatus()) {
                beenden();
            }
            return;
        }
        this.employer = employer;
        this.gehalt = gehalt;
    }

    public EmployerComp getEmployer() {
        return employer;
    }

    public boolean isEmployed() {
        return employer != null;
    }

    public boolean currentlyWorkingForEmployer(EmployerComp<?> employerComp) {
        if (this.employer.equals(employerComp)) {
            return stpComp.getCurrentAi() instanceof ArbeitnehmerAi;
        }
        return false;
    }

    public boolean currentlyWorkingForDepartment(DepartmentComp departmentComp) {
        return false; // todo
    }
}