package de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.departments.gebiet;

import de.arnonym.strategiespiel.framework.werkzeuge.collections.Collector;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.CellsComp;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.CellsDepartmentComp;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.ArbeitnehmerAi;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.VarEmployerComp;
import de.arnonym.strategiespiel.framework.stp.Task;
import de.arnonym.strategiespiel.spielSchirm.arbeit.task.arbeitszeit.PflegenTask;
import de.arnonym.strategiespiel.spielSchirm.umwelt.ressourcen.PflegenComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.ressourcen.stufen.WachstumRessourceInfo;

public class PflegenDepartmentComp extends CellsDepartmentComp<WachstumRessourceInfo, RessourcenAssignment> {

    public PflegenDepartmentComp(Entity entity, CellsComp cellsComp,
                                 VarEmployerComp<WachstumRessourceInfo> employerComp) {
        super(new Collector<>(), entity, new Collector<>(), cellsComp, employerComp);
    }

    @Override
    public Task createTask_nl(ArbeitnehmerAi arbeitnehmerAi) {
        return new PflegenTask(cellsComp.comps(PflegenComp.class),

                employerComp.eigentuemerComp.geldComp, arbeitnehmerAi.getGehalt(),

                arbeitnehmerAi.einkommenComp.geldComp, arbeitnehmerAi.stpComp,
                arbeitnehmerAi.bewegungComp, arbeitnehmerAi.habitalwerteComp,
                arbeitnehmerAi.produktivitaetComp);
    }

    @Override
    protected RessourcenAssignment getAssignment(WachstumRessourceInfo wachstumRessourceInfo) {
        return wachstumRessourceInfo.pflegeAssignment;
    }
}
