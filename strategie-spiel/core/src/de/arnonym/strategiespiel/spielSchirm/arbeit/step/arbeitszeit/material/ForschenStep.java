package de.arnonym.strategiespiel.spielSchirm.arbeit.step.arbeitszeit.material;

import com.badlogic.gdx.math.Vector2;

import de.arnonym.strategiespiel.framework.ecs.component.Comp;
import de.arnonym.strategiespiel.framework.werkzeuge.collections.Collector;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.departments.bildung.ForschungDepartmentComp;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.departments.bildung.ForschungAssignment;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.bewegung.BewegungComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.EinkommenComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.GeldComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.werte.HabitalwerteComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.werte.ProduktivitaetComp;

public class ForschenStep extends StepMaterial<ForschungAssignment> {
    public final ForschungDepartmentComp forschungArbeitgeberComp;

    public ForschenStep(ForschungDepartmentComp forschungArbeitgeberComp,

                        BewegungComp aBewegungComp, EinkommenComp aEinkommenComp,
                        HabitalwerteComp aHabitalwerteComp, ProduktivitaetComp aProduktivitaetComp,

                        Vector2 start_nl) {
        super(new Collector<>(forschungArbeitgeberComp), forschungArbeitgeberComp.timeClock,
                forschungArbeitgeberComp.employerComp.bewegungszielComp,
                forschungArbeitgeberComp.employerComp.eigentuemerComp,
                forschungArbeitgeberComp.gegenstaendeComp, forschungArbeitgeberComp.getAssignment_nl(),
                forschungArbeitgeberComp.employerComp.eigentuemerComp.geldComp,
                aEinkommenComp.getEinkommen(),aBewegungComp, aEinkommenComp.geldComp,
                aHabitalwerteComp, aProduktivitaetComp, start_nl);
        this.forschungArbeitgeberComp = forschungArbeitgeberComp;
    }

    @Override
    protected void auftragErledigt_or() {
        forschungArbeitgeberComp.forschungsstand().setErforscht(true);
        //forschungArbeitgeberComp.setAssignment(null);
    }
}
