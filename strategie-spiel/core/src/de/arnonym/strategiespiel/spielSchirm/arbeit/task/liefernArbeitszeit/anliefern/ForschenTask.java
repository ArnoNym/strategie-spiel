package de.arnonym.strategiespiel.spielSchirm.arbeit.task.liefernArbeitszeit.anliefern;

import de.arnonym.strategiespiel.framework.stp.StpComp;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.departments.bildung.ForschungDepartmentComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.bewegung.BewegungComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.gegenstaende.GegenstaendeComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.EinkommenComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.werte.HabitalwerteComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.werte.ProduktivitaetComp;
import de.arnonym.strategiespiel.spielSchirm.ui.aufsteigendeSymbole.AufsteigendesSymbolFactory;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.listSystems.privateListSystems.SysLager;
import de.arnonym.strategiespiel.framework.stp.stacks.StepStack;
import de.arnonym.strategiespiel.spielSchirm.arbeit.step.arbeitszeit.material.ForschenStep;

public class ForschenTask extends AnliefernTask {
    public final ForschungDepartmentComp gForschungArbeitgeberComp;

    public ForschenTask(AufsteigendesSymbolFactory aufsteigendesSymbolFactory,

                        SysLager sysLager,

                        ForschungDepartmentComp gForschungArbeitgeberComp,

                        StpComp aStpComp, BewegungComp aBewegungComp,
                        EinkommenComp aEinkommenComp, GegenstaendeComp aGegenstaendeComp,
                        HabitalwerteComp aHabitalwerteComp,
                        ProduktivitaetComp aProduktivitaetComp) {
        super(aufsteigendesSymbolFactory, sysLager,
                gForschungArbeitgeberComp.employerComp.eigentuemerComp.geldComp, aStpComp,
                aBewegungComp, aEinkommenComp, aGegenstaendeComp, aHabitalwerteComp,
                aProduktivitaetComp, gForschungArbeitgeberComp);
        this.gForschungArbeitgeberComp = gForschungArbeitgeberComp;
    }

    @Override
    public void planeAlleSteps(StepStack stepStack) {
        ForschenStep forschenStep = new ForschenStep(gForschungArbeitgeberComp, aBewegungComp,
                aGeldComp, aHabitalwerteComp, aProduktivitaetComp, null);
        handleStepMaterial(stepStack, forschenStep);
    }

    @Override
    public void planeNaechstenStep(StepStack stepStack) {

    }
}
