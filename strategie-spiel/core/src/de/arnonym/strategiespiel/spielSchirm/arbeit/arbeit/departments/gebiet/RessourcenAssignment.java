package de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.departments.gebiet;

import de.arnonym.strategiespiel.spielSchirm.arbeit.auftrag.Assignment;
import de.arnonym.strategiespiel.spielSchirm.zahlen.schaden.MusterSchaden;

public class RessourcenAssignment extends Assignment {
    public final int id;

    public RessourcenAssignment(int id,
                                float optimaleProduktivitaetKopf,
                                float optimaleProduktivitaetKoerper,
                                float arbeitszeit) {
        super(optimaleProduktivitaetKopf, optimaleProduktivitaetKoerper, arbeitszeit);
        this.id = id;
    }

    public RessourcenAssignment(int id, float optimaleProduktivitaetKopf,
                                float optimaleProduktivitaetKoerper,
                                float arbeitszeit,
                                MusterSchaden musterSchaden) {
        super(optimaleProduktivitaetKopf, optimaleProduktivitaetKoerper, arbeitszeit, musterSchaden);
        this.id = id;
    }
}
