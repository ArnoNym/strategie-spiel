package de.arnonym.strategiespiel.spielSchirm.arbeit.task.arbeitszeit;

import java.util.Collection;

import de.arnonym.strategiespiel.framework.stp.Step;
import de.arnonym.strategiespiel.framework.stp.StpComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.bewegung.BewegungComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.EinkommenComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.GeldComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.werte.HabitalwerteComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.werte.ProduktivitaetComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.ressourcen.RessourcenFactory;
import de.arnonym.strategiespiel.framework.stp.stacks.StepStack;
import de.arnonym.strategiespiel.spielSchirm.StepNGZ;
import de.arnonym.strategiespiel.spielSchirm.arbeit.step.StepAnpflanzen;
import de.arnonym.strategiespiel.spielSchirm.worldMap.Zelle;
import de.arnonym.strategiespiel.spielSchirm.Enums;

public class AnpflanzenTask extends ArbeitszeitTask {
    public final RessourcenFactory ressourcenFactory;
    private final Collection<Zelle> cells;
    public final BewegungComp aBewegungComp;
    public final HabitalwerteComp aHabitalwerteComp;
    public final ProduktivitaetComp aProduktivitaetComp;
    private final int anpflanzenRessourceId;

    public AnpflanzenTask(RessourcenFactory ressourcenFactory,

                          Collection<Zelle> cells,

                          GeldComp gGeldComp, int salary,

                          BewegungComp aBewegungComp, GeldComp aGeldComp,
                          HabitalwerteComp aHabitalwerteComp,
                          ProduktivitaetComp aProduktivitaetComp, StpComp aStpComp,

                          int anpflanzenRessourceId) {
        super(gGeldComp, salary, aGeldComp, aStpComp, aBewegungComp, aHabitalwerteComp,
                aProduktivitaetComp);
        this.ressourcenFactory = ressourcenFactory;
        this.cells = cells;
        this.aBewegungComp = aBewegungComp;
        this.aHabitalwerteComp = aHabitalwerteComp;
        this.aProduktivitaetComp = aProduktivitaetComp;
        this.anpflanzenRessourceId = anpflanzenRessourceId;
    }

    @Override
    public void planeAlleSteps(StepStack stepStack) {
        //todo pflanzen auf fruchtbarstem Boden beguenstigen? Die Pflege sollte auf alle den gleichne Einfluss haben. Beim Anpflanzen die fruchtbareren beguenstigen;

        for (Zelle z : cells) {
            StepNGZ step = new StepAnpflanzen(ressourcenFactory, gGeldComp, salary, aBewegungComp,
                    aGeldComp, aHabitalwerteComp, aProduktivitaetComp,
                    z, anpflanzenRessourceId, null);
            if (stepStack.handle(step, true) == Enums.EnumStpStatus.OK) {
                return;
            }
        }
    }

    @Override
    public void planeNaechstenStep(StepStack stepStack) {

    }

    @Override
    protected void stepBeendet(Step step) {
        if (step instanceof StepAnpflanzen && step.isStatus(Enums.EnumStpStatus.OK)) {
            cells.remove(((StepAnpflanzen) step).zelle);
        }
        super.stepBeendet(step);
    }
}
