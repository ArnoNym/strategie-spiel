package de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import de.arnonym.strategiespiel.framework.ecs.collections.System;
import de.arnonym.strategiespiel.framework.ecs.component.Comp;
import de.arnonym.strategiespiel.framework.werkzeuge.collections.Collector;
import de.arnonym.strategiespiel.spielSchirm.info.muster.Info;
import de.arnonym.strategiespiel.spielSchirm.arbeit.auftrag.Assignment;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.spielSchirm.Enums;

public abstract class VarDepartmentComp<ProductInfo extends Info, A extends Assignment>
        extends DepartmentComp<VarEmployerComp<? extends ProductInfo>, A>
        implements PropertyChangeListener {


    public VarDepartmentComp(Collector<System> systems,
                             Entity entity,
                             VarEmployerComp<? extends ProductInfo> employerComp,
                             Collector<Comp> comps) {
        super(systems, entity, comps, employerComp);
    }

    // ASSIGNMENTS ////////////////////////////////////////////////////////////////////////////////

    @Override
    public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
        if (propertyChangeEvent.getPropertyName().equals(Enums.PROBERTY_CHANGE_PRODUCT)) {
            support.firePropertyChange(propertyChangeEvent);
            newProduct((ProductInfo) propertyChangeEvent.getNewValue());
        }
    }

    protected void newProduct(ProductInfo productInfo) {

    }

    @Override
    public final A getAssignment_nl() {
        return getAssignment(employerComp.getProduct());
    }

    protected abstract A getAssignment(ProductInfo productInfo);

    public boolean hasAssignment() {
        return getAssignment_nl() != null;
    }
}
