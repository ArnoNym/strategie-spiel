package de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Set;

import de.arnonym.strategiespiel.spielSchirm.Enums;
import de.arnonym.strategiespiel.spielSchirm.arbeit.auftrag.Assignment;

class PlaceholderAssignment implements PropertyChangeListener {
    private final Set<VarDepartmentComp> varDepartmentComps;

    private float prodKopf = 0;
    private float prodKoerper = 0;
    private int size = 0; // Seperat, da nicht immer alle Departments Assignments haben

    PlaceholderAssignment(Set<VarDepartmentComp> varDepartmentComps) {
        this.varDepartmentComps = varDepartmentComps;
    }

    @Override
    public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
        if (propertyChangeEvent.getPropertyName().equals(Enums.PROBERTY_CHANGE_DEPARTMENT)) {
            VarDepartmentComp varDepartmentComp = (VarDepartmentComp) propertyChangeEvent.getNewValue();
            Assignment assignment = varDepartmentComp.getAssignment_nl();

            if (assignment == null) {
                return;
            }

            float sumProdKopf = prodKopf * size + assignment.optimaleProduktivitaetKopf;
            float sumProdKoerper = prodKoerper * size + assignment.optimaleProduktivitaetKoerper;

            size++;
            prodKopf = sumProdKopf / size;
            prodKoerper = sumProdKoerper / size;

            return;
        }

        if (propertyChangeEvent.getPropertyName().equals(Enums.PROBERTY_CHANGE_PRODUCT)) {
            float sumProdKopf = 0;
            float sumProdKoerper = 0;
            size = 0;

            for (VarDepartmentComp varDepartmentComp : varDepartmentComps) {
                Assignment assignment = varDepartmentComp.getAssignment_nl();
                if (assignment != null) {
                    sumProdKopf += assignment.optimaleProduktivitaetKopf;
                    sumProdKoerper += assignment.optimaleProduktivitaetKoerper;
                    size++;
                }
            }

            if (size == 0) {
                return;
            }

            prodKopf = sumProdKopf / size;
            prodKoerper = sumProdKoerper / size;

            return;
        }
    }

    public float getProdKopf() {
        return prodKopf;
    }

    public float getProdKoerper() {
        return prodKoerper;
    }

    public int getSize() {
        return size;
    }
}
