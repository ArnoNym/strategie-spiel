package de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import de.arnonym.strategiespiel.framework.ecs.collections.System;
import de.arnonym.strategiespiel.framework.ecs.component.Comp;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.framework.listener.StoryTeller;
import de.arnonym.strategiespiel.framework.stp.Task;
import de.arnonym.strategiespiel.framework.werkzeuge.collections.Collector;
import de.arnonym.strategiespiel.spielSchirm.Enums;
import de.arnonym.strategiespiel.spielSchirm.arbeit.auftrag.Assignment;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.bewegung.BewegungszielComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.herrschaft.EigentuemerComp;
import de.arnonym.strategiespiel.spielSchirm.info.muster.Info;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.werte.ProduktivitaetComp;

public class EmployerComp<ProductInfo extends Info> extends Comp {
    public final BewegungszielComp bewegungszielComp;
    public final EigentuemerComp eigentuemerComp;

    private final Set<DepartmentComp> departmentComps = new HashSet<>();

    protected PlaceholderAssignment placeholderAssignment;

    public final EmploymentMatcher employmentMatcher;
    public final int maxStellen;
    private final Set<ArbeitnehmerAi> arbeitnehmers = new HashSet<>();

    private int gehalt_sekunde = -1; /* ... pro Sekunde! Arbeitnehmer koennen sich die Arbeit suchen fuer die
    sie am meisten Geld pro Sekunde bekommen = am wenigsten Zeit benoetigen = am Produktivsten
    sind */

    protected EmployerComp(EmploymentMatcher employmentMatcher,

                           Entity entity,

                           BewegungszielComp bewegungszielComp, EigentuemerComp eigentuemerComp,

                           int maxStellen) {
        super(new Collector<System>(employmentMatcher).toArray(new System[]{}), entity,
                new Collector<>(bewegungszielComp, eigentuemerComp).toArray(new Comp[]{}));
        this.employmentMatcher = employmentMatcher;
        this.bewegungszielComp = bewegungszielComp;
        this.eigentuemerComp = eigentuemerComp;
        this.maxStellen = maxStellen;
    }

    public Task createTask(ArbeitnehmerAi arbeitnehmerAi) {
        for (DepartmentComp departmentComp : departmentComps) {
            Task task = departmentComp.createTask_nl(arbeitnehmerAi);
            if (task != null) {
                return task;
            }
        }
        return null;
    }

    public void abortAi() {
        for (ArbeitnehmerAi arbeitnehmerAi : arbeitnehmers) {
            arbeitnehmerAi.beenden();
        }
    }

    // DEPARTMENTS ////////////////////////////////////////////////////////////////////////////////

    protected EmployerComp<ProductInfo> addDepartment(DepartmentComp departmentComp) {
        support.firePropertyChange(Enums.PROBERTY_CHANGE_DEPARTMENT, null, departmentComp);
        departmentComps.add(departmentComp);
        super.additionalConstruct(departmentComp);
        return this;
    }

    @Override
    protected void additionalConstruct(StoryTeller... storyTellers) {
        List<StoryTeller> storyTellerList = new LinkedList<>();
        for (StoryTeller storyTeller : storyTellers) {
            if (storyTeller instanceof DepartmentComp) {
                addDepartment((DepartmentComp) storyTeller);
            } else {
                storyTellerList.add(storyTeller);
            }
        }
        super.additionalConstruct(storyTellerList.toArray(new StoryTeller[]{}));
    }

    protected Set<DepartmentComp> departmentComps() {
        return new HashSet<>(departmentComps);
    }

    // ANGESTELLTE ////////////////////////////////////////////////////////////////////////////////

    public void addArbeitnehmer(ArbeitnehmerAi arbeitnehmerAi) {
        arbeitnehmers.add(arbeitnehmerAi);
    }

    public void removeArbeitnehmer(ArbeitnehmerAi arbeitnehmerAi) {
        arbeitnehmers.remove(arbeitnehmerAi);
    }

    public int freieStellen() {
        return maxStellen - arbeitnehmers.size();
    }

    public boolean hasFreieStellen() {
        return freieStellen() == 0;
    }

    public int getMaxStellen() {
        return maxStellen;
    }

    public int getAnzahlAngestellte() {
        return arbeitnehmers.size();
    }

    public ArbeitnehmerAi[] arbeitnehmerArray() {
        return arbeitnehmers.toArray(new ArbeitnehmerAi[arbeitnehmers.size()]);
    }

    public <COLL extends Collection<ArbeitnehmerAi>> COLL arbeitnehmers(COLL coll) {
        coll.addAll(arbeitnehmers);
        return coll;
    }

    // GEHALT /////////////////////////////////////////////////////////////////////////////////////

    private void calculateGehalt() {
        float sumProdKopf = 0;
        float sumProdKoerper = 0;
        int count = 0;
        for (DepartmentComp departmentComp : departmentComps) {
            Assignment assignment_nl = departmentComp.getAssignment_nl();
            if (assignment_nl != null) {
                sumProdKopf += assignment_nl.optimaleProduktivitaetKopf;
                sumProdKoerper += assignment_nl.optimaleProduktivitaetKoerper;
                count++;
            }
        }
        float avgProdKopf = sumProdKopf / count;
        float avgProdKoerper = sumProdKoerper / count;

        this.gehalt_sekunde = Math.max(1, employmentMatcher.schaetzeGehalt(avgProdKopf,
                avgProdKoerper, bewegungszielComp.compKollisionLaufen.positionComp.position));
    }

    public int getGehalt_sekunde() {
        return gehalt_sekunde;
    }

    public int getMaxGehalt() {
        throw new IllegalStateException(); // todo
    }

    public int realGehalt_sekunde(ProduktivitaetComp produktivitaetComp) {
        return realGehalt_sekunde(produktivitaetComp.realProduktivitaet(
                placeholderAssignment.getProdKopf(), placeholderAssignment.getProdKoerper()));
    }

    public int realGehalt_sekunde(float realProduktivitaet) {
        return (int) (gehalt_sekunde / realProduktivitaet);
    }
}
