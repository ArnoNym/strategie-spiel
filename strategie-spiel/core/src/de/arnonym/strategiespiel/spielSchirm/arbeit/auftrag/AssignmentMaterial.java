package de.arnonym.strategiespiel.spielSchirm.arbeit.auftrag;

import de.arnonym.strategiespiel.spielSchirm.zahlen.schaden.MusterSchaden;
import de.arnonym.strategiespiel.framework.werkzeuge.collections.hashMaps.IntIntHashMap;

public class AssignmentMaterial extends Assignment {
    public final int id;
    public final IntIntHashMap idMengeHashMap;

    public AssignmentMaterial(int id, float optimaleProduktivitaetKopf,
                              float optimaleProduktivitaetKoerper, float arbeitszeit,
                              int[][] idMengenArray) {
        super(optimaleProduktivitaetKopf, optimaleProduktivitaetKoerper, arbeitszeit);
        this.id = id;
        this.idMengeHashMap = new IntIntHashMap(idMengenArray);
    }

    public AssignmentMaterial(int id, float optimaleProduktivitaetKopf,
                              float optimaleProduktivitaetKoerper, float arbeitszeit,
                              MusterSchaden musterSchaden, int[][] idMengenArray) {
        super(optimaleProduktivitaetKopf, optimaleProduktivitaetKoerper, arbeitszeit, musterSchaden);
        this.id = id;
        this.idMengeHashMap = new IntIntHashMap(idMengenArray);
    }
}
