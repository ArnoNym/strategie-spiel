package de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.departments.bildung;

import java.util.ArrayList;
import java.util.List;

import de.arnonym.strategiespiel.framework.werkzeuge.collections.Collector;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.EmployerComp;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.DepartmentComp;
import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenBalance;
import de.arnonym.strategiespiel.spielSchirm.forschungUndWissen.schule.AiSchueler;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.ArbeitnehmerAi;
import de.arnonym.strategiespiel.spielSchirm.arbeit.auftrag.Assignment;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.framework.stp.Task;
import de.arnonym.strategiespiel.spielSchirm.forschungUndWissen.schule.TaskBerufLehrer;

public class TeachDepartmentComp extends DepartmentComp<EmployerComp<?>, Assignment> {
    public final int maxSchueler;
    public final BildungInfo bildenInfo; // Bilden Arbeitszeit = Arbeitszeit pro 1 Bildung
    public final List<AiSchueler> schuelerList = new ArrayList<>();

    private int vorbereitung = 0;

    public TeachDepartmentComp(Entity entity,

                               EmployerComp<?> employerComp,

                               int maxSchueler, BildungInfo bildenInfo) {
        super(new Collector<>(), entity, new Collector<>(), employerComp);
        this.maxSchueler = maxSchueler;
        this.bildenInfo = bildenInfo;
    }

    @Override
    public Task createTask_nl(ArbeitnehmerAi arbeitnehmerAi) {
        return new TaskBerufLehrer(this,

                arbeitnehmerAi.stpComp, arbeitnehmerAi.bewegungComp,
                arbeitnehmerAi.einkommenComp, arbeitnehmerAi.habitalwerteComp,
                arbeitnehmerAi.produktivitaetComp);
    }

    public int anzahlLehrerInGebaude() {
        List<Entity> containsEntitiesList = employerComp.bewegungszielComp.gettContainsEntitiesList();
        int count = 0;
        for (Entity e : containsEntitiesList) {
            ArbeitnehmerAi arbeitnehmerAi = e.gettComponent(ArbeitnehmerAi.class);
            if (arbeitnehmerAi != null && arbeitnehmerAi.currentlyWorkingForDepartment(this)) {
                count++;
            }
        }
        return count;
    }

    @Override
    public Assignment getAssignment_nl() {
        return bildenInfo.bildenAuftrag;
    }

    // VORBEREITUNG ///////////////////////////////////////////////////////////////////////////////

    public void addVorbereitung() {
        vorbereitung = Math.min(
                vorbereitung + KonstantenBalance.SCHULE_ANZAHL_VORBEREITUNGEN_PRO_LEHRER_ARBEIT,
                getVorbereitungMax());
    }

    public void removeVorbereitung() {
        if (vorbereitung == 0) throw new IllegalStateException();
        vorbereitung -= 1;
    }

    public int getVorbereitung() {
        return vorbereitung;
    }

    public int getVorbereitungMax() {
        return maxSchueler * KonstantenBalance.SCHULE_VORBEREITUNGEN_MAX_SPEICHERN_PRO_SCHUELER;
    }

    public boolean issMaxVorbereitung() {
        return vorbereitung == getVorbereitungMax();
    }
}
