package de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.departments.gebiet.farm;

import com.badlogic.gdx.math.Vector2;

import de.arnonym.strategiespiel.spielSchirm.Dorf;
import de.arnonym.strategiespiel.spielSchirm.World;
import de.arnonym.strategiespiel.spielSchirm.gebaude.GebaudeFactory;

public class FarmFactory extends GebaudeFactory<FarmInfo> {
    public FarmFactory(World world, Dorf dorf, FarmInfo info) {
        super(world, dorf, info);
    }

    @Override
    public Farm create(Vector2 position) {
        return new Farm(world, dorf, info, position);
    }
}
