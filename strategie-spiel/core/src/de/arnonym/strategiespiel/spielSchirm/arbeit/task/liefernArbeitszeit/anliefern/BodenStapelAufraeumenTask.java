package de.arnonym.strategiespiel.spielSchirm.arbeit.task.liefernArbeitszeit.anliefern;

import com.badlogic.gdx.math.Vector2;

import java.util.LinkedList;

import de.arnonym.strategiespiel.framework.stp.StpComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.bewegung.BewegungComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.gegenstaende.GegenstaendeComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.EinkommenComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.herrschaft.EigentuemerComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.lager.LagerComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.lager.LagerCompOhneGeld;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.werte.HabitalwerteComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.werte.ProduktivitaetComp;
import de.arnonym.strategiespiel.spielSchirm.bodenStapel.BodenStapelFactory;
import de.arnonym.strategiespiel.spielSchirm.ui.aufsteigendeSymbole.AufsteigendesSymbolFactory;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.listSystems.privateListSystems.SysLager;
import de.arnonym.strategiespiel.framework.stp.stacks.StepStack;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dorfbewohner.tauschen.plan.legen.PrivatLegenPlan;
import de.arnonym.strategiespiel.framework.stp.Step;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dorfbewohner.tauschen.step.StepNehmen;
import de.arnonym.strategiespiel.framework.stp.Task;
import de.arnonym.strategiespiel.spielSchirm.arbeit.task.liefernArbeitszeit.TaskLoswerden;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.endstufe.Gegenstand;
import de.arnonym.strategiespiel.framework.werkzeuge.collections.hashMaps.IntIntHashMap;
import de.arnonym.strategiespiel.spielSchirm.Enums;

public class BodenStapelAufraeumenTask extends AnliefernTask { // todo wieso task anliefern???
    public final BodenStapelFactory bodenStapelFactory;

    private EigentuemerComp eigentuemerComp = null; // Wessen gegenstaende transportiert werden
    private boolean transportiertFremdeMenge = false;

    public BodenStapelAufraeumenTask(AufsteigendesSymbolFactory aufsteigendesSymbolFactory,
                                     BodenStapelFactory bodenStapelFactory,

                                     SysLager sysLager,

                                     StpComp stpComp, BewegungComp bewegungComp,
                                     EinkommenComp einkommenComp, GegenstaendeComp gegenstaendeComp,
                                     HabitalwerteComp habitalwerteComp,
                                     ProduktivitaetComp produktivitaetComp) {
        super(aufsteigendesSymbolFactory, sysLager, null, stpComp, bewegungComp,
                einkommenComp, gegenstaendeComp, habitalwerteComp, produktivitaetComp);
        this.bodenStapelFactory = bodenStapelFactory;
    }

    @Override
    public void planeAlleSteps(StepStack stepStack) {
        final Vector2 nehmenStartPosition = aBewegungComp.positionComp.position;

        for (LagerComp lagerComp : sysLager.toCollection(new LinkedList<>())) { // todo sehr aehnlich koennte man auch haendler programmieren die Gewinne aus abitrage machen
            if (lagerComp instanceof LagerCompOhneGeld) {
                IntIntHashMap sollNehmenHashMap = new IntIntHashMap();
                for (Gegenstand bsGeg : lagerComp.gegenstaendeComp.gegenstandListe) {
                    sollNehmenHashMap.put(bsGeg.gettId(), bsGeg.getMengeVerfuegbar());
                }
                StepNehmen nehmenAusBsStep = new StepNehmen(aufsteigendesSymbolFactory,
                        aBewegungComp, lagerComp.bewegungszielComp, lagerComp.gegenstaendeComp,
                        aGegenstaendeComp, sollNehmenHashMap, Enums.EnumReservieren.JETZT_RESERVIEREN,
                        nehmenStartPosition);
                if (nehmenAusBsStep.versuchen() == Enums.EnumStpStatus.OK) {

                    Vector2 legenStartPosition = nehmenAusBsStep.stepGeheZu.gettZielZelle().position;
                    stepStack.handle(new PrivatLegenPlan(aufsteigendesSymbolFactory, sysLager,
                            aBewegungComp, aGeldComp, aGegenstaendeComp,
                            lagerComp.eigentumComp.getEigentuemer().geldComp, lagerComp, true,
                            legenStartPosition));

                    if (stepStack.isEmpty()) {
                        nehmenAusBsStep.setStatus(Enums.EnumStpStatus.FAIL);
                        stepStack.handle(nehmenAusBsStep, true);
                    } else {
                        eigentuemerComp = lagerComp.eigentumComp.getEigentuemer();
                        stepStack.handle(nehmenAusBsStep, true);
                        return;
                    }
                }
            }
        }
    }

    @Override
    protected void stepBeendet(Step step) {
        if (step instanceof StepNehmen && step.getStatus() == Enums.EnumStpStatus.ERLEDIGT) {
            transportiertFremdeMenge = true;
            eigentuemerComp.addListener(this);
        }
        super.stepBeendet(step);
    }

    @Override
    public void planeNaechstenStep(StepStack stepStack) {
        if (transportiertFremdeMenge
                && getCurrentStep().getStatus() == Enums.EnumStpStatus.FAIL) {
            Task task = new TaskLoswerden(aufsteigendesSymbolFactory, bodenStapelFactory, sysLager,
                    eigentuemerComp, stpComp, aBewegungComp, aGeldComp, aGegenstaendeComp,
                    null, false, null);
            stepStack.handle(task);
        }
    }
}
