package de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.departments.gebiet;

import de.arnonym.strategiespiel.framework.werkzeuge.collections.Collector;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.CellsComp;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.CellsDepartmentComp;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.ArbeitnehmerAi;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.VarEmployerComp;
import de.arnonym.strategiespiel.framework.stp.Task;
import de.arnonym.strategiespiel.spielSchirm.arbeit.task.liefernArbeitszeit.ausbeuten.TaskAbbauen;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.ausbeuten.AbbaubarComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.ressourcen.RessourceInfo;

public class AbbauenDepartmentComp extends CellsDepartmentComp<RessourceInfo, RessourcenAssignment> {

    public AbbauenDepartmentComp(Entity entity, CellsComp cellsComp,
                                 VarEmployerComp<? extends RessourceInfo> employerComp) {
        super(new Collector<>(), entity, new Collector<>(), cellsComp, employerComp);
    }

    @Override
    protected RessourcenAssignment getAssignment(RessourceInfo ressourceInfo) {
        return ressourceInfo.abbauenAssignment;
    }

    @Override
    public Task createTask_nl(ArbeitnehmerAi arbeitnehmerAi) {
        return new TaskAbbauen(arbeitnehmerAi.aufsteigendesSymbolFactory,
                arbeitnehmerAi.bodenStapelFactory, arbeitnehmerAi.sysLager,

                cellsComp.comps(AbbaubarComp.class), employerComp.eigentuemerComp, employerComp.eigentuemerComp.geldComp,

                arbeitnehmerAi.stpComp, arbeitnehmerAi.bewegungComp,
                arbeitnehmerAi.einkommenComp, arbeitnehmerAi.gegenstaendeComp,
                arbeitnehmerAi.habitalwerteComp, arbeitnehmerAi.produktivitaetComp);
    }
}
