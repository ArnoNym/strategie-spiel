package de.arnonym.strategiespiel.spielSchirm.arbeit.task.liefernArbeitszeit.ausbeuten;

import java.util.Collection;

import de.arnonym.strategiespiel.framework.stp.Step;
import de.arnonym.strategiespiel.framework.stp.StpComp;
import de.arnonym.strategiespiel.spielSchirm.Enums;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.ausbeuten.AusbeutbarComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.ausbeuten.FruechteComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.bewegung.BewegungComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.gegenstaende.GegenstaendeComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.EinkommenComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.GeldComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.herrschaft.EigentuemerComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.werte.HabitalwerteComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.werte.ProduktivitaetComp;
import de.arnonym.strategiespiel.spielSchirm.arbeit.step.ausbeuten.StepAusbeuten;
import de.arnonym.strategiespiel.spielSchirm.arbeit.step.ausbeuten.StepErnten;
import de.arnonym.strategiespiel.spielSchirm.bodenStapel.BodenStapelFactory;
import de.arnonym.strategiespiel.spielSchirm.ui.aufsteigendeSymbole.AufsteigendesSymbolFactory;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.listSystems.privateListSystems.SysLager;

public class TaskErnten extends TaskAusbeuten<FruechteComp> {
    public TaskErnten(AufsteigendesSymbolFactory aufsteigendesSymbolFactory, BodenStapelFactory bodenStapelFactory, SysLager sysLager, Collection<FruechteComp> zAusbeutbarComps, EigentuemerComp neuerEigentuemerComp, boolean overrideEigentuemer, GeldComp gGeldComp, int salary, StpComp aStpComp, BewegungComp aBewegungComp, GeldComp aGeldComp, GegenstaendeComp aGegenstaendeComp, HabitalwerteComp aHabitalwerteComp, ProduktivitaetComp aProduktivitaetComp) {
        super(aufsteigendesSymbolFactory, bodenStapelFactory, sysLager, zAusbeutbarComps, neuerEigentuemerComp, overrideEigentuemer, gGeldComp, salary, aStpComp, aBewegungComp, aGeldComp, aGegenstaendeComp, aHabitalwerteComp, aProduktivitaetComp);
    }

    @Override
    protected StepAusbeuten gettStep(AusbeutbarComp ausbeutbarComp) {
        return new StepErnten(neuerEigentuemerComp, (FruechteComp) ausbeutbarComp,
                gGeldComp, salary, aBewegungComp, aGeldComp, aHabitalwerteComp,
                aProduktivitaetComp, aGegenstaendeComp, null);
    }

    @Override
    protected void stepBeendet(Step step) {
        if (step instanceof StepErnten && step.isStatus(Enums.EnumStpStatus.OK)) {
            ausbeutbarComps.remove(((StepErnten) step).compFruechte);
        }
        super.stepBeendet(step);
    }
}
