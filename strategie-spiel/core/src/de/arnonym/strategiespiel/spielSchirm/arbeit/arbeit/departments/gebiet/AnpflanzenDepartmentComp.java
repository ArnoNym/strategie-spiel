package de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.departments.gebiet;

import java.util.Collection;

import de.arnonym.strategiespiel.framework.werkzeuge.collections.Collector;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.ArbeitnehmerAi;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.CellsComp;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.CellsDepartmentComp;
import de.arnonym.strategiespiel.framework.stp.Task;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.VarEmployerComp;
import de.arnonym.strategiespiel.spielSchirm.arbeit.task.arbeitszeit.AnpflanzenTask;
import de.arnonym.strategiespiel.spielSchirm.umwelt.ressourcen.stufen.WachstumRessourceInfo;
import de.arnonym.strategiespiel.spielSchirm.worldMap.Zelle;

public class AnpflanzenDepartmentComp extends CellsDepartmentComp<WachstumRessourceInfo, RessourcenAssignment> {

    public AnpflanzenDepartmentComp(Entity entity,

                                    CellsComp cellsComp, VarEmployerComp<WachstumRessourceInfo> employerComp) {
        super(new Collector<>(), entity, new Collector<>(), cellsComp, employerComp);
    }

    @Override
    public Task createTask_nl(ArbeitnehmerAi arbeitnehmerAi) {
        Collection<Zelle> cells = cellsComp.cells(cell -> !cell.isKollisionBauen());

        return new AnpflanzenTask(arbeitnehmerAi.ressourcenFactory,

                cells, employerComp.eigentuemerComp.getEigentuemer().geldComp,

                arbeitnehmerAi.bewegungComp, arbeitnehmerAi.einkommenComp,
                arbeitnehmerAi.habitalwerteComp, arbeitnehmerAi.produktivitaetComp,
                arbeitnehmerAi.stpComp,

                getAssignment_nl().id);
    }

    @Override
    protected RessourcenAssignment getAssignment(WachstumRessourceInfo wachstumRessourceInfo) {
        return wachstumRessourceInfo.pflegeAssignment;
    }
}
