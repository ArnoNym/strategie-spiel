package de.arnonym.strategiespiel.spielSchirm.arbeit.step.ausbeuten;

import com.badlogic.gdx.math.Vector2;

import de.arnonym.strategiespiel.framework.werkzeuge.collections.Collector;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.ausbeuten.AbbaubarComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.bewegung.BewegungComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.gegenstaende.GegenstaendeComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.EinkommenComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.GeldComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.herrschaft.EigentuemerComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.werte.HabitalwerteComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.werte.ProduktivitaetComp;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.endstufe.Gegenstand;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.endstufe.GegenstandList;

/**
 * Created by LeonPB on 25.06.2018.
 */

public class StepAbbauen extends StepAusbeuten {
    public final AbbaubarComp compAbbaubar;

    public StepAbbauen(EigentuemerComp eigentuemerComp,

                       AbbaubarComp zAbbauenComp,

                       GeldComp gGeldComp, int salary,

                       BewegungComp bewegungComp, GeldComp aGeldComp,
                       HabitalwerteComp habitalwerteComp, ProduktivitaetComp produktivitaetComp,
                       GegenstaendeComp gegenstaendeComp,

                       Vector2 start_nl) {
        super(new Collector<>(), eigentuemerComp, zAbbauenComp, gGeldComp, salary, bewegungComp,
                aGeldComp, habitalwerteComp, produktivitaetComp, gegenstaendeComp, start_nl);
        this.compAbbaubar = zAbbauenComp;
    }

    @Override
    protected Gegenstand gettAusbeutenGegenstand(GegenstandList aGegList, GegenstandList zGegList) {
        boolean hatSekundaerGegenstaende = false;
        for (Gegenstand gegenstand : zGegList) {
            /* Wenn eine Sekundaermenge von einem anderen Akteur zum Abbauen reserviert wurde kann
            es sein, dass PrimaerGeg abgebaut werden obwohl noch Sekundaer Geg in der Ressource
            sind. Dies sind allerdings Ausnahmen, die auch keinen echten negativen Einfluss haben */
            if (gegenstand.gegMuster.isNachwachsbar() && gegenstand.getMengeVerfuegbar() > 0) {
                hatSekundaerGegenstaende = true;
                break;
            }
        }

        for (Gegenstand gegenstand : zGegList) {
            if ((!hatSekundaerGegenstaende || gegenstand.gegMuster.isNachwachsbar())
                    && pruefeGegenstand(gegenstand)) {
                return gegenstand;
            }
        }
        return null;
    }
}
