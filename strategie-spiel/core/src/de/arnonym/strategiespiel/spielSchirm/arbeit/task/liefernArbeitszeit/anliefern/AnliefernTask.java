package de.arnonym.strategiespiel.spielSchirm.arbeit.task.liefernArbeitszeit.anliefern;

import com.badlogic.gdx.math.Vector2;

import de.arnonym.strategiespiel.framework.stp.StpComp;
import de.arnonym.strategiespiel.framework.listener.StoryTeller;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.bewegung.BewegungComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.bewegung.BewegungszielComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.gegenstaende.GegenstaendeComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.EinkommenComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.GeldComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.herrschaft.EigentumComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.werte.HabitalwerteComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.werte.ProduktivitaetComp;
import de.arnonym.strategiespiel.spielSchirm.ui.aufsteigendeSymbole.AufsteigendesSymbolFactory;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.listSystems.privateListSystems.SysLager;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dorfbewohner.tauschen.plan.nehmen.PlanNehmenBeruf;
import de.arnonym.strategiespiel.framework.stp.stacks.StepStack;
import de.arnonym.strategiespiel.spielSchirm.arbeit.step.arbeitszeit.material.StepMaterial;
import de.arnonym.strategiespiel.spielSchirm.arbeit.task.liefernArbeitszeit.TransportArbeitszeitTask;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.endstufe.GegenstandList;
import de.arnonym.strategiespiel.framework.werkzeuge.collections.hashMaps.IntIntHashMap;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.CollectionUtils;

public abstract class AnliefernTask extends TransportArbeitszeitTask {
    public final AufsteigendesSymbolFactory aufsteigendesSymbolFactory;

    public final SysLager sysLager;

    public final BewegungComp aBewegungComp;
    public final GegenstaendeComp aGegenstaendeComp;
    public final HabitalwerteComp aHabitalwerteComp;
    public final ProduktivitaetComp aProduktivitaetComp;

    public AnliefernTask(AufsteigendesSymbolFactory aufsteigendesSymbolFactory,

                         SysLager sysLager,

                         GeldComp gGeldComp, int salary,

                         StpComp aStpComp, BewegungComp aBewegungComp,
                         GeldComp aGeldComp,
                         GegenstaendeComp aGegenstaendeComp, HabitalwerteComp aHabitalwerteComp,
                         ProduktivitaetComp aProduktivitaetComp,

                         StoryTeller... storyTellers) {
        super(gGeldComp, salary, aGeldComp, aStpComp, CollectionUtils.combine(storyTellers,
                aBewegungComp, aGegenstaendeComp, aHabitalwerteComp, aProduktivitaetComp));
        this.aufsteigendesSymbolFactory = aufsteigendesSymbolFactory;
        this.sysLager = sysLager;
        this.aBewegungComp = aBewegungComp;
        this.aGegenstaendeComp = aGegenstaendeComp;
        this.aHabitalwerteComp = aHabitalwerteComp;
        this.aProduktivitaetComp = aProduktivitaetComp;
    }

    protected boolean handleStepMaterial(StepStack stepStack, StepMaterial<?> stepMaterial) {
        switch (stepStack.handle(stepMaterial, true)) {
            case OK:
                // Material vorhanden und erreichbar -> Arbeite
                return true;
            case FAIL_BEI_GEHE_ZU:
                // Nicht erreichbar -> verusche es mit anderer Arbeit
                return false;
            case FAIL:
                // Nicht genug Material -> beschaffe Material
                return einkaufen(stepStack, stepMaterial);
            default:
                throw new IllegalStateException();
        }
    }

    protected boolean einkaufen(StepStack stepStack, StepMaterial<?> stepMaterial) {
        return einkaufen(stepStack, stepMaterial.zBewegungszielComp, stepMaterial.zGegenstaendeComp,
                stepMaterial.gGeldComp, stepMaterial.zEigentumComp,
                stepMaterial.assignment.idMengeHashMap, aBewegungComp.positionComp.position);
    }

    protected boolean einkaufen(StepStack stepStack,

                                BewegungszielComp gBewegungszielComp,
                                GegenstaendeComp gGegenstaendeComp, GeldComp gGeldComp,
                                EigentumComp gEigentumComp,

                                IntIntHashMap idMengenHashMap, Vector2 startPosition) {
        IntIntHashMap zuNehmenHashMap = createZuNehmenHashMap(gGegenstaendeComp, idMengenHashMap);

        stepStack.handle(new PlanNehmenBeruf(aufsteigendesSymbolFactory, sysLager, aBewegungComp,
                aGeldComp, this.aGegenstaendeComp, gBewegungszielComp,
                gGegenstaendeComp, gGeldComp, gEigentumComp, zuNehmenHashMap, startPosition));

        return !stepStack.isEmpty();
    }

    private IntIntHashMap createZuNehmenHashMap(GegenstaendeComp gGegenstaendeComp,
                                                IntIntHashMap idMengenHashMap) {
        IntIntHashMap zuNehmenHashMap = new IntIntHashMap();
        GegenstandList zgl = gGegenstaendeComp.gegenstandListe;
        for (int id : idMengenHashMap.keySet()) {
            zuNehmenHashMap.put(id, zgl.gettDurchId(id).getMengeBisVoll());
        }
        return zuNehmenHashMap;
    }
}
