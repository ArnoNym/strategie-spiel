package de.arnonym.strategiespiel.spielSchirm.arbeit.task.liefernArbeitszeit.anliefern;

import java.util.LinkedList;

import de.arnonym.strategiespiel.framework.stp.StpComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.bewegung.BewegungComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.baugrube.BaugrubeComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.gegenstaende.GegenstaendeComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.EinkommenComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.werte.HabitalwerteComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.werte.ProduktivitaetComp;
import de.arnonym.strategiespiel.spielSchirm.ui.aufsteigendeSymbole.AufsteigendesSymbolFactory;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.listSystems.privateListSystems.SysBaugrube;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.listSystems.privateListSystems.SysLager;
import de.arnonym.strategiespiel.framework.stp.stacks.StepStack;
import de.arnonym.strategiespiel.spielSchirm.arbeit.step.arbeitszeit.material.StepBauen;

public class BauenTask extends AnliefernTask {
    public final SysBaugrube sysBaugrube;

    public BauenTask(AufsteigendesSymbolFactory aufsteigendesSymbolFactory,

                     SysBaugrube sysBaugrube, SysLager sysLager,

                     StpComp aStpComp, BewegungComp aBewegungComp,
                     EinkommenComp aEinkommenComp, GegenstaendeComp aGegenstaendeComp,
                     HabitalwerteComp aHabitalwerteComp, ProduktivitaetComp aProduktivitaetComp) {
        super(aufsteigendesSymbolFactory, sysLager, null, aStpComp, aBewegungComp,
                aEinkommenComp, aGegenstaendeComp, aHabitalwerteComp, aProduktivitaetComp);
        this.sysBaugrube = sysBaugrube;
    }

    @Override
    public void planeAlleSteps(StepStack stepStack) {
        for (BaugrubeComp baugrubeComp : sysBaugrube.toCollection(new LinkedList<>())) {
            StepBauen stepBauen = new StepBauen(baugrubeComp.timeClock,
                    baugrubeComp.bewegungszielComp, baugrubeComp.eigentumComp,
                    baugrubeComp.gegenstaendeComp, baugrubeComp.geldComp, baugrubeComp.transComp,
                    baugrubeComp.musterAuftragMaterial, aBewegungComp, aGeldComp,
                    aHabitalwerteComp, aProduktivitaetComp, null);
            if (handleStepMaterial(stepStack, stepBauen)) {
                return;
            }
        }
    }

    @Override
    public void planeNaechstenStep(StepStack stepStack) {

    }
}
