package de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit;

import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.framework.werkzeuge.collections.Collector;

public abstract class FixDepartmentComp extends DepartmentComp {
    public FixDepartmentComp(Collector systems, Entity entity, Collector comps, EmployerComp employerComp) {
        super(systems, entity, comps, employerComp);
    }
}
