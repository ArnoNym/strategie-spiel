package de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit;

import com.badlogic.gdx.math.Vector2;

import java.util.Set;

import de.arnonym.strategiespiel.spielSchirm.Dorf;
import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenUpdatePausenDauer;
import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenBalance;
import de.arnonym.strategiespiel.framework.ecs.collections.Matcher;

public class EmploymentMatcher extends Matcher<ArbeitnehmerAi, EmployerComp> {
    private final Dorf dorf;

    public EmploymentMatcher(Dorf dorf) {
        super(ArbeitnehmerAi.class, EmployerComp.class,
                KonstantenUpdatePausenDauer.BERUFE_MATCHER_UPDATE_PAUSE);
        this.dorf = dorf;
    }

    public int schaetzeGehalt(float kopfProd, float koerperProd, Vector2 position) {
        // todo employers in Umfeld betrachten, prodKopf, prodKoerper, gehalt zwischenspeichern und dann Regression durchfuehren um Gehalt zu schaetzen
        return (int) (KonstantenBalance.START_GEHALT_sek * kopfProd
                + KonstantenBalance.START_GEHALT_sek * koerperProd);
    }

    @Override
    protected EmployerComp findBestMatch(ArbeitnehmerAi arbeitnehmerAi,
                                         Set<EmployerComp> employerComps) {
        return employerComps.iterator().next();
        /*DepartmentComp aktuellerCa = aiArbeitnehmer.getEmployer();
        int maxRealGehalt = 0;
        DepartmentComp maxGehaltCa = null;

        //IdentityConnectable.out.println("MatcherBerufe ::: testesetste arbeitgeberCompList.size == "+arbeitgeberCompList.size());
        for (DepartmentComp ca : employerComps) {
            if (ca.getGehalt_sekunde() <= 0) {
                throw new IllegalStateException("Gehalt wurde nicht initialisiert!");
            }
            if (ca.getAnzahlAngestellte() < ca.maxStellen || ca == aktuellerCa) {
                int realGehalt = ca.gettRealGehalt_sekunde(aiArbeitnehmer.produktivitaetComp);

                if (realGehalt <= 0) {
                    throw new IllegalStateException("ArbeitgeberComp.getClass(): "
                            +ca.getClass().getSimpleName());
                }

                int nutzen = realGehalt;
                if (ca != aktuellerCa) {
                    nutzen *= KonstantenBalance.DB_BERUF_WECHSEL_MULT;
                }
                //if (zuhause != null) { //TODO Distanz zum Wohnort etc
                //    //wert -=
                //}

                if (nutzen > maxRealGehalt) {
                    maxRealGehalt = realGehalt;
                    maxGehaltCa = ca;
                    maxGehalt = ca.getGehalt_sekunde();
                }
            }
        }
        if (Konstanten.DEVELOPER_EXCEPTIONS
                && !employerComps.isEmpty()
                && maxGehaltCa == null) {
            throw new IllegalStateException();
        }
        return maxGehaltCa;*/
    }

    @Override
    protected void couple(ArbeitnehmerAi arbeitnehmerAi, EmployerComp employerComp) {
        if (employerComp.equals(arbeitnehmerAi.getEmployer())) {
            return;
        }
        arbeitnehmerAi.setEmployer(employerComp);
        employerComp.addArbeitnehmer(arbeitnehmerAi);
    }

    @Override
    protected void decoupleAkteur(ArbeitnehmerAi aktuer) {
        aktuer.setEmployer(null);
        aktuer.getEmployer().removeArbeitnehmer(aktuer);
    }

    @Override
    protected void decoupleReakteur(EmployerComp reakteur) {
        for (ArbeitnehmerAi arbeitnehmerAi : reakteur.arbeitnehmerArray()) {
            arbeitnehmerAi.setEmployer(null);
            reakteur.removeArbeitnehmer(arbeitnehmerAi);
        }
    }
}
