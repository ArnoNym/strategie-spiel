package de.arnonym.strategiespiel.spielSchirm.arbeit.step;

import com.badlogic.gdx.math.Vector2;

import java.beans.PropertyChangeEvent;

import de.arnonym.strategiespiel.framework.ecs.component.Comp;
import de.arnonym.strategiespiel.framework.listener.StoryTeller;
import de.arnonym.strategiespiel.framework.werkzeuge.collections.Collector;
import de.arnonym.strategiespiel.spielSchirm.balancing.Konstanten;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.bewegung.BewegungComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.bewegung.BewegungszielComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.EinkommenComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.GeldComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.werte.HabitalwerteComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.werte.ProduktivitaetComp;
import de.arnonym.strategiespiel.spielSchirm.StepNGZ;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.Geld;
import de.arnonym.strategiespiel.spielSchirm.Enums;
import de.arnonym.strategiespiel.framework.werkzeuge.pause.DeltaPause;
import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenBalance;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.MathUtils;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.SpecialUtils;

public abstract class StepArbeit<Assignment extends de.arnonym.strategiespiel.spielSchirm.arbeit.auftrag.Assignment> extends StepNGZ {
    public final GeldComp gGeldComp;
    public final int salary;
    public final Assignment assignment;

    public final GeldComp aGeldComp;
    public final HabitalwerteComp aHabitalwerteComp;
    public final ProduktivitaetComp aProduktivitaetComp;

    private float realProduktivitaet;
    private int summeGehalt = 0;
    private DeltaPause erledigtPause;

    public StepArbeit(Collector<Comp> comps,

                      BewegungszielComp zBewegungszielComp,

                      GeldComp gGeldComp, int salary,

                      Assignment assignment,

                      BewegungComp aBewegungComp, GeldComp aGeldComp,
                      HabitalwerteComp aHabitalwerteComp, ProduktivitaetComp aProduktivitaetComp,

                      Vector2 start_nl) {
        super(zBewegungszielComp, new Collector<StoryTeller>(comps.add(gGeldComp,
                aHabitalwerteComp, aProduktivitaetComp)).toArray(new StoryTeller[]{}),
                aBewegungComp, start_nl);
        this.gGeldComp = gGeldComp;
        this.assignment = assignment;
        this.salary = salary;
        this.aGeldComp = aGeldComp;
        this.aHabitalwerteComp = aHabitalwerteComp;
        this.aProduktivitaetComp = aProduktivitaetComp;
    }

    @Override
    protected final Enums.EnumStpStatus versuchen_or_or() {
        Enums.EnumStpStatus enumStpStatus = versuchen_or_or_or(); // Als Erstes wegen reservieren()
        if (enumStpStatus != Enums.EnumStpStatus.OK) {
            return enumStpStatus;
        }

        float kannBezahlenZeit;
        if (salary == 0) {
            kannBezahlenZeit = Float.MAX_VALUE;
        } else {
            kannBezahlenZeit = MathUtils.myIntDiv(gGeldComp.getGeld().getMengeVerfuegbar(), salary);
        }
        float arbeitszeit = MathUtils.min(
                gettZuPlanenArbeitsZeit(),
                kannBezahlenZeit,
                KonstantenBalance.ARBEIT_ZEIT_AM_STUECK);

        if (arbeitszeit <= 0) {
            if (Konstanten.DEVELOPER_VISUELL_STEP_SYSTEM_OUT) {
                System.out.println("Feedback StepArbeit"
                        + "\n- FAIL in versuchen(), da SollArbeiten-Zeit == "
                        +arbeitszeit+", zuPlanenArbeitszeit == "+gettZuPlanenArbeitsZeit()
                        +", kannBezahlenZeit == "+kannBezahlenZeit
                        +", KonstantenBalance.ARBEIT_ZEIT_AM_STUECK == "
                        +KonstantenBalance.ARBEIT_ZEIT_AM_STUECK);
            }
            return Enums.EnumStpStatus.FAIL;
        }

        reservieren(arbeitszeit);

        // Pause Zeit wird fuer eventuellen reset benoetigt
        erledigtPause = new DeltaPause(arbeitszeit, Enums.EnumPauseStart.VORBEI);
        realProduktivitaet = aProduktivitaetComp.realProduktivitaet(assignment);

        return Enums.EnumStpStatus.OK;
    }

    protected Enums.EnumStpStatus versuchen_or_or_or() {
        return Enums.EnumStpStatus.OK;
    }

    protected float gettZuPlanenArbeitsZeit() {
        return assignment.arbeitszeit;
    }

    @Override
    protected void anfangen_or() {
        erledigtPause.settStartzeitJetzt();
    }

    // RESERVIEREN ////////////////////////////////////////////////////////////////////////////////

    protected void reservieren(float arbeitszeit) {
        if (isReserviert()) throw new IllegalStateException();
        setReserviert(true);
        summeGehalt = (int) (arbeitszeit * salary);
        gGeldComp.getGeld().reservierenNehmen(summeGehalt);
        aGeldComp.getGeld().reservierenLegen(summeGehalt);
    }

    protected void reservierenRueckgaengig_or() {
        gGeldComp.getGeld().reservierenNehmenRueckgaengig(summeGehalt);
        aGeldComp.getGeld().reservierenLegenRueckgaengig(summeGehalt);
    }

    //

    @Override
    protected final void durchfuehren_or(float delta) { // Nutze erledigt_or()
        erledigtPause.issVorbei(delta * realProduktivitaet, false);
    }

    @Override
    public boolean pruefenObFail() {
        return false;
    }

    @Override
    protected void failNachVersuchenVorAnfangen_or() {
        if (erledigtPause != null) {
            reservierenRueckgaengig();
        }
    }

    @Override
    protected void failNachAnfangenVorDurchfuehren_or() {
        reservierenRueckgaengig();
    }

    @Override
    protected void failNachDurchfuehren_or() {
        reservierenRueckgaengig();
    }

    @Override
    public boolean pruefenObErledigt() {
        return erledigtPause.issVorbei();
    }

    @Override
    public void erledigt_or() {
        float arbeitDauer = erledigtPause.getDauer();

        gGeldComp.mengeVerringern(summeGehalt, true);
        aGeldComp.mengeErhoehen((int) (summeGehalt
                * KonstantenBalance.GEHALT_GELD_ERSCHAFFEN_MULTIPLIKATOR), true);

        aHabitalwerteComp.addArbeitDauer(arbeitDauer,
                assignment.optimaleProduktivitaetKopf,
                assignment.optimaleProduktivitaetKoerper);
        aHabitalwerteComp.schadenZufuegen(assignment.musterSchaden, arbeitDauer);
    }

    // PROPERTY CHANGE ////////////////////////////////////////////////////////////////////////////

    @Override
    public boolean propertyChange_or_or(PropertyChangeEvent propertyChangeEvent) {
        SpecialUtils.changeGeld(
                (Geld) propertyChangeEvent.getOldValue(),
                (Geld) propertyChangeEvent.getNewValue(),
                gGeldComp,
                aGeldComp,
                summeGehalt);
        return false;
    }

    @Override
    protected void addPropertyChangeListeners() {
        gGeldComp.addPropertyChangeListener(this);
        aGeldComp.addPropertyChangeListener(this);
    }

    @Override
    protected void removePropertyChangeListeners() {
        gGeldComp.removePropertyChangeListener(this);
        aGeldComp.removePropertyChangeListener(this);
    }

    // KOMPLEXE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    protected float gettArbeitszeit() {
        return erledigtPause.getDauer();
    }

    // PROTECTED GETTER UND SETTER ////////////////////////////////////////////////////////////////

    protected int getSummeGehalt() {
        return summeGehalt;
    }
}
