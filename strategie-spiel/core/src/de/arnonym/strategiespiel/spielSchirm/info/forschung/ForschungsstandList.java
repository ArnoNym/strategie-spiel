package de.arnonym.strategiespiel.spielSchirm.info.forschung;

import de.arnonym.strategiespiel.spielSchirm.balancing.IdsRessourcen;
import de.arnonym.strategiespiel.spielSchirm.gebaude.GebaudeInfo;
import de.arnonym.strategiespiel.spielSchirm.balancing.IdsGebaude;
import de.arnonym.strategiespiel.spielSchirm.balancing.IdsGegenstaende;
import de.arnonym.strategiespiel.spielSchirm.umwelt.ressourcen.RessourceInfo;
import de.arnonym.strategiespiel.framework.werkzeuge.collections.id.IdList;
import de.arnonym.strategiespiel.spielSchirm.info.muster.GegInfo;

public class ForschungsstandList extends IdList<Forschungsstand> {

    public ForschungsstandList() {
        for (GegInfo gegMuster : IdsGegenstaende.get()) {
            if (gegMuster.isErforschbar()) {
                add(new Forschungsstand(gegMuster.id, gegMuster.getForschungAssignment()));
            } else {
                add(new Forschungsstand(gegMuster.id));
            }
        }
        for (GebaudeInfo gebaudeMuster : IdsGebaude.get()) {
            if (gebaudeMuster.isErforschbar()) {
                add(new Forschungsstand(gebaudeMuster.id, gebaudeMuster.getForschungAssignment()));
            } else {
                add(new Forschungsstand(gebaudeMuster.id));
            }
        }
        for (RessourceInfo resOutMuster : IdsRessourcen.get()) {
            if (resOutMuster.isErforschbar()) {
                add(new Forschungsstand(resOutMuster.id, resOutMuster.getForschungAssignment()));
            } else {
                add(new Forschungsstand(resOutMuster.id));
            }
        }
    }
}
