package de.arnonym.strategiespiel.spielSchirm.info.muster;

import de.arnonym.strategiespiel.framework.werkzeuge.textures.Texture;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.departments.bildung.ForschungAssignment;
import de.arnonym.strategiespiel.framework.werkzeuge.collections.id.IdObjekt;

public abstract class Info implements IdObjekt {
    public final int id;
    public final String name;
    public final String beschreibung;

    private boolean erforschbar;
    private ForschungAssignment forschungAssignment;

    public Info(int id, String name, String beschreibung) {
        this.id = id;
        this.name = name;
        this.beschreibung = beschreibung;
        this.erforschbar = false;
    }

    public Info(int id, String name, String beschreibung,
                ForschungAssignment forschungAssignment_nl) {
        this.id = id;
        this.name = name;
        this.beschreibung = beschreibung;
        setForschungAssignment(forschungAssignment_nl);
    }

    // DRAW ///////////////////////////////////////////////////////////////////////////////////////

    public abstract Texture getIconTexture();

    // PROTECTED GETTER UND SETTER ////////////////////////////////////////////////////////////////

    protected void setForschungAssignment(ForschungAssignment forschungAssignment_nl) {
        if (this.forschungAssignment != null) {
            throw new IllegalStateException();
        }
        this.forschungAssignment = forschungAssignment_nl;
        if (forschungAssignment_nl != null) {
            this.erforschbar = true;
        }
    }

    // EINFACH GETTER UND SETTER //////////////////////////////////////////////////////////////////

    @Override
    public int gettId() {
        return id;
    }

    public boolean isErforschbar() {
        return erforschbar;
    }

    public ForschungAssignment getForschungAssignment() {
        if (!erforschbar) throw new IllegalStateException();
        return forschungAssignment;
    }
}
