package de.arnonym.strategiespiel.spielSchirm.info.muster;

import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.departments.bildung.ForschungAssignment;
import de.arnonym.strategiespiel.spielSchirm.arbeit.auftrag.AssignmentHerstellen;
import de.arnonym.strategiespiel.spielSchirm.zahlen.schaden.MusterSchaden;
import de.arnonym.strategiespiel.spielSchirm.Enums;
import de.arnonym.strategiespiel.framework.werkzeuge.textures.Texture;

public class GegInfo extends Info {
    /** Infos ueber Variablen in : "notizenGegUndTaschen" (txt im selben package)
     *
     *  _create = Wird nur beim Erstellen des Spiels / des Objekts benoetigt. Danach darf kein
     *  Zugriff mehr stattfinden */

    public final int platzVerbrauch;
    public final float vergammelnProzentProJahr;
    public final Texture texture;

    /** zB im Magen fuer alle Gegenstaende true */
    public final boolean teilbar_create;
    /** Fuer die ersten Gebaude bevor das Dorf eigene Preise hat und fuer den Nutzen */
    public final int wert_create;

    public GegInfo(int id, String name, String beschreibung, int wert, int platzVerbrauch,
                   boolean teilbar, float vergammelnProzentProJahr, Texture texture) {
        super(id, name, beschreibung);
        this.platzVerbrauch = platzVerbrauch;
        this.texture = texture;
        this.vergammelnProzentProJahr = vergammelnProzentProJahr;

        this.wert_create = wert;
        this.teilbar_create = teilbar;
    }

    @Override
    public Texture getIconTexture() {
        return texture;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    // GEGENSEITIGER AUSSCHLUSS ///////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////

    private Enums.EnumGegenstandKlasse enumGegenstandKlasse;
    private void settEnumGegenstandKlasse(Enums.EnumGegenstandKlasse enumGegenstandKlasse) {
        if (this.enumGegenstandKlasse != null) {
            throw new IllegalArgumentException("Es darf nur ein 'construct' verwendet werden! "
                    + "bisher:" + this.enumGegenstandKlasse.name()
                    + "neu:" + enumGegenstandKlasse.name());
        }
        this.enumGegenstandKlasse = enumGegenstandKlasse;
    }

    // Nahrung
    private float eiweissAnteil;
    private float vitamineAnteil;
    private float kohlenhydrateGesundAnteil;
    private float kohlenhydrateUngesundAnteil;
    public GegInfo constructNahrung(float eiweissAnteil, float vitamineAnteil,
                                    float kohlenhydrateGesundAnteil,
                                    float kohlenhydrateUngesundAnteil) {
        settEnumGegenstandKlasse(Enums.EnumGegenstandKlasse.NAHRUNG);
        this.eiweissAnteil = eiweissAnteil;
        this.vitamineAnteil = vitamineAnteil;
        this.kohlenhydrateGesundAnteil = kohlenhydrateGesundAnteil;
        this.kohlenhydrateUngesundAnteil = kohlenhydrateUngesundAnteil;
        return this;
    }

    // Medizin
    private float gesundheit;
    public GegInfo constructMedizin(float gesundheit) {
        settEnumGegenstandKlasse(Enums.EnumGegenstandKlasse.MEDIZIN);
        this.gesundheit = gesundheit;
        return this;
    }

    // Werkzeug
    private float produktivitaet;
    private float schutz;
    private Enums.EnumKoerperteil schuetztKoeperteil;
    public GegInfo constructWerkzeug(float produktivitaet, float schutz,
                                     Enums.EnumKoerperteil schuetztKoeperteil) {
        settEnumGegenstandKlasse(Enums.EnumGegenstandKlasse.WERKZEUG);
        this.produktivitaet = produktivitaet;
        this.schutz = schutz;
        this.schuetztKoeperteil = schuetztKoeperteil;
        return this;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    // ZUSATZ /////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////

    // Herstellbar
    private boolean herstellbar = false;
    private AssignmentHerstellen musterAuftragHerstellen;
    public GegInfo addHerstellbar(float optimaleProduktivitaetKopf,
                                  float optimaleProduktivitaetKoerper, float herstellDauer,
                                  MusterSchaden herstellenSchaden, int[][] inputs,
                                  int entstehendeMenge,
                                  ForschungAssignment forschungAssignment_nl) {
        this.herstellbar = true;
        this.musterAuftragHerstellen = new AssignmentHerstellen(gettId(),
                optimaleProduktivitaetKopf, optimaleProduktivitaetKoerper, herstellDauer,
                herstellenSchaden, inputs, entstehendeMenge);
        setForschungAssignment(forschungAssignment_nl);
        return this;
    }

    // Abbaubar
    private boolean abbaubar = false;
    private float abbauDauer;
    private float[] abbauSchadenArray;
    public GegInfo addAbbaubar(float abbauDauer, float[] abbauSchadenArray) {
        this.abbaubar = true;
        this.abbauDauer = abbauDauer;
        this.abbauSchadenArray = abbauSchadenArray;
        return this;
    }

    // Nachwachsbar
    private boolean nachwachsbar = false;
    private float nachwachsenMenge_jahr;
    public GegInfo addNachwachsbar(float nachwachsenMenge_jahr) {
        this.nachwachsbar = true;
        this.nachwachsenMenge_jahr = nachwachsenMenge_jahr;
        return this;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    // EINFACHE GETTER ////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////

    public float getVergammelnProzentProJahr() {
        return vergammelnProzentProJahr;
    }

    public Enums.EnumGegenstandKlasse getEnumGegenstandKlasse() {
        return enumGegenstandKlasse;
    }

    public float getEiweissAnteil() {
        return eiweissAnteil;
    }

    public float getVitamineAnteil() {
        return vitamineAnteil;
    }

    public float getKohlenhydrateGesundAnteil() {
        return kohlenhydrateGesundAnteil;
    }

    public float getKohlenhydrateUngesundAnteil() {
        return kohlenhydrateUngesundAnteil;
    }

    public float getGesundheit() {
        return gesundheit;
    }

    public float getProduktivitaet() {
        return produktivitaet;
    }

    public float getSchutz() {
        return schutz;
    }

    public Enums.EnumKoerperteil getSchuetztKoeperteil() {
        return schuetztKoeperteil;
    }

    public boolean isHerstellbar() {
        return herstellbar;
    }

    public AssignmentHerstellen getMusterAuftragHerstellen() { // todo Das ist ja eigentlich final. Also irritieren die getter an dieser stelle
        return musterAuftragHerstellen;
    }

    public boolean isAbbaubar() {
        return abbaubar;
    }

    public float getAbbauDauer() {
        return abbauDauer;
    }

    public float[] getAbbauSchadenArray() {
        return abbauSchadenArray;
    }

    public float getNachwachsenMenge_jahr() {
        return nachwachsenMenge_jahr;
    }

    public void setNachwachsenMenge_jahr(float nachwachsenMenge_jahr) {
        this.nachwachsenMenge_jahr = nachwachsenMenge_jahr;
    }

    public boolean isNachwachsbar() {
        return nachwachsbar;
    }
}
