package de.arnonym.strategiespiel.spielSchirm.info.forschung;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.departments.bildung.ForschungDepartmentComp;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.departments.bildung.ForschungAssignment;
import de.arnonym.strategiespiel.framework.werkzeuge.collections.id.IdObjekt;

public class Forschungsstand implements IdObjekt {
    public final int id;

    private boolean erforscht;
    private ForschungAssignment forschungAssignment_nl;
    private List<ForschungDepartmentComp> forscher = new ArrayList<>();

    public Forschungsstand(int id) {
        this.id = id;
        this.erforscht = true;
        this.forschungAssignment_nl = null;
    }

    public Forschungsstand(int id, ForschungAssignment forschungAssignment) {
        this.id = id;
        this.erforscht = false;
        this.forschungAssignment_nl = forschungAssignment;
    }

    // KOMPLEXE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public boolean issWirdErforscht() {
        return forscher != null;
    }

    // EINFACH GETTER UND SETTER //////////////////////////////////////////////////////////////////

    @Override
    public int gettId() {
        return id;
    }

    public ForschungAssignment getForschungAssignment() {
        if (erforscht) throw new IllegalStateException();
        return forschungAssignment_nl;
    }

    public void setForschungAuftragInfo(ForschungAssignment forschungAssignment) {
        if (erforscht) throw new IllegalStateException();
        this.forschungAssignment_nl = forschungAssignment;
    }

    public boolean isErforscht() {
        return erforscht;
    }

    public void setErforscht(boolean erforscht) {
        if (erforscht && forscher == null) throw new IllegalStateException();
        this.erforscht = erforscht;
    }

    public List<ForschungDepartmentComp> getForscher() {
        if (erforscht) throw new IllegalStateException();
        return new ArrayList<>(forscher);
    }

    public void addForscher(ForschungDepartmentComp forscher) {
        if (erforscht && forscher != null) throw new IllegalArgumentException();
        this.forscher.add(forscher);
    }

    public void removeForscher(ForschungDepartmentComp forscher) {
        if (erforscht && forscher != null) throw new IllegalArgumentException();
        this.forscher.remove(forscher);
    }
}
