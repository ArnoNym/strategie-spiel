package de.arnonym.strategiespiel.spielSchirm.balancing;

import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.departments.bildung.BildungAssignment;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.departments.bildung.BildungInfo;
import de.arnonym.strategiespiel.spielSchirm.arbeit.auftrag.Assignment;

public class IdsBildung {
    public static final int ID_BILDUNG_GRUNDSCHULE = 0;
    public static final float BILDUNG_GRUNDSCHULE = 1.25f;
    public static final int ID_BILDUNG_WEITERFUEHRENDE_SCHULE = 1;
    public static final float BILDUNG_WEITERFUEHRENDE_SCHULE = 2f;
    public static final int ID_BILDUNG_UNIVERSITAET = 2;
    public static final float BILDUNG_UNIVERSITAET = 3.5f;

    public static final BildungInfo BILDEN_INFO_GRUNDSCHULE = new BildungInfo(
            ID_BILDUNG_GRUNDSCHULE, "Elementary School", "TODO" /*TODO*/, EnumBildung.GRUNDSCHULE,
            new BildungAssignment(
                    0, BILDUNG_GRUNDSCHULE, // Produktivitaet bezieht sich auf den Lehrer
                    BILDUNG_GRUNDSCHULE, 0f,
                    10f));

    public static final BildungInfo BILDEN_INFO_WEITERFUEHRENDE_SCHULE = new BildungInfo(
            ID_BILDUNG_WEITERFUEHRENDE_SCHULE, "TODO" /*TODO*/, "TODO" /*TODO*/, EnumBildung.WEITERFUEHRENDE_SCHULE,
            new BildungAssignment(
                    BILDUNG_GRUNDSCHULE, BILDUNG_WEITERFUEHRENDE_SCHULE,
                    BILDUNG_WEITERFUEHRENDE_SCHULE, 0f,
                    15f));

    public static final BildungInfo BILDEN_INFO_UNIVERSITAET = new BildungInfo(
            ID_BILDUNG_UNIVERSITAET, "TODO" /*TODO*/, "TODO" /*TODO*/, EnumBildung.UNIVERSITAET,
            new BildungAssignment(
                    BILDUNG_WEITERFUEHRENDE_SCHULE, BILDUNG_UNIVERSITAET,
                    BILDUNG_UNIVERSITAET * 2, 0f,
                    20f));

    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////

    public enum EnumBildung {
        UNGEBILDET,
        GRUNDSCHULE,
        WEITERFUEHRENDE_SCHULE,
        UNIVERSITAET
    }

    public static BildungInfo gettBildungInfo(float bildung) {
        if (bildung < BILDUNG_GRUNDSCHULE) return BILDEN_INFO_GRUNDSCHULE;
        if (bildung < BILDUNG_WEITERFUEHRENDE_SCHULE) return BILDEN_INFO_WEITERFUEHRENDE_SCHULE;
        return BILDEN_INFO_UNIVERSITAET;
    }

    public static EnumBildung gettBildungEnum(float bildung) {
        if (bildung >= BILDUNG_UNIVERSITAET) return EnumBildung.UNIVERSITAET;
        if (bildung >= BILDUNG_WEITERFUEHRENDE_SCHULE) return EnumBildung.WEITERFUEHRENDE_SCHULE;
        if (bildung >= BILDUNG_GRUNDSCHULE) return EnumBildung.GRUNDSCHULE;
        return EnumBildung.UNGEBILDET;
    }

    public static float gettBildungFloat(EnumBildung enumBildung) {
        switch (enumBildung) {
            case UNGEBILDET: return 0;
            case GRUNDSCHULE: return BILDUNG_GRUNDSCHULE;
            case WEITERFUEHRENDE_SCHULE: return BILDUNG_WEITERFUEHRENDE_SCHULE;
            case UNIVERSITAET: return BILDUNG_UNIVERSITAET;
            default: throw new IllegalStateException();
        }
    }
}
