package de.arnonym.strategiespiel.spielSchirm.balancing;

public class KonstantenFehlerAusgleich {
    public final static int SEKUNDEN_RUNDEN_NACHKOMMASTELLEN = 2; // Um Rundungsfehler beim zusammenrechnen von Sekunden zu umgehen (Weil Sekunden in float gespeichert werden)
}
