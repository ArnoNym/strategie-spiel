package de.arnonym.strategiespiel.spielSchirm.balancing;

import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.departments.gebiet.farm.FarmInfo;
import de.arnonym.strategiespiel.spielSchirm.gebaude.GebaudeInfo;
import de.arnonym.strategiespiel.spielSchirm.gebaude.Lager.LagerInfo;
import de.arnonym.strategiespiel.spielSchirm.gebaude.haus.HausInfo;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.departments.material.MaterialGebaudeInfo;
import de.arnonym.strategiespiel.spielSchirm.forschungUndWissen.schule.SchuleInfo;
import de.arnonym.strategiespiel.spielSchirm.Assets;
import de.arnonym.strategiespiel.framework.werkzeuge.textures.TextureHashMap;
import de.arnonym.strategiespiel.framework.werkzeuge.textures.IdTextureRegion;
import de.arnonym.strategiespiel.framework.werkzeuge.textures.Texture;
import de.arnonym.strategiespiel.framework.werkzeuge.collections.id.IdList;
import de.arnonym.strategiespiel.spielSchirm.Enums;

public class IdsGebaude {
    // Gebaude 10000-99999
    public static final int GEB_HAUS_EINFACH_ID = 10000;
    public static final int GEB_BER_SCHMIED_ID = 10001;
    public static final int GEB_LAGER_NAHRUNG_ID = 10002;
    public static final int GEB_LAGER_MATERIAL_ID = 10003;
    public static final int GEB_BER_FARM_ID = 10004;
    public static final int GEB_BER_KIRCHE_KLEIN_ID = 10005;
    public static final int GEB_BER_GRUNDSCHULE_ID = 10006;

    ///////////////////////////////////////////////////////////////////////////////////////////////

    private static final IdList<GebaudeInfo> musterList = create();

    private IdsGebaude() {
    }

    public static GebaudeInfo get(int id) {
        return musterList.gettDurchId(id);
    }

    public static List<GebaudeInfo> get() {
        return Collections.unmodifiableList(musterList);
    }

    public static List<Integer> gettIds() {
        List<Integer> idList = new ArrayList<>();
        for (GebaudeInfo gebaudeMuster : musterList) {
            idList.add(gebaudeMuster.id);
        }
        return idList;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////

    private static IdList<GebaudeInfo> create() {
        IdList<GebaudeInfo> gebaudeInfoList = new IdList<>();

        gebaudeInfoList.add(new HausInfo(GEB_HAUS_EINFACH_ID,
                "small house",
                "BESCHREIBUNG TODO",
                new TextureHashMap(new IdTextureRegion(
                        Assets.instance.gebaudeAssets.hausEinfach, true, -1, new Vector2(2, 2))),
                new Vector2(2, 2),
                new int[][]{{IdsGegenstaende.MAT_HOLZ_ID, 500}}, 10,
                null,
                1000,
                10) {
        });

        TextureHashMap textureHashMap = new TextureHashMap(new IdTextureRegion(
                Assets.instance.gebaudeAssets.lagerMaterial, true, -1, new Vector2(3, 3)));
        Texture tuerTexture = new IdTextureRegion(Assets.instance.gebaudeAssets.lagerMaterialTuer,
                false, 2, new Vector2(3, 3));
        textureHashMap.put(Enums.EnumAktion.EINTRETEN_IN_THIS, tuerTexture); // TODO funktioniert nicht
        textureHashMap.put(Enums.EnumAktion.AUSTRETEN_AUS_THIS, tuerTexture);
        gebaudeInfoList.add(new LagerInfo(GEB_LAGER_MATERIAL_ID,
                "material storage",
                "BESCHREIBUNG TODO",
                textureHashMap,
                new Vector2(3, 3),
                new int[][]{{IdsGegenstaende.MAT_HOLZ_ID, 500}}, 10,
                null,
                new int[]{IdsGegenstaende.MAT_HOLZ_ID, IdsGegenstaende.MAT_PROD_WERKZEUG_EINFACH_ID,
                        IdsGegenstaende.MAT_PROD_KLEIDUNG_EINFACH_ID},
                10000) {
        });

        textureHashMap = new TextureHashMap(new IdTextureRegion(
                Assets.instance.gebaudeAssets.berufSchmied, true, -1, new Vector2(3, 3)));
        textureHashMap.put(Enums.EnumAktion.EINTRETEN_IN_THIS, tuerTexture);
        textureHashMap.put(Enums.EnumAktion.AUSTRETEN_AUS_THIS, tuerTexture);
        gebaudeInfoList.add(new MaterialGebaudeInfo(GEB_BER_SCHMIED_ID, // todo herstellgeschwindigkeit modifikator
                "smith",
                "BESCHREIBUNG TODO",
                textureHashMap,
                new Vector2(3, 3),
                new int[][]{{IdsGegenstaende.MAT_HOLZ_ID, 500}}, 10,
                null,
                2,
                new int[]{IdsGegenstaende.MAT_PROD_WERKZEUG_EINFACH_ID},
                10000) {
        });

        textureHashMap = new TextureHashMap(new IdTextureRegion(
                Assets.instance.gebaudeAssets.berufFarmGross, true, -1, new Vector2(5, 5)));
        gebaudeInfoList.add(new FarmInfo(GEB_BER_FARM_ID,
                "huge farm",
                "BESCHREIBUNG TODO",
                textureHashMap,
                new Vector2(5, 5),
                new int[][]{{IdsGegenstaende.MAT_HOLZ_ID, 1000}}, 20,
                null,//todo ausklammern new ForschungAuftragInfo(1, 0, 10f, new int[][]{{IdsGegenstaende.MAT_HOLZ_ID, 200}}),
                2,
                2,
                2,
                2,
                5,
                new int[]{IdsRessourcen.BAUM_TANNE_ID, IdsRessourcen.STRAUCH_BEEREN_ID}) {
        });

        textureHashMap = new TextureHashMap(new IdTextureRegion(
                Assets.instance.gebaudeAssets.schuleKlein, true, -1, new Vector2(3, 3)));
        gebaudeInfoList.add(new SchuleInfo(GEB_BER_GRUNDSCHULE_ID,
                "elementary school",
                "BESCHREIBUNG TODO",
                textureHashMap,
                new Vector2(4, 4),// todo 3, 3
                new int[][]{{IdsGegenstaende.MAT_HOLZ_ID, 1000}},
                20,
                null,
                1,
                new int[]{GEB_BER_FARM_ID},
                1000,
                1,
                30,
                IdsBildung.BILDEN_INFO_GRUNDSCHULE) {
        });

        return gebaudeInfoList;
    }
}
