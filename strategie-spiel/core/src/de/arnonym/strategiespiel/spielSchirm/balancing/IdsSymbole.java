package de.arnonym.strategiespiel.spielSchirm.balancing;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

import de.arnonym.strategiespiel.spielSchirm.Assets;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.TextureUtils;

public class IdsSymbole {
    public static final float oneSymbolWidth
            = Assets.instance.spielweltSymboleAssets.statischKeinPfad.getRegionWidth();
    public static final float height
            = Assets.instance.spielweltSymboleAssets.statischKeinPfad.getRegionWidth();
    public static final int maxPerRow = 5;

    public static final StatSymbol SYMBOL_NICHT_GENUG_INPUT = new StatSymbol(
            Assets.instance.spielweltSymboleAssets.statischKeinInput,
            "Input is missing!",
            "There isn't enough input in this building and for sale."
    );
    public static final StatSymbol SYMBOL_NICHT_GENUG_GELD = new StatSymbol(
            Assets.instance.spielweltSymboleAssets.statischKeinGeld,
            "No money!",
            "The owner of this building doesn't have enough money to buy more Input."
    );
    public static final StatSymbol SYMBOL_NICHT_GENUG_OUTPUT_KAEUFER = new StatSymbol(
            Assets.instance.spielweltSymboleAssets.statischKeinOutput,
            "No buyer!",
            "No one wants to buy the product! Other sellers might offer the product siginifanctly" +
                    " cheaper oder the market may need some time to find a fair price."
    );
    public static final StatSymbol SYMBOL_VERHUNGERT = new StatSymbol(
            Assets.instance.spielweltSymboleAssets.statischKeinEssen,
            "Starving!",
            "The villager doesn't have enough food as well as there is no way to toList any."
    );
    public static final StatSymbol SYMBOL_OBDACHLOS = new StatSymbol(
            Assets.instance.spielweltSymboleAssets.statischKeinZuhause,
            "Homeless!",
            "The villager doesn't have a save place to sleep."
    );
    public static final StatSymbol SYMBOL_KEIN_PFAD = new StatSymbol(
            Assets.instance.spielweltSymboleAssets.statischKeinPfad,
            "No Path!",
            "At least one villager can't reach this building!"
    );

    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////

    public static class StatSymbol {
        public final TextureRegion textureRegion;
        public final String name;
        public final String beschreibung;
        private final Vector2 position;

        private StatSymbol(TextureRegion textureRegion, String name, String beschreibung) {
            this(textureRegion, name, beschreibung, null);
        }

        private StatSymbol(TextureRegion textureRegion, String name, String beschreibung,
                           Vector2 position) {
            this.textureRegion = textureRegion;
            this.name = name;
            this.beschreibung = beschreibung;
            this.position = position;
        }

        public StatSymbol init(Vector2 position) {
            return new StatSymbol(textureRegion, name, beschreibung, position);
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////

        public void render(SpriteBatch spriteBatch) {
            TextureUtils.zeichneTexturRegion(spriteBatch, textureRegion, position, false);
        }
    }
}
