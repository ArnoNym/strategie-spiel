package de.arnonym.strategiespiel.spielSchirm.balancing;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

import de.arnonym.strategiespiel.spielSchirm.Assets;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.GeometryUtils;
import de.arnonym.strategiespiel.framework.werkzeuge.textures.IdTextureRegion;
import de.arnonym.strategiespiel.framework.werkzeuge.textures.Texture;

/**
 * Created by LeonPB on 07.04.2018.
 */

public class KonstantenOptik {
    public static final boolean SCHNELLSTART = true; //zB Baume werden im Render Array nicht exakt positioniert

    public static final Color HINTERGRUND_FARBE = Color.SKY;
    public static final float VIEWPORT_GROESSE = 160*3f; //120
    public static final float OVERLAY_VIWEPORT_GROESSE = 160*6f; //120

    // ASSETS /////////////////////////////////////////////////////////////////////////////////////
    public static final String TEXTUREN_ATLAS = "images/spielTextureatlas.pack.atlas";
    public static TextureRegion baugrubeTextureRegionAuswaehlen(Vector2 positionBlaupauseMod) {
        if (positionBlaupauseMod.x == 2 && positionBlaupauseMod.y == 2) {
            return Assets.instance.gebaudeAssets.baugrube2x2;
        } else if (positionBlaupauseMod.x == 3 && positionBlaupauseMod.y == 3) {
            return Assets.instance.gebaudeAssets.baugrube3x3;
        } else if (positionBlaupauseMod.x == 4 && positionBlaupauseMod.y == 4) {
            return Assets.instance.gebaudeAssets.baugrube4x4;
        } else if (positionBlaupauseMod.x == 5 && positionBlaupauseMod.y == 5) {
            return Assets.instance.gebaudeAssets.baugrube5x5;
        }
        throw new IllegalArgumentException("Keine passende Baugrube vorhanden!");
    }
    public static Texture gettBaugrubeTexture(Vector2 groesseInZellen) {
        TextureRegion textureRegion;
        if (groesseInZellen.x == 2 && groesseInZellen.y == 2) {
            textureRegion = Assets.instance.gebaudeAssets.baugrube2x2;
        } else if (groesseInZellen.x == 3 && groesseInZellen.y == 3) {
            textureRegion = Assets.instance.gebaudeAssets.baugrube3x3;
        } else if (groesseInZellen.x == 4 && groesseInZellen.y == 4) {
            textureRegion = Assets.instance.gebaudeAssets.baugrube4x4;
        } else if (groesseInZellen.x == 5 && groesseInZellen.y == 5) {
            textureRegion = Assets.instance.gebaudeAssets.baugrube5x5;
        } else {
            throw new IllegalArgumentException("Keine passende Baugrube vorhanden!");
        }
        return new IdTextureRegion(textureRegion, true, -1, groesseInZellen);
    }
    public static final Vector2 RESSOURCEN_FUNDAMENT = new Vector2(1, 1);
    public static final Vector2 BODEN_STAPEL_FUNDAMENT = new Vector2(1, 1);
    public static final Vector2 DORFBEWOHNER_FUNDAMENT = new Vector2(1, 1);
    public static final Vector2 ZELLE_FUNDAMENT = new Vector2(1, 1);

    // TILED //////////////////////////////////////////////////////////////////////////////////////
    public static final String TILED_MAP_NAME = "tiledMap_Level_2.tmx";// "tiledMap_Level_1.tmx" oder "tiledMap_Level_2.tmx";
    public static final String TILED_LAYER_NAME_SPAWNEN = "spawnen";
    public static final String TILED_LAYER_NAME_BODEN = "boden";
    public static final String TILED_TILESET_BODEN = "tileset_Boeden";
    public static final int[] LAYER_RENDERN = new int[]{0};
    public static final int[] LAYER_RENDERN_MIT_GRID = new int[]{0, 1};
    public static final int TILE_ISO_WIDTH = 64; //Groesse wie sie gemalt wurden
    public static final int TILE_ISO_HEIGHT = 32; //Groesse wie sie gemalt wurden
    public static final int TILE_ORTO_WIDTH_AND_HEIGHT = TILE_ISO_WIDTH; //Macht die Karte in Weltpixeln groesser
    public static final float TILE_ORTO_UND_ISOXACHSE_DIAGONAL = (int) GeometryUtils.hypothenuse(64, 32); //ISOXACHSE ist ein Begriff und beschreibt, dass es in der Isometrischen Sicht der Strecke auf der x-Achse entspricht
    public static final float TILE_ISOYACHSE_DIAGONAL = TILE_ORTO_UND_ISOXACHSE_DIAGONAL / 2;

    // HUD / UI ///////////////////////////////////////////////////////////////////////////////////
    public static final float ZEIT_DOUBLE_TAP = 0.8f;
    public static final float ZEIT_BIS_MOVED = 0.1f; // Zeit bis sich die Ui bei gedrueckter Maus bewegt. Damit beim klicken eines Knopfes nicht ausversehen etwas verschoben wird

    public static final String SKIN_PFAD = "skin-kenney-pixel/skin.json"; //skin-rusty-robot/rusty-robot-ui.json
    public static final int AUSWAHLKASTEN_ECKEN_BREITE = 5;
    public static final int AUSWAHLKASTEN_ECKEN_HOEHE = 5;
    public static final int DICKER_RAND_fensterA1_GROESSE = 5;
    public static final int DUENNER_RAND_fensterA2_GROESSE = 5;

    // CHAT-LOG //
    public static final float CHAT_MESSAGE_ANZEIGE_DAUER = 3;

    // KOSMETISCH (AUFSTEIGENDE SYMBOLE, TUEREN, SCHORNSTEINE etc) //
    public static final float AUFSTEIGENDE_SYMBOLE_DAUER = 4f;
    public static final float AUFSTEIGENDE_SYMBOLE_GESCHWINDIGKEIT = 35f;

    public static final float TUER_OFFEN_DAUER = 0.35f;

    // KAMERA /////////////////////////////////////////////////////////////////////////////////////
    public static final float VERFOLGUNGS_KAMERA_MAUSRAD_SCOLL_SPEED = 0.2f;
    public static final float VERFOLGUNGS_KAMERA_BEWEGUNGSGESCHWINDIGKEIT = 5*720;
    public static final float VERFOLGUNGS_KAMERA_MAUS_RAND_SCROLLEN_BREITE_PROZENT = 0.05f;
    public static final float VERFOLGUNGS_KAMERA_MAUS_RAND_SCROLLEN_BEWEGUNGSGESCHWINDIGKEIT_MODIFIKATOR = 0.6f;
}
