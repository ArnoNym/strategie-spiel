package de.arnonym.strategiespiel.spielSchirm.balancing;

public class KonstantenUpdatePausenDauer {
    public final static float FARM_GEBIET_UPDATE_PAUSE = 1;
    public final static float BERUFE_MATCHER_UPDATE_PAUSE = 1;
    public final static float SCHULE_MATCHER_UPDATE_PAUSE = 1;
    public final static float WOHNORT_MATCHER_UPDATE_PAUSE = 1;
}
