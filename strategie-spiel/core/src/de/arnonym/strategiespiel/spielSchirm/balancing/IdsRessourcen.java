package de.arnonym.strategiespiel.spielSchirm.balancing;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.departments.gebiet.RessourcenAssignment;
import de.arnonym.strategiespiel.spielSchirm.arbeit.auftrag.Assignment;
import de.arnonym.strategiespiel.spielSchirm.umwelt.ressourcen.RessourceInfo;
import de.arnonym.strategiespiel.spielSchirm.umwelt.ressourcen.stufen.primaer.PWachstumRessourceInfo;
import de.arnonym.strategiespiel.spielSchirm.umwelt.ressourcen.stufen.primaer.PWachstumInfo;
import de.arnonym.strategiespiel.spielSchirm.umwelt.ressourcen.stufen.sekundaer.SWachstumInfo;
import de.arnonym.strategiespiel.spielSchirm.umwelt.ressourcen.stufen.sekundaer.SWachstumRessourceInfo;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.vorstufe.EntityGegInfo;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.vorstufe.EntityGegInfoList;
import de.arnonym.strategiespiel.spielSchirm.zahlen.schaden.MusterSchaden;
import de.arnonym.strategiespiel.spielSchirm.Assets;
import de.arnonym.strategiespiel.spielSchirm.Enums;
import de.arnonym.strategiespiel.framework.werkzeuge.textures.TextureHashMap;
import de.arnonym.strategiespiel.framework.werkzeuge.textures.IdTextureRegion;

public class IdsRessourcen {
    /** Damit Tileset-Ids zu den korrekten Ressourcen-Ids fuehren! */
    private static final int FIRST_GRID = 19;

    public static final int BAUM_TANNE_ID = 0 + FIRST_GRID;
    public static final int STRAUCH_BEEREN_ID = 1 + FIRST_GRID;

    ///////////////////////////////////////////////////////////////////////////////////////////////

    private static final HashMap<Integer, RessourceInfo> idResOutInfoHashMap = new HashMap<>();
    static {
        idResOutInfoHashMap.put(BAUM_TANNE_ID, getTanne());
        idResOutInfoHashMap.put(STRAUCH_BEEREN_ID, getBeerenstrauch());
    }

    private IdsRessourcen() {
    }

    public static RessourceInfo get(int id) {
        RessourceInfo ressourceInfo = idResOutInfoHashMap.get(id);
        if (ressourceInfo == null) throw new IllegalArgumentException();
        return ressourceInfo;
    }

    public static List<RessourceInfo> get() {
        return new ArrayList<>(idResOutInfoHashMap.values());
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////

    public static final Assignment ANPFLANZEN_MUSTER_AUFTRAG = new Assignment(1, 1, 3,
            new MusterSchaden(0.001f, true, 0, 0, 0.1f, 0.2f, 0.7f));

    ///////////////////////////////////////////////////////////////////////////////////////////////

    private static PWachstumRessourceInfo getTanne() {
        float faellen_optimaleProdKopf = 0.8f;
        float faellen_optimaleProdKoerper = 1.3f;
        float faellen_schadenKopf_prozent = 0.1f;
        float faellen_schadenLunge_prozent = 0f;
        float faellen_schadenHaende_prozent = 0.2f;
        float faellen_schadenFuesse_prozent = 0.05f;
        float faellen_schadenSonstiges_prozent = 0.65f;
        float pflegen_optimaleProdKop = 0.8f;
        float pflegen_optimaleProdKoerper = 1.3f;
        float pflegen_schadenKopf_prozent = 0.1f;
        float pflegen_schadenLunge_prozent = 0f;
        float pflegen_schadenHaende_prozent = 0.2f;
        float pflegen_schadenFuesse_prozent = 0.05f;
        float pflegen_schadenSonstiges_prozent = 0.65f;

        TextureHashMap textureHashMap_jung = new TextureHashMap(
                new IdTextureRegion(Assets.instance.ressourcenAssets.tanneJung, true, -1,
                        KonstantenOptik.RESSOURCEN_FUNDAMENT));
        EntityGegInfoList entityGegInfoList_jung = new EntityGegInfoList();
        entityGegInfoList_jung.add(new EntityGegInfo(IdsGegenstaende.MAT_HOLZ_ID, 100, null));
        PWachstumInfo ressourceInfo_jung = new PWachstumInfo(textureHashMap_jung,
                entityGegInfoList_jung, 3 * KonstantenBalance.SEKUNDEN_PRO_JAHR);

        TextureHashMap textureHashMap_mittel = new TextureHashMap(
                new IdTextureRegion(Assets.instance.ressourcenAssets.tanneMittel, true, -1,
                        KonstantenOptik.RESSOURCEN_FUNDAMENT));
        EntityGegInfoList entityGegInfo_List_mittel = new EntityGegInfoList();
        entityGegInfo_List_mittel.add(new EntityGegInfo(IdsGegenstaende.MAT_HOLZ_ID, 500, null));
        PWachstumInfo ressourceInfo_mittel = new PWachstumInfo(
                textureHashMap_mittel, entityGegInfo_List_mittel,
                5 * KonstantenBalance.SEKUNDEN_PRO_JAHR);

        TextureHashMap textureHashMap_alt = new TextureHashMap(
                new IdTextureRegion(Assets.instance.ressourcenAssets.tanneAltGesund, true, -1,
                        KonstantenOptik.RESSOURCEN_FUNDAMENT));
        textureHashMap_alt.put(Enums.EnumAktion.PRIMAERMENGE_MITTEL, new IdTextureRegion(
                Assets.instance.ressourcenAssets.tanneAltGekippt, true, -1,
                KonstantenOptik.RESSOURCEN_FUNDAMENT));
        textureHashMap_alt.put(Enums.EnumAktion.PRIMAERMENGE_WENIG, new IdTextureRegion(
                Assets.instance.ressourcenAssets.tanneAltVergammeln, true, -1,
                KonstantenOptik.RESSOURCEN_FUNDAMENT));
        EntityGegInfoList entityGegInfo_List_alt = new EntityGegInfoList();
        entityGegInfo_List_alt.add(new EntityGegInfo(IdsGegenstaende.MAT_HOLZ_ID, 1000, null));
        PWachstumInfo ressourceInfo_alt = new PWachstumInfo(textureHashMap_alt,
                entityGegInfo_List_alt, 150 * KonstantenBalance.SEKUNDEN_PRO_JAHR);

        return new PWachstumRessourceInfo(
                BAUM_TANNE_ID,
                "fir",
                "TODO. Es ist eine Tanne!",
                null,
                new RessourcenAssignment(BAUM_TANNE_ID, faellen_optimaleProdKopf,
                        faellen_optimaleProdKoerper, 5,
                        new MusterSchaden(0.05f, true, faellen_schadenKopf_prozent,
                                faellen_schadenLunge_prozent, faellen_schadenHaende_prozent,
                                faellen_schadenFuesse_prozent, faellen_schadenSonstiges_prozent)),
                new RessourcenAssignment(BAUM_TANNE_ID, pflegen_optimaleProdKop,
                        pflegen_optimaleProdKoerper, 5,
                        new MusterSchaden(0.01f, true, pflegen_schadenKopf_prozent,
                                pflegen_schadenLunge_prozent, pflegen_schadenHaende_prozent,
                                pflegen_schadenFuesse_prozent, pflegen_schadenSonstiges_prozent)),
                0.01f,
                ressourceInfo_jung,
                ressourceInfo_mittel,
                ressourceInfo_alt);
    }

    private static SWachstumRessourceInfo getBeerenstrauch() {
        float faellen_optimaleProdKopf = 0.8f;
        float faellen_optimaleProdKoerper = 1.3f;
        float faellen_schadenKopf_prozent = 0.1f;
        float faellen_schadenLunge_prozent = 0f;
        float faellen_schadenHaende_prozent = 0.2f;
        float faellen_schadenFuesse_prozent = 0.05f;
        float faellen_schadenSonstiges_prozent = 0.65f;
        float pflegen_optimaleProdKop = 0.8f;
        float pflegen_optimaleProdKoerper = 1.3f;
        float pflegen_schadenKopf_prozent = 0.1f;
        float pflegen_schadenLunge_prozent = 0f;
        float pflegen_schadenHaende_prozent = 0.2f;
        float pflegen_schadenFuesse_prozent = 0.05f;
        float pflegen_schadenSonstiges_prozent = 0.65f;
        float ernten_optimaleProdKop = 0.8f;
        float ernten_optimaleProdKoerper = 1.3f;
        float ernten_schadenKopf_prozent = 0.1f;
        float ernten_schadenLunge_prozent = 0f;
        float ernten_schadenHaende_prozent = 0.2f;
        float ernten_schadenFuesse_prozent = 0.05f;
        float ernten_schadenSonstiges_prozent = 0.65f;

        TextureHashMap textureHashMap_jung = new TextureHashMap(
                new IdTextureRegion(Assets.instance.ressourcenAssets.beerenJung, true, -1,
                        KonstantenOptik.RESSOURCEN_FUNDAMENT));
        EntityGegInfoList entityGegInfoList_jung = new EntityGegInfoList();
        entityGegInfoList_jung.add(new EntityGegInfo(IdsGegenstaende.MAT_HOLZ_ID, 100, null)); // TODO Menge von 0 ohne Fehler beim Abbauen ernten etc ermoeglichen
        SWachstumInfo ressourceInfo_jung = new SWachstumInfo(textureHashMap_jung,
                entityGegInfoList_jung, 1 * KonstantenBalance.SEKUNDEN_PRO_JAHR);

        TextureHashMap textureHashMap_mittel = new TextureHashMap(
                new IdTextureRegion(Assets.instance.ressourcenAssets.beerenMittel, true, -1,
                        KonstantenOptik.RESSOURCEN_FUNDAMENT));
        EntityGegInfoList entityGegInfo_List_mittel = new EntityGegInfoList();
        entityGegInfo_List_mittel.add(new EntityGegInfo(IdsGegenstaende.MAT_HOLZ_ID, 100, null));
        SWachstumInfo ressourceInfo_mittel = new SWachstumInfo(textureHashMap_mittel,
                entityGegInfo_List_mittel, 2 * KonstantenBalance.SEKUNDEN_PRO_JAHR);

        TextureHashMap textureHashMap_alt = new TextureHashMap(
                new IdTextureRegion(Assets.instance.ressourcenAssets.beerenAltLeer, true, -1,
                        KonstantenOptik.RESSOURCEN_FUNDAMENT));
        textureHashMap_alt.put(Enums.EnumAktion.PRIMAERMENGE_MITTEL, new IdTextureRegion(
                Assets.instance.ressourcenAssets.tanneAltGekippt, true, -1,
                KonstantenOptik.RESSOURCEN_FUNDAMENT));
        textureHashMap_alt.put(Enums.EnumAktion.PRIMAERMENGE_WENIG, new IdTextureRegion(
                Assets.instance.ressourcenAssets.tanneAltVergammeln, true, -1,
                KonstantenOptik.RESSOURCEN_FUNDAMENT));
        textureHashMap_alt.put(Enums.EnumAktion.SEKUNDAERMENGE_WENIG, new IdTextureRegion(
                Assets.instance.ressourcenAssets.beerenAltHalbvoll, true, -1, // TODO neue Textur fuer mehr als null beeren erstellen
                KonstantenOptik.RESSOURCEN_FUNDAMENT));
        textureHashMap_alt.put(Enums.EnumAktion.SEKUNDAERMENGE_MITTEL, new IdTextureRegion(
                Assets.instance.ressourcenAssets.beerenAltHalbvoll, true, -1,
                KonstantenOptik.RESSOURCEN_FUNDAMENT));
        textureHashMap_alt.put(Enums.EnumAktion.SEKUNDAERMENGE_VOLL, new IdTextureRegion(
                Assets.instance.ressourcenAssets.beerenAltVoll, true, -1,
                KonstantenOptik.RESSOURCEN_FUNDAMENT));
        EntityGegInfoList entityGegInfo_List_alt = new EntityGegInfoList(500, true);
        entityGegInfo_List_alt.add(new EntityGegInfo(IdsGegenstaende.MAT_HOLZ_ID, 100, null));
        entityGegInfo_List_alt.add(new EntityGegInfo(IdsGegenstaende.NAH_BEEREN_ID, 0, null));
        SWachstumInfo ressourceInfo_alt = new SWachstumInfo(textureHashMap_alt,
                entityGegInfo_List_alt, 300 * KonstantenBalance.SEKUNDEN_PRO_JAHR);

        return new SWachstumRessourceInfo(
                STRAUCH_BEEREN_ID,
                "berry bush",
                "A bush full of berries!",
                null,
                new RessourcenAssignment(STRAUCH_BEEREN_ID,
                        faellen_optimaleProdKopf, faellen_optimaleProdKoerper, 3,
                        new MusterSchaden(0.05f, true, faellen_schadenKopf_prozent,
                                faellen_schadenLunge_prozent, faellen_schadenHaende_prozent,
                                faellen_schadenFuesse_prozent, faellen_schadenSonstiges_prozent)),
                new RessourcenAssignment(STRAUCH_BEEREN_ID,
                        pflegen_optimaleProdKop, pflegen_optimaleProdKoerper, 3,
                        new MusterSchaden(0.002f, true, pflegen_schadenKopf_prozent,
                                pflegen_schadenLunge_prozent, pflegen_schadenHaende_prozent,
                                pflegen_schadenFuesse_prozent, pflegen_schadenSonstiges_prozent)),
                new RessourcenAssignment(STRAUCH_BEEREN_ID,
                        ernten_optimaleProdKop, ernten_optimaleProdKoerper, 3,
                        new MusterSchaden(0.01f, true, ernten_schadenKopf_prozent,
                                ernten_schadenLunge_prozent, ernten_schadenHaende_prozent,
                                ernten_schadenFuesse_prozent, ernten_schadenSonstiges_prozent)),
                new Integer[]{IdsGegenstaende.MAT_HOLZ_ID},
                1,
                0.01f,
                ressourceInfo_jung,
                ressourceInfo_mittel,
                ressourceInfo_alt);
    }
}
