package de.arnonym.strategiespiel.spielSchirm.balancing;

import de.arnonym.strategiespiel.spielSchirm.zahlen.schaden.MusterSchaden;

/**
 * Created by LeonPB on 07.04.2018.
 */

public class KonstantenBalance {

    // SPIELER ////////////////////////////////////////////////////////////////////////////////////
    public static final int SPIELER_STARTGELD = 10000 * Konstanten.NACHKOMMASTELLEN;

    // ZEIT ///////////////////////////////////////////////////////////////////////////////////////
    public static final float UPDATE_PAUSE = 1; //Alle x Sekunden werden Sachen im Spiel geupdated bzw ausgefuehrt
    public static final float GROSSE_UPDATE_PAUSE = 2; //Alle x Sekunden werden groessere Sachen im Spiel geupdated bzw ausgefuehrt (zB Gehalt)
    public static final float SEKUNDEN_PRO_JAHR = 100;
    public static final float SEKUNDEN_PRO_MONAT = SEKUNDEN_PRO_JAHR / 2;

    // FAMILIE ////////////////////////////////////////////////////////////////////////////////////
    public static final int FAMIELIE_STARTGELD = 10000 * Konstanten.NACHKOMMASTELLEN;

    // DORFBEWOHNER ///////////////////////////////////////////////////////////////////////////////
    public static final int DORFBEWOHNER_NAHRUNG_GRUNDBEDARF_VERHUNGERN = 1 * Konstanten.NACHKOMMASTELLEN; // Schwelle bis zu der der DB unabhaengig vom Nutzen auffuellt
    public static final int DORFBEWOHNER_NAHRUNG_GRUNDBEDARF_SUCHEN_AB = 4 * Konstanten.NACHKOMMASTELLEN + DORFBEWOHNER_NAHRUNG_GRUNDBEDARF_VERHUNGERN; // Schwelle unter der der DB dem Spieler als verhungernt angezeigt wird
    public static final int DORFBEWOHNER_NAHRUNG_GRUNDBEDARF_AUFFUELLEN_BIS = 5 * Konstanten.NACHKOMMASTELLEN + DORFBEWOHNER_NAHRUNG_GRUNDBEDARF_SUCHEN_AB; // Schwelle bis zu der der DB unabhaengig vom Nutzen auffuellt
    public static final float AUSBEUTEN_GESCHWINDIGKEIT = 2;

    public static final float DORFBEWOHNER_DNA_BEWEGUNGSGESCHWINDIGKEIT = 280;
    public static final int DORFBEWOHNER_DNA_SCHLAFEN_AUFFUELL_GESCHWINDIGKEIT = 5;
    public static final int DORFBEWOHNER_SCHLAFEN_GEHEN = 5;
    public static final int DORFBEWOHNER_SCHLAFEN_MAX = DORFBEWOHNER_SCHLAFEN_GEHEN + 5;

    public static final int DORFBEWOHNER_PLATZ = 1000;
    public static final int DORFBEWOHNER_STANDARD_MAX_ALTER = 80;

    public static final float DORFBEWOHNER_PAUSE_KONSUMIEREN = 3f;
    public static final float DORFBEWOHNER_LERNEN_AM_STUECK = 3f;

    public static final float[] SCHADEN_DURCH_LAUFEN = {0.001f, 0f, 0.01f, 0, 0.99f};
    public static final float[] SCHADEN_DURCH_BAUEN = {0.01f, 0.05f, 0.65f, 0.2f, 0.1f};

    public static final float DB_FITNESS_PROZENT_LEBENSZEIT_LAUFEN = 0.25f; //Wieviel der Lebenszeit mit Laufen verbacht werden muss damit der DB eine Fitness von 1 (maximum) hat

    public static final float DB_UMZUGSKOSTEN = 0.9f; //Wert mit dem der Nutzen eines neuen Hauses multipliziert wird
    public static final float DB_BERUF_WECHSEL_MULT = 0.9f; //Wert mit dem der Nutzen eines neuen Jobs multipliziert wird

    public static final float STRECKE_MAXIMAL_ZU_BODEN_STAPEL = 100000000;

    // BAUEN //
    public static final float BAUEN_AM_STUECK_MAX = 3f;
    public static final float BAUEN_PRO_MENGENGEWICHT_DAUER = 1f;

    // TASCHEN //
    public static final float TASCHE_KONSUMIEREN_PROZENT_PRO_PERSON = 0.05f;
    public static final int TASCHE_KONSUMIEREN_REST_MENGE = 50;
    public static final int TASCHE_KONSUMIEREN_MAX_MENGE_AUF_EINMAL = 100;

    public static final float GESUNDHEITSSCHADEN_ZU_GEGENSTAND_SCHADEN_MULT = 10;

    public static final int GEGENSTAND_LISTE_STANDARD_PLATZ = 500;
    public static final int TASCHE_PLATZ = 1000;
    public static final int TASCHE_PRODUKTIVITAET_MAX_MENGE_PRO_GEG = 300;

    // PARTNER UND KINDER //
    public static final int DB_MINDESTALTER_HEIRATEN_UND_KINDER = 14;
    public static final float DB_MAX_DISTANZ_ZU_ANDEREM_DB_FUER_VERLIEBEN_UND_KINDER = 100000000000f;//1000f;
    public static final float DB_PARTNER_KOMMT_IN_FRAGE_CHANCE = 0.1f;
    public static final float DB_KIND_WIRD_GEZEUGT_CHANCE = 0.1f;
    public static final float DB_SCHWANGERSCHAFT_BIS_KANN_WIEDER_SCHWANGER_WERDEN_DAUER = 3 / SEKUNDEN_PRO_MONAT;

    // ZIVIL OBJEKT ///////////////////////////////////////////////////////////////////////////////
    public static final float ZIVIL_OBJEKT_MENGEN_INTERAKTION_DAUER = 2;
    public static final float ZIVIL_OBJEKT_MIN_GEWINN_MULT = 1.1f;
    public static final int ZIVIL_OBJEKT_BETRACHTE_PERIODEN = 5; //Fuer Preise, durchschn. Kosten etc
    // GEBAUDE //
    public static final int GEB_BERUF_HERST_FABRIK_PRODUKT_MAX_MENGE = 10 * Konstanten.NACHKOMMASTELLEN;
    public static final float GEBAUDE_BAUEN_OPTIMALE_PROD_KOPF = 1f;
    public static final float GEBAUDE_BAUEN_OPTIMALE_PROD_KOERPER = 1.5f;
    public static final MusterSchaden GEBAUDE_BAUEN_MUSTER_SCHADEN = new MusterSchaden(1, true,
            0.2f, 0.05f, 0.3f, 0.1f, 0.35f);
    // BODEN STAPEL //
    public static final int BODEN_STAPEL_MAX_PLATZ = 1000;

    // ARBEIT /////////////////////////////////////////////////////////////////////////////////////
    public static final float GEHALT_GELD_ERSCHAFFEN_MULTIPLIKATOR = 1f;
    public static final float ARBEIT_ZEIT_AM_STUECK = 10f;
    // SCHULE //
    public static final float SCHULE_LEHRER_ARBEIT_ZEIT_AM_STUECK = 10f;
    public static final int SCHULE_ANZAHL_VORBEREITUNGEN_PRO_LEHRER_ARBEIT = 10;
    public static final int SCHULE_VORBEREITUNGEN_MAX_SPEICHERN_PRO_SCHUELER = 10;

    // PREISE /////////////////////////////////////////////////////////////////////////////////////
    public static final float PREISE_VERAENDERUNG_prozent = 0.2f;
    public static final float PREISE_VERAENDERUNG_PAUSE_DAUER_sek = 1 * SEKUNDEN_PRO_MONAT;

    public static final float PREISE_VERAENDERUNG_MAX_PROZENT_jahr = 1.001f;
    public static final float PREISE_VERAENDERUNG_MIN_PROZENT_jahr = 0.999f;
    public static final float PREISE_STILLSTAND_PROZENT_ABZUG_jahr = 0.0001f;

    // GEHALT /////////////////////////////////////////////////////////////////////////////////////
    public static final int START_GEHALT_sek = 10;

    // RESSOURCEN /////////////////////////////////////////////////////////////////////////////////
    public static final float RESSOURCEN_STERBEN_WAHRSCHEINLICHKEIT_PRO_JAHR = 0.003f; //TODO Umstellen, dass jede Res ihre eigene Wahrscheinlichkeit hat?!?
    public static final float RESSOURCEN_ANBAUEN_DAUER_sekunden = 2f; //TODO Umstellen, dass jede Res ihre eigene Wahrscheinlichkeit hat?!?
    // RESSOURCE NACHWACHSEN //
    public static final float RES_PFLEGE_ABBAUEN_PROZENT_sekunde = 0.001f;
    public static final float RES_PFLEGE_AUFBAUEN_PROZENT = 0.5f;

    // BODEN //////////////////////////////////////////////////////////////////////////////////////
    public static final float BODEN_WACHSTUM_MULT_MIN = 0.05f; //zB auf Stein (noetig einen mindestwert zu haben fuer die nachwachsen Pause in ResNachwachsen)
    public static final float BODEN_FEUCHTIGKEIT_PROZENT_BAUEN_KOLLISION = 0.75f; //zB auf Stein (noetig einen mindestwert zu haben fuer die nachwachsen Pause in ResNachwachsen)

    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////
}
