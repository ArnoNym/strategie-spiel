package de.arnonym.strategiespiel.spielSchirm.balancing;

import java.util.List;

import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dorfbewohner.DorfbewohnerMuster;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dorfbewohner.familie.FamilieMuster;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.vorstufe.EntityGegInfoList;
import de.arnonym.strategiespiel.spielSchirm.Enums;

public class IdsFamilien {
    public static final FamilieMuster FAMILIENMITGLIEDER_NYM;
    public static final FamilieMuster FAMILIENMITGLIEDER_PARKER;

    static {
        // konstantenKopf : 1, also 100 ist durchschnitt. 50 ist super dumm und 150 super klug
        // konstantenKoerper : 1, also 100 ist durchschnitt. 50 ist schwach und 150 stark
        // konstantenNutzenExponentNahrung : z.B. 0.9f
        // konstantenNutzenExponentSpielzeugtasche : z.B. 0.9f
        // konstantenNutzenExponentWohnen : z.B. 0.9f

        FAMILIENMITGLIEDER_NYM = new FamilieMuster("Nym", 1000,
                new DorfbewohnerMuster("Bob", false, 30, Enums.EnumStatusInFamilie.ELTERN, 1.3f, 1f, 0.7f, 0.7f, 0.7f, 0.7f, 0.9f, gettEntityGegInfoList(), 1.1f, 1),
                new DorfbewohnerMuster("Aria", true, 30, Enums.EnumStatusInFamilie.ELTERN, 1.2f, 0.9f, 0.85f, 0.7f, 0.7f, 0.9f, 0.5f, gettEntityGegInfoList(), 1.1f, 1.1f));

        FAMILIENMITGLIEDER_PARKER = new FamilieMuster("Parker", 1000,
                new DorfbewohnerMuster("Francisca", true, 30, Enums.EnumStatusInFamilie.ELTERN, 1, 1f, 0.9f, 0.7f, 0.7f, 0.9f, 0.8f, gettEntityGegInfoList(), 1, 1));
    }

    private static EntityGegInfoList gettEntityGegInfoList() {
        EntityGegInfoList entityGegInfoList = new EntityGegInfoList(
                KonstantenBalance.DORFBEWOHNER_PLATZ, false);
        List<Integer> idList = IdsGegenstaende.gettIds();
        entityGegInfoList.addAll(idList.toArray(new Integer[idList.size()]));
        return entityGegInfoList;
    }
}
