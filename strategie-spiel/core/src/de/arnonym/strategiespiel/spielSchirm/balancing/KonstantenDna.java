package de.arnonym.strategiespiel.spielSchirm.balancing;

public class KonstantenDna {
    public static final float MULT_MIN = 0.7f;
    public static final float MULT_MAX = 1.3f;

    // Produktivitaet
    public static final float PRODUKTIVITAET_KOPF_MIN = 0.5f;
    public static final float PRODUKTIVITAET_KOPF_MAX = 2;
    public static final float PRODUKTIVITAET_KOERPER_MIN = 0.5f;
    public static final float PRODUKTIVITAET_KOERPER_MAX = 2;

    // Nutzen Gegenstaende
    public static final float GEG_NUTZEN_MIN = 0;
    public static final float GEG_NUTZEN_MAX = Float.MAX_VALUE;

    // Nutzen Exponenten
    public static final float NUTZEN_EXPONENT_MIN = 0.01f;
    public static final float NUTZEN_EXPONENT_MAX = 0.99f;

    // Sonstiges
    public static final float BEWEGUNGSGESCHWINDIGKEIT_MIN = 50;
    public static final float BEWEGUNGSGESCHWINDIGKEIT_MAX = 500;
    public static final float SCHLAF_AUFFUELL_GESCHWINDIGKEIT_MIN = 2;
    public static final float SCHLAF_AUFFUELL_GESCHWINDIGKEIT_MAX = 10;
}
