package de.arnonym.strategiespiel.spielSchirm.balancing;

import com.badlogic.gdx.maps.tiled.TiledMapTileSet;

import java.util.ArrayList;
import java.util.List;

import de.arnonym.strategiespiel.spielSchirm.worldMap.Boden;

public class IdsBoden {
    // Ressourcen und RessourcenGegenstaende 0-999.
    // WICHTIG Id muss gleich sein mit der id des Tiles in der entsprechenden TiledMap! WICHTIG
    private static final int FIRST_GRID = 1;
    public static final int BODEN_GRAS_NORMAL_ID = 0 + FIRST_GRID;
    public static final int BODEN_GRAS_FEUCHT_ID = 3 + FIRST_GRID;

    private static final List<Boden> bodenListe = new ArrayList<>();

    public IdsBoden(TiledMapTileSet bodenTiledSet){
        bodenListe.add(new Boden(BODEN_GRAS_NORMAL_ID, 1, 0.5f, 0.5f, 1, "Normal grass!",
                bodenTiledSet.getTile(BODEN_GRAS_NORMAL_ID).getTextureRegion()));
        bodenListe.add(new Boden(BODEN_GRAS_FEUCHT_ID, 1, 0.6f, 0.5f, 1, "Juicy grass!",
                bodenTiledSet.getTile(BODEN_GRAS_FEUCHT_ID).getTextureRegion()));
    }

    public static Boden get(final float feuchtigkeit, final float temperatur) {
        Boden passenderBoden = null;
        double passenderBodenAbweichung = Double.MAX_VALUE;
        for (Boden b : bodenListe) {
            double abweichung = Math.pow(Math.abs(b.getFeuchtigkeit() - feuchtigkeit), 0.5)
                    + Math.pow(Math.abs(b.getTemperatur() - temperatur), 0.5);
            if (abweichung < passenderBodenAbweichung) {
                passenderBoden = b;
                passenderBodenAbweichung = abweichung;
            }
        }
        if (passenderBoden == null) {
            throw new IllegalStateException("Es sollte immer ein Boden gefunden werden!");
        }
        return passenderBoden;
    }

    public static Boden get(final int id) {
        for (Boden b : bodenListe) {
            if (b.getId() == id) {
                return b;
            }
        }
        throw new IllegalArgumentException("Ungueltige Id!");
    }
}
