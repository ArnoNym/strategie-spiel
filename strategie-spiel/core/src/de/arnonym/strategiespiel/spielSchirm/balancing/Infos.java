package de.arnonym.strategiespiel.spielSchirm.balancing;

import java.util.HashMap;

import de.arnonym.strategiespiel.spielSchirm.Enums;
import de.arnonym.strategiespiel.spielSchirm.arbeit.auftrag.Assignment;

public class Infos {
    protected static final HashMap<Integer, HashMap<Enums.EnumAlter, Assignment>> id_abbauenAssignment = new HashMap<>();
    protected static final HashMap<Integer, Assignment> id_erntenAssignment = new HashMap<>();
    protected static final HashMap<Integer, Assignment> id_pflegenAssignment = new HashMap<>();
    protected static final HashMap<Integer, Assignment> id_pflanzenAssignment = new HashMap<>();
    protected static final HashMap<Integer, String> id_name = new HashMap<>();
    protected static final HashMap<Integer, String> id_beschreibung = new HashMap<>();
    protected static final HashMap<Integer, Integer> id_platzVerbauch = new HashMap<>();
    protected static final HashMap<Integer, Float> id_eiweissAnteil = new HashMap<>();
    protected static final HashMap<Integer, Float> id_vitaminAnteil = new HashMap<>();
    protected static final HashMap<Integer, Float> id_kohlenhydrateGesundAnteil = new HashMap<>();
    protected static final HashMap<Integer, Float> id_kohlenhydrateUngesundAnteil = new HashMap<>();
    public static Assignment abbauenAssignment(int id, Enums.EnumAlter enumAlter) {
        return id_abbauenAssignment.get(id).get(enumAlter);
    }
    
}
