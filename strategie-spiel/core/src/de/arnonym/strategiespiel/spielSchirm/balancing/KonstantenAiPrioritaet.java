package de.arnonym.strategiespiel.spielSchirm.balancing;

public class KonstantenAiPrioritaet {
    public static final int HUNGER = 5;
    public static final int SCHLAFEN = 8;
    public static final int BAUARBEITER = 9;
    public static final int ARBEITNEHMER = 10;
    public static final int ABBAUEN = 12;
    public static final int ERNTEN = 13;
    public static final int AUFRAUMEN = 14;
}
