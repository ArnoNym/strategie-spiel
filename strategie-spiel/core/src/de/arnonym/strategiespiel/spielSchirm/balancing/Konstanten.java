package de.arnonym.strategiespiel.spielSchirm.balancing;

import java.util.logging.Level;

import de.arnonym.strategiespiel.spielSchirm.World;

public class Konstanten {
    public static final int NACHKOMMASTELLEN = 100;
    public static final long NACHKOMMASTELLEN_LONG = (long) NACHKOMMASTELLEN;

    public static int UPDATE_LIST_SIZE = 500; // TODO Dynamisch an FPS koppeln?!
    public static int UPDATE_LIST_PAUSE = 1;

    // DEVELOPER //////////////////////////////////////////////////////////////////////////////////

    public static boolean DM = true; // DEVELOPER MASTER
    public static final Level LOG_LEVEL = Level.ALL;

    public static boolean DEVELOPER_EXCEPTIONS = DM&& true; // Nach Testen des Spiels koennen diverse ueberpruefungen weggelassen werden um fps zu sparen

    public static boolean DEVELOPER_VISUELL = DM&& true;
    public static boolean DEVELOPER_VISUELL_STEP_SYSTEM_OUT = DM&& true;
    public static boolean DEVELOPER_VISUELL_NUR_10_FPS = DM&& false;
    public static boolean DEVELOPER_VISUELL_MANAGER_OUT = DM&& false;

    public static boolean DEVELOPER_GAMEPLAY_GEBAUDE_STARTMENGEN = DM&& true;
    public static boolean DEVELOPER_GAMEPLAY_ALLE_AUSSER_BAUGRUBE_EIGENES_GELD = DM&& true;
    public static boolean DEVELOPER_GAMEPLAY_ERSTES_GEBAUDE_SOFORT_GEBAUT = DM&& true;
    public static boolean developer_erstesGebaudeGebaut = false;
    public static boolean DEVELOPER_GAMEPLAY_IMMER_SOFORT_GEBAUT = DM&& false;

    @Deprecated
    public static final boolean DEVELOPER = false;
}
