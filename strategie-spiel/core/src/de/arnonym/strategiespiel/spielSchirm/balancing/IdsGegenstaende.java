package de.arnonym.strategiespiel.spielSchirm.balancing;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import de.arnonym.strategiespiel.spielSchirm.info.muster.GegInfo;
import de.arnonym.strategiespiel.spielSchirm.zahlen.schaden.MusterSchaden;
import de.arnonym.strategiespiel.framework.werkzeuge.collections.id.IdList;
import de.arnonym.strategiespiel.spielSchirm.Assets;
import de.arnonym.strategiespiel.spielSchirm.Enums;
import de.arnonym.strategiespiel.framework.werkzeuge.textures.IdTextureRegion;

public class IdsGegenstaende {
    /** Gegenstaende und  Werkzeuge 1000-9999 */

    public static final int NAH_BEEREN_ID = 1000;
    public static final int MAT_HOLZ_ID = 1001;
    public static final int GELD_ID = 1002;
    public static final int MAT_PROD_WERKZEUG_EINFACH_ID = 1003;
    public static final int MAT_WEIZEN_ID = 1004;
    public static final int MAT_PROD_KLEIDUNG_EINFACH_ID = 1005;

    ///////////////////////////////////////////////////////////////////////////////////////////////

    private static final IdList<GegInfo> musterList = create();

    private IdsGegenstaende() {
    }

    public static GegInfo get(int id) {
        return musterList.gettDurchId(id);
    }

    public static boolean contains(int id) {
        return musterList.contains(id);
    }

    public static List<GegInfo> get() {
        return Collections.unmodifiableList(musterList);
    }

    public static List<Integer> gettIds() {
        List<Integer> idList = new ArrayList<>();
        for (GegInfo gegInfo : musterList) {
            idList.add(gegInfo.id);
        }
        return idList;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////

    /** - Gegensteande (Geld wird in der Klasse Geld definiert)
     *
     *  - Wenn ein Geg nicht teilbar_init ist darf nirgendwo eine geteilte Menge auftauchen
     *  (zB nicht in IdsRessourcen)
     *
     *  InputArray:
     *  - Gegenstand Id und Menge die zum Herstellen des Gegenstandes benoetigt werden
     *  - new int[][]{{MAT_HOLZ_ID, 200}, {NAH_BEEREN_ID, 200}} */
    private static IdList<GegInfo> create() {
        IdList<GegInfo> gegInfoListe = new IdList<>();

        gegInfoListe.add(
                new GegInfo(NAH_BEEREN_ID, "raspberries", "Raspberries contain many vitamins.",
                        1000, 100, true, 0.1f,
                        new IdTextureRegion(Assets.instance.iconAssets.iconGegenstandBeeren))
                .constructNahrung(0.1f, 0.7f, 0.1f, 0.1f)
                .addAbbaubar(1, new float[] {0.01f, 0, 0, 1f, 0})
                .addNachwachsbar(10000));
        gegInfoListe.add(
                new GegInfo(MAT_WEIZEN_ID, "wheat", "Wheat contains many carbohydrates.",
                        1000, 100, true, 0.1f,
                        new IdTextureRegion(Assets.instance.iconAssets.iconFarm))
                .constructNahrung(0.1f, 0.1f, 0.7f, 0.1f)
                .addAbbaubar(1, new float[] {0.001f, 0, 0, 1f, 0})
                .addNachwachsbar(10000));

        gegInfoListe.add(
                new GegInfo(MAT_HOLZ_ID, "wooden slats", "Simple slats of wood.",
                        1000, 100, true, 0.1f,
                        new IdTextureRegion(Assets.instance.iconAssets.iconGegenstandHolt))
                .addAbbaubar(1, new float[] {0.05f, 0.1f, 0.3f, 0.5f, 0.1f}));

        gegInfoListe.add(
                new GegInfo(MAT_PROD_WERKZEUG_EINFACH_ID, "tools", "Simple tools to speed up " +
                        "physical work processes and protect the users hands.",
                        3000, 100, true, 0.1f,
                        new IdTextureRegion(Assets.instance.iconAssets.iconGegenstandWerkzeugeEinfach))
                .constructWerkzeug(2, 0.9f, Enums.EnumKoerperteil.HAENDE)
                .addHerstellbar(1, 1.5f, 2, new MusterSchaden(0.01f, true, 0.05f, 0.1f, 0.5f, 0.05f,
                        0.3f), new int[][]{{MAT_HOLZ_ID, 200}}, 100, null));
        gegInfoListe.add(
                new GegInfo(MAT_PROD_KLEIDUNG_EINFACH_ID, "clothing",
                        "Simple clothing for protection against body damage.",
                        3000, 100, true, 0.1f,
                        new IdTextureRegion(Assets.instance.iconAssets.iconGegenstandKleidungEinfach))
                .constructWerkzeug(2, 0.9f, Enums.EnumKoerperteil.SONSTIGES)
                .addHerstellbar(1, 1.5f, 2, new MusterSchaden(0.01f, true, 0f, 0.5f, 0f, 0.5f, 0f),
                        new int[][]{{MAT_HOLZ_ID, 200}}, 100, null));

        return gegInfoListe;
    }
}
