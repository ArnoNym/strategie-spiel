package de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.ausbeuten;

import de.arnonym.strategiespiel.framework.stp.Ai;
import de.arnonym.strategiespiel.framework.ecs.component.Comp;
import de.arnonym.strategiespiel.framework.stp.StpComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.ausbeuten.AusbeutbarComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.bewegung.BewegungComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.gegenstaende.GegenstaendeComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.EinkommenComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.werte.HabitalwerteComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.werte.ProduktivitaetComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.rendern.RenderComp;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.spielSchirm.bodenStapel.BodenStapelFactory;
import de.arnonym.strategiespiel.spielSchirm.ui.aufsteigendeSymbole.AufsteigendesSymbolFactory;
import de.arnonym.strategiespiel.framework.ecs.collections.EcsList;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.listSystems.privateListSystems.SysLager;
import de.arnonym.strategiespiel.framework.ecs.collections.Matcher;

public abstract class AusbeutenDorfAi extends Ai {
    public final AufsteigendesSymbolFactory aufsteigendesSymbolFactory;
    public final BodenStapelFactory bodenStapelFactory;
    public final SysLager sysLager;
    public final BewegungComp bewegungComp;
    public final EinkommenComp einkommenComp;
    public final GegenstaendeComp gegenstaendeComp;
    public final HabitalwerteComp habitalwerteComp;
    public final ProduktivitaetComp produktivitaetComp;

    protected final EcsList<? extends AusbeutbarComp> ausbeutbarComps;

    // TODO Das Dorf als Employer und dann diese Klassen sparen??

    public AusbeutenDorfAi(AufsteigendesSymbolFactory aufsteigendesSymbolFactory,
                           BodenStapelFactory bodenStapelFactory,

                           SysLager sysLager,

                           EcsList<? extends AusbeutbarComp> ausbeutbarComps,

                           Entity entity,

                           BewegungComp bewegungComp, EinkommenComp einkommenComp,
                           GegenstaendeComp gegenstaendeComp, HabitalwerteComp habitalwerteComp,
                           ProduktivitaetComp produktivitaetComp, RenderComp renderComp,
                           StpComp stpComp, int prioritaetId) {
        super(new Matcher[]{}, entity, stpComp, renderComp,
                new Comp[]{bewegungComp, einkommenComp, gegenstaendeComp, habitalwerteComp,
                        produktivitaetComp},
                prioritaetId);
        this.aufsteigendesSymbolFactory = aufsteigendesSymbolFactory;
        this.bodenStapelFactory = bodenStapelFactory;
        this.sysLager = sysLager;
        this.ausbeutbarComps = ausbeutbarComps;
        this.bewegungComp = bewegungComp;
        this.einkommenComp = einkommenComp;
        this.gegenstaendeComp = gegenstaendeComp;
        this.habitalwerteComp = habitalwerteComp;
        this.produktivitaetComp = produktivitaetComp;
    }
}
