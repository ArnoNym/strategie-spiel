package de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dorfbewohner.tauschen.plan.score;

import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dorfbewohner.tauschen.plan.PlanTauschen;
import de.arnonym.strategiespiel.framework.werkzeuge.collections.score.Score;

public class PlanTauschenScore extends Score {
    private final int positiv;
    private final int negativ;
    private int laufenKosten;

    public PlanTauschenScore(int positiv, int negativ, int laufenKosten) {
        super(-1);
        this.positiv = positiv;
        this.negativ = negativ;
        setLaufenKosten(laufenKosten);
    }

    public void setLaufenKosten(int laufenKosten) {
        this.laufenKosten = laufenKosten;
        setValue(PlanTauschen.gettProzentLohnt(positiv, negativ, laufenKosten));
    }

    public int getLaufenKosten() {
        return laufenKosten;
    }

    public int getPositiv() {
        return positiv;
    }

    public int getNegativ() {
        return negativ;
    }
}
