package de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dorfbewohner.tauschen.step;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

import de.arnonym.strategiespiel.framework.listener.StoryTeller;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.bewegung.BewegungComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.bewegung.BewegungszielComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.gegenstaende.BasisGegComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.gegenstaende.CompWirtschaftsdaten;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.GeldComp;
import de.arnonym.strategiespiel.spielSchirm.ui.aufsteigendeSymbole.AufsteigendesSymbolFactory;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.undPreis.IntEintragHashMap;
import de.arnonym.strategiespiel.framework.werkzeuge.collections.hashMaps.IntIntHashMap;
import de.arnonym.strategiespiel.spielSchirm.Assets;
import de.arnonym.strategiespiel.spielSchirm.Enums;

/**
 * Created by LeonPB on 29.06.2018.
 */

public class StepLegeIn extends StepTauschen {
    private CompWirtschaftsdaten compWirtschaftsdaten;
    public final GeldComp bezahleLieferantenCompGeld; // TODO Das ist bullshit. Die Tasks haben schon das Geld und wickeln selbst die gesamte Bezahlung ab

    public StepLegeIn(AufsteigendesSymbolFactory aufsteigendesSymbolFactory,

                      BewegungComp bewegungComp, BewegungszielComp bewegungszielComp,

                      BasisGegComp<?> nehmenAusBasisGegComp,
                      BasisGegComp<?> legenInBasisGegComp,

                      GeldComp bezahleLieferantenCompGeld,

                      Vector2 start_nl) {
        super(aufsteigendesSymbolFactory, bewegungComp, bewegungszielComp, nehmenAusBasisGegComp,
                legenInBasisGegComp, start_nl, bezahleLieferantenCompGeld);
        this.bezahleLieferantenCompGeld = bezahleLieferantenCompGeld;
    }

    public StepLegeIn(AufsteigendesSymbolFactory aufsteigendesSymbolFactory,

                      BewegungComp bewegungComp, BewegungszielComp bewegungszielComp,

                      BasisGegComp<?> nehmenAusBasisGegComp,
                      BasisGegComp<?> legenInBasisGegComp,

                      GeldComp bezahleLieferantenCompGeld,

                      IntIntHashMap mengenHashMap, Enums.EnumReservieren enumReservieren,

                      Vector2 start_nl) {
        super(aufsteigendesSymbolFactory, bewegungComp, bewegungszielComp, nehmenAusBasisGegComp,
                legenInBasisGegComp, mengenHashMap, enumReservieren, start_nl,
                bezahleLieferantenCompGeld);
        this.bezahleLieferantenCompGeld = bezahleLieferantenCompGeld;
    }

    public StepLegeIn(AufsteigendesSymbolFactory aufsteigendesSymbolFactory,

                      BewegungComp bewegungComp, BewegungszielComp bewegungszielComp,

                      BasisGegComp<?> nehmenAusBasisGegComp,
                      BasisGegComp<?> legenInBasisGegComp,

                      GeldComp nehmenVonGeldComp, GeldComp legenInGeldComp,

                      GeldComp bezahleLieferantenCompGeld,

                      IntEintragHashMap mengenPreisHashMap, boolean bereitsReserviert,
                      Vector2 start_nl) {
        super(aufsteigendesSymbolFactory, bewegungComp, bewegungszielComp, nehmenAusBasisGegComp,
                legenInBasisGegComp, nehmenVonGeldComp, legenInGeldComp, mengenPreisHashMap,
                bereitsReserviert, start_nl,
                (nehmenVonGeldComp == bezahleLieferantenCompGeld
                        || legenInGeldComp == bezahleLieferantenCompGeld)
                        ? new StoryTeller[]{} : new StoryTeller[]{bezahleLieferantenCompGeld}
                        /* Damit die Comp nicht mehrfach dem StorySupport hinzugefuegt wird! */
                        );
        this.bezahleLieferantenCompGeld = bezahleLieferantenCompGeld;
    }

    public StepLegeIn add(CompWirtschaftsdaten compWirtschaftsdaten) {
        this.compWirtschaftsdaten = compWirtschaftsdaten;
        return this;
    }

    @Override
    protected TextureRegion gettSymbolTextureRegion() {
        return Assets.instance.spielweltSymboleAssets.schwebeGueter;
    }

    public CompWirtschaftsdaten getCompWirtschaftsdaten() {
        return compWirtschaftsdaten;
    }
}
