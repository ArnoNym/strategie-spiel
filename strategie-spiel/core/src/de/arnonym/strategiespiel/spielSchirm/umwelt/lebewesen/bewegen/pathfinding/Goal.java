package de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.bewegen.pathfinding;

import com.badlogic.gdx.math.Vector2;

import de.arnonym.strategiespiel.spielSchirm.worldMap.Zelle;

public class Goal {
    private final Zelle zelle;
    private final Vector2 indizes;
    private final float breadthKosten; // Summe aller BodenKosten bis zu diesem Part

    public Goal(Zelle zelle, Vector2 indizes, float breadthKosten) {
        this.zelle = zelle;
        this.indizes = indizes;
        this.breadthKosten = breadthKosten;
    }

    public Zelle getZelle() {
        return zelle;
    }

    public Vector2 getIndizes() {
        return indizes;
    }

    public float getBreadthKosten() {
        return breadthKosten;
    }

    public float getGesamtKosten() {
        return breadthKosten;
    }
}
