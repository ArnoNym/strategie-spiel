package de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.werte;

import de.arnonym.strategiespiel.framework.ecs.component.Comp;
import de.arnonym.strategiespiel.spielSchirm.arbeit.auftrag.Assignment;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.gegenstaende.TascheComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dna.DbDnaComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.wohnen.EinwohnerComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.wohnen.MieterComp;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.taschen.NahrungTasche;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.taschen.WerkzeugTasche;

public class ProduktivitaetComp extends Comp {
    public final BildungComp bildungComp;
    public final DbDnaComp dbDnaComp;
    public final EinwohnerComp einwohnerComp;
    public final HabitalwerteComp habitalwerteComp;
    public final TascheComp tascheComp;

    private float produktivitaetKopf = 1; // Produktivitaet fuer Arbeit mit dem Kopf
    private float produktivitaetKoerper = 1; // Produktivitaet fuer Arbeit mit dem Koerper

    public ProduktivitaetComp(Entity entity, BildungComp bildungComp, DbDnaComp dbDnaComp,
                              EinwohnerComp einwohnerComp, HabitalwerteComp habitalwerteComp,
                              TascheComp tascheComp) {
        super(entity, bildungComp, dbDnaComp, habitalwerteComp, tascheComp);
        this.bildungComp = bildungComp;
        this.dbDnaComp = dbDnaComp;
        this.einwohnerComp = einwohnerComp;
        this.habitalwerteComp = habitalwerteComp;
        this.tascheComp = tascheComp;
    }

    public void update() {
        produktivitaetKopf = gettProduktivitaetKopf(bildungComp.getBildung());
        produktivitaetKoerper = gettProduktivitaetKoerper();
    }

    public float gettProduktivitaetKopf(float bildung) {
        float produktivitaetNahrung = gettProduktivitaetNahrung();

        return 0.5f + 0.5f * produktivitaetNahrung
                * dbDnaComp.dnaStrang.produktivtiaetKopf * bildung;
    }

    private float gettProduktivitaetKoerper() {
        float produktivitaetNahrung = gettProduktivitaetNahrung();
        float produktivitaetWerkzeug = gettProduktivitaetWerkzeug();

        return 0.5f + 0.5f * produktivitaetNahrung * produktivitaetWerkzeug
                * dbDnaComp.dnaStrang.produktivitaetKoerper * habitalwerteComp.getFitness();
    }

    private float gettProduktivitaetNahrung() {
        NahrungTasche nahrungTasche = tascheComp.getMasterTasche().getNahrungTasche_nl();
        if (nahrungTasche == null) {
            return 1;
        } else {
            return nahrungTasche.gettProduktivitaet();
        }
    }

    private float gettProduktivitaetWerkzeug() {
        WerkzeugTasche werkzeugTasche = tascheComp.getMasterTasche().getWerkzeugTasche_nl();
        if (werkzeugTasche == null) {
            return 1;
        } else {
            return werkzeugTasche.getProduktivitaet();
        }
    }

    private float gettProduktivitaetWohnort() {
        MieterComp mieterComp = einwohnerComp.getMieterComp();
        if (mieterComp == null) {
            return 1;
        } else {
            return mieterComp.getWohnort().aktuelleProduktivitaet();
        }
    }

    public float realProduktivitaet(Assignment assignment) {
        final float optProdKopf = assignment.optimaleProduktivitaetKopf;
        final float optProdKoerper = assignment.optimaleProduktivitaetKoerper;
        return realProduktivitaet(optProdKopf, optProdKoerper);
    }

    public float realProduktivitaet(float optProdKopf, float optProdKoerper) {
        final float dbProdKopf = produktivitaetKopf;
        final float dbProdKoerper = produktivitaetKoerper;

        float prodKopf, prodKoerper;

        if (optProdKopf == 0) {
            prodKopf = 1;
        } else if (dbProdKopf > optProdKopf) {
            prodKopf = (float) Math.pow(optProdKopf / dbProdKopf, 0.5f); //TODO Konstante // Ueberqualifikation soll sich nicht so stark auswirken
        } else {
            prodKopf = dbProdKopf / optProdKopf;
        }

        if (optProdKoerper == 0) {
            prodKoerper = 1;
        } else if (dbProdKoerper > optProdKoerper) {
            prodKoerper = (float) Math.pow(optProdKoerper / dbProdKoerper, 0.5f); //TODO Konstante // Ueberqualifikation soll sich nicht so stark auswirken
        } else{
            prodKoerper = dbProdKoerper / optProdKoerper;
        }

        float realProduktivitaet = prodKopf * prodKoerper;
        if (realProduktivitaet <= 0) {
            throw new IllegalStateException("optProdKopf: "+optProdKopf
                    +", optProdKoerper: " +optProdKoerper+", dbProdKopf: "+dbProdKopf
                    +", dbProdKoerper: "+dbProdKoerper+", realProduktivitaet: "+realProduktivitaet);
        }
        return realProduktivitaet;
    }

    // EINFACHE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public float getProduktivitaetKopf() {
        return produktivitaetKopf;
    }

    public float getProduktivitaetKoerper() {
        return produktivitaetKoerper;
    }
}
