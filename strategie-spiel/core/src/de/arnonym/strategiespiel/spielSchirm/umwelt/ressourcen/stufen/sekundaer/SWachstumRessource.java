package de.arnonym.strategiespiel.spielSchirm.umwelt.ressourcen.stufen.sekundaer;

import com.badlogic.gdx.math.Vector2;

import de.arnonym.strategiespiel.spielSchirm.World;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.herrschaft.EigentuemerComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.herrschaft.EigentumComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.ressourcen.RessourcenFactory;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.ausbeuten.FruechteComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.ressourcen.stufen.WachstumRessource;
import de.arnonym.strategiespiel.spielSchirm.Enums;

public class SWachstumRessource extends WachstumRessource<SWachstumRessourceInfo> {
    public SWachstumRessource(World world, RessourcenFactory ressourcenFactory, SWachstumRessourceInfo sekStufeResOutInfo, Enums.EnumResNwStufe alter, float alterAbgeschlossenProzent, EigentuemerComp eigentuemer, Vector2 position) {
        super(world, ressourcenFactory, sekStufeResOutInfo, alter, alterAbgeschlossenProzent, eigentuemer, position);

        FruechteComp fruechteComp = new FruechteComp(this, world.sysFreuchte, bewegungszielComp,
                eigentumComp, gegenstaendeComp, unbeweglichRenderComp, kollisionComp, pflegenComp,
                sekStufeResOutInfo.pflegeAssignment, sekStufeResOutInfo.wachsen_mod);
        altTransComp.addActivateComps(fruechteComp);
        todTransComp.addLoeschenComps(fruechteComp);
    }
}
