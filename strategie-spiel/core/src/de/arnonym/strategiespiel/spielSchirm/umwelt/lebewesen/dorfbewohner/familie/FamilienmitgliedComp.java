package de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dorfbewohner.familie;

import de.arnonym.strategiespiel.framework.ecs.component.Comp;
import de.arnonym.strategiespiel.framework.stp.StpComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.gegenstaende.TascheComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.VariabelGeldComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.rendern.RenderComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.wohnen.EinwohnerComp;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.spielSchirm.Enums;

public class FamilienmitgliedComp extends Comp {
    public final EinwohnerComp einwohnerComp;
    public final VariabelGeldComp compGeld;
    public final RenderComp renderComp;
    public final StpComp stpComp;
    public final TascheComp tascheComp;

    private Familie familie;
    private Enums.EnumStatusInFamilie statusInFamilie;

    public FamilienmitgliedComp(Entity entity, EinwohnerComp einwohnerComp,
                                VariabelGeldComp compGeld, RenderComp renderComp,
                                StpComp stpComp, TascheComp tascheComp,
                                Familie familie, Enums.EnumStatusInFamilie statusInFamilie) {
        super(entity, compGeld, renderComp, stpComp, tascheComp);
        this.einwohnerComp = einwohnerComp;
        this.compGeld = compGeld;
        this.renderComp = renderComp;
        this.stpComp = stpComp;
        this.tascheComp = tascheComp;
        this.statusInFamilie = statusInFamilie;

        familie.add(this);
    }

    public void setFamilie(Familie familie) {
        this.familie = familie;
    }

    public Enums.EnumStatusInFamilie getStatusInFamilie() {
        return statusInFamilie;
    }

    public void setStatusInFamilie(Enums.EnumStatusInFamilie statusInFamilie) {
        this.statusInFamilie = statusInFamilie;
    }

    public Familie getFamilie() {
        return familie;
    }
}
