package de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.bewegen.pathfinding;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Align;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import de.arnonym.strategiespiel.Strategiespiel;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.bewegung.BewegungszielComp;
import de.arnonym.strategiespiel.spielSchirm.ui.ZellenMarkierungRenderer;
import de.arnonym.strategiespiel.spielSchirm.worldMap.WorldMap;
import de.arnonym.strategiespiel.spielSchirm.worldMap.Zelle;
import de.arnonym.strategiespiel.spielSchirm.Assets;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.GeometryUtils;

public class Path {
    // Stufe Null == Diverse Vorbereitungen
    // Stufe Eins == die Kosten wurden berechnet
    // Stufe Zwei == fuer alle Zellen um diese Zelle wurden die Kosten berechnet
    // Stufe Drei == teil der besten Strecke

    public final WorldMap worldMap;
    public final BewegungszielComp bewegungszielComp;
    public final Vector2 startPosition;

    private final PartManager stufeEinsListe = new PartManager();
    private final HashMap<Zelle, Part> stufeZweiListe = new HashMap<>();
    private final List<Part> stufeDreiListe = new ArrayList<>();
    private Vector2 zielPosition; // Nur fuer render

    public Path(WorldMap worldMap, BewegungszielComp bewegungszielComp,
                Vector2 startPosition) {
        this.worldMap = worldMap;
        this.bewegungszielComp = bewegungszielComp;
        this.startPosition = startPosition;
    }

    public List<Part> create() {
        // Stufe Null /////////////////////////////////////////////////////////////////////////////

        // Start erstellen
        Vector2 startIndizes = GeometryUtils.gettIndizes(startPosition);
        Zelle startZelle = worldMap.gett((int) startIndizes.x, (int) startIndizes.y);
        Part startPart = new Part(null, startZelle, startIndizes, 0, 0);
        stufeEinsListe.add(startPart);

        // Goals erstellen
        List<Zelle> zieleZellenListe;
        LowestCostList<Goal> goalListe = new LowestCostList<>();
        zieleZellenListe = gettZielZellenListe(worldMap, bewegungszielComp);
        for (Zelle zielZelle : zieleZellenListe) {
            goalListe = erstelleGoal(goalListe, startIndizes, zielZelle);
        }
        /* Mit einfuehren von Comps ausgeklammert
        if (zielActor instanceof StatischesObjekt) {
            zieleZellenListe = gettZielZellenListe(worldMap, compBewegungsziel);
            for (Zelle zielZelle : zieleZellenListe) {
                goalListe = erstelleGoal(goalListe, startIndizes, zielZelle);
            }
        } else {
            Zelle zielZelle = worldMap.toList(zielActor.getPositionMitte());
            zieleZellenListe = new ArrayList<>();
            zieleZellenListe.handle(zielZelle);
            goalListe = erstelleGoal(goalListe, startIndizes, zielZelle);
        }*/

        if (issGoal(startPart, zieleZellenListe)) {
            for (Goal g : goalListe) {
                if (g.getIndizes().equals(startPart.getIndizes())) {
                    stufeDreiListe.add(startPart);
                    return stufeDreiListe;
                }
            }
            return new ArrayList<>();
        }

        // Stufe Eins und Zwei ////////////////////////////////////////////////////////////////////
        Part zielPart = erschliessePathZumActor(goalListe, zieleZellenListe);
        if (zielPart == null) {
            return null;
        }

        // Stufe Drei /////////////////////////////////////////////////////////////////////////////
        zielPosition = zielPart.getZelle().getPosition();
        waehlePathZumActor(zielPart);
        return stufeDreiListe;
    }

    // STUFE DREI /////////////////////////////////////////////////////////////////////////////////

    private void waehlePathZumActor(Part zielPart) {
        if (zielPart.getGesamtKosten() == 0) {
            return;
        }
        stufeDreiListe.add(zielPart);
        waehlePathZumActor(zielPart.getVorherigerPart());
    }

    // STUFE ZWEI /////////////////////////////////////////////////////////////////////////////////

    private Part erschliessePathZumActor(LowestCostList<Goal> zieleList,
                                         List<Zelle> zielZellenListe) {
        while (!zieleList.isEmpty()) {
            Goal goal = zieleList.gettLowestCostPart();
            Part zielPart = erschliessePathZumGoal(goal, zielZellenListe);
            if (zielPart != null) {
                return zielPart;
            }

            zieleList.remove(goal);
            for (Part s2 : stufeZweiListe.values()) {
                if (issGoal(s2, zielZellenListe)) {
                    return s2;
                }
            }
        }
        return null;
    }

    private boolean issGoal(Part part, List<Zelle> zielZellenListe) {
        return zielZellenListe.contains(part.getZelle());
    }

    private Part erschliessePathZumGoal(Goal goal, List<Zelle> zielZellenListe) {
        float goalMaxBreathKosten = Math.max(goal.getBreadthKosten() * 1.05f, //TODO Konstanten balancen
                goal.getBreadthKosten() + 50);

        while (!stufeEinsListe.isEmpty()) {
            Part s1 = stufeEinsListe.gett();
            if (issGoal(s1, zielZellenListe)) {
                return s1;
            }
            stufeEinsListe.remove(s1);
            bewerteUmliegende(goal, goalMaxBreathKosten, s1);
            stufeZweiListe.put(s1.getZelle(), s1);
        }
        return null;
    }

    // STUFE EINS /////////////////////////////////////////////////////////////////////////////////

    private void bewerteUmliegende(Goal goal, float goalMaxBreathKosten, Part umliegendUmPart) {
        Vector2 indizes = GeometryUtils.gettIndizes(umliegendUmPart.getZelle().getPosition());
        int x = (int) indizes.x;
        int y = (int) indizes.y;
        Zelle links = bewerteZelle(goal, goalMaxBreathKosten, umliegendUmPart, x - 1, y, null, null);
        Zelle oben = bewerteZelle(goal, goalMaxBreathKosten, umliegendUmPart, x, y + 1, null, null);
        if (links != null && oben != null) {
            bewerteZelle(goal, goalMaxBreathKosten, umliegendUmPart, x - 1, y + 1, links, oben);
        }
        Zelle rechts = bewerteZelle(goal, goalMaxBreathKosten, umliegendUmPart, x + 1, y, null, null);
        if (oben != null && rechts != null) {
            bewerteZelle(goal, goalMaxBreathKosten, umliegendUmPart, x + 1, y + 1, oben, rechts);
        }
        Zelle unten = bewerteZelle(goal, goalMaxBreathKosten, umliegendUmPart, x, y - 1, null, null);
        if (rechts != null && unten != null) {
            bewerteZelle(goal, goalMaxBreathKosten, umliegendUmPart, x + 1, y - 1, rechts, unten);
        }
        if (unten != null && links != null) {
            bewerteZelle(goal, goalMaxBreathKosten, umliegendUmPart, x - 1, y - 1, unten, links);
        }

    }

    private Zelle bewerteZelle(Goal goal, float goalMaxBreathKosten, Part umliegendUmPart,
                                 int zelleIndexX, int zelleIndexY, Zelle eckZelleLinks_nl,
                                 Zelle eckZelleRechts_nl) {
        Zelle z = worldMap.gett(zelleIndexX, zelleIndexY);
        if (z == null || z.isKollisionLaufen()) {
            return null;
        }

        if (stufeZweiListe.containsKey(z)) {
            return z; // Nicht null ist richtig weil es nur darum geht ob die Zelle frei ist
        }

        float breathKosten = gettBreathKosten(umliegendUmPart.getBreadthKosten(), z,
                eckZelleLinks_nl != null || eckZelleRechts_nl != null); /* 'Oder' weil die
                bewertete Zelle auch am Rand liegen koennte */
        if (breathKosten <= goalMaxBreathKosten) {
            Part vorhandenerPart = stufeEinsListe.gett(z);
            Vector2 zelleIndizes = new Vector2(zelleIndexX, zelleIndexY);

            float gesamtKosten = breathKosten + gettGreedyFirstKosten(goal.getIndizes(),
                    zelleIndizes);

            if (vorhandenerPart == null || gesamtKosten < vorhandenerPart.getGesamtKosten()) {
                stufeEinsListe.add(new Part(umliegendUmPart, z, zelleIndizes,
                        breathKosten, gesamtKosten, eckZelleLinks_nl, eckZelleRechts_nl));
            }
        }
        return z;
    }

    // STUFE NULL /////////////////////////////////////////////////////////////////////////////////

    private LowestCostList<Goal> erstelleGoal(LowestCostList<Goal> goalListe, Vector2 startIndizes,
                                              Zelle zielZelle) {
        if (zielZelle.isKollisionLaufen()) {
            return goalListe;
        }
        Vector2 indizes = GeometryUtils.gettIndizes(zielZelle.getPosition());
        float prioritaet = gettGreedyFirstKosten(startIndizes, indizes);
        goalListe.add(new Goal(zielZelle, indizes, prioritaet));
        return goalListe;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////

    private float gettBreathKosten(float ursprungKosten, Zelle kostenZelle, boolean eckzelle) {
        float addKosten = kostenZelle.gettLaufenMult();
        if (eckzelle) {
            addKosten *= 1.5f; /* Ein Schritt ueber Eck muss sich mehr lohnen als einen zB nach
            rechts und dann einen nach oben. Gleichzeitig aber weniger als zB nur einen nach rechts.
            Multipliziert damit es sich auf schwierigem Terrain nicht ploetzlich lohnt nurnoch
            ueber Eck zu laufen */
        }
        return ursprungKosten + addKosten;
    }

    private float gettGreedyFirstKosten(Vector2 zielIndizes, Vector2 kostenZelleIndizes) {
        return  Math.abs(zielIndizes.x - kostenZelleIndizes.x)
                + Math.abs(zielIndizes.y - kostenZelleIndizes.y);
    }

    private List<Zelle> gettZielZellenListe(WorldMap worldMap, BewegungszielComp bewegungszielComp) {
        /* Gett alle Zellen die sich um das Objekt herum befinden und die Zelle des Objekts falls
        es ein BodenStapel ist */

        List<Vector2> indizesListe = new ArrayList<>();
        int minX = Integer.MAX_VALUE;
        int maxX = - Integer.MAX_VALUE;
        int minY = Integer.MAX_VALUE;
        int maxY = - Integer.MAX_VALUE;
        for (Zelle z : worldMap.gettBetroffeneZellenListe(
                bewegungszielComp.compKollisionLaufen.positionComp.getPosition(),
                bewegungszielComp.compKollisionLaufen.isoGroesseComp.getGroesseInZellen())) {
            Vector2 indizes = GeometryUtils.gettIndizes(z.getPosition());
            indizesListe.add(indizes);
            int x = (int) indizes.x;
            if (x < minX) {
                minX = x;
            }
            if (x > maxX) {
                maxX = x;
            }
            int y = (int) indizes.y;
            if (y < minY) {
                minY = y;
            }
            if (y > maxY) {
                maxY = y;
            }
        }

        List<Zelle> zielZellenListe = new ArrayList<>();
        if (!bewegungszielComp.compKollisionLaufen.issKollision()) {
            zielZellenListe.add(worldMap.gett(
                    bewegungszielComp.compKollisionLaufen.positionComp.getPosition()));
        }
        for (Vector2 indizes : indizesListe) {
            int x = (int) indizes.x;
            int y = (int) indizes.y;

            Zelle z;
            if (x == minX) {
                z = worldMap.gett(x - 1, y);
                if (z != null && !z.isKollisionLaufen()) zielZellenListe.add(z);
            }
            if (x == maxX) {
                z = worldMap.gett(x + 1, y);
                if (z != null && !z.isKollisionLaufen()) zielZellenListe.add(z);
            }
            if (y == minY) {
                z = worldMap.gett(x, y - 1);
                if (z != null && !z.isKollisionLaufen()) zielZellenListe.add(z);
            }
            if (y == maxY) {
                z = worldMap.gett(x, y + 1);
                if (z != null && !z.isKollisionLaufen()) zielZellenListe.add(z);
            }
        }
        return zielZellenListe;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    //region RENDER
    ///////////////////////////////////////////////////////////////////////////////////////////////

    public void render(SpriteBatch spriteBatch, ZellenMarkierungRenderer zellenMarkierungRenderer) {
        for (Part p : stufeEinsListe.getList()) {
            zellenMarkierungRenderer.add((int) p.getIndizes().x, (int) p.getIndizes().y,
                    Assets.instance.tiledMapAssets.grid, true);
            renderText(spriteBatch, p);
        }
        for (Part p : stufeZweiListe.values()) {
            zellenMarkierungRenderer.add((int) p.getIndizes().x, (int) p.getIndizes().y,
                    Assets.instance.tiledMapAssets.durchsichtigWeiss, true);
            renderText(spriteBatch, p);
        }
        for (Part p : stufeDreiListe) {
            zellenMarkierungRenderer.add(p.getZelle().getPosition(),
                    Assets.instance.tiledMapAssets.durchsichtigRot, true);
        }
        zellenMarkierungRenderer.add(startPosition,
                Assets.instance.tiledMapAssets.entfernenMarkierung, true);
        if (zielPosition != null) {
            zellenMarkierungRenderer.add(zielPosition,
                    Assets.instance.tiledMapAssets.entfernenMarkierung, true);
        }
    }

    private void renderText(SpriteBatch spriteBatch, Part p) {
        Strategiespiel.bitmapFont.draw(spriteBatch,
                "("+ p.getBreadthKosten() + "," + p.getGesamtKosten()+")",
                p.getZelle().getPosition().x + 30,
                p.getZelle().getPosition().y + 25,
                0, Align.center, false);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    //endregion
    ///////////////////////////////////////////////////////////////////////////////////////////////
}
