package de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.bewegen.pathfinding;

import java.util.ArrayList;
import java.util.HashMap;

import de.arnonym.strategiespiel.spielSchirm.worldMap.Zelle;

public class PartManager {
    private final ArrayList<Part> list = new ArrayList<>();
    private final HashMap<Zelle, Part> hashMap = new HashMap<>();

    public PartManager() {

    }

    public void add(Part part) {
        if (list.isEmpty()) {
            privateAdd(0, part);
            return;
        }

        if (hashMap.containsKey(part.getZelle())) {
            list.remove(part);
        }

        float partGesamtKosten = part.getGesamtKosten();
        for (Part p : list) {
            if (p.getGesamtKosten() >= partGesamtKosten ) {
                int i = list.indexOf(p);
                privateAdd(i, part);
                return;
            }
        }
        privateAdd(list.size(), part);
    }

    private void privateAdd(int index, Part part) {
        list.add(index, part);
        hashMap.put(part.getZelle(), part);
    }

    public void remove(Part part) {
        getList().remove(part);
        hashMap.remove(part.getZelle());
    }

    public Part gett() { // Das mit den niedrigsten Gesamtkosten
        return getList().get(0);
    }

    public Part gett(Zelle zelle) {
        return hashMap.get(zelle);
    }

    // EINFACHE LISTEN ODER HASH_MAP IMMITATION ///////////////////////////////////////////////////

    public boolean isEmpty() {
        return list.isEmpty();
    }

    // EINFACHE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public ArrayList<Part> getList() {
        return list;
    }
}
