package de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.werte;

import de.arnonym.strategiespiel.framework.ecs.component.Comp;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;

public class BildungComp extends Comp {
    private float bildung = 1;

    public BildungComp(Entity entity) {
        super(entity);
    }

    public void addBildung(float bildung) {
        this.bildung += bildung;
    }

    // EINFACHE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public float getBildung() {
        return bildung;
    }
}
