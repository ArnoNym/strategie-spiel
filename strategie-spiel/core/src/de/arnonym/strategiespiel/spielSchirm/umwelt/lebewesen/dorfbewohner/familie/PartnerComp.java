package de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dorfbewohner.familie;

import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dna.DbDnaComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.vermehren.FemaleComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.vermehren.LebewesenVermehrenComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.werte.HabitalwerteComp;

public class PartnerComp extends LebewesenVermehrenComp {
    private final HabitalwerteComp habitalwerteComp;

    private PartnerComp partnerComp;

    public PartnerComp(PartnerMatcher partnerMatcher, Entity entity, FemaleComp femaleComp,
                       HabitalwerteComp habitalwerteComp, float prop_sekunde) {
        super(partnerMatcher, entity, femaleComp, -1, prop_sekunde);
        this.habitalwerteComp = habitalwerteComp;
    }

    public PartnerComp(PartnerMatcher partnerMatcher, Entity entity, DbDnaComp dbDnaComp,
                       HabitalwerteComp habitalwerteComp, float prop_sekunde) {
        super(partnerMatcher, entity, dbDnaComp, -1, prop_sekunde);
        this.habitalwerteComp = habitalwerteComp;
    }

    public float evaluate(PartnerComp partnerComp) {
        if (this.partnerComp.isFemale() == partnerComp.isFemale()) {
            return -1;
        }
        return 1; //todo
    }

    public boolean hasPartner() {
        return partnerComp != null;
    }

    public PartnerComp getPartnerComp() {
        return partnerComp;
    }

    public void setPartnerComp(PartnerComp partnerComp) {
        this.partnerComp = partnerComp;
    }
}
