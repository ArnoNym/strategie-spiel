package de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dorfbewohner.familie;

import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dorfbewohner.DorfbewohnerMuster;

public class FamilieMuster {
    public final String name;
    public final int geldMenge;
    public final DorfbewohnerMuster[] familienmitglieder;

    public FamilieMuster(String name, int geldMenge, DorfbewohnerMuster... familienmitglieder) {
        this.name = name;
        this.geldMenge = geldMenge;
        this.familienmitglieder = familienmitglieder;
    }
}
