package de.arnonym.strategiespiel.spielSchirm.umwelt.ressourcen.stufen.primaer;

import com.badlogic.gdx.math.Vector2;

import de.arnonym.strategiespiel.spielSchirm.World;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.herrschaft.EigentuemerComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.herrschaft.EigentumComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.ressourcen.RessourcenFactory;
import de.arnonym.strategiespiel.spielSchirm.umwelt.ressourcen.stufen.WachstumRessource;
import de.arnonym.strategiespiel.spielSchirm.Enums;

public class PWachstumRessource extends WachstumRessource<PWachstumRessourceInfo> {
    public PWachstumRessource(World world, RessourcenFactory ressourcenFactory, PWachstumRessourceInfo primStufeResOutInfo, Enums.EnumResNwStufe alter, float alterAbgeschlossenProzent, EigentuemerComp eigentuemer, Vector2 position) {
        super(world, ressourcenFactory, primStufeResOutInfo, alter, alterAbgeschlossenProzent, eigentuemer, position);
    }
}
