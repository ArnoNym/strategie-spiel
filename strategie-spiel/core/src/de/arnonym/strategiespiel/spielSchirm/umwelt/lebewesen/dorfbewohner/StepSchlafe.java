package de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dorfbewohner;

import com.badlogic.gdx.math.Vector2;

import de.arnonym.strategiespiel.framework.listener.StoryTeller;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.bewegung.BewegungComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.bewegung.BewegungszielComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dna.DbDnaComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.werte.HabitalwerteComp;
import de.arnonym.strategiespiel.spielSchirm.StepNGZ;
import de.arnonym.strategiespiel.spielSchirm.Enums;
import de.arnonym.strategiespiel.framework.werkzeuge.pause.DeltaPause;
import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenBalance;

/**
 * Created by LeonPB on 11.08.2018.
 */

public class StepSchlafe extends StepNGZ {
    public final DbDnaComp dbDnaComp;
    public final HabitalwerteComp habitalwerteComp;
    private DeltaPause schlafenPause;

    public StepSchlafe(BewegungComp bewegungComp, BewegungszielComp bewegungszielComp,
                       DbDnaComp dbDnaComp, HabitalwerteComp habitalwerteComp, Vector2 start_nl) {
        super(bewegungszielComp, new StoryTeller[]{}, bewegungComp, start_nl);
        this.dbDnaComp = dbDnaComp;
        this.habitalwerteComp = habitalwerteComp;
    }

    @Override
    protected Enums.EnumStpStatus versuchen_or_or() {
        return Enums.EnumStpStatus.OK;
    }

    @Override
    protected void failNachVersuchenVorAnfangen_or() {

    }

    @Override
    protected void anfangen_or() {
        schlafenPause = new DeltaPause(1, Enums.EnumPauseStart.JETZT);
    }

    @Override
    protected void failNachAnfangenVorDurchfuehren_or() {

    }

    @Override
    protected void durchfuehren_or(float delta) {
        if (schlafenPause.issVorbei(delta, true)) {
            habitalwerteComp.setSchlaf(Math.max(habitalwerteComp.getSchlaf()
                    + dbDnaComp.dnaStrang.schlafAuffuellGeschwindigkeit,
                    KonstantenBalance.DORFBEWOHNER_SCHLAFEN_MAX));
        }
    }

    @Override
    protected void failNachDurchfuehren_or() {

    }

    @Override
    public boolean pruefenObErledigt() {
        return habitalwerteComp.getSchlaf() >= KonstantenBalance.DORFBEWOHNER_SCHLAFEN_MAX;
    }

    @Override
    public boolean pruefenObFail() {
        return false;
    }

    @Override
    public void erledigt_or() {

    }

    @Override
    protected void reservierenRueckgaengig_or() {

    }
}
