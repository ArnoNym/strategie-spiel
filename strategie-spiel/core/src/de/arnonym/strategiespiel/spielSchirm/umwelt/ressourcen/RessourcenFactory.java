package de.arnonym.strategiespiel.spielSchirm.umwelt.ressourcen;

import com.badlogic.gdx.math.Vector2;

import java.util.concurrent.ThreadLocalRandom;

import de.arnonym.strategiespiel.spielSchirm.World;
import de.arnonym.strategiespiel.spielSchirm.balancing.IdsRessourcen;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.herrschaft.EigentuemerComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.herrschaft.EigentumComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.ressourcen.stufen.primaer.PWachstumRessourceInfo;
import de.arnonym.strategiespiel.spielSchirm.umwelt.ressourcen.stufen.primaer.PWachstumRessource;
import de.arnonym.strategiespiel.spielSchirm.umwelt.ressourcen.stufen.sekundaer.SWachstumRessourceInfo;
import de.arnonym.strategiespiel.spielSchirm.umwelt.ressourcen.stufen.sekundaer.SWachstumRessource;
import de.arnonym.strategiespiel.spielSchirm.Enums;

public class RessourcenFactory {
    private final World world;

    public RessourcenFactory(World world) {
        this.world = world;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////

    /** Auch Res die zB kein Alter brauchen koennen diese Methoden verwenden. Die Variablen werden
     * dann einfach nicht verwendet */

    public Entity createRandom(int id, Vector2 position) {
        return create(id, gettAlter(), ThreadLocalRandom.current().nextFloat(), null, position);
    }

    public Entity createNeu(int id, Vector2 position) {
        return create(id, Enums.EnumResNwStufe.JUNG, 0, null, position);
    }

    public Entity createRandom(int id, EigentuemerComp eigentuemer, Vector2 position) {
        return create(id, gettAlter(), ThreadLocalRandom.current().nextFloat(), eigentuemer, position);
    }

    public Entity createNeu(int id, EigentuemerComp eigentuemer, Vector2 position) {
        return create(id, Enums.EnumResNwStufe.JUNG, 0, eigentuemer, position);
    }

    private Entity create(int id, Enums.EnumResNwStufe alter, float alterAbgeschlossenProzent,
                          EigentuemerComp eigentuemer_nl, Vector2 position) {
        switch (id) {
            case IdsRessourcen.BAUM_TANNE_ID:
                return new PWachstumRessource(
                        world,
                        this,
                        (PWachstumRessourceInfo) IdsRessourcen.get(id),
                        alter,
                        alterAbgeschlossenProzent,
                        eigentuemer_nl,
                        position);
            case IdsRessourcen.STRAUCH_BEEREN_ID:
                return new SWachstumRessource(
                        world,
                        this,
                        (SWachstumRessourceInfo) IdsRessourcen.get(id),
                        alter,
                        alterAbgeschlossenProzent,
                        eigentuemer_nl,
                        position);
            default: throw new IllegalArgumentException("id: "+id);
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////

    private static Enums.EnumResNwStufe gettAlter() {
        return gettAlter(0.4f, 0.3f);
    }

    private static Enums.EnumResNwStufe gettAlter(float propAlt, float propMittel) {
        Enums.EnumResNwStufe alter;
        float randomFloat = ThreadLocalRandom.current().nextFloat();
        if (randomFloat > 1 - propAlt) {
            alter = Enums.EnumResNwStufe.JUNG;
        } else if (randomFloat > 1 - propAlt - propMittel) {
            alter = Enums.EnumResNwStufe.MITTEL;
        } else {
            alter = Enums.EnumResNwStufe.ALT;
        }
        return alter;
    }
}
