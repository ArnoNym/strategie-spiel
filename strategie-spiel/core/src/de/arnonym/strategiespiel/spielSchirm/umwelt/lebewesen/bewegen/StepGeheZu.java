package de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.bewegen;

import com.badlogic.gdx.math.Vector2;

import java.util.List;

import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.rendern.BeweglichRenderComp;
import de.arnonym.strategiespiel.framework.stp.Step;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.bewegung.BewegungComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.bewegung.BewegungszielComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.bewegen.pathfinding.Part;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.bewegen.pathfinding.Path;
import de.arnonym.strategiespiel.spielSchirm.worldMap.WorldMap;
import de.arnonym.strategiespiel.spielSchirm.worldMap.Zelle;
import de.arnonym.strategiespiel.spielSchirm.Enums;
import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenBalance;
import de.arnonym.strategiespiel.framework.werkzeuge.math.FinalVector2;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.GeometryUtils;

/**
 * Created by LeonPB on 25.06.2018.
 */

public class StepGeheZu extends Step {
    public final WorldMap worldMap;
    public final BewegungComp bewegungComp;
    private final Vector2 p;
    public final BewegungszielComp bewegungszielComp;

    private final FinalVector2 vermuteteStartPosition;
    private Zelle zielZelle;
    private Vector2 actualMovementSpeed = new Vector2(0, 0);
    private float gelaufenGesamtStrecke = 0;
    private float zwischenStrecke = 0;
    private float gelaufenZwischenStrecke = 0;
    private List<Part> path = null;

    public StepGeheZu(WorldMap worldMap, BewegungComp bewegungComp,
                      BewegungszielComp bewegungszielComp, Vector2 start_nl) {
        super(bewegungszielComp);
        this.worldMap = worldMap;
        this.bewegungComp = bewegungComp;
        this.p = bewegungComp.positionComp.position;
        this.bewegungszielComp = bewegungszielComp;

        if (start_nl == null) {
            this.vermuteteStartPosition = new FinalVector2(gettDebugPosition(p));
        } else {
            this.vermuteteStartPosition = new FinalVector2(gettDebugPosition(start_nl));
        }
    }

    // VERSUCHEN //////////////////////////////////////////////////////////////////////////////////

    @Override
    protected Enums.EnumStpStatus versuchen_or() {
        createPath(vermuteteStartPosition.toVector2());
        if (path == null) {
            return Enums.EnumStpStatus.FAIL_BEI_GEHE_ZU;
        }
        return Enums.EnumStpStatus.OK;
    }

    // ANFANGEN ///////////////////////////////////////////////////////////////////////////////////

    @Override
    protected void anfangen_or() {
        bewegungComp.austreten();

        if (vermuteteStartPosition.equals(p)) {
            System.out.println("- Step ::: StepGeheZu ::: Vorrausgeplanten Standort : Berechnung gespart!");
        } else {
            settToDebugPositionAndCreatePath();
            System.out.println("- Step ::: StepGeheZu ::: Vorrausgeplanten Standort : Berechnung NICHT gespart!");
        }

        settZwischenZiel(worldMap.gett(p), path.get(path.size() - 1));
    }

    // DURCHFUEHREN ///////////////////////////////////////////////////////////////////////////////

    @Override
    protected void durchfuehren_or(float delta) {
        laufe(delta);

        /*getDorfbewohner().erhoehStreckeGelaufen(delta
                * getDorfbewohner().getKonstantenBewegungsgeschwindigkeit());*/

        BeweglichRenderComp beweglichRenderComp = bewegungComp.beweglichRenderComp;
        beweglichRenderComp.preufeVisible();
        if (beweglichRenderComp.isVisible()) {
            beweglichRenderComp.pruefeIndex();
        }
    }

    private void laufe(float delta) {
        if (delta == 0) return;
        if (delta < 0) throw new IllegalArgumentException();

        float strecke = GeometryUtils.hypothenuse(
                delta * actualMovementSpeed.x,
                delta * actualMovementSpeed.y);
        float bisZielStrecke = zwischenStrecke - gelaufenZwischenStrecke;

        if (bisZielStrecke > strecke) {
            p.x += delta * actualMovementSpeed.x;
            p.y += delta * actualMovementSpeed.y / 2;

            addStrecke(strecke);

            return;
        } else {
            addStrecke(bisZielStrecke);
        }

        // ZwischenZiel erreicht
        int index = path.size() - 1;
        path.remove(index);

        // ZwischenZiel setzten
        if (path.isEmpty()) {
            return;
        }
        settZwischenZiel(zielZelle, path.get(index - 1));

        // Rest der Strecke laufen
        float kannNochStrecke = strecke - bisZielStrecke;
        laufe(kannNochStrecke / strecke * delta);
    }

    private void addStrecke(float strecke) {
        gelaufenGesamtStrecke += strecke;
        gelaufenZwischenStrecke += strecke;
    }

    // SETZTE BEWEGUNGSZIEL ///////////////////////////////////////////////////////////////////////

    private void settZwischenZiel(Zelle aktuelleZelle, Part zielPart) {
        zielZelle = zielPart.getZelle();
        gelaufenZwischenStrecke = 0;

        // Pruefe erreichbarkeit Ziel und Veraenderung des Weges
        Zelle eckZelleLinks = zielPart.getEckZelleLinks();
        Zelle eckZelleRechts = zielPart.getEckZelleRechts();
        if (zielZelle.isKollisionLaufen()
                || (eckZelleLinks != null && eckZelleLinks.isKollisionLaufen())
                || (eckZelleRechts != null && eckZelleRechts.isKollisionLaufen())
                || zielZelle.gettLaufenMult() != zielPart.getVerwendeteZellenKosten()) {
            settToDebugPositionAndCreatePath();
            if (path == null || path.isEmpty()) {
                return;
            }
            aktuelleZelle = worldMap.gett(p);
            zielZelle = path.get(path.size() - 1).getZelle();
        }

        // Pruefe am Ziel angekommen
        Vector2 zielPosition = zielZelle.getPosition();
        if (p.equals(zielPosition)) {
            zwischenStrecke = 0;
            actualMovementSpeed = new Vector2(0,0);
            return;
        }

        // Berechne ZwischenStrecke
        float ankathete = zielPosition.x - p.x;
        float gegenkathete = (zielPosition.y - p.y) * 2;
        zwischenStrecke = GeometryUtils.hypothenuse(ankathete, gegenkathete);

        // Berechne ActualMovementSpeed
        boolean xPositiv = ankathete >= 0;
        boolean yPositiv = gegenkathete > 0;
        float bewegungsgeschwindigkeit = bewegungComp.bewegungsgeschwindigkeit;

        actualMovementSpeed.x = (float) (Math.cos(Math.atan(gegenkathete / ankathete))
                * (xPositiv ? bewegungsgeschwindigkeit
                : (-bewegungsgeschwindigkeit)));
        actualMovementSpeed.y = (float) (Math.sin(Math.atan(gegenkathete / ankathete))
                * (xPositiv ? bewegungsgeschwindigkeit
                : (-bewegungsgeschwindigkeit)));

        float bodenVerlangsamung = (aktuelleZelle.gettLaufenMult() + zielZelle.gettLaufenMult())/2;
        actualMovementSpeed.x /= bodenVerlangsamung;
        actualMovementSpeed.y /= bodenVerlangsamung;

        // Guckrichtung festlegen todo
        Enums.EnumRichtung enumRichtung;
        boolean xLaengerAlsY = Math.abs(actualMovementSpeed.x) > Math.abs(actualMovementSpeed.y);
        if (xLaengerAlsY) {
            if (xPositiv) {
                enumRichtung = Enums.EnumRichtung.RECHTS;
            } else {
                enumRichtung = Enums.EnumRichtung.LINKS;
            }
        } else {
            if (yPositiv) {
                enumRichtung = Enums.EnumRichtung.OBEN;
            } else {
                enumRichtung = Enums.EnumRichtung.UNTEN;
            }
        }
        bewegungComp.beweglichRenderComp.add(
                Enums.EnumAktion.GEHEN_RECHTS,
                Enums.EnumAddTexture.ERSETZE_WENN_UNGLEICH,
                -2);
    }

    // PATH BERECHNEN /////////////////////////////////////////////////////////////////////////////

    private Vector2 gettDebugPosition(Vector2 position) {
        Zelle zelle = worldMap.gett(position);
        if (zelle.isKollisionLaufen()) {
            zelle = worldMap.gettFreieZelle(zelle.getPosition(), false, true);
            return new Vector2(zelle.getPosition());
        }
        return position;
    }

    private void createPath(Vector2 startPosition) {
        Path pathClass = new Path(worldMap, bewegungszielComp, startPosition);
        path = pathClass.create();
        if (path == null) {
            bewegungComp.unerreichbarList.add(bewegungszielComp);
            bewegungszielComp.setKeinPfad(true);
        } else {
            bewegungszielComp.setKeinPfad(false);
        }
    }

    private void settToDebugPositionAndCreatePath() {
        p.set(gettDebugPosition(p));
        createPath(p);
    }

    // PRUEFEN ////////////////////////////////////////////////////////////////////////////////////

    @Override
    public boolean pruefenObErledigt() {
        return path != null && path.isEmpty();
    }

    @Override
    public boolean pruefenObFail() {
        return path == null;
    }

    // BEENDEN ////////////////////////////////////////////////////////////////////////////////////

    @Override
    protected void failNachVersuchenVorAnfangen_or() {
        if (getStatus() == Enums.EnumStpStatus.FAIL_BEI_GEHE_ZU) {
            bewegungComp.unerreichbarList.add(bewegungszielComp);
        }
    }

    @Override
    protected void failNachAnfangenVorDurchfuehren_or() {
        if (getStatus() == Enums.EnumStpStatus.FAIL_BEI_GEHE_ZU) {
            bewegungComp.unerreichbarList.add(bewegungszielComp);
        }
    }

    @Override
    protected void failNachDurchfuehren_or() {
        if (getStatus() == Enums.EnumStpStatus.FAIL_BEI_GEHE_ZU) {
            bewegungComp.unerreichbarList.add(bewegungszielComp);
        }
    }

    @Override
    public void erledigt_or() {
        float f[] = KonstantenBalance.SCHADEN_DURCH_LAUFEN;
        //getDorfbewohner().erledigt_or(gettGesamtLaufZeit(), f[0], f[1], f[2], f[3], f[4]); TODO
        //getDorfbewohner().laufenBeenden();
    }

    @Override
    protected void beenden() {
        bewegungComp.beweglichRenderComp.add(
                Enums.EnumAktion.STANDARD_PRIMAERMENGE_VOLL,
                Enums.EnumAddTexture.ERSETZE_IMMER,
                -2);
        super.beenden();
    }

    // KOMPLEXE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public float gettGesamtLaufZeit() {
        return gelaufenGesamtStrecke / bewegungComp.bewegungsgeschwindigkeit;
    }

    public Zelle gettZielZelle() {
        if (zielZelle == null) {
            zielZelle = path.get(path.size() - 1).getZelle();
        }
        return zielZelle;
    }

    // EINFACHE GETTER UND SETTER /////////////////////////////////////////////////////////////////
}