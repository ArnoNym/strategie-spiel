package de.arnonym.strategiespiel.spielSchirm.umwelt.ressourcen;

import de.arnonym.strategiespiel.framework.ecs.component.Comp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.gegenstaende.GegenstaendeComp;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.spielSchirm.umwelt.SysVergammeln;

public class CompVergammeln extends Comp {
    public final GegenstaendeComp gegenstaendeComp;

    public CompVergammeln(Entity entity, SysVergammeln sysVergammeln,
                          GegenstaendeComp gegenstaendeComp) {
        super(sysVergammeln, entity, gegenstaendeComp);
        this.gegenstaendeComp = gegenstaendeComp;
    }
}
