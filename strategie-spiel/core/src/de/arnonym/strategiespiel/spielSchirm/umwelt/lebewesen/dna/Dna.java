package de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dna;

import java.io.Serializable;

import de.arnonym.strategiespiel.spielSchirm.Enums;

class Dna implements Serializable {
    private float value;
    private Enums.EnumDnaStatus enumDnaStatus;

    public Dna(float value, Enums.EnumDnaStatus enumDnaStatus) {
        this.value = value;
        this.enumDnaStatus = enumDnaStatus;
    }

    public float getValue() {
        return value;
    }

    public Enums.EnumDnaStatus getEnumDnaStatus() {
        return enumDnaStatus;
    }
}
