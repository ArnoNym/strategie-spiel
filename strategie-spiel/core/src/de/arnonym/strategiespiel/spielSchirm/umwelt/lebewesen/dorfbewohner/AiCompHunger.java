package de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dorfbewohner;

import de.arnonym.strategiespiel.framework.stp.Ai;
import de.arnonym.strategiespiel.framework.ecs.component.Comp;
import de.arnonym.strategiespiel.framework.stp.StpComp;
import de.arnonym.strategiespiel.framework.ecs.collections.Matcher;
import de.arnonym.strategiespiel.framework.stp.StpMachine;
import de.arnonym.strategiespiel.framework.stp.Task;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.GeldComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.gegenstaende.TascheComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.rendern.RenderComp;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.taschen.NahrungTasche;
import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenAiPrioritaet;

public class AiCompHunger extends Ai {
    private final GeldComp geldComp;
    private final TascheComp tascheComp;
    private final float suchenAbMenge;
    private final float verhungernAbMenge;
    private final float auffuellenBisMenge;

    public AiCompHunger(Entity entity, RenderComp renderComp, StpComp stpComp,
                        GeldComp geldComp, TascheComp tascheComp, float suchenAbMenge,
                        float verhungernAbMenge, float auffuellenBisMenge) {
        super(new Matcher[]{}, entity, stpComp, renderComp, new Comp[]{geldComp, tascheComp},
                KonstantenAiPrioritaet.HUNGER);
        this.geldComp = geldComp;
        this.tascheComp = tascheComp;
        this.suchenAbMenge = suchenAbMenge;
        this.verhungernAbMenge = verhungernAbMenge;
        this.auffuellenBisMenge = auffuellenBisMenge;
    }

    @Override
    public Task versuchen() {
        return null;
    }

/* todo
    @Override
    public boolean entscheiden(StpMachine stpMachine) {
        NahrungTasche nahrungTasche = tascheComp.getMasterTasche().getNahrungTasche_nl();
        if (nahrungTasche == null) {
            return false;
        }
        float nahrungMenge = nahrungTasche.gettMenge(true, true, false);
        if (nahrungMenge <= suchenAbMenge) {
            if (essen()) {
                //verhungert = false; // TODO Verhungern Symbol
                return true;
            }
            if (nahrungMenge <= verhungernAbMenge) {
                //verhungert = true;
            }
        }
        return false;
    }
    */

    private boolean essen() {
        /*TaskRetteVorVerhungern taskKaufenPrivatVerhungern = new TaskRetteVorVerhungern(this); TODO
        if (taskKaufenPrivatVerhungern.versuchen() == Enums.EnumStepStatus.OK) {
            stackFiniteStepMachine.addTask(taskKaufenPrivatVerhungern, true);
            return true;
        }*/
        return false;
    }
}
