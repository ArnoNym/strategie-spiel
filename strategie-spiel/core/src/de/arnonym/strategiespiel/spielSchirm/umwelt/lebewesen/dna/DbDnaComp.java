package de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dna;

import de.arnonym.strategiespiel.framework.ecs.entity.Entity;

public class DbDnaComp extends CompDna { // TODO Ueberarbeiten wenn Tiere etc auch dna haben sollen. Am besten nimmt man die Speicherfunktion des DnaStrangs und packt sie in diese Klasse
    public final DnaStrang dnaStrang;

    public DbDnaComp(Entity entity, DnaStrang dnaStrang) {
        super(entity);
        this.dnaStrang = dnaStrang;
    }
}
