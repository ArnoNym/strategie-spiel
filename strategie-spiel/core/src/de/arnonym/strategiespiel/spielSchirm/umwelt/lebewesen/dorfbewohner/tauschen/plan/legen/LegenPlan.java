package de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dorfbewohner.tauschen.plan.legen;

import com.badlogic.gdx.math.Vector2;

import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.bewegung.BewegungComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.gegenstaende.GegenstaendeComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.EinkommenComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.GeldComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.lager.LagerComp;
import de.arnonym.strategiespiel.spielSchirm.ui.aufsteigendeSymbole.AufsteigendesSymbolFactory;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.listSystems.privateListSystems.SysLager;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dorfbewohner.tauschen.plan.PlanTauschen;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.Geld;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.endstufe.Gegenstand;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.MathUtils;

public abstract class LegenPlan extends PlanTauschen {
    public final GegenstaendeComp aGegenstaendeComp;
    public final GeldComp gGeldComp;
    public final LagerComp zuvorGenommenAusZiel_nl;

    public LegenPlan(AufsteigendesSymbolFactory aufsteigendesSymbolFactory,

                     SysLager sysLager,


                     GeldComp gGeldComp, int salary,

                     BewegungComp aBewegungComp, GeldComp aGeldComp,
                     GegenstaendeComp aGegenstaendeComp,

                     LagerComp zuvorGenommenAusZiel_nl, boolean mussLohnen, Vector2 startPosition) {
        super(aufsteigendesSymbolFactory, sysLager, salary, aBewegungComp, aGeldComp, mussLohnen, startPosition);
        this.aGegenstaendeComp = aGegenstaendeComp;
        this.gGeldComp = gGeldComp;
        this.zuvorGenommenAusZiel_nl = zuvorGenommenAusZiel_nl;
    }

    protected int applyGeldBounds(int menge, Gegenstand zGegenstand, Geld zGeld) {
        if (menge == 0) return 0;
        else if (menge < 0) throw new IllegalArgumentException();

        int preis = zGegenstand.getWert();
        int geldMenge = zGeld.getMengeVerfuegbar();

        if (MathUtils.myIntMult(menge, preis) > geldMenge) {
            int kannBezahlenMenge = MathUtils.myIntDiv(geldMenge, preis);
            if (!zGegenstand.isTeilbar()) {
                kannBezahlenMenge = MathUtils.myIntRundenAufGanzeZahl(kannBezahlenMenge);
            }
            menge = Math.min(menge, kannBezahlenMenge);
        }
        if (menge == 0) {
            getFeedback().addText("PlanLegen ::: kannBezahlenMenge == 0");
        }
        return menge;
    }

    // BEWERTEN ///////////////////////////////////////////////////////////////////////////////////

    @Override
    protected int gettPositiv(Gegenstand g) {
        return g.getWert();
    }

    @Override
    protected int gettNegativ(Gegenstand g) {
        return 1;
    }
}
