package de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dorfbewohner.tauschen.plan;

import com.badlogic.gdx.math.Vector2;

import java.util.List;
import java.util.Map;

import de.arnonym.strategiespiel.spielSchirm.balancing.Konstanten;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.bewegung.BewegungComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.EinkommenComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.GeldComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.lager.LagerComp;
import de.arnonym.strategiespiel.spielSchirm.ui.aufsteigendeSymbole.AufsteigendesSymbolFactory;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.listSystems.privateListSystems.SysLager;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dorfbewohner.tauschen.plan.score.PlanTauschenScoreHashMap;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dorfbewohner.tauschen.plan.score.PlanTauschenScore;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dorfbewohner.tauschen.plan.score.ZielGegenstandKombi;
import de.arnonym.strategiespiel.framework.stp.Plan;
import de.arnonym.strategiespiel.spielSchirm.StepNGZ;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.endstufe.Gegenstand;
import de.arnonym.strategiespiel.spielSchirm.Enums;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.SpecialUtils;

public abstract class PlanTauschen extends Plan {
    public final AufsteigendesSymbolFactory aufsteigendesSymbolFactory;

    public final int salary;
    public final BewegungComp aBewegungComp;
    public final GeldComp aGeldComp;
    public final boolean mussLohnen;

    private final List<LagerComp> zieleList;

    private PlanTauschenScoreHashMap scoreHashMap;
    private Vector2 planenPosition;
    private LagerComp ziel;

    public PlanTauschen(AufsteigendesSymbolFactory aufsteigendesSymbolFactory, SysLager sysLager,
                        int salary, BewegungComp aBewegungComp, GeldComp aGeldComp, boolean mussLohnen,
                        Vector2 startPosition) {
        this.aufsteigendesSymbolFactory = aufsteigendesSymbolFactory;
        this.salary = salary;
        this.aBewegungComp = aBewegungComp;
        this.aGeldComp = aGeldComp;
        this.planenPosition = new Vector2(startPosition);
        this.mussLohnen = mussLohnen;
        this.zieleList = gettZieleList(sysLager);
    }

    protected abstract List<LagerComp> gettZieleList(SysLager sysLager);

    public void init() { // Muss am Ende von Child Constrcutor gecallt werden
        if (!pruefeTotal()) {
            getFeedback().addText("PlanTauschen ::: pruefungTotal == false in loop");
            ende();
            return;
        }

        scoreHashMap = gettScoreHashMap(zieleList, planenPosition);
        if (scoreHashMap.isEmpty()) {
            getFeedback().addText("PlanTauschen ::: scoreHashMap.isEmpty() vor loop");
            ende();
            return;
        }

        Map.Entry<ZielGegenstandKombi, PlanTauschenScore> bestEntry = scoreHashMap.removeMaxScoreEntry();
        ziel = bestEntry.getKey().getZiel();
        neuesZiel_override();
        do {
            if (pruefeGeg(bestEntry)) { /* Erst hier pruefen weil sich 'negativ' durch neue
                laufenKosten veraendern kann */
                handeln(bestEntry);
            }

            boolean scoreHashMapEmpty = scoreHashMap.isEmpty();
            boolean pruefungTotal = pruefeTotal();
            if (scoreHashMapEmpty || !pruefungTotal) {
                if (scoreHashMapEmpty) {
                    getFeedback().addText("PlanTauschen ::: scoreHashMap.isEmpty() in loop");
                }
                if (pruefungTotal) {
                    getFeedback().addText("PlanTauschen ::: pruefungTotal == false in loop");
                }
                erstelleNeuenStep();
                ende();
                return;
            }

            bestEntry = scoreHashMap.removeMaxScoreEntry();

            LagerComp neuesZiel = bestEntry.getKey().getZiel();
            if (ziel != neuesZiel) {
                scoreHashMap.loescheZiel(ziel);
                scoreHashMap.settLaufenStartPosition(aBewegungComp, salary, planenPosition);

                erstelleNeuenStep();
                neuesZiel_override();

                ziel = neuesZiel;
            }
        } while (true);
    }

    // ABLAUF /////////////////////////////////////////////////////////////////////////////////////

    protected PlanTauschenScoreHashMap gettScoreHashMap(List<LagerComp> zielListe,
                                                        Vector2 startPosition) {
        getFeedback().addText("PlanTauschen ::: zielListe size: " + zielListe.size());
        int woPruefungGefailt = 0;
        int gegPruefungGefailt = 0;
        PlanTauschenScoreHashMap scoreHashMap = new PlanTauschenScoreHashMap();
        for (LagerComp ziel : zielListe) {
            if (basicPruefeZiel(ziel)) {
                int kostenLaufen = SpecialUtils.gettKostenLaufen(aBewegungComp, salary,
                        startPosition,
                        ziel.bewegungszielComp.compKollisionLaufen.positionComp.getPosition());
                for (Gegenstand geg : ziel.gegenstaendeComp.gegenstandListe) {
                    if (basicPruefeGeg(geg)) {
                        scoreHashMap.put(new ZielGegenstandKombi(ziel, geg),
                                new PlanTauschenScore(gettPositiv(geg), gettNegativ(geg),
                                        kostenLaufen));
                    } else {
                        gegPruefungGefailt += 1;
                    }
                }
            } else {
                woPruefungGefailt += 1;
            }
        }
        getFeedback().addText("PlanTauschen ::: woPruefungGefailt: " + woPruefungGefailt);
        getFeedback().addText("PlanTauschen ::: gegPruefungGefailt: " + gegPruefungGefailt);
        getFeedback().addText("PlanTauschen ::: scoreHashMap create-size: " + scoreHashMap.size());
        return scoreHashMap;
    }

    protected abstract void neuesZiel_override();

    protected abstract void handeln(Map.Entry<ZielGegenstandKombi, PlanTauschenScore> bestEntry);

    private void erstelleNeuenStep() {
        StepNGZ neuerStep = gettNeuerStep(ziel);
        if (neuerStep == null) {
            getFeedback().addText("PlanTauschen :::: kein Step Erstellt");
            return;
        }
        if (getStepStack().handle(neuerStep, false) == Enums.EnumStpStatus.OK) {
            planenPosition = neuerStep.stepGeheZu.gettZielZelle().position;
        }
    }

    protected abstract StepNGZ gettNeuerStep(LagerComp ziel);

    protected final void ende() {
        ende_or();
        posteFeedback();
    }

    protected abstract void ende_or();

    // PRUEFE /////////////////////////////////////////////////////////////////////////////////////

    protected abstract boolean pruefeTotal();

    private boolean pruefeGeg(Map.Entry<ZielGegenstandKombi, PlanTauschenScore> entry) {
        if (!mussLohnen) {
            return true;
        }
        PlanTauschenScore value = entry.getValue();
        boolean bestanden = value.getPositiv() > value.getNegativ() + value.getLaufenKosten();
        if (Konstanten.DEVELOPER_VISUELL_STEP_SYSTEM_OUT && !bestanden) {
            getFeedback().addText("PlanTauschen ::: geg mit id "
                    +entry.getKey().getGegenstand().gettId()
                    +" aussortiert weil er sich nicht lohnt. Positiv == "+value.getPositiv()
                    +", Negativ == "+value.getNegativ()
                    +", LaufenKosten == "+value.getLaufenKosten());
        }
        return value.getPositiv() > value.getNegativ() + value.getLaufenKosten();
    }

    /* Koennen platzEmpty gelassen werden. Sollten aber bei offensichtlichen Faellen wie einem
    leerenLager zum einkaufen die Performance erhoehen */

    protected abstract boolean basicPruefeZiel(LagerComp moeglichesZiel);

    protected abstract boolean basicPruefeGeg(Gegenstand geg);

    // BEWERTEN ///////////////////////////////////////////////////////////////////////////////////

    // zB Preis beim verkaufen oder Nutzen beim Kaufen
    protected abstract int gettPositiv(Gegenstand g);

    // zB Preis beim Kaufen
    protected abstract int gettNegativ(Gegenstand g);

    public static float gettProzentLohnt(int positiv, int negativ, int kostenLaufen) {
        int nenner = negativ + kostenLaufen;
        if (nenner == 0) {
            return Integer.MAX_VALUE;
        }
        return positiv / nenner;
    }

    // FEEDBACK ///////////////////////////////////////////////////////////////////////////////////

    protected void posteFeedback() {
        if (Konstanten.DEVELOPER_VISUELL_STEP_SYSTEM_OUT) {
            System.out.println(getFeedback().getText());
        }
    }

    // EINFACHE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public boolean isMussLohnen() {
        return mussLohnen;
    }

    public Vector2 getPlanenPosition() {
        return planenPosition;
    }

    public void setScoreHashMap(PlanTauschenScoreHashMap scoreHashMap) {
        this.scoreHashMap = scoreHashMap;
    }

    public List<LagerComp> getZieleList() {
        return zieleList;
    }
















    /*
    // FINDE BESTES ///////////////////////////////////////////////////////////////////////////////

    protected WorldObjekt gettBestesWorldObjekt(List<? extends WorldObjekt> worldObjektListe,
                                                Vector2 planenStart) {
        WorldObjekt maxProzentGewinnGebaude_nl = null;
        float maxProzentLohnt = -Float.MAX_VALUE;
        for (WorldObjekt wo : worldObjektListe) {
            int kostenLaufen = getDorfbewohner().kostenLaufen(planenStart, wo.getPositionMitte());
            Gegenstand kaufenGegenstand = gettBesterGegenstand(wo.getMusterGegListe(),
                    kostenLaufen);
            if (kaufenGegenstand != null) {
                float prozentLohnt = gettProzentLohnt(gettPositiv(kaufenGegenstand),
                        gettNegativ(kaufenGegenstand, kostenLaufen));
                if (prozentLohnt > maxProzentLohnt) {
                    maxProzentLohnt = prozentLohnt;
                    maxProzentGewinnGebaude_nl = wo;
                }
            }
        }
        return maxProzentGewinnGebaude_nl;
    }

    protected Gegenstand gettBesterGegenstand(List<Gegenstand> worldObjektGegenstandListe,
                                              int kostenLaufen) {
        if (worldObjektGegenstandListe.isEmpty()) {
            throw new IllegalArgumentException();
        }
        Gegenstand besterGegenstand = null;
        float maxProzentLohnt;
        maxProzentLohnt = -Float.MAX_VALUE;
        for (Gegenstand g : worldObjektGegenstandListe) {
            int positiv = gettPositiv(g);
            int negativ = gettNegativ(g, kostenLaufen);
            float prozentLohnt = gettProzentLohnt(positiv, negativ);
            IdentityConnectable.out.println("PlanTauschen ::: prozentLohnt: "+prozentLohnt);
            if (prozentLohnt > maxProzentLohnt) {
                maxProzentLohnt = prozentLohnt;
                besterGegenstand = g;
            }
        }
        if (besterGegenstand == null) {
            throw new IllegalStateException();
        }
        return besterGegenstand;
    }
     */
}
