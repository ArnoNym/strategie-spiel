package de.arnonym.strategiespiel.spielSchirm.umwelt.ressourcen.stufen;

import com.badlogic.gdx.math.Vector2;

import java.util.concurrent.ThreadLocalRandom;

import de.arnonym.strategiespiel.Strategiespiel;
import de.arnonym.strategiespiel.framework.werkzeuge.collections.groups.TwoGroup;
import de.arnonym.strategiespiel.spielSchirm.World;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.herrschaft.EigentuemerComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.herrschaft.EigentumComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.rendern.UnbeweglichRenderComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.transformieren.TransComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.ressourcen.RessourcenFactory;
import de.arnonym.strategiespiel.framework.ecs.component.Comp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.alter.EnumAlterComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.ausbeuten.AbbaubarComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.gegenstaende.GegenstaendeComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.gegenstaende.GegenstaendeComp3;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.gegenstaende.GegenstaendeComp4;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.klick.FensterKlickComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.CompSterben;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.transformieren.VonZuTransComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.ressourcen.PflegenComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.ressourcen.CompVergammeln;
import de.arnonym.strategiespiel.spielSchirm.umwelt.ressourcen.RessourceVermehrenComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.ressourcen.Ressource;
import de.arnonym.strategiespiel.spielSchirm.umwelt.ressourcen.stufen.sekundaer.SWachstumRessourceInfo;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterOnKlick.OnKlickFensterRessource;
import de.arnonym.strategiespiel.spielSchirm.Enums;

public abstract class WachstumRessource<SI extends WachstumRessourceInfo<?>> extends Ressource {
    protected final VonZuTransComp mittelTransComp;
    protected final VonZuTransComp altTransComp;
    protected final VonZuTransComp todTransComp;
    protected final GegenstaendeComp gegenstaendeComp;
    protected final PflegenComp pflegenComp;

    public WachstumRessource(World world, RessourcenFactory ressourcenFactory, SI si,
                             Enums.EnumResNwStufe alter, float alterAbgeschlossenProzent,
                             EigentuemerComp eigentuemer, Vector2 position) {
        super(world, si, si.get(alter).textureHashMap, eigentuemer, position);
        final float dauer_jung = si.ressourceInfo_jung.stufeDauer_sek;
        final float dauer_mittel = si.ressourceInfo_mittel.stufeDauer_sek;
        final float dauer_alt = si.ressourceInfo_alt.stufeDauer_sek;
        final float alter_jung = 0;
        final float alter_mittel = alter_jung + dauer_jung;
        final float alter_alt = alter_mittel + dauer_mittel;
        final float alter_tod = alter_alt + dauer_alt
                * (0.5f + ThreadLocalRandom.current().nextFloat());
        float alter_sek;
        switch (alter) {
            case JUNG:
                alter_sek = alter_jung + dauer_jung * alterAbgeschlossenProzent;
                break;
            case MITTEL:
                alter_sek = alter_mittel + dauer_mittel * alterAbgeschlossenProzent;
                break;
            case ALT:
                alter_sek = alter_alt + dauer_alt * alterAbgeschlossenProzent;
                break;
            default: throw new IllegalStateException();
        }
        if (alterAbgeschlossenProzent < 0 || alterAbgeschlossenProzent > 1) {
            throw new IllegalArgumentException("fehler: "+alterAbgeschlossenProzent);
        }

        ///////////////////////////////////////////////////////////////////////////////////////////

        mittelTransComp = new VonZuTransComp(this, TransComp.EnumDelete.TRANS_COMP, new Comp[]{},
                new Comp[]{});
        altTransComp = new VonZuTransComp(this, TransComp.EnumDelete.TRANS_COMP, new Comp[]{},
                new Comp[]{});
        todTransComp = new VonZuTransComp(this, TransComp.EnumDelete.TRANS_COMP, new Comp[]{},
                new Comp[]{});

        ///////////////////////////////////////////////////////////////////////////////////////////

        CompSterben compSterben = new CompSterben(todTransComp);

        WachstumInfo stufeResInInfo = si.get(Enums.EnumResNwStufe.JUNG); // Siehe spaeter Trans
        if (si instanceof SWachstumRessourceInfo) {
            gegenstaendeComp = new GegenstaendeComp4(this, unbeweglichRenderComp, compSterben,
                    stufeResInInfo.entityGegInfoList, ((SWachstumRessourceInfo) si).primaerGegIds);
        } else {
            gegenstaendeComp = new GegenstaendeComp3(this, unbeweglichRenderComp, compSterben,
                    stufeResInInfo.entityGegInfoList);
        }

        EnumAlterComp enumAlterComp = new EnumAlterComp(this, world.sysAltern, false, alter_sek,
                new TwoGroup<>(mittelTransComp, alter_mittel),
                new TwoGroup<>(altTransComp, alter_alt),
                new TwoGroup<>(todTransComp, alter_tod));
        enumAlterComp.activate();// todo loeschen

        new FensterKlickComp(this, world.sysKlick, isoGroesseComp, positionComp,
                unbeweglichRenderComp, () -> (new OnKlickFensterRessource(world,
                Strategiespiel.skin, enumAlterComp, gegenstaendeComp, unbeweglichRenderComp,
                nameComp)));

        AbbaubarComp compAbbaubar = new AbbaubarComp(this, world.sysAbbaubar, bewegungszielComp,
                eigentumComp, gegenstaendeComp, unbeweglichRenderComp, si.abbauenAssignment);

        pflegenComp = new PflegenComp(this, bewegungszielComp, si.abbauenAssignment);
        pflegenComp.activate();
        todTransComp.addLoeschenComps(pflegenComp);

        activate();

        // JUNG //

        // ... in Override

        // MITTEL //

        mittelTransComp.addTransLogic(() -> settStufeResInInfo(
                si.get(Enums.EnumResNwStufe.MITTEL),
                gegenstaendeComp,
                unbeweglichRenderComp));

        // ... in Override

        // ALT //

        altTransComp.addTransLogic(() -> settStufeResInInfo(
                si.get(Enums.EnumResNwStufe.ALT),
                gegenstaendeComp,
                unbeweglichRenderComp));

        RessourceVermehrenComp ressourceVermehrenComp = new RessourceVermehrenComp(ressourcenFactory, this, world.sysResVermehren,
                kollisionComp, pflegenComp, si.id, si.vermehrenProp_sekunde);
        altTransComp.addActivateComps(ressourceVermehrenComp);
        todTransComp.addActivateComps(ressourceVermehrenComp);

        // ... in Override

        // TOD //

        CompVergammeln compVergammeLn = new CompVergammeln(this, world.sysVergammeln,
                gegenstaendeComp);
        todTransComp.addActivateComps(compVergammeLn);

        // ... in Override
    }

    // JUNG ///////////////////////////////////////////////////////////////////////////////////////

    private static void settStufeResInInfo(WachstumInfo stufeResInInfo,
                                           GegenstaendeComp gegenstaendeComp,
                                           UnbeweglichRenderComp unbeweglichRenderComp) {
        gegenstaendeComp.gegenstandListe.mengenErhoehenBis(stufeResInInfo.entityGegInfoList);
        unbeweglichRenderComp.settAktionTextureHashMap(stufeResInInfo.textureHashMap);
    }
}
