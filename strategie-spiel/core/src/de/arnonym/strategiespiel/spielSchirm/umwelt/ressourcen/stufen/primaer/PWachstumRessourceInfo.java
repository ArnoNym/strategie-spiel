package de.arnonym.strategiespiel.spielSchirm.umwelt.ressourcen.stufen.primaer;

import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.departments.gebiet.RessourcenAssignment;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.departments.bildung.ForschungAssignment;
import de.arnonym.strategiespiel.spielSchirm.umwelt.ressourcen.stufen.WachstumRessourceInfo;

public class PWachstumRessourceInfo extends WachstumRessourceInfo<PWachstumInfo> {
    public PWachstumRessourceInfo(int id, String name, String beschreibung, ForschungAssignment forschungAssignment_nl, RessourcenAssignment abbauenAssignment, RessourcenAssignment pflegeAssignment, float vermehrenProp_sekunde, PWachstumInfo ressourceInfo_jung, PWachstumInfo ressourceInfo_mittel, PWachstumInfo ressourceInfo_alt) {
        super(id, name, beschreibung, forschungAssignment_nl, abbauenAssignment, pflegeAssignment, vermehrenProp_sekunde, ressourceInfo_jung, ressourceInfo_mittel, ressourceInfo_alt);
    }
}
