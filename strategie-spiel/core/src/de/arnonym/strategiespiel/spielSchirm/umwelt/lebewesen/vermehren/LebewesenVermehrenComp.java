package de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.vermehren;

import de.arnonym.strategiespiel.framework.ecs.collections.System;
import de.arnonym.strategiespiel.framework.ecs.component.Comp;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.spielSchirm.umwelt.VermehrenComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dna.DbDnaComp;

public class LebewesenVermehrenComp extends VermehrenComp {
    private final DbDnaComp dbDnaComp_nl;
    private final FemaleComp femaleComp_nl;

    // FEMALE
    public LebewesenVermehrenComp(System system, Entity entity, FemaleComp femaleComp,
                                  int id, float prop_sekunde) {
        super(system, entity, new Comp[]{}, id, prop_sekunde);
        this.dbDnaComp_nl = null;
        if (femaleComp == null) throw new IllegalArgumentException();
        this.femaleComp_nl = femaleComp;
    }

    // MALE
    public LebewesenVermehrenComp(System system, Entity entity, DbDnaComp dbDnaComp,
                                  int id, float prop_sekunde) {
        super(system, entity, new Comp[]{}, id, prop_sekunde);
        if (dbDnaComp == null) throw new IllegalArgumentException();
        this.dbDnaComp_nl = dbDnaComp;
        this.femaleComp_nl = null;
    }

    public boolean isFemale() {
        return femaleComp_nl != null;
    }

    public static void vermehren(LebewesenVermehrenComp lc1, LebewesenVermehrenComp lc2) {
        LebewesenVermehrenComp mother;
        LebewesenVermehrenComp father;
        if (lc1.isFemale()) {
            mother = lc1;
            father = lc2;
        } else {
            father = lc1;
            mother = lc2;
        }
        if (father.isFemale()) {
            throw new IllegalArgumentException();
        }
        if (mother.femaleComp_nl.isPregnant()) {
            return;
        }
        mother.femaleComp_nl.impregnate(father.dbDnaComp_nl);
    }
}
