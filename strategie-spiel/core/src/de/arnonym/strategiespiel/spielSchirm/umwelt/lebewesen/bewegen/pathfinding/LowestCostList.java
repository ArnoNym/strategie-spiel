package de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.bewegen.pathfinding;

import java.util.ArrayList;
import java.util.Collection;

public class LowestCostList<E extends Goal> extends ArrayList<E> {

    public LowestCostList() {

    }

    public Goal gettLowestCostPart() {
        return get(0);
    }

    @Override
    public boolean add(Goal goal) {
        float partCost = goal.getGesamtKosten();
        if (isEmpty()) {
            add(0, (E) goal);
            return true;
        }
        for (Goal z : this) {
            if (z.getGesamtKosten() >= partCost ) {
                int i = indexOf(z);
                super.add(i,(E) goal);
                return true;
            }
        }
        return super.add((E) goal);
    }

    // DEPRECATED /////////////////////////////////////////////////////////////////////////////////

    @Deprecated @Override
    public void add(int i, E e) {
        super.add(i, e);
    }

    @Deprecated @Override
    public boolean addAll(Collection<? extends E> collection) {
        return super.addAll(collection);
    }

    @Deprecated @Override
    public boolean addAll(int i, Collection<? extends E> collection) {
        return super.addAll(i, collection);
    }
}
