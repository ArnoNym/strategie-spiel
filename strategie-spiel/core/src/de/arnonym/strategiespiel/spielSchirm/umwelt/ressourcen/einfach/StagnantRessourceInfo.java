package de.arnonym.strategiespiel.spielSchirm.umwelt.ressourcen.einfach;

import de.arnonym.strategiespiel.framework.werkzeuge.textures.Texture;
import de.arnonym.strategiespiel.framework.werkzeuge.textures.TextureHashMap;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.departments.gebiet.RessourcenAssignment;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.departments.bildung.ForschungAssignment;
import de.arnonym.strategiespiel.spielSchirm.umwelt.ressourcen.RessourceInfo;
import de.arnonym.strategiespiel.spielSchirm.Enums;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.vorstufe.EntityGegInfoList;

public class StagnantRessourceInfo extends RessourceInfo {
    public final TextureHashMap textureHashMap;
    public final EntityGegInfoList entityGegInfoList;

    public StagnantRessourceInfo(int id, String name, String beschreibung,
                                 ForschungAssignment forschungAssignment_nl,
                                 RessourcenAssignment abbauenAssignment, TextureHashMap textureHashMap,
                                 EntityGegInfoList entityGegInfoList) {
        super(id, name, beschreibung, forschungAssignment_nl, abbauenAssignment);
        this.textureHashMap = textureHashMap;
        this.entityGegInfoList = entityGegInfoList;
    }

    @Override
    public Texture getIconTexture() {
        return textureHashMap.get(Enums.EnumAktion.STANDARD_PRIMAERMENGE_VOLL);
    }
}
