package de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dorfbewohner;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dna.DnaStrang;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.vorstufe.EntityGegInfo;
import de.arnonym.strategiespiel.spielSchirm.Enums;
import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenBalance;

public class DorfbewohnerMuster {
    public final String vorname;
    public final float alter_jahre;
    public final Enums.EnumStatusInFamilie enumStatusInFamilie;
    public final boolean geschlechtFemale;
    public final DnaStrang dnaStrang; // TODO Alle Variablen wieder irgendwo wert

    public DorfbewohnerMuster(DnaStrang motherDnaStrang, DnaStrang fatherDnaStrang) {
        this.vorname = "TODO Zufallsvorname";
        this.alter_jahre = 0;
        this.enumStatusInFamilie = Enums.EnumStatusInFamilie.KIND;
        this.geschlechtFemale = ThreadLocalRandom.current().nextBoolean();
        this.dnaStrang = new DnaStrang(motherDnaStrang, fatherDnaStrang);
    }

    public DorfbewohnerMuster(String vorname, boolean geschlechtFemale, float alter_jahre,
                              Enums.EnumStatusInFamilie enumStatusInFamilie,
                              float produktivtiaetKopf, float produktivitaetKoerper,
                              float nutzenExponentNahrung, float nutzenExponentSpielzeug,
                              float nutzenExponentWerkzeug, float nutzenExponentMedizin,
                              float nutzenExponentWohnen, List<EntityGegInfo> entityGegInfoList,
                              float bewegungsgeschwindigkeit_mult,
                              float schlafAuffuellGeschwindigkeit_mult) {
        this.vorname = vorname;
        this.alter_jahre = alter_jahre;
        this.enumStatusInFamilie = enumStatusInFamilie;
        this.geschlechtFemale = geschlechtFemale;
        this.dnaStrang = new DnaStrang(produktivtiaetKopf, produktivitaetKoerper, entityGegInfoList,
                nutzenExponentNahrung, nutzenExponentSpielzeug, nutzenExponentWerkzeug,
                nutzenExponentMedizin, nutzenExponentWohnen,
                (int) (bewegungsgeschwindigkeit_mult
                        * KonstantenBalance.DORFBEWOHNER_DNA_BEWEGUNGSGESCHWINDIGKEIT),
                (int) (schlafAuffuellGeschwindigkeit_mult
                        * KonstantenBalance.DORFBEWOHNER_DNA_SCHLAFEN_AUFFUELL_GESCHWINDIGKEIT));
    }

    /*
    public float getProduktivtiaetKopf() {
        return dnaStrang.produktivtiaetKopf;
    }
    public float getProduktivitaetKoerper() {
        return dnaStrang.produktivitaetKoerper;
    }

    public float getNutzenExponentNahrung() {
        return dnaStrang.nutzenExponentNahrung;
    }

    public float getNutzenExponentSpielzeug() {
        return dnaStrang.nutzenExponentSpielzeug;
    }

    public float getNutzenExponentWerkzeug() {
        return dnaStrang.nutzenExponentWerkzeug;
    }

    public float getNutzenExponentMedizin() {
        return dnaStrang.nutzenExponentMedizin;
    }

    public float getNutzenExponentWohnen() {
        return dnaStrang.nutzenExponentWohnen;
    }

    public float getBewegungsgeschwindigkeit() {
        return dnaStrang.bewegungsgeschwindigkeit;
    }

    public float getSchlafAuffuellGeschwindigkeit() {
        return dnaStrang.schlafAuffuellGeschwindigkeit;
    }
    */
}
