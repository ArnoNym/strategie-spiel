package de.arnonym.strategiespiel.spielSchirm.umwelt.ressourcen;

import com.badlogic.gdx.math.Vector2;

import de.arnonym.strategiespiel.framework.werkzeuge.textures.TextureHashMap;
import de.arnonym.strategiespiel.spielSchirm.World;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.bewegung.BewegungszielComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.groesse.IsoGroesseComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.herrschaft.EigentuemerComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.herrschaft.EigentumComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.kollision.KollisionComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.kollision.KollisionCompBauen;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.kollision.LaufenKollisionComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.name.NameComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.rendern.UnbeweglichRenderComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.sonstiges.PositionComp;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.SysRender;
import de.arnonym.strategiespiel.spielSchirm.umwelt.ressourcen.stufen.WachstumInfo;

public abstract class Ressource extends Entity {
    protected final BewegungszielComp bewegungszielComp;
    protected final EigentumComp eigentumComp;
    protected final IsoGroesseComp isoGroesseComp;
    protected final KollisionComp kollisionComp;
    protected final PositionComp positionComp;
    protected final UnbeweglichRenderComp unbeweglichRenderComp;
    protected final NameComp nameComp;

    public Ressource(World world, RessourceInfo ressourceInfo, TextureHashMap textureHashMap,
                     EigentuemerComp eigentuemer, Vector2 position) {
        super(world.manager);
        positionComp = new PositionComp(this, position);
        isoGroesseComp = new IsoGroesseComp(this, new Vector2(1, 1));
        LaufenKollisionComp laufenKollisionComp = new LaufenKollisionComp(this, world.sysKollision,
                isoGroesseComp, positionComp, 0);
        kollisionComp = new KollisionCompBauen(this, world.sysKollision, isoGroesseComp,
                positionComp);
        bewegungszielComp = new BewegungszielComp(this, laufenKollisionComp, false);
        unbeweglichRenderComp = new UnbeweglichRenderComp(this, world.sysRender, positionComp,
                textureHashMap, SysRender.EnumZ.ENTITIES, true);
        nameComp = new NameComp(this, ressourceInfo.name);
        if (eigentuemer == null) {
            eigentumComp = new EigentumComp(this);
        } else {
            eigentumComp = new EigentumComp(eigentuemer, this);
        }
    }
}
