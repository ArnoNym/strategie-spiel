package de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.ausbeuten;

import de.arnonym.strategiespiel.framework.stp.StpComp;
import de.arnonym.strategiespiel.framework.stp.Task;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.werte.HabitalwerteComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.lists.ausbeuten.ErntenEcsList;
import de.arnonym.strategiespiel.framework.stp.StpMachine;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.bewegung.BewegungComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.gegenstaende.GegenstaendeComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.EinkommenComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.werte.ProduktivitaetComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.rendern.RenderComp;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.spielSchirm.bodenStapel.BodenStapelFactory;
import de.arnonym.strategiespiel.spielSchirm.ui.aufsteigendeSymbole.AufsteigendesSymbolFactory;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.listSystems.privateListSystems.SysLager;
import de.arnonym.strategiespiel.spielSchirm.arbeit.task.liefernArbeitszeit.ausbeuten.TaskErnten;
import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenAiPrioritaet;

public class ErntenDorfAi extends AusbeutenDorfAi {
    private final ErntenEcsList erntenListe;

    public ErntenDorfAi(AufsteigendesSymbolFactory aufsteigendesSymbolFactory,
                        BodenStapelFactory bodenStapelFactory,

                        Entity entity,

                        ErntenEcsList listeErnten, SysLager sysLager,

                        RenderComp renderComp, StpComp stpComp,
                        BewegungComp bewegungComp, EinkommenComp einkommenComp,
                        GegenstaendeComp gegenstaendeComp, HabitalwerteComp habitalwerteComp,
                        ProduktivitaetComp produktivitaetComp) {
        super(aufsteigendesSymbolFactory, bodenStapelFactory, sysLager, listeErnten, entity,
                bewegungComp, einkommenComp, gegenstaendeComp, habitalwerteComp, produktivitaetComp,
                renderComp, stpComp, KonstantenAiPrioritaet.ERNTEN);
        this.erntenListe = listeErnten;
    }

    @Override
    public Task versuchen() {
        return new TaskErnten(
                aufsteigendesSymbolFactory, bodenStapelFactory,

                sysLager, erntenListe,

                erntenListe.eigentumComp, null,

                stpComp, bewegungComp, einkommenComp, gegenstaendeComp, habitalwerteComp,
                produktivitaetComp);
    }
}
