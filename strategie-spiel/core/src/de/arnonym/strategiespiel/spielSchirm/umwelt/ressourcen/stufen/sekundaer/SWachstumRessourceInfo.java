package de.arnonym.strategiespiel.spielSchirm.umwelt.ressourcen.stufen.sekundaer;

import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.departments.bildung.ForschungAssignment;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.departments.gebiet.RessourcenAssignment;
import de.arnonym.strategiespiel.spielSchirm.umwelt.ressourcen.stufen.WachstumRessourceInfo;

public class SWachstumRessourceInfo extends WachstumRessourceInfo<SWachstumInfo> {
    public final RessourcenAssignment erntenAssignment;
    public final Integer[] primaerGegIds;
    public final float wachsen_mod;

    public SWachstumRessourceInfo(int id, String name, String beschreibung,
                                  ForschungAssignment forschungAssignment_nl,
                                  RessourcenAssignment abbauenAssignment,
                                  RessourcenAssignment assignmentPflege,
                                  RessourcenAssignment erntenAssignment,
                                  Integer[] primaerGegIds,
                                  float wachsen_mod,
                                  float vermehrenProp_sekunde,
                                  SWachstumInfo ressourceInfo_jung,
                                  SWachstumInfo ressourceInfo_mittel,
                                  SWachstumInfo ressourceInfo_alt) {
        super(id, name, beschreibung, forschungAssignment_nl, abbauenAssignment,
                assignmentPflege, vermehrenProp_sekunde, ressourceInfo_jung,
                ressourceInfo_mittel, ressourceInfo_alt);
        this.erntenAssignment = erntenAssignment;
        this.primaerGegIds = primaerGegIds;
        this.wachsen_mod = wachsen_mod;
    }
}
