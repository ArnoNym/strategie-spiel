package de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dorfbewohner.familie;

import java.util.Random;
import java.util.Set;

import de.arnonym.strategiespiel.framework.ecs.collections.Matcher;
import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenBalance;

public class PartnerMatcher extends Matcher<PartnerComp, PartnerComp> {

    public PartnerMatcher() {
        super(PartnerComp.class, PartnerComp.class, 10f);
    }

    @Override
    protected PartnerComp findBestMatch(PartnerComp partnerComp, Set<PartnerComp> partnerComps) {
        float bestEvaluation;
        PartnerComp bestPartner = partnerComp.getPartnerComp();
        if (bestPartner == null) {
            bestEvaluation = 0;
        } else {
            bestEvaluation = partnerComp.evaluate(partnerComp.getPartnerComp());
        }

        for (PartnerComp pc2 : partnerComps) {
            float evaluation = partnerComp.evaluate(pc2);
            if (evaluation > bestEvaluation) {
                bestEvaluation = evaluation;
                bestPartner = pc2;
            }
        }

        if (bestPartner == null) {
            return null;
        }

        if (bestPartner.equals(partnerComp.getPartnerComp())) {
            if ((new Random()).nextFloat() < partnerComp.prop_sekunde) {
                PartnerComp.vermehren(bestPartner, partnerComp);
            }
            return null;
        }

        return bestPartner;
    }

    @Override
    protected void couple(PartnerComp partnerComp, PartnerComp partnerComp2) {
        partnerComp.setPartnerComp(partnerComp2);
        partnerComp2.setPartnerComp(partnerComp);
    }

    @Override
    protected void decoupleAkteur(PartnerComp partnerComp) {
        decoupleReakteur(partnerComp);
    }

    @Override
    protected void decoupleReakteur(PartnerComp partnerComp) {
        PartnerComp partnersPartner = partnerComp.getPartnerComp();
        if (partnersPartner == null) {
            return;
        }
        partnersPartner.setPartnerComp(null);
    }
}
