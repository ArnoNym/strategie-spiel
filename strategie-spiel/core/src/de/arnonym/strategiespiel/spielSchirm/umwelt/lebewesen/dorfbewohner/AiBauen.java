package de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dorfbewohner;

import de.arnonym.strategiespiel.framework.stp.Ai;
import de.arnonym.strategiespiel.framework.ecs.component.Comp;
import de.arnonym.strategiespiel.framework.stp.StpComp;
import de.arnonym.strategiespiel.framework.stp.Task;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.bewegung.BewegungComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.gegenstaende.GegenstaendeComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.EinkommenComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.werte.HabitalwerteComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.werte.ProduktivitaetComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.rendern.RenderComp;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.spielSchirm.ui.aufsteigendeSymbole.AufsteigendesSymbolFactory;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.listSystems.privateListSystems.SysBaugrube;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.listSystems.privateListSystems.SysLager;
import de.arnonym.strategiespiel.framework.stp.StpMachine;
import de.arnonym.strategiespiel.spielSchirm.arbeit.task.liefernArbeitszeit.anliefern.BauenTask;

public class AiBauen extends Ai {
    public final AufsteigendesSymbolFactory aufsteigendesSymbolFactory;
    public final SysBaugrube sysBaugrube;
    public final SysLager sysLager;
    public final BewegungComp bewegungComp;
    public final EinkommenComp einkommenComp;
    public final GegenstaendeComp gegenstaendeComp;
    public final HabitalwerteComp habitalwerteComp;
    public final ProduktivitaetComp produktivitaetComp;

    public AiBauen(AufsteigendesSymbolFactory aufsteigendesSymbolFactory,

                   Entity entity,

                   SysBaugrube sysBaugrube, SysLager sysLager,

                   BewegungComp bewegungComp, EinkommenComp einkommenComp,
                   GegenstaendeComp gegenstaendeComp, HabitalwerteComp habitalwerteComp,
                   ProduktivitaetComp produktivitaetComp, RenderComp renderComp,
                   StpComp stpComp,

                   int prioritaetId) {
        super(entity, renderComp, stpComp, new Comp[]{bewegungComp, einkommenComp,
                gegenstaendeComp, habitalwerteComp, produktivitaetComp}, prioritaetId);
        this.aufsteigendesSymbolFactory = aufsteigendesSymbolFactory;
        this.sysBaugrube = sysBaugrube;
        this.sysLager = sysLager;
        this.bewegungComp = bewegungComp;
        this.einkommenComp = einkommenComp;
        this.gegenstaendeComp = gegenstaendeComp;
        this.habitalwerteComp = habitalwerteComp;
        this.produktivitaetComp = produktivitaetComp;
    }

    @Override
    public Task versuchen() {
        return new BauenTask(aufsteigendesSymbolFactory, sysBaugrube, sysLager,
                stpComp, bewegungComp, einkommenComp, gegenstaendeComp, habitalwerteComp,
                produktivitaetComp);
    }
}
