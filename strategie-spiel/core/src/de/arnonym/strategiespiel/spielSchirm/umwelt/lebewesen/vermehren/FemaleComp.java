package de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.vermehren;

import com.badlogic.gdx.math.Vector2;

import de.arnonym.strategiespiel.framework.ecs.component.Comp;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.framework.werkzeuge.numbers.unit.Unit;
import de.arnonym.strategiespiel.framework.werkzeuge.numbers.unit.ZeitEnum;
import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenBalance;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.WorldComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.sonstiges.PositionComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dna.DbDnaComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dorfbewohner.DorfComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dorfbewohner.Dorfbewohner;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dorfbewohner.DorfbewohnerMuster;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dorfbewohner.familie.FamilienmitgliedComp;

public class FemaleComp extends Comp {
    private final DbDnaComp dbDnaComp;
    private final DorfComp dorfComp;
    private final FamilienmitgliedComp familienmitgliedComp;
    private final PositionComp positionComp;
    private final WorldComp worldComp;

    private Pregnancy pregnancy = null;
    private Unit<ZeitEnum> cooldown = null;

    public FemaleComp(Entity entity, DbDnaComp dbDnaComp, DorfComp dorfComp,
                      FamilienmitgliedComp familienmitgliedComp, PositionComp positionComp,
                      WorldComp worldComp) {
        super(entity, dbDnaComp, dorfComp, familienmitgliedComp, positionComp, worldComp);
        this.dbDnaComp = dbDnaComp;
        this.dorfComp = dorfComp;
        this.familienmitgliedComp = familienmitgliedComp;
        this.positionComp = positionComp;
        this.worldComp = worldComp;
    }

    public void update(float delta) {
        if (!isPregnant()) {
            if (cooldown == null) {
                return;
            }
            cooldown = cooldown.add(delta);
            if (cooldown.doubleValue(ZeitEnum.REAL_SEKUNDEN)
                    > KonstantenBalance.DB_SCHWANGERSCHAFT_BIS_KANN_WIEDER_SCHWANGER_WERDEN_DAUER) {
                cooldown = null;
            }
            return;
        }

        pregnancy.age = pregnancy.age.add(delta);

        if (pregnancy.age.doubleValue(ZeitEnum.INGAME_MONAT) > 9) {
            pregnancy.giveBirth();
            pregnancy = null;
            cooldown = new Unit<>(0, ZeitEnum.REAL_SEKUNDEN);
        }
    }

    public void impregnate(DbDnaComp father) {
        pregnancy = new Pregnancy(father);
    }

    public boolean isPregnant() {
        return pregnancy != null;
    }

    private class Pregnancy {
        private final DbDnaComp father;
        private Unit<ZeitEnum> age = new Unit<>(0, ZeitEnum.INGAME_MONAT);

        public Pregnancy(DbDnaComp father) {
            this.father = father;
        }

        private void giveBirth() {
            new Dorfbewohner(worldComp.world, dorfComp.dorf,
                    new DorfbewohnerMuster(dbDnaComp.dnaStrang, father.dnaStrang),
                    familienmitgliedComp.getFamilie(),
                    new Vector2(positionComp.position));
        }
    }
}
