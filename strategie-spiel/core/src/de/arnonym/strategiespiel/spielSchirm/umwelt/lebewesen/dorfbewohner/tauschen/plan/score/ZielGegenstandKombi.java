package de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dorfbewohner.tauschen.plan.score;

import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.lager.LagerComp;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.endstufe.Gegenstand;

public class ZielGegenstandKombi {
    private final LagerComp ziel;
    private final Gegenstand gegenstand;

    public ZielGegenstandKombi(LagerComp ziel, Gegenstand gegenstand) {
        this.ziel = ziel;
        this.gegenstand = gegenstand;
    }

    public LagerComp getZiel() {
        return ziel;
    }

    public Gegenstand getGegenstand() {
        return gegenstand;
    }
}
