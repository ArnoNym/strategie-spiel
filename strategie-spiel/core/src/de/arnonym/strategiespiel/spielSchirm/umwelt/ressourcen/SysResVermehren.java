package de.arnonym.strategiespiel.spielSchirm.umwelt.ressourcen;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import de.arnonym.strategiespiel.spielSchirm.balancing.Konstanten;
import de.arnonym.strategiespiel.framework.ecs.collections.UpdateSetSystem;
import de.arnonym.strategiespiel.spielSchirm.umwelt.VermehrenSys;
import de.arnonym.strategiespiel.spielSchirm.worldMap.Zelle;
import de.arnonym.strategiespiel.framework.werkzeuge.collections.UpdateSet;

public class SysResVermehren extends VermehrenSys<RessourceVermehrenComp> {
    public SysResVermehren() {
        super((ressourceVermehrenComp, delta) -> {
            List<Zelle> zelleList = ressourceVermehrenComp.kollisionComp.getBetrifftZellenListe_nl();

            if (zelleList.size() > 1) {
                throw new IllegalStateException("Es gibt noch keine Entity die sich "
                        + "vermehren kann und mehr als eine Zelle belegt!");
            }

            float vermehrenProp = ressourceVermehrenComp.prop_sekunde * delta;
            if (vermehrenProp < ThreadLocalRandom.current().nextFloat()) {
                return;
            }

            Zelle zelle = zelleList.get(0);

            if (zelle.isUmringtVonKollisionBauen()) {
                return;
            }

            List<Zelle> freieZellenListe = new ArrayList<>();
            for (Zelle z : zelle.gettAngrenzendeZellen()) {
                if (z != null && !z.isKollisionBauen()) {
                    freieZellenListe.add(z);
                }
            }
            if (freieZellenListe.isEmpty()) {
                return;
            }
            int randomIndex = (int) (ThreadLocalRandom.current().nextFloat()
                    * freieZellenListe.size());
            Zelle zielZelle = freieZellenListe.get(randomIndex);
            ressourceVermehrenComp.ressourcenFactory.createNeu(ressourceVermehrenComp.id, zielZelle.position);
        });
    }
}
