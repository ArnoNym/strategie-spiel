package de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.werte;

import de.arnonym.strategiespiel.framework.werkzeuge.numbers.unit.ZeitEnum;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.listSystems.privateListSystems.HabitalwerteSys;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.alter.AlterComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.name.NameComp;
import de.arnonym.strategiespiel.framework.ecs.component.Comp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.gegenstaende.TascheComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.transformieren.TransComp;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.spielSchirm.zahlen.schaden.MusterSchaden;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.taschen.MasterTasche;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.taschen.MedizinTasche;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.taschen.WerkzeugTasche;
import de.arnonym.strategiespiel.spielSchirm.zahlen.schaden.Schaden;
import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenBalance;

public class HabitalwerteComp extends Comp {
    private final AlterComp alterComp;
    private final TransComp transComp;
    private final NameComp nameComp;
    private final TascheComp tascheComp;
    //private final FensterChat chat; TODO

    private float schlaf = KonstantenBalance.DORFBEWOHNER_SCHLAFEN_MAX;
    private float fitness = 1; // Angeeignet durch Arbeiten, Gehen etc //TODO
    private float gelaufenDauer = 0;
    private float koerperlicheArbeitDauer = 0;
    private Float gesundheit = 100f;

    public HabitalwerteComp(Entity entity, HabitalwerteSys habitalwerteSystem, AlterComp alterComp,
                            TransComp transComp, NameComp nameComp, TascheComp tascheComp) {
        super(habitalwerteSystem, entity, transComp, nameComp, tascheComp);
        this.alterComp = alterComp;
        this.transComp = transComp;
        this.nameComp = nameComp;
        this.tascheComp = tascheComp;
        //this.chat = chat;
    }

    public void update(float delta) {
        MasterTasche masterTasche = tascheComp.getMasterTasche();

        fitness = updateFitness(delta, fitness);

        gesundheit -= delta / KonstantenBalance.SEKUNDEN_PRO_JAHR / fitness
                / masterTasche.getNahrungTasche_nl().gettGesundheit();

        MedizinTasche medizinTasche = masterTasche.getMedizinTasche_nl();
        if (medizinTasche != null) {
            gesundheit += masterTasche.getMedizinTasche_nl().verbauchen();
        }

        //if (gesundheit.isNaN()) gesundheit = 0.5f;

        if (gesundheit <= 0) {
            // todo chat.addMessage(compName.getName() + " passed away!");
            transComp.trans();
            return;
        }

        schlaf = Math.max(0, schlaf - delta);
    }

    // FITNESS ////////////////////////////////////////////////////////////////////////////////////

    public void addGelaufenDauer(float gelaufenDauer) {
        this.gelaufenDauer += gelaufenDauer;
    }

    public void addArbeitDauer(float dauer, float arbeitProdKopf, float arbeitProdKoerper) {
        if (arbeitProdKoerper > 0) {
            this.koerperlicheArbeitDauer += dauer;
        }
    }

    private float updateFitness(float delta, float fitness) { // TODO Nicht zu kleine Deltas. Am besten 10 sekuendige Pause verwenden oder so
        float fitnessDurchLaufen = (gelaufenDauer / delta)
                / KonstantenBalance.DB_FITNESS_PROZENT_LEBENSZEIT_LAUFEN;

        float fitnessDurchKoerperlicheArbeit = koerperlicheArbeitDauer / delta;

        fitness += 0.01f * fitnessDurchLaufen * fitnessDurchKoerperlicheArbeit;

        fitness = Math.max(Math.min(fitness, 1), 0);
        gelaufenDauer = 0;
        return fitness;
    }

    // SCHADEN ////////////////////////////////////////////////////////////////////////////////////

    public void schadenZufuegen(MusterSchaden musterSchaden, float sekunden) {
        Schaden schaden;
        if (musterSchaden.proSekunde) {
            schaden = new Schaden(musterSchaden.value * sekunden, musterSchaden.kopf_prozent,
                    musterSchaden.lunge_prozent, musterSchaden.haende_prozent,
                    musterSchaden.fuesse_prozent, musterSchaden.sonstiges_prozent);
        } else {
            schaden = new Schaden(musterSchaden.value, musterSchaden.kopf_prozent,
                    musterSchaden.lunge_prozent, musterSchaden.haende_prozent,
                    musterSchaden.fuesse_prozent, musterSchaden.sonstiges_prozent);
        }
        schadenZufuegen(schaden);
    }

    public void schadenZufuegen(Schaden schaden) {
        WerkzeugTasche werkzeugTasche = tascheComp.getMasterTasche().getWerkzeugTasche_nl();
        if (werkzeugTasche == null) {
            gesundheit -= schaden.value;
        } else {
            gesundheit -= tascheComp.getMasterTasche().getWerkzeugTasche_nl().schuetzen(schaden);
        }
    }

    // KOMPLEXE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public float gettAlter(ZeitEnum enumZeit) {
        float alter_sekunden = alterComp.getAlter_sek();
        switch (enumZeit) {
            case REAL_SEKUNDEN: return alter_sekunden;
            case INGAME_MONAT: return alter_sekunden / KonstantenBalance.SEKUNDEN_PRO_MONAT;
            case INGAME_JAHR: return alter_sekunden / KonstantenBalance.SEKUNDEN_PRO_JAHR;
            default: throw new IllegalArgumentException();
        }
    }

    // EINFACHE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public float getSchlaf() {
        return schlaf;
    }

    public float getFitness() {
        return fitness;
    }

    public Float getGesundheit() {
        return gesundheit;
    }

    public void setSchlaf(float schlaf) {
        this.schlaf = schlaf;
    }
}
