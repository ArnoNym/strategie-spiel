package de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dorfbewohner.tauschen.step;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

import java.beans.PropertyChangeEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import de.arnonym.strategiespiel.framework.listener.StoryTeller;
import de.arnonym.strategiespiel.framework.werkzeuge.collections.Collector;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.bewegung.BewegungComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.bewegung.BewegungszielComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.gegenstaende.BasisGegComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.GeldComp;
import de.arnonym.strategiespiel.spielSchirm.ui.aufsteigendeSymbole.AufsteigendesSymbolFactory;
import de.arnonym.strategiespiel.spielSchirm.StepNGZ;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.Geld;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.endstufe.Gegenstand;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.undPreis.IntEintragHashMap;
import de.arnonym.strategiespiel.framework.werkzeuge.collections.hashMaps.IntIntHashMap;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.undPreis.MengePreisEintrag;
import de.arnonym.strategiespiel.spielSchirm.Enums;
import de.arnonym.strategiespiel.framework.werkzeuge.pause.DeltaPause;
import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenBalance;
import de.arnonym.strategiespiel.framework.werkzeuge.collections.id.IdList;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.CollectionUtils;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.MathUtils;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.SpecialUtils;

public abstract class StepTauschen extends StepNGZ {
    public final AufsteigendesSymbolFactory aufsteigendesSymbolFactory;
    public final BewegungszielComp bewegungszielComp;
    public final BasisGegComp nehmenAusBasisGegComp;
    public final BasisGegComp legenInBasisGegComp;
    public final IdList<Gegenstand> nehmenAusGegList;
    public final IdList<Gegenstand> legenInGegList;
    public final GeldComp nehmenVonGeldComp_nl;
    public final GeldComp legenInGeldComp_nl;
    public final boolean bezahlen;
    private final IntIntHashMap preiseHashMap;
    private final IntIntHashMap mengenHashMap;
    private int ausgaben;
    private final IntEintragHashMap genommenHashMap = new IntEintragHashMap();
    private DeltaPause stepErledigtPause;

    // Nie bezahlen und nie reservieren
    public StepTauschen(AufsteigendesSymbolFactory aufsteigendesSymbolFactory,

                        BewegungComp bewegungComp, BewegungszielComp bewegungszielComp,

                        BasisGegComp<?> nehmenAusBasisGegComp,
                        BasisGegComp<?> legenInBasisGegComp,

                        Vector2 start_nl, StoryTeller... zgStoryTeller) {
        this(aufsteigendesSymbolFactory, bewegungComp, bewegungszielComp, nehmenAusBasisGegComp,
                legenInBasisGegComp, null, null, start_nl,

                false, false, null, new IntIntHashMap(), zgStoryTeller);

        for (Gegenstand g : nehmenAusBasisGegComp.getList()) {
            if (legenInBasisGegComp.getList().contains(g.gettId())) {
                mengenHashMap.put(g.gettId(), -1);
            }
        }
    }

    // Nie bezahlen aber bei Bedarf reservieren
    public StepTauschen(AufsteigendesSymbolFactory aufsteigendesSymbolFactory,

                        BewegungComp bewegungComp, BewegungszielComp bewegungszielComp,

                        BasisGegComp<?> nehmenAusBasisGegComp,
                        BasisGegComp<?> legenInBasisGegComp,

                        IntIntHashMap mengenHashMap, Enums.EnumReservieren enumReservieren,
                        Vector2 start_nl, StoryTeller... zgStoryTeller) {
        this(aufsteigendesSymbolFactory, bewegungComp, bewegungszielComp, nehmenAusBasisGegComp,
                legenInBasisGegComp, null, null, start_nl,

                false, enumReservieren != Enums.EnumReservieren.SPAETER_RESERVIEREN, null,
                mengenHashMap, zgStoryTeller);

        if (enumReservieren == Enums.EnumReservieren.JETZT_RESERVIEREN) {
            for (Map.Entry<Integer, Integer> entry : mengenHashMap.entrySet()) {
                int id = entry.getKey();

                Gegenstand nehmenAusGeg = nehmenAusBasisGegComp.getList().gettDurchId(id);
                Gegenstand legenInGeg = legenInBasisGegComp.getList().gettDurchId(id);

                int menge = MathUtils.min(entry.getValue(),
                        nehmenAusGeg.gettMenge(true, false, false),
                        legenInGeg.getMengeBisVoll());

                entry.setValue(menge);
                nehmenAusGeg.reservierenNehmen(menge);
                legenInGeg.reservierenLegen(menge);
            }
        }
    }

    // Immer bezahlen und reservieren
    public StepTauschen(AufsteigendesSymbolFactory aufsteigendesSymbolFactory,

                        BewegungComp bewegungComp, BewegungszielComp bewegungszielComp,

                        BasisGegComp<?> nehmenAusBasisGegComp,
                        BasisGegComp<?> legenInBasisGegComp,

                        GeldComp nehmenVonCompGeld, GeldComp legenInCompGeld,

                        IntEintragHashMap mengenPreisHashMap, boolean bereitsReserviert,
                        Vector2 start_nl, StoryTeller... zgStoryTeller) {
        this(aufsteigendesSymbolFactory, bewegungComp, bewegungszielComp, nehmenAusBasisGegComp,
                legenInBasisGegComp, nehmenVonCompGeld, legenInCompGeld, start_nl,

                true, true, new IntIntHashMap(), new IntIntHashMap(), zgStoryTeller);

        ausgaben = 0;
        for (Map.Entry<Integer, MengePreisEintrag> entry : mengenPreisHashMap.entrySet()) {
            int id = entry.getKey();

            int menge;
            if (bereitsReserviert) {
                menge = entry.getValue().getMenge();
            } else {
                Gegenstand nehmenAusGeg = nehmenAusBasisGegComp.getList().gettDurchId(id);
                Gegenstand legenInGeg = legenInBasisGegComp.getList().gettDurchId(id);

                menge = MathUtils.min(entry.getValue().getMenge(),
                        nehmenAusGeg.gettMenge(true, false, false),
                        legenInGeg.getMengeBisVoll());

                nehmenAusGeg.reservierenNehmen(menge);
                legenInGeg.reservierenLegen(menge);
            }

            int preis = entry.getValue().getPreis();

            this.preiseHashMap.put(id, preis);
            this.mengenHashMap.put(id, menge);

            ausgaben += MathUtils.myIntMult(menge, preis);
        }
        if (!bereitsReserviert) {
            this.nehmenVonGeldComp_nl.getGeld().reservierenNehmen(ausgaben);
            this.legenInGeldComp_nl.getGeld().reservierenLegen(ausgaben);
        }
    }

    private StepTauschen(AufsteigendesSymbolFactory aufsteigendesSymbolFactory,

                         BewegungComp bewegungComp, BewegungszielComp bewegungszielComp,

                         BasisGegComp<?> nehmenAusBasisGegComp,
                         BasisGegComp<?> legenInBasisGegComp,

                         GeldComp nehmenVonGeldComp_nl, GeldComp legenInGeldComp_nl,

                         Vector2 start_nl, boolean bezahlen, boolean reserviertInConstructor,
                         IntIntHashMap preiseHashMap, IntIntHashMap mengenHashMap,
                         StoryTeller... zgStoryTeller) {
        super(bewegungszielComp, CollectionUtils.combine(zgStoryTeller,
                new Collector<StoryTeller>(nehmenAusBasisGegComp, legenInBasisGegComp)
                        .addMayNull(nehmenVonGeldComp_nl)
                        .addMayNullOrDouble(legenInGeldComp_nl)
                        .toArray(new StoryTeller[]{})),
                bewegungComp, start_nl
        );
        this.aufsteigendesSymbolFactory = aufsteigendesSymbolFactory;
        this.bewegungszielComp = bewegungszielComp;
        this.nehmenAusBasisGegComp = nehmenAusBasisGegComp;
        this.legenInBasisGegComp = legenInBasisGegComp;
        this.nehmenAusGegList = nehmenAusBasisGegComp.getList();
        this.legenInGegList = legenInBasisGegComp.getList();
        this.nehmenVonGeldComp_nl = nehmenVonGeldComp_nl;
        this.legenInGeldComp_nl = legenInGeldComp_nl;
        this.bezahlen = bezahlen;
        setReserviert(reserviertInConstructor);
        this.preiseHashMap = preiseHashMap;
        this.mengenHashMap = mengenHashMap;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    protected Enums.EnumStpStatus versuchen_or_or() {
        /* Entfer ungueltige Eintraege. Wenn mit dem Ziel nichts passendes getauscht werden kann
        ist das ein Fail */
        List<Integer> removeList = new ArrayList<>();
        for (Map.Entry<Integer, Integer> entry : mengenHashMap.entrySet()) {
            int id = entry.getKey();
            Gegenstand nehmenGeg = nehmenAusGegList.gettDurchId(id);
            Gegenstand legenGeg = legenInGegList.gettDurchId(id);
            int menge = entry.getValue();
            if (nehmenGeg == null || legenGeg == null || menge == 0) {
                removeList.add(entry.getKey());
            }
        }
        for (Integer id : removeList) {
            mengenHashMap.remove(id);
        }
        if (mengenHashMap.isEmpty()) {
            return Enums.EnumStpStatus.FAIL;
        }
        return Enums.EnumStpStatus.OK;
    }

    @Override
    protected void failNachVersuchenVorAnfangen_or() {
        if (isReserviert()) {
            reservierenRueckgaengig();
        }
    }

    @Override
    protected void anfangen_or() {
        stepErledigtPause = new DeltaPause(KonstantenBalance
                .ZIVIL_OBJEKT_MENGEN_INTERAKTION_DAUER, Enums.EnumPauseStart.JETZT);

        // Reservieren wenn noch nicht in Constructor geschehen
        if (isReserviert()) {
            return;
        }
        for (Map.Entry<Integer, Integer> entry : mengenHashMap.entrySet()) {
            int key = entry.getKey();
            int value = entry.getValue();

            Gegenstand nehmenAusGeg = nehmenAusGegList.gettDurchId(key);
            Gegenstand legenInGeg = legenInGegList.gettDurchId(key);

            int menge = MathUtils.min(nehmenAusGeg.getMengeVerfuegbar(),
                    legenInGeg.getMengeBisVoll());
            if (value != -1) {
                menge = Math.min(value, menge);
            }

            nehmenAusGeg.reservierenNehmen(menge);
            legenInGeg.reservierenLegen(menge);
            entry.setValue(menge);
        }
        setReserviert(true);
    }

    @Override
    protected void failNachAnfangenVorDurchfuehren_or() {
        reservierenRueckgaengig();
    }

    @Override
    protected void durchfuehren_or(float delta) {
        if (!stepErledigtPause.issVorbei(delta, false)) {
            return;
        }
        for (Map.Entry<Integer, Integer> entry : mengenHashMap.entrySet()) {
            int id = entry.getKey();
            Gegenstand nehmenAusGeg = nehmenAusGegList.gettDurchId(id);
            Gegenstand legenInGeg = legenInGegList.gettDurchId(id);

            int menge = entry.getValue();
            nehmenAusGeg.mengeVerringern(menge, true);
            legenInGeg.mengeErhoehen(menge, true);

            int preis = 0;
            mengenHashMap.put(id, mengenHashMap.get(id) - menge);
            genommenHashMap.put(id, new MengePreisEintrag(menge, preis));
        }
        if (bezahlen) {
            nehmenVonGeldComp_nl.mengeVerringern(ausgaben, true);
            legenInGeldComp_nl.mengeErhoehen(ausgaben, true);
        }
    }

    @Override
    protected void failNachDurchfuehren_or() {

    }

    @Override
    public boolean pruefenObErledigt() {
        if (stepErledigtPause.issVorbei()) {
            return true;
        }

        return mengenHashMap.summeAllerValues() == 0; /* nicht <0 aufrgund des -1, dass vor
        anfangen() noch in der HashMap stehen kann */
    }

    @Override
    public boolean pruefenObFail() {
        return false;
    }

    @Override
    public void erledigt_or() {
        aufsteigendesSymbolFactory.create(gettSymbolTextureRegion(),
                bewegungszielComp.compKollisionLaufen.positionComp.getPosition());
    }

    protected abstract TextureRegion gettSymbolTextureRegion();

    // RESERVIEREN ////////////////////////////////////////////////////////////////////////////////

    protected void reservierenRueckgaengig_or() {
        for (Map.Entry<Integer, Integer> entry : mengenHashMap.entrySet()) {
            int id = entry.getKey();
            int value = entry.getValue();
            nehmenAusGegList.gettDurchId(id).reservierenNehmenRueckgaengig(value);
            legenInGegList.gettDurchId(id).reservierenLegenRueckgaengig(value);
        }
        if (bezahlen) {
            int zuZahlenGeldMenge = 0;
            for (Map.Entry<Integer, Integer> entry : preiseHashMap.entrySet()) {
                zuZahlenGeldMenge += MathUtils.myIntMult(entry.getValue(),
                        mengenHashMap.get(entry.getKey()));
            }
            nehmenVonGeldComp_nl.getGeld().reservierenNehmenRueckgaengig(zuZahlenGeldMenge);
            legenInGeldComp_nl.getGeld().reservierenLegenRueckgaengig(zuZahlenGeldMenge);
        }
    }

    // PROPERTY CHANGE ////////////////////////////////////////////////////////////////////////////

    @Override
    public boolean propertyChange_or_or(PropertyChangeEvent propertyChangeEvent) {
        String name = propertyChangeEvent.getPropertyName();

        if (name.equals(Enums.PC_EVENT_NAME_GELD)) {
            SpecialUtils.changeGeld(
                    (Geld) propertyChangeEvent.getOldValue(),
                    (Geld) propertyChangeEvent.getNewValue(),
                    nehmenVonGeldComp_nl,
                    legenInGeldComp_nl,
                    ausgaben);
            return false;
        } else if (name.equals(Enums.PC_EVENT_NAME_MASTER_TASCHE)) {
            return true;
        }
        throw new IllegalStateException();
    }

    @Override
    protected void addPropertyChangeListeners() {
        nehmenAusBasisGegComp.addPropertyChangeListener(this);
        legenInBasisGegComp.addPropertyChangeListener(this);
        if (nehmenVonGeldComp_nl != null) nehmenVonGeldComp_nl.addPropertyChangeListener(this);
        if (legenInGeldComp_nl != null) legenInGeldComp_nl.addPropertyChangeListener(this);
    }

    @Override
    protected void removePropertyChangeListeners() {
        nehmenAusBasisGegComp.removePropertyChangeListener(this);
        legenInBasisGegComp.removePropertyChangeListener(this);
        if (nehmenVonGeldComp_nl != null) nehmenVonGeldComp_nl.removePropertyChangeListener(this);
        if (legenInGeldComp_nl != null) legenInGeldComp_nl.removePropertyChangeListener(this);
    }

    // EINFACHE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public IntEintragHashMap getGenommenHashMap() {
        return genommenHashMap; // todo nicht roiginal uebergeben
    }
}