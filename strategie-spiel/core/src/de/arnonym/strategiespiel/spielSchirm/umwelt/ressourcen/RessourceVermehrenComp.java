package de.arnonym.strategiespiel.spielSchirm.umwelt.ressourcen;

import de.arnonym.strategiespiel.framework.ecs.component.Comp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.kollision.KollisionComp;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.spielSchirm.umwelt.VermehrenComp;

public class RessourceVermehrenComp extends VermehrenComp {
    public final RessourcenFactory ressourcenFactory;
    public final KollisionComp kollisionComp;
    public final PflegenComp pflegenComp;

    public RessourceVermehrenComp(RessourcenFactory ressourcenFactory, Entity entity,
                                  SysResVermehren sysResVermehren, KollisionComp kollisionComp,
                                  PflegenComp pflegenComp, int id, float prop_sekunde) {
        super(sysResVermehren, entity, new Comp[]{kollisionComp, pflegenComp}, id, prop_sekunde);
        this.ressourcenFactory = ressourcenFactory;
        this.kollisionComp = kollisionComp;
        this.pflegenComp = pflegenComp;
    }
}
