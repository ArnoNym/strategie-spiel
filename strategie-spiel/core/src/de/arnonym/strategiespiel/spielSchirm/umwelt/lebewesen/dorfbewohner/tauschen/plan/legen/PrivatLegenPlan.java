package de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dorfbewohner.tauschen.plan.legen;

import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import de.arnonym.strategiespiel.spielSchirm.balancing.Konstanten;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.bewegung.BewegungComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.gegenstaende.GegenstaendeComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.EinkommenComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.GeldComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.lager.LagerCompMitGeld;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.lager.LagerCompOhneGeld;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dorfbewohner.tauschen.step.StepLegeIn;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.lager.LagerComp;
import de.arnonym.strategiespiel.spielSchirm.ui.aufsteigendeSymbole.AufsteigendesSymbolFactory;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.listSystems.privateListSystems.SysLager;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dorfbewohner.tauschen.plan.score.ZielGegenstandKombi;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.endstufe.Gegenstand;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.Geld;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.undPreis.IntEintragHashMap;
import de.arnonym.strategiespiel.framework.werkzeuge.collections.hashMaps.IntIntHashMap;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.undPreis.MengePreisEintrag;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dorfbewohner.tauschen.plan.score.PlanTauschenScore;
import de.arnonym.strategiespiel.spielSchirm.Enums;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.MathUtils;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.SpecialUtils;

public class PrivatLegenPlan extends LegenPlan {
    // Sollten nie gleichzeitig befuellt werden
    private IntEintragHashMap intEintragHashMap; // todo zwei sepearete Plan legen in erstellen
    private IntIntHashMap intIntHashMap;

    public PrivatLegenPlan(AufsteigendesSymbolFactory aufsteigendesSymbolFactory,

                           SysLager sysLager,

                           BewegungComp bewegungComp, EinkommenComp einkommenComp,
                           GegenstaendeComp aGegenstaendeComp,

                           LagerComp zuvorGenommenAusZiel_nl, boolean mussLohnen,
                           Vector2 startPosition) {
        super(aufsteigendesSymbolFactory, sysLager, einkommenComp.geldComp,
                einkommenComp.getEinkommen(), bewegungComp, einkommenComp.geldComp,
                aGegenstaendeComp, zuvorGenommenAusZiel_nl, mussLohnen, startPosition);
        init();
    }

    public PrivatLegenPlan(AufsteigendesSymbolFactory aufsteigendesSymbolFactory,

                           SysLager sysLager,

                           BewegungComp bewegungComp, GeldComp geldComp,
                           GegenstaendeComp aGegenstaendeComp, int hypotheticSalary,

                           LagerComp zuvorGenommenAusZiel_nl, boolean mussLohnen,
                           Vector2 startPosition) {
        super(aufsteigendesSymbolFactory, sysLager, geldComp, hypotheticSalary, bewegungComp,
                geldComp, aGegenstaendeComp, zuvorGenommenAusZiel_nl, mussLohnen, startPosition);
        init();
    }

    @Override
    protected List<LagerComp> gettZieleList(SysLager sysLager) {
        List<LagerComp> zieleList = new ArrayList<>();
        boolean mussLohnen = isMussLohnen();
        for (LagerComp lagerComp : sysLager.toCollection(new LinkedList<>())) {
            if (!mussLohnen || lagerComp instanceof LagerCompMitGeld) {
                zieleList.add(lagerComp);
            }
        }
        return zieleList;
    }

    @Override
    protected void neuesZiel_override() {
        intEintragHashMap = new IntEintragHashMap();
        intIntHashMap = new IntIntHashMap();
    }

    @Override
    protected void handeln(Map.Entry<ZielGegenstandKombi, PlanTauschenScore> bestEntry) {
        if (bestEntry.getKey().getZiel() instanceof LagerCompOhneGeld) {
            handelnUebergeben(bestEntry);
        } else {
            handelnVerkaufen(bestEntry);
        }
    }

    private void handelnVerkaufen(Map.Entry<ZielGegenstandKombi, PlanTauschenScore> bestEntry) {
        ZielGegenstandKombi zielGegenstandKombi = bestEntry.getKey();
        LagerComp ziel = zielGegenstandKombi.getZiel();
        Geld zielGeld = ((LagerCompMitGeld) ziel).geldComp.getGeld();
        Gegenstand zielGeg = zielGegenstandKombi.getGegenstand();

        int id = zielGeg.gettId();
        Gegenstand akteurGeg = aGegenstaendeComp.gegenstandListe.gettDurchId(id);

        int verkaufenMenge = Math.min(
                akteurGeg.gettMenge(true, true, false),
                zielGeg.getMengeBisVoll());
        if (verkaufenMenge == 0) {
            getFeedback().addText("PlanLegen ::: verkaufenMenge == 0");
            return;
        }

        int preis = zielGeg.getWert();
        int geldMenge = zielGeld.getMengeVerfuegbar();
        if (MathUtils.myIntMult(verkaufenMenge, preis) > geldMenge) {
            int kannBezahlenMenge = MathUtils.myIntDiv(geldMenge, preis);
            if (!zielGeg.isTeilbar()) {
                kannBezahlenMenge = MathUtils.myIntRundenAufGanzeZahl(kannBezahlenMenge);
            }
            if (kannBezahlenMenge == 0) {
                getFeedback().addText("PlanLegen ::: kannBezahlenMenge == 0");
                return;
            }
            verkaufenMenge = Math.min(verkaufenMenge, kannBezahlenMenge);
        }

        intEintragHashMap.put(id, new MengePreisEintrag(verkaufenMenge, preis));
        SpecialUtils.reserviere(aGegenstaendeComp.gegenstandListe.gettDurchId(zielGeg.gettId()),
                zielGeg, verkaufenMenge);
        SpecialUtils.reserviere(zielGeld, gGeldComp.getGeld(), verkaufenMenge, preis);
    }

    private void handelnUebergeben(Map.Entry<ZielGegenstandKombi, PlanTauschenScore> bestEntry) {
        Gegenstand reakteurGeg = bestEntry.getKey().getGegenstand();

        int id = reakteurGeg.gettId();
        Gegenstand akteurGeg = aGegenstaendeComp.gegenstandListe.gettDurchId(id);

        int mengeWeglegen = Math.min(akteurGeg.gettMenge(true, true, false),
                reakteurGeg.getMengeBisVoll());

        if (mengeWeglegen == 0) {
            getFeedback().addText("PlanLegen ::: mengeWeglegen == 0");
            return;
        }

        SpecialUtils.reserviere(akteurGeg, reakteurGeg, mengeWeglegen);
        intIntHashMap.put(id, mengeWeglegen);
    }

    @Override
    protected StepLegeIn gettNeuerStep(LagerComp ziel) {
        StepLegeIn neuerStep;
        if (ziel instanceof LagerCompOhneGeld) {
            if (intIntHashMap.isEmpty()) {
                getFeedback().addText("PlanLegen ::: intIntHashMap.isEmpty()");
                return null;
            }
            neuerStep = new StepLegeIn(
                    aufsteigendesSymbolFactory,
                    aBewegungComp,
                    ziel.bewegungszielComp,
                    aGegenstaendeComp,
                    ziel.gegenstaendeComp,
                    gGeldComp,
                    intIntHashMap,
                    Enums.EnumReservieren.BEREITS_RESERVIERT,
                    getPlanenPosition());
        } else {
            if (intEintragHashMap.isEmpty()) {
                getFeedback().addText("PlanLegen ::: intEintragHashMap.isEmpty()");
                return null;
            }
            neuerStep = new StepLegeIn(
                    aufsteigendesSymbolFactory,
                    aBewegungComp,
                    ziel.bewegungszielComp,
                    aGegenstaendeComp,
                    ziel.gegenstaendeComp,
                    ((LagerCompMitGeld) ziel).geldComp,
                    gGeldComp,
                    gGeldComp,
                    intEintragHashMap,
                    true,
                    getPlanenPosition());
        }
        return neuerStep;
    }

    @Override
    protected void ende_or() {

    }

    // PRUEFEN ////////////////////////////////////////////////////////////////////////////////////

    @Override
    protected boolean pruefeTotal() {
        if (!aGegenstaendeComp.gegenstandListe.issMenge(true, true, false)) {
            getFeedback().addText("Akteur hat keine Menge zum legen!");
            ende();
            return false;
        }
        return true;
    }

    @Override
    protected boolean basicPruefeZiel(LagerComp ziel) {
        if (ziel == zuvorGenommenAusZiel_nl) {
            if (Konstanten.DEVELOPER_VISUELL_STEP_SYSTEM_OUT) {
                getFeedback().addText("PlanLegen ::: ziel aussortiert : weil aus diesem Ziel genommen");
            }
            return false;
        }
        if (ziel.gegenstaendeComp.gegenstandListe.getPlatz() == 0) {
            if (Konstanten.DEVELOPER_VISUELL_STEP_SYSTEM_OUT) {
                getFeedback().addText("PlanLegen ::: ziel aussortiert : weil keinen Platz");
            }
            return false;
        }
        return true;
    }

    @Override
    protected boolean basicPruefeGeg(Gegenstand geg) {
        Gegenstand dbGeg = aGegenstaendeComp.gegenstandListe.gettDurchId(geg.gettId());
        if (geg.getMengeBisVoll() == 0) {
            if (Konstanten.DEVELOPER_VISUELL_STEP_SYSTEM_OUT) {
                getFeedback().addText("PlanLegen ::: geg aussortiert : weil voll");
            }
            return false;
        }
        if (dbGeg == null) {
            if (Konstanten.DEVELOPER_VISUELL_STEP_SYSTEM_OUT) {
                getFeedback().addText("PlanLegen ::: geg aussortiert : weil null");
            }
            return false;
        }
        if (!dbGeg.issMenge(true, true, false)) {
            if (Konstanten.DEVELOPER_VISUELL_STEP_SYSTEM_OUT) {
                getFeedback().addText("PlanLegen ::: geg aussortiert : weil keine > 0 Menge");
            }
            return false;
        }
        return true;
    }
}
