package de.arnonym.strategiespiel.spielSchirm.umwelt.ressourcen;

import de.arnonym.strategiespiel.framework.ecs.component.Comp;
import de.arnonym.strategiespiel.spielSchirm.arbeit.auftrag.Assignment;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.bewegung.BewegungszielComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.werte.ProduktivitaetComp;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenBalance;

public class PflegenComp extends Comp {
    public final BewegungszielComp bewegungszielComp;

    private Assignment assignment;
    private float pflege;

    public PflegenComp(Entity entity, BewegungszielComp bewegungszielComp,
                       Assignment assignment) {
        this(entity, bewegungszielComp, assignment, 1);
    }

    public PflegenComp(Entity entity, BewegungszielComp bewegungszielComp,
                       Assignment assignment, float pflegeZustand) {
        super(entity, bewegungszielComp);
        this.bewegungszielComp = bewegungszielComp;
        this.assignment = assignment;
        this.pflege = pflegeZustand;
    }

    public float gettPflegenDelta(float delta) {
        float deltaPflege = pflege * delta;
        pflege = Math.max(pflege - KonstantenBalance.RES_PFLEGE_ABBAUEN_PROZENT_sekunde * delta, 1);
        return deltaPflege;
    }

    public void gepflegt(ProduktivitaetComp produktivitaetComp) {
        pflege += KonstantenBalance.RES_PFLEGE_AUFBAUEN_PROZENT
                * produktivitaetComp.realProduktivitaet(assignment);
    }

    public Assignment getAssignment() {
        return assignment;
    }

    public void setAssignment(Assignment assignment) {
        this.assignment = assignment;
    }

    public float getPflege() { // todo verwenden
        return pflege;
    }
}
