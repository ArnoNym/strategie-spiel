package de.arnonym.strategiespiel.spielSchirm.umwelt.ressourcen.stufen.sekundaer;

import de.arnonym.strategiespiel.spielSchirm.umwelt.ressourcen.stufen.WachstumInfo;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.vorstufe.EntityGegInfoList;
import de.arnonym.strategiespiel.framework.werkzeuge.textures.TextureHashMap;

public class SWachstumInfo extends WachstumInfo {
    public SWachstumInfo(TextureHashMap textureHashMap,
                         EntityGegInfoList entityGegInfoList, float stufeDauer_sek) {
        super(textureHashMap, entityGegInfoList, stufeDauer_sek);
    }
}
