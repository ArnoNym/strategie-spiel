package de.arnonym.strategiespiel.spielSchirm.umwelt.ressourcen;

import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.departments.bildung.ForschungAssignment;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.departments.gebiet.RessourcenAssignment;
import de.arnonym.strategiespiel.spielSchirm.info.muster.Info;

public abstract class RessourceInfo extends Info {
    public final RessourcenAssignment abbauenAssignment;

    public RessourceInfo(int id, String name, String beschreibung,
                         ForschungAssignment forschungAssignment_nl,
                         RessourcenAssignment abbauenAssignment) {
        super(id, name, beschreibung, forschungAssignment_nl);
        this.abbauenAssignment = abbauenAssignment;
    }


}
