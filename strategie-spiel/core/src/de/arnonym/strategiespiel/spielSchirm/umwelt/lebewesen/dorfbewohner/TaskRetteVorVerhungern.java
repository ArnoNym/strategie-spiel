package de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dorfbewohner;

import de.arnonym.strategiespiel.framework.stp.StpComp;
import de.arnonym.strategiespiel.framework.stp.Task;
import de.arnonym.strategiespiel.spielSchirm.Dorf;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.bewegung.BewegungComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.gegenstaende.TascheComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.EinkommenComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.GeldComp;
import de.arnonym.strategiespiel.spielSchirm.ui.aufsteigendeSymbole.AufsteigendesSymbolFactory;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.listSystems.privateListSystems.SysLager;
import de.arnonym.strategiespiel.framework.stp.stacks.StepStack;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dorfbewohner.tauschen.plan.nehmen.PlanNehmenPrivat;
import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenBalance;

public class TaskRetteVorVerhungern extends Task {
    public final Dorf dorf;
    public final AufsteigendesSymbolFactory aufsteigendesSymbolFactory;
    public final SysLager sysLager;
    public final BewegungComp bewegungComp;
    public final EinkommenComp einkommenComp;
    public final GeldComp geldComp;
    public final TascheComp tascheComp;

    public TaskRetteVorVerhungern(Dorf dorf, AufsteigendesSymbolFactory aufsteigendesSymbolFactory,
                                  SysLager sysLager,

                                  StpComp stpComp, BewegungComp bewegungComp,
                                  EinkommenComp einkommenComp,
                                  GeldComp geldComp, TascheComp tascheComp) {
        super(stpComp, bewegungComp, einkommenComp, geldComp, tascheComp);
        this.dorf = dorf;
        this.aufsteigendesSymbolFactory = aufsteigendesSymbolFactory;
        this.sysLager = sysLager;
        this.bewegungComp = bewegungComp;
        this.einkommenComp = einkommenComp;
        this.geldComp = geldComp;
        this.tascheComp = tascheComp;
    }

    @Override
    public void planeAlleSteps(StepStack stepStack) {
        stepStack.handle(planeNehmenPrivat(geldComp));

        if (tascheComp.getMasterTasche().getNahrungTasche_nl().gettMenge(true, true, false)
                < KonstantenBalance.DORFBEWOHNER_NAHRUNG_GRUNDBEDARF_AUFFUELLEN_BIS) {
            stepStack.handle(planeNehmenPrivat(dorf.eigentuemerComp.geldComp));
        }
    }

    private PlanNehmenPrivat planeNehmenPrivat(GeldComp geldComp) {
        return new PlanNehmenPrivat(aufsteigendesSymbolFactory, sysLager, bewegungComp,
                einkommenComp, tascheComp, false, bewegungComp.positionComp.position);
    }

    @Override
    public void planeNaechstenStep(StepStack stepStack) {

    }
}
