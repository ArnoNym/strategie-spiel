package de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dna;

import de.arnonym.strategiespiel.framework.ecs.component.Comp;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;

public abstract class CompDna extends Comp {

    public CompDna(Entity entity) {
        super(entity);
    }
}