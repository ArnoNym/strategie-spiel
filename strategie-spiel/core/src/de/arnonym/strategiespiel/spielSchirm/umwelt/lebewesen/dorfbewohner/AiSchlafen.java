package de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dorfbewohner;

import de.arnonym.strategiespiel.framework.stp.Ai;
import de.arnonym.strategiespiel.framework.ecs.component.Comp;
import de.arnonym.strategiespiel.framework.stp.StpComp;
import de.arnonym.strategiespiel.framework.stp.Task;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.rendern.RenderComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.wohnen.EinwohnerComp;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.framework.stp.StpMachine;
import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenAiPrioritaet;
import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenBalance;

public class AiSchlafen extends Ai {
    public final EinwohnerComp einwohnerComp;

    public AiSchlafen(Entity entity, RenderComp renderComp, StpComp stpComp,
                      EinwohnerComp einwohnerComp) {
        super(entity, renderComp, stpComp, new Comp[]{einwohnerComp},
                KonstantenAiPrioritaet.SCHLAFEN);
        this.einwohnerComp = einwohnerComp;
    }

    @Override
    public Task versuchen() {
        if (einwohnerComp.habitalwerteComp.getSchlaf() > KonstantenBalance.DORFBEWOHNER_SCHLAFEN_GEHEN) {
            return null;
        }
        return new TaskSchlafen(stpComp, einwohnerComp);
    }
}
