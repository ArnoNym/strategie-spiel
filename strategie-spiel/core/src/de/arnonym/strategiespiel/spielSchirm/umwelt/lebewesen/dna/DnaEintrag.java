package de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dna;

import com.badlogic.gdx.math.MathUtils;

import java.io.Serializable;
import java.util.concurrent.ThreadLocalRandom;

import de.arnonym.strategiespiel.spielSchirm.Enums;

class DnaEintrag implements Serializable {
    private String name;
    private float minValue;
    private float maxValue;
    private Dna motherDna;
    private Dna fatherDna;

    public DnaEintrag(String name, float minValue, float maxValue, Dna motherDna, Dna fatherDna) {
        this.name = name;
        this.minValue = minValue;
        this.maxValue = maxValue;
        this.motherDna = motherDna;
        this.fatherDna = fatherDna;
    }

    public Dna gettSingleDna() {
        if (ThreadLocalRandom.current().nextBoolean()) {
            return motherDna;
        } else {
            return fatherDna;
        }
    }

    public float gettSingleValue() {
        if (motherDna.getEnumDnaStatus() == Enums.EnumDnaStatus.NUTZEN) { //Dann muss auch fatherDna Nutzen sein
            return gettNutzenExponenten(motherDna.getValue(), fatherDna.getValue());
        }
        if (motherDna.getEnumDnaStatus() == Enums.EnumDnaStatus.MULT
                && fatherDna.getEnumDnaStatus() == Enums.EnumDnaStatus.MULT) {
            return minValue;
        }
        if (motherDna.getEnumDnaStatus() == Enums.EnumDnaStatus.MULT
                || fatherDna.getEnumDnaStatus() == Enums.EnumDnaStatus.MULT) {
            return MathUtils.clamp(motherDna.getValue() * fatherDna.getValue(), minValue, maxValue);
        }
        return Math.max(motherDna.getValue(), fatherDna.getValue());
    }

    private float gettNutzenExponenten(float nutzenExponentParent1, float nutzenExponentParent2) {
        float distributedAround = (nutzenExponentParent1 + nutzenExponentParent2) / 2;
        float standardDeviation = Math.abs(nutzenExponentParent1 - nutzenExponentParent2) / 2;
        return (float) com.badlogic.gdx.math.MathUtils.clamp(ThreadLocalRandom.current()
                .nextGaussian() * standardDeviation + distributedAround, minValue, maxValue);
    }

    // EINFACHE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public String getName() {
        return name;
    }

    public float getMinValue() {
        return minValue;
    }

    public float getMaxValue() {
        return maxValue;
    }
}
