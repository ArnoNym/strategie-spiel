package de.arnonym.strategiespiel.spielSchirm.umwelt.ressourcen.stufen;

import de.arnonym.strategiespiel.framework.werkzeuge.textures.Texture;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.departments.gebiet.RessourcenAssignment;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.departments.bildung.ForschungAssignment;
import de.arnonym.strategiespiel.spielSchirm.umwelt.ressourcen.RessourceInfo;
import de.arnonym.strategiespiel.spielSchirm.Enums;

public abstract class WachstumRessourceInfo<RI extends WachstumInfo> extends RessourceInfo {
    public final RessourcenAssignment pflegeAssignment;
    public final float vermehrenProp_sekunde;
    public final RI ressourceInfo_jung;
    public final RI ressourceInfo_mittel;
    public final RI ressourceInfo_alt;

    public WachstumRessourceInfo(int id, String name, String beschreibung,
                                 ForschungAssignment forschungAssignment_nl,
                                 RessourcenAssignment abbauenAssignment,
                                 RessourcenAssignment pflegeAssignment,
                                 float vermehrenProp_sekunde,
                                 RI ressourceInfo_jung, RI ressourceInfo_mittel,
                                 RI ressourceInfo_alt) {
        super(id, name, beschreibung, forschungAssignment_nl, abbauenAssignment);
        this.pflegeAssignment = pflegeAssignment;
        this.vermehrenProp_sekunde = vermehrenProp_sekunde;
        this.ressourceInfo_jung = ressourceInfo_jung;
        this.ressourceInfo_mittel = ressourceInfo_mittel;
        this.ressourceInfo_alt = ressourceInfo_alt;
    }

    public RI get(Enums.EnumResNwStufe alter) {
        switch (alter) {
            case JUNG: return ressourceInfo_jung;
            case MITTEL: return ressourceInfo_mittel;
            case ALT: return ressourceInfo_alt;
            default: throw new IllegalStateException();
        }
    }

    @Override
    public Texture getIconTexture() {
        return ressourceInfo_alt.textureHashMap.get(Enums.EnumAktion.STANDARD_PRIMAERMENGE_VOLL);
    }
}
