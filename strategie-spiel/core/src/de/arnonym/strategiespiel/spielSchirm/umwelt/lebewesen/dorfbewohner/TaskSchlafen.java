package de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dorfbewohner;

import de.arnonym.strategiespiel.framework.stp.StpComp;
import de.arnonym.strategiespiel.framework.stp.Task;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.bewegung.BewegungComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.bewegung.BewegungszielComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dna.DbDnaComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.werte.HabitalwerteComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.wohnen.EinwohnerComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.wohnen.WohnortComp;
import de.arnonym.strategiespiel.framework.stp.stacks.StepStack;
import de.arnonym.strategiespiel.spielSchirm.Enums;

public class TaskSchlafen extends Task {
    public final BewegungComp bewegungComp;
    public final DbDnaComp dbDnaComp;
    public final EinwohnerComp einwohner_Comp_nl;
    public final HabitalwerteComp habitalwerteComp;

    public TaskSchlafen(StpComp stpComp, EinwohnerComp einwohnerComp) {
        super(stpComp, einwohnerComp);
        this.bewegungComp = einwohnerComp.bewegungComp;
        this.dbDnaComp = einwohnerComp.dbDnaComp;
        this.habitalwerteComp = einwohnerComp.habitalwerteComp;
        this.einwohner_Comp_nl = einwohnerComp;
    }

    public TaskSchlafen(StpComp stpComp, BewegungComp bewegungComp,
                        DbDnaComp dbDnaComp, HabitalwerteComp habitalwerteComp) { // Fuer Tiere etc
        super(stpComp, bewegungComp, dbDnaComp, habitalwerteComp);
        this.bewegungComp = bewegungComp;
        this.dbDnaComp = dbDnaComp;
        this.habitalwerteComp = habitalwerteComp;
        this.einwohner_Comp_nl = null;
    }

    @Override
    public void planeAlleSteps(StepStack stepStack) {
        if (einwohner_Comp_nl != null) {
            WohnortComp wohnortComp = einwohner_Comp_nl.getMieterComp().getWohnort();
            if (wohnortComp != null) {
                BewegungszielComp bewegungszielComp = wohnortComp.bewegungszielComp;
                if (!bewegungComp.unerreichbarList.contains(bewegungszielComp)
                        && erstelleStepSchlafe(stepStack, bewegungszielComp)) {
                    return;
                }
            }
        }
        erstelleStepSchlafe(stepStack, bewegungComp.worldMap.gettFreieZelle(
                bewegungComp.positionComp.position, false, true).bewegungszielComp);
    }

    private boolean erstelleStepSchlafe(StepStack stepStack, BewegungszielComp bewegungszielComp) {
        StepSchlafe stepSchlafe = new StepSchlafe(bewegungComp, bewegungszielComp, dbDnaComp,
                habitalwerteComp, bewegungComp.positionComp.position);
        Enums.EnumStpStatus stepSchlafeStatus = stepSchlafe.versuchen();
        if (stepSchlafeStatus == Enums.EnumStpStatus.OK) {
            stepStack.handle(stepSchlafe, true);
            return true;
        }
        return false;
    }

    @Override
    public void planeNaechstenStep(StepStack stepStack) {

    }
}
