package de.arnonym.strategiespiel.spielSchirm.umwelt.ressourcen.einfach;

import com.badlogic.gdx.math.Vector2;

import de.arnonym.strategiespiel.Strategiespiel;
import de.arnonym.strategiespiel.spielSchirm.World;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.ausbeuten.AbbaubarComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.gegenstaende.GegenstaendeComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.gegenstaende.GegenstaendeComp2;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.herrschaft.EigentuemerComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.herrschaft.EigentumComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.klick.FensterKlickComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.ressourcen.Ressource;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterOnKlick.OnKlickFenster;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterOnKlick.OnKlickFensterMenge;
import de.arnonym.strategiespiel.framework.werkzeuge.Paket;

public class StagnantRessource extends Ressource {
    public StagnantRessource(World world, StagnantRessourceInfo stagnantRessourceInfo,
                             EigentuemerComp eigentuemer, Vector2 position) {
        super(world, stagnantRessourceInfo, stagnantRessourceInfo.textureHashMap, eigentuemer, position);
        GegenstaendeComp gegenstaendeComp = new GegenstaendeComp2(this, unbeweglichRenderComp,
                stagnantRessourceInfo.entityGegInfoList);

        Paket<OnKlickFenster> paket = () -> new OnKlickFensterMenge(world, Strategiespiel.skin,
                gegenstaendeComp, unbeweglichRenderComp, nameComp);

        FensterKlickComp fensterKlickComp = new FensterKlickComp(this, world.sysKlick,
                isoGroesseComp, positionComp, unbeweglichRenderComp, paket);

        AbbaubarComp compAbbaubar = new AbbaubarComp(this, world.sysAbbaubar, bewegungszielComp,
                eigentumComp, gegenstaendeComp, unbeweglichRenderComp,
                stagnantRessourceInfo.abbauenAssignment);
    }
}
