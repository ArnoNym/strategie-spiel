package de.arnonym.strategiespiel.spielSchirm.umwelt;

import de.arnonym.strategiespiel.spielSchirm.balancing.Konstanten;
import de.arnonym.strategiespiel.framework.ecs.collections.UpdateSetSystem;
import de.arnonym.strategiespiel.spielSchirm.umwelt.ressourcen.CompVergammeln;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.endstufe.Gegenstand;
import de.arnonym.strategiespiel.framework.werkzeuge.collections.UpdateSet;

public class SysVergammeln extends UpdateSetSystem<CompVergammeln> {
    public SysVergammeln() {
        super(true, (compVergammeln, delta) -> {
            for (Gegenstand g : compVergammeln.gegenstaendeComp.gegenstandListe) {
                g.vergammeln(delta, 1); // TODO Vergammeln Modifikator durch naesse etc
            }
        });
    }
}
