package de.arnonym.strategiespiel.spielSchirm.umwelt;

import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.transformieren.TransComp;

public class CompSterben {
    private final TransComp transComp;
    private boolean tod = false;

    public CompSterben(TransComp transComp) {
        this.transComp = transComp;
    }

    public void sterbenBedingungErfuellt() {
        if (tod) return;
        tod = true;

        transComp.trans();
    }

    public boolean isTod() {
        return tod;
    }
}
