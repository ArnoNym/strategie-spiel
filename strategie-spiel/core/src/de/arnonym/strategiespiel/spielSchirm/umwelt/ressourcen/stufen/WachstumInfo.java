package de.arnonym.strategiespiel.spielSchirm.umwelt.ressourcen.stufen;

import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.vorstufe.EntityGegInfoList;
import de.arnonym.strategiespiel.framework.werkzeuge.textures.TextureHashMap;

public class WachstumInfo {
    public final TextureHashMap textureHashMap;
    public final EntityGegInfoList entityGegInfoList;
    public final float stufeDauer_sek;

    public WachstumInfo(TextureHashMap textureHashMap, EntityGegInfoList entityGegInfoList, float stufeDauer_sek) {
        this.textureHashMap = textureHashMap;
        this.entityGegInfoList = entityGegInfoList;
        this.stufeDauer_sek = stufeDauer_sek;
    }
}
