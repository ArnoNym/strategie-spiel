package de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dorfbewohner.tauschen.plan.nehmen;

import com.badlogic.gdx.math.Vector2;

import java.util.Map;

import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.bewegung.BewegungComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.gegenstaende.TascheComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.EinkommenComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.GeldComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.lager.LagerCompMitGeld;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dorfbewohner.tauschen.step.StepNehmen;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.lager.LagerComp;
import de.arnonym.strategiespiel.spielSchirm.ui.aufsteigendeSymbole.AufsteigendesSymbolFactory;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.listSystems.privateListSystems.SysLager;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dorfbewohner.tauschen.plan.score.PlanTauschenScore;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dorfbewohner.tauschen.plan.score.ZielGegenstandKombi;
import de.arnonym.strategiespiel.spielSchirm.StepNGZ;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.endstufe.Gegenstand;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.endstufe.GegenstandList;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.undPreis.IntEintragHashMap;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.undPreis.MengePreisEintrag;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.taschen.MasterTasche;
import de.arnonym.strategiespiel.spielSchirm.Enums;
import de.arnonym.strategiespiel.spielSchirm.balancing.Konstanten;
import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenBalance;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.MathUtils;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.SpecialUtils;

public class PlanNehmenPrivat extends PlanNehmen {
    public final TascheComp tascheComp;

    private IntEintragHashMap gekauftHashMap;

    public PlanNehmenPrivat(AufsteigendesSymbolFactory aufsteigendesSymbolFactory,

                            SysLager sysLager,

                            BewegungComp bewegungComp, EinkommenComp einkommenComp,
                            TascheComp tascheComp,

                            boolean mussLohnen, Vector2 startPosition) {
        super(aufsteigendesSymbolFactory, sysLager, einkommenComp.getEinkommen(), bewegungComp,
                einkommenComp.geldComp, mussLohnen, startPosition);
        this.tascheComp = tascheComp;
        init();
    }

    @Override
    protected void neuesZiel_override() {
        gekauftHashMap = new IntEintragHashMap();
    }

    @Override
    protected void handeln(Map.Entry<ZielGegenstandKombi, PlanTauschenScore> bestEntry) {
        Gegenstand geg = bestEntry.getKey().getGegenstand();
        MasterTasche masterTasche = tascheComp.getMasterTasche();

        // Kaufen Menge bestimmen
        int kaufenGegId = geg.gettId();
        int kaufeMenge = MathUtils.min(Konstanten.NACHKOMMASTELLEN,
                masterTasche.gettDurchId(kaufenGegId).getMengeBisVoll(),
                geg.getMengeVerfuegbar()); /* Maximal eine ganze Einheit weil sich durch jede Menge
                die Nutzenfunktion aendert */
        if (kaufeMenge == 0) {
            return;
        }

        // Ist genug Geld verfuegbar
        int preis = geg.getWert();
        kaufeMenge = Math.min(kaufeMenge,
                MathUtils.myIntDiv(aGeldComp.getGeld().getMengeVerfuegbar(), preis));
        if (kaufeMenge == 0) {
            return;
        }

        // Lohnt es sich ueberhaupt die Menge zu kaufen
        if (isMussLohnen()) {
            if (geg.gettGrenznutzen() < MathUtils.myIntMult(kaufeMenge, preis)) {
                return;
            }
        } else {
            if (geg.gegMuster.getEnumGegenstandKlasse() != Enums.EnumGegenstandKlasse.NAHRUNG) {
                throw new IllegalStateException();
            }
            kaufeMenge = Math.min(kaufeMenge, KonstantenBalance
                    .DORFBEWOHNER_NAHRUNG_GRUNDBEDARF_AUFFUELLEN_BIS
                    - masterTasche.getNahrungTasche_nl().gettMenge(true, true, false));
            if (kaufeMenge == 0) {
                return;
            }
        }

        // Teilbarkeit beruecksichtigen
        kaufeMenge = MathUtils.myIntRunden(kaufeMenge, geg.isTeilbar());
        if (kaufeMenge == 0) {
            return;
        }

        // Reservieren
        SpecialUtils.reserviere(geg, masterTasche.gettDurchId(kaufenGegId), kaufeMenge);
        SpecialUtils.reserviere(aGeldComp.getGeld(),
                ((LagerCompMitGeld) bestEntry.getKey().getZiel()).geldComp.getGeld(),
                kaufeMenge,
                preis);

        // Einkaufen HashMap aus der spaeter der Steps erstellt wird updaten
        if (gekauftHashMap.containsKey(kaufenGegId)) {
            int alteMenge = gekauftHashMap.get(kaufenGegId).getMenge();
            gekauftHashMap.get(kaufenGegId).setMenge(alteMenge + kaufeMenge);
        } else {
            gekauftHashMap.put(kaufenGegId, new MengePreisEintrag(kaufeMenge, preis));
        }

        // ScoreHashMap erneuer, da sich durch den kauf die Grenznutzen veraendert haben
        setScoreHashMap(gettScoreHashMap(getZieleList(), getPlanenPosition()));
    }

    @Override
    protected StepNGZ gettNeuerStep(LagerComp ziel) {
        if (gekauftHashMap.isEmpty()) {
            return null;
        }
        return new StepNehmen(
                aufsteigendesSymbolFactory,
                aBewegungComp,
                ziel.bewegungszielComp,
                ziel.gegenstaendeComp,
                tascheComp,
                aGeldComp,
                ((LagerCompMitGeld) ziel).geldComp,
                gekauftHashMap,
                true,
                null/*TODO Zelle*/);
        /*return new StepNehmen(getDorfbewohner(), getPlanenPosition(), (ZivilObjekt) getZiel(),
                getAkteur().getCompTasche().getMasterTasche(), getAkteur().getCompGeld().getGeld(),
                gekauftHashMap, true);*/
    }

    @Override
    protected void ende_or() {

    }

    // BEWERTEN ///////////////////////////////////////////////////////////////////////////////////

    @Override
    protected int gettPositiv(Gegenstand g) {
        return g.gettGrenznutzen();
    }

    @Override
    protected int gettNegativ(Gegenstand g) {
        return g.getWert();
    }

    // PRUEFEN ////////////////////////////////////////////////////////////////////////////////////

    @Override
    protected boolean basicPruefeZiel(LagerComp ziel) {
        GegenstandList woGegListe = ziel.gegenstaendeComp.gegenstandListe;
        return woGegListe.getPlatz() != woGegListe.getMaxPlatz();
    }

    @Override
    protected boolean basicPruefeGeg(Gegenstand geg) {
        return geg.getMengeVerfuegbar() > 0;
    }
}











/*
    {
        super(dorfbewohner, mussLohnen);
        dorfbewohnerGeld = dorfbewohner.getGeld();
        Dorf dorf = dorfbewohner.getDorf();
        dorfGeld = dorf.getGeld();
        mundraubErlaubt = dorf.getMundraubErlaubt();

        /* Plant den Einkauf aus der Entfernung bevor das Gekaufte abgeholt wird. Der Einkauf
        wird nicht perfekt durchgeplant mit allen moeglichen Kombinationen sondern es wird immer
        zu dem Gebaude gegangen, dass den Gegenstand (und Preis) bietet der zum groessten
        prozentualen Nutzengewinn fuehrt

        Zelle planenZelle = startZelle;
        do {
            /* Einkaufen in fuer den DB besten baugrube
            1. mit DB Geld
            2. wenn DB pleite und verhungert auch mit Dorf Geld

            Falls der DB kein Gebaude finden kann in dem er etwas bezahlen kann wird das fuer das
            Dorf passende Gebaude ermittelt (Pruefung unterscheidet sich nur durch das verfuegbare
            Geld)
            List<StepNehmen> versuchenStepStack = gettZuVersuchenStepStack(reakteurListe,
                    planenZelle);

            if (versuchenStepStack == null) {
                return;
            }

            // Steps versuchen und dem StepStack hinzufuegen
            for (StepNehmen s : versuchenStepStack) { /* Jeder Step muss versucht() werden! Darauf
                wird sich beim schreiben der Steps verlassen!
                switch (s.versuchen()) {
                    case OK:
                        getStepStack().addFrei(s, true, true);
                        break;
                    case FAIL_BEI_GEHE_ZU: /* Fuer alle Steps failNachVersuchenVorAnfangen() callen. Fuer den
                        aktuellen Step wird es bereits in versuchen() gecallt!
                        for (Step failStep : versuchenStepStack) {
                            if (failStep != s) {
                                failStep.failNachVersuchenVorAnfangen();
                            }
                        }
                        getStepStack().clear();
                        return;
                    default:
                        throw new IllegalStateException();
                }
            }

            planenZelle = versuchenStepStack.getText(0).getStepGeheZu().gettZielZelle();
        } while (!reakteurListe.isEmpty()); wo werden welche entfernt??
    }

    private List<StepNehmen> gettZuVersuchenStepStack(final List<WorldObjekt> reakteurListe,
                                                      Zelle planenZelle) {
        // Vom DB Geld alles moegliche einkaufen

        GebaudeLager bestGebLager = (GebaudeLager) gettBestesWorldObjekt(planenZelle.getPositionMitte(),
                dorfbewohnerGeld.gettMenge());
        if (bestGebLager != null) {
            List<StepNehmen> ss = einkaufenIn(planenZelle, bestGebLager, dorfbewohnerGeld);
            if (ss == null) {
                return null;
            }

            // Wenn eh schon bei baugrube und verhungert dann ggf mit Dorf Geld einkaufen
            if (bestGebLager.isLagerNahrung()
                    && mundraubErlaubt
                    && getDorfbewohner().getNahrungsTasche().mengeVerfuegbarUndLegenRes()
                        < KonstantenBalancing.DORFBEWOHNER_NAHRUNG_GRUNDBEDARF_SUCHEN_AB) {
                List<StepNehmen> ss2 = einkaufenIn(planenZelle, bestGebLager, dorfGeld);
                ss.handle(ss2);
            }

            getZieleList().removeFrei(bestGebLager);
            return ss;
        }

        // Wenn erlaubt und verhungert mit Dorf Geld einkaufen gehen. Sonst abort
        if (mundraubErlaubt && getDorfbewohner().getNahrungsTasche().mengeVerfuegbarUndLegenRes()
                >= KonstantenBalancing.DORFBEWOHNER_NAHRUNG_GRUNDBEDARF_SUCHEN_AB) {
            return null;
        }
        FilterOld filterOld = new FilterOld() { // Unterschied zum reinen Luxus StepStack
            @Override
            public boolean act(Gegenstand g) {
                return g instanceof GegenstandNahrung;
            }
        };
        bestGebLager = (GebaudeLager) gettBestesWorldObjekt(planenZelle.getPositionMitte(),
                dorfGeld.gettMenge(), filterOld);
        getZieleList().removeFrei(bestGebLager);
        if (bestGebLager != null) {
            return einkaufenIn(planenZelle, bestGebLager, dorfGeld);
        }
        return null;
    }

    private List<StepNehmen> einkaufenIn(Zelle startZelle_nl, GebaudeLager gebaudeLager,
                                         GegenstandGeld bezahlenMitGeld) {
        /* PruefeGegenstaende. Das wird in der Schleife um kaufe() so oft aufgerufen, dass sich das
        lohnen muesste
        List<Gegenstand> gepruefteGegenstaende = new ArrayList<>();
        for (Gegenstand g : gebaudeLager.getMusterGegListe()) {
            if (pruefeGegenstand(g, bezahlenMitGeld.gettMenge())) {
                gepruefteGegenstaende.addFrei(g);
            }
        }

        if (gepruefteGegenstaende.isEmpty()) {
            throw new IllegalStateException("Gebaude haette nicht zum einkaufen ausgewaehlt"
                    + " werden sollen!");
        }

        HashMap<Tasche, IntEintragHashMap> kaufenTaschenHMHM = new HashMap<>();
        do {
            Gegenstand besterGegenstand = gettBesterGegenstand(gepruefteGegenstaende, 0);
            if (besterGegenstand == null) {
                IdentityConnectable.out.println("PlanNehmenPrivat :::: einkaufenIn ::: 11111111111111");
                break;
            }
            HashMap<Tasche, IntEintragHashMap> neueKaufenTaschenHMHM = kaufe(kaufenTaschenHMHM,
                    gebaudeLager, bezahlenMitGeld, besterGegenstand);
            if (neueKaufenTaschenHMHM == null) { /* Alternativ koennte man alles zusammenrechnen
                und die Mengen vergleichen. Das wuerde vermutlich aber deutlich laenger dauern
                IdentityConnectable.out.println("PlanNehmenPrivat :::: einkaufenIn ::: 22222222222");
                break;
            }
            kaufenTaschenHMHM = neueKaufenTaschenHMHM;
        } while (true);

        if (kaufenTaschenHMHM.isEmpty()) {
            throw new IllegalStateException("Gebaude haette nicht zum einkaufen ausgewaehlt"
                    + " werden sollen!");
        }

        List<StepNehmen> stepList = new ArrayList<>();
        for (Map.Entry<Tasche, IntEintragHashMap> entry : kaufenTaschenHMHM.entrySet()) {
            Tasche tasche = entry.getIndex();
            IntEintragHashMap einkaufenHashMap = entry.getValue();

            stepList.addFrei(new StepNehmen(getDorfbewohner(), startZelle_nl, gebaudeLager,
                    tasche, bezahlenMitGeld, einkaufenHashMap, true));
        }

        return stepList;
    }

    private HashMap<Tasche, IntEintragHashMap> kaufe(
            HashMap<Tasche, IntEintragHashMap> einkaufenTaschenHashMapHashMap,
            Gebaude kaufenInGebaude, GegenstandGeld bezahlenMitGeld,
            Gegenstand kaufeGegenstandGebaude) {
        /* Platz reservieren und die zu kaufende Menge ausrechnen. Maximal eine ganze Einheit weil
        sich durch jede Menge die Nutzenfunktion aendert
        if (bezahlenMitGeld == dorfGeld && !(kaufeGegenstandGebaude instanceof GegenstandNahrung)) {
            throw new IllegalStateException();
        }

        Tasche tasche = SpecialUtils.passendeTasche(getDorfbewohner(), kaufeGegenstandGebaude);

        // Kaufen Menge bestimmen
        int kaufenGegId = kaufeGegenstandGebaude.gettId();
        int kaufeMenge = MathUtils.min(Konstanten.NACHKOMMASTELLEN,
                tasche.gettDurchId(kaufenGegId).getMengeBisVoll(),
                kaufeGegenstandGebaude.gettMenge());
        if (kaufeMenge <= 0) {
            IdentityConnectable.out.println("PlanNehmenPrivat :::: 11111111111111");
            return null;
        }

        // Ist genug Geld verfuegbar
        int preis = kaufeGegenstandGebaude.getWert_init();
        kaufeMenge = Math.min(kaufeMenge, MathUtils.myDiv(bezahlenMitGeld.gettMenge(),
                preis));
        if (kaufeMenge <= 0) {
            IdentityConnectable.out.println("PlanNehmenPrivat :::: 22222222222");
            return null;
        }

        // Lohnt es sich ueberhaupt die Menge zu kaufen
        if (isMussLohnen()) {
            if (tasche.gettGrenznutzen(kaufeGegenstandGebaude.gettId())
                    < MathUtils.myMult(kaufeMenge, preis)) {
                IdentityConnectable.out.println("PlanNehmenPrivat :::: 33333333333");
                return null;
            }
        } else {
            if (!(kaufeGegenstandGebaude instanceof GegenstandNahrung)) {
                throw new IllegalStateException();
            }
            kaufeMenge = Math.min(kaufeMenge, KonstantenBalancing
                    .DORFBEWOHNER_NAHRUNG_GRUNDBEDARF_AUFFUELLEN_BIS
                    - tasche.mengeVerfuegbarUndLegenRes());
            if (kaufeMenge <= 0) {
                IdentityConnectable.out.println("PlanNehmenPrivat :::: 444444444444444");
                return null;
            }
        }

        // Teilbarkeit beruecksichtigen
        kaufeMenge = MathUtils.myIntRunden(kaufeMenge, kaufeGegenstandGebaude.isTeilbar());
        if (kaufeMenge == 0) {
            return null;
        }

        // Reservieren
        SpecialUtils.reservieren(kaufeGegenstandGebaude, tasche.gettDurchId(kaufenGegId), kaufeMenge);
        SpecialUtils.reservieren(bezahlenMitGeld, kaufenInGebaude.getGeld(),
                kaufeMenge, preis);

        // Einkaufen HashMap aus der spaeter die Steps erstellt werden updaten
        IntEintragHashMap einkaufenHashMap = einkaufenTaschenHashMapHashMap.getText(tasche);
        if (einkaufenHashMap == null) {
            einkaufenHashMap = new IntEintragHashMap();
        }
        if (einkaufenHashMap.containsKey(kaufenGegId)) {
            int alteMenge = einkaufenHashMap.getText(kaufenGegId).getMengeVerfuegbar();
            einkaufenHashMap.getText(kaufenGegId).setMengeVerfuegbar(alteMenge + kaufeMenge);
        } else {
            einkaufenHashMap.put(kaufenGegId, new MengePreisEintrag(kaufeMenge, preis));
        }
        einkaufenTaschenHashMapHashMap.put(tasche, einkaufenHashMap);
        return einkaufenTaschenHashMapHashMap;
    }

    // PRUEFEN ////////////////////////////////////////////////////////////////////////////////////

    private List<WorldObjekt> pruefeWorldObjekt(List<WorldObjekt> worldObjektListe,
                                                int geldMengeVerfuegbar) {
        List<WorldObjekt> gepruefteWorldObjekte = new ArrayList<>();
        for (WorldObjekt wo : worldObjektListe) {
            List<Gegenstand> gepruefteGegenstandListe = pruefeGegenstaende(wo.getMusterGegListe(),
                    geldMengeVerfuegbar);
            if (!gepruefteGegenstandListe.isEmpty()) {
                gepruefteWorldObjekte.addFrei(wo);
            }
        }
        return gepruefteWorldObjekte;
    }

    private List<Gegenstand> pruefeGegenstaende(List<Gegenstand> gegenstandListe,
                                                int geldMengeVerfuegbar) {
        List<Gegenstand> gepruefteGegenstandListe = new ArrayList<>();
        for (Gegenstand g : gegenstandListe) {
            if (pruefeGegenstand(g, geldMengeVerfuegbar)) {
                gepruefteGegenstandListe.addFrei(g);
            }
        }
        return gepruefteGegenstandListe;
    }

    private boolean pruefeGegenstand(Gegenstand g, int geldMengeVerfuegbar_nl) {
        int mengeVerfuegbar = g.gettMenge();
        if (mengeVerfuegbar == 0) {
            return false;
        }

        /* Pruefen ob transportiert und abgelegt werden kann (getMengeBisVoll() beruchsichtigt
        Teilbarkeit!)
        int id = g.gettId();
        if (getDorfbewohner().getMusterGegListe().gettDurchId(id).getMengeBisVoll() == 0) {
            return false;
        }
        Tasche tasche = SpecialUtils.passendeTasche(getDorfbewohner(), g);
        if (tasche.gettDurchId(id).getMengeBisVoll() == 0) {
            return false;
        }

        // Pruefen kann bezahlt werden (Erlauterung von dorfBezahlt vor Constructor)
        return geldMengeVerfuegbar_nl == -1 || g.bezahlbar(geldMengeVerfuegbar_nl);
    }

    // OVERRIDE GETTER ////////////////////////////////////////////////////////////////////////////

    @Override
    protected int gettPositiv(Gegenstand g) {
        return SpecialUtils.passendeTasche(getDorfbewohner(), g).gettGrenznutzen(g.gettId());
    }

    @Override
    protected int gettNegativ(Gegenstand g) {
        return g.getWert_init();
    }
}*/