package de.arnonym.strategiespiel.spielSchirm.umwelt;

import de.arnonym.strategiespiel.framework.ecs.collections.System;
import de.arnonym.strategiespiel.framework.ecs.component.Comp;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;

public class VermehrenComp extends Comp {
    public final int id;
    public final float prop_sekunde;

    public VermehrenComp(System system, Entity entity, Comp[] comps, int id, float prop_sekunde) {
        super(system, entity, comps);
        this.id = id;
        this.prop_sekunde = prop_sekunde;
    }
}
