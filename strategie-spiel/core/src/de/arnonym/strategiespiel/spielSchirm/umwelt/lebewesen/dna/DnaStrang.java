package de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dna;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.vorstufe.EntityGegInfo;
import de.arnonym.strategiespiel.spielSchirm.Enums;
import de.arnonym.strategiespiel.spielSchirm.balancing.IdsGegenstaende;
import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenDna;

public class DnaStrang extends ArrayList<DnaEintrag> {

    // Produktivitaet
    public final float produktivtiaetKopf;
    public final float produktivitaetKoerper;

    // Nutzen fuer einzelne Gegenstaende
    public final List<EntityGegInfo> entityGegInfoList = new ArrayList<>();

    // Nutzen Exponenten
    public final float nutzenExponentNahrung;
    public final float nutzenExponentSpielzeug;
    public final float nutzenExponentWerkzeug;
    public final float nutzenExponentMedizin;
    public final float nutzenExponentWohnen;

    // Sonstiges
    public final float bewegungsgeschwindigkeit;
    public final float schlafAuffuellGeschwindigkeit;

    ///////////////////////////////////////////////////////////////////////////////////////////////
    // CONSTRUCTOR WAEHREND DES LAUFENDEN SPIELS //////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////

    public DnaStrang(DnaStrang motherDnaStrang, DnaStrang fatherDnaStrang) {
        for (DnaEintrag motherDnaEintrag : motherDnaStrang) {
            add(new DnaEintrag(motherDnaEintrag.getName(), motherDnaEintrag.getMinValue(),
                    motherDnaEintrag.getMaxValue(), motherDnaEintrag.gettSingleDna(),
                    fatherDnaStrang.gettDnaEintrag(motherDnaEintrag.getName()).gettSingleDna()));
        }
        this.produktivtiaetKopf = gettSingleValue("produktivtiaetKopf");
        this.produktivitaetKoerper = gettSingleValue("produktivitaetKoerper");

        for (int id : IdsGegenstaende.gettIds()) {
            this.entityGegInfoList.add(new EntityGegInfo(id, 0, null,
                    (int) gettSingleValue("musterGeg"+id)));
        }

        this.nutzenExponentNahrung = gettSingleValue("nutzenExponentNahrung");
        this.nutzenExponentSpielzeug = gettSingleValue("nutzenExponentSpielzeug");
        this.nutzenExponentWerkzeug = gettSingleValue("nutzenExponentWerkzeug");
        this.nutzenExponentMedizin = gettSingleValue("nutzenExponentMedizin");
        this.nutzenExponentWohnen = gettSingleValue("nutzenExponentWohnen");

        this.bewegungsgeschwindigkeit = gettSingleValue("bewegungsgeschwindigkeit");
        this.schlafAuffuellGeschwindigkeit = gettSingleValue("schlafAuffuellGeschwindigkeit");
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    //region CONSUTRUCTOR FUER BESCHRIEBENE DBS
    ///////////////////////////////////////////////////////////////////////////////////////////////

    public DnaStrang(float konstantenKopf, float konstantenKoerper,
                     List<EntityGegInfo> entityGegInfoList, float nutzenExponentNahrung,
                     float nutzenExponentSpielzeug, float nutzenExponentWerkzeug,
                     float nutzenExponentMedizin, float nutzenExponentWohnen,
                     int bewegungsgeschwindigkeit, int schlafAuffuellGeschwindigkeit) {
        this.produktivtiaetKopf = konstantenKopf;
        rekreiereMutterUndVaterStats("konstantenKopf",
                konstantenKopf,
                KonstantenDna.PRODUKTIVITAET_KOPF_MIN,
                KonstantenDna.PRODUKTIVITAET_KOPF_MAX);
        this.produktivitaetKoerper = konstantenKoerper;
        rekreiereMutterUndVaterStats("konstantenKoerper",
                konstantenKoerper,
                KonstantenDna.PRODUKTIVITAET_KOERPER_MIN,
                KonstantenDna.PRODUKTIVITAET_KOERPER_MAX);

        for (EntityGegInfo entityGegInfo : entityGegInfoList) {
            this.entityGegInfoList.add(entityGegInfo);
            rekreiereMutterUndVaterStats("musterGeg"+ entityGegInfo.id,
                    entityGegInfo.getWert(),
                    KonstantenDna.GEG_NUTZEN_MIN,
                    KonstantenDna.GEG_NUTZEN_MAX);
        }

        this.nutzenExponentNahrung = nutzenExponentNahrung;
        rekreiereMutterUndVaterZahlen("nutzenExponentNahrung",
                true,
                nutzenExponentNahrung,
                KonstantenDna.NUTZEN_EXPONENT_MIN,
                KonstantenDna.NUTZEN_EXPONENT_MAX);
        this.nutzenExponentSpielzeug = nutzenExponentSpielzeug;
        rekreiereMutterUndVaterZahlen("nutzenExponentSpielzeug",
                true,
                nutzenExponentSpielzeug,
                KonstantenDna.NUTZEN_EXPONENT_MIN,
                KonstantenDna.NUTZEN_EXPONENT_MAX);
        this.nutzenExponentWerkzeug = nutzenExponentWerkzeug;
        rekreiereMutterUndVaterZahlen("nutzenExponentWerkzeug",
                true,
                nutzenExponentWerkzeug,
                KonstantenDna.NUTZEN_EXPONENT_MIN,
                KonstantenDna.NUTZEN_EXPONENT_MAX);
        this.nutzenExponentMedizin = nutzenExponentMedizin;
        rekreiereMutterUndVaterZahlen("nutzenExponentMedizin",
                true,
                nutzenExponentMedizin,
                KonstantenDna.NUTZEN_EXPONENT_MIN,
                KonstantenDna.NUTZEN_EXPONENT_MAX);
        this.nutzenExponentWohnen = nutzenExponentWohnen;
        rekreiereMutterUndVaterZahlen("nutzenExponentWohnen",
                true,
                nutzenExponentWohnen,
                KonstantenDna.NUTZEN_EXPONENT_MIN,
                KonstantenDna.NUTZEN_EXPONENT_MAX);

        this.bewegungsgeschwindigkeit = bewegungsgeschwindigkeit;
        rekreiereMutterUndVaterStats("bewegungsgeschwindigkeit",
                bewegungsgeschwindigkeit,
                KonstantenDna.BEWEGUNGSGESCHWINDIGKEIT_MIN,
                KonstantenDna.BEWEGUNGSGESCHWINDIGKEIT_MAX);
        this.schlafAuffuellGeschwindigkeit = schlafAuffuellGeschwindigkeit;
        rekreiereMutterUndVaterStats("schlafAuffuellGeschwindigkeit",
                schlafAuffuellGeschwindigkeit,
                KonstantenDna.SCHLAF_AUFFUELL_GESCHWINDIGKEIT_MIN,
                KonstantenDna.SCHLAF_AUFFUELL_GESCHWINDIGKEIT_MAX);
    }

    private void rekreiereMutterUndVaterStats(String name, float kindStat, float minStatValue,
                                              float maxStatValue) {
        /* Wenn kleiner als der minStatValue sind zwei Multiplikatoren aufeinander getroffen
        (Es wird dennoch minimal der minValue ausgegeben, siehe getSingleValue()) */
        if (kindStat <= minStatValue) {
            add(new DnaEintrag(name, minStatValue, maxStatValue,
                    new Dna(KonstantenDna.MULT_MIN + (ThreadLocalRandom.current().nextFloat())
                            * (KonstantenDna.MULT_MAX - KonstantenDna.MULT_MIN),
                            Enums.EnumDnaStatus.MULT),
                    new Dna(KonstantenDna.MULT_MIN + (ThreadLocalRandom.current().nextFloat())
                            * (KonstantenDna.MULT_MAX - KonstantenDna.MULT_MIN),
                            Enums.EnumDnaStatus.MULT)));
            return;
        }
        // Ein Mult und ein Stat
        if (ThreadLocalRandom.current().nextFloat() <= 0.32f) { /* Wahrscheinlichkeit von 0.2 fuer
            Multiplikator. Vater ODER Mutter haben einen -> WKeit von (1/5 * 4/5 * 2) */
            float mult = KonstantenDna.MULT_MIN + ThreadLocalRandom.current().nextFloat()
                    * (KonstantenDna.MULT_MAX - KonstantenDna.MULT_MIN);
            float stat = kindStat / mult;
            if (stat < minStatValue) { //TODO Beim balancen pruefen wie oft das vorkommt, damit nicht immer nur maximale oder minimale Multiplikatoren vorkommen
                stat = minStatValue;
                mult = kindStat / minStatValue;
            }
            if (ThreadLocalRandom.current().nextBoolean()) {
                add(new DnaEintrag(name, minStatValue, maxStatValue,
                        new Dna(mult, Enums.EnumDnaStatus.MULT),
                        new Dna(stat, Enums.EnumDnaStatus.STAT)));
            } else {
                add(new DnaEintrag(name, minStatValue, maxStatValue,
                        new Dna(stat, Enums.EnumDnaStatus.STAT),
                        new Dna(mult, Enums.EnumDnaStatus.MULT)));
            }
            return;
        }
        // Zwei Stats
        rekreiereMutterUndVaterZahlen(name, false, kindStat, minStatValue, maxStatValue);
    }

    private void rekreiereMutterUndVaterZahlen(String name, boolean isNutzen, float kindZahl,
                                               float minZahl, float maxZahl) {
        float abweichendeZahl = kindZahl * (1 - ThreadLocalRandom.current().nextFloat()/10); //TODO Konstanten
        Enums.EnumDnaStatus enumDnaStatus;
        if (isNutzen) {
            enumDnaStatus = Enums.EnumDnaStatus.NUTZEN;
        } else {
            enumDnaStatus = Enums.EnumDnaStatus.STAT;
        }
        Dna dna1 = new Dna(kindZahl, enumDnaStatus);
        Dna dna2 = new Dna(abweichendeZahl, enumDnaStatus);
        if (ThreadLocalRandom.current().nextBoolean()) {
            add(new DnaEintrag(name, minZahl, maxZahl, dna1, dna2));
        } else {
            add(new DnaEintrag(name, minZahl, maxZahl, dna2, dna1));
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    //endregion
    ///////////////////////////////////////////////////////////////////////////////////////////////

    ///////////////////////////////////////////////////////////////////////////////////////////////
    // SONSTIGES //////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////

    public float gettSingleValue(String statName) {
        return gettDnaEintrag(statName).gettSingleValue();
    }

    private DnaEintrag gettDnaEintrag(String statName) {
        for (DnaEintrag dnaEintrag : this) {
            if (dnaEintrag.getName().equals(statName)) {
                return dnaEintrag;
            }
        }
        throw new IllegalArgumentException();
    }
}
