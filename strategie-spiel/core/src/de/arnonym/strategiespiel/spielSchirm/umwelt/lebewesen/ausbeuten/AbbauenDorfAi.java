package de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.ausbeuten;

import de.arnonym.strategiespiel.framework.stp.StpComp;
import de.arnonym.strategiespiel.framework.stp.Task;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.bewegung.BewegungComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.gegenstaende.GegenstaendeComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.EinkommenComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.werte.HabitalwerteComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.werte.ProduktivitaetComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.rendern.RenderComp;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.spielSchirm.bodenStapel.BodenStapelFactory;
import de.arnonym.strategiespiel.spielSchirm.ui.aufsteigendeSymbole.AufsteigendesSymbolFactory;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.lists.ausbeuten.AbzubauenEcsList;
import de.arnonym.strategiespiel.framework.stp.StpMachine;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.listSystems.privateListSystems.SysLager;
import de.arnonym.strategiespiel.spielSchirm.arbeit.task.liefernArbeitszeit.ausbeuten.TaskAbbauen;
import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenAiPrioritaet;

public class AbbauenDorfAi extends AusbeutenDorfAi {
    private final AbzubauenEcsList abzubauenListe;

    public AbbauenDorfAi(AufsteigendesSymbolFactory aufsteigendesSymbolFactory,
                         BodenStapelFactory bodenStapelFactory,

                         Entity entity,

                         AbzubauenEcsList abzubauenListe, SysLager sysLager,

                         BewegungComp bewegungComp, EinkommenComp einkommenComp,
                         GegenstaendeComp gegenstaendeComp, HabitalwerteComp habitalwerteComp,
                         ProduktivitaetComp produktivitaetComp, RenderComp renderComp,
                         StpComp stpComp) {
        super(aufsteigendesSymbolFactory, bodenStapelFactory, sysLager, abzubauenListe, entity,
                bewegungComp, einkommenComp, gegenstaendeComp, habitalwerteComp, produktivitaetComp,
                renderComp, stpComp, KonstantenAiPrioritaet.ABBAUEN);
        this.abzubauenListe = abzubauenListe;
    }

    @Override
    public Task versuchen() {
        return new TaskAbbauen(
                aufsteigendesSymbolFactory, bodenStapelFactory,

                sysLager, abzubauenListe,

                abzubauenListe.eigentumComp, null,

                stpComp, bewegungComp, einkommenComp, gegenstaendeComp, habitalwerteComp,
                produktivitaetComp);
    }
}
