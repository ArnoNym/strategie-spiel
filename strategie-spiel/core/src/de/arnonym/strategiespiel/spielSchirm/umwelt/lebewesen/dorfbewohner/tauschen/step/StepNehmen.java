package de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dorfbewohner.tauschen.step;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.bewegung.BewegungComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.bewegung.BewegungszielComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.gegenstaende.BasisGegComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.GeldComp;
import de.arnonym.strategiespiel.spielSchirm.ui.aufsteigendeSymbole.AufsteigendesSymbolFactory;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.undPreis.IntEintragHashMap;
import de.arnonym.strategiespiel.framework.werkzeuge.collections.hashMaps.IntIntHashMap;
import de.arnonym.strategiespiel.spielSchirm.Assets;
import de.arnonym.strategiespiel.spielSchirm.Enums;

/**
 * Created by LeonPB on 25.06.2018.
 */

public class StepNehmen extends StepTauschen {

    public StepNehmen(AufsteigendesSymbolFactory aufsteigendesSymbolFactory,

                      BewegungComp bewegungComp, BewegungszielComp bewegungszielComp,

                      BasisGegComp<?> nehmenAusBasisGegComp,
                      BasisGegComp<?> legenInBasisGegComp,

                      Vector2 start_nl) {
        super(aufsteigendesSymbolFactory, bewegungComp, bewegungszielComp, nehmenAusBasisGegComp,
                legenInBasisGegComp, start_nl);
    }

    public StepNehmen(AufsteigendesSymbolFactory aufsteigendesSymbolFactory,

                      BewegungComp bewegungComp, BewegungszielComp bewegungszielComp,

                      BasisGegComp<?> nehmenAusBasisGegComp,
                      BasisGegComp<?> legenInBasisGegComp,

                      IntIntHashMap mengenHashMap, Enums.EnumReservieren enumReservieren,
                      Vector2 start_nl) {
        super(aufsteigendesSymbolFactory, bewegungComp, bewegungszielComp, nehmenAusBasisGegComp,
                legenInBasisGegComp, mengenHashMap, enumReservieren, start_nl);
    }

    public StepNehmen(AufsteigendesSymbolFactory aufsteigendesSymbolFactory,

                      BewegungComp bewegungComp, BewegungszielComp bewegungszielComp,

                      BasisGegComp<?> nehmenAusBasisGegComp,
                      BasisGegComp<?> legenInBasisGegComp,

                      GeldComp nehmenVonGeldComp, GeldComp legenInGeldComp,

                      IntEintragHashMap mengenPreisHashMap, boolean bereitsReserviert,
                      Vector2 start_nl) {
        super(aufsteigendesSymbolFactory, bewegungComp, bewegungszielComp, nehmenAusBasisGegComp,
                legenInBasisGegComp, nehmenVonGeldComp, legenInGeldComp, mengenPreisHashMap,
                bereitsReserviert, start_nl);
    }

    @Override
    protected TextureRegion gettSymbolTextureRegion() {
        return Assets.instance.spielweltSymboleAssets.schwebeGeld;
    }
}