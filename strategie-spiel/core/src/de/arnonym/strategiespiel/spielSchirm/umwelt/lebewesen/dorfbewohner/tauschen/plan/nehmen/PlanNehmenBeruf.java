package de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dorfbewohner.tauschen.plan.nehmen;

import com.badlogic.gdx.math.Vector2;

import java.util.Map;

import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.bewegung.BewegungComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.bewegung.BewegungszielComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.gegenstaende.GegenstaendeComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.EinkommenComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.GeldComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.herrschaft.EigentuemerComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.herrschaft.EigentumComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.lager.LagerCompMitGeld;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.lager.LagerCompOhneGeld;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dorfbewohner.tauschen.step.StepNehmen;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.lager.LagerComp;
import de.arnonym.strategiespiel.spielSchirm.ui.aufsteigendeSymbole.AufsteigendesSymbolFactory;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.listSystems.privateListSystems.SysLager;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dorfbewohner.tauschen.plan.score.PlanTauschenScore;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dorfbewohner.tauschen.plan.score.ZielGegenstandKombi;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dorfbewohner.tauschen.step.StepLegeIn;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.endstufe.Gegenstand;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.Geld;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.endstufe.GegenstandList;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.undPreis.IntEintragHashMap;
import de.arnonym.strategiespiel.framework.werkzeuge.collections.hashMaps.IntIntHashMap;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.undPreis.MengePreisEintrag;
import de.arnonym.strategiespiel.spielSchirm.Enums;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.MathUtils;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.SpecialUtils;

public class PlanNehmenBeruf extends PlanNehmen {
    // TODO Geplanten EInkauf und die Kosten mit moeglichem Verkaufserloes vergleichne und entscheiden

    private final BewegungszielComp gBewegungszielComp;
    private final GegenstaendeComp aGegComp;
    private final GegenstaendeComp gGegComp;
    private final GeldComp gGeldComp;
    private final EigentuemerComp gEigentuemerComp;

    private final IntIntHashMap zuNehmenHashMap;
    private IntEintragHashMap aktuellesZielIntEintragHashMap;
    private IntIntHashMap aktuellesZielIntIntHashMap;
    private final IntIntHashMap genommenHashMap = new IntIntHashMap();

    public PlanNehmenBeruf(AufsteigendesSymbolFactory aufsteigendesSymbolFactory,

                           SysLager sysLager,

                           int salary,

                           BewegungComp aBewegungComp, GeldComp aGeldComp, GegenstaendeComp aGegComp,

                           BewegungszielComp gBewegungszielComp, GegenstaendeComp gGegComp,
                           GeldComp gGeldComp, EigentumComp gEigentumComp,

                           IntIntHashMap zuNehmenHashMap, Vector2 startPosition) {
        super(aufsteigendesSymbolFactory, sysLager, salary, aBewegungComp, aGeldComp,
                false, startPosition);
        this.gBewegungszielComp = gBewegungszielComp;
        this.aGegComp = aGegComp;
        this.gGegComp = gGegComp;
        this.gEigentuemerComp = gEigentumComp.getEigentuemer();
        this.zuNehmenHashMap = new IntIntHashMap(zuNehmenHashMap);
        this.gGeldComp = gGeldComp;
        init();
    }

    @Override
    protected void neuesZiel_override() {
        aktuellesZielIntEintragHashMap = new IntEintragHashMap();
        aktuellesZielIntIntHashMap = new IntIntHashMap();
    }

    @Override
    protected void handeln(Map.Entry<ZielGegenstandKombi, PlanTauschenScore> bestEntry) {
        LagerComp ziel = bestEntry.getKey().getZiel();
        Gegenstand geg = bestEntry.getKey().getGegenstand();
        int id = geg.gettId();
        int preis = geg.getWert();

        int verfuegbarMenge = geg.gettMenge(true, false, false);
        int zuNehmenMenge = zuNehmenHashMap.get(id);
        int transportMenge = aGegComp.gegenstandListe.gettDurchId(id).getMengeBisVoll();
        int auftraggeberMenge = gGegComp.gegenstandListe.gettDurchId(id).getMengeBisVoll();
        int menge = MathUtils.min(verfuegbarMenge, zuNehmenMenge, transportMenge, auftraggeberMenge);
        getFeedback().addText("PlanNehmenBeruf ::: menge == 0, " +"verfuegbarMenge == "
                +verfuegbarMenge +", zuNehmenMenge == "+zuNehmenMenge+", " +"transportMenge == "
                +transportMenge+", auftraggeberMenge == "+auftraggeberMenge);

        if (menge == 0) {
            return;
        }

        if (nehmenOhneBezahlen(ziel)) {
            aktuellesZielIntIntHashMap.put(id, menge);
        } else {
            menge = Math.min(menge,
                    MathUtils.myIntDiv(gGeldComp.getGeld().gettMenge(true, true, false), preis));

            if (menge == 0) {
                getFeedback().addText("PlanNehmenBeruf ::: nach Geld Pruefung menge == 0");
                return;
            }

            Geld zielGeld = ((LagerCompMitGeld) ziel).geldComp.getGeld();
            SpecialUtils.reserviere(gGeldComp.getGeld(), zielGeld, menge, preis);

            aktuellesZielIntEintragHashMap.put(id, new MengePreisEintrag(menge, preis));
        }

        Gegenstand transportGegenstand = aGegComp.gegenstandListe.gettDurchId(id);
        SpecialUtils.reserviere(geg, transportGegenstand, menge);
        SpecialUtils.reserviere(transportGegenstand,
                gGegComp.gegenstandListe.gettDurchId(id), menge);

        zuNehmenHashMap.put(id, zuNehmenMenge - menge);
    }

    @Override
    protected StepNehmen gettNeuerStep(LagerComp ziel) {
        boolean nehmenOhneBezahlen = nehmenOhneBezahlen(ziel);

        StepNehmen stepNehmen;
        if (nehmenOhneBezahlen) {
            if (aktuellesZielIntIntHashMap.isEmpty()) {
                getFeedback().addText("PlanNehmenBeruf ::: aktuellesZielIntIntHashMap.isEmpty()");
                return null;
            }
            stepNehmen = new StepNehmen(
                    aufsteigendesSymbolFactory,
                    aBewegungComp,
                    ziel.bewegungszielComp,
                    ziel.gegenstaendeComp,
                    aGegComp,
                    aktuellesZielIntIntHashMap,
                    Enums.EnumReservieren.BEREITS_RESERVIERT,
                    getPlanenPosition());
        } else {
            if (aktuellesZielIntEintragHashMap.isEmpty()) {
                getFeedback().addText("PlanNehmenBeruf ::: aktuellesZielIntEintragHashMap.isEmpty()");
                return null;
            }
            stepNehmen = new StepNehmen(
                    aufsteigendesSymbolFactory,
                    aBewegungComp,
                    ziel.bewegungszielComp,
                    ziel.gegenstaendeComp,
                    aGegComp,
                    gGeldComp,
                    ((LagerCompMitGeld) ziel).geldComp,
                    aktuellesZielIntEintragHashMap,
                    true,
                    getPlanenPosition());
        }

        IntIntHashMap addZuHashMap;
        if (stepNehmen.versuchen() == Enums.EnumStpStatus.OK) {
            addZuHashMap = genommenHashMap;
        } else {
            addZuHashMap = zuNehmenHashMap;
        }

        if (nehmenOhneBezahlen) {
            for (Map.Entry<Integer, Integer> entry : aktuellesZielIntIntHashMap.entrySet()) {
                int id = entry.getKey();
                addZuHashMap.addUpEntry(id, entry.getValue());
            }
        } else {
            for (Map.Entry<Integer, MengePreisEintrag> entry : aktuellesZielIntEintragHashMap.entrySet()) {
                int id = entry.getKey();
                MengePreisEintrag mengePreisEintrag = entry.getValue();
                addZuHashMap.addUpEntry(id, mengePreisEintrag.getMenge());
            }
        }

        return stepNehmen;
    }

    @Override
    protected void ende_or() {
        if (getStepStack().isEmpty()) {
            return;
        }
        StepLegeIn stepLegeIn = new StepLegeIn(
                aufsteigendesSymbolFactory,
                aBewegungComp,
                gBewegungszielComp,
                aGegComp,
                gGegComp,
                gGeldComp,
                genommenHashMap,
                Enums.EnumReservieren.BEREITS_RESERVIERT,
                getPlanenPosition());
        if (getStepStack().handle(stepLegeIn, false) != Enums.EnumStpStatus.OK) {
            getStepStack().beenden();
        }
    }

    // BEWERTEN ///////////////////////////////////////////////////////////////////////////////////

    @Override
    protected int gettPositiv(Gegenstand g) {
        return 1;
    }

    @Override
    protected int gettNegativ(Gegenstand g) {
        return g.getWert();
    }

    // PRUEFEN ////////////////////////////////////////////////////////////////////////////////////

    @Override
    protected boolean basicPruefeZiel(LagerComp ziel) {
        if (aBewegungComp.unerreichbarList.contains(ziel.bewegungszielComp)) {
            getFeedback().addText("PlanNehmenBeruf ::: ziel aussortiert weil auf unerreichbarList!");
            return false;
        }

        if (ziel instanceof LagerCompOhneGeld && !gEigentuemerComp.contains(ziel.eigentumComp)) {
            getFeedback().addText("PlanNehmenBeruf ::: ziel aussortiert weil reinesLager und nicht"
                    + " den selben Eigentuemer!");
            return false;
        }

        GegenstandList zielGegListe = ziel.gegenstaendeComp.gegenstandListe;
        return zielGegListe.getPlatz() != zielGegListe.getMaxPlatz();
    }

    @Override
    protected boolean basicPruefeGeg(Gegenstand geg) {
        return geg.issMenge(true, true, false) && zuNehmenHashMap.containsKey(geg.gettId());
    }

    private boolean nehmenOhneBezahlen(LagerComp lagerComp) {
        return lagerComp instanceof LagerCompOhneGeld;
    }
}









/*
    {
        // Erstelle Liste mit Gebauden die fuer den Einkauf in Frage kommen
        if (einkaufenInGebaudeListe.isEmpty()) {
            return;
        }
        ScoreHashMapPlanTauschen scoreHashMap = gettScoreHashMap(einkaufenInGebaudeListe,
                startZelle.getPositionMitte());

        /* Entscheide wo einzukaufen ist und verringere die Mengen der einzkaufenHashMap um die
        eingekauften Mengen
        planenZelle = startZelle;
        do {
            Map.Entry<WoGegKombi, ScorePlanTauschen> entry = scoreHashMap.gettMaxScoreEntry();

            // Einkauf in Gebaude planen
            IntEintragHashMap gekauftHashMap = einkaufenIn(entry);
            if (gekauftHashMap.summeAllerMengen() <= 0) {
                break;
            }
            for (Map.Entry<Integer, MengePreisEintrag> mengePreisEntry : gekauftHashMap.entrySet()) {
                int gegenstandId = mengePreisEntry.getIndex();
                einzukaufenHashMapOriginal.put(gegenstandId, einzukaufenHashMapOriginal.getText(gegenstandId)
                        - mengePreisEntry.createRandom().getMengeVerfuegbar());
            }
        } while (!scoreHashMap.isEmpty() && einzukaufenHashMapOriginal.summeAllerValues() > 0);

        if (getStepStack().isEmpty()) {
            return;
        }

        // Lege das eingekaufte in das entsprechende Gebaude und starte den task
        for (Map.Entry<Integer, Integer> entry : genommenHashMap.entrySet()) {
            int key = entry.getIndex();
            genommenHashMap.put(key, entry.createRandom() - einzukaufenHashMapOriginal.getText(key));
        }
        StepLegeIn stepLegeIn = new StepLegeIn(getDorfbewohner(), planenZelle,
                fuerZivilObjekt, genommenHashMap, Enums.EnumReservieren.BEREITS_RESERVIERT);
                // Wird in der Funktion einkaufen() reserviert
        if (stepLegeIn.versuchen() == Enums.EnumStepStatus.OK) {
            getStepStack().addFrei(stepLegeIn, false, true);
        } else {
            stepLegeIn.failNachVersuchenVorAnfangen();
            for (Step s : getStepStack().iterate()) {
                s.failNachVersuchenVorAnfangen();
            }
        }
    }

    // EINKAUFEN BERUF ////////////////////////////////////////////////////////////////////////////

    private Map.Entry<WoGegKombi, ScorePlanTauschen> einkaufenIn(
            ScoreHashMapPlanTauschen scoreHashMap, Map.Entry<WoGegKombi, ScorePlanTauschen> entry) {
        final GebaudeLager gebLager = (GebaudeLager) entry.getIndex().getWorldObjekt();

        IntEintragHashMap gekauftHashMap = new IntEintragHashMap();
        IntIntHashMap zuNehmenHashMap = this.einzukaufenHashMapOriginal.kopieren(); /* Nicht das
        original verwenden fuer den Fall, dass die Steps nicht funktionieren
        MengePreisEintrag gekauftEintrag;
        do {
            scoreHashMap.removeFrei(entry.getIndex());

            Gegenstand zuKaufenGeg = entry.getIndex().getWoGegenstand();
            int gegId = zuKaufenGeg.gettId();
            gekauftEintrag = kaufe(gebLager.gettEigentuemer().getGeld(), zuKaufenGeg,
                    zuNehmenHashMap);
            zuNehmenHashMap.put(gegId,
                    zuNehmenHashMap.getText(gegId) - gekauftEintrag.getMengeVerfuegbar());
            gekauftHashMap.put(gegId, gekauftEintrag);

            if (scoreHashMap.isEmpty()) {
                break;
            }
            entry = scoreHashMap.gettMaxScoreEntry();
        } while (gebLager == entry.getIndex().getWorldObjekt());

        if (gekauftHashMap.isEmpty()) {
            return null;
        }

        //Step Erstellen
        StepNehmen stepNehmen = new StepNehmen(getDorfbewohner(),
                planenZelle, gebLager, getDorfbewohner().getMusterGegListe(),
                fuerZivilObjekt.getGeld(), gekauftHashMap, true);
        switch (stepNehmen.versuchen()) {
            case OK:
                getStepStack().addFrei(stepNehmen, false);
                planenZelle = stepNehmen.getStepGeheZu().gettZielZelle();
                break;
            case FAIL:
            case ERLEDIGT:
                throw new IllegalStateException();
        }

        return entry;
    }

    private MengePreisEintrag kaufe(GegenstandGeld einkaufenInObjektBesitzerGeld,
                                    Gegenstand g, IntIntHashMap zuNehmenHashMap) {
        int kaufenGegID = g.gettId();
        int preis = g.getWert_init();

        int kaufeMenge = MathUtils.min(g.gettMenge(),
                zuNehmenHashMap.getText(kaufenGegID),
                getDorfbewohner().getMusterGegListe().gettDurchId(kaufenGegID).getMengeBisVoll(),
                fuerZivilObjekt.getMusterGegListe().gettDurchId(kaufenGegID).getMengeBisVoll());
        if (fuerZivilObjekt.getGeld() != einkaufenInObjektBesitzerGeld) {
            kaufeMenge = Math.min(kaufeMenge,
                    MathUtils.myDiv(fuerZivilObjekt.getGeld().gettMenge(), preis));
        }

        if (kaufeMenge > 0) {
            Gegenstand dbGegenstand = getDorfbewohner().getMusterGegListe().gettDurchId(kaufenGegID);
            SpecialUtils.reservieren(g, dbGegenstand, kaufeMenge);
            SpecialUtils.reservieren(dbGegenstand, fuerZivilObjekt.getMusterGegListe().gettDurchId(kaufenGegID), kaufeMenge);

            SpecialUtils.reservieren(fuerZivilObjekt.getGeld(), einkaufenInObjektBesitzerGeld, kaufeMenge, preis);
        }
        return new MengePreisEintrag(kaufeMenge, preis);
    }

    //

    private IntEintragHashMap einkaufenIn(GebaudeLager kaufenInGebaudeLager) {
        /* Im baugrube vorhandene und gewuenschte Gegenstaende rausschreiben. Das wird in der
        Schleife um kaufe() so oft aufgerufen, dass sich das lohnen muesste
        List<Gegenstand> gepruefteGegenstandListe = new ArrayList<>();
        for (Gegenstand g : kaufenInGebaudeLager.getMusterGegListe()) {
            if (pruefeGegenstand(g, kaufenInGebaudeLager.getGeld().gettMenge())) {
                gepruefteGegenstandListe.addFrei(g);
            }
        }
        if (gepruefteGegenstandListe.isEmpty()) {
            throw new IllegalStateException();
        }

        IntEintragHashMap gekauftHashMap = new IntEintragHashMap();
        IntIntHashMap einzukaufenVerringernHashMap = einzukaufenHashMapOriginal.kopieren();
        MengePreisEintrag gekauftEintrag = null;
        do {
            Gegenstand zuKaufenGegenstand = gettBesterGegenstand(gepruefteGegenstandListe, 0, -1,
                    null);
            if (zuKaufenGegenstand != null) {
                int gegenstandId = zuKaufenGegenstand.gettId();
                gekauftEintrag = kaufe(kaufenInGebaudeLager.gettEigentuemer().getGeld(),
                        zuKaufenGegenstand, einzukaufenVerringernHashMap);
                einzukaufenVerringernHashMap.put(gegenstandId,
                        einzukaufenVerringernHashMap.getText(gegenstandId)
                                - gekauftEintrag.getMengeVerfuegbar());
                gekauftHashMap.put(gegenstandId, gekauftEintrag);
            }
        } while (gekauftEintrag == null); // Wenn Kaufensliste platzEmpty ist wird gekaufteMenge = 0

        return gekauftHashMap;
    }

    private MengePreisEintrag kaufe(GegenstandGeld einkaufenInObjektBesitzerGeld,
                                    Gegenstand g, IntIntHashMap zuNehmenHashMap) { //Platz initReserviert und die zu kaufende Menge ausrechnen. Maximal 1 weil sich durch jede Menge die Nutzenfunktion aendert
        int kaufenGegID = g.gettId();
        int preis = g.getWert_init();

        int kaufeMenge = MathUtils.min(g.gettMenge(),
                zuNehmenHashMap.getText(kaufenGegID),
                getDorfbewohner().getMusterGegListe().gettDurchId(kaufenGegID).getMengeBisVoll(),
                fuerZivilObjekt.getMusterGegListe().gettDurchId(kaufenGegID).getMengeBisVoll());
        if (fuerZivilObjekt.getGeld() != einkaufenInObjektBesitzerGeld) {
            kaufeMenge = Math.min(kaufeMenge,
                    MathUtils.myDiv(fuerZivilObjekt.getGeld().gettMenge(), preis));
        }

        if (kaufeMenge > 0) {
            Gegenstand dbGegenstand = getDorfbewohner().getMusterGegListe().gettDurchId(kaufenGegID);
            SpecialUtils.reservieren(g, dbGegenstand, kaufeMenge);
            SpecialUtils.reservieren(dbGegenstand, fuerZivilObjekt.getMusterGegListe().gettDurchId(kaufenGegID), kaufeMenge);

            SpecialUtils.reservieren(fuerZivilObjekt.getGeld(), einkaufenInObjektBesitzerGeld, kaufeMenge, preis);
        }
        return new MengePreisEintrag(kaufeMenge, preis);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    //region // OVERRIDE //
    ///////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    protected boolean pruefeGegenstand(Gegenstand g, int geldMengeVerfuegbar_nl) {
        // Pruefen steht auf Einkaufsliste
        if (!einzukaufenHashMapOriginal.containsKey(g.gettId())) {
            IdentityConnectable.out.println("PlanNehmenBeruf ::: nicht auf einkaufs HashMap id: "+g.gettId());
            return false;
        }

        // Pruefe ob Menge verfuegbar ist
        int mengeVerfuegbar = g.gettMenge();
        if (mengeVerfuegbar == 0) {
            IdentityConnectable.out.println("PlanNehmenBeruf 3 ::: keine Menge verfuegbar id: "+g.gettId());
            return false;
        }

        /* Pruefen ob vom DB transportiert und in das Ziel abgelegt werden kann (getMengeBisVoll()
        beruchsichtigt Teilbarkeit!)
        int id = g.gettId();
        if (getDorfbewohner().getMusterGegListe().gettDurchId(id).getMengeBisVoll() == 0) {
            IdentityConnectable.out.println("PlanNehmenBeruf ::: Db Kann keine Menge aufnehmen id: "+g.gettId());
            return false;
        }
        if (fuerZivilObjekt.getMusterGegListe()
                .gettDurchId(id).getMengeBisVoll() == 0) {
            IdentityConnectable.out.println("PlanNehmenBeruf ::: ksdsdbar id: "+g.gettId());
            return false;
        }

        // Pruefen kann bezahlt werden
        return geldMengeVerfuegbar_nl == -1 || g.bezahlbar(geldMengeVerfuegbar_nl);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    //endregion
    ///////////////////////////////////////////////////////////////////////////////////////////////
}*/