package de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dorfbewohner;

import de.arnonym.strategiespiel.framework.stp.StpComp;
import de.arnonym.strategiespiel.framework.stp.Task;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.bewegung.BewegungComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.gegenstaende.TascheComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.EinkommenComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.GeldComp;
import de.arnonym.strategiespiel.spielSchirm.ui.aufsteigendeSymbole.AufsteigendesSymbolFactory;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.listSystems.privateListSystems.SysLager;
import de.arnonym.strategiespiel.framework.stp.stacks.StepStack;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dorfbewohner.tauschen.plan.nehmen.PlanNehmenPrivat;

public class TaskEinkaufenLuxus extends Task { //Sachen einkaufen die dem DB Nutzen bringen
    public final AufsteigendesSymbolFactory aufsteigendesSymbolFactory;
    public final SysLager sysLager;
    public final BewegungComp bewegungComp;
    public final EinkommenComp einkommenComp;
    public final GeldComp geldComp;
    public final TascheComp tascheComp;

    public TaskEinkaufenLuxus(AufsteigendesSymbolFactory aufsteigendesSymbolFactory,

                              SysLager sysLager,

                              StpComp stpComp, BewegungComp bewegungComp,
                              EinkommenComp einkommenComp,
                              GeldComp geldComp, TascheComp tascheComp) {
        super(stpComp, bewegungComp, einkommenComp, geldComp, tascheComp);
        this.aufsteigendesSymbolFactory = aufsteigendesSymbolFactory;
        this.sysLager = sysLager;
        this.bewegungComp = bewegungComp;
        this.einkommenComp = einkommenComp;
        this.geldComp = geldComp;
        this.tascheComp = tascheComp;
    }

    @Override
    public void planeAlleSteps(StepStack stepStack) {
        stepStack.handle(new PlanNehmenPrivat(aufsteigendesSymbolFactory, sysLager, bewegungComp,
                einkommenComp, tascheComp, true , bewegungComp.positionComp.position));
    }

    @Override
    public void planeNaechstenStep(StepStack stepStack) {

    }
}