package de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.bewegen.pathfinding;

import com.badlogic.gdx.math.Vector2;

import de.arnonym.strategiespiel.spielSchirm.worldMap.Zelle;

public class Part extends Goal {
    private final Part vorherigerPart;
    private final float gesamtKosten;
    private final float verwendeteZellenKosten; // Beim Planen verwendete Kosten
    private final Zelle eckZelleLinks;
    private final Zelle eckZelleRechts;

    public Part(Part vorherigerPart, Zelle zelle, Vector2 indizes, float breadthKosten,
                float gesamtKosten, Zelle eckZelleLinks, Zelle eckZelleRechts) {
        super(zelle, indizes, breadthKosten);
        this.vorherigerPart = vorherigerPart;
        this.gesamtKosten = gesamtKosten;
        this.verwendeteZellenKosten = zelle.gettLaufenMult();
        this.eckZelleLinks = eckZelleLinks;
        this.eckZelleRechts = eckZelleRechts;
    }

    public Part(Part vorherigerPart, Zelle zelle, Vector2 indizes, float breadthKosten,
                 float gesamtKosten) {
        this(vorherigerPart, zelle, indizes, breadthKosten, gesamtKosten, null, null);
    }

    public Part getVorherigerPart() {
        return vorherigerPart;
    }

    @Override
    public float getGesamtKosten() {
        return gesamtKosten;
    }

    public Zelle getEckZelleLinks() {
        return eckZelleLinks;
    }

    public Zelle getEckZelleRechts() {
        return eckZelleRechts;
    }

    public float getVerwendeteZellenKosten() {
        return verwendeteZellenKosten;
    }
}