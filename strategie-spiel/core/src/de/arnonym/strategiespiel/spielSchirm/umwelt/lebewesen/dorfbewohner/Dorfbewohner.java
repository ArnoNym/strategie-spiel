package de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dorfbewohner;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

import de.arnonym.strategiespiel.Strategiespiel;
import de.arnonym.strategiespiel.spielSchirm.Dorf;
import de.arnonym.strategiespiel.spielSchirm.World;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.ArbeitnehmerAi;
import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenAiPrioritaet;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.WorldComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.alter.TransAlterComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.ausbeuten.AbbauenDorfAi;
import de.arnonym.strategiespiel.framework.stp.StpComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.bewegung.BewegungComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.gegenstaende.GegenstaendeComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.gegenstaende.GegenstaendeComp1;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.gegenstaende.TascheComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.EinkommenComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.VariabelGeldComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.klick.FensterKlickComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dorfbewohner.familie.FamilienmitgliedComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dna.DbDnaComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dorfbewohner.familie.PartnerComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.vermehren.FemaleComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.werte.BildungComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.werte.HabitalwerteComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.werte.ProduktivitaetComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.rendern.BeweglichRenderComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.groesse.CompGroesseOrto;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.name.NameComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.sonstiges.PositionComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.transformieren.TransComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.transformieren.zu.TransZuBodenStapelComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.wohnen.EinwohnerComp;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dorfbewohner.familie.Familie;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.SysRender;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterOnKlick.OnKlickFenster;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterOnKlick.DorfbewohnerOnKlickFenster;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.vorstufe.EntityGegInfoList;
import de.arnonym.strategiespiel.spielSchirm.Assets;
import de.arnonym.strategiespiel.spielSchirm.Enums;
import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenBalance;
import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenOptik;
import de.arnonym.strategiespiel.framework.werkzeuge.Paket;
import de.arnonym.strategiespiel.framework.werkzeuge.textures.TextureHashMap;
import de.arnonym.strategiespiel.framework.werkzeuge.textures.IdTextureAnimation;
import de.arnonym.strategiespiel.framework.werkzeuge.textures.IdTextureRegion;

public class Dorfbewohner extends Entity {
    public Dorfbewohner(World world, Dorf dorf, DorfbewohnerMuster dorfbewohnerMuster,
                        Familie familie, Vector2 position) {
        super(world.manager);
        PositionComp positionComp = new PositionComp(this, position);
        NameComp nameComp = new NameComp(this, dorfbewohnerMuster.vorname, "null");
        TransZuBodenStapelComp compTransVariabelBodenStapel = new TransZuBodenStapelComp(this,
                world.sysKlick, world.sysKollision, TransComp.EnumDelete.ENTITY);
        TransAlterComp transAlterComp = new TransAlterComp(this, world.sysAltern, false,
                10,//todo ausklammern dorfbewohnerMuster.alter_jahre * KonstantenBalance.SEKUNDEN_PRO_JAHR,
                compTransVariabelBodenStapel,
                2000000);//todo ausklammern KonstantenBalance.DORFBEWOHNER_STANDARD_MAX_ALTER * KonstantenBalance.SEKUNDEN_PRO_JAHR);

        TextureRegion standardTextureRegion = Assets.instance.dorfbewohnerAAssets.dorfbewohnerStehen;
        TextureHashMap textureHashMap = new TextureHashMap(
                new IdTextureRegion(standardTextureRegion, true, -1, new Vector2(1, 1)));
        textureHashMap.put(Enums.EnumAktion.GEHEN_OBEN, new IdTextureAnimation(
                Assets.instance.dorfbewohnerAAssets.obenLaufenAnimation,
                Enums.EnumAddTexture.ERSETZE_WENN_UNGLEICH, 2,
                KonstantenOptik.DORFBEWOHNER_FUNDAMENT));
        textureHashMap.put(Enums.EnumAktion.GEHEN_RECHTS, new IdTextureAnimation(
                Assets.instance.dorfbewohnerAAssets.seiteLaufenAnimation,
                Enums.EnumAddTexture.ERSETZE_WENN_UNGLEICH, 2,
                KonstantenOptik.DORFBEWOHNER_FUNDAMENT));
        textureHashMap.put(Enums.EnumAktion.GEHEN_UNTEN, new IdTextureAnimation(
                Assets.instance.dorfbewohnerAAssets.untenLaufenAnimation,
                Enums.EnumAddTexture.ERSETZE_WENN_UNGLEICH, 2,
                KonstantenOptik.DORFBEWOHNER_FUNDAMENT));
        BeweglichRenderComp beweglichRenderComp = new BeweglichRenderComp(this,
                world.getSysRender(), positionComp,
                textureHashMap, SysRender.EnumZ.ENTITIES, true);

        CompGroesseOrto compGroesseOrto = new CompGroesseOrto(this,
                standardTextureRegion.getRegionWidth(), standardTextureRegion.getRegionHeight());
        BewegungComp bewegungComp = new BewegungComp(world.worldMap, this,
                positionComp, beweglichRenderComp,
                KonstantenBalance.DORFBEWOHNER_DNA_BEWEGUNGSGESCHWINDIGKEIT);
        EntityGegInfoList entityGegInfoListe = new EntityGegInfoList(
                KonstantenBalance.DORFBEWOHNER_PLATZ, false);
        entityGegInfoListe.addAll(dorfbewohnerMuster.dnaStrang.entityGegInfoList);
        GegenstaendeComp gegenstaendeComp = new GegenstaendeComp1(this, beweglichRenderComp,
                entityGegInfoListe);
        StpComp stpComp = new StpComp(world.stpSystem, this);

        VariabelGeldComp compGeldVariabel = new VariabelGeldComp(this); // Wird in Familienmitglied durch Familie gesetzt
        TascheComp tascheComp = new TascheComp(this, null); // Wird in Familienmitglied durch Familie gesetzt

        DbDnaComp dbDnaComp = new DbDnaComp(this, dorfbewohnerMuster.dnaStrang);
        HabitalwerteComp habitalwerteComp = new HabitalwerteComp(this, world.habitalwerteSystem,
                transAlterComp, compTransVariabelBodenStapel, nameComp, tascheComp);
        EinwohnerComp einwohnerComp = new EinwohnerComp(this, bewegungComp, dbDnaComp,
                habitalwerteComp);
        FamilienmitgliedComp familienmitgliedComp = new FamilienmitgliedComp(this, einwohnerComp,
                compGeldVariabel, beweglichRenderComp, stpComp, tascheComp, familie,
                dorfbewohnerMuster.enumStatusInFamilie);

        BildungComp bildungComp = new BildungComp(this);
        ProduktivitaetComp produktivitaetComp = new ProduktivitaetComp(this, bildungComp,
                dbDnaComp, einwohnerComp, habitalwerteComp, tascheComp);
        new AiCompHunger(
                this,
                beweglichRenderComp,
                stpComp,
                compGeldVariabel,
                tascheComp,
                KonstantenBalance.DORFBEWOHNER_NAHRUNG_GRUNDBEDARF_SUCHEN_AB,
                KonstantenBalance.DORFBEWOHNER_NAHRUNG_GRUNDBEDARF_VERHUNGERN,
                KonstantenBalance.DORFBEWOHNER_NAHRUNG_GRUNDBEDARF_AUFFUELLEN_BIS);

        EinkommenComp einkommenComp = new EinkommenComp(dorf, this, world.sysEinkommen,
                compGeldVariabel);
        AiSchlafen aiSchlafen = new AiSchlafen(this, beweglichRenderComp, stpComp,
                einwohnerComp);
        AiAufraumen aiAufraumen = new AiAufraumen(this, world.aufsteigendesSymbolFactory,
                dorf.bodenStapelFactory, world.sysLager, beweglichRenderComp, stpComp,
                bewegungComp, einkommenComp, gegenstaendeComp, habitalwerteComp,
                produktivitaetComp);
        ArbeitnehmerAi arbeitnehmerAi = new ArbeitnehmerAi(world.aufsteigendesSymbolFactory,
                dorf.bodenStapelFactory, world.worldMap.ressourcenFactory, this, world.sysLager,
                dorf.employmentMatcher, beweglichRenderComp, stpComp, bewegungComp,
                gegenstaendeComp, habitalwerteComp, einkommenComp, produktivitaetComp);
        AbbauenDorfAi abbauenDorfAi = new AbbauenDorfAi(world.aufsteigendesSymbolFactory,
                dorf.bodenStapelFactory, this, dorf.abzubauenListe, world.sysLager, bewegungComp,
                einkommenComp, gegenstaendeComp, habitalwerteComp, produktivitaetComp,
                beweglichRenderComp, stpComp);
        AiBauen aiBauen = new AiBauen(world.aufsteigendesSymbolFactory, this, world.sysBaugrube,
                world.sysLager, bewegungComp, einkommenComp, gegenstaendeComp, habitalwerteComp,
                produktivitaetComp, beweglichRenderComp, stpComp,
                KonstantenAiPrioritaet.BAUARBEITER);
        WorldComp worldComp = new WorldComp(this, world);
        DorfComp dorfComp = new DorfComp(this, dorf);
        if (dorfbewohnerMuster.geschlechtFemale) {
            FemaleComp femaleComp = new FemaleComp(this, dbDnaComp, dorfComp, familienmitgliedComp,
                    positionComp, worldComp);
            PartnerComp partnerComp = new PartnerComp(world.partnerMatcher, this, femaleComp,
                    habitalwerteComp, KonstantenBalance.DB_KIND_WIRD_GEZEUGT_CHANCE);
        } else {
            PartnerComp partnerComp = new PartnerComp(world.partnerMatcher, this, dbDnaComp,
                    habitalwerteComp, KonstantenBalance.DB_KIND_WIRD_GEZEUGT_CHANCE);
        }

        Paket<OnKlickFenster> paket = () -> new DorfbewohnerOnKlickFenster(world,
                Strategiespiel.skin, arbeitnehmerAi, gegenstaendeComp, einkommenComp.geldComp,
                habitalwerteComp, beweglichRenderComp, stpComp, nameComp);
        FensterKlickComp fensterKlickComp = new FensterKlickComp(this, world.sysKlick,
                compGroesseOrto, positionComp, beweglichRenderComp, paket);

        activate();
    }
}
