package de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dorfbewohner.tauschen.plan.score;

import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.bewegung.BewegungComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.EinkommenComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.lager.LagerComp;
import de.arnonym.strategiespiel.framework.werkzeuge.collections.score.ScoreHashMap;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.SpecialUtils;

public class PlanTauschenScoreHashMap extends ScoreHashMap<ZielGegenstandKombi, PlanTauschenScore> {

    public PlanTauschenScoreHashMap() {
    }

    public void loescheZiel(LagerComp ziel) {
        List<ZielGegenstandKombi> loeschenKeyListe = new ArrayList<>();
        for (Map.Entry<ZielGegenstandKombi, PlanTauschenScore> entry : entrySet()) {
            ZielGegenstandKombi key = entry.getKey();
            if (key.getZiel() == ziel) {
                loeschenKeyListe.add(key);
            }
        }
        for (ZielGegenstandKombi key : loeschenKeyListe) {
            remove(key);
        }
    }

    public void settLaufenStartPosition(BewegungComp bewegungComp, int salary,
                                        Vector2 startPosition) {
        for (Map.Entry<ZielGegenstandKombi, PlanTauschenScore> entry : entrySet()) {
            LagerComp ziel = entry.getKey().getZiel();
            int laufenKosten = SpecialUtils.gettKostenLaufen(bewegungComp, salary,
                    startPosition,
                    ziel.bewegungszielComp.compKollisionLaufen.positionComp.getPosition());
            entry.getValue().setLaufenKosten(laufenKosten);
        }
    }
}
