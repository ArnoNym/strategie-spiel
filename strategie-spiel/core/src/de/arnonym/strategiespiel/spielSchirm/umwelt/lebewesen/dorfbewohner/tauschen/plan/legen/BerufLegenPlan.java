package de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dorfbewohner.tauschen.plan.legen;

import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import de.arnonym.strategiespiel.spielSchirm.balancing.Konstanten;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.bewegung.BewegungComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.bewegung.BewegungszielComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.gegenstaende.GegenstaendeComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.EinkommenComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.GeldComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.lager.LagerComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.lager.LagerCompMitGeld;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.lager.LagerCompOhneGeld;
import de.arnonym.strategiespiel.spielSchirm.ui.aufsteigendeSymbole.AufsteigendesSymbolFactory;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.listSystems.privateListSystems.SysLager;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dorfbewohner.tauschen.plan.score.PlanTauschenScore;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dorfbewohner.tauschen.plan.score.ZielGegenstandKombi;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dorfbewohner.tauschen.step.StepLegeIn;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dorfbewohner.tauschen.step.StepNehmen;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.Geld;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.endstufe.Gegenstand;
import de.arnonym.strategiespiel.spielSchirm.Enums;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.undPreis.IntEintragHashMap;
import de.arnonym.strategiespiel.framework.werkzeuge.collections.hashMaps.IntIntHashMap;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.undPreis.MengePreisEintrag;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.MathUtils;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.SpecialUtils;

public class BerufLegenPlan extends LegenPlan {
    private final BewegungszielComp gBewegungszielComp;
    private final GegenstaendeComp gGegenstaendeComp;
    private final Vector2 originalStartPosition;

    private final IntIntHashMap gZuLegenHashMap;
    private final IntIntHashMap gelegtHashMap = new IntIntHashMap();

    /** Sollten nie gleichzeitig befuellt werden */
    private IntEintragHashMap intEintragHashMap;
    private IntIntHashMap intIntHashMap;

    public BerufLegenPlan(AufsteigendesSymbolFactory aufsteigendesSymbolFactory,

                          SysLager sysLager,

                          BewegungszielComp gBewegungszielComp, GegenstaendeComp gGegenstaendeComp,
                          GeldComp gGeldComp, int salary,

                          BewegungComp aBewegungComp, GeldComp aGeldComp,
                          GegenstaendeComp aGegenstaendeComp,

                          IntIntHashMap gZuLegenHashMap, LagerComp zuvorGenommenAusZiel_nl,
                          boolean mussLohnen, Vector2 startPosition) {
        super(aufsteigendesSymbolFactory, sysLager, gGeldComp, salary, aBewegungComp, aGeldComp,
                aGegenstaendeComp, zuvorGenommenAusZiel_nl, mussLohnen,
                gBewegungszielComp.compKollisionLaufen.positionComp.position);
        this.gBewegungszielComp = gBewegungszielComp;
        this.gGegenstaendeComp = gGegenstaendeComp;
        this.gZuLegenHashMap = gZuLegenHashMap;
        this.originalStartPosition = new Vector2(startPosition);
        init();
    }

    @Override
    protected List<LagerComp> gettZieleList(SysLager sysLager) {
        List<LagerComp> zieleList = new ArrayList<>();
        boolean mussLohnen = isMussLohnen();
        for (LagerComp lagerComp : sysLager.toCollection(new LinkedList<>())) {
            if (!mussLohnen || lagerComp instanceof LagerCompMitGeld) {
                zieleList.add(lagerComp);
            }
        }
        return zieleList;
    }

    @Override
    protected void neuesZiel_override() {
        intEintragHashMap = new IntEintragHashMap();
        intIntHashMap = new IntIntHashMap();
    }

    // HANDELN ////////////////////////////////////////////////////////////////////////////////////

    @Override
    protected void handeln(Map.Entry<ZielGegenstandKombi, PlanTauschenScore> bestEntry) {
        Gegenstand zGeg = bestEntry.getKey().getGegenstand();
        int id = zGeg.gettId();
        Gegenstand gGeg = gGegenstaendeComp.gegenstandListe.gettDurchId(id);
        Gegenstand aGeg = aGegenstaendeComp.gegenstandListe.gettDurchId(id);

        LagerComp ziel = bestEntry.getKey().getZiel();

        int menge;
        if (ziel instanceof LagerCompOhneGeld) {
            menge = ohneGeldHandeln(zGeg, gGeg, aGeg);
        } else {
            Geld zGeld = ((LagerCompMitGeld) ziel).geldComp.getGeld();
            menge = mitGeldHandeln(zGeg, zGeld, gGeg, aGeg);
        }

        SpecialUtils.reserviere(gGeg, aGeg, menge);
        SpecialUtils.reserviere(aGeg, zGeg, menge);
        gZuLegenHashMap.put(id, gZuLegenHashMap.get(id) - menge);
        gelegtHashMap.put(id, gelegtHashMap.getOrDefault(id, 0) + menge);
    }

    private int ohneGeldHandeln(Gegenstand zGeg, Gegenstand gGeg, Gegenstand aGeg) {
        int menge = applyMengenBounds(zGeg, gGeg, aGeg);

        intIntHashMap.put(zGeg.gettId(), menge);

        return menge;
    }

    private int mitGeldHandeln(Gegenstand zGeg, Geld zGeld, Gegenstand gGeg, Gegenstand aGeg) {
        int menge = applyMengenBounds(zGeg, gGeg, aGeg);
        menge = applyGeldBounds(menge, zGeg, zGeld);

        int preis = zGeg.getWert();

        intEintragHashMap.put(zGeg.gettId(), new MengePreisEintrag(menge, preis));
        SpecialUtils.reserviere(zGeld, gGeldComp.getGeld(), menge, preis);

        return menge;
    }

    private int applyMengenBounds(Gegenstand zGeg, Gegenstand gGeg, Gegenstand aGeg) {
        int verkaufenMenge = MathUtils.min(
                zGeg.getMengeBisVoll(),
                aGeg.getMengeBisVoll(),
                gGeg.gettMenge(true, false, false),
                gZuLegenHashMap.get(zGeg.gettId()));
        if (verkaufenMenge == 0) {
            getFeedback().addText("PlanLegen ::: verkaufenMenge == 0");
        }
        return verkaufenMenge;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    protected StepLegeIn gettNeuerStep(LagerComp ziel) {
        StepLegeIn neuerStep;
        if (ziel instanceof LagerCompOhneGeld) {
            if (intIntHashMap.isEmpty()) {
                getFeedback().addText("PlanLegen ::: intIntHashMap.isEmpty()");
                return null;
            }
            neuerStep = new StepLegeIn(
                    aufsteigendesSymbolFactory,
                    aBewegungComp,
                    ziel.bewegungszielComp,
                    aGegenstaendeComp,
                    ziel.gegenstaendeComp,
                    gGeldComp,
                    intIntHashMap,
                    Enums.EnumReservieren.BEREITS_RESERVIERT,
                    getPlanenPosition());
        } else {
            if (intEintragHashMap.isEmpty()) {
                getFeedback().addText("PlanLegen ::: intEintragHashMap.isEmpty()");
                return null;
            }
            neuerStep = new StepLegeIn( //todo todo todo dieser step ist nicht ok
                    aufsteigendesSymbolFactory,
                    aBewegungComp,
                    ziel.bewegungszielComp,
                    aGegenstaendeComp,
                    ziel.gegenstaendeComp,
                    ((LagerCompMitGeld) ziel).geldComp,
                    gGeldComp,
                    gGeldComp,
                    intEintragHashMap,
                    true,
                    getPlanenPosition());
        }
        return neuerStep;
    }

    @Override
    protected void ende_or() {
        if (getStepStack().isEmpty()) return;
        StepNehmen stepNehmen = new StepNehmen(
                aufsteigendesSymbolFactory,
                aBewegungComp,
                gBewegungszielComp,
                gGegenstaendeComp,
                aGegenstaendeComp,
                gelegtHashMap,
                Enums.EnumReservieren.BEREITS_RESERVIERT,
                originalStartPosition);
        if (getStepStack().handle(stepNehmen, true) != Enums.EnumStpStatus.OK) {
            getFeedback().addText("BerufLegenPlan ::: es kann nicht aus Auftraggeber genommen werden!");
            getStepStack().beenden();
        }
    }

    // PRUEFEN ////////////////////////////////////////////////////////////////////////////////////

    @Override
    protected boolean pruefeTotal() {
        if (gZuLegenHashMap.isEmpty()) {
            getFeedback().addText(getClass().getSimpleName()+" ::: zuLegenHashMap.isEmpty()");
            return false;
        }
        if (aGegenstaendeComp.gegenstandListe.getPlatz() == 0) {
            getFeedback().addText(getClass().getSimpleName()+" ::: aktuer ist voll!");
            return false;
        }
        return true;
    }

    @Override
    protected boolean basicPruefeZiel(LagerComp ziel) {
        if (ziel == zuvorGenommenAusZiel_nl) {
            if (Konstanten.DEVELOPER_VISUELL_STEP_SYSTEM_OUT) {
                getFeedback().addText("PlanLegen ::: ziel aussortiert : weil aus diesem Ziel genommen");
            }
            return false;
        }
        if (ziel.gegenstaendeComp.gegenstandListe.getPlatz() == 0) {
            if (Konstanten.DEVELOPER_VISUELL_STEP_SYSTEM_OUT) {
                getFeedback().addText("PlanLegen ::: ziel aussortiert : weil keinen Platz");
            }
            return false;
        }
        return true;
    }

    @Override
    protected boolean basicPruefeGeg(Gegenstand geg) {
        // todo wenn mussLohnen mit wert des Auftraggebers vergleichen (Diesen als die produktioskosten setzen)

        return gZuLegenHashMap.getOrDefault(geg.gegMuster.id, 0) > 0;
    }
}
