package de.arnonym.strategiespiel.spielSchirm.umwelt.ressourcen.stufen.primaer;

import de.arnonym.strategiespiel.spielSchirm.umwelt.ressourcen.stufen.WachstumInfo;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.vorstufe.EntityGegInfoList;
import de.arnonym.strategiespiel.framework.werkzeuge.textures.TextureHashMap;

public class PWachstumInfo extends WachstumInfo {
    public PWachstumInfo(TextureHashMap textureHashMap, EntityGegInfoList entityGegInfoList,
                         float stufeDauer_sek) {
        super(textureHashMap, entityGegInfoList, stufeDauer_sek);
    }
}
