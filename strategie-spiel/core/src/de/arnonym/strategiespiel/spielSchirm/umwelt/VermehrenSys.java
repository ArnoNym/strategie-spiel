package de.arnonym.strategiespiel.spielSchirm.umwelt;

import de.arnonym.strategiespiel.framework.ecs.collections.UpdateSetSystem;
import de.arnonym.strategiespiel.framework.werkzeuge.functionalInterfaces.Method2;
import de.arnonym.strategiespiel.spielSchirm.balancing.Konstanten;

public class VermehrenSys<C extends VermehrenComp> extends UpdateSetSystem<C> {
    public VermehrenSys(Method2<C, Float> updateMethod) {
        super(false, updateMethod);
    }
}
