package de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dorfbewohner;

import de.arnonym.strategiespiel.framework.ecs.component.Comp;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.spielSchirm.Dorf;

public class DorfComp extends Comp {
    public final Dorf dorf;

    public DorfComp(Entity entity, Dorf dorf) {
        super(entity);
        this.dorf = dorf;
    }
}
