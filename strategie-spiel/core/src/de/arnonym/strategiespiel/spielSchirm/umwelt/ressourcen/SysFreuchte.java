package de.arnonym.strategiespiel.spielSchirm.umwelt.ressourcen;

import java.util.List;

import de.arnonym.strategiespiel.spielSchirm.balancing.Konstanten;
import de.arnonym.strategiespiel.framework.ecs.collections.UpdateSetSystem;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.ausbeuten.FruechteComp;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.endstufe.Gegenstand;
import de.arnonym.strategiespiel.framework.werkzeuge.collections.UpdateSet;

public class SysFreuchte extends UpdateSetSystem<FruechteComp> {
    public SysFreuchte() {
        super(false, (fruechteComp, delta) -> {
                int freierPlatz = fruechteComp.gegenstaendeComp.gegenstandListe.getPlatz();
                List<Gegenstand> fruechte = fruechteComp.gettFreuchte();
                int freierPlatzProFrucht = freierPlatz / fruechte.size();
                float realDelta = fruechteComp.pflegenComp.gettPflegenDelta(delta)
                        * fruechteComp.wachsenMod;
                for (Gegenstand g : fruechte) {
                    g.wachsen(realDelta, freierPlatzProFrucht);
                }
        });
    }
}
