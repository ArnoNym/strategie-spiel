package de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dorfbewohner.tauschen.plan.nehmen;

import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;
import java.util.List;

import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.bewegung.BewegungComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.EinkommenComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.GeldComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.lager.LagerComp;
import de.arnonym.strategiespiel.spielSchirm.ui.aufsteigendeSymbole.AufsteigendesSymbolFactory;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.listSystems.privateListSystems.SysLager;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dorfbewohner.tauschen.plan.PlanTauschen;

public abstract class PlanNehmen extends PlanTauschen {
    public PlanNehmen(AufsteigendesSymbolFactory aufsteigendesSymbolFactory,

                      SysLager sysLager,

                      int salary,

                      BewegungComp bewegungComp, GeldComp aGeldComp,

                      boolean mussLohnen, Vector2 startPosition) {
        super(aufsteigendesSymbolFactory, sysLager, salary, bewegungComp, aGeldComp, mussLohnen,
                startPosition);
    }

    @Override
    protected boolean pruefeTotal() {
        return true;
    }

    @Override
    protected List<LagerComp> gettZieleList(SysLager sysLager) {
        return new ArrayList<>(sysLager.toCollection(new ArrayList<>()));
    }
}
