package de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dorfbewohner.familie;

import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;
import java.util.List;

import de.arnonym.strategiespiel.framework.listener.EnumStory;
import de.arnonym.strategiespiel.framework.listener.StoryListener;
import de.arnonym.strategiespiel.framework.listener.StoryTeller;
import de.arnonym.strategiespiel.framework.werkzeuge.numbers.unit.ZeitEnum;
import de.arnonym.strategiespiel.spielSchirm.Dorf;
import de.arnonym.strategiespiel.spielSchirm.World;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.OriginGeldComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.wohnen.FamilieMieterComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.gegenstaende.TascheComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.name.NameComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.wohnen.MieterComp;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dorfbewohner.Dorfbewohner;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dorfbewohner.DorfbewohnerMuster;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.vorstufe.EntityGegInfoList;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.taschen.MasterTasche;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.taschen.MedizinTasche;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.taschen.NahrungTasche;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.taschen.SpielzeugTasche;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.taschen.WerkzeugTasche;
import de.arnonym.strategiespiel.spielSchirm.Enums;

public class Familie extends Entity implements StoryListener {
    public final MieterComp mieterComp;
    public final NameComp nameComp;
    public final TascheComp tascheComp;

    private final List<FamilienmitgliedComp> familienmitgliedList = new ArrayList<>();

    public Familie(World world, Dorf dorf, FamilieMuster familieMuster) {
        super(world.manager);
        OriginGeldComp originGeldComp = new OriginGeldComp(this, world.sysGeld, familieMuster.geldMenge);
        //CompEigentuemerMitGeld compEigentuemerMitGeld = new CompEigentuemerMitGeld(this, originGeldComp);

        // TODO LOESCHEN //
        /*if (1==1) {
            Enums.ZeitEnum e1 = Enums.ZeitEnum.INGAME_JAHR;
            Enums.ZeitEnum e2 = Enums.ZeitEnum.INGAME_MONAT;
            if (e1.getClass().equals(e2.getClass())) {
                throw new IllegalStateException();
            } else {
                throw new IllegalStateException();
            }
        }*/
        ///////////////////

        this.mieterComp = new FamilieMieterComp(this, world.wohnenMatcher, originGeldComp);
        this.nameComp = new NameComp(this, familieMuster.name);

        this.tascheComp = new TascheComp(this, new MasterTasche(
                new NahrungTasche(new EntityGegInfoList()
                        .addAll(Enums.EnumGegenstandKlasse.NAHRUNG)),
                new WerkzeugTasche(new EntityGegInfoList()
                        .addAll(Enums.EnumGegenstandKlasse.WERKZEUG), this),
                new MedizinTasche(new EntityGegInfoList()
                        .addAll(Enums.EnumGegenstandKlasse.MEDIZIN), this),
                new SpielzeugTasche(new EntityGegInfoList()
                        .addAll(Enums.EnumGegenstandKlasse.SPIELZEUG))));

        //new SterbenCompVererben(familie, componentList); TODO Inhalte der Taschen an Dorf vererben

        for (DorfbewohnerMuster dorfbewohnerMuster : familieMuster.familienmitglieder) {
            new Dorfbewohner(world, dorf, dorfbewohnerMuster, this, new Vector2(0, 0));
        }
    }

    // ECS ////////////////////////////////////////////////////////////////////////////////////////

    public boolean add(FamilienmitgliedComp familienmitgliedComp) {
        Familie alteFamilie = familienmitgliedComp.getFamilie();

        if (alteFamilie == this) {
            return false;
        }
        if (alteFamilie != null) {
            alteFamilie.remove(familienmitgliedComp);
        }
        familienmitgliedList.add(familienmitgliedComp);

        familienmitgliedComp.einwohnerComp.setMieterComp(mieterComp);
        familienmitgliedComp.compGeld.settGeld(mieterComp.geldComp.getGeld());
        familienmitgliedComp.tascheComp.setMasterTasche(tascheComp.getMasterTasche());

        familienmitgliedComp.setFamilie(this);
        familienmitgliedComp.addListener(this);

        return true;
    }

    public boolean remove(FamilienmitgliedComp familienmitgliedComp) {
        if (familienmitgliedList.remove(familienmitgliedComp)) {
            familienmitgliedComp.addListener(this);
            return true;
        }
        return false;
    }

    @Override
    public void react(StoryTeller teller, EnumStory story) {
        if (teller instanceof FamilienmitgliedComp) {
            if (story.equals(EnumStory.DELETE)) {
                remove((FamilienmitgliedComp) teller);
            }
        }
    }

    // SONSTIGES //////////////////////////////////////////////////////////////////////////////////

    public int gettDurchschnGehalt(ZeitEnum enumZeit) {
        return 100; // TODO
        /*int summeGehalt = 0;
        int anzahlArbeitenderPersonen = 0;
        for (CompFamilienmitglied cf : familienmitglieder) {
            AiCompArbeitnehmer arbeitnehmer = cf.getCompArbeitnehmer();
            if (arbeitnehmer != null) {

            int einkommen = .einkommen(unitEnum);
            if (einkommen > 0) { //Einkommen bedeutet, dass auch Arbeitslose ein Gehalt haben koennen. So soll es sein, da Werkzeuge auch zum abbauen etc verwendet werden
                summeGehalt += einkommen;
                anzahlArbeitenderPersonen += 1;
            }
        }
        return summeGehalt / anzahlArbeitenderPersonen;*/
    }

    public int gettMieteNutzenExponent() {
        int mieteNutzenExponentSum = 0;
        for (FamilienmitgliedComp fc : familienmitgliedList) {
            mieteNutzenExponentSum += fc.einwohnerComp.dbDnaComp.dnaStrang.nutzenExponentWohnen;
        }
        return mieteNutzenExponentSum / familienmitgliedList.size();
    }

    // EINFACHE GETTER UND SETTER /////////////////////////////////////////////////////////////////
}
