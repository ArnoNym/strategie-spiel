package de.arnonym.strategiespiel.spielSchirm.gebaude;

import com.badlogic.gdx.math.Vector2;

import de.arnonym.strategiespiel.spielSchirm.Dorf;
import de.arnonym.strategiespiel.spielSchirm.World;
import de.arnonym.strategiespiel.framework.ecs.entity.Factory;

public abstract class GebaudeFactory<INFO extends GebaudeInfo> extends Factory {
    public final Dorf dorf;
    public final INFO info;

    public GebaudeFactory(World world, Dorf dorf, INFO info) {
        super(world);
        this.dorf = dorf;
        this.info = info;
    }

    public abstract Gebaude create(Vector2 position);
}
