package de.arnonym.strategiespiel.spielSchirm.gebaude;

import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;
import java.util.List;

import de.arnonym.strategiespiel.Strategiespiel;
import de.arnonym.strategiespiel.spielSchirm.Dorf;
import de.arnonym.strategiespiel.spielSchirm.World;
import de.arnonym.strategiespiel.framework.ecs.component.Comp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.bewegung.BewegungszielComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.baugrube.BaugrubeComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.gegenstaende.GegenstaendeComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.gegenstaende.GegenstaendeComp1;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.VariabelGeldComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.herrschaft.EigentuemerComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.klick.FensterKlickComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.rendern.HashMapRenderComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.rendern.UnbeweglichRenderComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.groesse.IsoGroesseComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.kollision.KollisionCompBauen;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.kollision.LaufenKollisionComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.name.NameComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.sonstiges.BlaupausenObjektComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.sonstiges.PositionComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.transformieren.TransComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.transformieren.VonZuTransComp;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.listSystems.privateListSystems.SysKlick;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.SysKollision;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.SysRender;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterOnKlick.OnKlickFenster;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterOnKlick.BaugrubeOnKlickFenster;
import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenOptik;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.vorstufe.EntityGegInfoList;
import de.arnonym.strategiespiel.framework.werkzeuge.textures.TextureHashMap;

public abstract class Gebaude<INFO extends GebaudeInfo> extends Entity {
    protected final World world;
    protected final Dorf dorf;
    protected final SysKollision sysKollision;
    protected final SysKlick sysKlick;
    protected final INFO info;
    protected final IsoGroesseComp isoGroesseComp;
    protected final NameComp nameComp;
    protected final PositionComp positionComp;
    protected final HashMapRenderComp compRender;

    public final BlaupausenObjektComp blaupausenObjektComp;

    protected BewegungszielComp bewegungszielComp;
    protected VariabelGeldComp variabelGeldComp;
    protected EigentuemerComp eigentuemerComp;

    protected Gebaude(World world, Dorf dorf, INFO info, Vector2 position) {
        super(world.manager);
        this.world = world;
        this.dorf = dorf;
        this.sysKlick = world.sysKlick;
        this.sysKollision = world.sysKollision;
        this.info = info;

        isoGroesseComp = new IsoGroesseComp(this, info.fundament);
        positionComp = new PositionComp(this, position);

        compRender = new UnbeweglichRenderComp(this, world.getSysRender(), positionComp,
                info.textureHashMap, SysRender.EnumZ.SYMBOL_IN_SPIELWELT, true);

        nameComp = new NameComp(this, info.name);

        activate();

        blaupausenObjektComp = new BlaupausenObjektComp(this, isoGroesseComp, positionComp,
                gettZuBaugrubeTransComp());
        blaupausenObjektComp.activate();
    }

    private VonZuTransComp gettZuBaugrubeTransComp() {
        KollisionCompBauen compKollisionBauen = new KollisionCompBauen(Gebaude.this, sysKollision,
                isoGroesseComp, positionComp);
        LaufenKollisionComp compKollisionLaufen = new LaufenKollisionComp(Gebaude.this,
                sysKollision, isoGroesseComp, positionComp, 0);
        bewegungszielComp = new BewegungszielComp(this, compKollisionLaufen, false);
        variabelGeldComp = new VariabelGeldComp(this);
        eigentuemerComp = new EigentuemerComp(dorf.eigentuemerComp, this, variabelGeldComp);

        // ! Reihenfolge beachten ! //
        FensterKlickComp fensterKlickComp = new FensterKlickComp(Gebaude.this, sysKlick,
                isoGroesseComp, positionComp, compRender, null);
        VonZuTransComp compTransVonBaugrubeZuGebaude = gettCompTransZuGebaude(fensterKlickComp);

        GegenstaendeComp gegenstaendeComp = new GegenstaendeComp1(this, compRender,
                new EntityGegInfoList().addAll());
        BaugrubeComp baugrubeComp = new BaugrubeComp(this, world.sysBaugrube, bewegungszielComp,
                gegenstaendeComp, eigentuemerComp, compRender,
                compTransVonBaugrubeZuGebaude, info.musterAuftragBauen);

        compTransVonBaugrubeZuGebaude.addLoeschenComps(baugrubeComp);
        fensterKlickComp.setOnKlickFenster(new BaugrubeOnKlickFenster(world, baugrubeComp, nameComp,
                compRender, Strategiespiel.skin));
        // ! Reihenfolge beachten ! //

        return new VonZuTransComp(
                Gebaude.this,
                TransComp.EnumDelete.TRANS_COMP,
                new Comp[]{baugrubeComp, compKollisionBauen, compKollisionLaufen, fensterKlickComp},
                new Comp[]{},
                () -> {
                    TextureHashMap textureHashMap = new TextureHashMap(
                            KonstantenOptik.gettBaugrubeTexture(
                                    isoGroesseComp.getGroesseInZellen()));
                    compRender.settAktionTextureHashMap(textureHashMap);
                    compRender.settZ(SysRender.EnumZ.ENTITIES); // Prueft auch SysRender-Index
                }
        );
    }

    private VonZuTransComp gettCompTransZuGebaude(FensterKlickComp fensterKlickComp) {
        List<Comp> activateCompList = new ArrayList<>();
        List<Comp> loescheCompList = new ArrayList<>();

        OnKlickFenster onKlickFenster = definiereGebaude(activateCompList, loescheCompList);

        return new VonZuTransComp(this, TransComp.EnumDelete.TRANS_COMP,
                activateCompList.toArray(new Comp[activateCompList.size()]),
                loescheCompList.toArray(new Comp[loescheCompList.size()]),
                new TransComp.TransLogic() {
                    @Override
                    public void trans() {
                        compRender.settAktionTextureHashMap(info.textureHashMap);
                        fensterKlickComp.setOnKlickFenster(onKlickFenster);
                    }
                });
    }

    protected abstract OnKlickFenster definiereGebaude(List<Comp> activateCompList, List<Comp> loescheCompList);
}