package de.arnonym.strategiespiel.spielSchirm.gebaude.haus;

import com.badlogic.gdx.math.Vector2;

import java.util.List;

import de.arnonym.strategiespiel.spielSchirm.Dorf;
import de.arnonym.strategiespiel.spielSchirm.World;
import de.arnonym.strategiespiel.framework.ecs.component.Comp;
import de.arnonym.strategiespiel.spielSchirm.gebaude.Gebaude;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterOnKlick.OnKlickFenster;

public class Haus extends Gebaude<HausInfo> {
    public Haus(World world, Dorf dorf, HausInfo hausInfo, Vector2 position) {
        super(world, dorf, hausInfo, position);
    }

    @Override
    protected OnKlickFenster definiereGebaude(List<Comp> activateCompList, List<Comp> loescheCompList) {
        return null;
    }
}
