package de.arnonym.strategiespiel.spielSchirm.gebaude.Lager;

import com.badlogic.gdx.math.Vector2;

import java.util.List;

import de.arnonym.strategiespiel.Strategiespiel;
import de.arnonym.strategiespiel.spielSchirm.Dorf;
import de.arnonym.strategiespiel.spielSchirm.World;
import de.arnonym.strategiespiel.spielSchirm.balancing.IdsGegenstaende;
import de.arnonym.strategiespiel.framework.ecs.component.Comp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.gegenstaende.GegenstaendeComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.gegenstaende.GegenstaendeComp1;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.gegenstaende.CompWirtschaftsdaten;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.VariabelGeldComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.herrschaft.EigentuemerComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.lager.LagerCompMitGeld;
import de.arnonym.strategiespiel.spielSchirm.gebaude.Gebaude;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterOnKlick.OnKlickFenster;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterOnKlick.OnKlickFensterLager;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.vorstufe.EntityGegInfo;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.vorstufe.EntityGegInfoList;

public class Lager extends Gebaude<LagerInfo> {
    public Lager(World world, Dorf dorf, LagerInfo lagerInfo, Vector2 position) {
        super(world, dorf, lagerInfo, position);
    }

    @Override
    protected OnKlickFenster definiereGebaude(List<Comp> activateCompList,
                                              List<Comp> loescheCompList) {
        EntityGegInfoList entityGegInfoListe = new EntityGegInfoList(info.maxPlatz, false);
        for (int id : info.gegenstaendeIds) {
            entityGegInfoListe.add(new EntityGegInfo(id, 0, null,
                    IdsGegenstaende.get(id).wert_create));
        }
        GegenstaendeComp gegenstaendeComp = new GegenstaendeComp1(this, compRender,
                entityGegInfoListe);
        CompWirtschaftsdaten compWirtschaftsdaten = new CompWirtschaftsdaten(this,
                world.sysWirtschaftsdatenManager, gegenstaendeComp);
        activateCompList.add(gegenstaendeComp);

        VariabelGeldComp compGeldVariabel = new VariabelGeldComp(this);
        EigentuemerComp geldEigentumComp = new EigentuemerComp(dorf.eigentuemerComp, this,
                compGeldVariabel);
        LagerCompMitGeld compLagerMitGeld = new LagerCompMitGeld(this, world.sysLager,
                dorf.gegMarketPriceSystem, geldEigentumComp, bewegungszielComp, gegenstaendeComp,
                compGeldVariabel);
        activateCompList.add(compLagerMitGeld);

        // todo loeschen
        gegenstaendeComp.gegenstandListe.gettDurchId(IdsGegenstaende.MAT_HOLZ_ID).mengeErhoehen(1000, false);

        return new OnKlickFensterLager(world, compGeldVariabel, compWirtschaftsdaten, nameComp,
                compRender, Strategiespiel.skin);
    }
}
