package de.arnonym.strategiespiel.spielSchirm.gebaude.haus;

import com.badlogic.gdx.math.Vector2;

import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.departments.bildung.ForschungAssignment;
import de.arnonym.strategiespiel.spielSchirm.gebaude.GebaudeInfo;
import de.arnonym.strategiespiel.framework.werkzeuge.textures.TextureHashMap;

public class HausInfo extends GebaudeInfo {
    public final int wert;
    public final int maxBewohner;
    public HausInfo(int id, String name, String beschreibung,
                    TextureHashMap textureHashMap, Vector2 fundament,
                    int[][] bauMaterial, float arbeitszeit,
                    ForschungAssignment forschungAuftrag_nl, int wert, int maxBewohner) {
        super(id, name, beschreibung, textureHashMap, fundament, bauMaterial, arbeitszeit,
                forschungAuftrag_nl);
        this.wert = wert;
        this.maxBewohner = maxBewohner;
    }
}
