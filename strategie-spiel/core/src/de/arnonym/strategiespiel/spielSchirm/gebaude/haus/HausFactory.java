package de.arnonym.strategiespiel.spielSchirm.gebaude.haus;

import com.badlogic.gdx.math.Vector2;

import de.arnonym.strategiespiel.spielSchirm.Dorf;
import de.arnonym.strategiespiel.spielSchirm.World;
import de.arnonym.strategiespiel.spielSchirm.gebaude.Gebaude;
import de.arnonym.strategiespiel.spielSchirm.gebaude.GebaudeFactory;

public class HausFactory extends GebaudeFactory<HausInfo> {
    public HausFactory(World world, Dorf dorf, HausInfo hausInfo) {
        super(world, dorf, hausInfo);
    }

    public Gebaude create(Vector2 position) {
        return new Haus(world, dorf, info, position);
    }
}
