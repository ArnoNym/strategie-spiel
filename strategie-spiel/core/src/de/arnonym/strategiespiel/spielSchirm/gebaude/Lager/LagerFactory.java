package de.arnonym.strategiespiel.spielSchirm.gebaude.Lager;

import com.badlogic.gdx.math.Vector2;

import de.arnonym.strategiespiel.spielSchirm.Dorf;
import de.arnonym.strategiespiel.spielSchirm.World;
import de.arnonym.strategiespiel.spielSchirm.gebaude.Gebaude;
import de.arnonym.strategiespiel.spielSchirm.gebaude.GebaudeFactory;

public class LagerFactory extends GebaudeFactory<LagerInfo> {
    public LagerFactory(World world, Dorf dorf, LagerInfo lagerInfo) {
        super(world, dorf, lagerInfo);
    }

    @Override
    public Gebaude create(Vector2 position) {
        return new Lager(world, dorf, info, position);
    }
}
