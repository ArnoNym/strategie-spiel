package de.arnonym.strategiespiel.spielSchirm.gebaude.Lager;

import com.badlogic.gdx.math.Vector2;

import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.departments.bildung.ForschungAssignment;
import de.arnonym.strategiespiel.spielSchirm.gebaude.GebaudeInfo;
import de.arnonym.strategiespiel.framework.werkzeuge.textures.TextureHashMap;

public class LagerInfo extends GebaudeInfo {
    public final int[] gegenstaendeIds;
    public final int maxPlatz;

    public LagerInfo(int id, String name, String beschreibung,
                     TextureHashMap textureHashMap, Vector2 fundament,
                     int[][] bauMaterial, float arbeitszeit,
                     ForschungAssignment forschungAuftrag_nl, int[] gegenstaendeIds,
                     int maxPlatz) {
        super(id, name, beschreibung, textureHashMap, fundament, bauMaterial, arbeitszeit,
                forschungAuftrag_nl);
        this.gegenstaendeIds = gegenstaendeIds;
        this.maxPlatz = maxPlatz;
    }
}
