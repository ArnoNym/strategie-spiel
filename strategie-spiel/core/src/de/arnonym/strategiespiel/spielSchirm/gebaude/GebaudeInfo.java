package de.arnonym.strategiespiel.spielSchirm.gebaude;

import com.badlogic.gdx.math.Vector2;

import de.arnonym.strategiespiel.framework.werkzeuge.textures.Texture;
import de.arnonym.strategiespiel.spielSchirm.Enums;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.departments.bildung.ForschungAssignment;
import de.arnonym.strategiespiel.spielSchirm.arbeit.auftrag.AssignmentMaterial;
import de.arnonym.strategiespiel.spielSchirm.info.muster.Info;
import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenBalance;
import de.arnonym.strategiespiel.framework.werkzeuge.textures.TextureHashMap;

public class GebaudeInfo extends Info {
    public final TextureHashMap textureHashMap;
    public final Vector2 fundament;
    public final AssignmentMaterial musterAuftragBauen;

    public GebaudeInfo(int id, String name, String beschreibung,
                       TextureHashMap textureHashMap, Vector2 fundament,
                       int[][] bauMaterial, float arbeitszeit,
                       ForschungAssignment forschungAuftrag_nl) {
        super(id, name, beschreibung, forschungAuftrag_nl);
        this.textureHashMap = textureHashMap;
        this.fundament = fundament;

        this.musterAuftragBauen = new AssignmentMaterial(id,
                KonstantenBalance.GEBAUDE_BAUEN_OPTIMALE_PROD_KOPF,
                KonstantenBalance.GEBAUDE_BAUEN_OPTIMALE_PROD_KOERPER, arbeitszeit,
                KonstantenBalance.GEBAUDE_BAUEN_MUSTER_SCHADEN, bauMaterial);
    }

    @Override
    public Texture getIconTexture() {
        return textureHashMap.get(Enums.EnumAktion.STANDARD_PRIMAERMENGE_VOLL);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    // GEGENSEITIGER AUSSCHLUSS ///////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////

    /*private Class<? extends Gebaude> gebaudeKlasse;
    private void settEnumGegenstandKlasse(Class<? extends Gebaude> gebaudeKlasse) {
        if (this.gebaudeKlasse != null) {
            throw new IllegalArgumentException("Es darf nur ein 'construct' verwendet werden! "
                    + "bisher:" + this.gebaudeKlasse.getSimpleName()
                    + "neu:" + gebaudeKlasse.getSimpleName());
        }
        this.gebaudeKlasse = gebaudeKlasse;
    }

    // Nahrung
    private float eiweissAnteil;
    private float vitamineAnteil;
    private float kohlenhydrateGesundAnteil;
    private float kohlenhydrateUngesundAnteil;
    public InfoGeg constructNahrung(float eiweissAnteil, float vitamineAnteil,
                                    float kohlenhydrateGesundAnteil,
                                    float kohlenhydrateUngesundAnteil) {
        settEnumGegenstandKlasse(Enums.EnumGegenstandKlasse.NAHRUNG);
        this.eiweissAnteil = eiweissAnteil;
        this.vitamineAnteil = vitamineAnteil;
        this.kohlenhydrateGesundAnteil = kohlenhydrateGesundAnteil;
        this.kohlenhydrateUngesundAnteil = kohlenhydrateUngesundAnteil;
        return this;
    }

    // Medizin
    private float gesundheit;
    public InfoGeg constructMedizin(float gesundheit) {
        settEnumGegenstandKlasse(Enums.EnumGegenstandKlasse.MEDIZIN);
        this.gesundheit = gesundheit;
        return this;
    }

    // Werkzeug
    private float produktivitaet;
    private float schutz;
    private Enums.EnumKoerperteil schuetztKoeperteil;
    public InfoGeg constructWerkzeug(float produktivitaet, float schutz,
                                     Enums.EnumKoerperteil schuetztKoeperteil) {
        settEnumGegenstandKlasse(Enums.EnumGegenstandKlasse.WERKZEUG);
        this.produktivitaet = produktivitaet;
        this.schutz = schutz;
        this.schuetztKoeperteil = schuetztKoeperteil;
        return this;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    // ZUSATZ /////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////

    // Herstellbar
    private boolean herstellbar = false;
    private IntIntHashMap inputHashMap;
    private int entstehendeMenge;
    private float herstellDauer;
    private float[] herstellenSchadenArray;
    public InfoGeg addHerstellbar(int[][] inputs, int entstehendeMenge, float produktionsDauer,
                                  float[] herstellenSchadenArray) {
        this.herstellbar = true;
        this.inputHashMap = new IntIntHashMap(inputs);
        this.entstehendeMenge = entstehendeMenge;
        this.herstellDauer = produktionsDauer;
        this.herstellenSchadenArray = herstellenSchadenArray;
        return this;
    }

    // Abbaubar
    private boolean abbaubar = false;
    private float abbauDauer;
    private float[] abbauSchadenArray;
    public InfoGeg addAbbaubar(float abbauDauer, float[] abbauSchadenArray) {
        this.abbaubar = true;
        this.abbauDauer = abbauDauer;
        this.abbauSchadenArray = abbauSchadenArray;
        return this;
    }*/
}
