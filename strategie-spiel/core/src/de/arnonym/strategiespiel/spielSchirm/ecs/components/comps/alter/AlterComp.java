package de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.alter;

import de.arnonym.strategiespiel.framework.ecs.component.Comp;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.SysAltern;

public abstract class AlterComp extends Comp {
    private float alter_sek;

    protected AlterComp(Entity entity, SysAltern sysAltern, Comp[] comps, float alter_sek) {
        super(sysAltern, entity, comps);
        this.alter_sek = alter_sek;
    }

    public void update(float delta) {
        alter_sek += delta;
    }

    public float getAlter_sek() {
        return alter_sek;
    }
}