package de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.viewport.Viewport;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;

import de.arnonym.strategiespiel.spielSchirm.World;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.rendern.RenderComp;
import de.arnonym.strategiespiel.framework.ecs.collections.System;

public class SysRender extends System<RenderComp> {
    private final LinkedHashMap<EnumZ, List<RenderComp>> renderHashMap = new LinkedHashMap<>(); // TODO Datentyp List ueberdenken. Sollte schon eine Liste sein, da Reihenfolge wichtig ist aber vielleicht eine Linked List? Linkedlist sollte perfomanter sein wenn ich es schaffe gettIndex zu ueberarbeiten
    private final World world;

    public enum EnumZ {
        UI,
        SYMBOL_IN_SPIELWELT,
        ENTITIES,
        ZELLE
    }

    public SysRender(World world) {
        this.world = world;
        renderHashMap.put(EnumZ.ZELLE, new ArrayList<>());
        renderHashMap.put(EnumZ.ENTITIES, new ArrayList<>());
        renderHashMap.put(EnumZ.SYMBOL_IN_SPIELWELT, new ArrayList<>());
        renderHashMap.put(EnumZ.UI, new ArrayList<>());
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    protected void update_or(float delta) {
        throw new IllegalStateException("Dont call this!");
    }

    public void render(SpriteBatch spriteBatch, float delta) {
        if (!isActiv()) return;

        for (List<RenderComp> renderList : renderHashMap.values()) {
            for (RenderComp renderComp : renderList) {
                renderComp.render(spriteBatch, delta);
            }
        }
    }

    // PRUEFEN ////////////////////////////////////////////////////////////////////////////////////

    public void preufeVisible(float kameraLeft, float kameraRight, float kameraBottom,
                              float kameraTop) {
        for (List<RenderComp> renderList : renderHashMap.values()) {
            for (RenderComp renderComp : renderList) {
                renderComp.preufeVisible(kameraLeft, kameraRight, kameraBottom, kameraTop);
            }
        }
    }

    public void preufeVisible(RenderComp renderComp) {
        Viewport viewport = world.getViewport();
        float width = viewport.getWorldWidth();
        float heigt = viewport.getWorldHeight();
        Vector2 position = world.getIngameKamera().gettPosition();
        float x = position.x - width/2;
        float y = position.y - heigt/2;
        renderComp.preufeVisible(x, x + width, y, y + heigt);
    }

    // ECS ////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public boolean connect_or(RenderComp renderComp) {
        List<RenderComp> renderList = renderHashMap.get(renderComp.getZ());
        Vector2 position = renderComp.getPositionComp().getPosition();
        int index = gettIndex(renderList, position, 0, renderList.size() - 1);
        renderList.add(index, renderComp);
        preufeVisible(renderComp);
        return true;
    }

    @Override
    public boolean disconnect_or(RenderComp renderComp) {
        for (List<RenderComp> renderList : renderHashMap.values()) {
            if (renderList.remove(renderComp)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean contains(RenderComp renderComp) {
        for (List<RenderComp> l : renderHashMap.values()) { // todo effizienz
            for (RenderComp cr : l) {
                if (renderComp.equals(cr)) {
                    return true;
                }
            }
        }
        return false;
    }

    /*@Override
    public int size() {
        int s = 0;
        for (List<RenderComp> l : renderHashMap.values()) {
            s += l.size();
        }
        return s;
    }*/

    @Override
    public <COLL extends Collection<RenderComp>> COLL toCollection(COLL coll) {
        for (List<RenderComp> l : renderHashMap.values()) {
            coll.addAll(l);
        }
        return coll;
    }

    // FINDE INDEX ////////////////////////////////////////////////////////////////////////////////

    private static int gettIndex(List<RenderComp> renderList, Vector2 positon,
                                 int heighYIndex, int lowYIndex) {
        if (renderList.size() == 0) {
            return 0;
        }

        if (renderList.size() == 1 || lowYIndex - heighYIndex == 1) {
            float lowY = renderList.get(lowYIndex).getPositionComp().getPosition().y;
            float heighY = renderList.get(heighYIndex).getPositionComp().getPosition().y;
            if (positon.y > heighY) {
                return heighYIndex;
            } else if (positon.y < lowY) {
                return lowYIndex + 1;
            } else {
                return lowYIndex;
            }
        }

        int testeIndex = heighYIndex + (lowYIndex - heighYIndex) / 2;
        float testeY = renderList.get(testeIndex).getPositionComp().getPosition().y;
        if (positon.y > testeY) {
            lowYIndex = testeIndex;
        } else if (positon.y < testeY) {
            heighYIndex = testeIndex;
        } else {
            return testeIndex;
        }
        return gettIndex(renderList, positon, heighYIndex, lowYIndex);
    }
}
