package de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.name;

import de.arnonym.strategiespiel.framework.ecs.component.Comp;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;

public class NameComp extends Comp {
    private String name;
    private String vorName;
    private String nachName;

    public NameComp(Entity entity, String name) {
        super(entity);
        setName(name);
    }

    public NameComp(Entity entity, String vorName, String nachName) {
        super(entity);
        settName(vorName, nachName);
    }

    /*@Override
    public int ident() {
        return Identifiable.IDENT_COMP_NAME;
    }*/

    public void settName(String vorName, String nachName) {
        this.vorName = vorName;
        this.nachName = nachName;
        this.name = vorName + " " + nachName;
    }

    public void settVorName(String vorName) {
        settName(vorName, nachName);
    }

    public void settNachName(String nachName) {
        settName(vorName, nachName);
    }

    // EINFACHE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public void setName(String name) {
        this.name = name;
        // TODO Wenn Name mit Luecke geschrieben in vor und Nachname unterteilen
    }

    public String getName() {
        return name;
    }

    public String getVorName() {
        return vorName;
    }

    public String getNachName() {
        return nachName;
    }
}
