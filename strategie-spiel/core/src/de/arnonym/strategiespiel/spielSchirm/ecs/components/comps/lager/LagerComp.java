package de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.lager;

import de.arnonym.strategiespiel.framework.ecs.component.Comp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.bewegung.BewegungszielComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.herrschaft.EigentumComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.gegenstaende.GegenstaendeComp;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.listSystems.privateListSystems.SysLager;
import de.arnonym.strategiespiel.framework.ecs.collections.System;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.listSystems.privateListSystems.priceSystems.market.GegMarketPriceSys;
import de.arnonym.strategiespiel.framework.ecs.collections.Matcher;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.CollectionUtils;

public abstract class LagerComp extends Comp {
    public final EigentumComp eigentumComp;
    public final BewegungszielComp bewegungszielComp;
    public final GegenstaendeComp gegenstaendeComp;

    public LagerComp(Entity entity, SysLager sysLager, GegMarketPriceSys gegMarketPriceSystem,
                     EigentumComp eigentumComp, BewegungszielComp bewegungszielComp,
                     GegenstaendeComp gegenstaendeComp, Comp... comps) {
        super(new System[]{sysLager, gegMarketPriceSystem}, entity,
                CollectionUtils.combineComp(comps, eigentumComp, bewegungszielComp, gegenstaendeComp));
        this.eigentumComp = eigentumComp;
        this.bewegungszielComp = bewegungszielComp;
        this.gegenstaendeComp = gegenstaendeComp;
    }
}
