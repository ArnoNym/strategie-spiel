package de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld;

import de.arnonym.strategiespiel.framework.ecs.component.Comp;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.framework.ecs.collections.System;
import de.arnonym.strategiespiel.framework.ecs.collections.Matcher;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.Geld;

public abstract class GeldComp extends Comp {
    public final long entityId;

    public GeldComp(Entity entity) {
        super(entity);
        this.entityId = entity.id;
    }

    public GeldComp(Entity entity, System systems) {
        super(systems, entity);
        this.entityId = entity.id;
    }

    public boolean mengeErhoehen(int menge, boolean reserviertLegen) {
        if (menge == 0) return false;
        getGeld().mengeErhoehen(entityId, menge, reserviertLegen);
        return true;
    }

    public boolean mengeVerringern(int menge, boolean reserviertNehmen) {
        if (menge == 0) return false;
        getGeld().mengeVerringern(entityId, menge, reserviertNehmen);
        return true;
    }

    public abstract Geld getGeld();




















/*
    private Geld geld = null;
    private final GeldManager geldManager = new GeldManager(this);

    public CompGeld(Entity entityId) {
        super(entityId);
    }

    public void update(float delta) {
        geldManager.update(delta);
    }

    public void mengeErhoehen(int menge, boolean reserviertLegen) {
        geld.mengeErhoehen(entityId, menge, reserviertLegen);
    }

    public void mengeVerringern(int menge, boolean reserviertNehmen) {
        geld.mengeVerringern(entityId, menge, reserviertNehmen);
    }

    // KOMPLEXE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public void settGeld(Geld geld) {
        this.geldManager.updateDaten(); // Ein letztes mal mit altem Geld aktualisieren
        this.geld = geld;
    }

    // EINFACHE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public Geld getGeld() {
        return geld;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////

    public FinalGeld finialize() {
        return new FinalGeld(entityId, geld);
    }

    public class FinalGeld {
        public final int entityId;
        public final Geld geld;

        public FinalGeld(int entityId, Geld geld) {
            this.entityId = entityId;
            this.geld = geld;
        }

        public void mengeErhoehen(int menge, boolean reserviertLegen) {
            geld.mengeErhoehen(entityId, menge, reserviertLegen);
        }

        public void mengeVerringern(int menge, boolean reserviertNehmen) {
            geld.mengeVerringern(entityId, menge, reserviertNehmen);
        }
    }*/
}
