package de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.gegenstaende;

import de.arnonym.strategiespiel.framework.ecs.component.Comp;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.listSystems.privateListSystems.SysWirtschaftsdatenManager;
import de.arnonym.strategiespiel.spielSchirm.zahlen.wirtschaftsdaten.gegenstaende.GegenstandManager;

public class CompWirtschaftsdaten extends Comp {
    public final GegenstaendeComp gegenstaendeComp;
    public final GegenstandManager gegenstandManager;

    public CompWirtschaftsdaten(Entity entity,
                                SysWirtschaftsdatenManager sysWirtschaftsdatenManager,
                                GegenstaendeComp gegenstaendeComp) {
        super(sysWirtschaftsdatenManager, entity, gegenstaendeComp);
        this.gegenstaendeComp = gegenstaendeComp;
        this.gegenstandManager = new GegenstandManager(gegenstaendeComp.gegenstandListe);
    }
}
