package de.arnonym.strategiespiel.spielSchirm.ecs.components.comps;

import de.arnonym.strategiespiel.framework.ecs.component.Comp;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.spielSchirm.World;

public class WorldComp extends Comp {
    public final World world;

    public WorldComp(Entity entity, World world) {
        super(entity);
        this.world = world;
    }
}
