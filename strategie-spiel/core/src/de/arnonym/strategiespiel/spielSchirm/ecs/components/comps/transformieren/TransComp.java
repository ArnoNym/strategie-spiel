package de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.transformieren;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import de.arnonym.strategiespiel.framework.ecs.component.Comp;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;

public class TransComp extends Comp {
    public final Entity entity;
    private EnumDelete enumDelete;
    private final List<TransLogic> transLogicList = new ArrayList<>();
    private boolean transed;

    public TransComp(Entity entity, EnumDelete enumDelete, TransLogic... transLogics) {
        super(entity);
        this.entity = entity;
        this.enumDelete = enumDelete;
        transLogicList.addAll(Arrays.asList(transLogics));
    }

    public final boolean trans() {
        if (transed) {
            /* zB wenn mehrere Sterbebedingungen nacheinander auftreten (zB Baum transed aufgrund
            von Altersschwaeche zu einem toten Baum und dann nocheinmal aufgrund der Mengen
            Verringerung durch vergammeln) */
            return true;
        }
        transed = true;

        if (!trans_or()) {
            return false;
        }
        for (TransLogic transLogic : transLogicList) {
            transLogic.trans();
        }
        switch (enumDelete) {
            case ENTITY:
                entity.delete();
                break;
            case TRANS_COMP:
                delete();
                break;
        }
        return true;
    }

    protected boolean trans_or() {
        return true; // Override
    }

    public void addTransLogic(TransLogic transLogic) {
        transLogicList.add(transLogic);
    }

    public void setEnumDelete(EnumDelete enumDelete) {
        this.enumDelete = enumDelete;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////

    public interface TransLogic {
        void trans();
    }

    public enum EnumDelete {
        NONE,
        ENTITY,
        TRANS_COMP
    }
}
