package de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.gegenstaende;

import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.rendern.HashMapRenderComp;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.vorstufe.EntityGegInfoList;

public class GegenstaendeComp2 extends GegenstaendeComp {
    private final Entity entity;

    public GegenstaendeComp2(Entity entity, HashMapRenderComp hashMapRenderComp,
                             EntityGegInfoList entityGegInfoListe) {
        super(entity, hashMapRenderComp, entityGegInfoListe);
        this.entity = entity;
    }

    @Override
    protected void platzEmpty() {
        entity.delete();
    }
}
