package de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.listSystems.privateListSystems;

import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.werte.HabitalwerteComp;
import de.arnonym.strategiespiel.framework.ecs.collections.SetSystem;

public class HabitalwerteSys extends SetSystem<HabitalwerteComp> {
    public HabitalwerteSys() {
    }

    @Override
    protected void update_or(float delta) {
        for (HabitalwerteComp hc : compSet) {
            hc.update(delta);
        }
    }
}
