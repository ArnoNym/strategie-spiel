package de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.sonstiges;

import de.arnonym.strategiespiel.framework.ecs.component.Comp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.groesse.IsoGroesseComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.transformieren.TransComp;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;

public class BlaupausenObjektComp extends Comp {
    private final Entity entity;
    public final IsoGroesseComp isoGroesseComp;
    public final PositionComp positionComp;
    public final TransComp zuBaugrubeTransComp;

    public BlaupausenObjektComp(Entity entity, IsoGroesseComp isoGroesseComp,
                                PositionComp positionComp, TransComp zuBaugrubeTransComp) {
        super(entity, isoGroesseComp, positionComp, zuBaugrubeTransComp);
        this.entity = entity;
        this.isoGroesseComp = isoGroesseComp;
        this.positionComp = positionComp;
        this.zuBaugrubeTransComp = zuBaugrubeTransComp;

        /*CompSimpleKlick compSimpleKlick = new CompSimpleKlick(true, entityId, sysKlick) {
            @Override
            public void gotHit() {
                compTransZuBaugrube.trans_or();
            }
        };
        compSimpleKlick.handle(this);*/
    }

    public void entitiyDelete() {
        entity.delete();
    }

    public IsoGroesseComp getIsoGroesseComp() {
        return isoGroesseComp;
    }

    public PositionComp getPositionComp() {
        return positionComp;
    }
}
