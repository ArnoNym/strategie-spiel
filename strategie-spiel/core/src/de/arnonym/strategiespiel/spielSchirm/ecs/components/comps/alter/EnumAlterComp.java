package de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.alter;

import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.framework.werkzeuge.collections.groups.TwoGroup;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.SysAltern;
import de.arnonym.strategiespiel.spielSchirm.Enums;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.transformieren.TransComp;

public class EnumAlterComp extends TransAlterComp {
    private Enums.EnumAlter enumAlter;

    public EnumAlterComp(Entity entity, SysAltern sysAltern, boolean exakt, float alter_sek,

                         TwoGroup<TransComp, Float> transComp_transAlterMittel,
                         TwoGroup<TransComp, Float> transComp_transAlterAlt,
                         TwoGroup<TransComp, Float> transComp_transAlterTod) {
        super(entity, sysAltern, exakt, alter_sek, transComp_transAlterMittel,
                transComp_transAlterAlt, transComp_transAlterTod);
        settEnumAlter();
    }

    @Override
    public void update(float delta) {
        super.update(delta);
        settEnumAlter(); // todo push loesung. Macht das in der Performance ienen Unterschied??
    }

    private void settEnumAlter() {
        switch (size()) {
            case 3:
                enumAlter = Enums.EnumAlter.JUNG;
                break;
            case 2:
                enumAlter = Enums.EnumAlter.MITTEL;
                break;
            case 1:
                enumAlter = Enums.EnumAlter.ALT;
                break;
            case 0: // Tod
                if (enumAlter != null) break;
                // else default ...
            default:
                throw new IllegalStateException("size: "+size());
        }
    }

    // EINFACHE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public Enums.EnumAlter getEnumAlter() {
        return enumAlter;
    }
}
