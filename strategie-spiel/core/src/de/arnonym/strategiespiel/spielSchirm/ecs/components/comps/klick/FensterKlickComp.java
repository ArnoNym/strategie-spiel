package de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.klick;

import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.groesse.CompGroesse;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.rendern.RenderComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.sonstiges.PositionComp;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.listSystems.privateListSystems.SysKlick;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterOnKlick.OnKlickFenster;
import de.arnonym.strategiespiel.framework.werkzeuge.Paket;

public class FensterKlickComp extends CompKlick {
    private OnKlickFenster onKlickFenster;
    private final Paket<? extends OnKlickFenster> onKlickFensterPaket;

    public FensterKlickComp(Entity entity, SysKlick sysKlick, CompGroesse compGroesse,
                            PositionComp positionComp, RenderComp renderComp,
                            Paket<? extends OnKlickFenster> onKlickFensterPaket) {
        super(entity, sysKlick, compGroesse, positionComp, renderComp);
        this.onKlickFensterPaket = onKlickFensterPaket;
    }

    @Override
    public void gotHit() {
        if (onKlickFenster == null) {
            onKlickFenster = onKlickFensterPaket.entpacken();
        }
        onKlickFenster.oeffnen(true);
    }

    @Override
    public void delete() {
        if (onKlickFenster != null) {
            onKlickFenster.delete();
        }
        super.delete();
    }

    public OnKlickFenster getOnKlickFenster() {
        return onKlickFenster;
    }

    public void setOnKlickFenster(OnKlickFenster onKlickFenster) {
        this.onKlickFenster = onKlickFenster;
    }
}
