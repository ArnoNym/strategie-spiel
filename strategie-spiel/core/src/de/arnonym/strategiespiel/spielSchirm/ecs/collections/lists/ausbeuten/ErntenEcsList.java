package de.arnonym.strategiespiel.spielSchirm.ecs.collections.lists.ausbeuten;

import de.arnonym.strategiespiel.framework.listener.DeletableStoryListener;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.ausbeuten.FruechteComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.herrschaft.EigentuemerComp;

public class ErntenEcsList extends AuszubeutenEcsList<FruechteComp> {
    public ErntenEcsList(DeletableStoryListener parent, EigentuemerComp eigentumComp) {
        super(parent, eigentumComp);
    }
}
