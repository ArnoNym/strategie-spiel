package de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.rendern;

import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.sonstiges.PositionComp;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.SysRender;
import de.arnonym.strategiespiel.framework.werkzeuge.textures.TextureHashMap;

public class UnbeweglichRenderComp extends HashMapRenderComp {

    public UnbeweglichRenderComp(Entity entity, SysRender sysRendern, PositionComp positionComp,
                                 TextureHashMap textureHashMap, SysRender.EnumZ z,
                                 boolean pruefeRenderIndex) {
        super(entity, sysRendern, positionComp, textureHashMap, z, pruefeRenderIndex);
    }
}
