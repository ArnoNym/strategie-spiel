package de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.sonstiges;

import com.badlogic.gdx.math.Vector2;

import de.arnonym.strategiespiel.framework.ecs.component.Comp;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;

public class PositionComp extends Comp {
    public final Vector2 position;

    public PositionComp(Entity entity, Vector2 position) {
        super(entity);
        this.position = position;
    }

    // EINFACHE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public void settPosition(Vector2 position) {
        settPosition(position.x, position.y);
    }

    public void settPosition(float x, float y) {
        this.position.x = x;
        this.position.y = y;
    }

    public Vector2 getPosition() {
        return position;
    }
}
