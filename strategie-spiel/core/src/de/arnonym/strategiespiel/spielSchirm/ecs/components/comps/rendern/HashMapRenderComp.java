package de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.rendern;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.sonstiges.PositionComp;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.SysRender;
import de.arnonym.strategiespiel.spielSchirm.Enums;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.TextureUtils;
import de.arnonym.strategiespiel.framework.werkzeuge.textures.TextureHashMap;
import de.arnonym.strategiespiel.framework.werkzeuge.textures.Texture;

public class HashMapRenderComp extends RenderComp {
    private final HashMap<Texture, Float> renderHashMap = new HashMap<>();

    private TextureHashMap textureHashMap;
    private Texture mainTexture;

    public HashMapRenderComp(Entity entity, SysRender sysRendern, PositionComp positionComp,
                             TextureHashMap textureHashMap, SysRender.EnumZ z,
                             boolean pruefeRenderIndex) { // TODO
        super(entity, sysRendern, positionComp, z);
        settAktionTextureHashMap(textureHashMap);
    }

    protected void render_or(SpriteBatch spriteBatch, float delta) {
        if (renderHashMap.isEmpty()) {
            renderHashMap.put(mainTexture, -1f);
            mainTexture.start();
        }

        List<Texture> removeList = new ArrayList<>();
        for (Map.Entry<Texture, Float> entry : renderHashMap.entrySet()) {
            Texture texture = entry.getKey();
            Float restDauer = entry.getValue();

            Vector2 p = positionComp.getPosition();
            float renderX = p.x - texture.leftWidth;
            float renderY = p.y - texture.bottomHeight;

            TextureUtils.zeichneTexturRegion(spriteBatch, texture.get(),
                    new Vector2(renderX, renderY), false);

            if (restDauer != -1) {
                float neueRestDauer = restDauer - delta;
                if (neueRestDauer < 0) {
                    removeList.add(texture);
                } else {
                    entry.setValue(neueRestDauer);
                }
            }
        }

        for (Texture myTr : removeList) {
            renderHashMap.remove(myTr);
        }
    }

    // PRUEFE /////////////////////////////////////////////////////////////////////////////////////

    protected boolean preufeVisible_or(float kameraLeft, float kameraRight, float kameraBottom,
                                       float kameraTop) {
        float x = positionComp.getPosition().x;
        float y = positionComp.getPosition().y;
        float width = mainTexture.get().getRegionWidth();
        float heigt = mainTexture.get().getRegionHeight();
        /* + und - width, da nicht eindeutig ist wo in der Textur sich die Position befindet und es
        einfacher ist als CompGroesse etc zu uebergeben */
        return (!(kameraLeft >= x + width || kameraRight <= x - width
                || kameraBottom >= y + heigt || kameraTop <= y - heigt));
    }

    // IMMITIERE LISTE ////////////////////////////////////////////////////////////////////////////

    public void add(Enums.EnumAktion enumAktion) {
        Texture texture = textureHashMap.get(enumAktion);
        if (texture == null) {
            return;
        }
        add(texture, texture.enumAddTexture, texture.standardDauer_code);
    }

    public void add(Enums.EnumAktion enumAktion, Enums.EnumAddTexture enumAddTexture_nl,
                    float dauer_code) {
        Texture texture = textureHashMap.get(enumAktion);
        if (texture == null) {
            return;
        }
        add(texture, enumAddTexture_nl, dauer_code);
    }

    private void add(Texture texture, Enums.EnumAddTexture enumAddTexture_nl, float dauer_code) {
        /* dauer_code
        > 0 : stufeDauer_sek die die Textur angezeigt werden soll
        = -1 : unendlich lange anzeigen
        = -2 : In Texture angegebene Dauer verwenden */

        if (enumAddTexture_nl == null) {
            enumAddTexture_nl = texture.enumAddTexture;
        }
        switch (enumAddTexture_nl) {
            case ERSETZE_WENN_UNGLEICH:
                if (mainTexture == texture) {
                    return;
                }
            case ERSETZE_IMMER:
                renderHashMap.remove(mainTexture);
                mainTexture = texture;
        }

        if (dauer_code == -2) {
            dauer_code = texture.standardDauer_code;
        }

        renderHashMap.put(texture, dauer_code);
        texture.start();
    }

    public void remove(Enums.EnumAktion enumAktion) {
        renderHashMap.remove(textureHashMap.get(enumAktion));
    }

    public void clear() {
        renderHashMap.clear();
    }

    // KOMPLEXE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public void settAktionTextureHashMap(TextureHashMap textureHashMap) {
        clear();
        this.textureHashMap = textureHashMap;
        this.mainTexture = textureHashMap.get(Enums.EnumAktion.STANDARD_PRIMAERMENGE_VOLL);
    }

    // EINFACHE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public Texture getMainTexture() {
        return mainTexture;
    }
}
