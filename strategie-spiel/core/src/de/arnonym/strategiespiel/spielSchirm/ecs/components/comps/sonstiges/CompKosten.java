package de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.sonstiges;

import de.arnonym.strategiespiel.framework.ecs.component.Comp;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.framework.werkzeuge.collections.hashMaps.IntIntHashMap;

public class CompKosten extends Comp {
    private final IntIntHashMap idMengeHashMap;

    public CompKosten(Entity entity, IntIntHashMap idMengeHashMap) {
        super(entity);
        this.idMengeHashMap = idMengeHashMap;
    }

    public IntIntHashMap getIdMengeHashMap() {
        return new IntIntHashMap(idMengeHashMap); // TODO noetig?
    }
}
