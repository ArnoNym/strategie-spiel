package de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.bewegung;

import java.util.ArrayList;
import java.util.List;

import de.arnonym.strategiespiel.framework.ecs.component.Comp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.rendern.BeweglichRenderComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.sonstiges.PositionComp;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.spielSchirm.worldMap.WorldMap;

public class BewegungComp extends Comp {
    public final WorldMap worldMap;
    public final Entity entity;
    public final PositionComp positionComp;
    public final BeweglichRenderComp beweglichRenderComp;
    public final float bewegungsgeschwindigkeit;
    public final List<BewegungszielComp> unerreichbarList = new ArrayList<>();

    private BewegungszielComp inBewegungsziel = null;

    public BewegungComp(WorldMap worldMap,
                        Entity entity,
                        PositionComp positionComp, BeweglichRenderComp beweglichRenderComp,
                        float bewegungsgeschwindigkeit) {
        super(entity, positionComp, beweglichRenderComp);
        this.entity = entity;
        this.worldMap = worldMap;
        this.positionComp = positionComp;
        this.beweglichRenderComp = beweglichRenderComp;
        this.bewegungsgeschwindigkeit = bewegungsgeschwindigkeit;
    }

    public void eintreten(BewegungszielComp bewegungszielComp) {
        if (bewegungszielComp == null) {
            throw new IllegalArgumentException("Verwende austreten()!");
        }
        if (inBewegungsziel != null) {
            throw new IllegalArgumentException("Vorher aus anderem Ziel austreten!");
        }
        if (!bewegungszielComp.betretbar) {
            return;
        }
        bewegungszielComp.add(this);
        inBewegungsziel = bewegungszielComp;
        beweglichRenderComp.setInBewegungsziel(true);
    }

    public void austreten() {
        if (inBewegungsziel == null) return;
        inBewegungsziel.remove(this);
        inBewegungsziel = null;
        beweglichRenderComp.setInBewegungsziel(false);
    }

    public BewegungszielComp getInBewegungsziel() {
        return inBewegungsziel;
    }

    public void setInBewegungsziel(BewegungszielComp inBewegungsziel) {
        if (this.inBewegungsziel != null && inBewegungsziel != null) {
            throw new IllegalArgumentException();
        }
        this.inBewegungsziel = inBewegungsziel;
    }
}
