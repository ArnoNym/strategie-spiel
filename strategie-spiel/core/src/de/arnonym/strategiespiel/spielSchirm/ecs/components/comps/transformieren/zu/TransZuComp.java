package de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.transformieren.zu;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import de.arnonym.strategiespiel.framework.ecs.component.Comp;
import de.arnonym.strategiespiel.framework.ecs.component.Component;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.transformieren.TransComp;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;

public abstract class TransZuComp extends TransComp {
    private final List<MussVorhandenComp> mussVorhandenCompList = new ArrayList<>();
    private final List<KannVorhandenComp> kannVorhandenCompList = new ArrayList<>();
    private final Entity entity;

    public TransZuComp(Entity entity, EnumDelete enumDelete, TransLogic... transLogics) {
        super(entity, enumDelete, transLogics);
        this.entity = entity;
    }

    @Override
    public final boolean trans_or() {
        List<Component> componentList = entity.gettComponents(new LinkedList<>(), Component.class);

        // Pruefe ob alle Comps die zu Beginn der Trans vorhanden sein muessen auch vorhanden sind
        for (MussVorhandenComp mvc : mussVorhandenCompList) {
            Component removeComponent = null;
            for (Component c : componentList) {
                if (mvc.zuweisen(c)) {
                    removeComponent = c;
                    break;
                }
            }
            if (removeComponent == null) {
                return false; // Kann nicht transformiert werden
            } else {
                componentList.remove(removeComponent);
            }
        }

        // Erstelle alle Comps die noch nicht vorhanden sind, es aber am Ende der Trans sein sollen
        for (KannVorhandenComp kvc : kannVorhandenCompList) {
            Component removeComponent = null;
            for (Component c : componentList) {
                if (kvc.zuweisen(c)) {
                    removeComponent = c;
                    break;
                }
            }
            if (removeComponent == null) {
                kvc.ertellen();
            } else {
                componentList.remove(removeComponent);
            }
        }

        // Loesche alle ueberfluessigen Componenten
        for (Component component : componentList) {
            component.delete();
        }

        return true;
    }

    // INNER CLASS ////////////////////////////////////////////////////////////////////////////////

    protected abstract class MussVorhandenComp<C extends Component> {
        public MussVorhandenComp() {
            mussVorhandenCompList.add(this);
        }

        public final boolean zuweisen(Component c) {
            try {
                zuweisen_or((C) c);
                return true;
            } catch (ClassCastException e) {
                return false;
            }
        }

        protected abstract void zuweisen_or(C c);
    }

    protected abstract class KannVorhandenComp<C extends Component> extends MussVorhandenComp<C> {
        public KannVorhandenComp() {
            kannVorhandenCompList.add(this);
        }

        public final void ertellen() {
            C c = ertellen_or();
            if (c == null) {
                throw new IllegalStateException();
            }
            zuweisen_or(c);
        }

        protected abstract C ertellen_or();
    }

    // PROTECTED GETTER UND SETTER ////////////////////////////////////////////////////////////////
}
