package de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.herrschaft;

import java.util.Collection;

import de.arnonym.strategiespiel.framework.ecs.component.Comp;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.framework.werkzeuge.collections.Collector;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.GeldComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.OriginGeldComp;

public class EigentumComp extends Comp {
    private EigentuemerComp eigentuemer;

    public EigentumComp(EigentuemerComp eigentuemer, Entity entity) {
        this(eigentuemer, entity, new Comp[]{});
    }

    protected EigentumComp(EigentuemerComp eigentuemer, Entity entity, Comp... comps) {
        this(entity, comps);
        this.eigentuemer = eigentuemer;

        this.eigentuemer.addEigentum(this);
    }

    public EigentumComp(Entity entity) {
        this(entity, new Comp[]{});
    }

    protected EigentumComp(Entity entity, Comp... comps) {
        super(entity, comps);
    }

    // KOMPLEXE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public void setEigentuemer(EigentuemerComp eigentuemerComp) {
        this.eigentuemer.removeEigentum(this);

        this.eigentuemer = eigentuemerComp;
        this.eigentuemer.addEigentum(this);
    }

    public static boolean isSelberEigentuemer(EigentumComp eigentumComp1,
                                               EigentumComp eigentumComp2) {
        return eigentumComp1.eigentuemer.equals(eigentumComp2.eigentuemer);
    }

    public static boolean isEigentumVon(EigentumComp eigentumComp,
                                         EigentuemerComp eigentuemerComp) {
        return eigentumComp.eigentuemer.equals(eigentuemerComp);
    }

    // EINFACHE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    /** Mit Vorsicht geniessen, da die geld dann zu einer anderen Entity gehoert was zB zu
     * Fehlern beim erstellen der Wirtschaftsdaten fuehrt. Dennoch noetig wenn
     * 1. Ein Task Geld benoetigt, die Entity an der es ausgefuehrt wird allerdings keine CompGeld
     * besitzt
     * 1.1. BodenStapel die aufgeraumt werden und wo das Geld an den Eigentuemer gehen soll
     * 1.2. Baume die Gefaellt werden sollen und wo das Geld an den Auftraggeber (Also das Dorf)
     * und nicht den Eigentumer (Also die WorldMap) gehen soll
     * 2. Beim Erstellen neuer BodenStapel muss der Eigentuemer gespeichert werden */
    public EigentuemerComp getEigentuemer() {
        return eigentuemer;
    }
}
