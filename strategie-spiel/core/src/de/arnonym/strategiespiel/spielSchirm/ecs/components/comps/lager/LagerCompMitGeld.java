package de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.lager;

import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.bewegung.BewegungszielComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.gegenstaende.GegenstaendeComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.GeldComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.herrschaft.EigentumComp;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.listSystems.privateListSystems.SysLager;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.listSystems.privateListSystems.priceSystems.market.GegMarketPriceSys;

public class LagerCompMitGeld extends LagerComp {
    public final GeldComp geldComp;

    public LagerCompMitGeld(Entity entity, SysLager sysLager, GegMarketPriceSys gegMarketPriceSystem,
                            EigentumComp eigentumComp, BewegungszielComp bewegungszielComp,
                            GegenstaendeComp gegenstaendeComp, GeldComp geldComp) {
        super(entity, sysLager, gegMarketPriceSystem, eigentumComp, bewegungszielComp, gegenstaendeComp,
                geldComp);
        this.geldComp = geldComp;
    }
}
