package de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.listSystems.privateListSystems.priceSystems.market;

import de.arnonym.strategiespiel.spielSchirm.balancing.IdsGegenstaende;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.lager.LagerComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.lager.LagerCompMitGeld;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.endstufe.GegenstandList;
import de.arnonym.strategiespiel.framework.werkzeuge.collections.hashMaps.IntIntHashMap;
import de.arnonym.strategiespiel.spielSchirm.info.muster.GegInfo;

public class GegMarketPriceSys extends MarketPriceSys<LagerComp> {
    public GegMarketPriceSys() {
        super(marktpreiseHashMap());
    }

    private static IntIntHashMap marktpreiseHashMap() {
        IntIntHashMap marktpreiseHashMap = new IntIntHashMap();
        for (GegInfo gegMuster : IdsGegenstaende.get()) {
            marktpreiseHashMap.put(gegMuster.id, gegMuster.wert_create);
        }
        return marktpreiseHashMap;
    }

    @Override
    public int gettNeuerWert_nl(int id) {
        int neuerWertSumme = 0;
        int count = 0;
        for (LagerComp lagerComp : compSet) {
            GegenstandList gl = lagerComp.gegenstaendeComp.gegenstandListe;
            if (lagerComp instanceof LagerCompMitGeld && gl.contains(id)) {
                neuerWertSumme += gl.gettDurchId(id).getWert(); // todo nicht contaisn und dnach get verwenden!! Ineffizient. Stattdessen auf null pruefen
                count++;
            }
        }
        if (count == 0) {
            return -1;
        }
        return neuerWertSumme / count;
    }
}
