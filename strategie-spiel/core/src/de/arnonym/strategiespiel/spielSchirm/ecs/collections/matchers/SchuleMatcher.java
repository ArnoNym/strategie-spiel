package de.arnonym.strategiespiel.spielSchirm.ecs.collections.matchers;

import java.util.Set;

import de.arnonym.strategiespiel.spielSchirm.Dorf;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.departments.bildung.BildungAssignment;
import de.arnonym.strategiespiel.spielSchirm.balancing.IdsBildung;
import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenBalance;
import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenUpdatePausenDauer;
import de.arnonym.strategiespiel.spielSchirm.forschungUndWissen.schule.AiSchueler;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.departments.bildung.TeachDepartmentComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.EinkommenComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.werte.HabitalwerteComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.werte.ProduktivitaetComp;
import de.arnonym.strategiespiel.framework.ecs.collections.Matcher;

public class SchuleMatcher extends Matcher<AiSchueler, TeachDepartmentComp> {
    private final Dorf dorf;

    public SchuleMatcher(Dorf dorf) {
        super(AiSchueler.class, TeachDepartmentComp.class,
                KonstantenUpdatePausenDauer.SCHULE_MATCHER_UPDATE_PAUSE);
        this.dorf = dorf; // TODO loeschen wenn dorf geloescht
    }

    @Override
    protected TeachDepartmentComp findBestMatch(AiSchueler aiSchueler,
                                                Set<TeachDepartmentComp> schuleArbeitgeberComps) {
        // Alles nur Schaetzungen bzw Erwartungen!

        EinkommenComp einkommenComp = aiSchueler.einkommenComp;
        HabitalwerteComp habitalwerteComp = aiSchueler.habitalwerteComp;
        ProduktivitaetComp produktivitaetComp = aiSchueler.produktivitaetComp;
        float bildung = produktivitaetComp.bildungComp.getBildung();

        float restLeben_sek = habitalwerteComp.getGesundheit() * KonstantenBalance.SEKUNDEN_PRO_JAHR;

        int aktuellEinkommen = einkommenComp.getEinkommen();

        int neuEinkommen = dorf.employmentMatcher.schaetzeGehalt(
                produktivitaetComp.gettProduktivitaetKopf(bildung + 1),
                produktivitaetComp.getProduktivitaetKoerper(),
                aiSchueler.bewegungComp.positionComp.position);

        int mehrEinkommen = neuEinkommen - aktuellEinkommen;
        if (mehrEinkommen < 0) {
            return null;
        }
        int mehrLebenEinkommen = (int) (mehrEinkommen * restLeben_sek);

        float proEinBildungDauer = IdsBildung.gettBildungInfo(bildung).bildenAuftrag.arbeitszeit;
        int opportunitaetsKosten = (int) (aktuellEinkommen * proEinBildungDauer);
        if (mehrEinkommen < opportunitaetsKosten) {
            return null;
        }

        int minKosten = Integer.MAX_VALUE;
        TeachDepartmentComp minKostenSchule = null;
        for (TeachDepartmentComp schuleArbeitgeberComp : schuleArbeitgeberComps) {
            BildungAssignment bildungAssignment = schuleArbeitgeberComp.bildenInfo.bildenAuftrag;
            if (bildung > bildungAssignment.minBildung && bildung > bildungAssignment.maxBildung) {
                int studiengebuehren = 0; // todo

                // todo distanz zum Wohnort float distanz = GeometryUtils.hypothenuseIsoStrecke(getPositionMitte(), g.getPositionMitte());

                int kosten = studiengebuehren + opportunitaetsKosten;

                if (kosten < mehrLebenEinkommen && kosten < minKosten) {
                    minKosten = kosten;
                    minKostenSchule = schuleArbeitgeberComp;
                }
            }
        }
        return minKostenSchule;
    }

    @Override
    protected void couple(AiSchueler aiSchueler, TeachDepartmentComp schuleArbeitgeberComp) {
        // todo
    }

    @Override
    protected void decoupleAkteur(AiSchueler aiSchueler) {
        // todo
    }

    @Override
    protected void decoupleReakteur(TeachDepartmentComp schuleArbeitgeberComp) {
        // todo
    }

    /*private boolean sucheSchule() {
        GebBerufHerstSchule minDistanzSchule = null;
        float minDistanz = Float.MAX_VALUE;
        for (Gebaude g : dorf.getGebaudeListe()) {
            if (g instanceof GebBerufHerstSchule && ((GebBerufHerstSchule) g).issPassendeSchule(this)) {
                float distanz = GeometryUtils.hypothenuse(getPositionMitte(), g.getPositionMitte());
                if (distanz < minDistanz) {
                    minDistanz = distanz;
                    minDistanzSchule = (GebBerufHerstSchule) g;
                }
            }
        }
        gebBeruf = minDistanzSchule;
        if (minDistanzSchule == null) {
            return false;
        } else {
            minDistanzSchule.addNeuerSchueler(this);
            return true;
        }
    }*/
}
