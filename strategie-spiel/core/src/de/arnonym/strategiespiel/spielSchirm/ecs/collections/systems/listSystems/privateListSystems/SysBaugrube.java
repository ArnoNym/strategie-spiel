package de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.listSystems.privateListSystems;

import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.baugrube.BaugrubeComp;
import de.arnonym.strategiespiel.framework.ecs.collections.SetSystem;

public class SysBaugrube extends SetSystem<BaugrubeComp> {
    public SysBaugrube() {
        super();
    }

    @Override
    protected void update_or(float delta) {

    }
}
