package de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.rendern;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.sonstiges.PositionComp;
import de.arnonym.strategiespiel.framework.ecs.component.Comp;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.SysRender;

public abstract class RenderComp extends Comp {
    public final PositionComp positionComp;

    private SysRender.EnumZ z; // Siehe SysRender
    private boolean visible = false;

    public RenderComp(Entity entity, SysRender sysRendern, PositionComp positionComp,
                      SysRender.EnumZ z) {
        super(sysRendern, entity, positionComp);
        this.positionComp = positionComp;
        this.z = z;
    }

    public final void render(SpriteBatch spriteBatch, float delta) {
        if (!visible) return;
        render_or(spriteBatch, delta);
    }

    protected abstract void render_or(SpriteBatch spriteBatch, float delta);

    // PRUEFE /////////////////////////////////////////////////////////////////////////////////////

    public void preufeVisible(float kameraLeft, float kameraRight, float kameraBottom,
                              float kameraTop) {
        visible = preufeVisible_or(kameraLeft, kameraRight, kameraBottom, kameraTop);
    }

    protected abstract boolean preufeVisible_or(float kameraLeft, float kameraRight,
                                                float kameraBottom, float kameraTop);

    public void pruefeIndex() {
        deactivate();
        activate();
    }

    // KOMPLEXE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public void settZ(SysRender.EnumZ z) {
        deactivate();
        this.z = z;
        activate();
    }

    // PROTECTED GETTER UND SETTER ////////////////////////////////////////////////////////////////

    // EINFACHE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public boolean isVisible() {
        return visible;
    }

    public PositionComp getPositionComp() {
        return positionComp;
    }

    public SysRender.EnumZ getZ() {
        return z;
    }
}
