package de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.baugrube;

import de.arnonym.strategiespiel.framework.ecs.component.Comp;
import de.arnonym.strategiespiel.spielSchirm.arbeit.TimeClock;
import de.arnonym.strategiespiel.spielSchirm.arbeit.auftrag.AssignmentMaterial;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.bewegung.BewegungszielComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.gegenstaende.GegenstaendeComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.GeldComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.herrschaft.EigentuemerComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.herrschaft.EigentumComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.rendern.HashMapRenderComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.transformieren.TransComp;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.listSystems.privateListSystems.SysBaugrube;

public class BaugrubeComp extends Comp {
    public final BewegungszielComp bewegungszielComp;
    public final EigentumComp eigentumComp;
    public final GegenstaendeComp gegenstaendeComp;
    public final GeldComp geldComp;
    public final TransComp transComp;
    public final AssignmentMaterial musterAuftragMaterial;

    public final TimeClock timeClock;

    public BaugrubeComp(Entity entity, SysBaugrube sysBaugrube,
                        BewegungszielComp bewegungszielComp, GegenstaendeComp gegenstaendeComp,
                        EigentuemerComp eigentuemerComp,
                        HashMapRenderComp hashMapRenderComp, TransComp transComp,
                        AssignmentMaterial musterAuftragMaterial) {
        super(sysBaugrube, entity, bewegungszielComp, gegenstaendeComp,
                eigentuemerComp, hashMapRenderComp, transComp);
        this.bewegungszielComp = bewegungszielComp;
        this.gegenstaendeComp = gegenstaendeComp;
        this.eigentumComp = eigentuemerComp;
        this.geldComp = eigentuemerComp.geldComp;
        this.transComp = transComp;
        this.musterAuftragMaterial = musterAuftragMaterial;

        this.gegenstaendeComp.settMaxMengen(musterAuftragMaterial, true);

        this.timeClock = new TimeClock(musterAuftragMaterial.arbeitszeit);
    }
}
