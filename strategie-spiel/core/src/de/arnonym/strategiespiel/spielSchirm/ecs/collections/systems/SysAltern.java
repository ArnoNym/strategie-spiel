package de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import de.arnonym.strategiespiel.spielSchirm.balancing.Konstanten;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.alter.AlterComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.alter.EndlosAlterComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.alter.TransAlterComp;
import de.arnonym.strategiespiel.framework.ecs.collections.System;
import de.arnonym.strategiespiel.framework.werkzeuge.collections.UpdateSet;

public class SysAltern extends System<AlterComp> {
    private final List<EndlosAlterComp> endlosList = new ArrayList<>();
    private final List<TransAlterComp> exaktList = new ArrayList<>();
    private final UpdateSet<TransAlterComp> updateSet;

    public SysAltern() {
        updateSet = new UpdateSet<>(Konstanten.UPDATE_LIST_SIZE, true, TransAlterComp::update);
    }

    @Override
    protected void update_or(float delta) {
        //java.lang.IdentityConnectable.out.println("SysAltern ::: exaktList.size: "+exaktList.size());
        //java.lang.IdentityConnectable.out.println("SysAltern ::: endlosList.size: "+endlosList.size());
        //java.lang.IdentityConnectable.out.println("SysAltern ::: updateSet.toString: "+updateSet.toString());

        int i = exaktList.size() - 1;
        while (i >= 0) {
            exaktList.get(i).update(delta);
            i--;
        }

        for (EndlosAlterComp compAlterEndlos : endlosList) {
            compAlterEndlos.update(delta);
        }

        updateSet.update(delta);
    }

    @Override
    public boolean connect_or(AlterComp alterComp) {
        //java.lang.IdentityConnectable.out.println("SysAltern ::: setObjectInfo gecallt!"+updateSet.size());
        if (alterComp instanceof EndlosAlterComp) {
            return endlosList.add((EndlosAlterComp) alterComp);
        } else if (alterComp instanceof TransAlterComp) {
            TransAlterComp transAlterComp = ((TransAlterComp) alterComp);
            if (transAlterComp.exakt) {
                return exaktList.add(transAlterComp);
            } else {
                return updateSet.add(transAlterComp);
            }
        } else {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public boolean disconnect_or(AlterComp alterComp) {
        if (alterComp instanceof EndlosAlterComp) {
            return endlosList.remove(alterComp);
        } else if (alterComp instanceof TransAlterComp) {
            TransAlterComp transAlterComp = ((TransAlterComp) alterComp);
            if (transAlterComp.exakt) {
                return exaktList.remove(transAlterComp);
            } else {
                return updateSet.remove(transAlterComp);
            }
        } else {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public boolean contains(AlterComp alterComp) {
        if (alterComp instanceof EndlosAlterComp) {
            return endlosList.contains(alterComp);
        } else if (alterComp instanceof TransAlterComp) {
            TransAlterComp transAlterComp = ((TransAlterComp) alterComp);
            if (transAlterComp.exakt) {
                return exaktList.contains(transAlterComp);
            } else {
                return updateSet.contains(transAlterComp);
            }
        } else {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public <COLL extends Collection<AlterComp>> COLL toCollection(COLL coll) {
        coll.addAll(endlosList);
        coll.addAll(exaktList);
        coll.addAll(updateSet);
        return coll;
    }
}
