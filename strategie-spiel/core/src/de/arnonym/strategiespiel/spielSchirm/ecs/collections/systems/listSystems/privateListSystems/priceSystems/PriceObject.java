package de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.listSystems.privateListSystems.priceSystems;

public interface PriceObject {
    int id();
    int gettVerfuegbar();
    int gettNichtVerfuegbar();
    int gettPrice();
    void settPrice(int wert);
}
