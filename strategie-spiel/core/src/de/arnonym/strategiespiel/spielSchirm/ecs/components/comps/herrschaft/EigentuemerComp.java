package de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.herrschaft;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import de.arnonym.strategiespiel.framework.ecs.component.Comp;
import de.arnonym.strategiespiel.framework.werkzeuge.collections.Collector;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.GeldComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.OriginGeldComp;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.VariabelGeldComp;

public class EigentuemerComp extends EigentumComp {
    public final GeldComp geldComp;

    private final List<EigentumComp> eigentum = new ArrayList<>(); /** Achtung! Eigentum ist
     wiederrum Eigentuemer von Eigentum */

    public EigentuemerComp(Entity entity, OriginGeldComp originGeldComp) {
        super(entity, originGeldComp);
        this.geldComp = originGeldComp;
    }

    public EigentuemerComp(EigentuemerComp eigentuemer, Entity entity, VariabelGeldComp variabelGeldComp) {
        super(eigentuemer, entity, variabelGeldComp);
        variabelGeldComp.settGeld(eigentuemer.geldComp.getGeld());
        this.geldComp = variabelGeldComp;
    }

    public void addEigentum(EigentumComp ce) {
        this.eigentum.add(ce);
    }

    public void removeEigentum(EigentumComp eigentum) {
        this.eigentum.remove(eigentum);
    }

    public boolean contains(EigentumComp eigentumComp) {
        return eigentum().contains(eigentumComp);
    }

    public Set<EigentumComp> eigentum() {
        Set<EigentumComp> eigentum = new HashSet<>(this.eigentum);
        for (EigentumComp eigentumComp : this.eigentum) {
            if (eigentumComp instanceof EigentuemerComp) {
                eigentum.addAll(((EigentuemerComp) eigentumComp).eigentum());
            }
        }
        return eigentum;
    }

    // EINFACHE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public List<EigentumComp> gettEigentum() {
        return Collections.unmodifiableList(eigentum);
    }
}
