package de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.bewegung;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import de.arnonym.strategiespiel.framework.ecs.component.Comp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.kollision.LaufenKollisionComp;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;

public class BewegungszielComp extends Comp {
    public final LaufenKollisionComp compKollisionLaufen;
    public final boolean betretbar;

    private final List<BewegungComp> beherbergtList = new ArrayList<>();

    private boolean keinPfad;

    public BewegungszielComp(Entity entity, LaufenKollisionComp compKollisionLaufen,
                             boolean betretbar) {
        super(entity, compKollisionLaufen);
        this.compKollisionLaufen = compKollisionLaufen;
        this.betretbar = betretbar;
    }

    // IMMITIERE LISTE ////////////////////////////////////////////////////////////////////////////

    public void add(BewegungComp bewegungComp) {
        beherbergtList.add(bewegungComp);
    }

    public void remove(BewegungComp bewegungComp) {
        if (!beherbergtList.remove(bewegungComp)) {
            throw new IllegalArgumentException();
        }
    }

    // KOMPLEXE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public int gettNumberContainsEntities() {
        return beherbergtList.size();
    }

    public List<Entity> gettContainsEntitiesList() {
        List<Entity> entities = new ArrayList<>(beherbergtList.size());
        for (BewegungComp bewegungComp : beherbergtList) {
            entities.add(bewegungComp.entity);
        }
        return Collections.unmodifiableList(entities);
    }

    // EINFACHE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public boolean isKeinPfad() {
        return keinPfad;
    }

    public void setKeinPfad(boolean keinPfad) {
        this.keinPfad = keinPfad;
    }
}
