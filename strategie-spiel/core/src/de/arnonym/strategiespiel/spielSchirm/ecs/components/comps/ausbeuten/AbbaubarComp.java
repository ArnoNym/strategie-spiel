package de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.ausbeuten;

import de.arnonym.strategiespiel.framework.ecs.component.Comp;
import de.arnonym.strategiespiel.spielSchirm.arbeit.auftrag.Assignment;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.bewegung.BewegungszielComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.gegenstaende.GegenstaendeComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.herrschaft.EigentumComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.rendern.RenderComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.transformieren.TransComp;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.listSystems.privateListSystems.SysAbbaubar;

public class AbbaubarComp extends AusbeutbarComp {
    public final TransComp transComp;

    public AbbaubarComp(Entity entity, SysAbbaubar sysAbbaubar, BewegungszielComp bewegungszielComp,
                        EigentumComp eigentumComp, GegenstaendeComp gegenstaendeComp,
                        RenderComp renderComp, Assignment assignment) {
        super(entity, sysAbbaubar, bewegungszielComp, eigentumComp, gegenstaendeComp, renderComp,
                new Comp[]{}, assignment);
        this.transComp = null;
    }
}
