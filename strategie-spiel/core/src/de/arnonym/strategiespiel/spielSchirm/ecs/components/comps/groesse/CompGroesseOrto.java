package de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.groesse;

import de.arnonym.strategiespiel.framework.ecs.entity.Entity;

public class CompGroesseOrto extends CompGroesse {
    private float width;
    private float height;

    public CompGroesseOrto(Entity entity, float width, float height) {
        super(entity);
        this.width = width;
        this.height = height;
    }

    public float getWidth() {
        return width;
    }

    public float getHeight() {
        return height;
    }

    public void setWidth(float width) {
        this.width = width;
    }

    public void setHeight(float height) {
        this.height = height;
    }
}
