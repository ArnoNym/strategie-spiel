package de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.rendern;

import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.sonstiges.PositionComp;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.SysRender;
import de.arnonym.strategiespiel.framework.werkzeuge.textures.TextureHashMap;

public class BeweglichRenderComp extends HashMapRenderComp {
    private final SysRender sysRender;

    private boolean inBewegungsziel = false;

    public BeweglichRenderComp(Entity entity, SysRender sysRendern, PositionComp positionComp,
                               TextureHashMap textureHashMap, SysRender.EnumZ z,
                               boolean pruefeRenderIndex) {
        super(entity, sysRendern, positionComp, textureHashMap, z, pruefeRenderIndex);
        this.sysRender = sysRendern;
    }

    @Override
    protected boolean preufeVisible_or(float kameraLeft, float kameraRight, float kameraTop,
                                       float kameraBottom) {
        return !inBewegungsziel && super.preufeVisible_or(kameraLeft, kameraRight, kameraTop,
                kameraBottom);
    }

    public void preufeVisible() { // Wenn sich die Entity bewegt hat
        if (inBewegungsziel) {
            return;
        }
        sysRender.preufeVisible(this);
    }

    public void setInBewegungsziel(boolean inBewegungsziel) {
        this.inBewegungsziel = inBewegungsziel;
    }

    public boolean isInBewegungsziel() {
        return inBewegungsziel;
    }
}