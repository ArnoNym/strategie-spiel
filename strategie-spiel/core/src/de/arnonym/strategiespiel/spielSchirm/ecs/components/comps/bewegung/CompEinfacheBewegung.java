package de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.bewegung;

import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.arnonym.strategiespiel.framework.ecs.component.Comp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.sonstiges.PositionComp;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.listSystems.privateListSystems.SysBewegung;

public class CompEinfacheBewegung extends Comp {
    // TODO Ueberdenken. gefaellt mir so nicht
    /* Starre Bewegung die extern diktiert und dann in jedem Fall ausgefuehrt wird. ZB bei
    schwebenden Symbolen. Dorfbewohner sollten durch die Ai bewegt werden */

    private final PositionComp positionComp;
    private final HashMap<Bewegung, Float> bewegungDauerHashMap = new HashMap<>();
    private final List<Comp> unerreichbarList = new ArrayList<>(); // TODO Ueberflussig, da es nicht fuer Dorfbewohner sondenr nur Statische Smybole usw verwendet wird

    public CompEinfacheBewegung(Entity entity, SysBewegung sysBewegung,
                                PositionComp positionComp) {
        super(sysBewegung, entity, positionComp);
        this.positionComp = positionComp;
    }

    public void update(float delta) {
        List<Bewegung> removeList = new ArrayList<>();
        for (Map.Entry<Bewegung, Float> entry : bewegungDauerHashMap.entrySet()) {
            Bewegung bewegung = entry.getKey();
            Vector2 direction = bewegung.direction;
            float restDauer = entry.getValue();
            float speed = bewegung.speed * Math.min(delta, restDauer);

            Vector2 position = positionComp.getPosition();
            positionComp.settPosition(position.x + direction.x * speed,
                    position.y + direction.y * speed);

            float neueRestDauer = restDauer - delta;
            if (neueRestDauer > 0) {
                entry.setValue(neueRestDauer);
            } else {
                removeList.add(bewegung);
            }
        }

        for (Bewegung b : removeList) {
            bewegungDauerHashMap.remove(b);
        }
    }

    public void add(Vector2 direction, float speed, float dauer_sekunden) {
        bewegungDauerHashMap.put(new Bewegung(direction, speed), dauer_sekunden);
    }

    private class Bewegung {
        final Vector2 direction;
        final float speed;

        private Bewegung(Vector2 direction, float speed) { // TODO Grad-Zahl als Input verwenden
            // Hypothenuse auf 1 normen
            float ankathete = direction.x;
            float gegenkathete = direction.y;
            boolean xPositiv = ankathete >= 0;
            direction.x = (float) (Math.cos(Math.atan(gegenkathete / ankathete))
                    * (xPositiv ? 1 : (-1)));
            direction.y = (float) (Math.sin(Math.atan(gegenkathete / ankathete))
                    * (xPositiv ? 1 : (-1)));
            this.direction = direction;

            this.speed = speed; // TODO Geschwindigkeit unitEnum nach Winkel anpassen
        }
    }

    // ERREICHBARKEIT /////////////////////////////////////////////////////////////////////////////

    public boolean issErreichbar(Comp c) {
        if (unerreichbarList.contains(c)) {
            return false;
        }

        // TODO Step Logik pruefne ob erreichbar

        // TODO Add zu unerreichbarList wenn nicht

        return true;
    }
}
