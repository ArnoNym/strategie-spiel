package de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.listSystems.privateListSystems;

import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.bewegung.CompEinfacheBewegung;
import de.arnonym.strategiespiel.framework.ecs.collections.SetSystem;

public class SysBewegung extends SetSystem<CompEinfacheBewegung> {
    public SysBewegung() {

    }

    @Override
    protected void update_or(float delta) {
        for (CompEinfacheBewegung c : compSet) {
            c.update(delta);
        }
    }
}
