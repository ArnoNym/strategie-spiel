package de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.wohnen;

import java.util.List;
import java.util.Set;

import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenUpdatePausenDauer;
import de.arnonym.strategiespiel.framework.ecs.collections.Matcher;

public class WohnenMatcher extends Matcher<MieterComp, WohnortComp> {
    public WohnenMatcher() {
        super(MieterComp.class, WohnortComp.class,
                KonstantenUpdatePausenDauer.WOHNORT_MATCHER_UPDATE_PAUSE);
    }

    @Override
    protected WohnortComp findBestMatch(MieterComp mieterComp, Set<WohnortComp> wohnortComps) {
        float nutzenExponent = mieterComp.nutzenExponent();

        int maxNutzen = 0;
        WohnortComp maxNutzenWohnort = null;
        for (WohnortComp wohnortComp : wohnortComps) {
            final int bruttoNutzen = wohnortComp.bruttoNutzen(nutzenExponent, mieterComp.einwohner());
            final int nutzen = bruttoNutzen - wohnortComp.gettPrice();

            if (nutzen > maxNutzen) {
                maxNutzen = nutzen;
                maxNutzenWohnort = wohnortComp;
            }
        }

        return maxNutzenWohnort;
    }

    @Override
    protected void couple(MieterComp mieterComp, WohnortComp wohnortComp) {
        mieterComp.setWohnort(wohnortComp);
        wohnortComp.addMieter(mieterComp);
    }

    @Override
    protected void decoupleAkteur(MieterComp mieterComp) {
        mieterComp.getWohnort().removeMieter(mieterComp);
        mieterComp.setWohnort(null);
    }

    @Override
    protected void decoupleReakteur(WohnortComp wohnortComp) {
        for (MieterComp mieterComp : wohnortComp.mieterList()) {
            mieterComp.setWohnort(null);
        }
        wohnortComp.removeAllMieter();
    }
}
