package de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.gegenstaende;

import de.arnonym.strategiespiel.spielSchirm.umwelt.CompSterben;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.rendern.HashMapRenderComp;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.endstufe.Gegenstand;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.vorstufe.EntityGegInfoList;

public class GegenstaendeComp3 extends GegenstaendeComp2 {
    public final CompSterben compSterben;

    public GegenstaendeComp3(Entity entity, HashMapRenderComp hashMapRenderComp,
                             CompSterben compSterben, EntityGegInfoList entityGegInfoListe) {
        super(entity, hashMapRenderComp, entityGegInfoListe);
        this.compSterben = compSterben;
    }

    @Override
    public void veraenderungMengeVerfuegbar(Gegenstand gegenstand, int veraendertUmMenge) {
        if (veraendertUmMenge <= 0) {
            compSterben.sterbenBedingungErfuellt();
        }

        super.veraenderungMengeVerfuegbar(gegenstand, veraendertUmMenge);
    }
}
