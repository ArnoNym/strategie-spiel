package de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.kollision;

import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.groesse.IsoGroesseComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.sonstiges.PositionComp;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.SysKollision;

public class LaufenKollisionComp extends KollisionComp {
    private float laufenMult; // 0 == unpassierbar, 1 == normal, >1 == schneller

    public LaufenKollisionComp(Entity entity, SysKollision sysKollision,
                               IsoGroesseComp isoGroesseComp, PositionComp positionComp,
                               float laufenMult) {
        super(entity, sysKollision, isoGroesseComp, positionComp);
        this.laufenMult = laufenMult;
    }

    public boolean issKollision() {
        return laufenMult == 0;
    }

    public float getLaufenMult() {
        return laufenMult;
    }

    public void setLaufenMult(float laufenMult) {
        this.laufenMult = laufenMult;
    }
}
