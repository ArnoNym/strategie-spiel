package de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.ausbeuten;

import java.util.ArrayList;
import java.util.List;

import de.arnonym.strategiespiel.framework.ecs.component.Comp;
import de.arnonym.strategiespiel.spielSchirm.arbeit.auftrag.Assignment;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.bewegung.BewegungszielComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.herrschaft.EigentumComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.kollision.KollisionComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.gegenstaende.GegenstaendeComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.rendern.RenderComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.ressourcen.PflegenComp;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.spielSchirm.umwelt.ressourcen.SysFreuchte;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.endstufe.Gegenstand;

public class FruechteComp extends AusbeutbarComp {
    public final KollisionComp kollisionComp;
    public final PflegenComp pflegenComp;
    public final Assignment assignmentErnten;
    public final float wachsenMod;

    public FruechteComp(Entity entity,

                        SysFreuchte sysFreuchte,

                        BewegungszielComp bewegungszielComp, EigentumComp eigentumComp,
                        GegenstaendeComp gegenstaendeComp, RenderComp renderComp,
                        KollisionComp kollisionComp, PflegenComp pflegenComp,

                        Assignment assignmentErnten, float wachsenMod) {
        super(entity, sysFreuchte, bewegungszielComp, eigentumComp, gegenstaendeComp, renderComp,
                new Comp[]{kollisionComp, pflegenComp}, assignmentErnten);
        this.kollisionComp = kollisionComp;
        this.pflegenComp = pflegenComp;
        this.assignmentErnten = assignmentErnten;
        this.wachsenMod = wachsenMod;

    }

    public List<Gegenstand> gettFreuchte() {
        List<Gegenstand> freuchte = new ArrayList<>();
        for (Gegenstand g : gegenstaendeComp.gegenstandListe) {
            if (g.gegMuster.isNachwachsbar()) {
                freuchte.add(g);
            }
        }
        if (freuchte.size() == 0) throw new IllegalArgumentException("gegListe.isEmpty() == "
                + gegenstaendeComp.gegenstandListe.isEmpty());
        return freuchte;
    }

    public boolean issMengeFreuchte(boolean verfuegbar, boolean legen, boolean nehmen) {
        for (Gegenstand g : gegenstaendeComp.gegenstandListe) {
            if (g.gegMuster.isNachwachsbar() && g.gettMenge(verfuegbar, legen, nehmen) > 0) {
                return true;
            }
        }
        return false;
    }
}
