package de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.wohnen;

import java.util.ArrayList;
import java.util.List;

import de.arnonym.strategiespiel.framework.werkzeuge.numbers.unit.ZeitEnum;
import de.arnonym.strategiespiel.spielSchirm.balancing.Konstanten;
import de.arnonym.strategiespiel.framework.ecs.collections.SetSystem;
import de.arnonym.strategiespiel.framework.ecs.component.Comp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.GeldComp;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.framework.werkzeuge.numbers.unit.Unit;

public abstract class MieterComp extends Comp {
    public final GeldComp geldComp;

    private final List<EinwohnerComp> einwohnerList = new ArrayList<>(); // Kann nicht platzEmpty sein
    private WohnortComp wohnort;

    public MieterComp(Entity entity, WohnenMatcher wohnenMatcher, GeldComp geldComp) {
        super(wohnenMatcher, entity, geldComp);
        this.geldComp = geldComp;
    }

    public void addEinwohner(EinwohnerComp einwohner) {
        einwohnerList.add(einwohner);
    }

    public boolean removeEinwohner(EinwohnerComp einwohner) {
        boolean removed = einwohnerList.remove(einwohner);
        if (einwohnerList.isEmpty()) {
            throw new IllegalStateException("removed: "+removed);
        }
        return removed;
    }

    public void setWohnort(WohnortComp wohnort) {
        if (this.wohnort != null) {
            mieteZahlen();
            cummulatedDelta = cummulatedDelta.clean();
        }
        this.wohnort = wohnort;
    }

    private void mieteZahlen() {
        /*Pin zuZahlenGeld = cummulatedDelta.mult(wohnort.gettPrice()).pinValue();
        if (zuZahlenGeld.compareToZero() > 0) {
            geldComp.mengeVerringern(zuZahlenGeld);
        }*/
        int zuZahlenGeld = (int) (cummulatedDelta.mult(wohnort.gettPrice()).doubleValue()
                * Konstanten.NACHKOMMASTELLEN);
        if (geldComp.mengeVerringern(zuZahlenGeld, false)) {
            cummulatedDelta = cummulatedDelta.clean();
        }
    }

    // GETTER UND SETTER //////////////////////////////////////////////////////////////////////////

    public WohnortComp getWohnort() {
        return wohnort;
    }

    public boolean hasWohnort() {
        return wohnort != null;
    }

    public int einwohner() {
        return einwohnerList.size();
    }

    public abstract float nutzenExponent();

    ///////////////////////////////////////////////////////////////////////////////////////////////

    private Unit<ZeitEnum> cummulatedDelta = new Unit<>(0, ZeitEnum.REAL_SEKUNDEN);

    public class MieterSys extends SetSystem<MieterComp> {
        @Override
        protected void update_or(float delta) {
            cummulatedDelta = cummulatedDelta.add(delta);
            mieteZahlen();
        }
    }
}
