package de.arnonym.strategiespiel.spielSchirm.ecs.collections.lists.ausbeuten;

import de.arnonym.strategiespiel.framework.listener.DeletableStoryListener;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.ausbeuten.AusbeutbarComp;
import de.arnonym.strategiespiel.framework.ecs.collections.EcsList;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.herrschaft.EigentuemerComp;

public class AuszubeutenEcsList<C extends AusbeutbarComp> extends EcsList<C> {
    public final EigentuemerComp eigentumComp;

    public AuszubeutenEcsList(DeletableStoryListener parent, EigentuemerComp eigentumComp) {
        super(parent);
        this.eigentumComp = eigentumComp;
    }
}
