package de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.listSystems.privateListSystems.priceSystems.shop;

import java.util.Collection;

import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenBalance;
import de.arnonym.strategiespiel.framework.ecs.collections.SetSystem;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.listSystems.privateListSystems.priceSystems.PriceObject;
import de.arnonym.strategiespiel.framework.ecs.component.Comp;
import de.arnonym.strategiespiel.spielSchirm.Enums;
import de.arnonym.strategiespiel.framework.werkzeuge.pause.DeltaPause;

public abstract class ShopPriceSys<C extends Comp, P extends PriceObject> extends SetSystem<C> {
    private final DeltaPause pause = new DeltaPause(
            KonstantenBalance.PREISE_VERAENDERUNG_PAUSE_DAUER_sek, Enums.EnumPauseStart.JETZT);

    @Override
    protected void update_or(float delta) {
        if (!pause.issVorbei(delta, true)) return;

        for (P p : priceObjects()) {
            int alterWert = p.gettPrice();
            int neuerWert = (int) (alterWert * gettPreisaenderung(p));
            p.settPrice(neuerWert);
        }
    }

    protected abstract Collection<P> priceObjects();

    private float gettPreisaenderung(P p) {
        return 1.10f /*TODO Konstanten wie viel uebershcuss ist normal */ * - (float) p.gettVerfuegbar() / p.gettNichtVerfuegbar();
    }
}
