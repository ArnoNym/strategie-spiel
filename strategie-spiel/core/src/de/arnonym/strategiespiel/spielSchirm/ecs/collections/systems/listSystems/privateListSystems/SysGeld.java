package de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.listSystems.privateListSystems;

import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.OriginGeldComp;
import de.arnonym.strategiespiel.framework.ecs.collections.SetSystem;

public class SysGeld extends SetSystem<OriginGeldComp> {
    public SysGeld() {

    }

    @Override
    protected void update_or(float delta) {
        for (OriginGeldComp compGeldOrigin : compSet) {
            compGeldOrigin.geld.geldManager.update(delta);
        }
    }
}
