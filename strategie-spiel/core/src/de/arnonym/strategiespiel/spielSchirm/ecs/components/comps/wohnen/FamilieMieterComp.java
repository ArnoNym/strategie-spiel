package de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.wohnen;

import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.GeldComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dorfbewohner.familie.Familie;

public class FamilieMieterComp extends MieterComp {
    private final Familie familie;

    public FamilieMieterComp(Familie familie, WohnenMatcher wohnenMatcher, GeldComp geldComp) {
        super(familie, wohnenMatcher, geldComp);
        this.familie = familie;
    }

    @Override
    public float nutzenExponent() {
        return familie.gettMieteNutzenExponent();
    }
}
