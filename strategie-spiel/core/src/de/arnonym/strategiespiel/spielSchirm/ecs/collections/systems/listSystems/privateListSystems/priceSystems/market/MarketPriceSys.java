package de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.listSystems.privateListSystems.priceSystems.market;

import de.arnonym.strategiespiel.spielSchirm.balancing.IdsGegenstaende;
import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenBalance;
import de.arnonym.strategiespiel.framework.ecs.collections.SetSystem;
import de.arnonym.strategiespiel.framework.ecs.component.Comp;
import de.arnonym.strategiespiel.spielSchirm.Enums;
import de.arnonym.strategiespiel.framework.werkzeuge.collections.hashMaps.IntIntHashMap;
import de.arnonym.strategiespiel.spielSchirm.info.muster.GegInfo;
import de.arnonym.strategiespiel.framework.werkzeuge.pause.DeltaPause;

public abstract class MarketPriceSys<C extends Comp> extends SetSystem<C> {
    private final IntIntHashMap marktpreiseHashMap;
    private final DeltaPause pause = new DeltaPause(
            KonstantenBalance.PREISE_VERAENDERUNG_PAUSE_DAUER_sek, Enums.EnumPauseStart.JETZT);

    public MarketPriceSys(IntIntHashMap marktpreiseHashMap) {
        this.marktpreiseHashMap = marktpreiseHashMap;
    }

    @Override
    protected void update_or(float delta) {
        if (!pause.issVorbei(delta, true)) return;

        for (GegInfo gegInfo : IdsGegenstaende.get()) {
            int id = gegInfo.gettId();
            int neuerWert = gettNeuerWert_nl(id);
            if (neuerWert != -1) {
                int alterWert = marktpreiseHashMap.get(id);
                marktpreiseHashMap.put(id,
                        (int) (alterWert * (1 - KonstantenBalance.PREISE_VERAENDERUNG_prozent)
                                + neuerWert * KonstantenBalance.PREISE_VERAENDERUNG_prozent));
            }
        }
    }

    public abstract int gettNeuerWert_nl(int id);

    public int gettMarketPrice(int id) {
        return marktpreiseHashMap.get(id);
    }
}
