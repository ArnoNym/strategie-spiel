package de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.gegenstaende;

import java.util.Map;

import de.arnonym.strategiespiel.spielSchirm.balancing.IdsGegenstaende;
import de.arnonym.strategiespiel.spielSchirm.arbeit.auftrag.AssignmentMaterial;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.rendern.HashMapRenderComp;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.endstufe.Gegenstand;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.endstufe.GegenstandList;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.vorstufe.EntityGegInfoList;
import de.arnonym.strategiespiel.framework.werkzeuge.collections.hashMaps.IntIntHashMap;
import de.arnonym.strategiespiel.spielSchirm.Enums;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.MathUtils;

public abstract class GegenstaendeComp extends BasisGegComp<GegenstandList> {
    public final HashMapRenderComp hashMapRenderComp;
    public final GegenstandList gegenstandListe;

    public GegenstaendeComp(Entity entity, HashMapRenderComp hashMapRenderComp,
                            EntityGegInfoList entityGegInfoListe) {
        super(entity, hashMapRenderComp);
        this.hashMapRenderComp = hashMapRenderComp;
        this.gegenstandListe = new GegenstandList(entityGegInfoListe) {
            @Override
            public void veraenderungMengeVerfuegbar(Gegenstand gegenstand, int veraendertUmMenge) {
                GegenstaendeComp.this.veraenderungMengeVerfuegbar(gegenstand, veraendertUmMenge);

                if (veraendertUmMenge > 0) {
                    hashMapRenderComp.add(Enums.EnumAktion.NEHMEN);
                } else {
                    hashMapRenderComp.add(Enums.EnumAktion.WEGLEGEN);
                }
            }
        };
    }

    public void veraenderungMengeVerfuegbar(Gegenstand gegenstand, int veraendertUmMenge) {
        float verbrauchterPlatzProzent = 1 - (float) gegenstandListe.getPlatz()
                / gegenstandListe.getMaxPlatz();
        if (verbrauchterPlatzProzent > 1) {
            throw new IllegalArgumentException(""+gegenstandListe.getPlatz()+", "
                    +gegenstandListe.getMaxPlatz());
        }
        actOnPlatz(verbrauchterPlatzProzent);
    }

    protected void actOnPlatz(float usedSpace_percent) {
        if (usedSpace_percent > 1) {
            throw new IllegalArgumentException(""+usedSpace_percent);
        } else if (usedSpace_percent == 1) {
            platzFull();
        } else if (usedSpace_percent > 0.5) {
            platzMedium();
        } else if (usedSpace_percent > 0) {
            platzLow();
        } else {
            platzEmpty();
        }
    }

    protected void platzFull() {
        hashMapRenderComp.add(Enums.EnumAktion.STANDARD_PRIMAERMENGE_VOLL);
    }

    protected void platzMedium() {
        hashMapRenderComp.add(Enums.EnumAktion.PRIMAERMENGE_MITTEL);
    }

    protected void platzLow() {
        hashMapRenderComp.add(Enums.EnumAktion.PRIMAERMENGE_WENIG);
    }

    protected abstract void platzEmpty();

    // MAXMENGEN //////////////////////////////////////////////////////////////////////////////////

    public void settMaxMengen(AssignmentMaterial musterAuftragMaterial,
                              boolean setMaxPlatzUrspruenglich) {
        int maxPlatz = 0;
        if (setMaxPlatzUrspruenglich) {
            for (Map.Entry<Integer, Integer> entry : musterAuftragMaterial.idMengeHashMap.entrySet()) {
                int id = entry.getKey();
                int menge = entry.getValue();

                gegenstandListe.gettDurchId(id).setMengeMaximal(menge);

                maxPlatz += MathUtils.myIntMult(IdsGegenstaende.get(id).platzVerbrauch, menge);
            }
        } else {
            // ACHTUNG! Nicht getestet!

            int inputPlusOutputPlatzProEinheit = 0;
            for (Map.Entry<Integer, Integer> entry : musterAuftragMaterial.idMengeHashMap.entrySet()) {
                int id = entry.getKey();
                int menge = entry.getValue();
                inputPlusOutputPlatzProEinheit += MathUtils.myIntMult(
                        IdsGegenstaende.get(id).platzVerbrauch,
                        menge);
            }

            int platzFuerEinheitenAnzahl = Math.max(1,
                    gegenstandListe.getMaxPlatzUrspruenglich() / inputPlusOutputPlatzProEinheit);

            IntIntHashMap idMengeHashMap = musterAuftragMaterial.idMengeHashMap;
            for (Gegenstand g : gegenstandListe) {
                int id = g.gegMuster.id;
                int menge = idMengeHashMap.getOrDefault(id, 0);
                if (menge > 0) {
                    menge = menge * platzFuerEinheitenAnzahl;
                    g.setMengeMaximal(menge);
                    maxPlatz += MathUtils.myIntMult(g.gegMuster.platzVerbrauch, menge);
                }
            }
        }
        gegenstandListe.settMaxPlatz(maxPlatz, setMaxPlatzUrspruenglich, false);
    }

    // EINFACHE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    @Override
    public GegenstandList getList() {
        return gegenstandListe;
    }
}