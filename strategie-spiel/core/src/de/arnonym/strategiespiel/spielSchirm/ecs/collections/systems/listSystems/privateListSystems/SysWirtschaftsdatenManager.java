package de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.listSystems.privateListSystems;

import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.gegenstaende.CompWirtschaftsdaten;
import de.arnonym.strategiespiel.framework.ecs.collections.SetSystem;

public class SysWirtschaftsdatenManager extends SetSystem<CompWirtschaftsdaten> {
    public SysWirtschaftsdatenManager() {

    }

    @Override
    protected void update_or(float delta) {
        for (CompWirtschaftsdaten cwd : compSet) {
            cwd.gegenstandManager.update(delta);
        }
    }
}
