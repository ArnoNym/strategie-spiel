package de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.ausbeuten;

import de.arnonym.strategiespiel.framework.ecs.component.Comp;
import de.arnonym.strategiespiel.spielSchirm.arbeit.auftrag.Assignment;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.bewegung.BewegungszielComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.gegenstaende.GegenstaendeComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.herrschaft.EigentumComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.rendern.RenderComp;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.framework.ecs.collections.System;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.CollectionUtils;

public abstract class AusbeutbarComp extends Comp {
    public final BewegungszielComp bewegungszielComp;
    public final EigentumComp eigentumComp;
    public final GegenstaendeComp gegenstaendeComp;
    public final RenderComp renderComp;
    private Assignment assignment;

    public AusbeutbarComp(Entity entity,

                          System<? extends AusbeutbarComp> ausbeutbarSystem,

                          BewegungszielComp bewegungszielComp, EigentumComp eigentumComp,
                          GegenstaendeComp gegenstaendeComp, RenderComp renderComp, Comp[] comps,

                          Assignment assignment) {
        super(ausbeutbarSystem, entity, CollectionUtils.combineComp(comps, bewegungszielComp,
                gegenstaendeComp, renderComp));
        this.bewegungszielComp = bewegungszielComp;
        this.eigentumComp = eigentumComp;
        this.gegenstaendeComp = gegenstaendeComp;
        this.renderComp = renderComp;
        this.assignment = assignment;
    }

    public Assignment getAssignment() {
        return assignment;
    }

    public void setAssignment(Assignment assignment) {
        this.assignment = assignment;
    }
}
