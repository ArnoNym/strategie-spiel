package de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.groesse;

import de.arnonym.strategiespiel.framework.ecs.component.Comp;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;

public abstract class CompGroesse extends Comp {

    public CompGroesse(Entity entity) {
        super(entity);
    }

    public abstract float getWidth();

    public abstract float getHeight();

}