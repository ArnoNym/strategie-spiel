package de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.alter;

import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.SysAltern;

public class EndlosAlterComp extends AlterComp {
    public EndlosAlterComp(Entity entity, SysAltern sysAltern, int alter_sekunden) {
        super(entity, sysAltern, null, alter_sekunden);
    }
}
