package de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.kollision;

import java.util.Collections;
import java.util.List;

import de.arnonym.strategiespiel.framework.ecs.component.Comp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.groesse.IsoGroesseComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.sonstiges.PositionComp;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.SysKollision;
import de.arnonym.strategiespiel.spielSchirm.worldMap.Zelle;

public abstract class KollisionComp extends Comp {
    public final Entity entity;
    public final IsoGroesseComp isoGroesseComp;
    public final PositionComp positionComp;

    private List<Zelle> betrifftZellenListe_nl;

    public KollisionComp(Entity entity, SysKollision sysKollision, IsoGroesseComp isoGroesseComp,
                         PositionComp positionComp) {
        super(sysKollision, entity, isoGroesseComp, positionComp);
        this.entity = entity;
        this.isoGroesseComp = isoGroesseComp;
        this.positionComp = positionComp;
    }

    public void setBetrifftZellenListe_nl(List<Zelle> betrifftZelleListe_nl) {
        this.betrifftZellenListe_nl = betrifftZelleListe_nl;
    }

    public List<Zelle> getBetrifftZellenListe_nl() {
        if (betrifftZellenListe_nl == null) {
            return null;
        }
        return Collections.unmodifiableList(betrifftZellenListe_nl);
    }
}
