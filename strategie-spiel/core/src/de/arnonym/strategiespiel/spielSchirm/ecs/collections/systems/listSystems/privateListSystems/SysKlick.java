package de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.listSystems.privateListSystems;

import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.math.Vector2;

import de.arnonym.strategiespiel.spielSchirm.World;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.klick.CompKlick;
import de.arnonym.strategiespiel.framework.ecs.collections.SetSystem;

public class SysKlick extends SetSystem<CompKlick> implements InputProcessor {
    private final World world; /* Level anstatt Viewports speichern um Problem mit speichern und
    laden zu umgehen */

    public SysKlick(World world) {
        this.world = world;
    }

    @Override
    protected void update_or(float delta) {
        throw new IllegalStateException("Dont call this!");
    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        Vector2 isoWorldKlick = world.getViewport().unproject(new Vector2(screenX, screenY));
        for (CompKlick compKlick : compSet) {
            if (compKlick.issHit(isoWorldKlick)) {
                compKlick.gotHit();
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
