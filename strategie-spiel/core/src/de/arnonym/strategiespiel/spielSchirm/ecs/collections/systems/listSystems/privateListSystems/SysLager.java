package de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.listSystems.privateListSystems;

import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.lager.LagerComp;
import de.arnonym.strategiespiel.framework.ecs.collections.SetSystem;

public class SysLager extends SetSystem<LagerComp> {
    public SysLager() {

    }

    @Override
    protected void update_or(float delta) {
        throw new IllegalStateException("Dont call this!");
    }
}
