package de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.wohnen;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import de.arnonym.strategiespiel.spielSchirm.balancing.Konstanten;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.listSystems.privateListSystems.WohnortSystem;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.listSystems.privateListSystems.priceSystems.PriceObject;
import de.arnonym.strategiespiel.framework.ecs.component.Comp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.bewegung.BewegungszielComp;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.spielSchirm.gebaude.haus.HausInfo;

public class WohnortComp extends Comp implements PriceObject {
    public final BewegungszielComp bewegungszielComp;
    public final HausInfo info;

    private final List<MieterComp> mieterList = new ArrayList<>();
    private int price_year;

    public WohnortComp(Entity entity, WohnortSystem wohnortSystem,
                       BewegungszielComp bewegungszielComp, HausInfo info) {
        super(wohnortSystem, entity, bewegungszielComp);
        this.bewegungszielComp = bewegungszielComp;
        this.info = info;
        this.price_year = info.wert;
    }

    // EIN UND AUSZiEHEN //////////////////////////////////////////////////////////////////////////

    protected void addMieter(MieterComp mieter) {
        mieterList.add(mieter);
    }

    protected void removeMieter(MieterComp mieter) {
        mieterList.remove(mieter);
    }

    protected void removeAllMieter() {
        mieterList.clear();
    }

    protected List<MieterComp> mieterList() {
        return Collections.unmodifiableList(mieterList);
    }

    // WERTE //////////////////////////////////////////////////////////////////////////////////////

    public int hypothetischerBruttoNutzen(MieterComp mieterComp) {
        return bruttoNutzen(mieterComp.nutzenExponent(), einwohner() + mieterComp.einwohner());
    }
    public int aktuellerBruttoNutzen(MieterComp mieterComp) {
        return bruttoNutzen(mieterComp.nutzenExponent(), einwohner());
    }
    protected int bruttoNutzen(float nutzenExponent, int anzahlBewohner) {
        float bruttoNutzen = (float) Math.pow((double) info.wert / Konstanten.NACHKOMMASTELLEN,
                nutzenExponent);
        if (anzahlBewohner > info.maxBewohner) {
            bruttoNutzen *= (float) info.maxBewohner / anzahlBewohner;
        }
        return (int) bruttoNutzen;
    }

    public float aktuelleProduktivitaet() {
        return produktivitaet(einwohner());
    }
    private float produktivitaet(int anzahlBewohner) {
        float produktivitaet = (float) info.wert / Konstanten.NACHKOMMASTELLEN / 10; //TODO Konstanten
        if (anzahlBewohner > info.maxBewohner) {
            produktivitaet *= (float) info.maxBewohner / anzahlBewohner;
        }
        //gettProduktivitaet *= (1 - 0.05 * anzahlKinder); //TODO Konstanten //TODO Soll ich das einfuegen?
        return Math.min(produktivitaet, 1);
    }

    public float aktuelleGesundheit() {
        return gesundheit(einwohner());
    }
    private float gesundheit(int anzahlBewohner) {
        float produktivitaet = (float) info.wert / Konstanten.NACHKOMMASTELLEN / 10; //TODO Konstanten
        if (anzahlBewohner > info.maxBewohner) {
            produktivitaet *= (float) info.maxBewohner / anzahlBewohner;
        }
        return Math.min(produktivitaet, 1);
    }

    // PRICE OBJECT ///////////////////////////////////////////////////////////////////////////////

    @Override
    public int id() {
        return info.id;
    }

    @Override
    public int gettVerfuegbar() {
        if (isEmpty()) {
            return info.maxBewohner;
        } else {
            return 0;
        }
    }

    @Override
    public int gettNichtVerfuegbar() {
        if (isEmpty()) {
            return 0;
        } else {
            return einwohner();
        }
    }

    @Override
    public int gettPrice() {
        return price_year;
    }

    @Override
    public void settPrice(int wert) {
        this.price_year = wert;
    }

    // GETTER UND SETTER //////////////////////////////////////////////////////////////////////////

    public int einwohner() {
        int e = 0;
        for (MieterComp mc : mieterList) {
            e += mc.einwohner();
        }
        return e;
    }

    public boolean isEmpty() {
        return !mieterList.isEmpty();
    }
}
