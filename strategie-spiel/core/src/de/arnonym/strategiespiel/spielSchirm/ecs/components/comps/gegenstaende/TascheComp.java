package de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.gegenstaende;

import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.taschen.MasterTasche;
import de.arnonym.strategiespiel.spielSchirm.Enums;

public class TascheComp extends BasisGegComp<MasterTasche> {
    private MasterTasche masterTasche;

    public TascheComp(Entity entity, MasterTasche masterTasche) {
        super(entity);
        this.masterTasche = masterTasche;
    }

    @Override
    public MasterTasche getList() {
        return getMasterTasche();
    }

    public MasterTasche getMasterTasche() {
        return masterTasche;
    }

    public void setMasterTasche(MasterTasche masterTasche) {
        support.firePropertyChange(Enums.PC_EVENT_NAME_MASTER_TASCHE,
                this.masterTasche, masterTasche);
        this.masterTasche = masterTasche;
    }
}