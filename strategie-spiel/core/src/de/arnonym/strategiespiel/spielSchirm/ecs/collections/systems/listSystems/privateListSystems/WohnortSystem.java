package de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.listSystems.privateListSystems;

import java.util.Collection;

import de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.listSystems.privateListSystems.priceSystems.shop.ShopPriceSys;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.wohnen.WohnortComp;

public class WohnortSystem extends ShopPriceSys<WohnortComp, WohnortComp> {
    @Override
    protected Collection<WohnortComp> priceObjects() {
        return compSet;
    }
}
