package de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.alter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import de.arnonym.strategiespiel.framework.ecs.component.Comp;
import de.arnonym.strategiespiel.framework.werkzeuge.collections.groups.TwoGroup;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.transformieren.TransComp;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.SysAltern;
import de.arnonym.strategiespiel.framework.ecs.collections.EcsList;

public class TransAlterComp extends AlterComp {
    public final boolean exakt; // Siehe SysAltern

    private final List<Float> alterList = new ArrayList<>();
    private final List<TransComp> transCompList = new ArrayList<>();

    public TransAlterComp(Entity entity, SysAltern sysAltern,

                          boolean exakt, float alter_sek,

                          TransComp transComp, float transAlter_sek) {
        this(entity, sysAltern, exakt, alter_sek, new TwoGroup<>(transComp, transAlter_sek));
    }

    public TransAlterComp(Entity entity, SysAltern sysAltern,
                          boolean exakt, float alter_sek,
                          TwoGroup<TransComp, Float>... transComp_transAlter) {
        super(entity, sysAltern, new Comp[]{}, alter_sek);
        this.exakt = exakt;

        List<TwoGroup<TransComp, Float>> tgList = new ArrayList<>(Arrays.asList(transComp_transAlter));
        while (!tgList.isEmpty()) {
            float maxTransAlter = -Float.MAX_VALUE;
            TwoGroup<TransComp, Float> maxTac = null;
            for (TwoGroup<TransComp, Float> tg : tgList) {
                float alter = tg.v2;
                if (alter > maxTransAlter) {
                    maxTransAlter = alter;
                    maxTac = tg;
                } else if (alter == maxTransAlter) {
                    throw new IllegalArgumentException();
                }
            }
            if (maxTac == null) throw new IllegalStateException();
            alterList.add(maxTransAlter);
            transCompList.add(maxTac.v1);
            tgList.remove(maxTac);
        }
    }

    /** Kein Einsortieren moeglich, da fuer eine {@link EcsList} kein add mit Index moeglich ist! */
    public void addTrans(TransComp transComp, float transAlter_sek) {
        if (!alterList.isEmpty()) {
            float maxAlter = alterList.get(0);
            if (maxAlter < transAlter_sek) {
                throw new IllegalArgumentException("Aelteste zuerst adden!");
            } else if (maxAlter == transAlter_sek) {
                throw new IllegalArgumentException("TransComps kombinieren!");
            }
        }
        alterList.add(transAlter_sek);
        transCompList.add(transComp);
    }

    @Override
    public void update(float delta) {
        super.update(delta);

        int executeAbIndex = -1;
        for (int i = 0; i < alterList.size(); i++) {
            if (alterList.get(i) < getAlter_sek()) {
                executeAbIndex = i;
                break;
            }
        }
        if (executeAbIndex == -1) {
            return;
        }
        while (alterList.size() > executeAbIndex) {
            int s = alterList.size() - 1;
            alterList.remove(s);
            TransComp transComp = transCompList.remove(s);
            if (transComp.isDeleted()) {
                /* Aktuell der einfachste Weg, da es schwieriger ist eine EcsList und die AlterList
                synchron zu halten. Alternativ diese Klasse zu einem Connectable umwandeln oder
                neue EcsListe einfuegen */
                delete();
                return;
            }
            transComp.trans();
        }
    }

    public int size() {
        return transCompList.size();
    }
}
