package de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld;

import de.arnonym.strategiespiel.spielSchirm.Dorf;
import de.arnonym.strategiespiel.framework.ecs.component.Comp;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.ArbeitnehmerAi;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.listSystems.privateListSystems.SysEinkommen;

public class EinkommenComp extends Comp {
    public final Dorf dorf;
    public final GeldComp geldComp;

    private ArbeitnehmerAi arbeitnehmerAi;

    public EinkommenComp(Dorf dorf, Entity entity, SysEinkommen sysEinkommen, GeldComp geldComp) {
        super(sysEinkommen, entity, geldComp);
        this.dorf = dorf;
        this.geldComp = geldComp;
    }

    // KOMPLEXE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public int getEinkommen() {
        if (arbeitnehmerAi.isEmployed()) {
            return arbeitnehmerAi.getGehalt();
        }
        return dorf.getGehalt();
    }// todo ist nicht real Gehalt. evtl will ich lieber das anzeigen lassen
}
