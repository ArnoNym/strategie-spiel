package de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.kollision;

import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.groesse.IsoGroesseComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.sonstiges.PositionComp;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.SysKollision;

public class KollisionCompBauen extends KollisionComp {
    public KollisionCompBauen(Entity entity, SysKollision sysKollision,
                              IsoGroesseComp isoGroesseComp, PositionComp positionComp) {
        super(entity, sysKollision, isoGroesseComp, positionComp);
    }
}
