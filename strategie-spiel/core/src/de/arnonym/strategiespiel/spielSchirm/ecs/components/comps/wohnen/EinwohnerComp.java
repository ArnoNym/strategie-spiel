package de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.wohnen;

import de.arnonym.strategiespiel.framework.ecs.component.Comp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.bewegung.BewegungComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dna.DbDnaComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.werte.HabitalwerteComp;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;

public class EinwohnerComp extends Comp {
    public final BewegungComp bewegungComp;
    public final DbDnaComp dbDnaComp;
    public final HabitalwerteComp habitalwerteComp;
    private MieterComp mieterComp;

    public EinwohnerComp(Entity entity, BewegungComp bewegungComp, DbDnaComp dbDnaComp,
                         HabitalwerteComp habitalwerteComp) {
        super(entity, bewegungComp, dbDnaComp, habitalwerteComp);
        this.bewegungComp = bewegungComp;
        this.dbDnaComp = dbDnaComp;
        this.habitalwerteComp = habitalwerteComp;
    }

    public MieterComp getMieterComp() {
        return mieterComp;
    }

    public void setMieterComp(MieterComp mieterComp) {
        this.mieterComp = mieterComp;
    }
}
