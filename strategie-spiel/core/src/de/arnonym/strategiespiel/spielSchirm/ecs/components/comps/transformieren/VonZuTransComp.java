package de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.transformieren;

import java.util.LinkedList;

import de.arnonym.strategiespiel.framework.ecs.component.Comp;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.framework.ecs.collections.EcsList;
import de.arnonym.strategiespiel.spielSchirm.Enums;

public class VonZuTransComp extends TransComp {
    private final EcsList<Comp> activateComps;
    private final EcsList<Comp> loeschenComps;

    public VonZuTransComp(Entity entity, EnumDelete enumDelete, Comp[] activateComps,
                          Comp[] loeschenComps, TransLogic... transLogics) {
        super(entity, enumDelete, transLogics);

        this.activateComps = new EcsList<>(this, Enums.EnumDopplung.EXCEPTION,
                Enums.EnumNull.IGNORE, new LinkedList<>());
        this.loeschenComps = new EcsList<>(this, Enums.EnumDopplung.EXCEPTION,
                Enums.EnumNull.IGNORE, new LinkedList<>());

        addActivateComps(activateComps);
        addLoeschenComps(loeschenComps);
    }

    @Override
    protected boolean trans_or() {
        for (Comp c : activateComps) {
            c.activate();
        }
        activateComps.clear();

        for (Comp c : loeschenComps.toArray(new Comp[]{})) {
            c.delete();
        }

        return true;
    }

    public void addActivateComps(Comp... activateComps) {
        this.activateComps.addAll(activateComps);
    }

    public void addLoeschenComps(Comp... loeschenComps) {
        this.loeschenComps.addAll(loeschenComps);
    }
}