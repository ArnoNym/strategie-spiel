package de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld;

import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.Geld;
import de.arnonym.strategiespiel.spielSchirm.Enums;

public class VariabelGeldComp extends GeldComp {
    private Geld geld;

    public VariabelGeldComp(Entity entity) {
        super(entity);
    }

    public void settGeld(Geld geld) {
        support.firePropertyChange(Enums.PC_EVENT_NAME_GELD, this.geld, geld);
        this.geld = geld;
    }

    @Override
    public Geld getGeld() {
        return geld;
    }
}
