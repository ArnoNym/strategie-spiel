package de.arnonym.strategiespiel.spielSchirm.ecs.collections.lists.ausbeuten;

import de.arnonym.strategiespiel.framework.listener.DeletableStoryListener;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.ausbeuten.AbbaubarComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.herrschaft.EigentuemerComp;

public class AbzubauenEcsList extends AuszubeutenEcsList<AbbaubarComp> {
    public AbzubauenEcsList(DeletableStoryListener parent, EigentuemerComp eigentumComp) {
        super(parent, eigentumComp);
    }
}
