package de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld;

import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.listSystems.privateListSystems.SysGeld;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.Geld;

public class OriginGeldComp extends GeldComp {
    public final Geld geld;

    public OriginGeldComp(Entity entity, SysGeld sysGeld, int geldMenge) {
        super(entity, sysGeld);
        this.geld = new Geld(geldMenge);
    }

    @Override
    public Geld getGeld() {
        return geld;
    }
}
