package de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.klick;

import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.groesse.CompGroesse;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.rendern.RenderComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.sonstiges.PositionComp;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.listSystems.privateListSystems.SysKlick;

public class KlickComp extends CompKlick {

    public KlickComp(Entity entity, SysKlick sysKlick, CompGroesse compGroesse, PositionComp positionComp, RenderComp renderComp) {
        super(entity, sysKlick, compGroesse, positionComp, renderComp);
    }

    @Override
    public void gotHit() {

    }
}
