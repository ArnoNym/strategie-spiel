package de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.transformieren.zu;

import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.klick.FensterKlickComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.rendern.RenderComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.groesse.IsoGroesseComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.kollision.KollisionCompBauen;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.kollision.LaufenKollisionComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.sonstiges.PositionComp;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.listSystems.privateListSystems.SysKlick;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.SysKollision;

public class TransZuBodenStapelComp extends TransZuComp {
    private PositionComp positionComp;
    private IsoGroesseComp isoGroesseComp;
    private RenderComp renderComp;

    private FensterKlickComp fensterKlickComp;
    private KollisionCompBauen compKollisionBauen;
    private LaufenKollisionComp compKollisionLaufen;

    public TransZuBodenStapelComp(Entity entity, SysKlick sysKlick, SysKollision sysKollision, EnumDelete enumDelete, TransLogic... transLogics) {
        super(entity, enumDelete, transLogics); // TODO Es duerfte in nahezu allen Faellen einfacher sein einen neuen BodenStapel zu erstellen

        // MUSS VORHANDEN SEIN ////////////////////////////////////////////////////////////////////

        new MussVorhandenComp<PositionComp>() {
            @Override
            protected void zuweisen_or(PositionComp positionComp) {
                TransZuBodenStapelComp.this.positionComp = positionComp;
            }
        };
        new MussVorhandenComp<IsoGroesseComp>() {
            @Override
            protected void zuweisen_or(IsoGroesseComp isoGroesseComp) {
                TransZuBodenStapelComp.this.isoGroesseComp = isoGroesseComp;
            }
        };
        new MussVorhandenComp<RenderComp>() {
            @Override
            protected void zuweisen_or(RenderComp renderComp) {
                TransZuBodenStapelComp.this.renderComp = renderComp;
            }
        };

        // KANN VORHANDEN SEIN ////////////////////////////////////////////////////////////////////

        /*new KannVorhandenComp<CompOeffneFensterKlick>() {
            @Override
            protected void zuweisen_or(CompOeffneFensterKlick compOeffneFensterKlickIso) {
                CompTransZuBodenStapel.this.compOeffneFensterKlick = compOeffneFensterKlickIso;
            }
            @Override
            protected CompOeffneFensterKlick ertellen_or() {
                return new CompOeffneFensterKlick(entityId, sysKlick, compGroesseIso, compPosition,
                        compRenderUnbeweglich, new FokNameMenge()).setObjectInfo();
            }
        };*/
        new KannVorhandenComp<KollisionCompBauen>() {
            @Override
            protected void zuweisen_or(KollisionCompBauen compKollisionBauen) {
                TransZuBodenStapelComp.this.compKollisionBauen = compKollisionBauen;
            }
            @Override
            protected KollisionCompBauen ertellen_or() {
                KollisionCompBauen kollisionCompBauen = new KollisionCompBauen(entity, sysKollision,
                        isoGroesseComp, positionComp);
                kollisionCompBauen.activate();
                return kollisionCompBauen;
            }
        };
        new KannVorhandenComp<LaufenKollisionComp>() {
            @Override
            protected void zuweisen_or(LaufenKollisionComp compKollisionLaufen) {
                TransZuBodenStapelComp.this.compKollisionLaufen = compKollisionLaufen;
            }
            @Override
            protected LaufenKollisionComp ertellen_or() {
                LaufenKollisionComp laufenKollisionComp = new LaufenKollisionComp(entity,
                        sysKollision, isoGroesseComp, positionComp, 0);
                laufenKollisionComp.activate();
                return laufenKollisionComp;
            }
        };
    }
}
