package de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.klick;

import com.badlogic.gdx.math.Vector2;

import de.arnonym.strategiespiel.framework.ecs.component.Comp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.groesse.CompGroesse;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.rendern.RenderComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.sonstiges.PositionComp;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.listSystems.privateListSystems.SysKlick;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.GeometryUtils;

public abstract class CompKlick extends Comp {
    private final CompGroesse compGroesse;
    private final PositionComp positionComp;
    private final RenderComp renderComp;

    public CompKlick(Entity entity, SysKlick sysKlick, CompGroesse compGroesse,
                     PositionComp positionComp, RenderComp renderComp) {
        super(sysKlick, entity, compGroesse, positionComp, renderComp);
        this.compGroesse = compGroesse;
        this.positionComp = positionComp;
        this.renderComp = renderComp;
    }

    /*@Override
    public int ident() {
        return Identifiable.IDENT_COMP_KLICK;
    }*/

    public boolean issHit(Vector2 isoWorldKlick) {
        if (!getRenderComp().isVisible()) {
            return false;
        }
        Vector2 ortoWorldKlick = GeometryUtils.isoToOrto(isoWorldKlick);
        float cX = ortoWorldKlick.x;
        float cY = ortoWorldKlick.y;
        Vector2 ortoWorldPosition = GeometryUtils.isoToOrto(getPositionComp().getPosition());
        float x = ortoWorldPosition.x;
        float y = ortoWorldPosition.y;
        float width = compGroesse.getWidth() / 2;
        float height = compGroesse.getHeight() / 2;
        return cX >= x - width && cX < x + width && cY >= y - height && cY < y + height;
    }

    public abstract void gotHit();

    public RenderComp getRenderComp() {
        return renderComp;
    }

    public CompGroesse getCompGroesse() {
        return compGroesse;
    }

    public PositionComp getPositionComp() {
        return positionComp;
    }
}
