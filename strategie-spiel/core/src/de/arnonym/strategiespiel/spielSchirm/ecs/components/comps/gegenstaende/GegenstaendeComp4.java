package de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.gegenstaende;

import java.util.Arrays;
import java.util.List;

import de.arnonym.strategiespiel.spielSchirm.umwelt.CompSterben;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.rendern.HashMapRenderComp;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.endstufe.Gegenstand;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.vorstufe.EntityGegInfoList;
import de.arnonym.strategiespiel.spielSchirm.Enums;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.MathUtils;

public class GegenstaendeComp4 extends GegenstaendeComp2 {
    public final CompSterben compSterben;
    public final List<Integer> primaerGegIds; // Alle Geg die aussschlaggebend dafuer sind ob stirbt

    public GegenstaendeComp4(Entity entity, HashMapRenderComp hashMapRenderComp,
                             CompSterben compSterben, EntityGegInfoList entityGegInfoListe,
                             Integer[] primaerGegIds) {
        super(entity, hashMapRenderComp, entityGegInfoListe);
        this.compSterben = compSterben;
        this.primaerGegIds = Arrays.asList(primaerGegIds);
    }

    @Override
    public void veraenderungMengeVerfuegbar(Gegenstand gegenstand, int veraendertUmMenge) {
        /* TODO Das meusste richtigER sein. Konnte es gerad enicht testen
        if (compSterben.isTod()) {
            super.veraenderungMengeVerfuegbar(gegenstand, veraendertUmMenge);
            return;
        }

        boolean isPrimaerGeg = primaerGegIds.contains(gegenstand.gettId());
        if (isPrimaerGeg) {
            if (veraendertUmMenge <= 0) {
                compSterben.sterbenBedingungErfuellt();
            }
            super.veraenderungMengeVerfuegbar(gegenstand, veraendertUmMenge);
            return;
        }
        */

        boolean primaerGeg = primaerGegIds.contains(gegenstand.gettId()); // Potentielle Fehlerquelle. Funktioniert cotains mit Integers die nicht das selbe Objekt sind?

        if (compSterben.isTod()) {
            super.veraenderungMengeVerfuegbar(gegenstand, veraendertUmMenge);
            return;
        }

        if (primaerGeg) {
            if (veraendertUmMenge <= 0) {
                compSterben.sterbenBedingungErfuellt();
            }
            super.veraenderungMengeVerfuegbar(gegenstand, veraendertUmMenge);
            return;
        }

        int fruechteVerbrauchterPlatz = 0;
        int sonstigesVerbrauchterPlatz = 0;
        for (Gegenstand g : gegenstandListe) {
            int verbrauchterPlatz = MathUtils.myIntMult(g.gettMenge(true, false, true),
                    g.gegMuster.platzVerbrauch);
            if (primaerGegIds.contains(g.gettId())) {
                sonstigesVerbrauchterPlatz += verbrauchterPlatz;
            } else {
                fruechteVerbrauchterPlatz += verbrauchterPlatz;
            }
        }

        float verbrauchterPlatzProzent = (float) fruechteVerbrauchterPlatz
                / (gegenstandListe.getMaxPlatz() - sonstigesVerbrauchterPlatz);

        if (verbrauchterPlatzProzent > 1) {
            throw new IllegalArgumentException(""+verbrauchterPlatzProzent);
        } else if (verbrauchterPlatzProzent == 1) {
            hashMapRenderComp.add(Enums.EnumAktion.SEKUNDAERMENGE_VOLL);
        } else if (verbrauchterPlatzProzent > 0.5) {
            hashMapRenderComp.add(Enums.EnumAktion.SEKUNDAERMENGE_MITTEL);
        } else if (verbrauchterPlatzProzent > 0) {
            hashMapRenderComp.add(Enums.EnumAktion.SEKUNDAERMENGE_WENIG);
        } else {
            hashMapRenderComp.add(Enums.EnumAktion.STANDARD_PRIMAERMENGE_VOLL);
        }
    }

    @Override
    protected void platzEmpty() {
        hashMapRenderComp.add(Enums.EnumAktion.PRIMAERMENGE_LEER);
    }
}
