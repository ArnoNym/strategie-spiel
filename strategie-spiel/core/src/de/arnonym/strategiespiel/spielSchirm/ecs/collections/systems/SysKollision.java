package de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import de.arnonym.strategiespiel.spielSchirm.World;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.kollision.KollisionComp;
import de.arnonym.strategiespiel.framework.ecs.collections.System;
import de.arnonym.strategiespiel.spielSchirm.worldMap.Zelle;

public class SysKollision extends System<KollisionComp> {
    private final World world;

    public SysKollision(World world) {
        this.world = world;
    }

    @Override
    protected void update_or(float delta) {
        throw new IllegalStateException("Dont call this!");
    }

    @Override
    public boolean connect_or(KollisionComp kollisionComp) {
        List<Zelle> zellenListe = world.worldMap.gettBetroffeneZellenListe(
                kollisionComp.positionComp.getPosition(),
                kollisionComp.isoGroesseComp.getGroesseInZellen());
        List<Zelle> notNullZellenListe = new ArrayList<>();

        for (Zelle z : zellenListe) {
            if (z != null) {
                z.add(kollisionComp);
                notNullZellenListe.add(z);
            }
        }

        kollisionComp.setBetrifftZellenListe_nl(notNullZellenListe);

        return true;
    }

    @Override
    public boolean disconnect_or(KollisionComp kollisionComp) {
        List<Zelle> betroffeneZelleListe = kollisionComp.getBetrifftZellenListe_nl();

        boolean removed = false;
        for (Zelle z : betroffeneZelleListe) {
            removed = removed | z.remove(kollisionComp);
        }

        kollisionComp.setBetrifftZellenListe_nl(null);

        return removed;
    }

    @Override
    public boolean contains(KollisionComp kollisionComp) {
        return kollisionComp.getBetrifftZellenListe_nl() != null;
    }

    @Override
    public <COLL extends Collection<KollisionComp>> COLL toCollection(COLL coll) {
        for (Zelle z : world.worldMap.gettAlleZellenList()) {
            coll.addAll(z.onCellKollisionComps());
        }
        return coll;
    }

    /*@Override
    public int size() {
        throw new IllegalStateException(); //todo
    }*/
}
