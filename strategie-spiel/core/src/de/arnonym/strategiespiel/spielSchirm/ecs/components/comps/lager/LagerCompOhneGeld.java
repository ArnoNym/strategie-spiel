package de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.lager;

import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.bewegung.BewegungszielComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.gegenstaende.GegenstaendeComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.herrschaft.EigentumComp;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.listSystems.privateListSystems.SysLager;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.listSystems.privateListSystems.priceSystems.market.GegMarketPriceSys;

public class LagerCompOhneGeld extends LagerComp {
    public LagerCompOhneGeld(Entity entity, SysLager sysLager, GegMarketPriceSys gegMarketPriceSystem,
                             EigentumComp eigentumComp, BewegungszielComp bewegungszielComp,
                             GegenstaendeComp gegenstaendeComp) {
        super(entity, sysLager, gegMarketPriceSystem, eigentumComp, bewegungszielComp,
                gegenstaendeComp);
    }
}
