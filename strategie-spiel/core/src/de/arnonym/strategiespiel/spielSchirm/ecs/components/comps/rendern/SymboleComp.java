package de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.rendern;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;
import java.util.List;

import de.arnonym.strategiespiel.spielSchirm.balancing.IdsSymbole;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.bewegung.BewegungszielComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.sonstiges.PositionComp;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.SysRender;

public class SymboleComp extends RenderComp {
    private final BewegungszielComp bewegungsziel_Comp_nl;
    private float heightMod;

    public SymboleComp(Entity entity,

                       SysRender sysRendern,

                       PositionComp positionComp, BewegungszielComp bewegungsziel_Comp_nl,

                       SysRender.EnumZ z, float heightMod) {
        super(entity, sysRendern, positionComp, z);
        this.bewegungsziel_Comp_nl = bewegungsziel_Comp_nl;
        this.heightMod = heightMod;
    }


    @Override
    protected void render_or(SpriteBatch spriteBatch, float delta) {
        List<IdsSymbole.StatSymbol> statSymbolList = new ArrayList<>();

        addStatischeSymbole(statSymbolList);

        updatePositionen(statSymbolList);

        for (IdsSymbole.StatSymbol ss : statSymbolList) {
            ss.render(spriteBatch);
        }
    }

    private void addStatischeSymbole(List<IdsSymbole.StatSymbol> statSymbolList) {
        if (bewegungsziel_Comp_nl != null && bewegungsziel_Comp_nl.isKeinPfad()) {
            statSymbolList.add(IdsSymbole.SYMBOL_KEIN_PFAD);
        }

        // TODO Weitere hinzufuegen ...
    }

    private void updatePositionen(List<IdsSymbole.StatSymbol> statSymbolList) {
        float width = IdsSymbole.oneSymbolWidth * statSymbolList.size();

        Vector2 linksUntenPosition = new Vector2(
                positionComp.position.x - width / 2,
                positionComp.position.y - IdsSymbole.height / 2 + heightMod);

        List<IdsSymbole.StatSymbol> initStatSymbolList = new ArrayList<>(statSymbolList.size());

        for (IdsSymbole.StatSymbol ss : statSymbolList) {
            initStatSymbolList.add(ss.init(linksUntenPosition));
            linksUntenPosition.x += IdsSymbole.oneSymbolWidth;
        }

        statSymbolList.clear();
        statSymbolList.addAll(initStatSymbolList);
    }

    // PRUEFE /////////////////////////////////////////////////////////////////////////////////////

    protected boolean preufeVisible_or(float kameraLeft, float kameraRight, float kameraTop,
                                       float kameraBottom) {
        float x = positionComp.getPosition().x;
        float y = positionComp.getPosition().y + heightMod;
        float halfWidth = IdsSymbole.maxPerRow * IdsSymbole.oneSymbolWidth / 2;
        float halfHeigt = IdsSymbole.height / 2;
        return (!(kameraLeft >= x + halfWidth || kameraRight <= x - halfWidth
                || kameraTop >= y + halfHeigt || kameraBottom <= y - halfHeigt));
    }

    // EINFACHE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public float getHeightMod() {
        return heightMod;
    }

    public void setHeightMod(float heightMod) {
        this.heightMod = heightMod;
    }
}
