package de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.gegenstaende;

import de.arnonym.strategiespiel.framework.ecs.component.Comp;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.endstufe.Gegenstand;
import de.arnonym.strategiespiel.framework.werkzeuge.collections.id.IdList;

public abstract class BasisGegComp<LIST extends IdList<Gegenstand>> extends Comp {

    public BasisGegComp(Entity entity, Comp... comps) {
        super(entity, comps);
    }

    public abstract LIST getList();
}
