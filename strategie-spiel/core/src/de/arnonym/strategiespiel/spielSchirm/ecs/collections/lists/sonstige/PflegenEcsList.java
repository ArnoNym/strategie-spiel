package de.arnonym.strategiespiel.spielSchirm.ecs.collections.lists.sonstige;

import de.arnonym.strategiespiel.framework.listener.DeletableStoryListener;
import de.arnonym.strategiespiel.spielSchirm.umwelt.ressourcen.PflegenComp;
import de.arnonym.strategiespiel.framework.ecs.collections.EcsList;

public class PflegenEcsList extends EcsList<PflegenComp> {
    public PflegenEcsList(DeletableStoryListener parent) {  // Koennte man auch zum reparieren verwenden!! Dazu muss die Componente veraendert werden
        super(parent);
    }

}
