package de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.groesse;

import com.badlogic.gdx.math.Vector2;

import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenOptik;

public class IsoGroesseComp extends CompGroesse {
    private Vector2 groesseInZellen;

    public IsoGroesseComp(Entity entity, Vector2 groesseInZellen) {
        super(entity);
        this.groesseInZellen = groesseInZellen;
    }

    @Override
    public float getWidth() {
        return groesseInZellen.x * KonstantenOptik.TILE_ORTO_WIDTH_AND_HEIGHT;
    }

    @Override
    public float getHeight() {
        return groesseInZellen.y * KonstantenOptik.TILE_ORTO_WIDTH_AND_HEIGHT;
    }

    public Vector2 getGroesseInZellen() {
        return groesseInZellen;
    }

    public void setGroesseInZellen(Vector2 groesseInZellen) {
        this.groesseInZellen = groesseInZellen;
    }
}
