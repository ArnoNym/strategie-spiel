package de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.gegenstaende;

import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.rendern.HashMapRenderComp;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.vorstufe.EntityGegInfoList;
import de.arnonym.strategiespiel.spielSchirm.Enums;

public class GegenstaendeComp1 extends GegenstaendeComp {

    public GegenstaendeComp1(Entity entity, HashMapRenderComp hashMapRenderComp,
                             EntityGegInfoList entityGegInfoListe) {
        super(entity, hashMapRenderComp, entityGegInfoListe);
    }

    @Override
    protected void platzEmpty() {
        hashMapRenderComp.add(Enums.EnumAktion.PRIMAERMENGE_LEER);
    }
}
