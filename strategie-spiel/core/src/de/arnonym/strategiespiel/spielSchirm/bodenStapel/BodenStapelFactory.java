package de.arnonym.strategiespiel.spielSchirm.bodenStapel;

import com.badlogic.gdx.math.Vector2;

import de.arnonym.strategiespiel.spielSchirm.Dorf;
import de.arnonym.strategiespiel.spielSchirm.World;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.herrschaft.EigentuemerComp;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.vorstufe.EntityGegInfo;
import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenBalance;

public class BodenStapelFactory {
    public final World world;
    public final Dorf dorf;

    public BodenStapelFactory(World world, Dorf dorf) {
        this.world = world;
        this.dorf = dorf;
    }

    public BodenStapel create(EigentuemerComp eigentuemerComp, int gegId, int gegMengeVerfuegbar,
                              Vector2 inNaheVonPosition) {
        return new BodenStapel(world, dorf, eigentuemerComp,
                new EntityGegInfo(gegId, gegMengeVerfuegbar, KonstantenBalance.BODEN_STAPEL_MAX_PLATZ, 0),
                world.worldMap.gettFreieZelle(inNaheVonPosition, true, false).getPosition());
    }
}
