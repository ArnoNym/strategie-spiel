package de.arnonym.strategiespiel.spielSchirm.bodenStapel;

import com.badlogic.gdx.math.Vector2;

import de.arnonym.strategiespiel.Strategiespiel;
import de.arnonym.strategiespiel.spielSchirm.Dorf;
import de.arnonym.strategiespiel.spielSchirm.World;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.bewegung.BewegungszielComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.gegenstaende.GegenstaendeComp2;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.herrschaft.EigentuemerComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.herrschaft.EigentumComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.klick.FensterKlickComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.kollision.KollisionCompBauen;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.lager.LagerCompOhneGeld;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.name.NameComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.rendern.HashMapRenderComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.rendern.UnbeweglichRenderComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.groesse.IsoGroesseComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.kollision.LaufenKollisionComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.gegenstaende.GegenstaendeComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.sonstiges.PositionComp;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.SysRender;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterOnKlick.OnKlickFenster;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterOnKlick.OnKlickFensterMenge;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.vorstufe.EntityGegInfo;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.vorstufe.EntityGegInfoList;
import de.arnonym.strategiespiel.spielSchirm.Assets;
import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenOptik;
import de.arnonym.strategiespiel.framework.werkzeuge.Paket;
import de.arnonym.strategiespiel.framework.werkzeuge.textures.TextureHashMap;
import de.arnonym.strategiespiel.framework.werkzeuge.textures.IdTextureRegion;

public class BodenStapel extends Entity {
    private final BewegungszielComp bewegungszielComp;
    private final GegenstaendeComp gegenstaendeComp;

    protected BodenStapel(World world, Dorf dorf, EigentuemerComp eigentuemerComp,
                          EntityGegInfo entityGegInfo, Vector2 position) {
        super(world.manager);
        PositionComp positionComp = new PositionComp(this, position);
        IsoGroesseComp isoGroesseComp = new IsoGroesseComp(this, new Vector2(1, 1));
        LaufenKollisionComp compKollisionLaufen = new LaufenKollisionComp(this, world.sysKollision,
                isoGroesseComp, positionComp, 0.5f);
        KollisionCompBauen compKollisionBauen = new KollisionCompBauen(this, world.sysKollision,
                isoGroesseComp, positionComp);
        bewegungszielComp = new BewegungszielComp(this, compKollisionLaufen, false);
        HashMapRenderComp hashMapRenderComp = new UnbeweglichRenderComp(this, world.sysRender,
                positionComp,
                new TextureHashMap(
                        new IdTextureRegion(Assets.instance.gebaudeAssets.bodenStapel, true, -1,
                                KonstantenOptik.BODEN_STAPEL_FUNDAMENT)), SysRender.EnumZ.ENTITIES,
                true);
        EigentumComp eigentumComp = new EigentumComp(eigentuemerComp, this);
        EntityGegInfoList entityGegInfoListe = new EntityGegInfoList(1000, false);
        entityGegInfoListe.add(entityGegInfo);
        gegenstaendeComp = new GegenstaendeComp2(this, hashMapRenderComp, entityGegInfoListe);
        NameComp nameComp = new NameComp(this, "pile");
        LagerCompOhneGeld compLagerOhneGeld = new LagerCompOhneGeld(this, world.sysLager,
                dorf.gegMarketPriceSystem, eigentumComp, bewegungszielComp, gegenstaendeComp);

        Paket<OnKlickFenster> paket = () -> (new OnKlickFensterMenge(world, Strategiespiel.skin,
                gegenstaendeComp, hashMapRenderComp, nameComp));
        new FensterKlickComp(this, world.sysKlick, isoGroesseComp, positionComp,
                hashMapRenderComp, paket);

        activate();
    }

    public BewegungszielComp getBewegungszielComp() {
        return bewegungszielComp;
    }

    public GegenstaendeComp getGegenstaendeComp() {
        return gegenstaendeComp;
    }
}
