package de.arnonym.strategiespiel.spielSchirm.worldMap.creator;

import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;

import de.arnonym.strategiespiel.spielSchirm.World;
import de.arnonym.strategiespiel.spielSchirm.worldMap.WorldMap;

public class CreatorEntities extends Creator {

    public CreatorEntities(World world, WorldMap worldMap, TiledMapTileLayer layer) {
        super(world, worldMap, layer);
    }

    @Override
    protected void createSingle(int x, int y) {
        TiledMapTileLayer.Cell cell = layer.getCell(x, y);
        if (cell == null) {
            return;
        }
        worldMap.ressourcenFactory.createRandom(cell.getTile().getId(), worldMap.gett(x, y).getPosition());
    }

    /*
    private void erstelleEntities(SysRender sysRender, TiledMapTile tiledMapTile) {


        for (HashMap<Integer, Zelle> yHashMap : xHashMap.values()) {
            for (Zelle zelle : yHashMap.values()) {
                zelle.pruefeUmringtVonKoll();
            }
        }
    }

    private void erstelleEntitiy() {
        int key = tiledMapTile.createId();

        //TODO Ressource und Ressource Nachwachsen unterscheiden

        Enums.EnumResNwStufe alter;
        float randomFloat = ThreadLocalRandom.current().nextFloat();
        if (randomFloat > 0.75f) {
            alter = Enums.EnumResNwStufe.JUNG;
        } else if (randomFloat > 0.5f) {
            alter = Enums.EnumResNwStufe.MITTEL;
        } else {
            alter = Enums.EnumResNwStufe.ALT;
        }

        Entity entityId = new Tanne(false, worldMap.getInfoGegListe(), level.getSysAbbaubar(),
                level.getSysKollision(), sysRender, position, alter);

        gespawntEntities.handle(entityId);
        /*settStatischesObjekt(IdsRessourcenWachsen.getValue(key, alter).finialize(getLevel(),
                getLevel().getRessourcenListe(), getLevel().getRenderArrayList(), false, this,
                Enums.EnumAbbauen.NEIN, true));*
    }*/
}
