package de.arnonym.strategiespiel.spielSchirm.worldMap.creator;

import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;

import de.arnonym.strategiespiel.spielSchirm.World;
import de.arnonym.strategiespiel.spielSchirm.worldMap.WorldMap;

public abstract class Creator {
    // Um WorldMap zu entrumpeln und Methoden zu abstrahieren

    public final World world;
    public final WorldMap worldMap;
    public final TiledMapTileLayer layer;
    public final int width;
    public final int height;

    public Creator(World world, WorldMap worldMap, TiledMapTileLayer layer) {
        this.world = world;
        this.worldMap = worldMap;
        this.layer = layer;
        this.width = layer.getWidth();
        this.height = layer.getHeight();
    }

    public void create(int left, int bottom) {
        // Zellen oben Bis mitte
        int laengsteSeite = width; // Rows und Columns muessen gleich lang sein!!
        for (int zellen = 1; zellen <= laengsteSeite; zellen++) {
            for (int z = 0; z < zellen; z++) {
                int j = z;
                int i = laengsteSeite - zellen + z;
                createSingle(j, i);
            }
        }
        // Zellen einen unter der Mitte bis unten
        for (int zellen = laengsteSeite - 1; zellen >= 1; zellen--) {
            for (int z = 0; z < zellen; z++) {
                int j = laengsteSeite - zellen + z;
                int i = z;
                createSingle(j, i);
            }
        }
    }

    protected abstract void createSingle(int x, int y);

    /*public void erstelleBoden(TiledMapTileLayer tiledMapBodenLayer, Enums.EnumEinfacheRichtung direction) {
        int width = tiledMapBodenLayer.getProperties().createRandom("width", Integer.class);
        int height = tiledMapBodenLayer.getProperties().createRandom("height", Integer.class);
        if (xHashMap.isEmpty()) {
            erstelleBoden(tiledMapBodenLayer, width, height);
        } else {
            //vergreossern(tiledMapBodenLayer, tiledMapSpawnenLayer, width, height, Enums.EnumEinfacheRichtung.RECHTS);
        }
    }*/
}
