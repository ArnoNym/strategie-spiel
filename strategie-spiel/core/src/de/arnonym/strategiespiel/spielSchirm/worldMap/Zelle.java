package de.arnonym.strategiespiel.spielSchirm.worldMap;

import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import de.arnonym.strategiespiel.spielSchirm.World;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.bewegung.BewegungszielComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.herrschaft.EigentumComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.rendern.RenderComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.rendern.UnbeweglichRenderComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.groesse.IsoGroesseComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.herrschaft.EigentuemerComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.kollision.KollisionComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.kollision.KollisionCompBauen;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.kollision.LaufenKollisionComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.sonstiges.PositionComp;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.SysRender;
import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenBalance;
import de.arnonym.strategiespiel.framework.werkzeuge.Reservierer;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.GeometryUtils;
import de.arnonym.strategiespiel.spielSchirm.balancing.IdsBoden;
import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenOptik;
import de.arnonym.strategiespiel.framework.werkzeuge.textures.TextureHashMap;

public class Zelle extends Entity {
    private final World world;
    private final WorldMap worldMap;

    public final BewegungszielComp bewegungszielComp;
    public final EigentumComp eigentumComp;
    public final RenderComp renderComp;
    public final Vector2 position; // Position in der (auch visuellen) Mitte der Zelle

    private final Set<KollisionComp> kollisionComps = new HashSet<>();
    private boolean kollisionBauen;
    private boolean kollisionLaufen;
    private boolean umringtVonKollisionBauen;
    private boolean umringtVonKollisionLaufen;
    public final Reservierer reservierer = new Reservierer(1);
    private transient Boden boden;
    private float fruchtbarkeit; // f(initialeFruchtbarkeit, verschmutzung, feuchtigkeit, temperatur)
    private final float initialeFruchtbarkeit; // 0 < X < 1. zB steiniger Boden etc
    private float verschmutzung = 0; // 0 < X < 1
    private float feuchtigkeit; // 0 < X < 1
    private float temperatur; // 0 < X < 1

    public Zelle(World world, WorldMap worldMap, EigentuemerComp eigentuemerComp,
                 int indexX, int indexY, TiledMapTile bodenLayerTiledMapTile) {
        super(world.manager);
        this.world = world;
        this.worldMap = worldMap;
        SysRender sysRender = world.getSysRender();
        //worldInputProcessor.connectTo(this);
        //this.compEigentuemer = compEigentuemer;

        // Boden definieren //
        int tileId = bodenLayerTiledMapTile.getId();
        boden = IdsBoden.get(tileId);
        initialeFruchtbarkeit = boden.getGegebeneFruchtbarkeit();
        feuchtigkeit = boden.getFeuchtigkeit();
        temperatur = boden.getTemperatur();
        updateFruchtbarkeit();

        // Componenten erstellen //
        Vector2 linkeEckePosition = GeometryUtils.ortoToIso(new Vector2(
                indexX * KonstantenOptik.TILE_ORTO_WIDTH_AND_HEIGHT,
                indexY * KonstantenOptik.TILE_ORTO_WIDTH_AND_HEIGHT));
        position = new Vector2(
                linkeEckePosition.x + KonstantenOptik.TILE_ISO_WIDTH / 2,
                linkeEckePosition.y);
        PositionComp positionComp = new PositionComp(this, position);
        IsoGroesseComp isoGroesseComp = new IsoGroesseComp(this, new Vector2(1, 1));
        bewegungszielComp = new BewegungszielComp(this, new LaufenKollisionComp(this,
                world.sysKollision, isoGroesseComp, positionComp, 1), false);
        // TODO Anklickbar?!
        if (eigentuemerComp == null) {
            eigentumComp = new EigentumComp(this);
        } else {
            eigentumComp = new EigentumComp(eigentuemerComp, this);
        }
        renderComp = new UnbeweglichRenderComp(this, sysRender, positionComp,
                new TextureHashMap(new ZellenTexture(boden)),
                SysRender.EnumZ.ZELLE, false);

        activate();

        updateKollision(kollisionComps, boden);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////

    // UPDATE /////////////////////////////////////////////////////////////////////////////////////

    public void update(float delta) {
        updateFruchtbarkeit();
        /* TODO Update feuchtigkeit und temperatur
        - Feuchtigkeit koennte durch Wasserfelder in der Naehe steigen (sodass man Felder
        bewaessern kann) + Umweldereignisse (updateKollision(); nicht vergessen!)
        - Temperatur steigt durch Umwelteriegnisse */

        //if (isKollisionBauen()) {
            //Level.zellenMarkierungRenderer.handle(position, Assets.instance.tiledMapAssets.durchsichtigWeiss, true);
            //Level.zellenMarkierungRenderer.handle(position, new Vector2(1,1), Assets.instance.tiledMapAssets.durchsichtigWeiss);
        //}
    }

    private void updateFruchtbarkeit() {
        fruchtbarkeit = (float) Math.min(Math.pow(initialeFruchtbarkeit * (1-verschmutzung)
                * feuchtigkeit * temperatur, 0.5), 1);
        /*if (statischesObjekt != null && statischesObjekt instanceof RessourceWachsen) {
            ((RessourceWachsen) statischesObjekt).updateFruchtbarkeit(fruchtbarkeit);
        }*/
    }

    // LADEN //////////////////////////////////////////////////////////////////////////////////////

    public void lade() {
        boden = IdsBoden.get(feuchtigkeit, temperatur);
        updateKollision(kollisionComps, boden);
    }

    // KOLLISION //////////////////////////////////////////////////////////////////////////////////

    public void add(KollisionComp kollisionComp) {
        if (kollisionComp == this.bewegungszielComp.compKollisionLaufen) {
            return;
        }
        kollisionComps.add(kollisionComp);
        updateKollision(kollisionComps, boden);
    }

    public boolean remove(KollisionComp kollisionComp) {
        if (kollisionComps.remove(kollisionComp)) {
            updateKollision(kollisionComps, boden);
            return true;
        }
        return false;
    }

    private void updateKollision(Set<KollisionComp> kollisionComps, Boden boden) {
        kollisionBauen = feuchtigkeit >= KonstantenBalance.BODEN_FEUCHTIGKEIT_PROZENT_BAUEN_KOLLISION;
        float laufenMult = boden.getLaufenMult();

        for (KollisionComp kollisionComp : kollisionComps) {
            if (kollisionComp instanceof KollisionCompBauen) {
                kollisionBauen = true;
            } else if (kollisionComp instanceof LaufenKollisionComp) {
                laufenMult *= ((LaufenKollisionComp) kollisionComp).getLaufenMult();
            }
        }
        kollisionLaufen = laufenMult == 0;

        for (Zelle z : gettAngrenzendeZellen()) {
            z.pruefeUmringtVonKoll();
        }

        bewegungszielComp.compKollisionLaufen.setLaufenMult(laufenMult);
    }

    protected void pruefeUmringtVonKoll() {
        List<Zelle> nichtDiagonalAngrenzedeZellen = gettNichtDiagonalAngrenzendeZellen();
        umringtVonKollisionBauen = true;
        for (Zelle z : nichtDiagonalAngrenzedeZellen) {
            if (!z.isKollisionBauen()) {
                umringtVonKollisionBauen = false;
                break;
            }
        }
        umringtVonKollisionLaufen = true;
        for (Zelle z : nichtDiagonalAngrenzedeZellen) {
            if (!z.isKollisionLaufen()) {
                umringtVonKollisionLaufen = false;
                break;
            }
        }
    }

    /*// BESITZER UND EIGENTUEMER ///////////////////////////////////////////////////////////////////

    public void setEigentuemer(Eigentuemer eigentuemer) {
        this.eigentuemer = eigentuemer;
        eigentuemer.getEigentumListe().connectTo(this);
    }

    public Eigentuemer gettEigentuemer() {
        return eigentuemer;
    }

    public void settBesitzer(ZivilObjekt besitzer_NL) {
        if ((besitzer instanceof GebBerufFarm
                && ((GebBerufFarm) besitzer).getBewirtschafteteZellenListe().contains(this))) {
            throw new IllegalArgumentException("Eine Farm besitzt diese Zelle bereits!");
        }
        /*if ((besitzer != null && this.besitzer.getAufZellenListe().contains(this)
                && besitzer_NL != null)) {
            throw new IllegalArgumentException();
        }

        if (this.besitzer != null && this.besitzer instanceof GebBerufFarm) {
            ((GebBerufFarm) besitzer).getBewirtschafteteZellenListe().remove(this);
        }
        /*if (besitzer_NL != null && besitzer_NL instanceof GebBeruf) {
            ((GebBeruf) besitzer_NL).getBewirtschafteteZellenListe().addFrei(this);
        }

        this.besitzer = besitzer_NL;
    }*/

    // KOMPLEXE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public List<Zelle> gettAngrenzendeZellen() {
        Vector2 indizes = GeometryUtils.gettIndizes(position);
        int x = (int) indizes.x;
        int y = (int) indizes.y;
        List<Zelle> angrenzendeZellenListe = new ArrayList<>();
        for (int xCount = x - 1; xCount <= x + 1; xCount++) {
            for (int yCount = y - 1; yCount <= y + 1; yCount++) {
                if (!(xCount == x && yCount == y)) {
                    Zelle z = worldMap.gett(xCount, yCount);
                    if (z != null) {
                        angrenzendeZellenListe.add(z);
                    }
                }
            }
        }
        return angrenzendeZellenListe;
    }

    public List<Zelle> gettNichtDiagonalAngrenzendeZellen() {
        Vector2 indizes = GeometryUtils.gettIndizes(position);
        int x = (int) indizes.x;
        int y = (int) indizes.y;
        List<Zelle> nichtDiagonalangrenzendeZellenListe = new ArrayList<>();
        Zelle z = worldMap.gett(x+1, y);
        if (z != null) {
            nichtDiagonalangrenzendeZellenListe.add(z);
        }
        z = worldMap.gett(x-1, y);
        if (z != null) {
            nichtDiagonalangrenzendeZellenListe.add(z);
        }
        z = worldMap.gett(x, y+1);
        if (z != null) {
            nichtDiagonalangrenzendeZellenListe.add(z);
        }
        z = worldMap.gett(x, y-1);
        if (z != null) {
            nichtDiagonalangrenzendeZellenListe.add(z);
        }
        return nichtDiagonalangrenzendeZellenListe;
    }

    public Vector2 getPosition() {
        return position;
    }

    public float gettLaufenMult() {
        return bewegungszielComp.compKollisionLaufen.getLaufenMult();
    }

    // AUF ZELLE //////////////////////////////////////////////////////////////////////////////////

    public Set<Entity> onCellEntities() {
        Set<Entity> entities = new HashSet<>();
        for (KollisionComp kollisionComp : kollisionComps) {
            entities.add(kollisionComp.entity);
        }
        return entities;
    }

    public Set<KollisionComp> onCellKollisionComps() {
        return new HashSet<>(kollisionComps);
    }

    // EINFACHE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public boolean isUmringtVonKollisionBauen() {
        return umringtVonKollisionBauen;
    }

    public boolean isUmringtVonKollisionLaufen() {
        return umringtVonKollisionLaufen;
    }

    public boolean isKollisionBauen() {
        return kollisionBauen;
    }

    public boolean isKollisionLaufen() {
        return kollisionLaufen;
    }

    public boolean isKollision() {
        return kollisionBauen || kollisionLaufen;
    }

    public float getFruchtbarkeit() {
        return fruchtbarkeit;
    }

    public Boden getBoden() {
        return boden;
    }

    public RenderComp getRenderComp() {
        return renderComp;
    }
}
