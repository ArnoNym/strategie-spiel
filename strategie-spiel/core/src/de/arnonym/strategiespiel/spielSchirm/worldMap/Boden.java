package de.arnonym.strategiespiel.spielSchirm.worldMap;

import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class Boden {
    private final int id;

    // Vom Boden bereit gestellt um eine Zelle zu beschreiben
    // - Einmailig beim erstellen der Map
    private final float gegebeneFruchtbarkeit; //0 < X < 1
    // - Bei jeder Veraenderung des Bodens
    private final float laufenMult; //0 < X < unendlich. Je hoeher desto einfacher
    private final String beschreibung;
    private final TextureRegion textureRegion;

    // Verwendet um fuer eine Zelle zu ermitteln welcher Boden auf ihr besteht
    private final float feuchtigkeit; //0 < X < 1
    private final float temperatur; //0 < X < 1

    public Boden(int id, float gegebeneFruchtbarkeit, float feuchtigkeit, float temperatur,
                 float laufenMult, String beschreibung, TextureRegion textureRegion) {
        this.id = id;
        this.gegebeneFruchtbarkeit = gegebeneFruchtbarkeit;
        this.feuchtigkeit = feuchtigkeit;
        this.temperatur = temperatur;
        this.laufenMult = laufenMult;
        this.beschreibung = beschreibung;
        this.textureRegion = textureRegion;
    }

    public int getId() {
        return id;
    }

    public float getGegebeneFruchtbarkeit() {
        return gegebeneFruchtbarkeit;
    }

    public float getFeuchtigkeit() {
        return feuchtigkeit;
    }

    public float getTemperatur() {
        return temperatur;
    }

    public float getLaufenMult() {
        return laufenMult;
    }

    public String getBeschreibung() {
        return beschreibung;
    }

    public TextureRegion getTextureRegion() {
        return textureRegion;
    }
}
