package de.arnonym.strategiespiel.spielSchirm.worldMap.creator;

import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;

import java.util.HashMap;

import de.arnonym.strategiespiel.spielSchirm.World;
import de.arnonym.strategiespiel.spielSchirm.worldMap.WorldMap;
import de.arnonym.strategiespiel.spielSchirm.worldMap.Zelle;

public class CreatorBoden extends Creator {
    private final World world;
    private final WorldMap worldMap;
    private final HashMap<Integer, HashMap<Integer, Zelle>> xHashMap;

    public CreatorBoden(World world, WorldMap worldMap,
                        HashMap<Integer, HashMap<Integer, Zelle>> xHashMap,
                        TiledMapTileLayer bodenLayer) {
        super(world, worldMap, bodenLayer);
        this.world = world;
        this.worldMap = worldMap;
        this.xHashMap = xHashMap;
    }

    @Override
    public void create(int left, int bottom) {
        for (int x = 0; x < width; x++) {
            HashMap<Integer, Zelle> yHashMap = new HashMap<>();
            for (int y = 0; y < height; y++) {
                yHashMap.put(y, null);
            }
            xHashMap.put(x, yHashMap);
        }

        super.create(left, bottom);
    }

    @Override
    protected void createSingle(int x, int y) {
        TiledMapTileLayer.Cell bodenCell = layer.getCell(x, y);
        Zelle zelle = new Zelle(world, worldMap, null, x, y, bodenCell.getTile());
        put(x, y, zelle);
    }

    public void put(int x, int y, Zelle zelle) {
        HashMap<Integer, Zelle> yHashMap = xHashMap.get(x);
        yHashMap.put(y, zelle);
    }
}
