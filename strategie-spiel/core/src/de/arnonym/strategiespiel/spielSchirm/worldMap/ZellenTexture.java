package de.arnonym.strategiespiel.spielSchirm.worldMap;

import com.badlogic.gdx.graphics.g2d.TextureRegion;

import de.arnonym.strategiespiel.framework.werkzeuge.textures.Texture;
import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenOptik;
import de.arnonym.strategiespiel.spielSchirm.Enums;

public class ZellenTexture extends Texture {
    private final Boden boden;
    private TextureRegion textureRegion;

    public ZellenTexture(Boden boden) {
        super(false, false, Enums.EnumAddTexture.ERSETZE_WENN_UNGLEICH, -1,
                KonstantenOptik.ZELLE_FUNDAMENT);
        this.boden = boden;
    }

    @Override
    public TextureRegion get() {
        if (textureRegion == null) {
            return textureRegion = boden.getTextureRegion();
        }
        return textureRegion;
    }

    @Override
    protected float gettRightWidth(float leftWidth) {
        return leftWidth;
    }

    @Override
    protected float gettTopHeight(float bottomHeight) {
        return bottomHeight;
    }
}
