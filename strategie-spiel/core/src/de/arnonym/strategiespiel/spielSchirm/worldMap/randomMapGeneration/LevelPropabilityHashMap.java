package de.arnonym.strategiespiel.spielSchirm.worldMap.randomMapGeneration;

public class LevelPropabilityHashMap {
/*
    private IntIntHashMap shouldFrequencyHM = new IntIntHashMap();
    private HashMap<Integer, Integer> occurenceHM = new HashMap<>();
    private int sum = 0;
    
    public LevelPropabilityHashMap() {

    }

    public void lade() {
        shouldFrequencyHM.valueSumToOne();
        for (int key : shouldFrequencyHM.keySet()) {
            occurenceHM.put(key, 0);
        }
    }

    public void put(int terrainId, float percentageOfMap) {
        shouldFrequencyHM.put(terrainId, (int) percentageOfMap);//TODO Das wurde alles nicht beim aendern von Float auf Integer mitgeaendert
    }

    public IntIntHashMap propabilityHashMap() {
        IntIntHashMap propabilityHashMap = new IntIntHashMap();
        for (Map.Entry<Integer, Integer> entry : shouldFrequencyHM.entrySet()) { //TODO Das wurde alles nicht beim aendern von Float auf Integer mitgeaendert
            int key = entry.getIndex();
            float value = entry.gettValue();

            float propability = 1;
            int occurences = occurenceHM.toList(key);
            if (occurences > 0) {
                float currentPropability = (float)occurences / (float)sum;
                propability = value / currentPropability;
                //IdentityConnectable.out.println(currentPropability);
            }
            propability = (float) Math.pow(propability, 0.5);
            //IdentityConnectable.out.println("\n"+key+": "+propability);
            propabilityHashMap.put(key,(int) propability);//TODO Das wurde alles nicht beim aendern von Float auf Integer mitgeaendert
        }
        propabilityHashMap.valueSumToOne();
        return propabilityHashMap;
    }

    public void addOne(int id) {
        sum += 1;
        int oldValue = occurenceHM.toList(id);
        occurenceHM.put(id, oldValue + 1);
    }

    // EINFACHE GETTER UND SETTER /////////////////////////////////////////////////////////////////*/
}