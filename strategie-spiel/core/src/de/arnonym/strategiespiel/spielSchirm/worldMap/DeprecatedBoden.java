package de.arnonym.strategiespiel.spielSchirm.worldMap;

import java.io.Serializable;

public class DeprecatedBoden implements Serializable {
    private final int id;
    private final float feuchtigkeit; //0 < X < 1
    private final float temperatur; //0 < X < 1
    private final String beschreibung;

    public DeprecatedBoden(int id, float feuchtigkeit, float temperatur, String beschreibung) {
        this.id = id;
        this.feuchtigkeit = feuchtigkeit;
        this.temperatur = temperatur;
        this.beschreibung = beschreibung;
    }

    //TODO WIe bei Ressurcen und Gebauden einfuegen, dass die Texture Region neu geladen wird nachdem gespeichertUndGeladen wurde



    // EINFACHE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public int getId() {
        return id;
    }

    public String getBeschreibung() {
        return beschreibung;
    }

    public float getFeuchtigkeit() {
        return feuchtigkeit;
    }

    public float getTemperatur() {
        return temperatur;
    }

}
