package de.arnonym.strategiespiel.spielSchirm.worldMap.randomMapGeneration;

public class WorldGeneration {
    /*private TiledMapTileSet tileSet;
    private TiledMapTileLayer tiledMapSpawnenLayer;
    private int mapWidth;
    private int mapHeight;

    private final static int firstGid = 1; //Die Id in Tiled ist nicht die Id im Programm. Sie wird um das firstGid erweitert. Die auch etwas mit den vorherigen Tilesets zu tun hat

    private final static int nullId = 0;
    private final static int treeId = 12 + firstGid;
    private final static int berryId = 13 + firstGid;

    private List<Vector3> nextGeneratedList = new ArrayList<>();
    private List<Vector3> generatedList = new ArrayList<>();

    private Random random = new Random();

    private LevelPropabilityHashMap levelPropabilityHashMap;

    private HashMap<Integer, IntIntHashMap> propabilityHashMapHashMap = new HashMap<>();

    public WorldGeneration(TiledMapTileSet tileSet, TiledMapTileLayer tiledMapLayer,
                           int mapWidth, int mapHeight,
                           LevelPropabilityHashMap levelPropabilityHashMap) {

        float size = 10; //TODO Konstanten bzw anpassbar machen

        /* BEIM WECHSEL VON FLAOT AUF INT NICHT MITGEAENDERT
        IntIntHashMap nullPHM = new IntIntHashMap();
        nullPHM.put(nullId, 2f * size);
        nullPHM.put(treeId, 1f);
        nullPHM.put(berryId, 1f);
        nullPHM.valueSumToOne();
        propabilityHashMapHashMap.put(nullId, nullPHM);

        IntIntHashMap woodPHM = new IntIntHashMap();
        woodPHM.put(nullId, 1f);
        woodPHM.put(treeId, 1f * size);
        woodPHM.valueSumToOne();
        propabilityHashMapHashMap.put(treeId, woodPHM);

        IntIntHashMap berryPHM = new IntIntHashMap();
        berryPHM.put(nullId, 1f);
        berryPHM.put(berryId, 1f * size);
        berryPHM.valueSumToOne();
        propabilityHashMapHashMap.put(berryId, berryPHM);

        // BEGINN EIGENTLICHER KONSTRUKTOR //

        this.tileSet = tileSet;
        this.tiledMapSpawnenLayer = tiledMapLayer;
        this.mapWidth = mapWidth;
        this.mapHeight = mapHeight;
        levelPropabilityHashMap.lade();
        this.levelPropabilityHashMap = levelPropabilityHashMap;
        //levelPropabilityHashMap.put(nullId, mapWidth * mapHeight);

        int j;
        int i;
        for (int z=1; z <= 1; z++) {
            j = random.nextInt(mapWidth);
            i = random.nextInt(mapHeight);
            generatedList.addFrei(new Vector3(j, i, -1));
        }

        // AUSFUEHREN DER GENERATION //

        useGeneratedList();
    }

    private void useGeneratedList() {
        int aa = 0;
        do {
            for (Vector3 v : generatedList) {
                newCell((int)v.x, (int)v.y, (int)v.z);
            }
            if (nextGeneratedList.isEmpty()) {
                return;
            }
            generatedList = nextGeneratedList;
            nextGeneratedList = new ArrayList<>();
            IdentityConnectable.out.println(aa);
            aa += 1;
        } while (!generatedList.isEmpty() && aa < 15);
    }

    private void newCell(int j, int i, Integer originTileId_NL) {
        TiledMapTileLayer.Cell cell = new TiledMapTileLayer.Cell();

        int tileId = createId(originTileId_NL);
        TiledMapTile tile = tileSet.getTile(tileId);

        if (tile != null) {
            cell.setTile(tile);
        }
        tiledMapSpawnenLayer.setCell(j, i, cell); //TODO Leere Zellen erlauben oder nicht??

        newCellsAroundOrigin(j, i, tileId);
    }

    private void newCellsAroundOrigin(int jOrigin, int iOrigin, int originTileId) {
        addNewCellToNextList(jOrigin + 1, iOrigin, originTileId);
        addNewCellToNextList(jOrigin - 1, iOrigin, originTileId);
        addNewCellToNextList(jOrigin, iOrigin + 1, originTileId);
        addNewCellToNextList(jOrigin, iOrigin - 1, originTileId);
    }
    private void addNewCellToNextList(int j, int i, int originTileId) {
        if (j < 0 || i < 0
                || j > mapWidth || i > mapHeight
                || tiledMapSpawnenLayer.getCell(j, i) != null) {
            IdentityConnectable.out.println("JA!");
            return;
        }
        nextGeneratedList.addFrei(new Vector3(j, i, originTileId));
    }

    private int createId(Integer originTileId_NL) {
        if (originTileId_NL == null) {
            originTileId_NL = nullId;
        }

        int createTileId = calculateNextTileId(originTileId_NL);
        levelPropabilityHashMap.addOne(createTileId);
        return createTileId;
    }

    private int calculateNextTileId(int originTileId) {
        IntIntHashMap propabilityHashmap = new IntIntHashMap();
        IntIntHashMap terrainPropabilityHashmap = terrainPropabilityHashmap(originTileId);

        /*for (Map.Entry<Integer, Float> entry : levelPropabilityHashMap.propabilityHashMap().entrySet()) {
            int id = entry.getIndex();

            float levelMult = 1f;//entry.gettValue();

            Float terrainMult = 0F;
            if (terrainPropabilityHashmap != null) {
                terrainMult = terrainPropabilityHashmap.toList(id);
                if (terrainMult == null) { //TODO Irgendwie unelegant
                    terrainMult = 0F;
                }
            }

            propabilityHashmap.put(id, levelMult * terrainMult);
        }

        propabilityHashmap.removeValueIsSmallerOrZero();
        propabilityHashmap.valueSumToOne();

        float randomFloat = random.nextFloat();

        float sumPropabilities = 0;
        for (Map.Entry<Integer, Float> entry : propabilityHashmap.entrySet()) {
            sumPropabilities += entry.gettValue();
            if (randomFloat <= sumPropabilities) {
                return entry.getIndex();
            }
        }
        return nullId;
    }

    private IntIntHashMap terrainPropabilityHashmap(int id) {
        for (Map.Entry<Integer, IntIntHashMap> entry : propabilityHashMapHashMap.entrySet()) {
            if (entry.getIndex() == id) {
                return entry.gettValue();
            }
        }
        return null;
    }*/
}