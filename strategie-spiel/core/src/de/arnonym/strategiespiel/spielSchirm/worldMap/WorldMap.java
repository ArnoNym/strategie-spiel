package de.arnonym.strategiespiel.spielSchirm.worldMap;

import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.math.Vector2;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import de.arnonym.strategiespiel.spielSchirm.World;
import de.arnonym.strategiespiel.spielSchirm.balancing.Konstanten;
import de.arnonym.strategiespiel.spielSchirm.umwelt.ressourcen.RessourcenFactory;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.OriginGeldComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.herrschaft.EigentuemerComp;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.SysRender;
import de.arnonym.strategiespiel.spielSchirm.worldMap.creator.CreatorBoden;
import de.arnonym.strategiespiel.spielSchirm.worldMap.creator.CreatorEntities;
import de.arnonym.strategiespiel.framework.werkzeuge.collections.UpdateSet;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.GeometryUtils;

public class WorldMap extends Entity implements Serializable {
    public final World world;
    public final SysRender sysRendern;

    public final RessourcenFactory ressourcenFactory;
    private final HashMap<Integer, HashMap<Integer, Zelle>> xHashMap = new HashMap<>();
    private final UpdateSet<Zelle> updateSet;

    private boolean gridZeichnen = false;

    public WorldMap(World world, SysRender sysRendern) {
        super(world.manager);
        this.world = world;
        this.sysRendern = sysRendern;
        this.ressourcenFactory = new RessourcenFactory(world);

        this.updateSet = new UpdateSet<Zelle>(Konstanten.UPDATE_LIST_SIZE, false, Zelle::update);
    }

    public void createBoden(TiledMapTileLayer tiledMapBodenLayer, int left, int bottom) {
        new CreatorBoden(world, this, xHashMap, tiledMapBodenLayer).create(left, bottom);
    }

    public void createEntities(TiledMapTileLayer tiledMapBodenLayer, int left, int bottom) {
        new CreatorEntities(world, this, tiledMapBodenLayer).create(left, bottom);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////

    public void update(float delta) {
        updateSet.update(delta);
        /*for (int i = 1; i < Math.pow(xHashMap.size(), 2) * 0.01; i++) { // Es wird angenommen, dass das Spielfeld ein Rechteck ist //TODO Konstante 0.001 = Anzahl Zellen Updaten
            HashMap<Integer, Zelle> yHashMap = xHashMap.toList((int) (xHashMap.size() * ThreadLocalRandom.current().nextFloat()));
            Zelle zelle = yHashMap.toList((int) (yHashMap.size() * ThreadLocalRandom.current().nextFloat()));
            zelle.update(delta);
        }
        for (HashMap<Integer, Zelle> yHashMap : xHashMap.values()) {
            for (Zelle zelle : yHashMap.values()) {
                zelle.update(delta);
            }
        }*/
    }

    public void preufeVisible(float kameraLeft, float kameraRight, float kameraTop,
                              float kameraBottom) {
        for (HashMap<Integer, Zelle> yHashMap : xHashMap.values()) {
            for (Zelle zelle : yHashMap.values()) {
                zelle.getRenderComp().preufeVisible(kameraLeft, kameraRight, kameraTop,
                        kameraBottom);
            }
        }
    }

    // KOMPLEXE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public List<Zelle> gettBetroffeneZellenListe(Vector2 position, Vector2 fundament) {
        List<Zelle> zellenListe = new ArrayList<>();
        for (Vector2 index : GeometryUtils.gettIndizesListe(position, fundament)) {
            zellenListe.add(gett((int) index.x, (int) index.y));
        }
        return zellenListe;
    }

    public Zelle gett(Vector2 position) {
        Vector2 indizes = GeometryUtils.gettIndizes(position);
        return gett((int) indizes.x, (int) indizes.y);
    }

    public Zelle gett(int xIndex, int yIndex) {
        HashMap<Integer, Zelle> yHashMap = xHashMap.get(xIndex);
        return yHashMap == null ? null : yHashMap.get(yIndex);
    }

    private int gettMinInHashMap(HashMap<Integer, ?> hashMap) {
        int min = -Integer.MAX_VALUE;
        for (int i : hashMap.keySet()) { //TODO Nicht in Werkzeuge weil ich es nicht hinbekommen habe das KeySet zu uebergeben
            if (i < min) min = i;
        }
        return min;
    }

    private int gettMaxInHashMap(HashMap<Integer, ?> hashMap) {
        int max = -Integer.MAX_VALUE;
        for (int x : hashMap.keySet()) {
            if (x > max) max = x;
        }
        return max;
    }

    public Zelle gettFreieZelle(Vector2 inNaeheVonPosition, boolean freiBauen,
                                boolean freiLaufen) {
        /* Nicht fuer Zellen die null sind optimiert weil das im fertigen Spiel eigentlich nicht
        vorkommen soll */
        Vector2 keineBauenKollZelleIndizes = GeometryUtils.gettIndizes(inNaeheVonPosition);
        int x = (int) keineBauenKollZelleIndizes.x;
        int y = (int) keineBauenKollZelleIndizes.y;
        int left = x, right = x, down = y, up = y;
        boolean leftNull = false, rightNull = false, downNull = false, upNull = false;
        do {
            while (y > down - 1) {
                Zelle z = gett(x, y);
                if (z == null) {
                    if (!rightNull) {
                        downNull = true;
                    }
                } else if ((!freiBauen || !z.isKollisionBauen())
                        && (!freiLaufen || !z.isKollisionLaufen())) {
                    return z;
                }
                y -= 1;
            }
            down -= 1;

            while (x > left - 1) {
                Zelle z = gett(x, y);
                if (z == null) {
                    if (!downNull) {
                        leftNull = true;
                    }
                } else if ((!freiBauen || !z.isKollisionBauen())
                        && (!freiLaufen || !z.isKollisionLaufen())) {
                    return z;
                }
                x -= 1;
            }
            left -= 1;

            while (y < up + 1) {
                Zelle z = gett(x, y);
                if (z == null) {
                    if (!leftNull) {
                        upNull = true;
                    }
                } else if ((!freiBauen || !z.isKollisionBauen())
                        && (!freiLaufen || !z.isKollisionLaufen())) {
                    return z;
                }
                y += 1;
            }
            up += 1;

            while (x < right + 1) {
                Zelle z = gett(x, y);
                if (z == null) {
                    if (!upNull) {
                        rightNull = true;
                    }
                } else if ((!freiBauen || !z.isKollisionBauen())
                        && (!freiLaufen || !z.isKollisionLaufen())) {
                    return z;
                }
                x += 1;
            }
            right += 1;
        } while (!leftNull || !rightNull || !downNull || !upNull);
        System.out.println("WORLD MAP : FREI BAUEN ZELLE_UND_FLACHEN IS UNSICHTBAR"); //TODO Loeschen
        return null;
    }

    public List<Zelle> gettAlleZellenList() {
        List<Zelle> zelleList = new ArrayList<>(); /* Kein Groesse vorgeben, da WorldMap in Zukunft
        evtl nicht mehr symmetrisch */
        for (HashMap<Integer, Zelle> yHashMap : xHashMap.values()) {
            zelleList.addAll(yHashMap.values());
        }
        return zelleList;
    }

    // EINFACHE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public boolean isGridZeichnen() {
        return gridZeichnen;
    }

    public void setGridZeichnen(boolean gridZeichnen) {
        this.gridZeichnen = gridZeichnen;
    }
}
