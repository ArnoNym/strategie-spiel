package de.arnonym.strategiespiel.spielSchirm;

import java.util.LinkedList;

import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenBalance;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.OriginGeldComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.VariabelGeldComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.herrschaft.EigentuemerComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.lager.LagerComp;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.spielSchirm.bodenStapel.BodenStapelFactory;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.listSystems.privateListSystems.priceSystems.market.GegMarketPriceSys;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.lists.ausbeuten.AbzubauenEcsList;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.lists.ausbeuten.ErntenEcsList;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dorfbewohner.familie.Familie;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.EmploymentMatcher;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.matchers.SchuleMatcher;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.endstufe.Gegenstand;
import de.arnonym.strategiespiel.framework.werkzeuge.collections.hashMaps.IntIntHashMap;
import de.arnonym.strategiespiel.spielSchirm.balancing.IdsFamilien;
import de.arnonym.strategiespiel.spielSchirm.info.forschung.ForschungsstandList;

/**
 * Created by LeonPB on 07.04.2018.
 */

public class Dorf extends Entity {
    private final World world;

    public final EigentuemerComp eigentuemerComp;

    public final ForschungsstandList forschungsstandList = new ForschungsstandList();

    public final AbzubauenEcsList abzubauenListe;
    public final ErntenEcsList erntenListe;
    public final GegMarketPriceSys gegMarketPriceSystem = new GegMarketPriceSys();
    public final EmploymentMatcher employmentMatcher = new EmploymentMatcher(this);
    public final SchuleMatcher schuleMatcher = new SchuleMatcher(this);
    public final BodenStapelFactory bodenStapelFactory;

    private int gehalt = 10; // Gehalt fuer alle Arbeiten die fuer das Dorf ausgefuehrt werden // todo regler zum festlegen
    private boolean sozialhilfe = true; // Das Dorf rettet mit seinem Geld DBs vor dem Verhungern // todo regler zum festlegen

    public Dorf(World world) {
        super(world.manager);
        this.world = world;

        this.bodenStapelFactory = new BodenStapelFactory(world, this);

        OriginGeldComp compGeld = new OriginGeldComp(this, world.sysGeld,
                KonstantenBalance.SPIELER_STARTGELD);
        eigentuemerComp = new EigentuemerComp(this, compGeld);

        VariabelGeldComp compGeldVariabel = new VariabelGeldComp(this);
        EigentuemerComp geldEigentumComp = new EigentuemerComp(eigentuemerComp,this,
                compGeldVariabel);
        this.abzubauenListe = new AbzubauenEcsList(geldEigentumComp, geldEigentumComp);
        this.erntenListe = new ErntenEcsList(geldEigentumComp, geldEigentumComp);

        Familie familie = new Familie(world, this, IdsFamilien.FAMILIENMITGLIEDER_PARKER);
    }

    public void update (float delta) {
        gegMarketPriceSystem.update(delta);
        schuleMatcher.update(delta);
        employmentMatcher.update(delta);
    }

    // LISTEN UND HASHMAPS ZUSAMMENRECHNEN ////////////////////////////////////////////////////////

    public IntIntHashMap gettAlleGegenstaendeIdMengenHashMap(boolean verfuegbar, boolean legen,
                                                             boolean nehmen) {
        IntIntHashMap hm = new IntIntHashMap();
        for (LagerComp lagerComp : world.sysLager.toCollection(new LinkedList<>())) {
            for (Gegenstand g : lagerComp.gegenstaendeComp.gegenstandListe) {
                int id = g.gettId();
                int addMenge = g.gettMenge(verfuegbar, legen, nehmen);
                Integer oldMenge = hm.get(id);
                int neuMenge;
                if (oldMenge == null) {
                    neuMenge = addMenge;
                } else {
                    neuMenge = oldMenge + addMenge;
                }
                hm.put(id, neuMenge);
            }
        }
        return hm;
    }

    // EINFACHE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public int getGehalt() {
        return gehalt;
    }

    public boolean isSozialhilfe() {
        return sozialhilfe;
    }
}
