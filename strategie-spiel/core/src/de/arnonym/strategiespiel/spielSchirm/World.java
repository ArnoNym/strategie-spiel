package de.arnonym.strategiespiel.spielSchirm;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import de.arnonym.strategiespiel.Strategiespiel;
import de.arnonym.strategiespiel.spielSchirm.balancing.Konstanten;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.listSystems.privateListSystems.HabitalwerteSys;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.listSystems.privateListSystems.SysBaugrube;
import de.arnonym.strategiespiel.framework.stp.StpSystem;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.listSystems.privateListSystems.WohnortSystem;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.wohnen.WohnenMatcher;
import de.arnonym.strategiespiel.spielSchirm.ui.aufsteigendeSymbole.AufsteigendesSymbolFactory;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.listSystems.privateListSystems.SysGeld;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.SysAltern;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.listSystems.privateListSystems.SysBewegung;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dorfbewohner.familie.PartnerMatcher;
import de.arnonym.strategiespiel.spielSchirm.umwelt.ressourcen.SysResVermehren;
import de.arnonym.strategiespiel.spielSchirm.umwelt.SysVergammeln;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.listSystems.privateListSystems.SysWirtschaftsdatenManager;
import de.arnonym.strategiespiel.framework.werkzeuge.Manager;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.bewegen.pathfinding.Path;
import de.arnonym.strategiespiel.spielSchirm.ui.Blaupause;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.listSystems.privateListSystems.SysEinkommen;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.listSystems.privateListSystems.SysKlick;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.SysKollision;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.SysRender;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.listSystems.privateListSystems.SysAbbaubar;
import de.arnonym.strategiespiel.spielSchirm.umwelt.ressourcen.SysFreuchte;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.listSystems.privateListSystems.SysLager;
import de.arnonym.strategiespiel.spielSchirm.ui.IngameCursorManager;
import de.arnonym.strategiespiel.spielSchirm.ui.ZellenMarkierungRenderer;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.ImLevelHUD;
import de.arnonym.strategiespiel.spielSchirm.ui.DiverseInputs;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.chat.FensterChat;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.menue.IngameTaskLeiste;
import de.arnonym.strategiespiel.spielSchirm.worldMap.WorldMap;
import de.arnonym.strategiespiel.framework.werkzeuge.IngameKamera;
import de.arnonym.strategiespiel.framework.werkzeuge.pause.DeltaPause;
import de.arnonym.strategiespiel.spielSchirm.balancing.IdsBoden;
import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenOptik;

/**
 * Created by LeonPB on 07.04.2018.
 */

public class World implements java.io.Serializable {
    public static SpielSchirm spielSchirm;
    private static SpriteBatch spriteBatch;

    public final Blaupause blaupause;

    public final WohnortSystem wohnortSystem = new WohnortSystem();
    public final WohnenMatcher wohnenMatcher = new WohnenMatcher();
    public final Manager manager = new Manager(this);
    public final HabitalwerteSys habitalwerteSystem = new HabitalwerteSys();
    public final SysResVermehren sysResVermehren = new SysResVermehren();
    public final SysRender sysRender = new SysRender(this);
    public final SysAltern sysAltern = new SysAltern();
    public final SysBaugrube sysBaugrube = new SysBaugrube();
    public final SysBewegung sysBewegung = new SysBewegung();
    public final SysAbbaubar sysAbbaubar = new SysAbbaubar();
    public final SysFreuchte sysFreuchte = new SysFreuchte();
    public final SysKlick sysKlick = new SysKlick(this);
    public final SysKollision sysKollision = new SysKollision(this);
    public final StpSystem stpSystem = new StpSystem();
    public final SysLager sysLager = new SysLager();
    public final SysEinkommen sysEinkommen = new SysEinkommen();
    public final SysVergammeln sysVergammeln = new SysVergammeln();
    public final SysGeld sysGeld = new SysGeld();
    public final SysWirtschaftsdatenManager sysWirtschaftsdatenManager = new SysWirtschaftsdatenManager();
    public final PartnerMatcher partnerMatcher = new PartnerMatcher();
    public final AufsteigendesSymbolFactory aufsteigendesSymbolFactory;

    private static Viewport viewport;
    private static Viewport overlayViewport;
    private static IngameKamera ingameKamera;
    private static DiverseInputs diverseInputs; //Allgemeiner Input, der nicht in worldInputProcessor oder GUIStage aufgefangen wird
    //private static Blaupause blaupause; //Gebaude, dass noch an der Maus 'klebt' und noch platziert werden kann
    public static ZellenMarkierungRenderer zellenMarkierungRenderer = new ZellenMarkierungRenderer(); // zB Zelle unter dem Mauszeiger und belegte Flaechen beim bauen
    private static Stage guiStage; // Beinhaltet alle Fenster mit Informationen ueber die Welt
    private static FensterChat fensterChat; // Zeigt Nachrichten fuer den Spieler (zB Kind geboren). Am liebsten nie Static. Ist die Muehe aber nicht wert (Texture Region nicht speicherbar)
    private static InputMultiplexer inputMultiplexer; //Verteilt den Input auf verschiedene objekte
    private static IngameCursorManager ingameCursorManager;
    @Deprecated public static ImLevelHUD imLevelHUD;

    private DeltaPause ressourcenUpdatePause = new DeltaPause(1, Enums.EnumPauseStart.VORBEI); //TODO Konstante
    public final  Dorf spielerDorf;
    public final WorldMap worldMap;
    private float spielgeschwindigkeit = 1;
    private Path pathClass = null;

    public World(SpielSchirm spielSchirm, SpriteBatch spriteBatch){
        this.spielSchirm = spielSchirm;
        this.spriteBatch = spriteBatch;
        Skin skin = Strategiespiel.skin;

        // UI
        viewport = new ExtendViewport(KonstantenOptik.VIEWPORT_GROESSE,
                KonstantenOptik.VIEWPORT_GROESSE);
        overlayViewport = new ExtendViewport(KonstantenOptik.OVERLAY_VIWEPORT_GROESSE,
                KonstantenOptik.OVERLAY_VIWEPORT_GROESSE);
        ingameKamera = new IngameKamera(this, viewport);
        guiStage = new Stage(overlayViewport, spriteBatch);
        imLevelHUD = new ImLevelHUD(this);

        this.aufsteigendesSymbolFactory = new AufsteigendesSymbolFactory(manager, sysAltern,
                sysBewegung, sysRender);

        TiledMap tiledMap = new TmxMapLoader().load(KonstantenOptik.TILED_MAP_NAME);
        new IdsBoden(tiledMap.getTileSets().getTileSet(KonstantenOptik.TILED_TILESET_BODEN));
        TiledMapTileLayer tiledMapBodenLayer = (TiledMapTileLayer) tiledMap.getLayers().get(
                KonstantenOptik.TILED_LAYER_NAME_BODEN);
        TiledMapTileLayer tiledMapSpawnenLayer = (TiledMapTileLayer) tiledMap.getLayers()
                .get(KonstantenOptik.TILED_LAYER_NAME_SPAWNEN);
        worldMap = new WorldMap(this, sysRender);
        worldMap.createBoden(tiledMapBodenLayer, 0, 0);

        ingameCursorManager = new IngameCursorManager(this, Strategiespiel.cursorManger, worldMap,
                viewport, overlayViewport, zellenMarkierungRenderer, skin); // TODO World Map entfernen (erst testen ob es so funktionier)

        spielerDorf = new Dorf(this);

        fensterChat = new FensterChat(guiStage, viewport, overlayViewport, ingameCursorManager,
                Strategiespiel.skin);
        guiStage.addActor(new IngameTaskLeiste(this, guiStage, viewport, overlayViewport,
               ingameCursorManager, fensterChat, skin));
        blaupause = new Blaupause(viewport, ingameCursorManager, worldMap);
        diverseInputs = new DiverseInputs(this, spielerDorf, blaupause, viewport, overlayViewport,
                ingameKamera, ingameCursorManager, guiStage, sysAbbaubar, sysFreuchte);

        inputMultiplexer = new InputMultiplexer();
        inputMultiplexer.addProcessor(ingameCursorManager);
        inputMultiplexer.addProcessor(guiStage); //Reihenfolge beachten. So koennen Events von der GUI Stage behandelt werden und erreichen so ggf nie die anderen Stages
        inputMultiplexer.addProcessor(diverseInputs);
        inputMultiplexer.addProcessor(sysKlick);
        Gdx.input.setInputProcessor(inputMultiplexer);

        worldMap.createEntities(tiledMapSpawnenLayer, 0, 0);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////

    public void update(float delta){
        ingameKamera.update(delta);
        delta = delta * spielgeschwindigkeit;
        viewport.apply();
        overlayViewport.apply();

        wohnortSystem.update(delta);
        wohnenMatcher.update(delta);
        habitalwerteSystem.update(delta);
        sysAltern.update(delta);
        sysVergammeln.update(delta);
        sysResVermehren.update(delta);
        sysFreuchte.update(delta);
        sysBewegung.update(delta);
        stpSystem.update(delta);
        spielerDorf.update(delta);
        sysWirtschaftsdatenManager.update(delta);
        sysGeld.update(delta);

        worldMap.update(delta);

        guiStage.act();
        diverseInputs.update(delta);
        ingameCursorManager.update();
    }

    public void render(SpriteBatch spriteBatch, float delta){
        delta = delta * spielgeschwindigkeit;
        spriteBatch.setProjectionMatrix(viewport.getCamera().combined);
        spriteBatch.begin();

        sysRender.render(spriteBatch, delta); // todo Ist definitv der Bottleneck. Vielleicht in compRenderUnbeweglich das mit der HashMap ueberarbeiten

        if (Konstanten.DEVELOPER_VISUELL && pathClass != null) {
            pathClass.render(spriteBatch, zellenMarkierungRenderer);
        }
        zellenMarkierungRenderer.render(spriteBatch);

        imLevelHUD.render(spriteBatch);
        spriteBatch.end();

        guiStage.draw(); // Benoetigt eigenen Batch, da dieser in der Stage modifiziert wird
    }

    public void resize(int width, int height) {
        viewport.update(width, height, true);
        overlayViewport.update(width, height, true);
        ingameKamera.resize();
    }

    // GETTER FUER STATIC VARIABLES ///////////////////////////////////////////////////////////////

    public Viewport getViewport() {
        return viewport;
    }

    public Stage getGuiStage() {
        return guiStage;
    }

    public Viewport getOverlayViewport() {
        return overlayViewport;
    }

    public IngameCursorManager getIngameCursorManager() {
        return ingameCursorManager;
    }

    public DiverseInputs getDiverseInputs() {
        return diverseInputs;
    }

    // EINFACHE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public void setSpielgeschwindigkeit(float spielgeschwindigkeit) {
        this.spielgeschwindigkeit = spielgeschwindigkeit;
    }

    public float getSpielgeschwindigkeit() {
        return spielgeschwindigkeit;
    }

    public SysRender getSysRender() {
        return sysRender;
    }

    public IngameKamera getIngameKamera() {
        return ingameKamera;
    }

    //region TILED MAP SPAWNEN EXPERIMENT
    /*
        private void tiledMapSpawnen () {
        /*LevelPropabilityHashMap worldGenerationHashMap = new LevelPropabilityHashMap();
        worldGenerationHashMap.put(0, 20f);
        worldGenerationHashMap.put(13, 5f);
        worldGenerationHashMap.put(14, 1f);
        worldMap = new WorldGeneration(tiledMap.getTileSets().getTileSet("tileset_Spawnen"),
                tiledMapSpawnenLayer, mapWidth, mapHeight, worldGenerationHashMap);
        worldMap = null;*/

        /*int j = 0;
        int i = -1;
        for (int z = 0; z < mapWidth*2; z++) {
            if (j > i) {
                i += 1;
                for (int jj = 0; jj <= j; jj++) {
                    createId(jj, i);
                }
            } else {
                j += 1;
                for (int ii = 0; ii <= i; ii++) {
                    createId(j, ii);
                }
            }

            //tiledMapSpawnenLayer.setCell(j, i, cell));
            /*
            TiledMapTileLayer.Cell cell = tiledMapSpawnenLayer.getCell(j, i);
            if (cell != null) {
                if (cell.getTile().getProperties().containsKey("baumAltBasic")) {
                    ressourcenListe.addFrei(new ResMatHolz(this, GeometryUtils.ortoToIso(new Vector2(j * KonstantenAssets.TILE_ORTO_WIDTH_AND_HEIGHT, i * KonstantenAssets.TILE_ORTO_WIDTH_AND_HEIGHT))));
                } else if (cell.getTile().getProperties().containsKey("beeren")) {
                    ressourcenListe.addFrei(new ResNahBeeren(this, GeometryUtils.ortoToIso(new Vector2(j * KonstantenAssets.TILE_ORTO_WIDTH_AND_HEIGHT, i * KonstantenAssets.TILE_ORTO_WIDTH_AND_HEIGHT))));
                }
            }

        }*/

        /*FUKTIONIER MIT RENDER_ARRAY_LIST
        int laengsteSeite = Math.max(mapWidth, mapHeight); //Bei nicht gleichlangen Seiten ist der Code ineffizient aber er sollte funktionieren
        int isoSpalteN = laengsteSeite * 2 - 1;
        int ueberHaelfteHinaus = 0;
        for (int isoSpalte = 0; isoSpalte < isoSpalteN; isoSpalte++) {
            int zellen = isoSpalte + 1;
            if (zellen > laengsteSeite) {
                ueberHaelfteHinaus += 1;
            }
            zellen -= ueberHaelfteHinaus;
            for (int z = 0; z < zellen; z++) {
                int i = laengsteSeite - zellen + z;
                int j = z + ueberHaelfteHinaus;
                spawneZelle(j, i);
            }
        }
        /*FUNKTIONIERT ABER BENOETIGT, DASS FUER JEDEN BAUM DIE POSITION IM RENDER ARRAY MUEHSAM ERRECHNET WIRD
        for (int j = 0; j <= mapWidth; j++) {
            for (int i = mapHeight; i >= 0 ; i--) {
                spawneZelle(int j, int i)
            }
        }*/

    /*private void spawneZelle(int j, int i) {
        TiledMapTileLayer.Cell cell = tiledMapSpawnenLayer.getCell(j, i);
        if (cell != null) {
            Vector2 position = GeometryUtils.ortoToIso(new Vector2(j * KonstantenOptik.TILE_ORTO_WIDTH_AND_HEIGHT, i * KonstantenOptik.TILE_ORTO_WIDTH_AND_HEIGHT));
            Ressource ressource = null;
            if (cell.getTile().getProperties().containsKey("baumAltBasic")) {
                ressource = new Ressource(this, renderArrayList, IdsRessourcen.BAUM_TANNE_ID, position);
            } else if (cell.getTile().getProperties().containsKey("beeren")) {
                ressource = new Ressource(this, renderArrayList, IdsRessourcen.STRAUCH_BEEREN_ID, position);
            }
            worldOnlyClickStage.addActor(ressource);
            ressourcenListe.addFrei(ressource);
        }
    }
    */
    //endregion
}
