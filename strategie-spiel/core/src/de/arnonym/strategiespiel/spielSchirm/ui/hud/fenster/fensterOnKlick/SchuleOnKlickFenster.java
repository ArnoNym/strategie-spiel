package de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterOnKlick;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import java.util.ArrayList;
import java.util.List;

import de.arnonym.strategiespiel.spielSchirm.World;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.EmployerComp;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.VarEmployerComp;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.departments.bildung.TeachDepartmentComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.gegenstaende.GegenstaendeComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.name.NameComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.rendern.HashMapRenderComp;
import de.arnonym.strategiespiel.spielSchirm.info.muster.Info;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterOnKlick.content.AktivButton;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterOnKlick.content.AngestellteLabel;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterOnKlick.content.UnterrichtTable;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.labels.UpdateLabel;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.buttons.icons.Icon;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.buttons.icons.ForschungIcon;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterOnKlick.content.GegenstandTable;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterOnKlick.content.IconTable;
import de.arnonym.strategiespiel.spielSchirm.Assets;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.SpecialUtils;

public class SchuleOnKlickFenster extends OnKlickFenster {
    public SchuleOnKlickFenster(World world, Skin skin,
                                VarEmployerComp<Info> employerComp,
                                TeachDepartmentComp teachDepartmentComp,
                                GegenstaendeComp gegenstaendeComp,
                                NameComp nameComp,
                                HashMapRenderComp hashMapRenderComp) {
        super(world, skin, 300, 300, nameComp, hashMapRenderComp, employerComp);

        getContentTable().add(new UnterrichtTable(employerComp, teachDepartmentComp, skin)).expandX().fillX();

        Button unterrichtButton = new AktivButton(employerComp);
        getContentTable().add(unterrichtButton).right();

        SpecialUtils.leerzeile(getContentTable());

        Label label = new Label("scientists: ", skin);
        getContentTable().add(label).left();
        UpdateLabel updateLabel = new AngestellteLabel(employerComp, skin);
        getUpdateLabelList().add(updateLabel);
        getContentTable().add(updateLabel).expandX().right();

        getContentTable().row();

        IconTable iconTable = createForschungIconTable(employerComp);
        getUpdateTableList().add(iconTable);
        getContentTable().add(iconTable);

        SpecialUtils.leerzeile(getContentTable());

        GegenstandTable gegenstandTable = new GegenstandTable(ingameCursorManager, gegenstaendeComp,
                false, this);
        getContentTable().add(gegenstandTable);
        getUpdateTableList().add(gegenstandTable);
    }

    private IconTable createForschungIconTable(VarEmployerComp<Info> forschungEmployerComp) {
        List<Icon> iconList = new ArrayList<>();
        for (Info info : forschungEmployerComp.possibleProducts) {
            ForschungIcon forschungIcon = new ForschungIcon(world.spielerDorf,
                    SchuleOnKlickFenster.this, world.getIngameCursorManager().getAnzeigeFenster(),
                    forschungEmployerComp, info);
            iconList.add(forschungIcon);
        }
        return new IconTable(skin, 5, 0, 0, iconList.toArray(new Icon[iconList.size()]));
    }
}
