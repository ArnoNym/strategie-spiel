package de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterOnKlick;

/*public class FensterOnKlickGebBerufFabrik extends FensterOnKlick {
    public FensterOnKlickGebBerufFabrik(Level level, GebBerufHerstFabrik gebBerufHerstFabrik,
                                        String gebaudeBezeichnung) {
        super(level, gebBerufHerstFabrik, 320, 440, gebaudeBezeichnung);

        Label label = new Label("money: ", getSkin());
        getContentTable().handle(label).left();
        UpdateLabel updateLabel = new UpdateLabel(getSkin()) {
            @Override
            public String aktualisiereInformationenOverride() {
                return ""+((GebBerufHerstFabrik) getObjekt()).getGeld().getMengeVerfuegbar();
            }
        };
        getUpdateLabelList().handle(updateLabel);
        getContentTable().handle(updateLabel).expandX();
        updateLabel = new UpdateLabel(getSkin()) {
            @Override
            public String aktualisiereInformationenOverride() {
                return "+ "+gebBerufHerstFabrik.getWirtschaftsdatenManager().gettSummeEingang(IdsGegenstaende.GELD_ID, 5); // TODO Konstanten
            }
        };
        getUpdateLabelList().handle(updateLabel);
        getContentTable().handle(updateLabel).expandX();
        updateLabel = new UpdateLabel(getSkin()) {
            @Override
            public String aktualisiereInformationenOverride() {
                return "- "+gebBerufHerstFabrik.getWirtschaftsdatenManager().gettSummeAusgang(IdsGegenstaende.GELD_ID, 5); // TODO Konstanten
            }
        };
        getUpdateLabelList().handle(updateLabel);
        getContentTable().handle(updateLabel).expandX();

        getContentTable().row();
        getContentTable().handle(new Label(" ", getSkin())).row();

        label = new Label("produced percent: ", getSkin());
        getContentTable().handle(label).left();
        updateLabel = new UpdateLabel(getSkin()) {
            @Override
            public String aktualisiereInformationenOverride() {
                return ""+gebBerufHerstFabrik.getHergestelltProzent();
            }
        };
        getUpdateLabelList().handle(updateLabel);
        getContentTable().handle(updateLabel);

        getContentTable().row();
        getContentTable().handle(new Label(" ", getSkin())).row();

        label = new Label("products: ", getSkin());
        getContentTable().handle(label).left();

        getContentTable().row();

        List<IconFabrikProdukt> iconList = new ArrayList<>();
        for (int id : gebBerufHerstFabrik.getProduktIDs()) {
            iconList.handle(new IconFabrikProdukt(getIngameCursorManager(), this, gebBerufHerstFabrik, id));
        }
        IconTable iconTable = new IconTable(getSkin(), 5, 5, 5,
                iconList.toList(new IconFabrikProdukt[iconList.size()]));
        // Kein regelmaessiges Updaten noetig -> nicht in updateTableList
        getContentTable().handle(iconTable);

        getContentTable().row();
        getContentTable().handle(new Label(" ", getSkin())).row();

        GegenstandTable gegenstandTable = new GegenstandTable(getIngameCursorManager(),
                gebBerufHerstFabrik.getWirtschaftsdatenManager(), this);
        getUpdateTableList().handle(gegenstandTable);
        getContentTable().handle(gegenstandTable).colspan(100).fillX().expandX();
    }
}*/
