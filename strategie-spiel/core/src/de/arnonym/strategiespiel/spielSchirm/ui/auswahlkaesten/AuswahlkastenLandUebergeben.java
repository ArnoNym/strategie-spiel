package de.arnonym.strategiespiel.spielSchirm.ui.auswahlkaesten;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.viewport.Viewport;

import java.util.Collection;
import java.util.List;

import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.CellsComp;
import de.arnonym.strategiespiel.spielSchirm.ui.IngameCursorManager;
import de.arnonym.strategiespiel.spielSchirm.worldMap.WorldMap;
import de.arnonym.strategiespiel.spielSchirm.worldMap.Zelle;
import de.arnonym.strategiespiel.spielSchirm.Assets;

public class AuswahlkastenLandUebergeben extends Auswahlkasten<Zelle> {
    private final WorldMap worldMap;
    private final CellsComp cellsComp;

    public AuswahlkastenLandUebergeben(Viewport worldViewport,
                                       IngameCursorManager ingameCursorManager, WorldMap worldMap,
                                       CellsComp cellsComp) {
        super(worldViewport, ingameCursorManager,
                Assets.instance.tiledMapAssets.grid,
                Assets.instance.tiledMapAssets.durchsichtigGruen);
        this.worldMap = worldMap;
        this.cellsComp = cellsComp;
    }

    @Override
    protected Collection<Zelle> gettMoeglichkeitenList() {
        return worldMap.gettAlleZellenList();
    }

    @Override
    protected Collection<Zelle> getAusgewaehltList() {
        return cellsComp.cells();
    }

    @Override
    protected boolean pruefeZeichnen(Zelle zelle) {
        return zelle.getRenderComp().isVisible();
    }

    @Override
    protected void ausloesen_or(Zelle zelle, boolean loeschen) {
        if (loeschen) {
            cellsComp.removeCell(zelle);
        } else {
            cellsComp.addCell(zelle);
        }
    }

    @Override
    protected Vector2 gettPosition(Zelle zelle) {
        return zelle.getPosition();
    }

    @Override
    protected Vector2 gettGroesseInZellen(Zelle zelle) {
        return new Vector2(1, 1);
    }
}
