package de.arnonym.strategiespiel.spielSchirm.ui.hud.buttons.icons;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import de.arnonym.strategiespiel.spielSchirm.ui.IngameCursorManager;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.MoveableGroup;
import de.arnonym.strategiespiel.spielSchirm.Assets;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.TextureUtils;

public abstract class Icon extends Button {
    public static final TextureRegion blankTR = Assets.instance.iconAssets.iconBlank;
    private final IngameCursorManager ingameCursorManager;
    private final MoveableGroup parentFenster;
    public final int id;

    public Icon(IngameCursorManager ingameCursorManager, MoveableGroup parentFenster, int id) {
        super(new TextureRegionDrawable(blankTR));
        this.ingameCursorManager = ingameCursorManager;
        this.parentFenster = parentFenster;
        this.id = id;

        addListener(new ClickListener() {
            @Override
            public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
                if (pointer == -1) { // 0 ist Klicken
                    // TODO ingameCursorManager.getAnzeigeFenster().aktiviereHover(id, parentFenster);
                }
                super.enter(event, x, y, pointer, fromActor);
            }

            @Override
            public void exit(InputEvent event, float x, float y, int pointer, Actor toActor) {
                if (pointer == -1) { // 0 ist Klick loslassen
                    ingameCursorManager.getAnzeigeFenster().deaktiviereHover();
                }
                super.exit(event, x, y, pointer, toActor);
            }
        });
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        draw_or(batch, parentAlpha);
        /*if (isPressed()) {
            TextureUtils.zeichneTexturRegion((SpriteBatch) batch,
                    Assets.instance.iconAssets.iconRahmenKlick, getX(), getY(), 1, 1, false);
        }*/
    }

    protected abstract void draw_or(Batch batch, float parentAlpha);

    protected void renderRahmenKannKlicken(SpriteBatch spriteBatch) {
        TextureUtils.zeichneTexturRegion(spriteBatch,
                Assets.instance.iconAssets.iconRahmenKannKlicken, getX(), getY(), 1, 1, false);
    }

    protected void renderRahmenKreuz(SpriteBatch spriteBatch) {
        TextureUtils.zeichneTexturRegion(spriteBatch,
                Assets.instance.iconAssets.iconRahmenKreuz, getX(), getY(), 1, 1, false);
    }

    protected void renderWorldTextureRegion(SpriteBatch spriteBatch, TextureRegion worldTextureRegion) {
        float prefWidth = Icon.blankTR.getRegionWidth();
        float prefHeight = Icon.blankTR.getRegionHeight();
        TextureRegion textureRegion = worldTextureRegion;
        float actualWidth = textureRegion.getRegionWidth();
        float actualHeight = textureRegion.getRegionHeight();
        float shouldScaleX = prefWidth / actualWidth;
        float shouldScaleY = prefHeight / actualHeight;
        float scale = Math.min(shouldScaleX, shouldScaleY);
        Vector2 centeredPosition = TextureUtils.center(getX(), getY(),
                actualWidth * scale, actualHeight * scale, prefWidth, prefHeight);
        TextureUtils.zeichneTexturRegion(spriteBatch, textureRegion,
                centeredPosition.x, centeredPosition.y, scale, scale, false);
    }

    public int getId() {
        return id;
    }
}
