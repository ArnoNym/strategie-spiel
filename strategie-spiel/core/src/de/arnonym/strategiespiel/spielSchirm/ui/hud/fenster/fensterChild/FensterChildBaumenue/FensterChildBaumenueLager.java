package de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterChild.FensterChildBaumenue;

import com.badlogic.gdx.scenes.scene2d.ui.Skin;

import de.arnonym.strategiespiel.spielSchirm.Dorf;
import de.arnonym.strategiespiel.spielSchirm.World;
import de.arnonym.strategiespiel.spielSchirm.balancing.IdsGebaude;
import de.arnonym.strategiespiel.spielSchirm.gebaude.Lager.LagerFactory;
import de.arnonym.strategiespiel.spielSchirm.gebaude.Lager.LagerInfo;
import de.arnonym.strategiespiel.spielSchirm.ui.Blaupause;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.MoveableGroup;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.buttons.icons.IconGebaude;

public class FensterChildBaumenueLager extends FensterChildBaumenue {
    public FensterChildBaumenueLager(World world, MoveableGroup ausklappenParentMoveableGroup,
                                     final Blaupause blaupause, Skin skin) {
        super(world, blaupause, ausklappenParentMoveableGroup, skin, 200, 200,
                "warehouse building menue");

        Dorf spielerDorf = world.spielerDorf;
        LagerInfo lagerInfo = (LagerInfo) IdsGebaude.get(IdsGebaude.GEB_LAGER_MATERIAL_ID);
        IconGebaude iconGebaude = new IconGebaude(ausklappenParentMoveableGroup, this,
                blaupause, new LagerFactory(world, spielerDorf, lagerInfo));

        getContentTable().add(iconGebaude);

        /*IconGebaude iconGebaude = new IconGebaude(ausklappenParentMoveableGroup, this,
                blaupause,
                level.getSpielerDorf().gebaudeInfoList.gettDurchId(IdsGebaude.GEB_HAUS_EINFACH_ID), null);//level.getSpielerDorf().gebaudeInfoList.gettDurchId(IdsGebaude.GEB_LAGER_MATERIAL_ID), null/*TODO);
        getContentTable().handle(iconGebaude);

        iconGebaude = new IconGebaude(ausklappenParentMoveableGroup, this, blaupause,
                level.getSpielerDorf().gebaudeInfoList.gettDurchId(IdsGebaude.GEB_HAUS_EINFACH_ID), null);//level.getSpielerDorf().gebaudeInfoList.gettDurchId(IdsGebaude.GEB_LAGER_NAHRUNG_ID), null/*TODO);
        getContentTable().handle(iconGebaude);*/
    }
}
