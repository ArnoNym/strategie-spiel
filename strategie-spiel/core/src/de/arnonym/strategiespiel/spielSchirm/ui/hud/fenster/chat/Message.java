package de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.chat;

import com.badlogic.gdx.scenes.scene2d.ui.Label;

import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenOptik;

public class Message {
    private Label label;

    private float anzeigenSekunden = KonstantenOptik.CHAT_MESSAGE_ANZEIGE_DAUER;
    private boolean wirdAngezeigt;

    public Message(Label label) {
        this.label = label;
    }

    protected void act(float delta) {
        if (wirdAngezeigt) {
            anzeigenSekunden -= delta;
        }
        wirdAngezeigt = false;
    }

    // KOMPLEXE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    protected Label gettLabel() {
        wirdAngezeigt = true;
        return label;
    }

    protected boolean issFertig() {
        return anzeigenSekunden <= 0;
    }
}
