package de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterOnKlick;

import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

import de.arnonym.strategiespiel.framework.listener.DeletableStoryListener;
import de.arnonym.strategiespiel.framework.listener.EnumStory;
import de.arnonym.strategiespiel.framework.listener.StoryTeller;
import de.arnonym.strategiespiel.framework.werkzeuge.exceptions.OutOfBoundsException;
import de.arnonym.strategiespiel.spielSchirm.World;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.name.NameComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.rendern.HashMapRenderComp;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.Fenster;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.labels.UpdateLabel;
import de.arnonym.strategiespiel.spielSchirm.Assets;
import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenOptik;

public abstract class OnKlickFenster extends Fenster implements DeletableStoryListener {
    public final World world;
    private final HashMapRenderComp hashMapRenderComp;
    private boolean mausUeberObjekt = false;
    private boolean mausUeberThis = false;
    private boolean delete = false;

    public OnKlickFenster(World world, Skin skin, float width, float height,
                          NameComp nameComp, HashMapRenderComp hashMapRenderComp,
                          StoryTeller... storyTellers) {
        super(world.getGuiStage(), world.getOverlayViewport(), world.getViewport(),
                world.getIngameCursorManager(), skin,
                new NinePatch(Assets.instance.levelGuiAssets.fensterDickerRand,
                        KonstantenOptik.AUSWAHLKASTEN_ECKEN_BREITE,
                        KonstantenOptik.AUSWAHLKASTEN_ECKEN_BREITE,
                        KonstantenOptik.AUSWAHLKASTEN_ECKEN_HOEHE,
                        KonstantenOptik.AUSWAHLKASTEN_ECKEN_HOEHE), width, height,
                new UpdateLabel(skin) {
                    @Override
                    public String aktualisiereInformationenOverride() {
                        return nameComp.getName();
                    }
                }, true);
        this.world = world;
        this.hashMapRenderComp = hashMapRenderComp;

        nameComp.addListener(this);
        hashMapRenderComp.addListener(this);
        for (StoryTeller st : storyTellers) {
            try {
                st.addListener(this);
            }
            catch (ClassCastException e) {
                throw new OutOfBoundsException(st);
            }
        }

        /* MAUS OVER: WAR NICHT SO GEIL
        addListener(new ClickListener(){
            @Override
            public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
                mausUeberThis = true;
                super.enter(event, x, y, pointer, fromActor);
            }

            @Override
            public void exit(InputEvent event, float x, float y, int pointer, Actor toActor) {
                mausUeberThis = false;
                super.exit(event, x, y, pointer, toActor);;
            }
        });*/

        setVisible(false);
        settStandardposition(true);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public void act(float delta) {
        if (!isAngeklickt() && !mausUeberObjekt && !mausUeberThis) {
            if (isVisible()) {
                schliessen();
            }
            return;
        }
        if (isStandardposition()) {
            settPosition(hashMapRenderComp.positionComp.getPosition(), 0, 0);
        }
        super.act(delta);
    }

    @Override
    public void oeffnen(boolean angeklickt) {
        Vector2 p = hashMapRenderComp.getPositionComp().getPosition();
        float x = hashMapRenderComp.getMainTexture().rightWidth;
        float y = hashMapRenderComp.getMainTexture().topHeight;
        settPosition(p, x, y);
        // TODO objekt.setAusgewaehlt(true);
        super.oeffnen(angeklickt);
    }

    @Override
    public void schliessen() {
        //TODO objekt.setAusgewaehlt(false);
        mausUeberObjekt = false;
        mausUeberThis = false;
        super.schliessen();
    }

    // ECS ////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public void delete() {
        if (delete) return;
        delete = true;
        schliessen();
        remove();
    }

    @Override
    public boolean isDeleted() {
        return delete;
    }

    @Override
    public void react(StoryTeller teller, EnumStory story) {
        if (story.equals(EnumStory.DELETE)) {
            delete();
        }
    }

    // EINFACHE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public World getWorld() {
        return world;
    }

    public void setMausUeberObjekt(boolean mausUeberObjekt) {
        this.mausUeberObjekt = mausUeberObjekt;
    }
}
