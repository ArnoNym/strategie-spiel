package de.arnonym.strategiespiel.spielSchirm.ui.hud.buttons.icons;

import com.badlogic.gdx.graphics.g2d.Batch;

import de.arnonym.strategiespiel.spielSchirm.ui.IngameCursorManager;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.MoveableGroup;

public class IconSimple extends Icon {
    public IconSimple(IngameCursorManager ingameCursorManager, MoveableGroup parentFenster, int id) {
        super(ingameCursorManager, parentFenster, id);
    }

    @Override
    protected void draw_or(Batch batch, float parentAlpha) {

    }
}
