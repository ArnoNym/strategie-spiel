package de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterChild;

import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

import de.arnonym.strategiespiel.spielSchirm.Dorf;
import de.arnonym.strategiespiel.spielSchirm.World;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.MoveableGroup;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterOnKlick.content.GeldTable;
import de.arnonym.strategiespiel.spielSchirm.Enums;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.SpecialUtils;

public class FensterChildDorf extends FensterChild {

    private World world; // Damit keine Null-Pointer-Exception beim erstellen
    private Label label;

    public FensterChildDorf(World world, MoveableGroup ausklappenParentMoveableGroup, Skin skin) {
        super(world, ausklappenParentMoveableGroup, Enums.EnumRichtung.OBEN, skin, 500, 400,
                "village"); //TODO Dorf name
        this.world = world;

        getContentTable().row();

        GeldTable geldTable = new GeldTable(skin, world.spielerDorf.eigentuemerComp.geldComp);
        getContentTable().add(geldTable);
        getUpdateTableList().add(geldTable);

        SpecialUtils.leerzeile(getContentTable());

        label = new Label("TODO", skin);
        getContentTable().add(label).expand();
    }

    @Override
    public void act(float delta) {
        super.act(delta);

        label.setText(aktualisierteInformationen());
    }

    private String aktualisierteInformationen() {
        Dorf dorf = world.spielerDorf;

        int dorfGeld = dorf.eigentuemerComp.geldComp.getGeld().getMengeVerfuegbar();

        /*for (Familie f : dorf.getFamilienListe()) {
            gesamtesGeld += f.getGeld().getMengeVerfuegbar();
        }
        for (Baugrube b : dorf.getBaugrubenListe()) {
            if (dorfGeld != b.getGeld()) {
                gesamtesGeld += b.getGeld().getMengeVerfuegbar();
            }
        }
        for (Gebaude g : dorf.getGebaudeListe()) {
            if (dorfGeld != g.getGeld()) {
                gesamtesGeld += g.getGeld().getMengeVerfuegbar();
            }
        }*/

        /*int gesamteMenge = 0;
        for (Kollisionsobjekt k : level.getKollisionsobjektListe()) {
            for (Gegenstand g : k.getMusterGegListe()) {
                gesamteMenge += g.gettMenge();
            }
        }
        for (Familie f : dorf.getFamilienListe()) {
            gesamteMenge += mengeVefuegbar(f.getNahrungsTasche().getMusterGegListe());
            gesamteMenge += mengeVefuegbar(f.getSpielzeugTasche_nl().getMusterGegListe());
            for (Dorfbewohner d : f.getFamilienmitglieder()) {
                gesamteMenge += mengeVefuegbar(d.getMusterGegListe());
            }
        }*/

        String returnString ="\n\nGeld: " + dorfGeld;

        return returnString;
    }
}
