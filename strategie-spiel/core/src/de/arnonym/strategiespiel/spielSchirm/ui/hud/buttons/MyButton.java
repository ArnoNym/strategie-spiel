package de.arnonym.strategiespiel.spielSchirm.ui.hud.buttons;

import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.TimeUtils;

import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenOptik;

public class MyButton extends Button {

    private float doubleTapTime = KonstantenOptik.ZEIT_DOUBLE_TAP;
    private long firstTapTime = (long) -(doubleTapTime/MathUtils.nanoToSec);

    public MyButton(Skin skin) {
        super(skin);
    }

    public MyButton(Skin skin, String styleName) {
        super(skin, styleName);
    }

    public MyButton(Actor child, Skin skin, String styleName) {
        super(child, skin, styleName);
    }

    public MyButton(Actor child, ButtonStyle style) {
        super(child, style);
    }

    public MyButton(ButtonStyle style) {
        super(style);
    }

    public MyButton() {
    }

    public MyButton(Drawable up) {
        super(up);
    }

    public MyButton(Drawable up, Drawable down) {
        super(up, down);
    }

    public MyButton(Drawable up, Drawable down, Drawable checked) {
        super(up, down, checked);
    }

    public MyButton(Actor child, Skin skin) {
        super(child, skin);
    }

    public boolean isDoubleTap() {
        if (firstTapTime + doubleTapTime/MathUtils.nanoToSec > TimeUtils.nanoTime()) {
            firstTapTime = (long) -(doubleTapTime/MathUtils.nanoToSec); //Damit man nicht noch einen Doppelklick ausloest
            return true;
        }
        firstTapTime = TimeUtils.nanoTime();
        return false;
    }
}
