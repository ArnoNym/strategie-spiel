package de.arnonym.strategiespiel.spielSchirm.ui.hud.menue;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import java.util.ArrayList;
import java.util.List;

import de.arnonym.strategiespiel.spielSchirm.ui.Blaupause;
import de.arnonym.strategiespiel.spielSchirm.World;
import de.arnonym.strategiespiel.spielSchirm.ui.auswahlkaesten.AuswahlkastenAbbauen;
import de.arnonym.strategiespiel.spielSchirm.ui.auswahlkaesten.AuswahlkastenErnten;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.MoveableGroup;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.buttons.IconAusklappButton;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.AnzeigeFenster;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterChild.FensterChildBaumenue.FensterChildBaumenue;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterChild.FensterChildBaumenue.FensterChildBaumenueFabrik;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterChild.FensterChildBaumenue.FensterChildBaumenueForschung;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterChild.FensterChildBaumenue.FensterChildBaumenueHauser;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterChild.FensterChildBaumenue.FensterChildBaumenueLager;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterChild.FensterChildBaumenue.FensterChildBaumenueLandwirtschaft;
import de.arnonym.strategiespiel.spielSchirm.Assets;
import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenOptik;

public class MasterBaumenue extends MoveableGroup {
    private List<MoveableGroup> umPlatzKonkurrierendeFensterListe = new ArrayList<>(); /* Man
    koennte auch ueber die Children der Tabelle (die Buttons) auf die Fenster kommen. Dann ist das
    Fenster aber nicht emhr so leicht modifizierbar */

    public MasterBaumenue(World world, Blaupause blaupause,
                          AuswahlkastenAbbauen auswahlkastenAbbauen,
                          AuswahlkastenErnten auswahlkastenErnten, Skin skin) {
        super(world.getGuiStage(), world.getOverlayViewport(), world.getViewport(),
                world.getIngameCursorManager(), skin, -1, -1);
        AnzeigeFenster anzeigeFenster = ingameCursorManager.getAnzeigeFenster();

        schliessen();

        Table table = new Table(skin);
        this.addActor(table);
        table.setFillParent(true);
        table.setBackground(new NinePatchDrawable(new NinePatch(Assets.instance.levelGuiAssets
                .auswahlKastenA, KonstantenOptik.AUSWAHLKASTEN_ECKEN_BREITE,
                KonstantenOptik.AUSWAHLKASTEN_ECKEN_BREITE,
                KonstantenOptik.AUSWAHLKASTEN_ECKEN_HOEHE,
                KonstantenOptik.AUSWAHLKASTEN_ECKEN_HOEHE)));

        int width = 0;
        int height = 1;

        TextureRegion r = Assets.instance.iconAssets.iconHaus;
        FensterChildBaumenue fcb = new FensterChildBaumenueHauser(world, this, blaupause, skin);
        table.add(new IconAusklappButton(this, fcb, umPlatzKonkurrierendeFensterListe, r));
        umPlatzKonkurrierendeFensterListe.add(fcb);
        width += 1;

        r = Assets.instance.iconAssets.iconLagerMaterialEinfach;
        fcb = new FensterChildBaumenueLager(world, this, blaupause, skin);
        table.add(new IconAusklappButton(this, fcb, umPlatzKonkurrierendeFensterListe, r));
        umPlatzKonkurrierendeFensterListe.add(fcb);
        width += 1;

        r = Assets.instance.iconAssets.iconFabrik;
        fcb = new FensterChildBaumenueFabrik(world, blaupause, this, skin); //TODO
        table.add(new IconAusklappButton(this, fcb, umPlatzKonkurrierendeFensterListe, r));
        umPlatzKonkurrierendeFensterListe.add(fcb);
        width += 1;

        r = Assets.instance.iconAssets.iconFarm;
        fcb = new FensterChildBaumenueLandwirtschaft(world, this, blaupause, skin);
        table.add(new IconAusklappButton(this, fcb, umPlatzKonkurrierendeFensterListe, r));
        umPlatzKonkurrierendeFensterListe.add(fcb);
        width += 1;

        r = Assets.instance.iconAssets.iconWissenschaft;
        fcb = new FensterChildBaumenueForschung(world, blaupause, this, skin);
        table.add(new IconAusklappButton(this, fcb, umPlatzKonkurrierendeFensterListe, r));
        umPlatzKonkurrierendeFensterListe.add(fcb);
        width += 1;

        r = Assets.instance.iconAssets.iconFreizeit;
        fcb = new FensterChildBaumenueLandwirtschaft(world, this, blaupause, skin); //TODO
        table.add(new IconAusklappButton(this, fcb, umPlatzKonkurrierendeFensterListe, r));
        umPlatzKonkurrierendeFensterListe.add(fcb);
        width += 1;

        table.row();
        height += 1;

        Button button = new Button(new TextureRegionDrawable(r));
        button.addListener(new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if (button == Input.Buttons.LEFT) {
                    auswahlkastenAbbauen.aktivieren();
                    MasterBaumenue.this.schliessen();
                    return true;
                }
                return super.touchDown(event, x, y, pointer, button);
            }
        });
        table.add(button);

        button = new Button(new TextureRegionDrawable(r));
        button.addListener(new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if (button == Input.Buttons.LEFT) {
                    auswahlkastenErnten.aktivieren();
                    MasterBaumenue.this.schliessen();
                    return true;
                }
                return super.touchDown(event, x, y, pointer, button);
            }
        });
        table.add(button);

        setWidth(width * r.getRegionWidth() + 10);
        setHeight(height * r.getRegionHeight() + 10);
    }

    // OEFFNEN UND SCHLIESSEN /////////////////////////////////////////////////////////////////////

    @Override
    public void schliessen() {
        super.schliessen();
        for (MoveableGroup mvg : umPlatzKonkurrierendeFensterListe) {
            mvg.schliessen();
        }
    }

    // KOMPLEXE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public void settPositionMitte(Vector2 positionMitte) {
        settPosition(new Vector2(positionMitte.x - getWidth()/2, positionMitte.y - getHeight()/2));
    }

    // EINFACHE GETTER UND SETTER /////////////////////////////////////////////////////////////////
}
