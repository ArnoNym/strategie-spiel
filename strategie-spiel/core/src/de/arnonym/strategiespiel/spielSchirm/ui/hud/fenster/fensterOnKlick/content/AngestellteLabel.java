package de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterOnKlick.content;

import com.badlogic.gdx.scenes.scene2d.ui.Skin;

import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.EmployerComp;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.labels.UpdateLabel;

public class AngestellteLabel extends UpdateLabel {
    private final EmployerComp<?> employerComp;

    public AngestellteLabel(EmployerComp<?> employerComp, Skin skin) {
        super(skin);
        this.employerComp = employerComp;
    }

    @Override
    public String aktualisiereInformationenOverride() {
        return employerComp.getAnzahlAngestellte() + " / " + employerComp.maxStellen;
    }
}
