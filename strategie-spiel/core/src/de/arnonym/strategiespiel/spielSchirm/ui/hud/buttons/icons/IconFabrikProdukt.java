package de.arnonym.strategiespiel.spielSchirm.ui.hud.buttons.icons;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.departments.material.HerstellerDepartmentComp;
import de.arnonym.strategiespiel.spielSchirm.ui.IngameCursorManager;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.MoveableGroup;
import de.arnonym.strategiespiel.spielSchirm.info.muster.GegInfo;
import de.arnonym.strategiespiel.spielSchirm.Assets;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.TextureUtils;

/*public class IconFabrikProdukt extends IconGegenstand {
    private final HerstellerDepartmentComp herstellerDepartmentComp;

    public IconFabrikProdukt(IngameCursorManager ingameCursorManager, MoveableGroup parentFenster,
                             HerstellerDepartmentComp herstellerDepartmentComp, GegInfo gegInfo) {
        super(ingameCursorManager, parentFenster, gegInfo);
        this.herstellerDepartmentComp = herstellerDepartmentComp;
    }

    @Override
    protected void touchDown() {
        GegInfo gi;
        // Cast zur Sicherheit falls sich die Programmierung hinter dem ArbeitAnObject veraendert
        if (gegMuster.equals((GegInfo) herstellerDepartmentComp.getArbeitAnObject())) {
            gi = null;
        } else {
            gi = gegMuster;
        }
        herstellerDepartmentComp.settArbeitAnObject(gi);
    }

    @Override
    public void draw_or(Batch batch, float parentAlpha) {
        super.draw_or(batch, parentAlpha);

        TextureRegion textureRegion;
        if (gegMuster == herstellerDepartmentComp.getArbeitAnObject()) {
            textureRegion = Assets.instance.iconAssets.iconRahmenHaken;
        } else {
            textureRegion = Assets.instance.iconAssets.iconRahmenKannKlicken;
        }
        TextureUtils.zeichneTexturRegion(batch, textureRegion, getX(), getY(), 1, 1, false);
    }
}*/