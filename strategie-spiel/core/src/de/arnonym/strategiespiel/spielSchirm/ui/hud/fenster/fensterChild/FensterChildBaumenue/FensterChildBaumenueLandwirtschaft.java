package de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterChild.FensterChildBaumenue;

import com.badlogic.gdx.scenes.scene2d.ui.Skin;

import de.arnonym.strategiespiel.spielSchirm.Dorf;
import de.arnonym.strategiespiel.spielSchirm.World;
import de.arnonym.strategiespiel.spielSchirm.balancing.IdsGebaude;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.departments.gebiet.farm.FarmFactory;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.departments.gebiet.farm.FarmInfo;
import de.arnonym.strategiespiel.spielSchirm.ui.Blaupause;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.MoveableGroup;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.buttons.icons.IconGebaude;

public class FensterChildBaumenueLandwirtschaft extends FensterChildBaumenue {
    public FensterChildBaumenueLandwirtschaft(World world,
                                              MoveableGroup ausklappenParentMoveableGroup,
                                              final Blaupause blaupause, Skin skin) {
        super(world, blaupause, ausklappenParentMoveableGroup, skin, 200, 200,
                "agriculture building menue");

        Dorf spielerDorf = world.spielerDorf;
        FarmInfo farmInfo = (FarmInfo) IdsGebaude.get(IdsGebaude.GEB_BER_FARM_ID);
        IconGebaude iconGebaude = new IconGebaude(ausklappenParentMoveableGroup, this, blaupause,
                new FarmFactory(world, spielerDorf, farmInfo));
        getContentTable().add(iconGebaude);

        getContentTable().row();
    }
}
