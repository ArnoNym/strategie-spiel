package de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterOnKlick.content;

import com.badlogic.gdx.scenes.scene2d.ui.Skin;

import de.arnonym.strategiespiel.spielSchirm.ui.hud.buttons.icons.Icon;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.tables.UpdateTable;

public class IconTable extends UpdateTable {
    private int iconsProZeile;
    private float insidePadX;
    private float insidePadY;
    private Icon[] icons;

    public IconTable(Skin skin, int iconsProZeile, float insidePadX, float insidePadY,
                     Icon... icons) {
        super(skin);
        this.iconsProZeile = iconsProZeile;
        this.insidePadX = insidePadX;
        this.insidePadY = insidePadY;
        this.icons = icons;
    }

    @Override
    public void aktualisiereInformationen_or() {
        boolean rowOne = true;
        for (int i = 0; i < icons.length; i++) {
            if ((float) i % iconsProZeile == 0) {
                row();
                rowOne = false;
            }
            if (i > 0) {
                if (rowOne) {
                    add(icons[i]).padLeft(insidePadX);
                } else {
                    add(icons[i]).padLeft(insidePadX).padTop(insidePadY);
                }
            } else {
                if (rowOne) {
                    add(icons[i]);
                } else {
                    add(icons[i]).padTop(insidePadY);
                }
            }
        }
    }
}