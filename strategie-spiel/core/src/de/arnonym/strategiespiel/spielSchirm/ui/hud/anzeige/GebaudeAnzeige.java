package de.arnonym.strategiespiel.spielSchirm.ui.hud.anzeige;

import com.badlogic.gdx.scenes.scene2d.ui.Label;

import java.util.Map;

import de.arnonym.strategiespiel.Strategiespiel;
import de.arnonym.strategiespiel.spielSchirm.Dorf;
import de.arnonym.strategiespiel.spielSchirm.balancing.IdsGegenstaende;
import de.arnonym.strategiespiel.spielSchirm.gebaude.GebaudeInfo;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.tables.UpdateTable;
import de.arnonym.strategiespiel.framework.werkzeuge.collections.hashMaps.IntIntHashMap;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.SpecialUtils;

public class GebaudeAnzeige extends Anzeige {
    public GebaudeAnzeige(Dorf dorf, GebaudeInfo gebaudeInfo) {
        super(new UpdateTable(Strategiespiel.skin){
            @Override
            public void aktualisiereInformationen_or() {
                add(new Label("building", getSkin())).expandX().fillX().left();
                row();
                add(new Label("name: ", getSkin())).expandX().fillX().left();
                add(new Label("" + gebaudeInfo.name, getSkin())).right();
                row();
                add(new Label("description: ", getSkin())).expandX().fillX().left();
                add(new Label("" + gebaudeInfo.beschreibung, getSkin())).right();

                SpecialUtils.leerzeile(this);

                UpdateTable innerUpdateTable = new UpdateTable(getSkin()) {
                    // UpdateTable weil ich erstelleHeader() wert moechte
                    @Override
                    public void aktualisiereInformationen_or() {
                        erstelleHeader("materials needed for building: ", true, "name", " / avaiable",
                                " / purchasable", " / needet");
                        row();

                        IntIntHashMap alleGegHashMap = dorf.gettAlleGegenstaendeIdMengenHashMap(true,
                                false, true);
                        IntIntHashMap baukosten = gebaudeInfo.musterAuftragBauen.idMengeHashMap;
                        for (Map.Entry<Integer, Integer> entry : baukosten.entrySet()) {

                            add(new Label(IdsGegenstaende.get(entry.getKey()).name,
                                    getSkin())).left().expandX();

                            int id = entry.getKey();
                            Integer vorhandenGegenstandMenge = alleGegHashMap.get(id);
                            if (vorhandenGegenstandMenge == null) {
                                vorhandenGegenstandMenge = 0;
                            }
                            add(new Label(""+vorhandenGegenstandMenge, getSkin())).right();

                            add(new Label("0", getSkin())).right(); //TODO Zum kauf verfuegbare Menge anpassen wenn Gebaude verkauft werden koennen oder Handel eingefuegt wird etc

                            int zumBauBenoetigteMenge = entry.getValue();
                            add(new Label(""+zumBauBenoetigteMenge, getSkin())).right();

                            row();
                        }
                    }
                };
                innerUpdateTable.aktualisiereInformationen();

                add(innerUpdateTable).colspan(100).expandX().fillX();
            }
        });
    }
}
