package de.arnonym.strategiespiel.spielSchirm.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.Viewport;

import de.arnonym.strategiespiel.Strategiespiel;
import de.arnonym.strategiespiel.spielSchirm.Dorf;
import de.arnonym.strategiespiel.spielSchirm.World;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.listSystems.privateListSystems.SysAbbaubar;
import de.arnonym.strategiespiel.spielSchirm.umwelt.ressourcen.SysFreuchte;
import de.arnonym.strategiespiel.spielSchirm.ui.auswahlkaesten.AuswahlkastenAbbauen;
import de.arnonym.strategiespiel.spielSchirm.ui.auswahlkaesten.AuswahlkastenErnten;
import de.arnonym.strategiespiel.spielSchirm.ui.auswahlkaesten.AuswahlkastenLandUebergeben;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.menue.MasterBaumenue;
import de.arnonym.strategiespiel.spielSchirm.Enums;
import de.arnonym.strategiespiel.framework.werkzeuge.IngameKamera;
import de.arnonym.strategiespiel.framework.werkzeuge.save.Laden;
import de.arnonym.strategiespiel.framework.werkzeuge.save.Speicher;

/**
 * Created by LeonPB on 07.04.2018.
 */

public class DiverseInputs extends InputAdapter{
    private final World world;
    private final Viewport worldViewport;
    private final Viewport overlayViewport;
    private final IngameKamera ingameKamera;
    private final Blaupause blaupause;
    private final AuswahlkastenAbbauen auswahlkastenAbbauen;
    private final AuswahlkastenErnten auswahlkastenErnten; // TODO
    private AuswahlkastenLandUebergeben auswahlkastenLandUebergeben;
    private final MasterBaumenue masterBaumenue;

    public DiverseInputs(World world, Dorf spielerDorf, Blaupause blaupause, Viewport worldViewport,
                         Viewport overlayViewport, IngameKamera ingameKamera,
                         IngameCursorManager ingameCursorManager, Stage guiStage,
                         SysAbbaubar sysAbbaubar, SysFreuchte sysFreuchte) {
        this.world = world;
        this.blaupause = blaupause;
        this.worldViewport = worldViewport;
        this.overlayViewport = overlayViewport;
        this.ingameKamera = ingameKamera;

        auswahlkastenAbbauen = new AuswahlkastenAbbauen(this.worldViewport, ingameCursorManager,
                sysAbbaubar, spielerDorf.abzubauenListe);
        auswahlkastenErnten = new AuswahlkastenErnten(this.worldViewport, ingameCursorManager,
                sysFreuchte, spielerDorf.erntenListe);
        auswahlkastenLandUebergeben = new AuswahlkastenLandUebergeben(this.worldViewport,
                ingameCursorManager, null, null); // TODO
        masterBaumenue = new MasterBaumenue(world, blaupause, auswahlkastenAbbauen,
                auswahlkastenErnten, Strategiespiel.skin);
    }

    public void update(float delta) {
        blaupause.update(delta);
        auswahlkastenLandUebergeben.update();
        auswahlkastenAbbauen.update();
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        world.getGuiStage().unfocusAll(); /* Zu diesem Punkt kommt es nur wenn kein UI Element
        angeklickt wurde. Es sorgt dafuer, dass alle GUI Elemente aus dem Fokus genommen werden,
        also z.B. das Scrollen sich wieder auf die {@Link World} bezieht. */

        Vector2 weltKlick = worldViewport.unproject(new Vector2(screenX, screenY));

        if(button == Input.Buttons.LEFT){ // Linke Maustaste
            if (blaupause.getAktiv()) {
                blaupause.bauen();
                return true;
            }

            if (auswahlkastenLandUebergeben.getEnumAktiv() == Enums.EnumAktiv.AKTIV) {
                auswahlkastenLandUebergeben.vorschlagen(weltKlick);
                return true;
            }
            if (auswahlkastenAbbauen.getEnumAktiv() == Enums.EnumAktiv.AKTIV) {
                auswahlkastenAbbauen.vorschlagen(weltKlick);
                return true;
            }
        } else if(button == Input.Buttons.RIGHT){ // Rechte Maustaste
            if (blaupause.getAktiv()) {
                blaupause.planenBeenden();
                return true;
            }

            if (auswahlkastenLandUebergeben.getEnumAktiv() != Enums.EnumAktiv.INAKTIV) {
                auswahlkastenLandUebergeben.deaktivieren();
                return true;
            }
            if (auswahlkastenAbbauen.getEnumAktiv() != Enums.EnumAktiv.INAKTIV) {
                auswahlkastenAbbauen.deaktivieren();
                return true;
            }

            if (masterBaumenue.isVisible()) {
                masterBaumenue.settVisible(false, false);
            } else {
                masterBaumenue.settPositionMitte(overlayViewport.unproject(
                        new Vector2(screenX, screenY)));
                masterBaumenue.settVisible(true, false);
            }
        }
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        Vector2 weltKlick = worldViewport.unproject(new Vector2(screenX, screenY));

        if(button == Input.Buttons.LEFT){ // Linke Maustaste
            if (auswahlkastenLandUebergeben.getEnumAktiv() == Enums.EnumAktiv.VORSCHLAGEN) {
                auswahlkastenLandUebergeben.ausloesen();
                return true;
            }
            if (auswahlkastenAbbauen.getEnumAktiv() == Enums.EnumAktiv.VORSCHLAGEN) {
                auswahlkastenAbbauen.ausloesen();
                return true;
            }
        } else if(button == Input.Buttons.RIGHT){ // Rechte Maustaste
        }
        return false;
    }

    @Override
    public boolean keyDown(int keycode) { //TODO Vielleicht dort wo auch die Buttons sind einfuehren
        /*if (keycode == Input.Keys.NUM_1) {
            blaupause.planen(IdsGebaude.GEB_LAGER_MATERIAL_ID);
        } else if (keycode == Input.Keys.NUM_2) {
            blaupause.planen(IdsGebaude.GEB_LAGER_NAHRUNG_ID);
        } else if (keycode == Input.Keys.NUM_3) {
            blaupause.planen(IdsGebaude.GEB_HAUS_EINFACH_ID);
        } else if (keycode == Input.Keys.NUM_4) {
            blaupause.planen(IdsGebaude.GEB_BER_SCHMIED_ID);
        } else if (keycode == Input.Keys.NUM_5) {
            blaupause.planen(IdsGebaude.GEB_BER_FARM_ID);
        } else*/ if (keycode == Input.Keys.M) {
            world.setSpielgeschwindigkeit(world.getSpielgeschwindigkeit() * 2);
        } else if (keycode == Input.Keys.N) {
            world.setSpielgeschwindigkeit(world.getSpielgeschwindigkeit() / 2);
        } else if (keycode == Input.Keys.C) {
            /* TODO for (Gebaude g : level.getSpielerDorf().getGebaudeListe()) {
                if (g.isAusgewaehlt()) {
                    g.loesche();
                    return true;
                }
            }*/
        } else if (keycode == Input.Keys.O) {
            Speicher.main(world);
        } else if (keycode == Input.Keys.P) {
            World ladenWorld = Laden.main();
            // TODO ladenLevel.create(true);
            World.spielSchirm.setWorld(ladenWorld); //TODO Das alte level hat immernoch viele Klassen die wiederum das Level haben. Der Garbage Collecotr sammelt den Kram nicht ein!!!!!!!!
            /*IdentityConnectable.out.println("Neues Level: " + level.getSpielerDorf().getFamilienListe().toList(0).getFamilienmitglieder().toList(0).getPosition().x);
            level.getSpielerDorf().getFamilienListe().toList(0).getFamilienmitglieder().toList(0).settPositionMitte(
                    ladenLevel.getSpielerDorf().getFamilienListe().toList(0).getFamilienmitglieder().toList(0).getPosition()
            );
            IdentityConnectable.out.println("Gelandenes Level: " + ladenLevel.getSpielerDorf().getFamilienListe().toList(0).getFamilienmitglieder().toList(0).getPosition().x);*/
        } else if (keycode == Input.Keys.SPACE) {
            /* TODO for (Dorfbewohner d : level.getSpielerDorf().gettDorfbewohnerListe()) {
                d.getStackFiniteStepMachine().loescheAlleTasks();
                d.setFreeze(!d.isFreeze());
            }*/
        }
        return true;
    }

    @Override
    public boolean keyUp(int keycode){
        if(!Gdx.input.isKeyPressed(Input.Keys.W)) { //Ist das nötig??
            if (keycode == Input.Keys.W) {

            }
        }
        return true;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return super.mouseMoved(screenX, screenY);
    }

    @Override
    public boolean scrolled(int amount) {
        ingameKamera.mausradScrolled(amount);
        return true;
    }

    // EINFACHE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public void setAuswahlkastenLandUebergeben(AuswahlkastenLandUebergeben auswahlkastenLandUebergeben) {
        this.auswahlkastenLandUebergeben = auswahlkastenLandUebergeben;
    }


    //public Gebaude getPlatzierenGebaude() {
    //    return platzierenGebaude;
    //}

/*
    public final Viewport overlayViewport;
    public Viewport worldViewport;
    public FigurSpieler figurSpieler;
    private Vector2 moveLeftCenter;
    private Vector2 moveRightCenter;
    private Vector2 shootCenter;
    private Vector2 jumpCenter;
    private int linksLaufenPointer;
    private int rechtsLaufenPointer;
    private int obenLaufenPointer;
    private int untenLaufenPointer;
    private int linksSchlagenPointer;
    private int rechtsSchlagenPointer;

    private boolean tasteGedrueckt;
    private boolean laufenBeenden;
    private boolean schlagenBeenden;
    private boolean springenBeenden;

    public EigenerInputAdapter(FigurSpieler figurSpieler, Viewport worldViewport, Viewport overlayViewport) {
        Gdx.input.setInputProcessor(this);
        this.figurSpieler = figurSpieler;
        this.worldViewport = worldViewport;
        this.overlayViewport = overlayViewport;

        moveLeftCenter = new Vector2();
        moveRightCenter = new Vector2();
        shootCenter = new Vector2();
        jumpCenter = new Vector2();

        linksLaufenPointer = 0;
        rechtsLaufenPointer = 0;
        obenLaufenPointer = 0;
        untenLaufenPointer = 0;
        linksSchlagenPointer = 0;
        rechtsSchlagenPointer = 0;

        tasteGedrueckt = false;
    }

    @Override
    public boolean keyDown(int keycode){
        if(keycode == Input.Keys.A) {
            figurSpieler.laufenAnfangen(Enums.EnumRichtung.LINKS);
        }else if(keycode == Input.Keys.D) {
            figurSpieler.laufenAnfangen(Enums.EnumRichtung.RECHTS);
        }
        if(keycode == Input.Keys.W) {
            figurSpieler.springen();
            //figurSpieler.springenBeginnenSobaldMoeglich();
        }
        return true;
    }
    @Override
    public boolean keyUp(int keycode){
        if(!Gdx.input.isKeyPressed(Input.Keys.A) && !Gdx.input.isKeyPressed(Input.Keys.D)) { //Damit die Figur beim gleichzeitgen drücken nicht aufhört zu laufen wenn eine der Tasten losgelassen wird
            if (keycode == Input.Keys.A || keycode == Input.Keys.D) {
                figurSpieler.laufenBeenden();
            }
        }
        if(!Gdx.input.isKeyPressed(Input.Keys.W)) {
            if (keycode == Input.Keys.W) {
                figurSpieler.springenBeenden();
            }
        }
        return true;
    }


    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        tasteGedrueckt = false;

        Vector2 weltKlick = worldViewport.unprojecttt(new Vector2(screenX, screenY));
        Vector2 weltKlickOverlay = overlayViewport.unprojecttt(new Vector2(screenX, screenY));

        if (Gdx.app.getType() == Application.ApplicationType.Android) {
            if (Werkzeuge.berechneKollisionViereckUndPunkt(Konstanten.BILDSCHIRMTASTEN_TASTE_LINKS_LAUFEN_POSITION, Konstanten.BILDSCHIRMTASTEN_TASTE_BREITE_UND_HOEHE, weltKlickOverlay)) {
                figurSpieler.laufenAnfangen(Enums.EnumRichtung.LINKS);
                linksLaufenPointer = pointer;
                tasteGedrueckt = true;
            } else if (Werkzeuge.berechneKollisionViereckUndPunkt(Konstanten.BILDSCHIRMTASTEN_TASTE_RECHTS_LAUFEN_POSITION, Konstanten.BILDSCHIRMTASTEN_TASTE_BREITE_UND_HOEHE, weltKlickOverlay)) {
                figurSpieler.laufenAnfangen(Enums.EnumRichtung.RECHTS);
                rechtsLaufenPointer = pointer;
                tasteGedrueckt = true;
            }

            if (Werkzeuge.berechneKollisionViereckUndPunkt(Konstanten.BILDSCHIRMTASTEN_TASTE_OBEN_LAUFEN_POSITION, Konstanten.BILDSCHIRMTASTEN_TASTE_BREITE_UND_HOEHE, weltKlickOverlay)) {
                figurSpieler.springen();
                //figurSpieler.springenBeginnenSobaldMoeglich();
                obenLaufenPointer = pointer;
                tasteGedrueckt = true;
            } else if (Werkzeuge.berechneKollisionViereckUndPunkt(Konstanten.BILDSCHIRMTASTEN_TASTE_UNTEN_LAUFEN_POSITION, Konstanten.BILDSCHIRMTASTEN_TASTE_BREITE_UND_HOEHE, weltKlickOverlay)) {

                untenLaufenPointer = pointer;
                tasteGedrueckt = true;
            }
        }

        if(!tasteGedrueckt){
            if(weltKlick.x < figurSpieler.getPositionMitte().x){
                figurSpieler.angreifenAnfangen(Enums.EnumRichtung.LINKS);
                linksSchlagenPointer = pointer;
            }else if(weltKlick.x >= figurSpieler.getPositionMitte().x){
                figurSpieler.angreifenAnfangen(Enums.EnumRichtung.RECHTS);
                rechtsSchlagenPointer = pointer;
            }
        }
        return super.touchDown(screenX, screenY, pointer, button);
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer){
        tasteGedrueckt = false;
        laufenBeenden = true;
        schlagenBeenden = true;
        springenBeenden = true;

        if(pointer == linksLaufenPointer){linksLaufenPointer =0;}
        if(pointer == rechtsLaufenPointer){rechtsLaufenPointer =0;}
        if(pointer == obenLaufenPointer){obenLaufenPointer =0;}
        if(pointer == untenLaufenPointer){untenLaufenPointer =0;}
        if(pointer == linksSchlagenPointer){linksSchlagenPointer =0;}
        if(pointer == rechtsSchlagenPointer){rechtsSchlagenPointer =0;}

        Vector2 weltKlick = worldViewport.unprojecttt(new Vector2(screenX, screenY));
        Vector2 weltKlickOverlay = overlayViewport.unprojecttt(new Vector2(screenX, screenY));

        if (Gdx.app.getType() == Application.ApplicationType.Android) {
            if (Werkzeuge.berechneKollisionViereckUndPunkt(Konstanten.BILDSCHIRMTASTEN_TASTE_LINKS_LAUFEN_POSITION, Konstanten.BILDSCHIRMTASTEN_TASTE_BREITE_UND_HOEHE, weltKlickOverlay)) {
                figurSpieler.setEnumRichtung(Enums.EnumRichtung.LINKS);
                linksLaufenPointer = pointer;
                laufenBeenden = false;
                if (figurSpieler.getEnumLaufen() == Enums.EnumLaufen.NICHT_LAUFEND) {
                    figurSpieler.laufenAnfangen(Enums.EnumRichtung.LINKS);
                }
                tasteGedrueckt = true;
            } else if (Werkzeuge.berechneKollisionViereckUndPunkt(Konstanten.BILDSCHIRMTASTEN_TASTE_RECHTS_LAUFEN_POSITION, Konstanten.BILDSCHIRMTASTEN_TASTE_BREITE_UND_HOEHE, weltKlickOverlay)) {
                figurSpieler.setEnumRichtung(Enums.EnumRichtung.RECHTS);
                rechtsLaufenPointer = pointer;
                laufenBeenden = false;
                if (figurSpieler.getEnumLaufen() == Enums.EnumLaufen.NICHT_LAUFEND) {
                    figurSpieler.laufenAnfangen(Enums.EnumRichtung.RECHTS);
                }
                tasteGedrueckt = true;
            }

            if (Werkzeuge.berechneKollisionViereckUndPunkt(Konstanten.BILDSCHIRMTASTEN_TASTE_OBEN_LAUFEN_POSITION, Konstanten.BILDSCHIRMTASTEN_TASTE_BREITE_UND_HOEHE, weltKlickOverlay)) {
                obenLaufenPointer = pointer;
                figurSpieler.springen();
                //springenBeenden = false;
                //figurSpieler.springenBeginnenSobaldMoeglich();
                tasteGedrueckt = true;
            } else if (Werkzeuge.berechneKollisionViereckUndPunkt(Konstanten.BILDSCHIRMTASTEN_TASTE_UNTEN_LAUFEN_POSITION, Konstanten.BILDSCHIRMTASTEN_TASTE_BREITE_UND_HOEHE, weltKlickOverlay)) {

                untenLaufenPointer = pointer;
                tasteGedrueckt = true;
            }
        }

        if(!tasteGedrueckt){
            if(weltKlick.x < figurSpieler.getPositionMitte().x){
                linksSchlagenPointer = pointer;
                schlagenBeenden = false;
                if(figurSpieler.getEnumAngreifen() != Enums.EnumAngreifen.ANGREIFEN){
                    figurSpieler.angreifenAnfangen(Enums.EnumRichtung.LINKS);
                }else{
                    figurSpieler.setEnumRichtung(Enums.EnumRichtung.LINKS);
                }
            }else if(weltKlick.x >= figurSpieler.getPositionMitte().x){
                rechtsSchlagenPointer = pointer;
                schlagenBeenden = false;
                if(figurSpieler.getEnumAngreifen() != Enums.EnumAngreifen.ANGREIFEN){
                    figurSpieler.angreifenAnfangen(Enums.EnumRichtung.RECHTS);
                }else{
                    figurSpieler.setEnumRichtung(Enums.EnumRichtung.RECHTS);
                }
            }
        }

        if(laufenBeenden && !Gdx.input.isKeyPressed(Input.Keys.A) && !Gdx.input.isKeyPressed(Input.Keys.D)){
            figurSpieler.laufenBeenden();}
        if(schlagenBeenden){
            figurSpieler.angreifenBeenden();}
        if(springenBeenden && !Gdx.input.isKeyPressed(Input.Keys.W)){
            figurSpieler.springenBeenden();}

        if (Gdx.app.getType() == Application.ApplicationType.Android) {
            aktionBerechnen(linksLaufenPointer, pointer, weltKlick, false, Konstanten.BILDSCHIRMTASTEN_TASTE_LINKS_LAUFEN_POSITION, Konstanten.BILDSCHIRMTASTEN_TASTE_BREITE_UND_HOEHE, weltKlickOverlay);
            aktionBerechnen(rechtsLaufenPointer, pointer, weltKlick, false, Konstanten.BILDSCHIRMTASTEN_TASTE_RECHTS_LAUFEN_POSITION, Konstanten.BILDSCHIRMTASTEN_TASTE_BREITE_UND_HOEHE, weltKlickOverlay);
            aktionBerechnen(obenLaufenPointer, pointer, weltKlick, false, Konstanten.BILDSCHIRMTASTEN_TASTE_OBEN_LAUFEN_POSITION, Konstanten.BILDSCHIRMTASTEN_TASTE_BREITE_UND_HOEHE, weltKlickOverlay);
            aktionBerechnen(untenLaufenPointer, pointer, weltKlick, false, Konstanten.BILDSCHIRMTASTEN_TASTE_UNTEN_LAUFEN_POSITION, Konstanten.BILDSCHIRMTASTEN_TASTE_BREITE_UND_HOEHE, weltKlickOverlay);
            aktionBerechnen(linksSchlagenPointer, pointer, weltKlick, true, new Vector2(), new Vector2(), weltKlickOverlay);
            aktionBerechnen(rechtsSchlagenPointer, pointer, weltKlick, true, new Vector2(), new Vector2(), weltKlickOverlay);
        }
        return super.touchDragged(screenX, screenY, pointer);
    }

    public void aktionBerechnen(int eigenerPointer, int pointer, Vector2 weltKlick, boolean mussKeineTasteTreffen, Vector2 untenLinksPositionDesVierecks, Vector2 breiteUndHoeheDesVierecks, Vector2 positionDesPunktes){
        if(eigenerPointer == pointer && !Werkzeuge.berechneKollisionViereckUndPunkt(untenLinksPositionDesVierecks, breiteUndHoeheDesVierecks, positionDesPunktes) || mussKeineTasteTreffen){
            tasteGedrueckt = false;

            if (Werkzeuge.berechneKollisionViereckUndPunkt(Konstanten.BILDSCHIRMTASTEN_TASTE_LINKS_LAUFEN_POSITION, Konstanten.BILDSCHIRMTASTEN_TASTE_BREITE_UND_HOEHE, positionDesPunktes)) {
                if(figurSpieler.getEnumLaufen() == Enums.EnumLaufen.LAUFEND) {
                    figurSpieler.setEnumRichtung(Enums.EnumRichtung.LINKS);
                    rechtsLaufenPointer = 0;
                    linksLaufenPointer = pointer;
                }else{
                    figurSpieler.angreifenBeenden();
                    figurSpieler.laufenAnfangen(Enums.EnumRichtung.LINKS);
                    linksLaufenPointer = pointer;
                }
                tasteGedrueckt = true;
            } else if (Werkzeuge.berechneKollisionViereckUndPunkt(Konstanten.BILDSCHIRMTASTEN_TASTE_RECHTS_LAUFEN_POSITION, Konstanten.BILDSCHIRMTASTEN_TASTE_BREITE_UND_HOEHE, positionDesPunktes)) {
                if(figurSpieler.getEnumLaufen() == Enums.EnumLaufen.LAUFEND) {
                    figurSpieler.setEnumRichtung(Enums.EnumRichtung.RECHTS);
                    linksLaufenPointer = 0;
                    rechtsLaufenPointer = pointer;
                }else{
                    figurSpieler.angreifenBeenden();
                    figurSpieler.laufenAnfangen(Enums.EnumRichtung.RECHTS);
                    rechtsLaufenPointer = pointer;
                }
                tasteGedrueckt = true;
            }

            if (Werkzeuge.berechneKollisionViereckUndPunkt(Konstanten.BILDSCHIRMTASTEN_TASTE_OBEN_LAUFEN_POSITION, Konstanten.BILDSCHIRMTASTEN_TASTE_BREITE_UND_HOEHE, positionDesPunktes)
                    && figurSpieler.getEnumSpringen() != Enums.EnumSpringen.SPRINGEND) {
                figurSpieler.angreifenBeenden();
                figurSpieler.springenBeginnenSobaldMoeglich();
                obenLaufenPointer = pointer;
                tasteGedrueckt = true;
            } else if (Werkzeuge.berechneKollisionViereckUndPunkt(Konstanten.BILDSCHIRMTASTEN_TASTE_UNTEN_LAUFEN_POSITION, Konstanten.BILDSCHIRMTASTEN_TASTE_BREITE_UND_HOEHE, positionDesPunktes)) {

                untenLaufenPointer = pointer;
                tasteGedrueckt = true;
            }

            if(!tasteGedrueckt){
                if(weltKlick.x < figurSpieler.getPositionMitte().x){
                    if(figurSpieler.getEnumAngreifen() == Enums.EnumAngreifen.ANGREIFEN){
                        figurSpieler.setEnumRichtung(Enums.EnumRichtung.LINKS);
                        linksSchlagenPointer = pointer;
                    }else {
                        figurSpieler.laufenBeenden();
                        figurSpieler.angreifenAnfangen(Enums.EnumRichtung.LINKS);
                        linksSchlagenPointer = pointer;
                    }
                }else if(weltKlick.x >= figurSpieler.getPositionMitte().x){
                    if(figurSpieler.getEnumAngreifen() == Enums.EnumAngreifen.ANGREIFEN){
                        figurSpieler.setEnumRichtung(Enums.EnumRichtung.RECHTS);
                        rechtsSchlagenPointer = pointer;
                    }else {
                        figurSpieler.laufenBeenden();
                        figurSpieler.angreifenAnfangen(Enums.EnumRichtung.RECHTS);
                        rechtsSchlagenPointer = pointer;
                    }
                }
            }
        }
    }

    public void updateProduktivitaetUndGehalt(SpriteBatch spriteBatch) {

        overlayViewport.apply();
        spriteBatch.setProjectionMatrix(overlayViewport.getCamera().combined);

        if(!Gdx.input.isKeyPressed(Input.Keys.W) && !Gdx.input.isTouched(obenLaufenPointer)) {
            figurSpieler.springenBeenden();
            obenLaufenPointer = 0;
        }
        if (Gdx.app.getType() == Application.ApplicationType.Android) {
            if (figurSpieler.getEnumSpringen() != Enums.EnumSpringen.SPRINGEND) {
                if (Gdx.input.isKeyPressed(Input.Keys.W)) {
                    figurSpieler.springenBeginnenSobaldMoeglich();
                } else if (Gdx.input.isTouched(obenLaufenPointer)) {
                    figurSpieler.springenBeginnenSobaldMoeglich();
                    obenLaufenPointer = 0;
                }
            }
        }

        if (!Gdx.input.isKeyPressed(Input.Keys.A) && !Gdx.input.isKeyPressed(Input.Keys.D)
                && figurSpieler.getEnumLaufen() == Enums.EnumLaufen.LAUFEND) { //Damit nicht andauernd das Laufen Beendet wird was den idle Timer resettet und somit nie die Idle Animation eingeleitet wird
            if (!Gdx.input.isTouched(linksLaufenPointer)) {
                figurSpieler.laufenBeenden();
                linksLaufenPointer = 0;
            }
            if (!Gdx.input.isTouched(rechtsLaufenPointer)) {
                figurSpieler.laufenBeenden();
                rechtsLaufenPointer = 0;
            }
            if (!Gdx.input.isTouched(obenLaufenPointer)) {
                figurSpieler.springenBeenden();
                obenLaufenPointer = 0;
            }
            if (!Gdx.input.isTouched(untenLaufenPointer)) {

                untenLaufenPointer = 0;
            }
        }
        if (figurSpieler.getEnumAngreifen() == Enums.EnumAngreifen.ANGREIFEN) { //Damit nicht andauernd der ANgriff Beendet wird was den idle Timer resettet und somit nie die Idle Animation eingeleitet wird
            if (!Gdx.input.isTouched(linksSchlagenPointer)) {
                figurSpieler.angreifenBeenden();
                linksSchlagenPointer = 0;
            }
            if (!Gdx.input.isTouched(rechtsSchlagenPointer)) {
                figurSpieler.angreifenBeenden();
                rechtsSchlagenPointer = 0;
            }
        }

        if (Gdx.app.getType() == Application.ApplicationType.Android) {
            Werkzeuge.drawTextureRegion(spriteBatch, Assets.instance.bildschirmtastenAssets.links, Konstanten.BILDSCHIRMTASTEN_TASTE_LINKS_LAUFEN_POSITION);
            Werkzeuge.drawTextureRegion(spriteBatch, Assets.instance.bildschirmtastenAssets.rechts, Konstanten.BILDSCHIRMTASTEN_TASTE_RECHTS_LAUFEN_POSITION);
            Werkzeuge.drawTextureRegion(spriteBatch, Assets.instance.bildschirmtastenAssets.oben, Konstanten.BILDSCHIRMTASTEN_TASTE_OBEN_LAUFEN_POSITION);
            Werkzeuge.drawTextureRegion(spriteBatch, Assets.instance.bildschirmtastenAssets.unten, Konstanten.BILDSCHIRMTASTEN_TASTE_UNTEN_LAUFEN_POSITION);
        }

    }

    }*/
}
