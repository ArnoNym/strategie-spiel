package de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterOnKlick;

import com.badlogic.gdx.scenes.scene2d.ui.Skin;

import de.arnonym.strategiespiel.spielSchirm.World;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.alter.EnumAlterComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.gegenstaende.GegenstaendeComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.name.NameComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.rendern.HashMapRenderComp;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.labels.UpdateLabel;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterOnKlick.content.GegenstandTable;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.SpecialUtils;

public class OnKlickFensterRessource extends OnKlickFenster {

    public OnKlickFensterRessource(World world, Skin skin, EnumAlterComp enumAlterComp,
                                   GegenstaendeComp gegenstaendeComp,
                                   HashMapRenderComp hashMapRenderComp, NameComp nameComp) {
        super(world, skin, 220, 300, nameComp, hashMapRenderComp);

        UpdateLabel updateLabel = new UpdateLabel(skin) {
            @Override
            public String aktualisiereInformationenOverride() {
                return "" + enumAlterComp.getEnumAlter().name() + ", " + enumAlterComp.getAlter_sek() + " Sekunden";
            }
        };
        getContentTable().add(updateLabel);
        getUpdateLabelList().add(updateLabel);

        SpecialUtils.leerzeile(getContentTable());

        GegenstandTable gegenstandTable = new GegenstandTable(world.getIngameCursorManager(),
                gegenstaendeComp, true, this);
        getContentTable().add(gegenstandTable).expandX().fillX();
        getUpdateTableList().add(gegenstandTable);
    }
}
