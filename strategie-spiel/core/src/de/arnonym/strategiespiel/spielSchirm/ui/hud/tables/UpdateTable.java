package de.arnonym.strategiespiel.spielSchirm.ui.hud.tables;

import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

public abstract class UpdateTable extends Table {

    public UpdateTable() {
    }

    public UpdateTable(Skin skin) {
        super(skin);
    }

    public final void aktualisiereInformationen() {
        clearChildren();
        aktualisiereInformationen_or();
    }

    public abstract void aktualisiereInformationen_or();

    public final void erstelleHeader(String tabellenUeberschrift_NL, boolean onlyExpandFirstColumn,
                               String... spaltenNamen) {
        Label label;
        if (tabellenUeberschrift_NL != null) {
            label = new Label(tabellenUeberschrift_NL, getSkin());
            add(label).expandX().colspan(100).left();
            row();
        }
        boolean firstColumn = true;
        for (String s : spaltenNamen) {
            label = new Label(s, getSkin());
            if (!onlyExpandFirstColumn || firstColumn) {
                add(label).expandX().left();
                firstColumn = false;
            } else {
                add(label).left();
            }
        }
    }
}
