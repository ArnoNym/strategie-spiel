package de.arnonym.strategiespiel.spielSchirm.ui.auswahlkaesten;

import com.badlogic.gdx.utils.viewport.Viewport;

import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.ausbeuten.FruechteComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.lists.ausbeuten.ErntenEcsList;
import de.arnonym.strategiespiel.spielSchirm.umwelt.ressourcen.SysFreuchte;
import de.arnonym.strategiespiel.spielSchirm.ui.IngameCursorManager;
import de.arnonym.strategiespiel.spielSchirm.Assets;

public class AuswahlkastenErnten extends AuswahlkastenAusbeuten<FruechteComp> {

    public AuswahlkastenErnten(Viewport worldViewport, IngameCursorManager ingameCursorManager,
                               SysFreuchte sysFreuchte, ErntenEcsList erntenListe) {
        super(worldViewport, ingameCursorManager, sysFreuchte, erntenListe,
                Assets.instance.tiledMapAssets.grid, Assets.instance.tiledMapAssets.ernten);
    }
}
