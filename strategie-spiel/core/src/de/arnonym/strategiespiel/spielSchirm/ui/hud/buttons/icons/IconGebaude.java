package de.arnonym.strategiespiel.spielSchirm.ui.hud.buttons.icons;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import de.arnonym.strategiespiel.spielSchirm.gebaude.GebaudeFactory;
import de.arnonym.strategiespiel.spielSchirm.gebaude.GebaudeInfo;
import de.arnonym.strategiespiel.spielSchirm.ui.Blaupause;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.MoveableGroup;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.anzeige.GebaudeAnzeige;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterChild.FensterChildBaumenue.FensterChildBaumenue;
import de.arnonym.strategiespiel.spielSchirm.Assets;
import de.arnonym.strategiespiel.spielSchirm.Enums;
import de.arnonym.strategiespiel.spielSchirm.info.forschung.Forschungsstand;

public class IconGebaude extends Icon {
    public final GebaudeInfo gebaudeMuster;
    public final Forschungsstand forschungsstand;

    public IconGebaude(MoveableGroup parentMoveableGroup,
                       FensterChildBaumenue iconsParentFensterBaumenue, Blaupause blaupause,
                       GebaudeFactory gebaudeFactory) {
        super(blaupause.getIngameCursorManager(), iconsParentFensterBaumenue,
                gebaudeFactory.info.gettId());
        this.gebaudeMuster = gebaudeFactory.info;
        this.forschungsstand = gebaudeFactory.dorf.forschungsstandList.gettDurchId(id);

        addListener(new ClickListener() {
            private boolean dontExit = false;

            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if (button == Input.Buttons.LEFT) {
                    if (!forschungsstand.isErforscht()) {
                        dontExit = true;
                        return super.touchDown(event, x, y, pointer, button);
                    }
                    blaupause.planen(gebaudeFactory);
                    parentMoveableGroup.schliessen();
                    iconsParentFensterBaumenue.schliessen();
                }
                return super.touchDown(event, x, y, pointer, button);
            }
            @Override
            public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
                iconsParentFensterBaumenue.getAnzeigeFensterBauen().aktiviereHover(
                        iconsParentFensterBaumenue,
                        new GebaudeAnzeige(gebaudeFactory.dorf, gebaudeMuster));
                super.enter(event, x, y, pointer, fromActor);
            }
            @Override
            public void exit(InputEvent event, float x, float y, int pointer, Actor toActor) {
                if (dontExit) {
                    dontExit = false;
                    return;
                }
                iconsParentFensterBaumenue.getAnzeigeFensterBauen().deaktiviereHover();
                super.exit(event, x, y, pointer, toActor);
            }
        });
    }

    @Override
    public void draw_or(Batch batch, float parentAlpha) {
        renderWorldTextureRegion((SpriteBatch) batch,
                gebaudeMuster.textureHashMap.get(Enums.EnumAktion.STANDARD_PRIMAERMENGE_VOLL).get());

        if (forschungsstand.isErforscht()) {
            renderRahmenKannKlicken((SpriteBatch) batch);
        } else {
            if (forschungsstand.issWirdErforscht()) {
                renderWorldTextureRegion((SpriteBatch) batch,
                        Assets.instance.iconAssets.iconWissenschaft); // todo prozentanzeige
                return;
            }
            renderRahmenKreuz((SpriteBatch) batch);
        }
    }
}
