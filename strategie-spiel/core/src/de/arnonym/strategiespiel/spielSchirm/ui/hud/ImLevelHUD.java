package de.arnonym.strategiespiel.spielSchirm.ui.hud;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.Viewport;

import de.arnonym.strategiespiel.Strategiespiel;
import de.arnonym.strategiespiel.spielSchirm.World;

/**
 * Created by LeonPB on 07.04.2018.
 */

public class ImLevelHUD {
    private World world;
    private Viewport viewport;
    //private BitmapFont bitmapFont;
    private Vector2 position;

    public ImLevelHUD(World world) {
        this.world = world;
        this.viewport = world.getViewport();
        //bitmapFont = new BitmapFont();
        //bitmapFont.getRegion().getAssetContainer().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        position = new Vector2();
    }

    public void resize () {
        //bitmapFont.getData().setScale(viewport.getScreenHeight()/400);
    }

    public void update (float delta) {
    }

    public void render (SpriteBatch spriteBatch) {
        position = viewport.unproject(new Vector2(viewport.getScreenWidth()-10,10));
        int mengeLagerMaterial = 0;
        int mengeLagerNahrung = 0;
        int mengeLagerProdukte = 0;
        int mengeBerufeRessourcen = 0;
        int mengeBerufeProdukte = 0;


        //StrategieSpielGame.bitmapFont.draw(spriteBatch, "FPS: "+ Gdx.graphics.getFramesPerSecond()+"\nDB1 Step: "+level.getSpielerDorf().gettDorfbewohnerListe().propabilityHashMap(0).getStackFiniteStepMachine().currentStep()+"\n "+level.getSpielerDorf().gettDorfbewohnerListe().propabilityHashMap(0).getEnumDbStates()+"\nLMaterial: "+mengeLagerMaterial+"\nLProdukte: "+mengeLagerProdukte+"\nLNahrung: "+mengeLagerNahrung+"\nBerufeRessourcen: "+mengeBerufeRessourcen+"\nBerufeProdukte: "+mengeBerufeProdukte+"\nTransportDB: "+mengeTransportierenDorfbewohner+"\nNahrungnDB: "+mengeNahrungnDorfbewohner+"\nschlafDB: "+schlafDorfbewohner, position.x, position.y, 0, Align.right, false);
        //bitmapFont.draw(spriteBatch, "Hallo Welt!", position.x, position.y, 0, Align.left, false);
        Strategiespiel.bitmapFont.draw(
                spriteBatch, "FPS: "+ Gdx.graphics.getFramesPerSecond(),
                position.x, position.y, 0, Align.right, false);
        position = world.getViewport().unproject(new Vector2(0, world.getViewport().getScreenHeight()));
        //TextureUtils.zeichneTexturRegion(spriteBatch, Assets.instance.levelGuiAssets.gebaudeLeisteA, position, false);
        /*
        position = Level.viewport.unprojecttt(new Vector2(0, Level.viewport.getScreenHeight()));
        Werkzeuge.zeichneTexturRegion(spriteBatch, Assets.instance.hudAssets.gebaudeLeisteA, position, false);*/
    }





        /* private Level level;
    private FigurSpieler spieler;
    private boolean hatGegner;
    private Figur figurGegner;
    private final Viewport overlayViewport;
    private long siehtGegnerStartZeit;
    private float siehtGegnerPauseDauer;

    public ImLevelHUD(Level level, Viewport overlayViewport) {
        this.level = level;
        spieler = level.getFigurSpieler();
        hatGegner = true;
        figurGegner = level.getFigurenListe().toList(0);
        this.overlayViewport = overlayViewport;

        siehtGegnerStartZeit = 0;
        siehtGegnerPauseDauer = KonstantenAssets.HUD_KEINEN_GEGNER_GESEHEN_PAUSE_DAUER;

        for (Figur f : level.getFigurenListe()) {
            f.setMaxLebenspunkte(f.getLebenspunkte());
        }
    }

    public void durchfuehren (float delta) {
        boolean einenGegnerGefunden = false;
        if (spieler.getEnumRichtung() == Enums.EnumRichtung.LINKS) {
            for (Figur f : level.getFigurenListe()) {
                if (f.getPositionMitte().x < spieler.getPositionMitte().x
                        && f.getPositionMitte().x >= spieler.getPositionMitte().x - level.viewport.getWorldWidth() / 2) {
                    figurGegner = f;
                    hatGegner = true;
                    einenGegnerGefunden = true;
                }
            }
        } else if (spieler.getEnumRichtung() == Enums.EnumRichtung.RECHTS) {
            for (Figur f : level.getFigurenListe()) {
                if (f.getPositionMitte().x > spieler.getPositionMitte().x
                        && f.getPositionMitte().x <= spieler.getPositionMitte().x + level.viewport.getWorldWidth() / 2) {
                    figurGegner = f;
                    hatGegner = true;
                    einenGegnerGefunden = true;
                }
            }
        } else {
            einenGegnerGefunden = false;
        }
        if (einenGegnerGefunden) {
            siehtGegnerStartZeit = TimeUtils.nanoTime();
        }
        if (!Werkzeuge.pause(siehtGegnerStartZeit, siehtGegnerPauseDauer)) {
            hatGegner = false;
        }
    }

    public void renderUndUpdate (SpriteBatch spriteBatch) {
        overlayViewport.apply();
        spriteBatch.setProjectionMatrix(overlayViewport.getCamera().combined);
        TextureRegion t = new TextureRegion();
        Vector2 position;
        //5 pro Reihe
        //10 Lebenspunkte pro Herz
        int lebenspunkteProHerz = 10;
        int herzenProReihe = 5;
        int lebenspunkteProReihe = herzenProReihe*lebenspunkteProHerz;
        int i = 0;
        int j = 0;
        while (i < (int)(spieler.getMaxLebenspunkte()/(lebenspunkteProReihe))+1) {
            j = 0;
            int bisJ = Math.min((int)((spieler.getMaxLebenspunkte()-i*lebenspunkteProReihe)/lebenspunkteProHerz),herzenProReihe);
            while (j < bisJ) {
                position = new Vector2(KonstantenAssets.HUD_LEBENSBALKEN_SPIELER_POSITION.x + j*KonstantenAssets.HUD_HERZ_HITBOX_BREITE_UND_HOEHE.x, KonstantenAssets.HUD_LEBENSBALKEN_SPIELER_POSITION.y + (-i)*KonstantenAssets.HUD_HERZ_HITBOX_BREITE_UND_HOEHE.y);
                float lebenspunkteRest = (spieler.getLebenspunkte()-i*lebenspunkteProReihe-j*lebenspunkteProHerz);
                if (lebenspunkteRest < lebenspunkteProHerz) {
                    if (lebenspunkteRest < 0) {
                        t=Assets.instance.hudAssets.herz01von10;
                    } else {
                        switch ((int)lebenspunkteRest) {
                            case 1:
                                t = Assets.instance.hudAssets.herz02von10;
                                break;
                            case 2:
                                t = Assets.instance.hudAssets.herz03von10;
                                break;
                            case 3:
                                t = Assets.instance.hudAssets.herz04von10;
                                break;
                            case 4:
                                t = Assets.instance.hudAssets.herz05von10;
                                break;
                            case 5:
                                t = Assets.instance.hudAssets.herz06von10;
                                break;
                            case 6:
                                t = Assets.instance.hudAssets.herz07von10;
                                break;
                            case 7:
                                t = Assets.instance.hudAssets.herz08von10;
                                break;
                            case 8:
                                t = Assets.instance.hudAssets.herz09von10;
                                break;
                            case 9:
                                t = Assets.instance.hudAssets.herz09von10;
                                break;
                            default: //Wenn 0<lebenspunkteRest<1 sind und (int) dann zu 0 führt einfach ein leeres Herz anzeigen
                                t = Assets.instance.hudAssets.herz01von10;
                                break;
                        }
                    }
                } else {
                    t=Assets.instance.hudAssets.herz10von10;
                }
                Werkzeuge.drawTextureRegion(spriteBatch, t, position);
                j++;
            }
            i++;
        }

        //Gegner Leben
        if (hatGegner) {
            i = 0;
            while (i < (int) (figurGegner.getMaxLebenspunkte() / (lebenspunkteProReihe)) + 1) {
                j = 0;
                int bisJ = Math.min((int) ((figurGegner.getMaxLebenspunkte() - i * lebenspunkteProReihe) / lebenspunkteProHerz), herzenProReihe);
                while (j < bisJ) {
                    position = new Vector2(overlayViewport.getWorldWidth() - KonstantenAssets.HUD_LEBENSBALKEN_SPIELER_POSITION.x - KonstantenAssets.HUD_HERZ_HITBOX_BREITE_UND_HOEHE.x - j * KonstantenAssets.HUD_HERZ_HITBOX_BREITE_UND_HOEHE.x, KonstantenAssets.HUD_LEBENSBALKEN_SPIELER_POSITION.y + (-i) * KonstantenAssets.HUD_HERZ_HITBOX_BREITE_UND_HOEHE.y);
                    float lebenspunkteRest = (figurGegner.getLebenspunkte() - i * lebenspunkteProReihe - j * lebenspunkteProHerz);
                    if (lebenspunkteRest < lebenspunkteProHerz) {
                        if (lebenspunkteRest < 0) {
                            t = Assets.instance.hudAssets.herz01von10;
                        } else {
                            switch ((int) lebenspunkteRest) {
                                case 1:
                                    t = Assets.instance.hudAssets.herz02von10;
                                    break;
                                case 2:
                                    t = Assets.instance.hudAssets.herz03von10;
                                    break;
                                case 3:
                                    t = Assets.instance.hudAssets.herz04von10;
                                    break;
                                case 4:
                                    t = Assets.instance.hudAssets.herz05von10;
                                    break;
                                case 5:
                                    t = Assets.instance.hudAssets.herz06von10;
                                    break;
                                case 6:
                                    t = Assets.instance.hudAssets.herz07von10;
                                    break;
                                case 7:
                                    t = Assets.instance.hudAssets.herz08von10;
                                    break;
                                case 8:
                                    t = Assets.instance.hudAssets.herz09von10;
                                    break;
                                case 9:
                                    t = Assets.instance.hudAssets.herz09von10;
                                    break;
                                default: //Wenn 0<lebenspunkteRest<1 sind und (int) dann zu 0 führt einfach ein leeres Herz anzeigen
                                    t = Assets.instance.hudAssets.herz01von10;
                                    break;
                            }
                        }
                    } else {
                        t = Assets.instance.hudAssets.herz10von10;
                    }
                    Werkzeuge.zeichneTexturRegion(spriteBatch, t, position, true);
                    j++;
                }
                i++;
            }
        }
    }

    public Figur getFigurGegner() {
        return figurGegner;
    }
    public void setFigurGegner() {
        this.figurGegner = spieler; //Nur weil ich das nicht platzEmpty setzten kann
        hatGegner = false;
    }
    public void setFigurGegner(Figur figurGegner) {
        this.figurGegner = figurGegner;
        hatGegner = true;
    }
    */
}
