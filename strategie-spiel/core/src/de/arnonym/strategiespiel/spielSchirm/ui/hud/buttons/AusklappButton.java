package de.arnonym.strategiespiel.spielSchirm.ui.hud.buttons;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;

import java.util.List;

import de.arnonym.strategiespiel.spielSchirm.ui.hud.MoveableGroup;

public class AusklappButton extends Button {
    private MoveableGroup fensterAusklappen;
    private List<MoveableGroup> umPlatzKonkurrierendeFensterListe;

    public AusklappButton(MoveableGroup ausklappenParentMoveableGroup,
                          final MoveableGroup fensterAusklappen, Drawable up) {
        super(up);
        constructor(ausklappenParentMoveableGroup, fensterAusklappen);
    }

    public AusklappButton(MoveableGroup ausklappenParentMoveableGroup,
                          final MoveableGroup fensterAusklappen,
                          List<MoveableGroup> umPlatzKonkurrierendeFensterListe, Drawable up) {
        super(up);
        this.umPlatzKonkurrierendeFensterListe = umPlatzKonkurrierendeFensterListe;
        constructor(ausklappenParentMoveableGroup, fensterAusklappen);
    }

    private void constructor(final MoveableGroup ausklappenParentMoveableGroup,
                             final MoveableGroup fensterAusklappen) {
        this.fensterAusklappen = fensterAusklappen;

        addListener(new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if (button == Input.Buttons.LEFT) {
                    if (ausklappenParentMoveableGroup != null) {
                        ausklappenParentMoveableGroup.childAusklappen(fensterAusklappen);
                    }
                    if (umPlatzKonkurrierendeFensterListe != null) {
                        for (MoveableGroup f : umPlatzKonkurrierendeFensterListe) {
                            if (f != fensterAusklappen && f.isStandardposition()) {
                                f.schliessen();
                            }
                        }
                    }
                    return true;
                }
                return super.touchDown(event, x, y, pointer, button);
            }
        });
    }

    public MoveableGroup getFensterAusklappen() {
        return fensterAusklappen;
    }
}