package de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterChild;

import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

import de.arnonym.strategiespiel.spielSchirm.World;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.MoveableGroup;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.Fenster;
import de.arnonym.strategiespiel.spielSchirm.Assets;
import de.arnonym.strategiespiel.spielSchirm.Enums;
import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenOptik;

public abstract class FensterChild extends Fenster {
    public FensterChild(World world, MoveableGroup ausklappenParentMoveableGroup,
                        Enums.EnumRichtung enumRichtung_nl, Skin skin,
                        float width, float height, String fensterTitel) {
        super(world.getGuiStage(), world.getOverlayViewport(), world.getViewport(),
                world.getIngameCursorManager(), skin,
                new NinePatch(Assets.instance.levelGuiAssets.fensterDickerRand,
                        KonstantenOptik.AUSWAHLKASTEN_ECKEN_BREITE,
                        KonstantenOptik.AUSWAHLKASTEN_ECKEN_BREITE,
                        KonstantenOptik.AUSWAHLKASTEN_ECKEN_HOEHE,
                        KonstantenOptik.AUSWAHLKASTEN_ECKEN_HOEHE),
                width, height, new Label(fensterTitel, skin),
                true);
        setParentFenster(ausklappenParentMoveableGroup);
        if (enumRichtung_nl != null) setAufSeiteVonParentFenster(enumRichtung_nl);
        setVisible(false);
    }
}
