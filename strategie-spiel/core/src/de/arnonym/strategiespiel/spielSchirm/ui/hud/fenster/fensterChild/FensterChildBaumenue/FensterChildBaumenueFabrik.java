package de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterChild.FensterChildBaumenue;

import com.badlogic.gdx.scenes.scene2d.ui.Skin;

import de.arnonym.strategiespiel.spielSchirm.Dorf;
import de.arnonym.strategiespiel.spielSchirm.World;
import de.arnonym.strategiespiel.spielSchirm.balancing.IdsGebaude;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.departments.material.MaterialGebaudeInfo;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.departments.material.schmied.SchmiedFactory;
import de.arnonym.strategiespiel.spielSchirm.ui.Blaupause;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.MoveableGroup;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.buttons.icons.IconGebaude;

public class FensterChildBaumenueFabrik extends FensterChildBaumenue {
    public FensterChildBaumenueFabrik(World world, Blaupause blaupause,
                                      MoveableGroup ausklappenParentMoveableGroup, Skin skin) {
        super(world, blaupause, ausklappenParentMoveableGroup, skin, 200, 300,
                "factories building menue");

        Dorf spielerDorf = world.spielerDorf;
        MaterialGebaudeInfo materialGebaudeInfo = (MaterialGebaudeInfo) IdsGebaude.get(
                IdsGebaude.GEB_BER_SCHMIED_ID);
        IconGebaude iconGebaude = new IconGebaude(ausklappenParentMoveableGroup, this,
                blaupause, new SchmiedFactory(world, spielerDorf, materialGebaudeInfo));

        getContentTable().add(iconGebaude);
    }
}
