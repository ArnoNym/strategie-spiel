package de.arnonym.strategiespiel.spielSchirm.ui.aufsteigendeSymbole;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

import de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.SysAltern;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.listSystems.privateListSystems.SysBewegung;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.SysRender;
import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenOptik;
import de.arnonym.strategiespiel.framework.werkzeuge.Manager;

public class AufsteigendesSymbolFactory {
    private final Manager manager;
    public final SysAltern sysAltern;
    public final SysBewegung sysBewegung;
    public final SysRender sysRender;

    public AufsteigendesSymbolFactory(Manager manager, SysAltern sysAltern, SysBewegung sysBewegung,
                                      SysRender sysRender) {
        this.manager = manager;
        this.sysAltern = sysAltern;
        this.sysBewegung = sysBewegung;
        this.sysRender = sysRender;
    }

    public AufsteigendesSymbol create(TextureRegion textureRegion, float geschwindigkeit,
                                      float dauer, Vector2 position) {
        return new AufsteigendesSymbol(manager, sysAltern, sysBewegung, sysRender, textureRegion,
                dauer, geschwindigkeit, new Vector2(position));
    }

    public AufsteigendesSymbol create(TextureRegion textureRegion, Vector2 position) {
        return new AufsteigendesSymbol(manager, sysAltern, sysBewegung, sysRender, textureRegion,
                KonstantenOptik.AUFSTEIGENDE_SYMBOLE_DAUER,
                KonstantenOptik.AUFSTEIGENDE_SYMBOLE_GESCHWINDIGKEIT, new Vector2(position));
    }
}
