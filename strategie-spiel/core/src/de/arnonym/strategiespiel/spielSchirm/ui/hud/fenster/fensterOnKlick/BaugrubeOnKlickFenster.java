package de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterOnKlick;

import com.badlogic.gdx.scenes.scene2d.ui.Skin;

import de.arnonym.strategiespiel.spielSchirm.World;
import de.arnonym.strategiespiel.spielSchirm.arbeit.TimeClock;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.baugrube.BaugrubeComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.name.NameComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.rendern.HashMapRenderComp;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.labels.UpdateLabel;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterOnKlick.content.GegenstandTable;

public class BaugrubeOnKlickFenster extends OnKlickFenster {
    public BaugrubeOnKlickFenster(World world, BaugrubeComp baugrubeComp, NameComp nameComp,
                                  HashMapRenderComp hashMapRenderComp, Skin skin) {
        super(world, skin, 300, 300, nameComp, hashMapRenderComp);

        GegenstandTable gegenstandTable = new GegenstandTable(world.getIngameCursorManager(),
                baugrubeComp.gegenstaendeComp, true, this);
        getContentTable().add(gegenstandTable);
        getUpdateTableList().add(gegenstandTable);

        TimeClock cabz = baugrubeComp.timeClock;
        UpdateLabel updateLabel = new UpdateLabel(skin) {
            @Override
            public String aktualisiereInformationenOverride() {
                float arbeitszeit = cabz.getArbeitszeit();
                return "done: " + (arbeitszeit - cabz.getRestZuErledigenArbeitszeit()) / arbeitszeit;
            }
        };

        getContentTable().row();

        getContentTable().add(updateLabel);
        getUpdateLabelList().add(updateLabel);
    }
}
