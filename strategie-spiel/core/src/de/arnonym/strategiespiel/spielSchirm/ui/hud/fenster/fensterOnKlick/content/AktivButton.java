package de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterOnKlick.content;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import de.arnonym.strategiespiel.framework.ecs.component.Component;
import de.arnonym.strategiespiel.framework.listener.StoryTeller;
import de.arnonym.strategiespiel.spielSchirm.Assets;

public class AktivButton extends ImageButton {

    public AktivButton(Component component) {
        super(new TextureRegionDrawable(Assets.instance.iconAssets.iconRahmenKannKlicken),
                new TextureRegionDrawable(Assets.instance.iconAssets.iconRahmenKannKlicken),
                new TextureRegionDrawable(Assets.instance.iconAssets.iconRahmenHaken));
        addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if (isChecked()) {
                    System.out.println("SchuleOnKlicjFenster ::: deactivate");
                    component.deactivate();
                } else {
                    System.out.println("SchuleOnKlicjFenster ::: setObjectInfo");
                    component.activate();
                }
                return true; // Check wird in Button getogglet
            }
        });
        setChecked(true);
    }
}
