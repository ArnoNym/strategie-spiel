package de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;
import com.badlogic.gdx.utils.viewport.Viewport;

import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.groesse.IsoGroesseComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.sonstiges.BlaupausenObjektComp;
import de.arnonym.strategiespiel.spielSchirm.ui.IngameCursorManager;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.MoveableGroup;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.anzeige.Anzeige;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.anzeige.GebaudeAnzeige;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.tables.UpdateTable;
import de.arnonym.strategiespiel.spielSchirm.Assets;
import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenOptik;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.SpecialUtils;

public class AnzeigeFenster extends MoveableGroup {
    private NinePatchDrawable ninePatchDrawable;

    private boolean hovertUeberIcon;
    private BlaupausenObjektComp blaupausenObjektComp;
    private Table contentTable;

    public AnzeigeFenster(Stage levelGuiStage, Viewport overlayViewport, Viewport worldViewport,
                          IngameCursorManager ingameCursorManager, Skin skin) {
        super(levelGuiStage, overlayViewport, worldViewport, ingameCursorManager, skin, 1, 1);
        this.ninePatchDrawable = new NinePatchDrawable(new NinePatch(
                Assets.instance.levelGuiAssets.auswahlKastenA,
                KonstantenOptik.AUSWAHLKASTEN_ECKEN_BREITE, //TODO
                KonstantenOptik.AUSWAHLKASTEN_ECKEN_BREITE,
                KonstantenOptik.AUSWAHLKASTEN_ECKEN_HOEHE,
                KonstantenOptik.AUSWAHLKASTEN_ECKEN_HOEHE));
        setVisible(false);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public void act(float delta) {
        if (!isVisible()) return;

        if (contentTable instanceof UpdateTable) {
            ((UpdateTable) contentTable).aktualisiereInformationen();
        }

        if (hovertUeberIcon) {
            if (getParentFenster() != null) {
                settPositionAnParent();
            } else {
                settPositionAnMaus();
            }
        } else if (blaupausenObjektComp != null) {
            settPositionAnBlaupause(blaupausenObjektComp);
            toBack(); // Nicht ueber Fenstern wenn Maus auf Fenster bewegt wird
        } else {
            schliessen();
        }
        super.act(delta);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
    }

    // AKTIVIEREN UND DEAKTIVIEREN ////////////////////////////////////////////////////////////////

    public void aktiviereHover(MoveableGroup parentFenster, Anzeige... anzeiges) {
        setParentFenster(parentFenster);
        aktiviereHover(anzeiges);
    }

    public void aktiviereHover(Anzeige... anzeiges) {
        hovertUeberIcon = true;
        Table table = new Table(skin);
        for (Anzeige a : anzeiges) {
            table.add(a.gettUpdateTable()).expandX().fillX();
            SpecialUtils.leerzeile(table);
        }
        aktiviere(table);
    }

    public void deaktiviereHover() {
        setParentFenster(null);
        hovertUeberIcon = false;
        deaktiviere();
    }

    public void aktiviereBlaupause(BlaupausenObjektComp blaupausenObjektComp,
                                   GebaudeAnzeige gebaudeAnzeige) {
        this.blaupausenObjektComp = blaupausenObjektComp;
        aktiviere(gebaudeAnzeige.gettUpdateTable());
    }

    public void deaktiviereBlaupause() {
        blaupausenObjektComp = null;
        deaktiviere();
    }

    private void aktiviere(Table table) {
        toFront();
        oeffnen(false);
        settContent(table);
    }

    private void deaktiviere() {

    }

    // CONTENT ////////////////////////////////////////////////////////////////////////////////////

    private void settContent(Table table) {
        clearChildren();
        this.contentTable = table;
        contentTable.setBackground(ninePatchDrawable);
        contentTable.pack();
        addActor(contentTable);
        setWidth(contentTable.getWidth());
        setHeight(contentTable.getHeight());
    }

    // KOMPLEXE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    private void settPositionAnBlaupause(BlaupausenObjektComp blaupausenObjektComp) {
        float x, y, width, height;
        Vector2 blaupausePosition = blaupausenObjektComp.getPositionComp().getPosition();
        x = blaupausePosition.x;
        y = blaupausePosition.y;
        IsoGroesseComp isoGroesseComp = blaupausenObjektComp.getIsoGroesseComp();
        width = isoGroesseComp.getWidth();
        height = isoGroesseComp.getHeight();
        settPosition(new Vector2(x + width / 2, y + height / 2), 0, - getHeight());
    }

    /*private void settPositionAnParent() {
        float x = getParentFenster().getX();
        float y = getParentFenster().getY();
        float width = getParentFenster().getWidth();
        float height = getParentFenster().getHeight();
        /*if (0==0) throw new IllegalStateException("contentTable.getHeight(): "+contentTable.getHeight()
                + "contentTable.getPrefHeight(): "+contentTable.getPrefHeight()
                + "getHeight(): "+getHeight());
        settPosition(new Vector2(x + width, y + height - contentTable.getHeight()));
    }*/

    private void settPositionAnMaus() {
        Vector2 position = ingameCursorManager.gettMausOverlayPosition();
        position.x += 50; // TODO Konstante
        settPosition(position);
    }

    // PROTECTED GETTER UND SETTER ////////////////////////////////////////////////////////////////

    protected boolean isHovertUeberIcon() {
        return hovertUeberIcon;
    }

    protected Table getContentTable() {
        return contentTable;
    }

    // EINFACHE GETTER UND SETTER /////////////////////////////////////////////////////////////////
}
