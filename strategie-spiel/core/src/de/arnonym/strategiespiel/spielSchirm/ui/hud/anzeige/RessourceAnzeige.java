package de.arnonym.strategiespiel.spielSchirm.ui.hud.anzeige;

import de.arnonym.strategiespiel.Strategiespiel;
import de.arnonym.strategiespiel.spielSchirm.umwelt.ressourcen.RessourceInfo;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.tables.UpdateTable;

public class RessourceAnzeige extends Anzeige {
    public RessourceAnzeige(RessourceInfo ressourceInfo) {
        super(new UpdateTable(Strategiespiel.skin) {
            @Override
            public void aktualisiereInformationen_or() {
                add("name: ").left();
                add(ressourceInfo.name).left();
                row();
                add("description: ").left();
                add(ressourceInfo.beschreibung).left();
            }
        });
    }
}
