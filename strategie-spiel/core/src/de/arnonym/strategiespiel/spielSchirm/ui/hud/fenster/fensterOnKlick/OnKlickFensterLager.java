package de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterOnKlick;

import com.badlogic.gdx.scenes.scene2d.ui.Skin;

import de.arnonym.strategiespiel.spielSchirm.World;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.gegenstaende.CompWirtschaftsdaten;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.GeldComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.name.NameComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.rendern.HashMapRenderComp;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterOnKlick.content.GegenstandTable;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterOnKlick.content.GeldTable;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.SpecialUtils;

public class OnKlickFensterLager extends OnKlickFenster {
    public OnKlickFensterLager(World world, GeldComp geldComp,
                               CompWirtschaftsdaten compWirtschaftsdaten,
                               NameComp nameComp, HashMapRenderComp hashMapRenderComp, Skin skin) {
        super(world, skin, 500, 700, nameComp, hashMapRenderComp);

        GeldTable geldTable = new GeldTable(skin, geldComp);
        getContentTable().add(geldTable).fill().expand();
        getUpdateTableList().add(geldTable);

        SpecialUtils.leerzeile(getContentTable());

        GegenstandTable gegenstandTable = new GegenstandTable(world.getIngameCursorManager(),
                compWirtschaftsdaten, true, true, this);
        getContentTable().add(gegenstandTable).fill().expand();
        getUpdateTableList().add(gegenstandTable);
    }
}
