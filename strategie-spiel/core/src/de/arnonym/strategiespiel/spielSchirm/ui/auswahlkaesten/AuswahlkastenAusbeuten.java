package de.arnonym.strategiespiel.spielSchirm.ui.auswahlkaesten;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.viewport.Viewport;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import de.arnonym.strategiespiel.framework.ecs.collections.System;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.ausbeuten.AusbeutbarComp;
import de.arnonym.strategiespiel.framework.ecs.collections.EcsList;
import de.arnonym.strategiespiel.spielSchirm.ui.IngameCursorManager;

public abstract class AuswahlkastenAusbeuten<A extends AusbeutbarComp> extends Auswahlkasten<A> {
    private final System<A> systemAusbeutbar;
    private final EcsList<A> auszubeutenList;

    public AuswahlkastenAusbeuten(Viewport worldViewport, IngameCursorManager ingameCursorManager,
                                  System<A> systemAusbeutbar, EcsList<A> auszubeutenList,
                                  TextureRegion moeglichkeitTextureRegion,
                                  TextureRegion auswahlTextureRegion) {
        super(worldViewport, ingameCursorManager, moeglichkeitTextureRegion, auswahlTextureRegion);
        this.systemAusbeutbar = systemAusbeutbar;
        this.auszubeutenList = auszubeutenList;
    }

    @Override
    protected List<A> gettMoeglichkeitenList() {
        return systemAusbeutbar.toCollection(new ArrayList<>());
    }

    @Override
    protected List<A> getAusgewaehltList() {
        return auszubeutenList.toList();
    }

    @Override
    protected boolean pruefeZeichnen(A a) {
        return a.renderComp.isVisible();
    }

    @Override
    protected void ausloesen_or(A a, boolean loeschen) {
        if (loeschen) {
            auszubeutenList.remove(a); //Manager.remove((Connector) a, auszubeutenList);
        } else if (!auszubeutenList.contains(a)) {
            auszubeutenList.add(a); //Manager.add((Connector) a, auszubeutenList);
        }
    }

    @Override
    protected Vector2 gettPosition(A a) {
        return a.bewegungszielComp.compKollisionLaufen.positionComp.getPosition();
    }

    @Override
    protected Vector2 gettGroesseInZellen(A a) {
        return a.bewegungszielComp.compKollisionLaufen.isoGroesseComp.getGroesseInZellen();
    }
}
