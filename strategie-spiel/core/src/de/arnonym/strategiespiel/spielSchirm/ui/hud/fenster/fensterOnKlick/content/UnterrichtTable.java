package de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterOnKlick.content;

import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.EmployerComp;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.departments.bildung.TeachDepartmentComp;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.labels.UpdateLabel;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.tables.UpdateTable;

public class UnterrichtTable extends UpdateTable {
    private final EmployerComp employerComp;
    private final TeachDepartmentComp teachDepartmentComp;

    public UnterrichtTable(EmployerComp employerComp, TeachDepartmentComp teachDepartmentComp, Skin skin) {
        super(skin);
        this.employerComp = employerComp;
        this.teachDepartmentComp = teachDepartmentComp;
    }

    @Override
    public void aktualisiereInformationen_or() {
        Label label = new Label("school teacher: ", getSkin());
        add(label).expandX().fillX().left();

        label = new AngestellteLabel(employerComp, getSkin());
        add(label).expandX().right();

        row();

        label = new Label("students: ", getSkin());
        add(label).expandX().fillX().left();

        label = new Label(teachDepartmentComp.schuelerList.size()
                + " / " + teachDepartmentComp.maxSchueler, getSkin());
        add(label).expandX().right();

        row();

        label = new Label("prepared lessons: ", getSkin());
        add(label).expandX().fillX().left();

        label = new Label(teachDepartmentComp.getVorbereitung() + " / "
                + teachDepartmentComp.getVorbereitungMax(), getSkin());
        add(label).expandX().right();
    }
}
