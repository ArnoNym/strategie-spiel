package de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.viewport.Viewport;

import java.util.ArrayList;
import java.util.List;

import de.arnonym.strategiespiel.spielSchirm.ui.IngameCursorManager;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.MoveableGroup;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.tables.UpdateTable;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.labels.UpdateLabel;
import de.arnonym.strategiespiel.spielSchirm.Assets;

public abstract class Fenster extends MoveableGroup {
    private final Label titelLabel;
    private Stack moveableButtonStack;
    private Stack standardpositionButtonStack;

    private final Table contentTable;

    private List<UpdateLabel> updateLabelList = new ArrayList<>();
    private List<UpdateTable> updateTableList = new ArrayList<>();

    public Fenster(Stage levelGuiStage, Viewport overlayViewport, Viewport worldViewport,
                   IngameCursorManager ingameCursorManager, Skin skin, NinePatch ninePatch,
                   float width, float height, Label titelLabel, boolean fensterSchliessbar) {
        super(levelGuiStage, overlayViewport, worldViewport, ingameCursorManager, skin, width,
                height);
        this.titelLabel = titelLabel;
        if (titelLabel instanceof UpdateLabel) {
            if (updateLabelList.contains(titelLabel)) throw new IllegalStateException();
            updateLabelList.add((UpdateLabel) titelLabel);
        }

        addListener(new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if(button == Input.Buttons.RIGHT){
                    if (isMoveable()) {
                        schliessen();
                    }
                    return true; //Damit nicht hindurch geklickt wird
                }
                return false;
            }
        });

        Table basisTable = new Table(skin);
        this.addActor(basisTable);
        basisTable.setFillParent(true);
        basisTable.setBackground(new NinePatchDrawable(ninePatch));

        basisTable.add(titelLabel).expandX().left();

        // Oben-Rechts Buttons

        Button buttonMoveable = new Button(new TextureRegionDrawable(Assets.instance.levelGuiAssets.fensterButtonMoveable));
        buttonMoveable.addListener(new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                settMoveable(false);
                return super.touchDown(event, x, y, pointer, button);
            }
        });
        Button buttonNotMoveable = new Button(new TextureRegionDrawable(Assets.instance.levelGuiAssets.fensterButtonNotMoveable));
        buttonNotMoveable.addListener(new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                settMoveable(true);
                return super.touchDown(event, x, y, pointer, button);
            }
        });
        moveableButtonStack = new Stack(buttonMoveable, buttonNotMoveable);
        basisTable.add(moveableButtonStack).top();


        Button buttonStandard = new Button(new TextureRegionDrawable(Assets.instance.levelGuiAssets
                .fensterPin)); //Texture wird in settStandardposition() gesetzt
        buttonStandard.addListener(new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                settStandardposition(false);
                return super.touchDown(event, x, y, pointer, button);
            }
        });
        Button buttonNotStandard = new Button(new TextureRegionDrawable(Assets.instance
                .levelGuiAssets.fensterPinDeaktiv)); //Texture wird in settStandardposition() gesetzt
        buttonNotStandard.addListener(new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                settStandardposition(true);
                return super.touchDown(event, x, y, pointer, button);
            }
        });
        standardpositionButtonStack = new Stack(buttonStandard, buttonNotStandard);
        basisTable.add(standardpositionButtonStack).top();

        if (fensterSchliessbar) {
            TextureRegion r = Assets.instance.levelGuiAssets.fensterSchliessenKreuz;
            Button button = new Button(new TextureRegionDrawable(r));
            button.addListener(new ClickListener() {
                @Override
                public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                    Fenster.this.schliessen();
                    return super.touchDown(event, x, y, pointer, button);
                }
            });
            basisTable.add(button).top();
        }

        basisTable.row();

        // Scrollbare Content Table
        contentTable = new Table(skin);
        ScrollPane scrollPane = new ScrollPane(contentTable, skin);
        scrollPane.setFadeScrollBars(false);
        basisTable.add(scrollPane).expand().fill().colspan(100);

        // Einstellungen
        settMoveable(true); /* Funktionen, da sie veraendern welche Buttons angezeigt werden.
        Die Reihenfolge der Buttons in den Stacks koennte auch vertauscht werden, allerdings wird
        sich dabei auf den Staus = true in der MoveableGroup verlassen */
        settStandardposition(true);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        for (UpdateLabel l : updateLabelList) {
            l.aktualisiereInformationen();
        }
        for (UpdateTable t : updateTableList) {
            t.aktualisiereInformationen();
        }
        super.draw(batch, parentAlpha);
    }

    @Override
    public void schliessen() {
        getStage().unfocus(this);
        super.schliessen();
    }

    // KOMPLEXE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    @Override
    public void settMoveable(Boolean moveable) {
        moveableButtonStack.getChildren().get(0).setVisible(moveable);
        moveableButtonStack.getChildren().get(1).setVisible(!moveable);
        super.settMoveable(moveable);
    }

    @Override
    public void settStandardposition(boolean standardposition) {
        standardpositionButtonStack.getChildren().get(0).setVisible(standardposition);
        standardpositionButtonStack.getChildren().get(1).setVisible(!standardposition);
        super.settStandardposition(standardposition);
    }

    public void settTitel(String fensterTitel) {
        titelLabel.setText(fensterTitel);
    }

    // EINFACHE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public Table getContentTable() {
        return contentTable;
    }

    public List<UpdateLabel> getUpdateLabelList() {
        return updateLabelList;
    }

    public List<UpdateTable> getUpdateTableList() {
        return updateTableList;
    }
}
