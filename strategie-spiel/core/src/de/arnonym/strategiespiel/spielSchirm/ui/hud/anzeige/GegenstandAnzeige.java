package de.arnonym.strategiespiel.spielSchirm.ui.hud.anzeige;

import java.util.Map;

import de.arnonym.strategiespiel.Strategiespiel;
import de.arnonym.strategiespiel.spielSchirm.balancing.IdsGegenstaende;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.tables.UpdateTable;
import de.arnonym.strategiespiel.spielSchirm.info.muster.GegInfo;

public class GegenstandAnzeige extends Anzeige {
    public GegenstandAnzeige(GegInfo gegInfo) {
        super(new UpdateTable(Strategiespiel.skin) {
            @Override
            public void aktualisiereInformationen_or() {
                add("name: ").left();
                add(gegInfo.name).left();
                row();
                add("description: ").left();
                add(gegInfo.beschreibung).left();
                row();
                if (gegInfo.isHerstellbar()) {
                    add("");
                    row();
                    add("input: ").left();
                    row();
                    for (Map.Entry<Integer, Integer> entry : gegInfo.getMusterAuftragHerstellen()
                            .idMengeHashMap.entrySet()) {
                        int inputId = entry.getKey();
                        int menge = entry.getValue();
                        add(IdsGegenstaende.get(inputId).name + ", " + menge).left();
                        row();
                    }
                }
            }
        });
    }
}
