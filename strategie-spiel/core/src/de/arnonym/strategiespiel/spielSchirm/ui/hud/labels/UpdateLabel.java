package de.arnonym.strategiespiel.spielSchirm.ui.hud.labels;

import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

import java.util.List;

public abstract class UpdateLabel extends Label {

    public UpdateLabel(Skin skin) {
        super("null", skin); //Bei Bedarf weitere Konstuktoren einfuegen
        //aktualisiereInformationen();
    }

    public void aktualisiereInformationen() {
        setText(aktualisiereInformationenOverride());
    }

    public abstract String aktualisiereInformationenOverride();
}
