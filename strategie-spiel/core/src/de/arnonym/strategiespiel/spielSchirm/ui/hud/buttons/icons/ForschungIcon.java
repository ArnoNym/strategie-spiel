package de.arnonym.strategiespiel.spielSchirm.ui.hud.buttons.icons;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;

import de.arnonym.strategiespiel.spielSchirm.Dorf;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.EmployerComp;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.VarEmployerComp;
import de.arnonym.strategiespiel.spielSchirm.gebaude.GebaudeInfo;
import de.arnonym.strategiespiel.spielSchirm.info.forschung.Forschungsstand;
import de.arnonym.strategiespiel.spielSchirm.info.muster.Info;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.anzeige.Anzeige;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.anzeige.ForschungAnzeige;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.anzeige.GebaudeAnzeige;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.anzeige.GegenstandAnzeige;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.AnzeigeFenster;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.Fenster;
import de.arnonym.strategiespiel.spielSchirm.info.muster.GegInfo;
import de.arnonym.strategiespiel.spielSchirm.Assets;
import de.arnonym.strategiespiel.spielSchirm.Enums;
import de.arnonym.strategiespiel.framework.werkzeuge.MyClickListener;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.TextureUtils;

public class ForschungIcon extends Icon {
    private final Info info;
    private final Forschungsstand forschungsstand;

    public ForschungIcon(Dorf dorf, Fenster iconsParentFenster, AnzeigeFenster anzeigeFenster,
                         VarEmployerComp<Info> forschungEmployerComp, Info info) {
        super(iconsParentFenster.ingameCursorManager, iconsParentFenster, info.gettId());
        this.info = info;
        this.forschungsstand = dorf.forschungsstandList.gettDurchId(info.id);

        addListener(new MyClickListener() {
            @Override
            public boolean touchDown_or(InputEvent event, float x, float y, int pointer, int button) {
                if (!forschungsstand.isErforscht()) {
                    if (forschungsstand.issWirdErforscht()) {
                        forschungEmployerComp.setProduct(null);
                    } else {
                        forschungEmployerComp.setProduct(info);
                    }
                }
                return true;
            }

            @Override
            public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
                Anzeige zweiteAnzeige;
                if (info instanceof GebaudeInfo) {
                    zweiteAnzeige = new GebaudeAnzeige(dorf, (GebaudeInfo) info);
                } else if (info instanceof GegInfo) {
                    zweiteAnzeige = new GegenstandAnzeige((GegInfo) info);
                } else {
                    throw new IllegalStateException();
                }
                anzeigeFenster.aktiviereHover(iconsParentFenster,
                        new ForschungAnzeige(dorf, info),
                        zweiteAnzeige);
                super.enter(event, x, y, pointer, fromActor);
            }

            @Override
            public void exit_or(InputEvent event, float x, float y, int pointer, Actor toActor) {
                anzeigeFenster.deaktiviereHover();
            }
        });
    }

    @Override
    public void draw_or(Batch batch, float parentAlpha) {
        boolean erforscht = forschungsstand.isErforscht();

        if (info instanceof GebaudeInfo) {
            renderWorldTextureRegion((SpriteBatch) batch, ((GebaudeInfo) info).textureHashMap
                    .get(Enums.EnumAktion.STANDARD_PRIMAERMENGE_VOLL).get());
        } else {
            renderWorldTextureRegion((SpriteBatch) batch, ((GegInfo) info).texture.get());
        }

        if (!erforscht) {
            TextureUtils.zeichneTexturRegion(batch, Assets.instance.iconAssets.iconRahmenKlick,
                    getX(), getY(), 1, 1, false);
        }

        TextureUtils.zeichneTexturRegion(batch, Assets.instance.iconAssets.iconWissenschaft,
                getX(), getY(), 0.2f, 0.2f, false);

        if (erforscht) {
            TextureUtils.zeichneTexturRegion(batch, Assets.instance.iconAssets.iconRahmenHaken,
                    getX(), getY(), 1, 1, false);
        }

        if (forschungsstand.issWirdErforscht()) {
            TextureUtils.zeichneTexturRegion(batch, Assets.instance.iconAssets.iconWissenschaft,
                    getX(), getY(), 1, 1, false); // todo wird in diesem oder anemdem gebaud eerforscht
        }
    }
}
