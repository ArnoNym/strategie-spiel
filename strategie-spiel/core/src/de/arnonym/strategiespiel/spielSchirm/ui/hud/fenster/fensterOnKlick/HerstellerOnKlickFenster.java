package de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterOnKlick;

import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

import java.util.ArrayList;
import java.util.List;

import de.arnonym.strategiespiel.spielSchirm.World;
import de.arnonym.strategiespiel.spielSchirm.arbeit.TimeClock;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.EmployerComp;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.ProgressDepartmentComp;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.VarEmployerComp;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.departments.material.HerstellerDepartmentComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.gegenstaende.CompWirtschaftsdaten;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.GeldComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.name.NameComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.rendern.HashMapRenderComp;
import de.arnonym.strategiespiel.spielSchirm.info.forschung.Forschungsstand;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.anzeige.GegenstandAnzeige;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.buttons.icons.ProductIcon;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterOnKlick.content.FortschrittLabel;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.labels.UpdateLabel;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterOnKlick.content.GegenstandTable;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterOnKlick.content.GeldTable;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterOnKlick.content.IconTable;
import de.arnonym.strategiespiel.spielSchirm.info.muster.GegInfo;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.SpecialUtils;

public class HerstellerOnKlickFenster extends OnKlickFenster {
    public HerstellerOnKlickFenster(World world,

                                    VarEmployerComp<GegInfo> herstellerEmployerComp,
                                    ProgressDepartmentComp<GegInfo, ?> progressDepartmentComp,
                                    GeldComp geldComp, NameComp nameComp,
                                    HashMapRenderComp hashMapRenderComp,
                                    CompWirtschaftsdaten compWirtschaftsdaten,

                                    Forschungsstand forschungsstand,
                                    Skin skin) {
        super(world, skin, 420, 600, nameComp, hashMapRenderComp, geldComp, compWirtschaftsdaten);

        Label label = new Label("staff: ", skin);
        getContentTable().add(label).expandX().fillX().left();
        UpdateLabel updateLabel = new UpdateLabel(skin) {
            @Override
            public String aktualisiereInformationenOverride() {
                return herstellerEmployerComp.getAnzahlAngestellte()
                        + " / " + herstellerEmployerComp.getMaxStellen();
            }
        };
        getContentTable().add(updateLabel);
        getUpdateLabelList().add(updateLabel);

        SpecialUtils.leerzeile(getContentTable());

        List<ProductIcon<GegInfo>> iconList = new ArrayList<>();
        for (GegInfo gegInfo : herstellerEmployerComp.possibleProducts) {
            iconList.add(new ProductIcon<>(ingameCursorManager, this,
                    new GegenstandAnzeige(gegInfo), herstellerEmployerComp, gegInfo,
                    forschungsstand));
        }
        IconTable iconTable = new IconTable(skin, 5, 5, 5,
                iconList.toArray(new ProductIcon[iconList.size()]));
        getContentTable().add(iconTable).expandX().fillX().left();
        getUpdateTableList().add(iconTable);

        SpecialUtils.leerzeile(getContentTable());

        updateLabel = new FortschrittLabel(progressDepartmentComp.timeClock, skin);
        getContentTable().add(updateLabel).expandX().fillX().left();
        getUpdateLabelList().add(updateLabel);

        SpecialUtils.leerzeile(getContentTable());

        GeldTable geldTable = new GeldTable(skin, geldComp);
        getContentTable().add(geldTable).expandX().fillX().left();
        getUpdateTableList().add(geldTable);

        SpecialUtils.leerzeile(getContentTable());

        GegenstandTable gegenstandTable = new GegenstandTable(ingameCursorManager,
                compWirtschaftsdaten, false, false, this);
        getContentTable().add(gegenstandTable).expandX().fillX().left();
        getUpdateTableList().add(gegenstandTable);
    }
}
