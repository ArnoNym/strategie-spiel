package de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterChild.FensterChildBaumenue;

import com.badlogic.gdx.scenes.scene2d.ui.Skin;

import de.arnonym.strategiespiel.spielSchirm.Dorf;
import de.arnonym.strategiespiel.spielSchirm.World;
import de.arnonym.strategiespiel.spielSchirm.balancing.IdsGebaude;
import de.arnonym.strategiespiel.spielSchirm.forschungUndWissen.schule.SchuleFactory;
import de.arnonym.strategiespiel.spielSchirm.forschungUndWissen.schule.SchuleInfo;
import de.arnonym.strategiespiel.spielSchirm.ui.Blaupause;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.MoveableGroup;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.buttons.icons.IconGebaude;

public class FensterChildBaumenueForschung extends FensterChildBaumenue {
    public FensterChildBaumenueForschung(World world, Blaupause blaupause,
                                         MoveableGroup ausklappenParentMoveableGroup, Skin skin) {
        super(world, blaupause, ausklappenParentMoveableGroup, skin, 300, 300,
                "science building menue");

        Dorf spielerDorf = world.spielerDorf;
        SchuleInfo schuleInfo = (SchuleInfo) IdsGebaude.get(IdsGebaude.GEB_BER_GRUNDSCHULE_ID);
        IconGebaude iconGebaude = new IconGebaude(ausklappenParentMoveableGroup, this,
                blaupause, new SchuleFactory(world, spielerDorf, schuleInfo));

        getContentTable().add(iconGebaude);
    }
}
