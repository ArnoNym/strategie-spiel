package de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterOnKlick.content;

import com.badlogic.gdx.scenes.scene2d.ui.Label;

import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.gegenstaende.GegenstaendeComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.gegenstaende.CompWirtschaftsdaten;
import de.arnonym.strategiespiel.spielSchirm.ui.IngameCursorManager;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.MoveableGroup;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.buttons.icons.IconGegenstand;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.tables.UpdateTable;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.endstufe.Gegenstand;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.endstufe.GegenstandList;
import de.arnonym.strategiespiel.spielSchirm.zahlen.wirtschaftsdaten.gegenstaende.GegenstandManager;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.SpecialUtils;

public class GegenstandTable extends UpdateTable {
    public final IngameCursorManager ingameCursorManager;
    private final GegenstandManager gegenstandManager_nl;
    public final GegenstaendeComp gegenstaendeComp;
    private final boolean zeigePreise;
    private final boolean zeigeEmpty;
    private final MoveableGroup parentFenster;

    public GegenstandTable(IngameCursorManager ingameCursorManager,
                           CompWirtschaftsdaten compWirtschaftsdaten, boolean zeigePreise,
                           boolean zeigeEmpty, MoveableGroup parentFenster) {
        this(ingameCursorManager, compWirtschaftsdaten.gegenstandManager,
                compWirtschaftsdaten.gegenstaendeComp, zeigePreise, zeigeEmpty, parentFenster);
    }

    public GegenstandTable(IngameCursorManager ingameCursorManager,
                           GegenstaendeComp gegenstaendeComp, boolean zeigeEmpty,
                           MoveableGroup parentFenster) {
        this(ingameCursorManager, null, gegenstaendeComp, false, zeigeEmpty,
                parentFenster);
    }

    private GegenstandTable(IngameCursorManager ingameCursorManager,
                            GegenstandManager gegenstandManager_nl,
                            GegenstaendeComp gegenstaendeComp, boolean zeigePreise,
                            boolean zeigeEmpty, MoveableGroup parentFenster) {
        super(parentFenster.skin);
        this.ingameCursorManager = ingameCursorManager;
        this.gegenstandManager_nl = gegenstandManager_nl;
        this.gegenstaendeComp = gegenstaendeComp;
        this.zeigePreise = zeigePreise;
        this.zeigeEmpty = zeigeEmpty;
        this.parentFenster = parentFenster;
    }

    @Override
    public void aktualisiereInformationen_or() {
        GegenstandList gegenstandListe = gegenstaendeComp.gegenstandListe;

        clearChildren();

        add(new Label("occupied space: ", getSkin())).expandX().left();
        int maxPlatz = gegenstandListe.getMaxPlatz();
        add(new Label(maxPlatz - gegenstandListe.getPlatz()+" / "+maxPlatz, getSkin())).left();

        SpecialUtils.leerzeile(this);

        if (gegenstandManager_nl == null) {
            erstelleHeader("objects:", true, "icon / ", "available");
        } else {
            if (!zeigePreise) {
                erstelleHeader("objects:", true,
                        "icon / ", "available / ", "input / ", "output");
            } else {
                erstelleHeader("objects:", true,
                        "icon / ", "available / ", "input / ", "output / ", "price");
            }
        }

        row();

        boolean mindestensEinGegGeaddet = false;
        for (Gegenstand g : gegenstandListe) {
            mindestensEinGegGeaddet = mindestensEinGegGeaddet | createGegZeile(g);
            row();
        }
        if (!mindestensEinGegGeaddet) {
            add(new Label("empty ...", getSkin()));
            row();
        }
    }

    private boolean createGegZeile(Gegenstand g) {
        int mengeVerfuegbar = g.getMengeVerfuegbar();

        if (!zeigeEmpty && mengeVerfuegbar == 0) {
            return false;
        }

        add(new IconGegenstand(ingameCursorManager, parentFenster, g.gegMuster)).left();
        add(new Label("" + mengeVerfuegbar, getSkin()));

        if (gegenstandManager_nl == null) {
            return true;
        }

        add(new Label("" + gegenstandManager_nl.gettSummeEingang(g.gettId(), 5), getSkin())); // TODO Konstanten
        add(new Label("" + gegenstandManager_nl.gettSummeAusgang(g.gettId(), 5), getSkin())); // TODO Konstanten

        if (!zeigePreise) {
            return true;
        }

        add(new Label("" + g.getWert(), getSkin()));

        return true;
    }
}
