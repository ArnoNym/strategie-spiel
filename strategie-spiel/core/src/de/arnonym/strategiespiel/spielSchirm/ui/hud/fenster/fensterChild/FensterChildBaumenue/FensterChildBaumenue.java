package de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterChild.FensterChildBaumenue;

import com.badlogic.gdx.scenes.scene2d.ui.Skin;

import de.arnonym.strategiespiel.spielSchirm.World;
import de.arnonym.strategiespiel.spielSchirm.ui.Blaupause;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.MoveableGroup;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.AnzeigeFenster;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterChild.FensterChild;
import de.arnonym.strategiespiel.spielSchirm.Enums;

public abstract class FensterChildBaumenue extends FensterChild {
    private final AnzeigeFenster anzeigeFensterBauen;
    private final Blaupause blaupause;
    private MoveableGroup parentMoveableGroup;

    public FensterChildBaumenue(World world, final Blaupause blaupause,
                                MoveableGroup ausklappenParentMoveableGroup, Skin skin,
                                float width, float height, String fensterTitel) {
        super(world, ausklappenParentMoveableGroup, Enums.EnumRichtung.OBEN, skin, width, height,
                fensterTitel);
        this.anzeigeFensterBauen = ingameCursorManager.getAnzeigeFenster();
        this.blaupause = blaupause;
        this.parentMoveableGroup = ausklappenParentMoveableGroup;
    }

    public AnzeigeFenster getAnzeigeFensterBauen() {
        return anzeigeFensterBauen;
    }

    /*protected ClickListener gebaudeIconClickListener(int gebaudeId) {
        return new ClickListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                blaupause.planen(gebaudeId);
                parentMoveableGroup.schliessen();
                FensterChildBaumenue.this.schliessen();
                return super.touchDown(event, x, y, pointer, button);
            }
            @Override
            public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
                FensterChildBaumenue.this.fensterBauinformationen.aktiviereInfos(FensterChildBaumenue.this, gebaudeId);
                super.enter(event, x, y, pointer, fromActor);
            }
            @Override
            public void exit(InputEvent event, float x, float y, int pointer, Actor toActor) {
                FensterChildBaumenue.this.fensterBauinformationen.deaktiviereInformationenFuer();
                super.exit(event, x, y, pointer, toActor);
            }
        };
    }*/
}
