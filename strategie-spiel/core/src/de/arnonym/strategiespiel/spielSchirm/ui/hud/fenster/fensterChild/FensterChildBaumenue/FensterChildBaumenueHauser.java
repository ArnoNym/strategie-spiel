package de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterChild.FensterChildBaumenue;

import com.badlogic.gdx.scenes.scene2d.ui.Skin;

import de.arnonym.strategiespiel.spielSchirm.Dorf;
import de.arnonym.strategiespiel.spielSchirm.World;
import de.arnonym.strategiespiel.spielSchirm.gebaude.haus.HausFactory;
import de.arnonym.strategiespiel.spielSchirm.gebaude.haus.HausInfo;
import de.arnonym.strategiespiel.spielSchirm.ui.Blaupause;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.MoveableGroup;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.buttons.icons.IconGebaude;
import de.arnonym.strategiespiel.spielSchirm.balancing.IdsGebaude;

public class FensterChildBaumenueHauser extends FensterChildBaumenue {
    public FensterChildBaumenueHauser(World world, MoveableGroup ausklappenParentMoveableGroup,
                                      Blaupause blaupause, Skin skin) {
        super(world, blaupause, ausklappenParentMoveableGroup, skin, 200, 200,
                "houses building menue");

        Dorf spielerDorf = world.spielerDorf;
        HausInfo hausInfo = (HausInfo) IdsGebaude.get(IdsGebaude.GEB_HAUS_EINFACH_ID);
        IconGebaude iconGebaude = new IconGebaude(ausklappenParentMoveableGroup, this,
                blaupause, new HausFactory(world, spielerDorf, hausInfo));

        getContentTable().add(iconGebaude);
    }
}
