package de.arnonym.strategiespiel.spielSchirm.ui.hud.menue;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;
import com.badlogic.gdx.utils.viewport.Viewport;

import java.util.ArrayList;
import java.util.List;

import de.arnonym.strategiespiel.spielSchirm.ui.IngameCursorManager;
import de.arnonym.strategiespiel.spielSchirm.World;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.MoveableGroup;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.buttons.IconAusklappButton;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.chat.FensterChat;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterChild.FensterChildDorf;
import de.arnonym.strategiespiel.spielSchirm.Assets;
import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenOptik;

public class IngameTaskLeiste extends MoveableGroup {
    private final List<MoveableGroup> umPlatzKonkurrierendeFensterListe = new ArrayList<>(); /* Man
    koennte auch ueber die Children der Tabelle (die Buttons) auf die Fenster kommen. Dann ist das
    Fenster aber nicht emhr so leicht modifizierbar */

    public IngameTaskLeiste(World world, Stage levelGuiStage, Viewport worldViewport,
                            Viewport overlayViewport, IngameCursorManager ingameCursorManager,
                            FensterChat fensterChat, Skin skin) {
        super(levelGuiStage, overlayViewport, worldViewport, ingameCursorManager, skin, 400, 100);

        settPosition(new Vector2(0, 0));

        addListener(new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if (button == Input.Buttons.RIGHT) {
                    return true; //Um zu verhindern, dass ein rechtsklick zB das Beumenue oeffnet etc. Links wird in MoveableGroup aufgehalten
                }
                return super.touchDown(event, x, y, pointer, button);
            }
        });

        Table table = new Table(skin);
        this.addActor(table);
        table.setFillParent(true);
        table.setBackground(new NinePatchDrawable(new NinePatch(
                Assets.instance.levelGuiAssets.auswahlKastenA,
                KonstantenOptik.AUSWAHLKASTEN_ECKEN_BREITE,
                KonstantenOptik.AUSWAHLKASTEN_ECKEN_BREITE,
                KonstantenOptik.AUSWAHLKASTEN_ECKEN_HOEHE,
                KonstantenOptik.AUSWAHLKASTEN_ECKEN_HOEHE)));

        TextureRegion r = Assets.instance.iconAssets.iconHaus;
        table.add(new IconAusklappButton(this, fensterChat, umPlatzKonkurrierendeFensterListe, r));
        umPlatzKonkurrierendeFensterListe.add(fensterChat);

        FensterChildDorf fenster = new FensterChildDorf(world, this, skin);
        table.add(new IconAusklappButton(this, fenster, umPlatzKonkurrierendeFensterListe, r));
        umPlatzKonkurrierendeFensterListe.add(fenster);
    }

    // EINFACHE GETTER UND SETTER /////////////////////////////////////////////////////////////////
}
