package de.arnonym.strategiespiel.spielSchirm.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.viewport.Viewport;

import java.util.HashSet;

import de.arnonym.strategiespiel.framework.werkzeuge.CursorManger;
import de.arnonym.strategiespiel.spielSchirm.World;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.kollision.KollisionComp;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.AnzeigeFenster;
import de.arnonym.strategiespiel.spielSchirm.worldMap.WorldMap;
import de.arnonym.strategiespiel.spielSchirm.worldMap.Zelle;
import de.arnonym.strategiespiel.spielSchirm.Assets;
import de.arnonym.strategiespiel.spielSchirm.Enums;

public class IngameCursorManager extends InputAdapter {
    private final CursorManger cursorManger;
    private final Viewport worldViewport;
    private final Viewport overlayViewport;
    private final WorldMap worldMap;
    private final ZellenMarkierungRenderer zellenMarkierungRenderer;

    private final AnzeigeFenster anzeigeFenster;

    private Enums.EnumIngameWorldCursorStatus cursorStatus = Enums.EnumIngameWorldCursorStatus
            .ZELLE_UND_GEBAUDE_UND_CURSOR;
    private boolean ueberUi = false;

    public IngameCursorManager(World world, CursorManger cursorManger, WorldMap worldMap,
                               Viewport worldViewport, Viewport overlayViewport,
                               ZellenMarkierungRenderer zellenMarkierungRenderer, Skin skin) {
        this.cursorManger = cursorManger;
        this.worldViewport = worldViewport;
        this.overlayViewport = overlayViewport;
        this.worldMap = worldMap; //Durch setter, da es sonst mit der create() Reihenfolge in Level nicht passt
        this.zellenMarkierungRenderer = zellenMarkierungRenderer;

        anzeigeFenster = new AnzeigeFenster(world.getGuiStage(), overlayViewport, worldViewport,
                this, skin);
        anzeigeFenster.setVisible(false);
    }

    public void update() {
        if (ueberUi) {
            cursorManger.sett(Enums.EnumCursor.PFEIL);
            return;
        }
        switch (cursorStatus) {
            case NICHTS:
                cursorManger.sett(Enums.EnumCursor.UNSICHTBAR);
                return;
            case ZELLE:
            case ZELLE_UND_FLACHEN:
                cursorManger.sett(Enums.EnumCursor.UNSICHTBAR);
                cursorZelleMarkieren(cursorStatus == Enums.EnumIngameWorldCursorStatus.ZELLE_UND_FLACHEN);
                return;
            case ZELLE_UND_GEBAUDE_UND_CURSOR:
                cursorManger.sett(Enums.EnumCursor.PFEIL);
                cursorZelleMarkieren(true);
                return;
        }
        throw new IllegalStateException();
    }

    private void cursorZelleMarkieren(boolean auchFlaechen) {
        Vector2 mausIsoPosition = gettMausWorldPosisiton();

        Zelle z = worldMap.gett(mausIsoPosition); // TODO ersetzten durch GeometryUtils gettZellenPosition(Vector2 isoPosition)
        if (z == null) {
            return;
        }

        if (auchFlaechen) {
            if (z.isKollision()) {
                for (KollisionComp kollisionComp : z.onCellKollisionComps()) {
                    zellenMarkierungRenderer.add(kollisionComp.positionComp.getPosition(),
                            kollisionComp.isoGroesseComp.getGroesseInZellen(),
                            Assets.instance.tiledMapAssets.durchsichtigWeiss, false);
                }
                return;
            }
        }

        zellenMarkierungRenderer.add(z.getRenderComp().getPositionComp().getPosition(),
                Assets.instance.tiledMapAssets.durchsichtigWeiss, true);
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) { /* Muss vor GUI gecallt werden waehrend
        MyInputLeftover nach GUI gecallt werden muss. Deswegen als eigener Input processor. */
        ueberUi = false;
        return super.mouseMoved(screenX, screenY);
    }

    // KOMPLEXE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public Vector2 gettMausScreenPosisiton() {
        return new Vector2(Gdx.input.getX(), Gdx.input.getY());
    }

    public Vector2 gettMausOverlayPosition() {
        return overlayViewport.unproject(gettMausScreenPosisiton());
    }

    public Vector2 gettMausWorldPosisiton() {
        return worldViewport.unproject(gettMausScreenPosisiton());
    }

    // EINFACHE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public void setCursorStatus(Enums.EnumIngameWorldCursorStatus cursorStatus) {
        this.cursorStatus = cursorStatus;
    }

    public void setMausUeberUi(boolean ueberUi) {
        this.ueberUi = ueberUi;
    }

    public ZellenMarkierungRenderer getZellenMarkierungRenderer() {
        return zellenMarkierungRenderer;
    }

    public AnzeigeFenster getAnzeigeFenster() {
        return anzeigeFenster;
    }

    public boolean isUeberUi() {
        return ueberUi;
    }
}
