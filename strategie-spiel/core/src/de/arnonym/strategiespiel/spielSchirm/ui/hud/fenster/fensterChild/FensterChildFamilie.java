package de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterChild;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import de.arnonym.strategiespiel.spielSchirm.World;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dorfbewohner.familie.Familie;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.buttons.MyButton;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.labels.UpdateLabel;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.tables.UpdateTable;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.taschen.MasterTasche;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.taschen.NahrungTasche;
import de.arnonym.strategiespiel.spielSchirm.Enums;

public class FensterChildFamilie extends FensterChild {
    private final World world;
    private final Familie familie;

    public FensterChildFamilie(World world, Familie familie, Skin skin) {
        super(world, null, Enums.EnumRichtung.RECHTS, skin, 400, 400,
                "Family "+familie.nameComp.getName());
        this.world = world;
        this.familie = familie;

        initNonDeveloper();
        /*
        if (KonstantenBalancing.DEVELOPER) {
            initDeveloper();
        } else {
            initNonDeveloper();
        }
        */
    }

    private void initNonDeveloper() {
        Label label;
        UpdateLabel updateLabel;
        MasterTasche masterTasche = familie.tascheComp.getMasterTasche();
        NahrungTasche nahrungTasche = masterTasche.getNahrungTasche_nl();

        label = new Label("Money: ", skin);
        getContentTable().add(label).left();
        updateLabel = new UpdateLabel(skin) {
            @Override
            public String aktualisiereInformationenOverride() {
                return ""+familie.mieterComp.geldComp.getGeld().getMengeVerfuegbar();
            }
        };
        getUpdateLabelList().add(updateLabel);
        getContentTable().add(updateLabel).expand().right();

        getContentTable().row();

        label = new Label("Happiness: ", skin);
        getContentTable().add(label).left();
        updateLabel = new UpdateLabel(skin) {
            @Override
            public String aktualisiereInformationenOverride() {
                return ""+familie.mieterComp.geldComp.getGeld().getMengeVerfuegbar();
            }
        };
        getUpdateLabelList().add(updateLabel);
        getContentTable().add(updateLabel).expand().right();

        getContentTable().row();

        label = new Label("food: ", skin);
        getContentTable().add(label).left();
        updateLabel = new UpdateLabel(skin) {
            @Override
            public String aktualisiereInformationenOverride() {
                return ""+nahrungTasche.gettMenge(true, false, true);
            }
        };
        getUpdateLabelList().add(updateLabel);
        getContentTable().add(updateLabel).expand().right();

        getContentTable().row();

        label = new Label("vitamins: ", skin);
        getContentTable().add(label).left();
        updateLabel = new UpdateLabel(skin) {
            @Override
            public String aktualisiereInformationenOverride() {
                return ""+nahrungTasche.gettVitamine();
            }
        };
        getUpdateLabelList().add(updateLabel);
        getContentTable().add(updateLabel).expand().right();

        getContentTable().row();

        label = new Label("einweiss: ", skin);
        getContentTable().add(label).left();
        updateLabel = new UpdateLabel(skin) {
            @Override
            public String aktualisiereInformationenOverride() {
                return ""+nahrungTasche.gettEiweiss();
            }
        };
        getUpdateLabelList().add(updateLabel);
        getContentTable().add(updateLabel).expand().right();

        getContentTable().row();

        label = new Label("kohlenhydrate_gesund: ", skin);
        getContentTable().add(label).left();
        updateLabel = new UpdateLabel(skin) {
            @Override
            public String aktualisiereInformationenOverride() {
                return ""+nahrungTasche.gettKohlenhydrate_gesund();
            }
        };
        getUpdateLabelList().add(updateLabel);
        getContentTable().add(updateLabel).expand().right();

        getContentTable().row();

        label = new Label("kohlenhydrate_umgesund: ", skin);
        getContentTable().add(label).left();
        updateLabel = new UpdateLabel(skin) {
            @Override
            public String aktualisiereInformationenOverride() {
                return ""+nahrungTasche.gettKohlenhydrate_ungesund();
            }
        };
        getUpdateLabelList().add(updateLabel);
        getContentTable().add(updateLabel).expand().right();

        getContentTable().row();

        label = new Label("toys: ", skin);
        getContentTable().add(label).left();
        updateLabel = new UpdateLabel(skin) {
            @Override
            public String aktualisiereInformationenOverride() {
                return ""+masterTasche.getSpielzeugTasche_nl().gettMenge(true, false, true);
            }
        };
        getUpdateLabelList().add(updateLabel);
        getContentTable().add(updateLabel).expand().right();

        getContentTable().row();

        label = new Label("clothes and tools: ", skin);
        getContentTable().add(label).left();
        updateLabel = new UpdateLabel(skin) {
            @Override
            public String aktualisiereInformationenOverride() {
                return ""+masterTasche.getWerkzeugTasche_nl().gettMenge(true, false, true);
            }
        };
        getUpdateLabelList().add(updateLabel);
        getContentTable().add(updateLabel).expand().right();

        getContentTable().row();

        label = new Label("Home: ", skin);
        getContentTable().add(label).left();
        updateLabel = new UpdateLabel(skin) {
            @Override
            public String aktualisiereInformationenOverride() {
                /* TODO if (familie.getZuhause() != null) {
                    return ""+familie.getZuhause().getClass().getSimpleName();
                }
                else return "Homeless";*/ return "TODO";
            }
        };
        getUpdateLabelList().add(updateLabel);
        getContentTable().add(updateLabel).expand().right();

        MyButton myButton = new MyButton(new Label("Visit home", skin), skin);
        myButton.addListener(new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                /* TODO
                GebaudeHaus famZuhause = familie.getZuhause();
                if (famZuhause != null) { //TODO Button ausblenden wenn kein Zuhause
                    if (myButton.isDoubleTap()) {
                        famZuhause.onKlickFenster(true);
                        level.getIngameKamera().settPosition(famZuhause.getPosition());
                    } else {
                        famZuhause.onKlickFensterUmschalten();
                    }
                }*/
                return super.touchDown(event, x, y, pointer, button);
            }
        });
        getContentTable().add(myButton);

        getContentTable().row();

        UpdateTable familienmitgliederUpdateTable = initFamilienmtgliederTable();
        getUpdateTableList().add(familienmitgliederUpdateTable);
        getContentTable().add(familienmitgliederUpdateTable).expand().fill().colspan(100);
    }

    private void initDeveloper() {
        Label label;
        UpdateLabel updateLabel;

        initInnereTabelle_Konsum(familie);
    }

    private UpdateTable initFamilienmtgliederTable() {
        return new UpdateTable() {
            @Override
            public void aktualisiereInformationen_or() {
                /*TODO Skin skin = FensterChildFamilie.this.getSkin();
                Label label;

                label = new Label("Familienmitglieder: ", skin);
                handle(label).left().colspan(100);
                row();
                label = new Label("Name", skin);
                handle(label).expandX().left();
                label = new Label("Gender", skin);
                handle(label).expandX().left();
                label = new Label("Age", skin);
                handle(label).expandX().left();
                row();
                for (CompFamilienmitglied cFm : FensterChildFamilie.this.familie.getFamilienmitgliedList()) {
                    label = new Label(cFm.compName, skin);
                    handle(label).left();
                    String geschlecht = "male";
                    if (cFm.isFemale()) geschlecht = "female";
                    label = new Label(geschlecht, skin);
                    handle(label).left();
                    label = new Label("TODO", skin); //TODO Alter
                    handle(label).left();
                    label = new Label("Visit "+cFm.getVorname(), skin);
                    MyButton myButton = new MyButton(label, skin);
                    myButton.addListener(new ClickListener() {
                        @Override
                        public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                            if (myButton.isDoubleTap()) {
                                cFm.onKlickFenster(true);
                                level.getIngameKamera().settPosition(cFm.getPosition());
                            } else {
                                cFm.onKlickFensterUmschalten();
                            }
                            return super.touchDown(event, x, y, pointer, button);
                        }
                    });
                    handle(myButton).right();
                    row();
                }*/
            }
        };
    }

    private void initInnereTabelle_Konsum(Familie familie) {
        Table innerTable = new Table();
        getContentTable().add(innerTable).expand().fill().colspan(100);

        Label label;
        UpdateLabel updateLabel;

        label = new Label("", skin);
        innerTable.add(label).expandX().left();
        label = new Label("Consumption", skin);
        innerTable.add(label);
        label = new Label("Total", skin);
        innerTable.add(label);

        innerTable.row();

        /* AUSGEKLAMMERT
        label = new Label("Utility: ", getSkin());
        innerTable.handle(label).expandX().left();
        updateLabel = new UpdateLabel(getSkin()) {
            @Override
            public String aktualisiereInformationenOverride() {
                return "" + MathUtils.runden(familie.getNutzen(), 2);
            }
        };
        getUpdateLabelList().handle(updateLabel);
        innerTable.handle(updateLabel);
        updateLabel = new UpdateLabel(getSkin()) {
            @Override
            public String aktualisiereInformationenOverride() {
                return "" + MathUtils.runden(familie.getNutzen()
                        + familie.getGeld().getMengeVerfuegbar(), 2);
            }
        };
        getUpdateLabelList().handle(updateLabel);
        innerTable.handle(updateLabel);*/
    }
}
