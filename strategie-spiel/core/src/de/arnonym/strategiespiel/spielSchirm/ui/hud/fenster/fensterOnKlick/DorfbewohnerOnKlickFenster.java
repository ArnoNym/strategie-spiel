package de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterOnKlick;

import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

import de.arnonym.strategiespiel.framework.werkzeuge.numbers.unit.ZeitEnum;
import de.arnonym.strategiespiel.spielSchirm.World;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.EmployerComp;
import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenBalance;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.ArbeitnehmerAi;
import de.arnonym.strategiespiel.framework.stp.StpComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.gegenstaende.GegenstaendeComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.GeldComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.werte.HabitalwerteComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.name.NameComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.rendern.HashMapRenderComp;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.labels.UpdateLabel;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterOnKlick.content.GegenstandTable;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterOnKlick.content.GeldTable;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.tables.UpdateTable;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.SpecialUtils;

public class DorfbewohnerOnKlickFenster extends OnKlickFenster {

    public DorfbewohnerOnKlickFenster(World world, Skin skin, ArbeitnehmerAi arbeitnehmerAi,
                                      GegenstaendeComp gegenstaendeComp, GeldComp geldComp,
                                      HabitalwerteComp habitalwerteComp,
                                      HashMapRenderComp hashMapRenderComp,
                                      StpComp stpComp, NameComp nameComp) {
        super(world, skin, 500, 300, nameComp, hashMapRenderComp, habitalwerteComp,
                gegenstaendeComp, geldComp, stpComp);

        Label label;
        UpdateLabel updateLabel;
        UpdateTable updateTable;

        // HABITALWERTE ///////////////////////////////////////////////////////////////////////////

        label = new Label("well-being: ", skin);
        getContentTable().add(label).left().expandX().fillX().row();
        updateTable = new UpdateTable() {
            @Override
            public void aktualisiereInformationen_or() {
                add(new Label("health: ", skin)).left();
                add(new Label(""+habitalwerteComp.getGesundheit(), skin)).left().row();
                add(new Label("fitness: ", skin)).left();
                add(new Label(""+habitalwerteComp.getFitness(), skin)).left().row();
                add(new Label("sleep: ", skin)).left(); //todo weniger nachkommastellen (hier bei allen)
                add(new Label(""+habitalwerteComp.getSchlaf()+" / "
                        +KonstantenBalance.DORFBEWOHNER_SCHLAFEN_MAX, skin)).left().row();
                add(new Label("age: ", skin)).left();
                add(new Label(""+habitalwerteComp.gettAlter(ZeitEnum.INGAME_JAHR), skin))
                        .left().row();
            }
        };
        getContentTable().add(updateTable).left();
        getUpdateTableList().add(updateTable);

        SpecialUtils.leerzeile(getContentTable());

        // JOB ////////////////////////////////////////////////////////////////////////////////////

        label = new Label("job: ", skin);
        getContentTable().add(label).left().expandX().fillX();
        updateLabel = new UpdateLabel(skin) {
            @Override
            public String aktualisiereInformationenOverride() {
                EmployerComp employerComp = arbeitnehmerAi.getEmployer();
                if (employerComp == null) {
                    return "unemployed";
                }
                return employerComp.getClass().getSimpleName();
            }
        };
        getContentTable().add(updateLabel).right();
        getUpdateLabelList().add(updateLabel);

        SpecialUtils.leerzeile(getContentTable());

        // GELD UND GEGENSATAENDE /////////////////////////////////////////////////////////////////

        GeldTable geldTable = new GeldTable(skin, geldComp);
        getContentTable().add(geldTable);
        getUpdateTableList().add(geldTable);

        SpecialUtils.leerzeile(getContentTable());

        GegenstandTable gegenstandTable = new GegenstandTable(world.getIngameCursorManager(),
                gegenstaendeComp, false, this);
        getContentTable().add(gegenstandTable);
        getUpdateTableList().add(gegenstandTable);

        updateLabel = new UpdateLabel(skin) {
            @Override
            public String aktualisiereInformationenOverride() {
                return "current taks: "+ stpComp.gettAktuellerTaskName()
                        +"\ncurrent step : "+ stpComp.gettAktuellerStepName();
            }
        };
        getContentTable().row();

        getContentTable().add(updateLabel);
        getUpdateLabelList().add(updateLabel);
    }
}
