package de.arnonym.strategiespiel.spielSchirm.ui.auswahlkaesten;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.viewport.Viewport;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import de.arnonym.strategiespiel.spielSchirm.ui.IngameCursorManager;
import de.arnonym.strategiespiel.spielSchirm.ui.ZellenMarkierungRenderer;
import de.arnonym.strategiespiel.spielSchirm.Assets;
import de.arnonym.strategiespiel.spielSchirm.Enums;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.GeometryUtils;

public abstract class Auswahlkasten<A> {
    private final Viewport viewport;
    //private final MyInputProcessor myInputProcessor;
    private final IngameCursorManager ingameCursorManager;
    private final ZellenMarkierungRenderer zellenMarkierungRenderer;
    private final TextureRegion moeglichkeitTextureRegion;
    private static final TextureRegion vorschlagAddTextureRegion
            = Assets.instance.tiledMapAssets.durchsichtigGruen;
    private static final TextureRegion vorschlagRemoveTextureRegion
            = Assets.instance.tiledMapAssets.entfernenMarkierung;
    private final TextureRegion auswahlTextureRegion;

    private final List<A> vorschlaegeList = new ArrayList<>();

    private Vector2 positionAnfang;
    private Enums.EnumAktiv enumAktiv = Enums.EnumAktiv.INAKTIV;
    private boolean loeschen;

    public Auswahlkasten(Viewport worldViewport, IngameCursorManager ingameCursorManager,
                         TextureRegion moeglichkeitTextureRegion,
                         TextureRegion auswahlTextureRegion) {
        this.viewport = worldViewport;
        //this.myInputProcessor = myInputProcessor;
        this.ingameCursorManager = ingameCursorManager;
        this.zellenMarkierungRenderer = ingameCursorManager.getZellenMarkierungRenderer();
        this.moeglichkeitTextureRegion = moeglichkeitTextureRegion;
        this.auswahlTextureRegion = auswahlTextureRegion;
    }

    public final void update() {
        if (enumAktiv == Enums.EnumAktiv.INAKTIV) {
            return;
        }

        if (enumAktiv == Enums.EnumAktiv.VORSCHLAGEN) {
            vorschlaegeList.clear();

            Vector2 ortoPositionAnfang = GeometryUtils.isoToOrto(positionAnfang);
            Vector2 isoPosition = viewport.unproject(
                    new Vector2(
                            Gdx.input.getX(),
                            Gdx.input.getY()));
            Vector2 ortoPosition = GeometryUtils.isoToOrto(isoPosition);

            float leftBound = Math.min(ortoPositionAnfang.x , ortoPosition.x);// TODO An obere bzw untere Kante der Zellen verschieben
            float rightBound = Math.max(ortoPositionAnfang.x , ortoPosition.x);
            float bottomBound = Math.min(ortoPositionAnfang.y , ortoPosition.y);
            float topBound = Math.max(ortoPositionAnfang.y , ortoPosition.y);

            flaecheMarkieren(isoPosition);

            vorschlagen(leftBound, rightBound, bottomBound, topBound);
        }

        // Markiere alles was bereits zuvor ausgewaehlt wurde
        for (A a : gettMoeglichkeitenList()) {
            if (pruefeZeichnen(a)) {
                if (getAusgewaehltList().contains(a)) {
                    zellenMarkierungRenderer.add(
                            gettPosition(a),
                            gettGroesseInZellen(a),
                            auswahlTextureRegion, true);
                } else {
                    zellenMarkierungRenderer.add(
                            gettPosition(a),
                            gettGroesseInZellen(a),
                            moeglichkeitTextureRegion, false);
                }
            }
        }
    }

    private void flaecheMarkieren(Vector2 isoPosition) {
        Vector2 indexAnfang = GeometryUtils.gettIndizes(positionAnfang);
        Vector2 index = GeometryUtils.gettIndizes(isoPosition);
        int minX = (int) Math.min(indexAnfang.x , index.x);
        int maxX = (int) Math.max(indexAnfang.x , index.x);
        int minY = (int) Math.min(indexAnfang.y , index.y);
        int maxY = (int) Math.max(indexAnfang.y , index.y);
        for (int x = minX; x <= maxX; x++) {
            for (int y = minY; y <= maxY; y++) {
                zellenMarkierungRenderer.add(x, y, Assets.instance.tiledMapAssets.durchsichtigWeiss,
                        false);
            }
        }
    }

    private void vorschlagen(float leftBound, float rightBound, float bottomBound, float topBound) {
        for (A a : gettMoeglichkeitenList()) {
            Vector2 isoPosition = gettPosition(a);
            Vector2 ortoPosition = GeometryUtils.isoToOrto(isoPosition);
            float x = ortoPosition.x;
            float y = ortoPosition.y;
            if (x > leftBound && x < rightBound && y > bottomBound && y < topBound) {
                vorschlaegeList.add(a);
                zellenMarkierungRenderer.add(isoPosition, gettGroesseInZellen(a),
                        loeschen ? vorschlagRemoveTextureRegion : vorschlagAddTextureRegion, true);
            }
        }
    }

    // AKTIVIEREN ETC /////////////////////////////////////////////////////////////////////////////

    public final void aktivieren() {
        this.enumAktiv = Enums.EnumAktiv.AKTIV;
        ingameCursorManager.setCursorStatus(Enums.EnumIngameWorldCursorStatus.ZELLE);
    }

    public final void vorschlagen(Vector2 weltKlick) {
        this.enumAktiv = Enums.EnumAktiv.VORSCHLAGEN;
        ingameCursorManager.setCursorStatus(Enums.EnumIngameWorldCursorStatus.NICHTS);
        positionAnfang = weltKlick;
        loeschen = false; //TODO
    }

    public final void ausloesen() {
        for (A a : vorschlaegeList) {
            ausloesen_or(a, loeschen);
        }
        aktivieren();
    }

    protected abstract void ausloesen_or(A a, boolean loeschen);

    public final void deaktivieren() {
        this.enumAktiv = Enums.EnumAktiv.INAKTIV;
        ingameCursorManager.setCursorStatus(
                Enums.EnumIngameWorldCursorStatus.ZELLE_UND_GEBAUDE_UND_CURSOR);
    }

    // KOMPLEXE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    protected abstract Collection<A> gettMoeglichkeitenList();

    protected abstract Collection<A> getAusgewaehltList();

    protected abstract boolean pruefeZeichnen(A a);

    protected abstract Vector2 gettPosition(A a);

    protected abstract Vector2 gettGroesseInZellen(A a);

    // PROTECTED GETTER UND SETTER ////////////////////////////////////////////////////////////////

    protected Vector2 getPositionAnfang() {
        return positionAnfang;
    }

    public Enums.EnumAktiv getEnumAktiv() {
        return enumAktiv;
    }
}
