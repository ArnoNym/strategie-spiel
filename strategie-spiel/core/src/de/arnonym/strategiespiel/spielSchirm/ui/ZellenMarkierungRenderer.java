package de.arnonym.strategiespiel.spielSchirm.ui;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.arnonym.strategiespiel.spielSchirm.worldMap.Zelle;
import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenOptik;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.GeometryUtils;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.TextureUtils;

public class ZellenMarkierungRenderer {
    private HashMap<Vector2, TextureRegion> renderHashMap = new HashMap<>();

    public ZellenMarkierungRenderer() {

    }

    public void render(SpriteBatch spriteBatch) {
        for (Map.Entry<Vector2, TextureRegion> entry : renderHashMap.entrySet()) {
            float xDraw = entry.getKey().x - KonstantenOptik.TILE_ISO_WIDTH / 2;
            float yDraw = entry.getKey().y - KonstantenOptik.TILE_ISO_HEIGHT / 2;
            TextureUtils.zeichneTexturRegion(spriteBatch, entry.getValue(), xDraw, yDraw, 1, 1,
                    false);
        }
        renderHashMap = new HashMap<>();
    }

    // ADD ////////////////////////////////////////////////////////////////////////////////////////

    public void add(Vector2 position, Vector2 groesseInZellen, TextureRegion textureRegion,
                    boolean ueberschreibe) {
        for (Vector2 indizes : GeometryUtils.gettIndizesListe(position, groesseInZellen)) {
            add((int) indizes.x, (int) indizes.y, textureRegion, ueberschreibe);
        }
    }

    public void add(int xIndex, int yIndex, TextureRegion textureRegion, boolean ueberschreiben) {
        Vector2 position = GeometryUtils.ortoToIso(new Vector2(
                xIndex * KonstantenOptik.TILE_ORTO_WIDTH_AND_HEIGHT,
                yIndex * KonstantenOptik.TILE_ORTO_WIDTH_AND_HEIGHT));
        position.x += KonstantenOptik.TILE_ISO_WIDTH / 2;
        add(position, textureRegion, ueberschreiben);
    }

    public void add(List<Zelle> zellenListe, TextureRegion textureRegion, boolean ueberschreiben) {
        for (Zelle z : zellenListe) {
            add(z.getPosition(), textureRegion, ueberschreiben);
        }
    }

    public void add(Vector2 position, TextureRegion textureRegion, boolean ueberschreiben) {
        if (!ueberschreiben && renderHashMap.containsKey(position)) { // TODO Theoreitsch muessten auch die x und y werte ueberprueft werden
            return;
        }
        renderHashMap.put(position, textureRegion);
    }
}
