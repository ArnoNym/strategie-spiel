package de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.chat;

import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.viewport.Viewport;

import java.util.ArrayList;
import java.util.List;

import de.arnonym.strategiespiel.spielSchirm.ui.IngameCursorManager;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.tables.UpdateTable;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.Fenster;
import de.arnonym.strategiespiel.spielSchirm.Assets;
import de.arnonym.strategiespiel.spielSchirm.Enums;
import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenOptik;

public class FensterChat extends Fenster {
    private List<Message> messageList = new ArrayList<>();

    public FensterChat(Stage levelGuiStage, Viewport worldViewport, Viewport overlayViewport,
                       IngameCursorManager ingameCursorManager, Skin skin) {
        super(levelGuiStage, overlayViewport, worldViewport, ingameCursorManager, skin,
                new NinePatch(Assets.instance.levelGuiAssets.fensterDickerRand,
                        KonstantenOptik.DUENNER_RAND_fensterA2_GROESSE,
                        KonstantenOptik.DUENNER_RAND_fensterA2_GROESSE,
                        KonstantenOptik.DUENNER_RAND_fensterA2_GROESSE,
                        KonstantenOptik.DUENNER_RAND_fensterA2_GROESSE),
                360, 200, new Label("Log", skin), true);
        setAufSeiteVonParentFenster(Enums.EnumRichtung.OBEN);

        schliessen();

        UpdateTable updateTableMod = new UpdateTable() {
            @Override
            public void aktualisiereInformationen_or() {
                for (int i = 0; i < messageList.size() && i < 10; i++) { //TODO Konstanten
                    add(messageList.get(i).gettLabel()).expandX().left();
                    row();
                }
            }
        };
        getUpdateTableList().add(updateTableMod);
        getContentTable().add(updateTableMod).expand().fillX().colspan(100).top();
    }

    @Override
    public void act(float delta) {
        List<Message> removeMessageList = new ArrayList<>();
        for (Message m : messageList) {
            m.act(delta);
            if (m.issFertig()) {
                removeMessageList.add(m);
            }
        }
        for (Message m : removeMessageList) {
            messageList.remove(m);
        }
        super.act(delta);
    }

    public void addMessage(Message message) {
        messageList.add(message);
    }

    public void addMessage(String string) {
        messageList.add(new Message(new Label("- " + string, skin)));
    }
}
