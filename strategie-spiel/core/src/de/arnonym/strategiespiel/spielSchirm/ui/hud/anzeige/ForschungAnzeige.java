package de.arnonym.strategiespiel.spielSchirm.ui.hud.anzeige;

import com.badlogic.gdx.scenes.scene2d.ui.Label;

import java.util.Map;

import de.arnonym.strategiespiel.Strategiespiel;
import de.arnonym.strategiespiel.spielSchirm.Dorf;
import de.arnonym.strategiespiel.spielSchirm.balancing.IdsGegenstaende;
import de.arnonym.strategiespiel.spielSchirm.info.muster.Info;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.tables.UpdateTable;
import de.arnonym.strategiespiel.framework.werkzeuge.collections.hashMaps.IntIntHashMap;

public class ForschungAnzeige extends Anzeige {
    public ForschungAnzeige(Dorf dorf, Info info) {
        super(new UpdateTable(Strategiespiel.skin) {
            @Override
            public void aktualisiereInformationen_or() {
                erstelleHeader("materials needed for research: ", true, "name", "avaiable",
                        " / purchasable", " / needet");

                row();

                IntIntHashMap alleGegHashMap = dorf.gettAlleGegenstaendeIdMengenHashMap(true,
                        false, true);
                IntIntHashMap forschungMap = info.getForschungAssignment().idMengeHashMap;
                for (Map.Entry<Integer, Integer> entry : forschungMap.entrySet()) {

                    add(new Label(IdsGegenstaende.get(entry.getKey()).name, getSkin())).left();

                    int id = entry.getKey();
                    Integer vorhandenGegenstandMenge = alleGegHashMap.get(id);
                    if (vorhandenGegenstandMenge == null) {
                        vorhandenGegenstandMenge = 0;
                    }
                    add(new Label("" + vorhandenGegenstandMenge, getSkin())).left();

                    add(new Label(" / 0", getSkin())).left(); //TODO Zum kauf verfuegbare Menge anpassen wenn Gebaude verkauft werden koennen oder Handel eingefuegt wird etc

                    int zumBauBenoetigteMenge = entry.getValue();
                    add(new Label(" / " + zumBauBenoetigteMenge, getSkin())).left();

                    row();
                }
            }
        });
    }
}
