package de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterOnKlick.content;

import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.GeldComp;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.tables.UpdateTable;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.Geld;

public class GeldTable extends UpdateTable {
    public final GeldComp geldComp;

    public GeldTable(Skin skin, GeldComp geldComp) {
        super(skin);
        this.geldComp = geldComp;
    }

    @Override
    public void aktualisiereInformationen_or() {
        erstelleHeader("money:", true, "current amount", " / incomming", " / outgoing",
                " / income", " / expences");

        row();

        Geld geld = geldComp.getGeld();
        long entityId = geldComp.entityId;

        add(new Label(geld.getMengeVerfuegbar()+"", getSkin())).left();
        add(new Label(geld.getMengeLegenReserviert()+"", getSkin())).right();
        add(new Label(geld.getMengeNehmenReserviert()+"", getSkin())).right();

        add(new Label(geld.geldManager.gettSummeEingang(entityId, 5)+"", getSkin())).right(); // TODO Konstanten
        add(new Label(geld.geldManager.gettSummeAusgang(entityId, 5)+"", getSkin())).right();
    }
}
