package de.arnonym.strategiespiel.spielSchirm.ui.hud;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.Viewport;

import de.arnonym.strategiespiel.spielSchirm.ui.IngameCursorManager;
import de.arnonym.strategiespiel.spielSchirm.Enums;
import de.arnonym.strategiespiel.framework.werkzeuge.pause.Pause;
import de.arnonym.strategiespiel.framework.werkzeuge.pause.GuiPause;
import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenOptik;

public abstract class MoveableGroup extends Group {
    //Scene2d: https://github.com/libgdx/libgdx/wiki/Scene2d
    //Tables: https://github.com/libgdx/libgdx/wiki/Table

    public final Viewport worldViewport;
    public final Viewport overlayViewport;
    public final IngameCursorManager ingameCursorManager;
    public final Skin skin;

    private boolean moveable = true;
    private boolean standardposition = true;
    private boolean angeklickt = false; //Im Gegensatz dazu wenn es durch das Hovern der Maus geoeffnet wurde

    private MoveableGroup childFenster; //Fenster, dass an dieses geheftet werden kann
    private MoveableGroup parentFenster; //Fenster an das dieses geheftet wird
    private Enums.EnumRichtung aufSeiteVonParentFenster = Enums.EnumRichtung.RECHTS;

    public MoveableGroup(Stage levelGuiStage, Viewport overlayViewport, Viewport worldViewport,
                         final IngameCursorManager ingameCursorManager, Skin skin, float width,
                         float height) {
        levelGuiStage.addActor(this);
        this.overlayViewport = overlayViewport;
        this.worldViewport = worldViewport;
        this.ingameCursorManager = ingameCursorManager;
        this.skin = skin;
        setBounds(0, 0, width, height);

        addListener(new ClickListener() {
            private Vector2 mausMod;
            private Pause pause = new GuiPause(KonstantenOptik.ZEIT_BIS_MOVED,
                    Enums.EnumPauseStart.VORBEI);

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if (button == Input.Buttons.LEFT) {
                    if (moveable) {
                        Vector2 fensterPosition = new Vector2(MoveableGroup.this.getX(), MoveableGroup.this.getY());
                        Vector2 mausPosition = overlayViewport.unproject(new Vector2(Gdx.input.getX(), Gdx.input.getY()));
                        mausMod = new Vector2(mausPosition.x - fensterPosition.x, mausPosition.y - fensterPosition.y);
                    }
                    angeklickt = true;
                    MoveableGroup.this.toFront();
                    pause.settStartzeitJetzt();
                    return true;
                }
                return super.touchDown(event, x, y, pointer, button);
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                pause.settVorbei();
                super.touchUp(event, x, y, pointer, button);
            }

            @Override
            public void touchDragged(InputEvent event, float x, float y, int pointer) {
                if (moveable && pause.issVorbei()) {
                    settStandardposition(false);
                    angeklickt = true;

                    Vector2 mausPosition = overlayViewport.unproject(
                            new Vector2(Gdx.input.getX(), Gdx.input.getY()));
                    Vector2 fensterPosition = keepInBounds(new Vector2(mausPosition.x - mausMod.x,
                            mausPosition.y - mausMod.y));
                    MoveableGroup.this.setPosition(fensterPosition.x, fensterPosition.y);
                }
                super.touchDragged(event, x, y, pointer);
            }

            @Override
            public boolean mouseMoved(InputEvent event, float x, float y) {
                ingameCursorManager.setMausUeberUi(true);
                return true;
            }
        });
    }

    @Override
    public void act(float delta) {
        if (isVisible() && isStandardposition() && parentFenster != null) {
            settPositionAnParent();
        }
        super.act(delta);
    }

    // OEFFNEN UND SCHLIESSEN /////////////////////////////////////////////////////////////////////

    public void settVisible(boolean visible, boolean angeklicktWennVisible) {
        if (visible) {
            oeffnen(angeklicktWennVisible);
        } else {
            schliessen();
        }
    }

    public void oeffnen(boolean angeklickt) {
        setVisible(true);
        this.angeklickt = this.angeklickt || angeklickt;
        toFront();
        if (issMausOver()) {
            ingameCursorManager.setMausUeberUi(true);
        }
    }

    public void schliessen() {
        setVisible(false);
        settStandardposition(true);
        angeklickt = false;

        if (issMausOver()) {
            ingameCursorManager.setMausUeberUi(false); //TODO Pruefen ob die Maus ueber irgendeiner UI ist
        }
    }

    // SONSTIGES //////////////////////////////////////////////////////////////////////////////////

    public boolean issMausOver() {
        Vector2 mausOverlayPosition = overlayViewport.unproject(new Vector2(Gdx.input.getX(),
                Gdx.input.getY()));
        return issInBounds(mausOverlayPosition);
    }

    private boolean issInBounds(Vector2 testenPosition) {
        return !(testenPosition.x < getX() || testenPosition.x > getRight()
                || testenPosition.y < getY() || testenPosition.y > getTop());
    }

    // POSITIONEN /////////////////////////////////////////////////////////////////////////////////

    public void settPosition(Vector2 overlayPosition) {
        overlayPosition = keepInBounds(overlayPosition);
        setPosition(overlayPosition.x, overlayPosition.y);
    }

    private Vector2 keepInBounds(Vector2 overlayPosition) {
        final Viewport ov = overlayViewport;
        overlayPosition.x = Math.max(overlayPosition.x, ov.getScreenX());
        if (overlayPosition.x + getWidth() > ov.getScreenX() + ov.getWorldWidth()) {
            overlayPosition.x = ov.getScreenX() + ov.getWorldWidth() - getWidth();
        }
        overlayPosition.y = Math.max(overlayPosition.y, ov.getScreenY());
        if (overlayPosition.y + getHeight() > ov.getScreenY() + ov.getWorldHeight()) {
            overlayPosition.y = ov.getScreenY() + ov.getWorldHeight() - getHeight();
        }
        return overlayPosition;
    }

    public void settPosition(Vector2 worldPosition, float overlayModX, float overlayModY) {
        Vector2 p = keepInBounds(worldToOverlay(worldPosition, overlayModX, overlayModY));
        setPosition(p.x, p.y);
    }

    private Vector2 worldToOverlay(Vector2 worldPosition, float overlayModX, float overlayModY) {
        Vector2 p = worldViewport.project(new Vector2(worldPosition));
        float streckeMultiplikatorX = worldViewport.getScreenWidth() / overlayViewport.getWorldWidth();
        float streckeMultiplikatorY = worldViewport.getScreenHeight() / overlayViewport.getWorldHeight();
        p.x = p.x + overlayModX * streckeMultiplikatorX; //Noch nicht getestet
        p.y = overlayViewport.getScreenHeight() - p.y - overlayModY * streckeMultiplikatorY;
        p = overlayViewport.unproject(p);
        return p;
    }

    @Override
    protected void positionChanged () { //Das war im Parent platzEmpty
        setBounds(getX(), getY(), getWidth(), getHeight());
    }

    // FENSTER AUSKLAPPEN /////////////////////////////////////////////////////////////////////////

    public void settPositionAnParent() {
        settPosition(gettPositionAnParent(aufSeiteVonParentFenster, 1));
    }

    private Vector2 gettPositionAnParent(Enums.EnumRichtung aufSeiteVonParentFenster,
                                         int vorschlagNr) {
        Vector2 neuePosition;
        switch (aufSeiteVonParentFenster) {
            case LINKS:
                neuePosition = new Vector2(
                        parentFenster.getX() - getWidth(),
                        parentFenster.getY() + parentFenster.getHeight() - getHeight());
                break;
            case RECHTS:
                neuePosition = new Vector2(
                        parentFenster.getX() + parentFenster.getWidth(),
                        parentFenster.getY() + parentFenster.getHeight() - getHeight());
                break;
            case UNTEN:
                neuePosition = new Vector2(
                        parentFenster.getX(),
                        parentFenster.getY() - getHeight());
                break;
            case OBEN:
                neuePosition = new Vector2(
                        parentFenster.getX(),
                        parentFenster.getY() + parentFenster.getHeight());
                break;
            default: throw new IllegalArgumentException("EnumRichtung: " + aufSeiteVonParentFenster);
        }
        if (vorschlagNr <= 2) { // zB einmal recht -> false, einmal links -> false, rechts nehmen
            return positionVorschlagPruefen(aufSeiteVonParentFenster, neuePosition, vorschlagNr+1);
        }
        return neuePosition;
    }

    private Vector2 positionVorschlagPruefen(Enums.EnumRichtung aufSeiteVonParentFenster,
                                             Vector2 positionVorschlag,
                                             int vorschlagNr) {
        switch (aufSeiteVonParentFenster) {
            case OBEN:
                if (positionVorschlag.y + getHeight() > overlayViewport.getScreenY()
                        + overlayViewport.getWorldHeight()) {
                    return gettPositionAnParent(Enums.EnumRichtung.UNTEN, vorschlagNr);
                }
                break;
            case UNTEN:
                if (positionVorschlag.y < overlayViewport.getScreenY()) {
                    return gettPositionAnParent(Enums.EnumRichtung.OBEN, vorschlagNr);
                }
                break;
            case RECHTS:
                if (positionVorschlag.x + getWidth() > overlayViewport.getScreenX()
                        + overlayViewport.getWorldWidth()) {
                    return gettPositionAnParent(Enums.EnumRichtung.LINKS, vorschlagNr);
                }
                break;
            case LINKS:
                if (positionVorschlag.x < overlayViewport.getScreenX()) {
                    return gettPositionAnParent(Enums.EnumRichtung.RECHTS, vorschlagNr);
                }
                break;
            default: throw new IllegalArgumentException("EnumRichtung: " + aufSeiteVonParentFenster);
        }
        return positionVorschlag;
    }

    public void childAusklappen(MoveableGroup child) {
        if (childFenster != child || child.parentFenster != this) {
            child.parentFenster = this;
            child.oeffnen(true); //Magic happens
            childFenster = child;
        } else {
            childFenster.setVisible(!childFenster.isVisible());
        }
    }

    // EINFACHE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public Boolean isMoveable() {
        return moveable;
    }

    public void settMoveable(Boolean moveable) {
        this.moveable = moveable;
    }

    public boolean isStandardposition() {
        return standardposition;
    }

    public void settStandardposition(boolean standardposition) {
        this.standardposition = standardposition;
    }

    public boolean isAngeklickt() {
        return angeklickt;
    }

    public MoveableGroup getChildFenster() {
        return childFenster;
    }

    public MoveableGroup getParentFenster() {
        return parentFenster;
    }

    public void setParentFenster(MoveableGroup parentFenster) {
        this.parentFenster = parentFenster;
    }

    public void setAufSeiteVonParentFenster(Enums.EnumRichtung aufSeiteVonParentFenster) {
        this.aufSeiteVonParentFenster = aufSeiteVonParentFenster;
    }

    public void setAngeklickt(boolean angeklickt) {
        this.angeklickt = angeklickt;
    }
}
