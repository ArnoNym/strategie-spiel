package de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterOnKlick.content;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import de.arnonym.strategiespiel.spielSchirm.World;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.CellsComp;
import de.arnonym.strategiespiel.spielSchirm.ui.auswahlkaesten.AuswahlkastenLandUebergeben;

public class LandGebenButton extends Button {
    public LandGebenButton(World world, CellsComp cellsComp, Skin skin) {
        super(new Label("Land", skin) , skin);
        addListener(new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if (button == Input.Buttons.LEFT) {
                    AuswahlkastenLandUebergeben auswahlKasten = new AuswahlkastenLandUebergeben(
                            world.getViewport(), world.getIngameCursorManager(), world.worldMap,
                            cellsComp);
                    auswahlKasten.aktivieren();

                    world.getDiverseInputs().setAuswahlkastenLandUebergeben(auswahlKasten);

                    return true;
                }
                return super.touchDown(event, x, y, pointer, button);
            }
        });
    }
}
