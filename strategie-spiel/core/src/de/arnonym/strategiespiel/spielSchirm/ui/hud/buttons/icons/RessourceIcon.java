package de.arnonym.strategiespiel.spielSchirm.ui.hud.buttons.icons;

/*public class RessourceIcon extends Icon {
    public final EmployerComp<?> ressourcenDepartmentComp;
    public final RessourcenAssignment ressourcenAssignment;
    public final Forschungsstand forschungsstand;

    public RessourceIcon(IngameCursorManager ingameCursorManager,
                         MoveableGroup parentMoveableGroup,

                         RessourcenDepartmentComp<?> ressourcenDepartmentComp,
                         RessourcenAssignment ressourcenAssignment,

                         Forschungsstand forschungsstand) {
        super(ingameCursorManager, parentMoveableGroup, ressourcenAssignment.resInfo.id);
        this.ressourcenDepartmentComp = ressourcenDepartmentComp;
        this.ressourcenAssignment = ressourcenAssignment;
        this.forschungsstand = forschungsstand;

        if (ressourcenAssignment.resInfo.id != forschungsstand.id) {
            throw new IllegalArgumentException();
        }

        AnzeigeFenster anzeigeFenster = ingameCursorManager.getAnzeigeFenster();

        addListener(new MyClickListener() {
            @Override
            public boolean touchDown_or(InputEvent event, float x, float y, int pointer, int button) {
                if (ressourcenAssignment.equals(ressourcenDepartmentComp.getAssignment_nl())) {
                    ressourcenDepartmentComp.setAssignment(null);
                } else {
                    ressourcenDepartmentComp.setAssignment(ressourcenAssignment);
                }
                return true;
            }
            @Override
            public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
                anzeigeFenster.aktiviereHover(parentMoveableGroup,
                        new RessourceAnzeige(ressourcenAssignment.resInfo));
                super.enter(event, x, y, pointer, fromActor);
            }
            @Override
            public void exit_or(InputEvent event, float x, float y, int pointer, Actor toActor) {
                anzeigeFenster.deaktiviereHover();
            }
        });
    }

    @Override
    public void draw_or(Batch batch, float parentAlpha) {
        renderWorldTextureRegion((SpriteBatch) batch, ressourcenAssignment.resInfo.iconTextureRegion());

        if (forschungsstand.isErforscht()) {
            if (ressourcenAssignment.equals(ressourcenDepartmentComp.getAssignment_nl())) {
                renderWorldTextureRegion((SpriteBatch) batch,
                        Assets.instance.iconAssets.iconRahmenHaken);
                return;
            }
            renderRahmenKannKlicken((SpriteBatch) batch);
            return;
        }

        if (forschungsstand.issWirdErforscht()) {
            renderWorldTextureRegion((SpriteBatch) batch,
                    Assets.instance.iconAssets.iconWissenschaft); // todo prozentanzeige
            return;
        }

        renderRahmenKreuz((SpriteBatch) batch);
    }
}*/
