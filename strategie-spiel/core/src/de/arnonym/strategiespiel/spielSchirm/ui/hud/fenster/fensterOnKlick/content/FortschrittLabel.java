package de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterOnKlick.content;

import com.badlogic.gdx.scenes.scene2d.ui.Skin;

import de.arnonym.strategiespiel.spielSchirm.arbeit.TimeClock;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.labels.UpdateLabel;

public class FortschrittLabel extends UpdateLabel {
    private final TimeClock timeClock;

    public FortschrittLabel(TimeClock timeClock, Skin skin) {
        super(skin);
        this.timeClock = timeClock;
    }

    @Override
    public String aktualisiereInformationenOverride() {
        float arbeitszeit = timeClock.getArbeitszeit();
        return "done: " + (arbeitszeit - timeClock.getRestZuErledigenArbeitszeit())
                / arbeitszeit;
    }
}
