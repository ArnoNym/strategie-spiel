package de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterOnKlick;

import com.badlogic.gdx.scenes.scene2d.ui.Skin;

import de.arnonym.strategiespiel.spielSchirm.World;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.gegenstaende.GegenstaendeComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.name.NameComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.rendern.HashMapRenderComp;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.fensterOnKlick.content.GegenstandTable;

public class OnKlickFensterMenge extends OnKlickFenster {

    public OnKlickFensterMenge(World world, Skin skin, GegenstaendeComp gegenstaendeComp,
                               HashMapRenderComp hashMapRenderComp, NameComp nameComp) {
        super(world, skin, 220, 300, nameComp, hashMapRenderComp);

        GegenstandTable gegenstandTable = new GegenstandTable(world.getIngameCursorManager(),
                gegenstaendeComp, true, this);
        getContentTable().add(gegenstandTable).expandX().fillX();
        getUpdateTableList().add(gegenstandTable);
    }
}
