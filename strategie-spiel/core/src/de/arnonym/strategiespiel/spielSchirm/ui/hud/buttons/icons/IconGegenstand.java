package de.arnonym.strategiespiel.spielSchirm.ui.hud.buttons.icons;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import de.arnonym.strategiespiel.spielSchirm.ui.IngameCursorManager;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.MoveableGroup;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.anzeige.GegenstandAnzeige;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.AnzeigeFenster;
import de.arnonym.strategiespiel.spielSchirm.info.muster.GegInfo;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.TextureUtils;

public class IconGegenstand extends Icon {
    protected final GegInfo gegMuster;

    public IconGegenstand(IngameCursorManager ingameCursorManager,
                          MoveableGroup parentMoveableGroup, GegInfo gegMuster) {
        super(ingameCursorManager, parentMoveableGroup, gegMuster.gettId());
        this.gegMuster = gegMuster;
        AnzeigeFenster anzeigeFenster = ingameCursorManager.getAnzeigeFenster();

        addListener(new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                IconGegenstand.this.touchDown();
                return super.touchDown(event, x, y, pointer, button);
            }
            @Override
            public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
                anzeigeFenster.aktiviereHover(parentMoveableGroup,
                        new GegenstandAnzeige(gegMuster));
                super.enter(event, x, y, pointer, fromActor);
            }
            @Override
            public void exit(InputEvent event, float x, float y, int pointer, Actor toActor) {
                anzeigeFenster.deaktiviereHover();
                super.exit(event, x, y, pointer, toActor);
            }
        });
    }

    protected void touchDown() {
        // Ueberschreiben
    }

    @Override
    public void draw_or(Batch batch, float parentAlpha) {
        TextureUtils.zeichneTexturRegion(batch, gegMuster.texture.get(), getX(), getY(), 1, 1, false);
    }
}
