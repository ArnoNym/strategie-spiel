package de.arnonym.strategiespiel.spielSchirm.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.viewport.Viewport;

import de.arnonym.strategiespiel.spielSchirm.balancing.Konstanten;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.baugrube.BaugrubeComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.sonstiges.BlaupausenObjektComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.transformieren.TransComp;
import de.arnonym.strategiespiel.spielSchirm.gebaude.GebaudeFactory;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.anzeige.GebaudeAnzeige;
import de.arnonym.strategiespiel.spielSchirm.worldMap.WorldMap;
import de.arnonym.strategiespiel.spielSchirm.worldMap.Zelle;
import de.arnonym.strategiespiel.spielSchirm.Assets;
import de.arnonym.strategiespiel.spielSchirm.Enums;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.GeometryUtils;

public class Blaupause {
    private final Viewport worldViewport;
    private final IngameCursorManager ingameCursorManager;
    private final WorldMap worldMap;

    private GebaudeFactory gebaudeFactory;
    private BlaupausenObjektComp blaupausenObjektComp;
    private boolean aktiv = false;
    private boolean flaecheIstFrei;

    public Blaupause(Viewport worldViewport, IngameCursorManager ingameCursorManager,
                     WorldMap worldMap) {
        this.worldViewport = worldViewport;
        this.ingameCursorManager = ingameCursorManager;
        this.worldMap = worldMap;
    }

    public void update(float delta) {
        if (aktiv) {
            Vector2 position = gettPosition(blaupausenObjektComp);
            blaupausenObjektComp.getPositionComp().settPosition(position.x, position.y);
            flaecheIstFrei = belegteFlaechenMarkieren(
                    blaupausenObjektComp.getPositionComp().getPosition(),
                    blaupausenObjektComp.getIsoGroesseComp().getGroesseInZellen());
        }
    }

    // PLANEN UND BAUEN ///////////////////////////////////////////////////////////////////////////

    public void planen(GebaudeFactory gebaudeFactory) {
        this.gebaudeFactory = gebaudeFactory;
        if (blaupausenObjektComp != null) {
            blaupausenObjektComp.delete();
        }
        blaupausenObjektComp = gebaudeFactory.create(new Vector2(0, 0)).blaupausenObjektComp;
        blaupausenObjektComp.getPositionComp().settPosition(gettPosition(blaupausenObjektComp));
        worldMap.world.getIngameKamera().preufeVisible();

        aktiv = true;
        flaecheIstFrei = false;
        worldMap.setGridZeichnen(true); //todo
        ingameCursorManager.setCursorStatus(Enums.EnumIngameWorldCursorStatus.NICHTS);
        ingameCursorManager.getAnzeigeFenster().aktiviereBlaupause(blaupausenObjektComp,
                new GebaudeAnzeige(gebaudeFactory.dorf, gebaudeFactory.info));
    }

    public void planenBeenden() {
        blaupausenObjektComp.entitiyDelete();
        blaupausenObjektComp = null;

        aktiv = false;
        worldMap.setGridZeichnen(false);
        ingameCursorManager.setCursorStatus(
                Enums.EnumIngameWorldCursorStatus.ZELLE_UND_GEBAUDE_UND_CURSOR);
        ingameCursorManager.getAnzeigeFenster().deaktiviereBlaupause();
    }

    public void bauen() {
        if (!flaecheIstFrei) {
            return;
        }

        TransComp zuBaugrubeTransComp = blaupausenObjektComp.zuBaugrubeTransComp;
        TransComp zuGebaudeTransComp = null;
        if (Konstanten.DEVELOPER_GAMEPLAY_IMMER_SOFORT_GEBAUT
                || Konstanten.DEVELOPER_GAMEPLAY_ERSTES_GEBAUDE_SOFORT_GEBAUT
                && !Konstanten.developer_erstesGebaudeGebaut) {
            zuGebaudeTransComp = zuBaugrubeTransComp.entity.gettComponent(BaugrubeComp.class).transComp;
        }
        zuBaugrubeTransComp.trans();
        if (Konstanten.DEVELOPER_GAMEPLAY_IMMER_SOFORT_GEBAUT
                || Konstanten.DEVELOPER_GAMEPLAY_ERSTES_GEBAUDE_SOFORT_GEBAUT
                && !Konstanten.developer_erstesGebaudeGebaut) {
            if (zuGebaudeTransComp == null) throw new IllegalStateException();
            zuGebaudeTransComp.trans();
            Konstanten.developer_erstesGebaudeGebaut = true;
        }

        blaupausenObjektComp = null;
        planen(gebaudeFactory);
    }

    // ZELLEN /////////////////////////////////////////////////////////////////////////////////////

    private boolean belegteFlaechenMarkieren(Vector2 position, Vector2 blaubpausenMod) {
        boolean flaecheIstFrei = true;
        for (Vector2 index : GeometryUtils.gettIndizesListe(position, blaubpausenMod)) {
            Zelle zelle = worldMap.gett((int) index.x, (int) index.y);
            if (zelle == null || zelle.isKollisionBauen()) {
                ingameCursorManager.getZellenMarkierungRenderer().add((int) index.x, (int) index.y,
                        Assets.instance.tiledMapAssets.durchsichtigRot, true);
                flaecheIstFrei = false;
            }
        }
        return flaecheIstFrei;
    }

    // POSITION MODIFIKATOREN /////////////////////////////////////////////////////////////////////

    private Vector2 gettPosition(BlaupausenObjektComp blaupausenObjektComp) {
        return GeometryUtils.ganzerTile(
                worldViewport.unproject(new Vector2(Gdx.input.getX(), Gdx.input.getY())),
                blaupausenObjektComp.getIsoGroesseComp().getGroesseInZellen());
    }

    // EINFACHE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public boolean getAktiv () {
        return aktiv;
    }

    public IngameCursorManager getIngameCursorManager() {
        return ingameCursorManager;
    }

    public BlaupausenObjektComp getBlaupausenObjektComp() {
        return blaupausenObjektComp;
    }
}
