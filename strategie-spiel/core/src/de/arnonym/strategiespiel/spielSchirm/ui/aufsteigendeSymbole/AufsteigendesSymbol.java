package de.arnonym.strategiespiel.spielSchirm.ui.aufsteigendeSymbole;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.alter.TransAlterComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.bewegung.CompEinfacheBewegung;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.rendern.RenderComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.rendern.BeweglichRenderComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.groesse.CompGroesse;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.groesse.CompGroesseOrto;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.sonstiges.PositionComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.transformieren.TransComp;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.SysAltern;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.listSystems.privateListSystems.SysBewegung;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.SysRender;
import de.arnonym.strategiespiel.framework.werkzeuge.Manager;
import de.arnonym.strategiespiel.framework.werkzeuge.textures.TextureHashMap;
import de.arnonym.strategiespiel.framework.werkzeuge.textures.IdTextureRegion;

public class AufsteigendesSymbol extends Entity {

    protected AufsteigendesSymbol(Manager manager, SysAltern sysAltern, SysBewegung sysBewegung,
                                  SysRender sysRender,

                                  TextureRegion textureRegion, float dauer, float geschwindigkeit,
                                  Vector2 position) {
        super(manager);
        PositionComp positionComp = new PositionComp(this, position);
        CompGroesse compGroesse = new CompGroesseOrto(this, textureRegion.getRegionWidth(),
                textureRegion.getRegionHeight());
        RenderComp renderComp = new BeweglichRenderComp(this, sysRender,
                positionComp,
                new TextureHashMap(new IdTextureRegion(textureRegion, true, -1,
                        new Vector2(
                                - textureRegion.getRegionWidth() / 2,
                                - textureRegion.getRegionHeight() / 2))),
                SysRender.EnumZ.SYMBOL_IN_SPIELWELT, true);
        TransComp transComp = new TransComp(this, TransComp.EnumDelete.ENTITY);
        TransAlterComp transAlterComp = new TransAlterComp(this, sysAltern, true, 0, transComp,
                dauer);
        CompEinfacheBewegung compEinfacheBewegung = new CompEinfacheBewegung(this, sysBewegung,
                positionComp);
        compEinfacheBewegung.add(new Vector2(0, 1), geschwindigkeit, dauer);

        activate();
    }
}
