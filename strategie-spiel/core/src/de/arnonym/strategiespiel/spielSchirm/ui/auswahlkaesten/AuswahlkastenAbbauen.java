package de.arnonym.strategiespiel.spielSchirm.ui.auswahlkaesten;

import com.badlogic.gdx.utils.viewport.Viewport;

import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.ausbeuten.AbbaubarComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.lists.ausbeuten.AbzubauenEcsList;
import de.arnonym.strategiespiel.spielSchirm.ecs.collections.systems.listSystems.privateListSystems.SysAbbaubar;
import de.arnonym.strategiespiel.spielSchirm.ui.IngameCursorManager;
import de.arnonym.strategiespiel.spielSchirm.Assets;

public class AuswahlkastenAbbauen extends AuswahlkastenAusbeuten<AbbaubarComp> {

    public AuswahlkastenAbbauen(Viewport worldViewport, IngameCursorManager ingameCursorManager,
                                SysAbbaubar sysAbbaubar, AbzubauenEcsList abzubauenListe) {
        super(worldViewport, ingameCursorManager, sysAbbaubar, abzubauenListe,
                Assets.instance.tiledMapAssets.grid, Assets.instance.tiledMapAssets.abbauen);
    }
}
