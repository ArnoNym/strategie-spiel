package de.arnonym.strategiespiel.spielSchirm.ui.hud.buttons;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import java.util.List;

import de.arnonym.strategiespiel.spielSchirm.ui.hud.MoveableGroup;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.buttons.icons.Icon;
import de.arnonym.strategiespiel.spielSchirm.Assets;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.TextureUtils;

public class IconAusklappButton extends AusklappButton {
    private final TextureRegion up;

    public IconAusklappButton(MoveableGroup ausklappenParentMoveableGroup,
                              MoveableGroup fensterAusklappen,
                              List<MoveableGroup> umPlatzKonkurrierendeFensterListe,
                              TextureRegion up) {
        super(ausklappenParentMoveableGroup, fensterAusklappen, umPlatzKonkurrierendeFensterListe,
                new TextureRegionDrawable(Icon.blankTR));
        this.up = up;
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        TextureUtils.zeichneTexturRegion(batch, up, getX(), getY(), 1, 1, false);

        TextureUtils.zeichneTexturRegion(batch, Assets.instance.iconAssets.iconRahmenKannKlicken, getX(), getY(), 1, 1, false);

        if (getFensterAusklappen().isVisible()) {
            TextureUtils.zeichneTexturRegion(batch, Assets.instance.iconAssets.iconRahmenKlick, getX(), getY(), 1, 1, false);
        }

        super.draw(batch, parentAlpha);
    }
}
