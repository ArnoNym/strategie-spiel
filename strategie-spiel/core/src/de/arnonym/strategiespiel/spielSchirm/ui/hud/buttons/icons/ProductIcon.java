package de.arnonym.strategiespiel.spielSchirm.ui.hud.buttons.icons;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;

import de.arnonym.strategiespiel.framework.werkzeuge.MyClickListener;
import de.arnonym.strategiespiel.spielSchirm.Assets;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.EmployerComp;
import de.arnonym.strategiespiel.spielSchirm.arbeit.arbeit.VarEmployerComp;
import de.arnonym.strategiespiel.spielSchirm.info.forschung.Forschungsstand;
import de.arnonym.strategiespiel.spielSchirm.info.muster.Info;
import de.arnonym.strategiespiel.spielSchirm.ui.IngameCursorManager;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.MoveableGroup;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.anzeige.Anzeige;
import de.arnonym.strategiespiel.spielSchirm.ui.hud.fenster.AnzeigeFenster;

public class ProductIcon<ProductInfo extends Info> extends Icon {
    public final VarEmployerComp<ProductInfo> employerComp;
    public final ProductInfo productInfo;
    public final Forschungsstand forschungsstand;

    public ProductIcon(IngameCursorManager ingameCursorManager,
                         MoveableGroup parentMoveableGroup,
                         Anzeige anzeige,

                         VarEmployerComp<ProductInfo> employerComp,
                         ProductInfo productInfo,
                         Forschungsstand forschungsstand) {
        super(ingameCursorManager, parentMoveableGroup, productInfo.id);
        this.employerComp = employerComp;
        this.productInfo = productInfo;
        this.forschungsstand = forschungsstand;

        if (productInfo.id != forschungsstand.id) {
            throw new IllegalArgumentException();
        }

        AnzeigeFenster anzeigeFenster = ingameCursorManager.getAnzeigeFenster();

        addListener(new MyClickListener() {
            @Override
            public boolean touchDown_or(InputEvent event, float x, float y, int pointer, int button) {
                if (productInfo.equals(employerComp.getProduct())) {
                    employerComp.setProduct(null);
                } else {
                    employerComp.setProduct(productInfo);
                }
                return true;
            }
            @Override
            public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
                anzeigeFenster.aktiviereHover(parentMoveableGroup, anzeige);
                super.enter(event, x, y, pointer, fromActor);
            }
            @Override
            public void exit_or(InputEvent event, float x, float y, int pointer, Actor toActor) {
                anzeigeFenster.deaktiviereHover();
            }
        });
    }

    @Override
    public void draw_or(Batch batch, float parentAlpha) {
        renderWorldTextureRegion((SpriteBatch) batch, productInfo.getIconTexture().get());

        if (forschungsstand.isErforscht()) {
            if (productInfo.equals(employerComp.getProduct())) {
                renderWorldTextureRegion((SpriteBatch) batch,
                        Assets.instance.iconAssets.iconRahmenHaken);
                return;
            }
            renderRahmenKannKlicken((SpriteBatch) batch);
            return;
        }

        if (forschungsstand.issWirdErforscht()) {
            renderWorldTextureRegion((SpriteBatch) batch,
                    Assets.instance.iconAssets.iconWissenschaft); // todo prozentanzeige
            return;
        }

        renderRahmenKreuz((SpriteBatch) batch);
    }
}
