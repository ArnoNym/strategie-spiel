package de.arnonym.strategiespiel.spielSchirm.ui.hud.anzeige;

import de.arnonym.strategiespiel.spielSchirm.ui.hud.tables.UpdateTable;

public class Anzeige {
    private final UpdateTable updateTable;

    public Anzeige(UpdateTable updateTable) {
        this.updateTable = updateTable;
    }

    public UpdateTable gettUpdateTable() {
        updateTable.aktualisiereInformationen();
        return updateTable;
    }
}
