package de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.vorstufe;

import de.arnonym.strategiespiel.spielSchirm.balancing.IdsGegenstaende;
import de.arnonym.strategiespiel.spielSchirm.info.muster.GegInfo;
import de.arnonym.strategiespiel.spielSchirm.Enums;
import de.arnonym.strategiespiel.framework.werkzeuge.collections.id.IdList;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.MathUtils;

public class EntityGegInfoList extends IdList<EntityGegInfo> {
    /** Der {@see #maxPlatz} muss bevor diese Liste einer {@see GegenstandList} uebergeben wird
     * einen Wert erhalten. Entweder direkt ueber den Constructor, ueber den Setter oder ueber das
     * setzten einer {@see #gegInfoList_nl} ({@see #settGegInfoList}), wodurch
     * der {@see #maxPlatz} mit jeder hinzugefuegten Menge erhoeht wird */

    private int maxPlatz;
    private boolean wachsenderPlatz;

    public EntityGegInfoList() {
        this.maxPlatz = 0;
        this.wachsenderPlatz = true;
    }

    public EntityGegInfoList(int platz, boolean wachsenderPlatz) {
        this.maxPlatz = platz;
        this.wachsenderPlatz = wachsenderPlatz;
    }

    // UPDATE PLATZ ///////////////////////////////////////////////////////////////////////////////

    @Override // Nur dieses "Add" Ueberschrieben, da die anderen auf dieses verweisen
    public boolean add(EntityGegInfo entityGegInfo, boolean overrideSameId) {
        super.add(entityGegInfo, overrideSameId);
        updatePlatz(entityGegInfo);
        return true;
    }

    private void updatePlatz(EntityGegInfo entityGegInfo) {
        if (wachsenderPlatz) {
            maxPlatz += MathUtils.myIntMult(
                    entityGegInfo.getMengeVerfuegbar(),
                    IdsGegenstaende.get(entityGegInfo.id).platzVerbrauch);
        }
    }

    // ADD ALL ////////////////////////////////////////////////////////////////////////////////////

    public EntityGegInfoList addAll(Integer[] ids) {
        for (int id : ids) {
            add(new EntityGegInfo(id, 0, null, 0));
        }
        return this;
    }

    public EntityGegInfoList addAll(Enums.EnumGegenstandKlasse enumGegenstandKlasse) {
        for (GegInfo gegInfo : IdsGegenstaende.get()) {
            if (gegInfo.getEnumGegenstandKlasse() == enumGegenstandKlasse) {
                add(new EntityGegInfo(gegInfo.gettId(), 0, null, 0));
            }
        }
        return this;
    }

    public EntityGegInfoList addAll() {
        for (GegInfo gegInfo : IdsGegenstaende.get()) {
            add(new EntityGegInfo(gegInfo.gettId(), 0, null, 0));
        }
        return this;
    }

    // EINFACHE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public int getMaxPlatz() {
        return maxPlatz;
    }
}
