package de.arnonym.strategiespiel.spielSchirm.zahlen.mengen;

import de.arnonym.strategiespiel.spielSchirm.balancing.IdsGegenstaende;
import de.arnonym.strategiespiel.spielSchirm.zahlen.wirtschaftsdaten.geld.GeldManager;

public class Geld extends Menge {
    public final GeldManager geldManager = new GeldManager();

    public Geld(int mengeAktuell) {
        super(true, mengeAktuell, null);
    }

    public void add(Geld geld) {
        mengeErhoehen(getMengeVerfuegbar() + geld.getMengeVerfuegbar(), false);
        setMengeLegenReserviert(getMengeLegenReserviert() + geld.getMengeLegenReserviert());
        setMengeNehmenReserviert(getMengeNehmenReserviert() + geld.getMengeNehmenReserviert());
    }

    public void mengeErhoehen(long entityId, int menge, boolean reserviertLegen) {
        super.mengeErhoehen(menge, reserviertLegen);
        geldManager.updateDaten(entityId, menge, 0);
    }

    public void mengeVerringern(long entityId, int menge, boolean reserviertNehmen) {
        super.mengeVerringern(menge, reserviertNehmen);
        geldManager.updateDaten(entityId, 0, menge);
    }

    @Deprecated @Override
    public int getMengeBisVoll() {
        return Integer.MAX_VALUE;
    }

    @Deprecated @Override
    protected void verbauchePlatz(int menge) {

    }

    @Deprecated @Override
    protected void erhoehePlatz(int menge) {

    }

    @Override
    public int gettId() {
        return IdsGegenstaende.GELD_ID;
    }
}