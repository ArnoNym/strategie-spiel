package de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.taschen;

import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.endstufe.Gegenstand;
import de.arnonym.strategiespiel.framework.werkzeuge.collections.id.IdList;

public class MasterTasche extends IdList<Gegenstand> {
    private NahrungTasche nahrungTasche_nl;
    private WerkzeugTasche werkzeugTasche_nl;
    private MedizinTasche medizinTasche_nl;
    private SpielzeugTasche spielzeugTasche_nl;

    public MasterTasche(NahrungTasche nahrungTasche_nl, WerkzeugTasche werkzeugTasche_nl,
                        MedizinTasche medizinTasche_nl, SpielzeugTasche spielzeugTasche_nl) {
        this.nahrungTasche_nl = nahrungTasche_nl;
        this.werkzeugTasche_nl = werkzeugTasche_nl;
        this.medizinTasche_nl = medizinTasche_nl;
        this.spielzeugTasche_nl = spielzeugTasche_nl;
        this.addAll(nahrungTasche_nl);
        this.addAll(werkzeugTasche_nl);
        this.addAll(medizinTasche_nl);
        this.addAll(spielzeugTasche_nl);
    }

    // EINFACHE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public NahrungTasche getNahrungTasche_nl() {
        return nahrungTasche_nl;
    }

    public WerkzeugTasche getWerkzeugTasche_nl() {
        return werkzeugTasche_nl;
    }

    public MedizinTasche getMedizinTasche_nl() {
        return medizinTasche_nl;
    }

    public SpielzeugTasche getSpielzeugTasche_nl() {
        return spielzeugTasche_nl;
    }

    public void setNahrungTasche_nl(NahrungTasche nahrungTasche_nl) {
        this.nahrungTasche_nl = nahrungTasche_nl;
    }

    public void setWerkzeugTasche_nl(WerkzeugTasche werkzeugTasche_nl) {
        this.werkzeugTasche_nl = werkzeugTasche_nl;
    }

    public void setMedizinTasche_nl(MedizinTasche medizinTasche_nl) {
        this.medizinTasche_nl = medizinTasche_nl;
    }

    public void setSpielzeugTasche_nl(SpielzeugTasche spielzeugTasche_nl) {
        this.spielzeugTasche_nl = spielzeugTasche_nl;
    }
}
