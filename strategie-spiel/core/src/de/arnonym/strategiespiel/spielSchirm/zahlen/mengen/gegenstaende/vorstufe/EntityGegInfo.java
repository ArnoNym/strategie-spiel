package de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.vorstufe;

import de.arnonym.strategiespiel.framework.werkzeuge.collections.id.IdObjekt;

public class EntityGegInfo implements IdObjekt {
    public final int id;
    private int mengeVerfuegbar;
    private Integer maxMenge;
    private int wert;

    public EntityGegInfo(int id, int mengeVerfuegbar, Integer maxMenge) {
        this(id, mengeVerfuegbar, maxMenge, 0);
    }

    public EntityGegInfo(int id, int mengeVerfuegbar, Integer maxMenge, int wert) {
        this.id = id;
        this.mengeVerfuegbar = mengeVerfuegbar;
        this.maxMenge = maxMenge;
        this.wert = wert;
    }

    @Override
    public int gettId() {
        return id;
    }

    public int getMengeVerfuegbar() {
        return mengeVerfuegbar;
    }

    public void setMengeVerfuegbar(int mengeVerfuegbar) {
        this.mengeVerfuegbar = mengeVerfuegbar;
    }

    public Integer getMaxMenge() {
        return maxMenge;
    }

    public void setMaxMenge(Integer maxMenge) {
        this.maxMenge = maxMenge;
    }

    public int getWert() {
        return wert;
    }

    public void setWert(int wert) {
        this.wert = wert;
    }
}
