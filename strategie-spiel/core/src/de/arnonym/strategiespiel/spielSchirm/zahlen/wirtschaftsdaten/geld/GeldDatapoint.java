package de.arnonym.strategiespiel.spielSchirm.zahlen.wirtschaftsdaten.geld;

import de.arnonym.strategiespiel.spielSchirm.zahlen.wirtschaftsdaten.Datapoint;

public class GeldDatapoint extends Datapoint {
    public GeldDatapoint(int eingang, int ausgang) {
        super(eingang, ausgang);
    }

    @Override
    protected void update(int eingang, int ausgang) {
        super.update(eingang, ausgang);
    }

    public void add(int eingang, int ausgang) {
        super.update(getEingang() + eingang, getAusgang() + ausgang);
    }

    @Override
    public void setEingang(int eingang) {
        super.setEingang(eingang);
    }

    @Override
    public void setAusgang(int ausgang) {
        super.setAusgang(ausgang);
    }
}
