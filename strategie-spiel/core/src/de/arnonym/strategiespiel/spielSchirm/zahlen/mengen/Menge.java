
package de.arnonym.strategiespiel.spielSchirm.zahlen.mengen;

import java.io.Serializable;

import de.arnonym.strategiespiel.spielSchirm.balancing.Konstanten;

public abstract class Menge implements Serializable {
    private boolean teilbar;

    private int mengeVerfuegbar;
    private int mengeNehmenReserviert = 0;
    private int mengeLegenReserviert = 0;
    private Integer mengeMaximal = null; /* Wird nur verwendet wenn die Menge sonderfall dieses
    Gegenstandes in einer GegListe beschraenkt sein soll */

    protected Menge(boolean teilbar, int mengeVerfuegbar, Integer mengeMaximal_nl) {
        this.teilbar = teilbar;
        this.mengeVerfuegbar = mengeVerfuegbar;
        this.mengeMaximal = mengeMaximal_nl;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////

    // MENGEN /////////////////////////////////////////////////////////////////////////////////////

    protected void mengeErhoehen(int menge, boolean reserviertLegen) {
        if (reserviertLegen) {
            mengeLegenReserviert -= menge;
        } else {
            verbauchePlatz(menge);
        }
        mengeVerfuegbar += menge;
    }

    protected void mengeVerringern(int menge, boolean reserviertNehmen) {
        if (reserviertNehmen) {
            mengeNehmenReserviert -= menge;
        } else {
            mengeVerfuegbar -= menge;
        }
        erhoehePlatz(menge);
    }

    // RESERVIEREN ////////////////////////////////////////////////////////////////////////////////

    public void reservierenNehmen(int menge) {
        pruefeTeilbarEingehalten(menge);
        mengeNehmenReserviert += menge;
        mengeVerfuegbar -= menge;
    }

    public void reservierenNehmenRueckgaengig(int menge) {
        pruefeTeilbarEingehalten(menge);
        mengeNehmenReserviert -= menge;
        mengeVerfuegbar += menge;
    }

    public void reservierenLegen(int menge) {
        mengeLegenReserviert += menge;
        verbauchePlatz(menge);
    }

    public void reservierenLegenRueckgaengig(int menge) {
        mengeLegenReserviert -= menge;
        erhoehePlatz(menge);
    }

    // PLATZ //////////////////////////////////////////////////////////////////////////////////////

    protected abstract void verbauchePlatz(int menge);

    protected abstract void erhoehePlatz(int menge);

    public abstract int getMengeBisVoll();

    // SONSTIGES //////////////////////////////////////////////////////////////////////////////////

    protected void pruefeTeilbarEingehalten(int menge) {
        if (!teilbar && menge % Konstanten.NACHKOMMASTELLEN > 0) {
            throw new IllegalStateException("id: "+ gettId() +", menge: " + menge);
        }
    }

    // KOMPLEXE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public int gettMenge(boolean verfuegbar, boolean legen, boolean nehmen) {
        int menge = 0;
        if (verfuegbar) menge += mengeVerfuegbar;
        if (legen) menge += mengeLegenReserviert;
        if (nehmen) menge += mengeNehmenReserviert;
        return menge;
    }

    public boolean issMenge(boolean verfuegbar, boolean legen, boolean nehmen) {
        return gettMenge(verfuegbar, legen, nehmen) > 0;
    }

    public abstract int gettId();

    // PROTECTED GETTER UND SETTER ////////////////////////////////////////////////////////////////

    protected void setMengeNehmenReserviert(int mengeNehmenReserviert) {
        this.mengeNehmenReserviert = mengeNehmenReserviert;
    }

    protected void setMengeLegenReserviert(int mengeLegenReserviert) {
        this.mengeLegenReserviert = mengeLegenReserviert;
    }

    // EINFACHE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public int getMengeVerfuegbar() {
        return mengeVerfuegbar;
    }

    public int getMengeNehmenReserviert() {
        return mengeNehmenReserviert;
    }

    public boolean isTeilbar() {
        return teilbar;
    }

    protected void setMengeVerfuegbar(int mengeVerfuegbar) {
        this.mengeVerfuegbar = mengeVerfuegbar;
    }

    public int getMengeLegenReserviert() {
        return mengeLegenReserviert;
    }

    public Integer getMengeMaximal() {
        return mengeMaximal;
    }

    public void setMengeMaximal(Integer mengeMaximal) {
        this.mengeMaximal = mengeMaximal;
    }

    public void setTeilbar(boolean teilbar) {
        this.teilbar = teilbar;
    }
}