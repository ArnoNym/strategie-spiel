package de.arnonym.strategiespiel.spielSchirm.zahlen.ids;

import java.util.HashMap;

public class IdArray<O> {
    public final Id[] ids;
    public final O[] os;

    public IdArray(Id[] ids, O[] os) {
        if (ids.length != os.length) {
            throw new IllegalArgumentException("ids.length: "+ids.length+", os.length: "+os.length);
        }
        this.ids = ids;
        this.os = os;
    }

    public IdHashMap<O> createHashMap() {
        IdHashMap<O> hm = new IdHashMap<>();
        for (int i = 0; i < ids.length; i++) {
            hm.put(ids[i], os[i]);
        }
        return hm;
    }
}
