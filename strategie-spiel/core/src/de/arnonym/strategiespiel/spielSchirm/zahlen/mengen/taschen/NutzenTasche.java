package de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.taschen;

import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.endstufe.Gegenstand;
import de.arnonym.strategiespiel.framework.werkzeuge.collections.hashMaps.IntIntHashMap;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.vorstufe.EntityGegInfoList;
import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenBalance;

public abstract class NutzenTasche extends Tasche {
    private int mitLegenNutzen;

    public NutzenTasche(EntityGegInfoList entityGegInfoListe) {
        super(entityGegInfoListe);
    }

    // VERBRAUCHEN ////////////////////////////////////////////////////////////////////////////////

    public void verbauchen(int anzahlPersonen) {
        float anteilVerbauchen = Math.min(KonstantenBalance
                .TASCHE_KONSUMIEREN_PROZENT_PRO_PERSON * anzahlPersonen, 1);
        for (Gegenstand g : this) {
            int mengeVerbauchen;
            if (g.getMengeVerfuegbar() < KonstantenBalance.TASCHE_KONSUMIEREN_REST_MENGE) {
                mengeVerbauchen = g.getMengeVerfuegbar();
            } else {
                mengeVerbauchen = (int) Math.max(g.getMengeVerfuegbar() * anteilVerbauchen,
                        KonstantenBalance.TASCHE_KONSUMIEREN_MAX_MENGE_AUF_EINMAL);
            }

            g.mengeVerringern(mengeVerbauchen, false);
        }
    }

    // KOMPLEXE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public int gettMitLegenNutzen() {
        pruefeUndAktualisiereWerte(true);
        return mitLegenNutzen;
    }

    public void settNutzenHashMap(IntIntHashMap nutzenHashMap) {
        for (Gegenstand g : this) {
            g.setWert(nutzenHashMap.get(g.gettId()));
        }
    }

    // PROTECTED GETTER UND SETTER ////////////////////////////////////////////////////////////////

    protected void setMitLegenNutzen(int mitLegenNutzen) {
        this.mitLegenNutzen = mitLegenNutzen;
    }
}
