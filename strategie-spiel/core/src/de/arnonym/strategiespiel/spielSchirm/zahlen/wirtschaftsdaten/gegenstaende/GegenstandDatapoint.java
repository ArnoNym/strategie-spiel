package de.arnonym.strategiespiel.spielSchirm.zahlen.wirtschaftsdaten.gegenstaende;

import de.arnonym.strategiespiel.spielSchirm.zahlen.wirtschaftsdaten.Datapoint;

public class GegenstandDatapoint extends Datapoint {
    private int preis;
    private int durchschnKaufKosten; // Ein- oder Verkauf
    private int durchschnLieferKosten;

    public GegenstandDatapoint(int eingang, int ausgang, int preis, int durchschnKaufKosten,
                               int durchschnLieferKosten) {
        super(eingang, ausgang);
        this.preis = preis;
        this.durchschnKaufKosten = durchschnKaufKosten;
        this.durchschnLieferKosten = durchschnLieferKosten;
    }

    protected void update(int eingang, int ausgang, int preis, int durchschnKaufKosten,
                          int durchschnLieferKosten) {
        update(eingang, ausgang, preis);
        this.durchschnKaufKosten = durchschnKaufKosten;
        this.durchschnLieferKosten = durchschnLieferKosten;
    }

    protected void update(int eingang, int ausgang, int preis) {
        super.update(eingang, ausgang);
        this.preis = preis;
    }

    // EINFACHE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public int getPreis() {
        return preis;
    }

    public int getDurchschnKaufKosten() {
        return durchschnKaufKosten;
    }

    public int getDurchschnLieferKosten() {
        return durchschnLieferKosten;
    }
}
