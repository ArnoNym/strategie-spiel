package de.arnonym.strategiespiel.spielSchirm.zahlen.wirtschaftsdaten.gegenstaende;

import com.badlogic.gdx.utils.IntMap;

import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.endstufe.Gegenstand;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.endstufe.GegenstandList;
import de.arnonym.strategiespiel.spielSchirm.zahlen.wirtschaftsdaten.TimeUnit;

public class GegenstandTimeUnit extends TimeUnit<GegenstandDatapoint> {
    private final GegenstandList gegenstandListe;

    public GegenstandTimeUnit(GegenstandList gegenstandListe) {
        if (gegenstandListe.isEmpty()) {
            throw new IllegalStateException("GegenstandListe muss bereits alle Gegenstaende "
                    + "enthalten (allerdings nicht zwangslaufig Mengen)! Sollten keine "
                    + "Gegenstaende noetig sein, sollte kein Wirtschaftsmanager erstellt werden.");
        }

        this.gegenstandListe = gegenstandListe;
        for (Gegenstand g : gegenstandListe) {
            g.setEingang(0);
            g.setAusgang(0);
            getMap().put(g.gettId(), new GegenstandDatapoint(0, 0, g.getWert(), 0, 0));
        }
    }

    public void updateDaten(int id, int durchschnKaufKosten, int durchschnLieferKosten) {
        Gegenstand g = gegenstandListe.gettDurchId(id);
        getMap().get(id).update(g.getEingang(), g.getAusgang(), g.getWert(), durchschnKaufKosten,
                durchschnLieferKosten);
    }

    public void updateDaten() {
        for (Gegenstand g : gegenstandListe) {
            int id = g.gettId();
            getMap().get(id).update(g.getEingang(), g.getAusgang(), g.getWert());
        }
    }
}
