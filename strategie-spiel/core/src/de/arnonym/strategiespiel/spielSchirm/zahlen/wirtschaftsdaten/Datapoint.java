package de.arnonym.strategiespiel.spielSchirm.zahlen.wirtschaftsdaten;

import java.io.Serializable;

public class Datapoint implements Serializable {
    private int eingang;
    private int ausgang;

    public Datapoint(int eingang, int ausgang) {
        this.eingang = eingang;
        this.ausgang = ausgang;
    }

    protected void update(int eingang, int ausgang) {
        this.eingang = eingang;
        this.ausgang = ausgang;
    }

    // PROTECTED GETTER UND SETTER ////////////////////////////////////////////////////////////////

    protected void setEingang(int eingang) {
        this.eingang = eingang;
    }

    protected void setAusgang(int ausgang) {
        this.ausgang = ausgang;
    }

    // EINFACHE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public int getEingang() {
        return eingang;
    }

    public int getAusgang() {
        return ausgang;
    }
}
