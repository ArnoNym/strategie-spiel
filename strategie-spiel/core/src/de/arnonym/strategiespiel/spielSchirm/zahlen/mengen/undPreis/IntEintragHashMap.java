package de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.undPreis;

import java.util.HashMap;

import de.arnonym.strategiespiel.framework.werkzeuge.utils.MathUtils;

public class IntEintragHashMap extends HashMap<Integer, MengePreisEintrag> {

    public IntEintragHashMap() {
        super();
    }

    public int summeAllerMengen() {
        int summeAllerValues = 0;
        for (MengePreisEintrag mengePreisEintrag : this.values()) {
            summeAllerValues += mengePreisEintrag.getMenge();
        }
        return summeAllerValues;
    }

    public int mengeMalPreis() {
        int summeAllerAusgaben = 0;
        for (int i : this.keySet()) {
            summeAllerAusgaben += MathUtils.myIntMult(this.get(i).getMenge(),
                    this.get(i).getPreis());
        }
        return summeAllerAusgaben;
    }

    public void addUpEntrys(IntEintragHashMap intEintragHashMap) {
        for (Entry<Integer, MengePreisEintrag> entry : intEintragHashMap.entrySet()) {
            addUpEntry(entry.getKey(), entry.getValue().getMenge(), entry.getValue().getPreis());
        }
    }
    public void addUpEntry(int key, int menge, int preis) {
        if (this.containsKey(key)) {
            int oldAmount = this.get(key).getMenge();
            int oldPrice = this.get(key).getPreis();
            int averagePrice = MathUtils.myIntDiv(
                    (MathUtils.myIntMult(oldAmount, oldPrice) + MathUtils.myIntMult(menge, preis))
                    , (oldAmount + menge));
            this.put(key, new MengePreisEintrag(oldAmount + menge, averagePrice));
        } else {
            this.put(key, new MengePreisEintrag(menge, preis));
        }
    }
}
