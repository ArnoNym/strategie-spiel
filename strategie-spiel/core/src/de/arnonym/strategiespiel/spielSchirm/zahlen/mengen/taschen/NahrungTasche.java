package de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.taschen;

import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.endstufe.Gegenstand;
import de.arnonym.strategiespiel.spielSchirm.info.muster.GegInfo;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.vorstufe.EntityGegInfoList;
import de.arnonym.strategiespiel.spielSchirm.balancing.Konstanten;
import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenBalance;

/**
 * Created by LeonPB on 29.08.2018.
 */

public class NahrungTasche extends NutzenTasche {
    private float produktivitaetMult; // 0 < x < 8 auf skala um 1
    private float gesundheitMult; // 0 < x < 8 auf skala um 1

    private float eiweissAnteil = 0;
    private float vitaminAnteil = 0;
    private float kohlenhydrateGesundAnteil = 0;
    private float kohlenhydrateUngesundAnteil = 0;

    private float nutzenEiweissFloat = 0;
    private float nutzenVitamineFloat = 0; //vitaminAnteil, mineralstoffe, ballaststoffe
    private float nutzenKohlenhydrateGesundFloat = 0;
    private float nutzenKohlenhydrateUngesundFloat = 0;

    private float nutzenNahrungMischenExponent; // 0<X<1
    private float gesundheitNahrungMischenExponent; // 0<X<1

    public NahrungTasche(EntityGegInfoList entityGegInfoListe) {
        super(entityGegInfoListe);
        nutzenNahrungMischenExponent = 1f; //TODO Konstanten
        gesundheitNahrungMischenExponent = 0.5f; //TODO
    }

    @Override
    protected void berechneWerte(boolean mitLegen) {
        eiweissAnteil = vitaminAnteil = kohlenhydrateGesundAnteil = kohlenhydrateUngesundAnteil = 0;
        nutzenEiweissFloat = nutzenVitamineFloat = nutzenKohlenhydrateGesundFloat = nutzenKohlenhydrateUngesundFloat = 0;
        int gegenstandListeMenge = gettMenge(true, mitLegen, false);
        for (Gegenstand g : this) {
            int gMenge = g.gettMenge(true, mitLegen, false);
            int gNutzen = g.getWert();

            float gea = g.gegMuster.getEiweissAnteil();
            eiweissAnteil += gMenge * gea / gegenstandListeMenge;
            nutzenEiweissFloat += gNutzen * gMenge * gea / (Konstanten.NACHKOMMASTELLEN * 2);

            float gva = g.gegMuster.getVitamineAnteil();
            vitaminAnteil += gMenge * gva / gegenstandListeMenge;
            nutzenVitamineFloat += gNutzen * gMenge * gva / (Konstanten.NACHKOMMASTELLEN * 2);

            float gkga = g.gegMuster.getKohlenhydrateGesundAnteil();
            kohlenhydrateGesundAnteil += gMenge * gkga / gegenstandListeMenge;
            nutzenKohlenhydrateGesundFloat += gNutzen * gMenge * gkga / (Konstanten.NACHKOMMASTELLEN * 2);

            float gkua = g.gegMuster.getVitamineAnteil();
            kohlenhydrateUngesundAnteil += gMenge * gkua / gegenstandListeMenge;
            nutzenKohlenhydrateUngesundFloat += gNutzen * gMenge * gkua / (Konstanten.NACHKOMMASTELLEN * 2);
        }
        int n = Konstanten.NACHKOMMASTELLEN * Math.max (1,
                (int) (Math.pow(nutzenEiweissFloat, nutzenNahrungMischenExponent)
                        + Math.pow(nutzenVitamineFloat, nutzenNahrungMischenExponent)
                        + Math.pow((nutzenKohlenhydrateGesundFloat + nutzenKohlenhydrateUngesundFloat),
                        nutzenNahrungMischenExponent)));
        if (mitLegen) {
            setMitLegenNutzen(n);
            return; // Wichtig!
        }
        setNutzen(n);

        //Errechnen durch Ernährungs­pyramide des Bundes­zentrums für Ernährung (BZfE) https://getsurance.de/ratgeber/ernaehrungspyramide/
        produktivitaetMult = (eiweissAnteil + 2 * kohlenhydrateGesundAnteil + kohlenhydrateUngesundAnteil) / 4;
        gesundheitMult = Math.min(1, gettMenge(true, false, true)
                / KonstantenBalance.DORFBEWOHNER_NAHRUNG_GRUNDBEDARF_AUFFUELLEN_BIS)
                * ((float) (Math.pow(eiweissAnteil, gesundheitNahrungMischenExponent)
                + Math.pow(vitaminAnteil, gesundheitNahrungMischenExponent)
                + Math.pow(Math.min(kohlenhydrateGesundAnteil - kohlenhydrateUngesundAnteil, 0),
                gesundheitNahrungMischenExponent)));
    }

    @Override
    protected int gettGrenznutzen_or(Gegenstand g) {
        GegInfo gegInfo = g.gegMuster;

        // Erklaerung im nicht Git Ordner EcsDokumentation-001/002-wert
        return (int) (getNutzenExponent() * Math.pow(gettNutzen()/Konstanten.NACHKOMMASTELLEN, getNutzenExponent()-1) //g'(h)

                * nutzenNahrungMischenExponent

                * (Math.pow(nutzenEiweissFloat, nutzenNahrungMischenExponent -1) * g.getWert() * gegInfo.getEiweissAnteil()
                + Math.pow(nutzenVitamineFloat, nutzenNahrungMischenExponent -1) * g.getWert() * gegInfo.getVitamineAnteil()
                + Math.pow((nutzenKohlenhydrateGesundFloat + nutzenKohlenhydrateUngesundFloat), nutzenNahrungMischenExponent -1)
                * g.getWert() * (gegInfo.getKohlenhydrateGesundAnteil() + gegInfo.getKohlenhydrateUngesundAnteil())));
    }

    // KOMPLEXE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public float gettEiweiss() {
        pruefeUndAktualisiereWerte(false);
        return eiweissAnteil;
    }

    public float gettVitamine() {
        pruefeUndAktualisiereWerte(false);
        return vitaminAnteil;
    }

    public float gettKohlenhydrate_gesund() {
        pruefeUndAktualisiereWerte(false);
        return kohlenhydrateGesundAnteil;
    }

    public float gettKohlenhydrate_ungesund() {
        pruefeUndAktualisiereWerte(false);
        return kohlenhydrateUngesundAnteil;
    }

    public float gettProduktivitaet() {
        pruefeUndAktualisiereWerte(false);
        return produktivitaetMult;
    }

    public float gettGesundheit() {
        pruefeUndAktualisiereWerte(false);
        return gesundheitMult;
    }
}