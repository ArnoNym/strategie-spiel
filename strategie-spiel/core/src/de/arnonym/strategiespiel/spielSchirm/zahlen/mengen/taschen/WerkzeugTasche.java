package de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.taschen;

import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dorfbewohner.familie.Familie;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.endstufe.Gegenstand;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.vorstufe.EntityGegInfoList;
import de.arnonym.strategiespiel.spielSchirm.zahlen.schaden.Schaden;
import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenBalance;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.MathUtils;

public class WerkzeugTasche extends Tasche {
    private final Familie familie;
    private float produktivitaet; // 0 < x < 8. Auf Skala um 1

    public WerkzeugTasche(EntityGegInfoList entityGegInfoListe, Familie familie) {
        super(entityGegInfoListe);
        this.familie = familie;

        for (Gegenstand g : this) {
            g.setMengeMaximal(KonstantenBalance.TASCHE_PRODUKTIVITAET_MAX_MENGE_PRO_GEG);
        }
    }

    @Override
    public void berechneWerte(boolean mitLegen) {
        /*float summeGesundheitSchutz = 0;
        float summeProduktivitaet = 1;
        int size = 0;
        for (GegenstandWerkzeug gw : this) {
            int gwMenge = gw.gettMenge(true, mitLegen, false);
            if (gwMenge > 0) {
                summeGesundheitSchutz += gwMenge * gw.gettPlatzVerbrauch();

                summeProduktivitaet += gw.getProduktivitaet();
                size += 1;
            }
        }
        produktivitaet = summeProduktivitaet / size;

        float arbeitszeitVerringerun_prozent = 1 / produktivitaet;

        int gespartesGeldProArbeitszeit = arbeitszeitVerringerun_prozent
                * familie.getFamilienmitglieder().createRandom(0).getDorf().gettArbeitszeitProzent()
                * familie.gettDurchschnGehalt(Enums.ZeitEnum.INGAME_MONAT); // TODO Fraglich ob Monat hier passt. Muesste man ausprobieren

        setNutzen(); wert durch prod ist ab´haengig vona rbeitszeit*/
        produktivitaet = 1;
        setNutzen(1000); //TODO
    }

    @Override
    protected int gettGrenznutzen_or(Gegenstand geg) {
        return 1000; //TODO
        /*GegenstandWerkzeug gp = this.getDurchId(gegId);
        float einflussAufTascheProduktivitaet =
        float wertSekunde = familie.gettDurchschnGehalt();
        float verlaengertSekundeUm = gp.getProduktivitaet();
        float haufigkeit
        todo

        float prodGrenznutzen = (int) (gp.getProduktivitaet() * gp.gettPlatzVerbrauch()
                * KonstantenBalancing.GESUNDHEITSSCHADEN_ZU_GEGENSTAND_SCHADEN_MULT
                * familie.gettDurchschnGehalt() / Konstanten.NACHKOMMASTELLEN);
        float gesundheitFaktor = (float) Math.pow(gp.getGesundheit(),
                familie.gettDurchschnWichtigkeitGesundheit());
        return (int) (prodGrenznutzen + gesundheitFaktor);*/
    }

    // VERBAUCHE FUER SCHUTZ //////////////////////////////////////////////////////////////////////

    public float schuetzen(Schaden roherSchaden) {
        if (roherSchaden.value <= 0) {
            return 0;
        }

        int gesamterMengenPlatz = this.getMaxPlatz() - this.getPlatz(); /* Ja, MengePlatz ist
        korrekt, da hier der Platz der Gesamten GegListe verwendet wird */
        if (gesamterMengenPlatz <= 0) {
            return roherSchaden.value;
        }

        // Gett Tasche Schutz
        float lungeSchutz = 0; int anzahlLungeSchutz = 0;
        float kopfSchutz = 0; int anzahlKopfSchutz = 0;
        float sonstigesSchutz = 0; int anzahlSonstigesSchutz = 0;
        float haendeSchutz = 0; int anzahlHaendeSchutz = 0;
        float fuesseSchutz = 0; int anzahlFuesseSchutz = 0;
        for (Gegenstand g : this) {
            if (g.getMengeVerfuegbar() > 0) {
                float gpSchutz = g.gegMuster.getSchutz();
                switch (g.gegMuster.getSchuetztKoeperteil()) {
                    case LUNGE:
                        lungeSchutz += gpSchutz;
                        anzahlLungeSchutz += 1;
                    case KOPF:
                        kopfSchutz += gpSchutz;
                        anzahlKopfSchutz += 1;
                    case SONSTIGES:
                        sonstigesSchutz += gpSchutz;
                        anzahlSonstigesSchutz += 1;
                    case HAENDE:
                        haendeSchutz += gpSchutz;
                        anzahlHaendeSchutz += 1;
                    case FUESSE:
                        fuesseSchutz += gpSchutz;
                        anzahlFuesseSchutz += 1;
                }
            }
        }
        lungeSchutz = lungeSchutz / anzahlLungeSchutz;
        kopfSchutz = kopfSchutz / anzahlKopfSchutz;
        sonstigesSchutz = sonstigesSchutz / anzahlSonstigesSchutz;
        haendeSchutz = haendeSchutz / anzahlHaendeSchutz;
        fuesseSchutz = fuesseSchutz / anzahlFuesseSchutz;

        // Schaden berechnen
        float lungeNachSchutzSchaden = roherSchaden.value * roherSchaden.lunge_prozent
                * (1 - lungeSchutz);
        float kopfNachSchutzSchaden = roherSchaden.value * roherSchaden.kopf_prozent
                * (1 - kopfSchutz);
        float sonstigesNachSchutzSchaden = roherSchaden.value * roherSchaden.sonstiges_prozent
                * (1 - sonstigesSchutz);
        float haendeNachSchutzSchaden = roherSchaden.value * roherSchaden.haende_prozent
                * (1 - haendeSchutz);
        float fuesseNachSchutzSchaden = roherSchaden.value * roherSchaden.fuesse_prozent
                * (1 - fuesseSchutz);
        float nachSchutzSchaden = lungeNachSchutzSchaden + kopfNachSchutzSchaden
                + sonstigesNachSchutzSchaden + haendeNachSchutzSchaden + fuesseNachSchutzSchaden;

        // Gegenstaende entsprechend des Schadens verbrauchen
        float lungeGegSchaden = gettGegSchaden(roherSchaden.value, lungeNachSchutzSchaden);
        float kopfGegSchaden = gettGegSchaden(roherSchaden.value, kopfNachSchutzSchaden);
        float sonstigesGegSchaden = gettGegSchaden(roherSchaden.value, sonstigesNachSchutzSchaden);
        float haendeGegSchaden = gettGegSchaden(roherSchaden.value, haendeNachSchutzSchaden);
        float fuesseGegSchaden = gettGegSchaden(roherSchaden.value, fuesseNachSchutzSchaden);
        for (Gegenstand g : this) {
            float passenderGegSchaden;
            switch (g.gegMuster.getSchuetztKoeperteil()) {
                case LUNGE: passenderGegSchaden = lungeGegSchaden;
                break;
                case KOPF: passenderGegSchaden = kopfGegSchaden;
                break;
                case SONSTIGES: passenderGegSchaden = sonstigesGegSchaden;
                break;
                case HAENDE: passenderGegSchaden = haendeGegSchaden;
                break;
                case FUESSE: passenderGegSchaden = fuesseGegSchaden;
                break;
                default: throw new IllegalStateException();
            }
            int gegVerbrauch = (int) ((float) MathUtils.myIntMult(g.gegMuster.platzVerbrauch,
                    g.getMengeVerfuegbar()) / gesamterMengenPlatz * passenderGegSchaden);
            g.mengeVerringern(gegVerbrauch, false);
        }

        return nachSchutzSchaden;
    }

    private float gettGegSchaden(float roherSchadenValue, float schadenNachSchutz) {
        return (roherSchadenValue - schadenNachSchutz)
                * KonstantenBalance.GESUNDHEITSSCHADEN_ZU_GEGENSTAND_SCHADEN_MULT;
    }

    // EINFACHE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public float getProduktivitaet() {
        return produktivitaet;
    }
}