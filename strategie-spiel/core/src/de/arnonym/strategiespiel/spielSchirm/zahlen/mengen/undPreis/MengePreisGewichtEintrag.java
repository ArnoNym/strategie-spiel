package de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.undPreis;

public class MengePreisGewichtEintrag extends MengePreisEintrag implements java.io.Serializable {

    private int gewicht;

    public MengePreisGewichtEintrag(int menge, int preis, int gewicht) {
        super(menge, preis);
        this.gewicht = gewicht;
    }

    public MengePreisGewichtEintrag(MengePreisGewichtEintrag mengePreisGewichtEintrag) {
        super(mengePreisGewichtEintrag);
        this.gewicht = mengePreisGewichtEintrag.getGewicht();
    }

    public int getGewicht() {
        return gewicht;
    }

    public void setGewicht(int gewicht) {
        this.gewicht = gewicht;
    }
}
