package de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.endstufe;

import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.Geld;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.Menge;
import de.arnonym.strategiespiel.spielSchirm.info.muster.GegInfo;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.taschen.Tasche;
import de.arnonym.strategiespiel.framework.werkzeuge.collections.id.IdObjekt;
import de.arnonym.strategiespiel.spielSchirm.balancing.Konstanten;
import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenBalance;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.MathUtils;

/**
 * Created by LeonPB on 24.08.2018.
 */

public class Gegenstand extends Menge implements IdObjekt {
    public final GegInfo gegMuster;
    private final GegenstandList gegenstandListe;

    private int wert; // Nutzen-Vorfaktor oder Preis
    private float gesammeltVergammelnSekunden = 0;
    private float gesammeltWachsenSekunden = 0;

    /** Fuer die Wirtschaftsdaten */
    private int eingang = 0;
    private int ausgang = 0;

    protected Gegenstand(GegenstandList gegenstandListe, GegInfo gegMuster, int mengeVerfuegbar,
                         Integer mengeMaximal_nl, int wert) {
        super(gegMuster.teilbar_create, mengeVerfuegbar, mengeMaximal_nl);
        this.gegMuster = gegMuster;
        this.gegenstandListe = gegenstandListe;
        this.wert = wert;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////

    // VERGAMMELN /////////////////////////////////////////////////////////////////////////////////
    /* Da bei Taschen alle Gegenstaende teilbar werden, da sie onehin nicht mehr aus der Tasche
    entfernt werden koennen wird dort kein vergammeln verwendet weil es komplizierter ist und
    schwieriger ist zB den Verschleiss einer Spitzhacke darzustellen */

    public void vergammeln(float delta, float vergammelnMod) { // Mod zB bei nassem Wetter
        int mengeVerfuegbar = getMengeVerfuegbar();
        if (mengeVerfuegbar == 0) return;

        gesammeltVergammelnSekunden += delta;

        int vergammelnMenge = Math.min(mengeVerfuegbar,
                (int) (mengeVerfuegbar * gegMuster.getVergammelnProzentProJahr()
                        * gesammeltVergammelnSekunden / KonstantenBalance.SEKUNDEN_PRO_JAHR
                        * vergammelnMod));
        //todo das verlangsamt sich sehr stark mit weniger werdender maenge. Aendern, dass gleichbleibend!
        if (isTeilbar()) {
            if (vergammelnMenge < 1) {
                return;
            }
        } else {
            vergammelnMenge = MathUtils.myIntRunden(vergammelnMenge, 0);
            if (vergammelnMenge < Konstanten.NACHKOMMASTELLEN) {
                return;
            }
        }

        mengeVerringern(vergammelnMenge, false);
        gesammeltVergammelnSekunden = 0;
    }

    // WACHSEN ////////////////////////////////////////////////////////////////////////////////////

    public void wachsen(float modDelta, int maxPlatz) { // Mod zB bei guter Pflege
        if (!gegMuster.isNachwachsbar()) throw new IllegalStateException();

        gesammeltWachsenSekunden += modDelta;

        int wachsenMenge = Math.min(MathUtils.myIntDiv(maxPlatz, gegMuster.platzVerbrauch),
                (int) (gegMuster.getNachwachsenMenge_jahr()
                        * modDelta / KonstantenBalance.SEKUNDEN_PRO_JAHR));

        if (isTeilbar()) {
            if (wachsenMenge < 1) {
                return;
            }
        } else {
            wachsenMenge = MathUtils.myIntRunden(wachsenMenge, 0);
            if (wachsenMenge < Konstanten.NACHKOMMASTELLEN) {
                return;
            }
        }

        mengeErhoehen(wachsenMenge, false);
        gesammeltWachsenSekunden = 0;
    }

    // PREISE /////////////////////////////////////////////////////////////////////////////////////

    @Deprecated
    public boolean bezahlbar(Geld geld, boolean mitMengeLegen) {
        int geldVerfuegbar = geld.gettMenge(true, mitMengeLegen, false);
        return bezahlbar(geldVerfuegbar);
    }

    public boolean bezahlbar(int geldMenge) {
        return geldMenge >= 1 && (isTeilbar() || geldMenge >= wert);
    }

    // PLATZ //////////////////////////////////////////////////////////////////////////////////////

    protected void verbauchePlatz(int menge) {
        pruefeTeilbarEingehalten(menge);
        gegenstandListe.verbauchePlatz(MathUtils.myIntMult(menge, gegMuster.platzVerbrauch));
    }

    protected void erhoehePlatz(int menge) {
        pruefeTeilbarEingehalten(menge);
        gegenstandListe.erhoehenPlatz(MathUtils.myIntMult(menge, gegMuster.platzVerbrauch));
    }

    public int getMengeBisVoll() {
        int mengeBisVoll = MathUtils.myIntDiv(gegenstandListe.getPlatz(), gegMuster.platzVerbrauch);
        Integer mengeMaximal = getMengeMaximal();
        if (mengeMaximal != null) {
            mengeBisVoll = Math.min(mengeBisVoll, mengeMaximal - getMengeVerfuegbar()
                    - getMengeNehmenReserviert() - getMengeLegenReserviert());
        }
        if (!isTeilbar()) { // Nicht GegInfo.teilbar_init, da Teilbarkeit zB in Taschen veraendert
            int rest = mengeBisVoll % Konstanten.NACHKOMMASTELLEN;
            mengeBisVoll -= rest;
        }
        return mengeBisVoll;
    }

    public boolean issVoll() {
        return getMengeBisVoll() == 0;
    }

    public int gettMengePlatz(boolean verfuegbar, boolean legen, boolean nehmen) {
        return gettMenge(verfuegbar, legen, nehmen) * gegMuster.platzVerbrauch;
    }

    // NUTZEN GEGENSTAND //////////////////////////////////////////////////////////////////////////

    public int gettGrenznutzen() {
        return ((Tasche) gegenstandListe).gettGrenznutzen(this); /* Wenn Casten scheitert handelt
        es sich scheinbar nicht um einen Nutzen Gegenstand */
    }

    // MENGEN VERAENDERUNG ////////////////////////////////////////////////////////////////////////

    @Override
    public void mengeErhoehen(int menge, boolean reserviertLegen) {
        super.mengeErhoehen(menge, reserviertLegen);
        eingang += menge;
        gegenstandListe.veraenderungMengeVerfuegbar(this, menge);
    }

    @Override
    public void mengeVerringern(int menge, boolean reserviertNehmen) {
        super.mengeVerringern(menge, reserviertNehmen);
        ausgang += menge;
        gegenstandListe.veraenderungMengeVerfuegbar(this, - menge);
    }

    @Override
    public void reservierenNehmen(int menge) {
        super.reservierenNehmen(menge);
        gegenstandListe.veraenderungMengeNehmen(this, menge);
    }

    @Override
    public void reservierenNehmenRueckgaengig(int menge) {
        super.reservierenNehmenRueckgaengig(menge);
        gegenstandListe.veraenderungMengeNehmen(this, - menge);
    }

    @Override
    public void reservierenLegen(int menge) {
        super.reservierenLegen(menge);
        gegenstandListe.veraenderungMengeLegen(this, menge);
    }

    @Override
    public void reservierenLegenRueckgaengig(int menge) {
        super.reservierenLegenRueckgaengig(menge);
        gegenstandListe.veraenderungMengeLegen(this, - menge);
    }

    // KOMPLEXE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    @Override
    public int gettId() {
        return gegMuster.gettId();
    }

    // SONSTIGE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public int getWert() {
        return wert;
    }

    public void setWert(int wert) {
        this.wert = wert;
    }

    public int getEingang() {
        return eingang;
    }

    public void setEingang(int eingang) {
        this.eingang = eingang;
    }

    public int getAusgang() {
        return ausgang;
    }

    public void setAusgang(int ausgang) {
        this.ausgang = ausgang;
    }
}
