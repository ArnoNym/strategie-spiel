package de.arnonym.strategiespiel.spielSchirm.zahlen.wirtschaftsdaten.geld;

import com.badlogic.gdx.utils.LongMap;

import de.arnonym.strategiespiel.spielSchirm.zahlen.wirtschaftsdaten.Manager;

public class GeldManager extends Manager<GeldTimeUnit, GeldDatapoint> {

    public GeldManager() {
        super(new GeldTimeUnit());
    }

    public void updateDaten(long entityId, int eingang, int ausgang) {
        LongMap<GeldDatapoint> map = getAktuelleTimeUnit().getMap();

        GeldDatapoint geldDatapoint = map.get(entityId);
        if (geldDatapoint == null) {
            map.put(entityId, new GeldDatapoint(eingang, ausgang));
        } else {
            geldDatapoint.add(eingang, ausgang);
            map.put(entityId, geldDatapoint);
        }
    }

    @Override
    public GeldTimeUnit newTimeUnit() {
        return new GeldTimeUnit();
    }

    private static GeldDatapoint emptyDatapoint = new GeldDatapoint(0, 0);

    @Override
    protected GeldDatapoint gettDatapoint_or(long id, GeldDatapoint geldDatapoint) {
        if (geldDatapoint == null) {
            return emptyDatapoint;
        }
        return geldDatapoint;
    }
}
