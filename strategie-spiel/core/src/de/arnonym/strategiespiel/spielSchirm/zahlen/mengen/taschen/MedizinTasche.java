package de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.taschen;

import de.arnonym.strategiespiel.framework.werkzeuge.numbers.unit.ZeitEnum;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.dorfbewohner.familie.Familie;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.endstufe.Gegenstand;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.vorstufe.EntityGegInfoList;

public class MedizinTasche extends Tasche {
    private final Familie familie;
    private int gesundheit; /* 0 < x < 8. Auf Skale um die hp eines DBs herum.
    !!! WICHTIG 1 Gesundheit == 1 Lebensjahr WICHITG !!! */

    public MedizinTasche(EntityGegInfoList entityGegInfoListe, Familie familie) {
        super(entityGegInfoListe);
        this.familie = familie;
    }

    @Override
    public void berechneWerte(boolean mitLegen) {
        if (mitLegen) throw new IllegalStateException();
        gesundheit = 0;
        for (Gegenstand geg : this) {
            gesundheit += geg.gegMuster.getGesundheit();
        }
        setNutzen(gesundheit * familie.gettDurchschnGehalt(ZeitEnum.INGAME_JAHR));
    }

    @Override
    protected int gettGrenznutzen_or(Gegenstand geg) {
        float gegGesundheit = geg.gegMuster.getGesundheit();
        return (int) (gegGesundheit * familie.gettDurchschnGehalt(ZeitEnum.INGAME_JAHR));
    }

    public int verbauchen() {
        return 1; // TODO
    }
}
