package de.arnonym.strategiespiel.spielSchirm.zahlen.schaden;

import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenFehlerAusgleich;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.MathUtils;

public class Schaden {
    public final float value;
    public final float kopf_prozent;
    public final float lunge_prozent;
    public final float haende_prozent;
    public final float fuesse_prozent;
    public final float sonstiges_prozent;

    public Schaden(float value, float kopf_prozent, float lunge_prozent, float haende_prozent,
                   float fuesse_prozent, float sonstiges_prozent) {
        this.value = value;
        this.kopf_prozent = kopf_prozent;
        this.lunge_prozent = lunge_prozent;
        this.haende_prozent = haende_prozent;
        this.fuesse_prozent = fuesse_prozent;
        this.sonstiges_prozent = sonstiges_prozent;

        if (MathUtils.runden(kopf_prozent + lunge_prozent + haende_prozent + fuesse_prozent
                + sonstiges_prozent,
                KonstantenFehlerAusgleich.SEKUNDEN_RUNDEN_NACHKOMMASTELLEN) != 1) {
            throw new IllegalArgumentException();
        }
    }
}
