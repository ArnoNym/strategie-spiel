package de.arnonym.strategiespiel.spielSchirm.zahlen.wirtschaftsdaten.gegenstaende;

import java.util.Map;

import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.endstufe.GegenstandList;
import de.arnonym.strategiespiel.spielSchirm.zahlen.wirtschaftsdaten.Manager;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.undPreis.IntEintragHashMap;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.undPreis.MengePreisEintrag;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.MathUtils;

public class GegenstandManager extends Manager<GegenstandTimeUnit, GegenstandDatapoint> {
    private GegenstandList gegenstandListe;

    public GegenstandManager(GegenstandList gegenstandListe) {
        super(new GegenstandTimeUnit(gegenstandListe));
        this.gegenstandListe = gegenstandListe;
    }

    @Override
    public void update(float delta) {
        super.update(delta);
        getAktuelleTimeUnit().updateDaten();
    }

    @Override
    public void neueTimeUnit() {
        if (getAktuelleTimeUnit() != null) {
            getAktuelleTimeUnit().updateDaten();
        }
        super.neueTimeUnit();
    }

    @Override
    public GegenstandTimeUnit newTimeUnit() {
        return new GegenstandTimeUnit(gegenstandListe);
    }

    @Override
    protected GegenstandDatapoint gettDatapoint_or(long id, GegenstandDatapoint gegenstandDatapoint) {
        if (gegenstandDatapoint == null) {
            throw new IllegalArgumentException("Ungueltige Id: " + id);
        }
        return gegenstandDatapoint;
    }

    public void addLieferKosten(int lieferKosten, IntEintragHashMap intEintragHashMap) {
        int addGesamtMengenPreis = intEintragHashMap.mengeMalPreis();

        for (Map.Entry<Integer, MengePreisEintrag> entry : intEintragHashMap.entrySet()) {
            int id = entry.getKey();
            MengePreisEintrag mengePreisEintrag = entry.getValue();

            GegenstandDatapoint gegenstandObject = gettDatapoint(id, -1);
            int oldMenge = gegenstandObject.getEingang();
            int oldDurchschnLieferKosten = gegenstandObject.getDurchschnLieferKosten();
            int oldDurchschnKaufKosten = gegenstandObject.getDurchschnKaufKosten();

            int addMenge =  mengePreisEintrag.getMenge();
            int addMengePreis = MathUtils.myIntMult(addMenge, mengePreisEintrag.getPreis());
            int addLieferKosten = lieferKosten * MathUtils.myIntDiv(addMengePreis, addGesamtMengenPreis);
            int addKaufKosten = MathUtils.myIntDiv(mengePreisEintrag.getPreis(), addMenge);

            int updatedEingang = oldMenge + addMenge; //TODO Sicher, dass wen das hier gecallt wird der oldMenge noch nicht geupdated wurde
            int updatedDurchschnLieferKosten = MathUtils.myIntDiv(
                    MathUtils.myIntMult(oldMenge, oldDurchschnLieferKosten) + addLieferKosten,
                    updatedEingang);
            int updatedDurchschnKaufKosten = MathUtils.myIntDiv(
                    MathUtils.myIntMult(oldMenge, oldDurchschnKaufKosten) + addKaufKosten,
                    updatedEingang);

            getAktuelleTimeUnit().updateDaten(
                    id,
                    updatedDurchschnKaufKosten,
                    updatedDurchschnLieferKosten);
        }
    }

    // KOMPLEXE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public int gettDurchschnKaufKosten(int id, int betrachtePerioden) {
        long periode = getPeriode();
        int summeLieferKosten = 0;
        long p;
        for (p = periode; (p > periode - betrachtePerioden && p >= 0); p--) {
            summeLieferKosten += gettDatapoint(id, p).getDurchschnKaufKosten();
        }
        return summeLieferKosten / (int) (periode - p);
    }

    public int gettDurchschnLieferKosten(int id, int betrachtePerioden) {
        long periode = getPeriode();
        int summeLieferKosten = 0;
        long p;
        for (p = periode; (p > periode - betrachtePerioden && p >= 0); p--) {
            summeLieferKosten += gettDatapoint(id, p).getDurchschnLieferKosten();
        }
        return summeLieferKosten / (int) (periode - p);
    }

    // EINFACHE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public GegenstandList getGegenstandListe() {
        return gegenstandListe;
    }
}
