package de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.endstufe;

import java.io.Serializable;

import de.arnonym.strategiespiel.spielSchirm.balancing.IdsGegenstaende;
import de.arnonym.strategiespiel.spielSchirm.balancing.Konstanten;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.vorstufe.EntityGegInfo;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.vorstufe.EntityGegInfoList;
import de.arnonym.strategiespiel.framework.werkzeuge.collections.hashMaps.IntIntHashMap;
import de.arnonym.strategiespiel.framework.werkzeuge.collections.id.IdList;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.MathUtils;

/**
 * Created by LeonPB on 24.08.2018.
 */

public class GegenstandList extends IdList<Gegenstand> implements Serializable {
    private int maxPlatzUrspruenglich;
    private int maxPlatz;
    private int platz;

    public GegenstandList(EntityGegInfoList entityGegInfoListe) {
        settMaxPlatz(entityGegInfoListe.getMaxPlatz(), true, true);

        for (EntityGegInfo entityGegInfo : entityGegInfoListe) {
            addGegenstand(entityGegInfo);
        }
    }

    public void mengenErhoehenBis(EntityGegInfoList entityGegInfoListe) {
        settMaxPlatz(entityGegInfoListe.getMaxPlatz(), true, false);

        for (EntityGegInfo entityGegInfo : entityGegInfoListe) {
            int id = entityGegInfo.id;
            if (contains(id)) {
                Gegenstand g = gettDurchId(id);
                g.setMengeMaximal(entityGegInfo.getMaxMenge());
                g.setWert(entityGegInfo.getWert());
                int mengeVeraendern = entityGegInfo.getMengeVerfuegbar() - g.gettMenge(true, true, true);
                System.out.println("GegenstandList ::: mengeVeraendern: "+mengeVeraendern+", entityGegInfo.getMengeVerfuegbar(): "+entityGegInfo.getMengeVerfuegbar()+", g.getMengeVerfuegbar(): "+g.getMengeVerfuegbar());
                if (mengeVeraendern > 0) {
                    g.mengeErhoehen(mengeVeraendern, false);
                } else if (mengeVeraendern < 0){
                    g.mengeVerringern(mengeVeraendern, false);
                }
            } else {
                addGegenstand(entityGegInfo);
            }
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////

    public Gegenstand addGegenstand(EntityGegInfo entityGegInfo) {
        return addGegenstand(entityGegInfo.id, entityGegInfo.getMengeVerfuegbar(),
                entityGegInfo.getMaxMenge(), entityGegInfo.getWert());
    }

    public Gegenstand addGegenstand(int id, int mengeVerfuegbar, Integer mengeMax_nl, int wert) {
        Gegenstand addGegenstand = new Gegenstand(this, IdsGegenstaende.get(id),
                mengeVerfuegbar, mengeMax_nl, wert);
        this.add(addGegenstand, false);
        addGegenstand.verbauchePlatz(mengeVerfuegbar);
        return addGegenstand;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////

    // VERWALTE PLATZ /////////////////////////////////////////////////////////////////////////////

    protected void verbauchePlatz(float platz) {
        this.platz -= platz;
        if (Konstanten.DEVELOPER_EXCEPTIONS && this.platz < 0) {
            throw new IllegalArgumentException(this.platz+", "+platz);
        }
    }

    protected void erhoehenPlatz(float platz) {
        this.platz += platz;
        if (Konstanten.DEVELOPER_EXCEPTIONS && this.platz > maxPlatz) {
            throw new IllegalArgumentException(this.platz+", "+platz);
        }
    }

    // GETT SUMMEN ////////////////////////////////////////////////////////////////////////////////

    public int gettMenge(boolean verfuegbar, boolean legen, boolean nehmen) {
        int mengeVerfuegbar = 0;
        for (Gegenstand g : this) {
            mengeVerfuegbar += g.gettMenge(verfuegbar, legen, nehmen);
        }
        return mengeVerfuegbar;
    }

    public int gettPlatzMalMengen(boolean verfuegbar, boolean legen, boolean nehmen) {
        int platzMenge = 0;
        for (Gegenstand g : this) {
            platzMenge += MathUtils.myIntMult(g.gettMenge(verfuegbar, legen, nehmen),
                    g.gegMuster.platzVerbrauch);
        }
        return platzMenge;
    }

    public int gettPlatzMalMengenMaximal() {
        int platzMenge = 0;
        for (Gegenstand g : this) {
            platzMenge += MathUtils.myIntMult(g.gegMuster.platzVerbrauch, g.getMengeMaximal());
        }
        return platzMenge;
    }

    // GETT SONSTIGE COLLECTIONS //////////////////////////////////////////////////////////////////

    public IntIntHashMap gettMengeAktuellIntIntHashmap() {
        IntIntHashMap intIntHashMap = new IntIntHashMap();
        for (Gegenstand g : this) {
            intIntHashMap.put(g.gettId(), g.getMengeVerfuegbar());
        }
        return intIntHashMap;
    }

    public int[] getIds() {
        int[] returnIds = new int[this.size()];
        int i = 0;
        for (Gegenstand g : this) {
            returnIds[i] = g.gettId();
            i++;
        }
        return returnIds;
    }

    // MENGE VERAENDERUNG /////////////////////////////////////////////////////////////////////////

    protected void veraenderungMengeVerfuegbar(Gegenstand gegenstand, int veraendertUmMenge) {
        // Ueberschreibe bei Bedarf in Taschen etc
    }

    protected void veraenderungMengeLegen(Gegenstand gegenstand, int veraendertUmMenge) {
        // Ueberschreibe bei Bedarf in Taschen etc
    }

    protected void veraenderungMengeNehmen(Gegenstand gegenstand, int veraendertUmMenge) {
        // Ueberschreibe bei Bedarf in Taschen etc
    }

    // KOMPLEXE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public boolean issMenge(boolean verfuegbar, boolean legen, boolean nehmen) {
        return gettMenge(verfuegbar, legen, nehmen) > 0;
    }

    public boolean issPlatzFuerEtwas(Gegenstand gegenstand) {
        return platz >= gegenstand.gegMuster.platzVerbrauch
                || (gegenstand.isTeilbar() && platz > 0);
    }

    public boolean issPlatzFuerAlles(Gegenstand gegenstand) {
        return platz >= gegenstand.gettMengePlatz(true, false, false);
    }

    public void settMaxPlatz(int maxPlatz, boolean setMaxPlatzUrspruenglich, boolean resetPlatz) {
        if (resetPlatz) {
            this.platz = maxPlatz;
        } else {
            int platzVeraenderung = maxPlatz - this.maxPlatz;
            this.platz += platzVeraenderung;
        }
        this.maxPlatz = maxPlatz;
        if (setMaxPlatzUrspruenglich) this.maxPlatzUrspruenglich = maxPlatz;
    }

    // PROTECTED GETTER UND SETTER ////////////////////////////////////////////////////////////////

    // EINFACHE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public int getPlatz() {
        return platz;
    }

    public int getMaxPlatz() {
        return maxPlatz;
    }

    public int getMaxPlatzUrspruenglich() {
        return maxPlatzUrspruenglich;
    }
}