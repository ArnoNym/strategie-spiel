package de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.taschen;

import de.arnonym.strategiespiel.spielSchirm.balancing.IdsGegenstaende;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.endstufe.Gegenstand;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.vorstufe.EntityGegInfoList;
import de.arnonym.strategiespiel.spielSchirm.balancing.Konstanten;

public class SpielzeugTasche extends NutzenTasche {

    public SpielzeugTasche(EntityGegInfoList entityGegInfoListe) {
        super(entityGegInfoListe);
    }

    @Override
    public void berechneWerte(boolean mitLegen) {
        int n = 1; //Verhindert Grenznutzen der NaN ist (weil durch unendlich geteilt wird
        for (Gegenstand g : this) {
            int gMenge =  g.gettMenge(true, mitLegen, false);
            n += gMenge * g.getWert() / Konstanten.NACHKOMMASTELLEN;
        }

        if (mitLegen) {
            setMitLegenNutzen(n);
        } else {
            setNutzen(n);
        }

    }

    @Override
    public int gettGrenznutzen_or(Gegenstand gegenstand) {
        pruefeUndAktualisiereWerte(true);
        return (int) (getNutzenExponent() * Math.pow(gettMitLegenNutzen()
                    / Konstanten.NACHKOMMASTELLEN, getNutzenExponent() - 1) //g'(h)
                * gegenstand.getWert());//h'
    }
}
