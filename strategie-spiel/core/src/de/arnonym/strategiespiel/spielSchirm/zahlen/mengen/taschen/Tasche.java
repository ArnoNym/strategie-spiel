package de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.taschen;

import java.io.Serializable;

import de.arnonym.strategiespiel.spielSchirm.balancing.IdsGegenstaende;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.endstufe.Gegenstand;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.endstufe.GegenstandList;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.vorstufe.EntityGegInfoList;

public abstract class Tasche extends GegenstandList implements Serializable {
    private float nutzenExponent;
    private boolean veraenderungInVerfuegbar = true;
    private boolean veraenderungInLegen = true;

    private int nutzen; /* 0 < X < unendlich. Bewertet in einer Zahl wie gut der DB den Inhalt
    dieser Tasche findet */

    public Tasche(EntityGegInfoList entityGegInfoListe) {
        super(entityGegInfoListe);
        for (Gegenstand g : this) {
            g.setTeilbar(true);
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////

    // MENGEN VERAENDERUNG ////////////////////////////////////////////////////////////////////////

    @Override
    public void veraenderungMengeVerfuegbar(Gegenstand gegenstand, int veraendertUmMenge) {
        veraenderungInVerfuegbar = true;
    }

    @Override
    public void veraenderungMengeLegen(Gegenstand gegenstand, int veraendertUmMenge) {
        veraenderungInLegen = true;
    }

    // KOMPLEXE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public final int gettGrenznutzen(int gegId) {
        return gettGrenznutzen(gettDurchId(gegId));
    }

    public final int gettGrenznutzen(Gegenstand geg) {
        if (!contains(geg)) {
            throw new IllegalArgumentException();
        }
        return gettGrenznutzen_or(geg);
    }

    protected abstract int gettGrenznutzen_or(Gegenstand geg);

    public int gettNutzen() {
        if (veraenderungInVerfuegbar) {
            pruefeUndAktualisiereWerte(false);
        }
        return (int) Math.pow(nutzen, nutzenExponent);
    }

    protected void pruefeUndAktualisiereWerte(boolean mitLegen) {
        if (mitLegen) {
            if (!veraenderungInLegen) {
                return;
            }
            berechneWerte(true);
            veraenderungInLegen = false;
        } else {
            if (!veraenderungInVerfuegbar) {
                return;
            }
            berechneWerte(false);
            veraenderungInVerfuegbar = false;
        }
    }

    protected abstract void berechneWerte(boolean mitLegen);

    // PROTECTED GETTER UND SETTER ////////////////////////////////////////////////////////////////

    protected float getNutzenExponent() {
        return nutzenExponent;
    }

    // EINFACHE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public void setNutzenExponent(float nutzenExponent) {
        this.nutzenExponent = nutzenExponent;
    }

    public void setNutzen(int nutzen) {
        this.nutzen = nutzen;
    }
}
