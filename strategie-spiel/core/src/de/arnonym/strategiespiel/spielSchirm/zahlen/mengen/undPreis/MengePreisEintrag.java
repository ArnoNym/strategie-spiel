package de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.undPreis;

public class MengePreisEintrag implements java.io.Serializable {

    //Wenn sich beim einkaufen bis der Kram tatsaechlich abgeholt wird gemerkt werden soll welcher Preis gezahlt wurde

    private int menge;
    private int preis;

    public MengePreisEintrag(int menge, int preis) {
        this.menge = menge;
        this.preis = preis;
    }

    public MengePreisEintrag(MengePreisEintrag mengePreisEintrag) {
        this.menge = mengePreisEintrag.getMenge();
        this.preis = mengePreisEintrag.getPreis();
    }

    public int getMenge() {
        return menge;
    }

    public void setMenge(int menge) {
        this.menge = menge;
    }

    public int getPreis() {
        return preis;
    }

    public void setPreis(int preis) {
        this.preis = preis;
    }
}
