package de.arnonym.strategiespiel.spielSchirm.zahlen.wirtschaftsdaten;

import java.io.Serializable;
import java.util.HashMap;

public abstract class Manager<TU extends TimeUnit<O>, O extends Datapoint> implements Serializable {
    private final HashMap<Long, TU> historischeTimeUnitHashMap = new HashMap<>();

    private long periode = 0;

    private TU aktuelleTimeUnit;

    public Manager(TU aktuelleTimeUnit) {
        this.aktuelleTimeUnit = aktuelleTimeUnit;

        this.historischeTimeUnitHashMap.put(periode, aktuelleTimeUnit);
    }

    public void update(float delta) {
        // TODO Pause einfuegen neueTimeUnit();
    }

    public void neueTimeUnit() {
        periode += 1;
        aktuelleTimeUnit = newTimeUnit();
        historischeTimeUnitHashMap.put(periode, aktuelleTimeUnit);
    }

    public abstract TU newTimeUnit();

    // KOMPLEXE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    protected final O gettDatapoint(long id, long periode_nl) {
        if (periode_nl == -1) {
            return aktuelleTimeUnit.gett(id);
        }
        TU timeUnit = historischeTimeUnitHashMap.get(periode_nl);
        if (timeUnit == null) throw new IllegalArgumentException("Ungueltige Periode: " + periode_nl);
        O o = timeUnit.gett(id);
        return gettDatapoint_or(id, o);
    }

    protected abstract O gettDatapoint_or(long id, O o);

    public int gettSummeEingang(long id, int betrachtePerioden) {
        int summeEingang = 0;
        for (long p = periode; (p > periode - betrachtePerioden && p >= 0); p--) {
            summeEingang += gettDatapoint(id, p).getEingang();
        }
        return summeEingang;
    }

    public int gettSummeAusgang(long id, int betrachtePerioden) {
        int summeAusgang = 0;
        for (long p = periode; (p > periode - betrachtePerioden && p >= 0); p--) {
            summeAusgang += gettDatapoint(id, p).getAusgang();
        }
        return summeAusgang;
    }

    // PROTECTED GETTER UND SETTER ////////////////////////////////////////////////////////////////

    protected TU getAktuelleTimeUnit() {
        return aktuelleTimeUnit;
    }

    protected void setPeriode(long periode) {
        this.periode = periode;
    }

    // EINFACHE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public long getPeriode() {
        return periode;
    }
}
