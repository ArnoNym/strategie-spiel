package de.arnonym.strategiespiel.spielSchirm.zahlen.ids;

import de.arnonym.strategiespiel.spielSchirm.balancing.IdsGebaude;
import de.arnonym.strategiespiel.spielSchirm.balancing.IdsGegenstaende;
import de.arnonym.strategiespiel.spielSchirm.balancing.IdsRessourcen;

import java.util.HashSet;
import java.util.Set;

public class Id implements Comparable<Id> {
    private final int value;
    private static final Set<Id> inUseIds = new HashSet<>(); // todo bei neuladen nicht loeschen

    public Id(int idValue) {
        this.value = idValue;
        if (inUseIds.contains(this)) {
            throw new IllegalArgumentException("int already in use as id: "+idValue);
        }
        inUseIds.add(this);
    }

    public int intValue() {
        return value;
    }

    @Override
    public int compareTo(Id id) {
        return this.value - id.value;
    }

    @Override
    public int hashCode() {
        return Integer.hashCode(value);
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) return false;
        if (!(o instanceof Id)) return false;
        Id other = (Id) o;
        return value == other.value;
    }

    /*public boolean issGegenstand() {
        return IdsGegenstaende.contains(this);
    }

    public boolean issGebaude() {
        return IdsGebaude.contains(this);
    }

    public boolean issRessource() {
        return IdsRessourcen.contains(this);
    }*/
}
