package de.arnonym.strategiespiel.spielSchirm.zahlen.wirtschaftsdaten;

import com.badlogic.gdx.utils.LongMap;

import java.io.Serializable;

public abstract class TimeUnit<O extends Datapoint> implements Serializable {
    private final LongMap<O> map = new LongMap<>();

    public TimeUnit() {

    }

    public O gett(long id) {
        return map.get(id);
    }

    public LongMap<O> getMap() {
        return map;
    }
}
