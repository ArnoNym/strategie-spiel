package de.arnonym.strategiespiel.spielSchirm.zahlen.schaden;

public class MusterSchaden extends Schaden {
    public final boolean proSekunde;

    public MusterSchaden(float value, boolean proSekunde, float kopf_prozent, float lunge_prozent,
                         float haende_prozent, float fuesse_prozent, float sonstiges_prozent) {
        super(value, kopf_prozent, lunge_prozent, haende_prozent, fuesse_prozent,
                sonstiges_prozent);
        this.proSekunde = proSekunde;
    }
}
