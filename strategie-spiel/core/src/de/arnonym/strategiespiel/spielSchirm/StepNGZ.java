package de.arnonym.strategiespiel.spielSchirm;

import com.badlogic.gdx.math.Vector2;

import java.beans.PropertyChangeEvent;

import de.arnonym.strategiespiel.framework.listener.StoryTeller;
import de.arnonym.strategiespiel.framework.stp.Step;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.bewegung.BewegungComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.bewegung.BewegungszielComp;
import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.bewegen.StepGeheZu;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.CollectionUtils;

public abstract class StepNGZ extends Step { // NGZ = Nicht StepGeheZu
    public final BewegungComp aBewegungComp;
    public final BewegungszielComp zBewegungszielComp;
    public final StepGeheZu stepGeheZu;

    private boolean reserviert;

    public StepNGZ(BewegungszielComp zBewegungszielComp, StoryTeller[] zgStoryTeller,
                   BewegungComp aBewegungComp, Vector2 start_nl) {
        super(CollectionUtils.combine(zgStoryTeller, zBewegungszielComp));
        this.aBewegungComp = aBewegungComp; // TODO aBewegungComp nicht in den Story Tellern????!!
        this.zBewegungszielComp = zBewegungszielComp;
        this.stepGeheZu = new StepGeheZu(aBewegungComp.worldMap, aBewegungComp, zBewegungszielComp,
                start_nl);
    }

    @Override
    protected final Enums.EnumStpStatus versuchen_or() {
        Enums.EnumStpStatus stepGeheZuStatus = stepGeheZu.versuchen();

        Enums.EnumStpStatus status = versuchen_or_or(); /* Auch wenn bereits durch einen fehlenden
        Path klar ist, dass der Step nicht durchgefuehrt wird muss dennoch versuchen() gecallt
        werden damit sich bei failVorAnfangen() darauf verlassen werden kann! */

        switch (stepGeheZuStatus) {
            case FAIL:
            case FAIL_BEI_GEHE_ZU:
                return Enums.EnumStpStatus.FAIL_BEI_GEHE_ZU;
        }

        return status;
    }

    protected abstract Enums.EnumStpStatus versuchen_or_or();

    // RESERVIEREN ////////////////////////////////////////////////////////////////////////////////

    /** Kein reservieren() weil es zB in StepTauschen je nach Constructor sehr unterscheidlich
     * stattfindet und zB in StepArbeit noch der Parameter arbeitszeit uebergeben werden muss! */

    protected boolean isReserviert() {
        return reserviert;
    }

    protected void setReserviert(boolean reserviert) {
        this.reserviert = reserviert;
    }

    protected final void reservierenRueckgaengig() {
        if (!reserviert) return;
        reservierenRueckgaengig_or();
        reserviert = false;
    }

    protected abstract void reservierenRueckgaengig_or();

    @Override
    protected final boolean propertyChange_or(PropertyChangeEvent propertyChangeEvent) {
        if (propertyChange_or_or(propertyChangeEvent)) {
            reservierenRueckgaengig();
            return true;
        }
        return false;
    }

    protected boolean propertyChange_or_or(PropertyChangeEvent propertyChangeEvent) {
        return false;
    }
}
