package de.arnonym.strategiespiel.framework.listener;

public interface StoryListener {
    void react(StoryTeller teller, EnumStory story);

    //

    @FunctionalInterface
    public interface NoRetAction extends Action {
        void act();

        @Override
        default void act(Object o) {
            act();
        }
    }

    @FunctionalInterface
    public interface Action {
        void act(Object o);

        default Action compose(Action other) {
            return (Object o) -> { Action.this.act(o); other.act(o); };
        }
    }
}
