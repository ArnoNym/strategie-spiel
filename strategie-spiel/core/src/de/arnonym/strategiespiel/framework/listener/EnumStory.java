package de.arnonym.strategiespiel.framework.listener;

public enum EnumStory {
    DELETE,
    REMOVE,
    ACTIVATE,
    DEACTIVATE
}
