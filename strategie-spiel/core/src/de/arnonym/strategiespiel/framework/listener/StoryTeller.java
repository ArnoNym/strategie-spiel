package de.arnonym.strategiespiel.framework.listener;

public interface StoryTeller {
    void addListener(StoryListener listener);
    void removeListener(StoryListener listener);
}
