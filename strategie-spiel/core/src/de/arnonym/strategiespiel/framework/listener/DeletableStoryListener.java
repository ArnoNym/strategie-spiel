package de.arnonym.strategiespiel.framework.listener;

import de.arnonym.strategiespiel.framework.werkzeuge.interfaces.Deletable;

public interface DeletableStoryListener extends Deletable, StoryListener {
}
