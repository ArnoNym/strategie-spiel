package de.arnonym.strategiespiel.framework.listener;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import de.arnonym.strategiespiel.framework.werkzeuge.interfaces.Deletable;

public class TellerSupport {
    private final Set<StoryListener> listeners = new HashSet<>();

    private final StoryTeller teller;

    public TellerSupport(StoryTeller teller) {
        this.teller = teller;
    }

    public void addListener(StoryListener listener) {
        if (listener instanceof Deletable && ((Deletable) listener).isDeleted()) {
            throw new IllegalArgumentException();
        }
        this.listeners.add(listener);
    }

    public void addListener(Collection<StoryListener> listeners) {
        for (StoryListener listener : listeners) {
            addListener(listener);
        }
    }

    public void removeListener(StoryListener listeners) {
        this.listeners.remove(listeners);
    }

    public void removeAllListener(Collection<StoryListener> listeners) {
        this.listeners.removeAll(listeners);
    }

    public void tell(EnumStory story) {
        List<StoryListener> deletedListeners = new LinkedList<>();
        for (StoryListener listener : listeners.toArray(new StoryListener[]{})) {
            if (listener instanceof Deletable && ((Deletable) listener).isDeleted()) {
                deletedListeners.add(listener);
            } else {
                listener.react(teller, story);
            }
        }
        removeAllListener(deletedListeners);
    }

    // GETTER /////////////////////////////////////////////////////////////////////////////////////

    public List<StoryListener> listeners() {
        return new ArrayList<>(listeners);
    }

    public <C extends Collection<StoryListener>> C listeners(C collection) {
        collection.addAll(listeners);
        return collection;
    }
}
