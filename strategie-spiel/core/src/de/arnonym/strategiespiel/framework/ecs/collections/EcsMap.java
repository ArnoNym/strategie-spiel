package de.arnonym.strategiespiel.framework.ecs.collections;

import de.arnonym.strategiespiel.framework.ecs.component.Component;
import de.arnonym.strategiespiel.framework.listener.DeletableStoryListener;
import de.arnonym.strategiespiel.framework.listener.EnumStory;
import de.arnonym.strategiespiel.framework.listener.StoryTeller;
import de.arnonym.strategiespiel.framework.listener.StoryListener;
import de.arnonym.strategiespiel.framework.werkzeuge.collections.hashMaps.InefficientMap;

public class EcsMap<K extends Component, V> extends InefficientMap<K, V> implements DeletableStoryListener {
    private final DeletableStoryListener parent;
    private boolean deleted = false;

    public EcsMap() {
        this(null);
    }

    public EcsMap(DeletableStoryListener parent) {
        this.parent = parent;
    }

    @Override
    protected void put_or(K k, V v) {
        if (v instanceof StoryListener) {
            throw new IllegalArgumentException();
        }
        k.addListener(this);
    }

    @Override
    protected void remove_or(Object o) {
        ((K) o).removeListener(this);
    }

    // STROY SUPPORT //////////////////////////////////////////////////////////////////////////////

    @Override
    public void react(StoryTeller teller, EnumStory story) {
        if (story.equals(EnumStory.DELETE)) {
            if (teller.equals(parent)) {
                delete();
            } else {
                remove(teller);
            }
        }
    }

    // DELETEABLE /////////////////////////////////////////////////////////////////////////////////

    @Override
    public void delete() {
        deleted = true;
    }

    @Override
    public boolean isDeleted() {
        return deleted;
    }
}
