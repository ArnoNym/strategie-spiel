package de.arnonym.strategiespiel.framework.ecs.collections;

import de.arnonym.strategiespiel.framework.listener.EnumStory;
import de.arnonym.strategiespiel.framework.listener.StoryListener;
import de.arnonym.strategiespiel.framework.werkzeuge.interfaces.Activateable;
import de.arnonym.strategiespiel.framework.listener.StoryTeller;
import de.arnonym.strategiespiel.framework.werkzeuge.interfaces.CollectionContainer;

public abstract class System<C extends StoryTeller> implements StoryListener, Activateable, CollectionContainer<C> {
    private boolean activ = true;

    public System() {
    }

    public final void update(float delta) {
        if (!activ) return;
        update_or(delta);
    }
    protected abstract void update_or(float delta);

    public final boolean connect(C c) {
        if (connect_or(c)) {
            c.addListener(this);
            return true;
        }
        return false;
    }

    public abstract boolean connect_or(C c);

    public final boolean disconnect(C c) {
        if (disconnect_or(c)) {
            c.removeListener(this);
            return true;
        }
        return false;
    }

    public abstract boolean disconnect_or(C c);

    public abstract boolean contains(C c);

    // ACTIVATEABLE ///////////////////////////////////////////////////////////////////////////////

    @Override
    public boolean activate() {
        if (isActiv()) return false;
        activ = true;
        return true;
    }

    @Override
    public boolean deactivate() {
        if (!isActiv()) return false;
        activ = false;
        return true;
    }

    @Override
    public boolean isActiv() {
        return activ;
    }

    // STORY SUPPORT //////////////////////////////////////////////////////////////////////////////

    @Override
    public void react(StoryTeller teller, EnumStory story) {
        if (story.equals(EnumStory.DELETE)) {
            disconnect((C) teller);
        }
    }
}
