package de.arnonym.strategiespiel.framework.ecs.component;

import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.framework.ecs.collections.System;
import de.arnonym.strategiespiel.framework.listener.DeletableStoryListener;
import de.arnonym.strategiespiel.framework.listener.EnumStory;
import de.arnonym.strategiespiel.framework.listener.TellerSupport;
import de.arnonym.strategiespiel.framework.listener.StoryListener;
import de.arnonym.strategiespiel.framework.werkzeuge.interfaces.Activateable;
import de.arnonym.strategiespiel.framework.listener.StoryTeller;
import de.arnonym.strategiespiel.framework.werkzeuge.exceptions.OutOfBoundsException;

public abstract class Component implements Activateable, DeletableStoryListener, StoryTeller {
    private final TellerSupport tellerSupport = new TellerSupport(this);

    private final Entity entity;
    private final System[] systems;

    private boolean activ = false;
    private boolean deleted = false;

    public Component(System[] systems, Entity entity, StoryTeller[] storyTellers) {
        this.systems = systems;
        this.entity = entity;

        entity.addListener(this);
        additionalConstruct(storyTellers);
    }

    /** Wenn in einer Component neue Components erzeugt werden */
    protected void additionalConstruct(StoryTeller... storyTellers) {
        for (StoryTeller s : storyTellers) {
            try {
                s.addListener(this);
            }
            catch (NullPointerException e) {
                for (int i = 0; i < storyTellers.length; i++) {
                    if (storyTellers[i] == null) {
                        throw new IllegalArgumentException("Wert darf nicht null sein! Index: "+i);
                    }
                }
            }
        }
    }

    @Override
    public boolean activate() {
        if (activ) return false;
        activ = true;
        for (System system : systems) {
            try {
                system.connect(this);
            }
            catch (ClassCastException e) {
                throw new OutOfBoundsException(system);
            }
        }
        return true;
    }

    @Override
    public boolean deactivate() {
        if (!activ) return false;
        activ = false;
        for (System system : systems) {
            try {
                system.disconnect(this);
            }
            catch (ClassCastException e) {
                throw new OutOfBoundsException(system);
            }
        }
        return true;
    }

    @Override
    public boolean isActiv() {
        return activ;
    }

    @Override
    public void delete() {
        java.lang.System.out.println("Component ::: delete ::: "+toString());
        //if (1==1) throw new IllegalStateException(toString());
        if (deleted) throw new IllegalStateException(toString());
        deleted = true;
        tellerSupport.tell(EnumStory.DELETE);
    }

    @Override
    public boolean isDeleted() {
        return deleted;
    }

    @Override
    public void addListener(StoryListener listener) {
        tellerSupport.addListener(listener);
    }

    @Override
    public void removeListener(StoryListener listener) {
        tellerSupport.removeListener(listener);
    }

    @Override
    public void react(StoryTeller teller, EnumStory story) {
        if (story.equals(EnumStory.DELETE)) {
            delete();
            return;
        }
        if (teller.equals(entity)) {
            switch (story) {
                case ACTIVATE:
                    activate();
                    break;
                case DEACTIVATE:
                    deactivate();
            }
        }
    }
}