package de.arnonym.strategiespiel.framework.ecs.collections;

import de.arnonym.strategiespiel.framework.ecs.component.Component;

abstract class MultipleSystem<A extends Component, R extends Component> extends System<Component> {
    private final Class<A> aClazz;
    private final Class<R> bClazz;

    public MultipleSystem(Class<A> aClazz, Class<R> bClazz) {
        this.aClazz = aClazz;
        this.bClazz = bClazz;
    }

    @Override
    public boolean connect_or(Component component) {
        if (aClazz.isInstance(component)) {
            return activateAkteur(aClazz.cast(component));
        } else if (bClazz.isInstance(component)) {
            return activateReakteur(bClazz.cast(component));
        } else {
            throw new IllegalArgumentException(component.toString());
        }
    }

    @Override
    public boolean disconnect_or(Component component) {
        if (aClazz.isInstance(component)) {
            return deactivateAkteur(aClazz.cast(component));
        } else if (bClazz.isInstance(component)) {
            return deactivateReakteur(bClazz.cast(component));
        } else {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public boolean contains(Component component) {
        if (aClazz.isInstance(component)) {
            return isActivAkteur(aClazz.cast(component));
        } else if (bClazz.isInstance(component)) {
            return isActivReakteur(bClazz.cast(component));
        } else {
            throw new IllegalArgumentException();
        }
    }

    protected abstract boolean activateAkteur(A aktuer);

    protected abstract boolean deactivateAkteur(A aktuer);

    protected abstract boolean activateReakteur(R reakteur);

    protected abstract boolean deactivateReakteur(R reakteur);

    protected abstract boolean isActivAkteur(A aktuer);

    protected abstract boolean isActivReakteur(R reakteur);
}
