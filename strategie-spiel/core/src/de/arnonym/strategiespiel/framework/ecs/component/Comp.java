package de.arnonym.strategiespiel.framework.ecs.component;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.framework.ecs.collections.Matcher;
import de.arnonym.strategiespiel.framework.ecs.collections.System;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.CollectionUtils;

public abstract class Comp extends Component {
    protected final PropertyChangeSupport support = new PropertyChangeSupport(this);

    public Comp(Entity entity, Comp... comps) {
        this(new System[]{}, entity, comps);
    }

    public Comp(System system, Entity entity, Comp... comps) {
        this(new System[]{system}, entity, comps);
    }

    public Comp(System[] systems, Entity entity, Comp... comps) {
        super(systems, entity, comps);
    }

    // PROPERTY CHANGE ////////////////////////////////////////////////////////////////////////////

    public void addPropertyChangeListener(PropertyChangeListener pcl) {
        support.addPropertyChangeListener(pcl);
    }

    public void removePropertyChangeListener(PropertyChangeListener pcl) {
        support.removePropertyChangeListener(pcl);
    }
}