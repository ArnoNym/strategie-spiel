package de.arnonym.strategiespiel.framework.ecs.collections;

import java.util.Collection;

import de.arnonym.strategiespiel.framework.ecs.collections.System;
import de.arnonym.strategiespiel.framework.listener.StoryTeller;
import de.arnonym.strategiespiel.framework.werkzeuge.collections.UpdateSet;
import de.arnonym.strategiespiel.framework.werkzeuge.functionalInterfaces.Method2;
import de.arnonym.strategiespiel.spielSchirm.balancing.Konstanten;

public class UpdateSetSystem<C extends StoryTeller> extends System<C> {
    private final UpdateSet<C> updateSet;

    public UpdateSetSystem(boolean possibleDeleteInUpdateObject, Method2<C, Float> updateMethod) {
        this.updateSet = new UpdateSet<>(Konstanten.UPDATE_LIST_SIZE, possibleDeleteInUpdateObject,
                updateMethod);
    }

    @Override
    protected void update_or(float delta) {
        updateSet.update(delta);
    }

    @Override
    public boolean connect_or(C c) {
        return updateSet.add(c);
    }

    @Override
    public boolean disconnect_or(C c) {
        return updateSet.remove(c);
    }

    @Override
    public boolean contains(C c) {
        return updateSet.contains(c);
    }

    @Override
    public <COLL extends Collection<C>> COLL toCollection(COLL coll) {
        coll.addAll(updateSet);
        return coll;
    }
}
