package de.arnonym.strategiespiel.framework.ecs.collections;

import java.util.List;

import de.arnonym.strategiespiel.framework.listener.DeletableStoryListener;
import de.arnonym.strategiespiel.framework.listener.EnumStory;
import de.arnonym.strategiespiel.framework.listener.StoryTeller;
import de.arnonym.strategiespiel.spielSchirm.Enums;
import de.arnonym.strategiespiel.framework.werkzeuge.collections.PropertyChangeList;

public class EcsList<O extends StoryTeller> extends PropertyChangeList<O> implements DeletableStoryListener {
    private DeletableStoryListener parent;
    private boolean deleted = false;

    public EcsList() {
        this(null);
    }

    public EcsList(DeletableStoryListener parent) {
        this(parent, Enums.EnumDopplung.ALLOW, Enums.EnumNull.ALLOW);
    }

    public EcsList(DeletableStoryListener parent, Enums.EnumDopplung enumDopplung,
                   Enums.EnumNull enumNull) {
        super(enumDopplung, enumNull);
        this.parent = parent;
    }

    public EcsList(DeletableStoryListener parent, Enums.EnumDopplung enumDopplung,
                   Enums.EnumNull enumNull, List<O> list) {
        super(enumDopplung, enumNull, list);
        this.parent = parent;
    }

    @Override
    public boolean add(O o) {
        o.addListener(this);
        return super.add(o);
    }

    @Override
    public boolean remove(Object o) {
        if (super.contains(o)) {
            ((O) o).removeListener(this);
            return true;
        }
        return false;
    }

    @Override
    public void react(StoryTeller teller, EnumStory story) {
        if (story.equals(EnumStory.DELETE)) {
            if (teller.equals(parent)) {
                delete();
            } else {
                remove(teller);
            }
        }
    }

    @Override
    public void delete() {
        if (deleted) throw new IllegalStateException();
        deleted = true;
    }

    @Override
    public boolean isDeleted() {
        return deleted;
    }
}
