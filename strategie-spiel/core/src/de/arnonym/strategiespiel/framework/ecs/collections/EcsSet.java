package de.arnonym.strategiespiel.framework.ecs.collections;

import java.util.HashSet;
import java.util.Set;

import de.arnonym.strategiespiel.framework.listener.DeletableStoryListener;
import de.arnonym.strategiespiel.framework.listener.EnumStory;
import de.arnonym.strategiespiel.framework.listener.StoryTeller;
import de.arnonym.strategiespiel.framework.werkzeuge.collections.PropertyChangeCollection;
import de.arnonym.strategiespiel.spielSchirm.Enums;

public class EcsSet<O extends StoryTeller> extends PropertyChangeCollection<O, Set<O>> implements Set<O>, DeletableStoryListener {
    private DeletableStoryListener parent;
    private boolean deleted = false;

    public EcsSet() {
        this(null);
    }

    public EcsSet(DeletableStoryListener parent) {
        this(parent, Enums.EnumDopplung.ALLOW, Enums.EnumNull.ALLOW);
    }

    public EcsSet(DeletableStoryListener parent, Enums.EnumDopplung enumDopplung,
                  Enums.EnumNull enumNull) {
        super(enumDopplung, enumNull, new HashSet<>());
        this.parent = parent;
    }

    @Override
    public boolean add(O o) {
        o.addListener(this);
        return super.add(o);
    }

    @Override
    public boolean remove(Object o) {
        if (super.contains(o)) {
            ((O) o).removeListener(this);
            return true;
        }
        return false;
    }

    @Override
    public void react(StoryTeller teller, EnumStory story) {
        if (story.equals(EnumStory.DELETE)) {
            if (teller.equals(parent)) {
                delete();
            } else {
                remove(teller);
            }
        }
    }

    @Override
    public void delete() {
        if (deleted) throw new IllegalStateException();
        deleted = true;
    }

    @Override
    public boolean isDeleted() {
        return deleted;
    }
}
