package de.arnonym.strategiespiel.framework.ecs.entity;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import de.arnonym.strategiespiel.framework.werkzeuge.interfaces.Deletable;
import de.arnonym.strategiespiel.framework.listener.EnumStory;
import de.arnonym.strategiespiel.framework.listener.StoryListener;
import de.arnonym.strategiespiel.framework.listener.TellerSupport;
import de.arnonym.strategiespiel.framework.werkzeuge.interfaces.Activateable;
import de.arnonym.strategiespiel.framework.listener.StoryTeller;
import de.arnonym.strategiespiel.framework.werkzeuge.Manager;
import de.arnonym.strategiespiel.framework.werkzeuge.functionalInterfaces.ValueToBoolean;

public class Entity implements Activateable, StoryTeller, Deletable {
    private final TellerSupport tellerSupport = new TellerSupport(this);
    private final Manager manager;
    public final long id;
    private boolean activate = false;
    private boolean deleted = false;

    public Entity(Manager manager) {
        this.manager = manager;
        this.id = manager.createId();
    }

    // ECS ////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public boolean activate() {
        if (activate) throw new IllegalStateException();
        activate = true;
        tellerSupport.tell(EnumStory.ACTIVATE);
        return true;
    }

    @Override
    public boolean deactivate() {
        if (!activate) throw new IllegalStateException();
        activate = false;
        tellerSupport.tell(EnumStory.DEACTIVATE);
        return true;
    }

    @Override
    public boolean isActiv() {
        return activate;
    }

    public void delete() {
        if (deleted) return;
        deleted = true;
        tellerSupport.tell(EnumStory.DELETE);
        manager.freeId(id);
    }

    @Override
    public boolean isDeleted() {
        return deleted;
    }

    // STORY SUPPORT //////////////////////////////////////////////////////////////////////////////

    @Override
    public void addListener(StoryListener listener) {
        tellerSupport.addListener(listener);
    }

    @Override
    public void removeListener(StoryListener listener) {
        tellerSupport.removeListener(listener);
    }

    // HASH ///////////////////////////////////////////////////////////////////////////////////////

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) return true;
        if (!(o instanceof Entity)) {
            return false;
        }
        Entity entity = (Entity) o;
        return id == entity.id;
    }

    // KOMPLEXE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public <S extends StoryListener> S gettComponent(Class<S> clazz) {
        return gettComponent(clazz::isInstance);
    }

    public <S extends StoryListener, COLL extends Collection<S>> COLL gettComponents(COLL coll, Class<S> clazz) {
        return gettComponents(coll, clazz::isInstance);
    }

    public <S extends StoryListener> S gettComponent(ValueToBoolean<StoryListener> valueToBoolean) {
        List<S> list = gettComponents(new LinkedList<>(), valueToBoolean);
        switch (list.size()) {
            case 0: return null;
            case 1: return list.get(0);
            default: throw new IllegalArgumentException("Ergebnis nicht eindeutig! size: "+list.size());
        }
    }

    public <S extends StoryListener, COLL extends Collection<S>> COLL gettComponents(COLL coll, ValueToBoolean<StoryListener> valueToBoolean) {
        for (StoryListener s : tellerSupport.listeners()) {
            if (valueToBoolean.to(s)) {
                coll.add((S) s);
            }
        }
        return coll;
    }

    // EINFACHE GETTER UND SETTER /////////////////////////////////////////////////////////////////
}
