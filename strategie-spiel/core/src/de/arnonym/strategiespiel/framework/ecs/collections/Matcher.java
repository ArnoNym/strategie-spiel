package de.arnonym.strategiespiel.framework.ecs.collections;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import de.arnonym.strategiespiel.framework.ecs.component.Component;
import de.arnonym.strategiespiel.spielSchirm.Enums;
import de.arnonym.strategiespiel.framework.werkzeuge.pause.DeltaPause;

public abstract class Matcher<A extends Component, R extends Component> extends MultipleSystem<A, R> {
    private final Set<A> akteurSet = new HashSet<>();
    private final Set<R> reaktuerSet = new HashSet<>();
    private final DeltaPause pause;

    public Matcher(Class<A> aClazz, Class<R> bClazz, float pauseDauer) {
        super(aClazz, bClazz);
        this.pause = new DeltaPause(pauseDauer, Enums.EnumPauseStart.JETZT);
    }

    @Override
    protected void update_or(float delta) {
        if (!pause.issVorbei(delta, true)) {
            return;
        }

        if (reaktuerSet.isEmpty()) {
            return;
        }

        for (A akteur : akteurSet) {
            R reakteur = findBestMatch(akteur, reaktuerSet);
            if (reakteur != null) {
                couple(akteur, reakteur);
                break;
            }
        }
    }

    protected abstract R findBestMatch(A a, Set<R> rSet);

    protected abstract void couple(A a, R r);

    // VERWALTE VERBINDUNGEN //////////////////////////////////////////////////////////////////////

    @Override
    protected final boolean activateAkteur(A aktuer) {
        return akteurSet.add(aktuer);
    }

    @Override
    protected final boolean deactivateAkteur(A aktuer) {
        if (akteurSet.remove(aktuer)) {
            decoupleAkteur(aktuer);
            return true;
        }
        return false;
    }

    protected abstract void decoupleAkteur(A a);

    @Override
    protected final boolean activateReakteur(R reakteur) {
        return reaktuerSet.add(reakteur);
    }

    @Override
    protected final boolean deactivateReakteur(R reakteur) {
        if (reaktuerSet.remove(reakteur)) {
            decoupleReakteur(reakteur);
            return true;
        }
        return false;
    }

    protected abstract void decoupleReakteur(R r);

    @Override
    protected final boolean isActivAkteur(A aktuer) {
        return akteurSet.contains(aktuer);
    }

    @Override
    protected final boolean isActivReakteur(R reakteur) {
        return reaktuerSet.contains(reakteur);
    }

    // PROTECTED GETTER UND SETTER ////////////////////////////////////////////////////////////////

    /*protected final List<A> getAkteurSet() {
        return Collections.unmodifiableList(akteurSet);
    }

    protected final List<R> getReaktuerSet() {
        return Collections.unmodifiableList(reaktuerSet);
    }

    @Override
    public final int size() {
        return akteurSet.size() + reaktuerSet.size();
    }*/

    @Override
    public <COLL extends Collection<Component>> COLL toCollection(COLL coll) {
        coll.addAll(reaktuerSet);
        return coll;
    }

    // EINFACHE GETTER UND SETTER /////////////////////////////////////////////////////////////////
}
