package de.arnonym.strategiespiel.framework.ecs.entity;

import com.badlogic.gdx.math.Vector2;

import de.arnonym.strategiespiel.spielSchirm.World;

public abstract class Factory {
    public final World world;

    public Factory(World world) {
        this.world = world;
    }

    public abstract Entity create(Vector2 position);
}
