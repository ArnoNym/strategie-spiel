package de.arnonym.strategiespiel.framework.ecs.collections;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import de.arnonym.strategiespiel.framework.listener.StoryTeller;

import de.arnonym.strategiespiel.framework.ecs.component.Comp;

public abstract class SetSystem<C extends StoryTeller> extends System<C> {
    protected final Set<C> compSet = new HashSet<>();

    protected SetSystem() {

    }

    @Override
    public boolean connect_or(C c) {
        return compSet.add(c);
    }

    @Override
    public boolean disconnect_or(C c) {
        return compSet.remove(c);
    }

    @Override
    public boolean contains(C c) {
        return compSet.contains(c);
    }

    @Override
    public <COLL extends Collection<C>> COLL toCollection(COLL coll) {
        coll.addAll(compSet);
        return coll;
    }

    /*@Override
    public int size() {
        return compSet.size();
    }

    @Override
    public List<C> toList() {
        return Collections.unmodifiableList(compSet);
    }*/
}
