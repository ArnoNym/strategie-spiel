package de.arnonym.strategiespiel.framework.werkzeuge.textures;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

import de.arnonym.strategiespiel.spielSchirm.Assets;
import de.arnonym.strategiespiel.spielSchirm.Enums;

public class IdTextureRegion extends IdTexture {

    private transient TextureRegion textureRegion;

    public IdTextureRegion(TextureRegion textureRegion) {
        this(textureRegion, false, false, true, -1, new Vector2(-textureRegion.getRegionWidth() / 2,
                -textureRegion.getRegionHeight() / 2));
    }

    public IdTextureRegion(TextureRegion textureRegion, boolean primaerTextur,
                           float standardDauer, Vector2 renderMod_code) {
        this(textureRegion, false, false, primaerTextur, standardDauer, renderMod_code);
    }

    public IdTextureRegion(TextureRegion textureRegion, boolean flipHorizontaly,
                           boolean flipVerticaly, boolean primaerTextur,
                           float standardDauer, Vector2 renderMod_code) {
        super(Assets.instance.getId(textureRegion), flipHorizontaly, flipVerticaly,
                primaerTextur ? Enums.EnumAddTexture.ERSETZE_WENN_UNGLEICH : Enums.EnumAddTexture.ZUSATZ,
                standardDauer, renderMod_code);
        this.textureRegion = textureRegion;
    }

    @Override
    public TextureRegion get() {
        if (textureRegion == null) {
            textureRegion = Assets.instance.getTextureRegion(getId());
        }
        return textureRegion;
    }

    @Override
    protected float gettRightWidth(float leftWidth) {
        get(); /* Wird in parent Constructor gecallt also kommt es nicht dazu, dass wuerde die
        Textureregion nicht initialisiert bevor diese Methode gecallt wird */
        return textureRegion.getRegionWidth() - leftWidth;
    }

    @Override
    protected float gettTopHeight(float bottomHeight) {
        get();
        return textureRegion.getRegionHeight() - bottomHeight;
    }
}
