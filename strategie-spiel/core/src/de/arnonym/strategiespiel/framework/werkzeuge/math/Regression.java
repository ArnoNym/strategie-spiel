package de.arnonym.strategiespiel.framework.werkzeuge.math;

import java.util.LinkedList;
import java.util.List;

public class Regression {
    /**
     * Following these Tutorial: https://stackoverflow.com/questions/24747643/3d-linear-regression
     * E = X^T / (X^T * X) * y
     */

    private final int dimension;
    private final List<float[]> x = new LinkedList<>();
    private final List<Float> y = new LinkedList<>();
    private float[] result = null;

    public Regression(int dimension) {
        this.dimension = dimension + 1;
    }

    public void addData(float[] data) {
        if (data.length != dimension - 1) {
            throw new IllegalArgumentException("Array.length = "+data.length+"! Should be "
                    +(dimension-1));
        }
        result = null;
        x.add(data);
    }

    /*private void calc() {
        float[][] x = this.x.toArray(new float[][]{});
        float[][] xTransed = trans();

    }

    public static float[][] mutl(float[][] m1, float[][] m2) {
        int i = 0;
        int j = 0;




        float[][] transedX = new float[dimension][x.size()];
        int i = 0;
        for (float[] data : x) {
            int j = 0;
            for (float d : data) {
                transedX[i][j] = d;
                j++;
            }
            i++;
        }
        return transedX;
    }*/

    public static float[][] trans(float[][] matrix) {
        float[][] transedMatrix = new float[matrix[0].length][matrix.length];
        int i = 0;
        for (float[] data : matrix) {
            int j = 0;
            for (float d : data) {
                transedMatrix[i][j] = d;
                j++;
            }
            i++;
        }
        return transedMatrix;
    }
}
