package de.arnonym.strategiespiel.framework.werkzeuge.collections;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import de.arnonym.strategiespiel.framework.werkzeuge.collections.hashMaps.InefficientMap;
import de.arnonym.strategiespiel.framework.werkzeuge.functionalInterfaces.Method;
import de.arnonym.strategiespiel.framework.werkzeuge.functionalInterfaces.Method2;
import de.arnonym.strategiespiel.spielSchirm.balancing.Konstanten;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.MyTimeUtils;
import de.arnonym.strategiespiel.framework.werkzeuge.interfaces.CollectionContainer;

public class UpdateSet<O> implements Set<O>, CollectionContainer<O> {
    private final int miniListenSize;
    private final boolean possibleDeleteInUpdateObject;

    private InefficientMap<O, List<O>> identMap = new InefficientMap<>();
    private InefficientMap<List<O>, Long> masterMap = new InefficientMap<>();

    private List<O> addSoonList = new ArrayList<>();
    private List<O> removeSoonList = new ArrayList<>();

    private final Method2<O, Float> updateMethod;

    // todo ermoeglichen bestimtme Anzahl pro sekunden oder prozent von denen auf dem Spielfeld mit einem maximum zu updaten

    public UpdateSet(int miniListenSize, boolean possibleDeleteInUpdateObject,
                     Method2<O, Float> updateMethod) {
        this.miniListenSize = miniListenSize;
        this.possibleDeleteInUpdateObject = possibleDeleteInUpdateObject;
        this.updateMethod = updateMethod;
    }

    public void update(final float delta) {
        // Additions
        startAdditions();

        // Finde upzudatende Liste
        List<O> maxLongList = null;
        long maxLong = - Long.MAX_VALUE;
        for (Map.Entry<List<O>, Long> entry : masterMap.entrySet()) {
            entry.setValue(entry.getValue() + (long) (delta / MyTimeUtils.miliToSec));
            long l = entry.getValue();
            if (l > maxLong) {
                maxLong = l;
                maxLongList = entry.getKey();
                //IdentityConnectable.out.println("UpdateList ::: liste == "+liste+", maxLong == "+maxLong);
            }
        }
        if (maxLongList == null) return;

        /* Remove ::: vor updateObject() */
        if (startRemoval(maxLongList)) {
            return;
        }

        // Update Objects
        for (O o : maxLongList) {
            updateMethod.act(o, maxLong * MyTimeUtils.miliToSec);
        }

        /* Remove ::: nach updateObject(), da wenn das Object in updateObject() der remove Liste
        hinzugefuegt wird es in den meisten faellen am effizientesten sein sollte es direkt zu
        entfernen */
        if (possibleDeleteInUpdateObject && startRemoval(maxLongList)) {
            return;
        }

        // Setzte Long
        masterMap.put(maxLongList, 0L, true);
    }

    private void startAdditions() {
        if (addSoonList.isEmpty()) {
            return;
        }
        while (true) {
            List<O> list = null;
            for (Map.Entry<List<O>, Long> entry : masterMap.entrySet()) {
                List<O> tryList = entry.getKey();
                if (tryList.size() < miniListenSize) {
                    list = tryList;
                    break;
                }
            }

            if (list == null) {
                list = new ArrayList<>(miniListenSize);
                masterMap.put(list, 0L, false);
            }

            for (int i = list.size(); i < Konstanten.UPDATE_LIST_SIZE; i++) {
                O o = addSoonList.remove(addSoonList.size() - 1);
                list.add(o); /* TODO Hier besteht ein Fehler: Objekte die der Liste hinzugefuegt
                werden sind juenger als die anderen Objekte in der Liste. Dies zu heben ist aber zu
                umstaendlich und hat einen zu geringen Einfluss */
                identMap.put(o, list, false);

                if (addSoonList.isEmpty()) {
                    return;
                }
            }
        }
    }

    private boolean startRemoval(List<O> list) {
        List<O> removeNowList = new ArrayList<>(miniListenSize);
        for (O o : list) {
            if (removeSoonList.contains(o)) {
                removeNowList.add(o);
            }
        }

        list.removeAll(removeNowList);
        removeSoonList.removeAll(removeNowList);
        for (O o : removeNowList) {
            identMap.remove(o);
        }

        if (list.isEmpty()) {
            masterMap.remove(list);
            return true;
        }
        return false;
    }

    @Override
    public int size() {
        int s = addSoonList.size();
        for (List<O> l : masterMap.keySet()) {
            s += l.size();
        }
        return s;
    }

    @Override
    public boolean isEmpty() {
        return masterMap.isEmpty() && addSoonList.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        if (addSoonList.contains(o)) {
            return true;
        }
        try {
            return identMap.containsKey((O) o);
        } catch (ClassCastException e) {
            return false;
        }
    }

    @Override
    public boolean add(O o) {
        if (contains(o)) {
            return false;
        }
        addSoonList.add(o);
        return true;
    }

    @Override
    public boolean remove(Object o) {
        if (addSoonList.remove(o)) {
            return true;
        }
        try {
            if (identMap.containsKey((O) o)) {
                return removeSoonList.add((O) o);
            }
            return false;
        } catch (ClassCastException e) {
            return false;
        }
    }

    @Override
    public Iterator<O> iterator() {
        return new UpdateSetIterator();
    }
    private class UpdateSetIterator implements Iterator<O> {
        private final Iterator<O> iterator = toLinkedList().iterator();
        @Override
        public boolean hasNext() {
            return iterator.hasNext();
        }
        @Override
        public O next() {
            return iterator.next();
        }
    }

    @Override
    public O[] toArray() {
        throw new IllegalStateException();
    }

    @Override
    public <T> T[] toArray(T[] ts) {
        throw new IllegalStateException();
    }

    private List<O> toLinkedList() {
        LinkedList<O> linkedList = new LinkedList<>(addSoonList);
        for (List<O> l : masterMap.keySet()) {
            linkedList.addAll(l);
        }
        return linkedList;
    }

    @Override
    public <COLL extends Collection<O>> COLL toCollection(COLL coll) {
        coll.addAll(addSoonList);
        for (List<O> l : masterMap.keySet()) {
            coll.addAll(l);
        }
        return coll;
    }

    @Override
    public boolean containsAll(Collection<?> collection) {
        if (collection.size() == 0) {
            return false;
        }
        boolean[] contains = new boolean[collection.size()];
        for (O o : toLinkedList()) {
            int i = 0;
            for (Object co : collection) {
                if (o.equals(co)) {
                    contains[i] = true;
                }
            }
        }
        for (boolean b : contains) {
            if (!b) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends O> collection) {
        boolean ret = false;
        for (O o : collection) {
            ret = ret | add(o);
        }
        return ret;
    }

    @Override
    public boolean retainAll(Collection<?> collection) {
        throw new IllegalStateException();
    }

    @Override
    public boolean removeAll(Collection<?> collection) {
        boolean ret = false;
        for (Object o : collection) {
            ret = ret | remove(o);
        }
        return ret;
    }

    @Override
    public void clear() {
        identMap = new InefficientMap<>();
        masterMap = new InefficientMap<>();
        addSoonList = new ArrayList<>();
        removeSoonList = new ArrayList<>();
    }

    @Override
    public String toString() {
        return "gesamtSize: " +size()
                +", identMap.size: "+identMap.size()
                +" sollte gleich sein mit masterMap.size: " +masterMap.size()
                +" ; addSoonList.size = " +addSoonList.size()
                +", removeSoonList.size: "+removeSoonList.size();
    }
}
