package de.arnonym.strategiespiel.framework.werkzeuge.numbers.unit;

import de.arnonym.strategiespiel.framework.werkzeuge.numbers.Operation;
import de.arnonym.strategiespiel.framework.werkzeuge.numbers.pin.Pin;

public abstract class UnitOperation<E extends UnitInterface<E>> extends Operation<Unit<E>, Number> {
    public final double equalOperator1;
    public final double equalOperator2;
    public final E unitEnum;

    public UnitOperation(Unit<E> rawOperator1, Number rawOperator2) {
        this(rawOperator1, rawOperator2, rawOperator2 instanceof Unit ?
                rawOperator1.enumE.finest(rawOperator1, (Unit<E>) rawOperator2) :
                rawOperator1.enumE);
    }

    public UnitOperation(Unit<E> rawOperator1, Unit<E> rawOperator2) {
        this(rawOperator1, rawOperator2, rawOperator1.enumE.finest(rawOperator1, rawOperator2));
    }

    public UnitOperation(Unit<E> rawOperator1, Number rawOperator2, E e) {
        super(rawOperator1, rawOperator2);
        this.unitEnum = e;

        if (rawOperator2 instanceof Unit) {
            equalOperator1 = rawOperator1.doubleValue(e);
            equalOperator2 = ((Unit<E>) rawOperator2).doubleValue(e);
        } else {
            equalOperator1 = rawOperator1.value;
            equalOperator2 = rawOperator2.doubleValue();
        }
    }

    @Override
    public final int intValue() {
        return (int) doubleValue();
    }

    @Override
    public final long longValue() {
        return (long) doubleValue();
    }

    @Override
    public final float floatValue() {
        return (float) doubleValue();
    }

    @Override
    public final Pin pinValue() {
        return new Pin(doubleValue(), Pin.STANDARD_DIGITS);
    }

    @Override
    public final Pin pinValue(int digits) {
        return new Pin(doubleValue(), digits);
    }

    public final Unit<E> unitValue() {
        return new Unit<>(doubleValue(), unitEnum);
    }

    @Override
    public Unit<E> add(Number number) {
        return (new Add<E>(unitValue(), number, unitEnum)).unitValue();
    }

    @Override
    public Unit<E> sub(Number number) {
        return (new Sub<E>(unitValue(), number, unitEnum)).unitValue();
    }

    @Override
    public Unit<E> mult(Number number) {
        return (new Mult<E>(unitValue(), number, unitEnum)).unitValue();
    }

    @Override
    public Unit<E> div(Number number) {
        return (new Div<E>(unitValue(), number, unitEnum)).unitValue();
    }
}
