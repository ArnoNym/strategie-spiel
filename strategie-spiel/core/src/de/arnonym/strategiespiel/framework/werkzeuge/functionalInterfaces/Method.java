package de.arnonym.strategiespiel.framework.werkzeuge.functionalInterfaces;

@FunctionalInterface
public interface Method<O> {
    void act(O o);
}
