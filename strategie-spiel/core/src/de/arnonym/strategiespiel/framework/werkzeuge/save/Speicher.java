package de.arnonym.strategiespiel.framework.werkzeuge.save;


import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

import de.arnonym.strategiespiel.spielSchirm.World;

public class Speicher {

    public static void main(World world) {
        /*level.name = "Reyan Ali";
        level.address = "Phokka Kuan, Ambehta Peer";
        level.SSN = 11122333;
        level.number = 101;*/

        try {
            FileOutputStream fileOut =
                    new FileOutputStream("employee.ser"); // /tmp/employee.ser
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(world);
            out.close();
            fileOut.close();
            System.out.printf("Serialized data is saved in /tmp/employee.ser");
        } catch (IOException i) {
            i.printStackTrace();
        }
    }
}
