package de.arnonym.strategiespiel.framework.werkzeuge.save;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

import de.arnonym.strategiespiel.spielSchirm.World;

public class Laden {

    public static World main() {
        World world = null;
        try {
            FileInputStream fileIn = new FileInputStream("employee.ser");
            ObjectInputStream in = new ObjectInputStream(fileIn);
            world = (World) in.readObject();
            in.close();
            fileIn.close();
        } catch (IOException i) {
            i.printStackTrace();
            return null;
        } catch (ClassNotFoundException c) {
            System.out.println("Level class not found");
            c.printStackTrace();
            return null;
        }

        System.out.println("Level wurde geladen!");
        return world;
    }
}
