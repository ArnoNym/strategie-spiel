package de.arnonym.strategiespiel.framework.werkzeuge.collections.groups;

public class TwoGroup<V1, V2> {
    public final V1 v1;
    public final V2 v2;

    public TwoGroup(V1 v1, V2 v2) {
        this.v1 = v1;
        this.v2 = v2;
    }
}
