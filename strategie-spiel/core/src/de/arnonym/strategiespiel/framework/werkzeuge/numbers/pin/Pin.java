package de.arnonym.strategiespiel.framework.werkzeuge.numbers.pin;

import java.util.Objects;
import de.arnonym.strategiespiel.framework.werkzeuge.numbers.MyNumber;
import de.arnonym.strategiespiel.spielSchirm.balancing.Konstanten;

public class Pin extends MyNumber<Pin> {
    protected final long value;
    public final int digits;
    public static int STANDARD_DIGITS = Konstanten.NACHKOMMASTELLEN;

    public Pin(long value) {
        this(value, STANDARD_DIGITS);
    }

    public Pin(long value, int digits) {
        if (digits < 0) {
            throw new IllegalArgumentException();
        }
        this.value = value;
        this.digits = digits;
    }

    public Pin(double value) {
        this(value, STANDARD_DIGITS);
    }

    public Pin(double value, int digits) {
        if (digits < 0) {
            throw new IllegalArgumentException();
        }
        this.value = (long) (value / Math.pow(10, digits));
        this.digits = digits;
    }

    @Override
    public int intValue() {
        return (int) longValue();
    }

    @Override
    public long longValue() {
        return (long) (value / Math.pow(10, digits));
    }

    @Override
    public float floatValue() {
        return (float) doubleValue();
    }

    @Override
    public double doubleValue() {
        return value / Math.pow(10, digits);
    }

    @Override
    public Pin pinValue() {
        return pinValue(digits);
    }

    @Override
    public Pin pinValue(int digits) {
        int diff = this.digits - digits;
        if (diff == 0) {
            return this;
        }
        long v;
        long f = (long) Math.pow(10, diff);
        if (diff > 0) {
            v = value / f;
        } else {
            v = value * f;
        }
        return new Pin(v, digits);
    }

    @Override
    public String toString() {
        return super.toString() + ", value: "+value + ", digits: " + digits + ", double: "
                + doubleValue();
    }

    @Override
    public int hashCode() {
        return Objects.hash(value, digits);
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) return false;
        if (!(o instanceof Pin)) return false;
        Pin on = (Pin) o;
        return value == on.value && digits == on.digits;
    }

    public boolean hasDigits() {
        return digits > 0;
    }

    public long compareToZero() {
        return value;
    }

    // RECHENOPERATOREN /////////////7777//////////////////////////////////////////////////////////

    @Override
    public Pin add(Number number) {
        return new Add(this, number, digits).pinValue();
    }

    @Override
    public Pin sub(Number number) {
        return new Sub(this, number, digits).pinValue();
    }

    @Override
    public Pin mult(Number number) {
        return new Mult(this, number, digits).pinValue();
    }

    @Override
    public Pin div(Number number) {
        return new Div(this, number, digits).pinValue();
    }
}