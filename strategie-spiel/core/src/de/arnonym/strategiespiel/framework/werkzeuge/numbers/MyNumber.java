package de.arnonym.strategiespiel.framework.werkzeuge.numbers;

import de.arnonym.strategiespiel.framework.werkzeuge.numbers.pin.Pin;

public abstract class MyNumber<RET extends Number> extends Number implements Comparable<Number> {
    @Override
    public final int compareTo(Number number) {
        return Double.compare(doubleValue(), number.doubleValue());
    }

    public abstract Pin pinValue();

    public abstract Pin pinValue(int digits);

    // RECHENOPERATOREN ///////////////////////////////////////////////////////////////////////////

    public abstract RET add(Number number);

    public abstract RET sub(Number number);

    public abstract RET mult(Number number);

    public abstract RET div(Number number);
}
