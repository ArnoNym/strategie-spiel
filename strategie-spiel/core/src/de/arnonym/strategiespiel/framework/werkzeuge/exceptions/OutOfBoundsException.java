package de.arnonym.strategiespiel.framework.werkzeuge.exceptions;

public class OutOfBoundsException extends RuntimeException {
    public OutOfBoundsException(Object o) {
        throw new IllegalStateException("Not within Bounds: "+o.toString());
    }
}
