package de.arnonym.strategiespiel.framework.werkzeuge.collections.id;

import java.util.ArrayList;
import java.util.Collection;

public class IdList<O extends IdObjekt> extends ArrayList<O> {
    public IdList() {
    }

    @Override @Deprecated
    public O get(int i) {
        return super.get(i);
    }

    public O gettDurchIndex(int index) {
        return super.get(index);
    }

    public O gettDurchId(int id) {
        for (O o : this) {
            if (o.gettId() == id) return o;
        }
        throw new IllegalArgumentException("id: "+id+", assignmentObjecs.size: "+size());
    }

    // CONTAINS ///////////////////////////////////////////////////////////////////////////////////

    public boolean contains(O o) {
        return contains(o.gettId());
    }

    public boolean contains(int id) {
        for (O o : this) {
            if (o.gettId() == id) return true;
        }
        return false;
    }

    // ADD ////////////////////////////////////////////////////////////////////////////////////////

    public boolean add(O o, boolean overrideSameId) {
        int id = o.gettId();
        if (!overrideSameId && contains(id)) {
            throw new IllegalArgumentException("id: "+id);
        }
        return super.add(o);
    }

    public final boolean addAll(Collection<? extends O> collection, boolean overrideSameId) {
        for (O o : collection) {
            add(o, overrideSameId);
        }
        return true;
    }

    @Override
    public final boolean add(O o) {
        return add(o, false);
    }

    @Override
    public final void add(int i, O o) {
        add(o, false);
    }

    @Override
    public final boolean addAll(Collection<? extends O> collection) {
        return addAll(collection, false);
    }

    @Override
    public final boolean addAll(int i, Collection<? extends O> collection) {
        return addAll(collection, false);
    }
}