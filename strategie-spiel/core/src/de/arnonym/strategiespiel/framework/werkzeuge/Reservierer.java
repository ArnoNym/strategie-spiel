package de.arnonym.strategiespiel.framework.werkzeuge;

public class Reservierer {
    private int maxReservierungen;
    private int reservierungen = 0;

    public Reservierer(int maxReservierungen) {
        this.maxReservierungen = maxReservierungen;
    }

    public void reservieren() {
        reservierungen++;
        if (reservierungen > maxReservierungen) throw new IllegalStateException();
    }

    public void freigeben() {
        reservierungen--;
        if (reservierungen < 0) throw new IllegalStateException();
    }

    // KOMPLEXE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public boolean issFrei() {
        return maxReservierungen > reservierungen;
    }

    public int gettFrei() {
        return maxReservierungen - reservierungen;
    }

    // EINFACHE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public void setMaxReservierungen(int maxReservierungen) {
        this.maxReservierungen = maxReservierungen;
    }

    public int getReservierungen() {
        return reservierungen;
    }
}
