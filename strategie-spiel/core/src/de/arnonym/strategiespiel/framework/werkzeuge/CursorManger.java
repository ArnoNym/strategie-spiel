package de.arnonym.strategiespiel.framework.werkzeuge;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Cursor;
import com.badlogic.gdx.graphics.Pixmap;

import de.arnonym.strategiespiel.spielSchirm.Enums;

public class CursorManger {
    private final Cursor pfeil;
    private final Cursor unsichtbar;
    private boolean sichtbar; //Weil ich den Cursor nicht von Gdx bekommen kann

    public CursorManger() {
        pfeil = Gdx.graphics.newCursor(new Pixmap(Gdx.files.internal("mauszeigerA1.png")), 0, 0);
        unsichtbar = Gdx.graphics.newCursor(new Pixmap(Gdx.files.internal("mauszeigerA2.png")), 0, 0);
        sett(Enums.EnumCursor.PFEIL);
    }

    public void sett(Enums.EnumCursor enumCursor) {
        Cursor cursor;
        switch (enumCursor) {
            case PFEIL: cursor = pfeil;
            break;
            default: cursor = unsichtbar;
        }
        sichtbar = cursor != unsichtbar;
        Gdx.graphics.setCursor(cursor);
    }

    public boolean isSichtbar() {
        return sichtbar;
    }
}
