package de.arnonym.strategiespiel.framework.werkzeuge.utils;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.bewegung.BewegungComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.EinkommenComp;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.geld.GeldComp;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.gegenstaende.endstufe.Gegenstand;
import de.arnonym.strategiespiel.spielSchirm.zahlen.mengen.Geld;

public class SpecialUtils {

    public static int gettKostenLaufen(BewegungComp bewegungComp, int salary,
                                       Vector2 start, Vector2 ziel) {
        float hypothenuse = GeometryUtils.hypothenuseIsoStrecke(start, ziel);
        return (int) (hypothenuse / bewegungComp.bewegungsgeschwindigkeit * salary);
    }

    public static void leerzeile(Table table) {
        table.row();
        table.add(new Label(" ", table.getSkin())).row();
    }

    public static void changeGeld(Geld oldGeld, Geld newGeld, GeldComp nehmenVonGeldComp,
                                  GeldComp legenInGeldComp, int geldMenge) {
        if (oldGeld.equals(nehmenVonGeldComp.getGeld())) {
            oldGeld.reservierenNehmenRueckgaengig(geldMenge);
            newGeld.reservierenNehmen(geldMenge);
        } else if (oldGeld.equals(legenInGeldComp.getGeld())) {
            oldGeld.reservierenLegenRueckgaengig(geldMenge);
            newGeld.reservierenLegen(geldMenge);
        } else {
            throw new IllegalStateException();
        }
    }

    /*public static Tasche passendeTasche(Dorfbewohner dorfbewohner, int id) {
        return passendeTasche(dorfbewohner, IdsGegenstaende.toList(id));
    }
    public static Tasche passendeTasche(Dorfbewohner dorfbewohner, Gegenstand gegenstand) {
        if (gegenstand instanceof GegenstandNahrung) {
            return dorfbewohner.getNahrungsTasche();
        } else if (gegenstand instanceof GegenstandWerkzeug) {
            return dorfbewohner.getWerkzeugTasche_nl();
        } else if (gegenstand instanceof GegenstandMedizin) {
            return dorfbewohner.getMedizinTasche_nl();
        } else {
            return dorfbewohner.getSpielzeugTasche_nl();
        }
    }

    public static List<Step> gettSteps(List<Familie> familienListe, Class<? extends Step> bestimmteKlasse_NL) {
        List<Step> stepList = new ArrayList<>();
        for (Familie f : familienListe) {
            for (Dorfbewohner d : f.getFamilienmitglieder()) {
                for (Task t : d.getStackFiniteStepMachine().getTaskStack()) {
                    for (Step s : t.getStepStack().iterate()) {
                        if (bestimmteKlasse_NL == null || bestimmteKlasse_NL.isInstance(s)) {
                            stepList.connectTo(s);
                        }
                    }
                }
            }
        }
        return stepList;
    }*/
    public static void reserviere(Gegenstand nehmenAusGeg, Gegenstand legenInGeg, int menge) {
        nehmenAusGeg.reservierenNehmen(menge);
        legenInGeg.reservierenLegen(menge);
    }
    public static void reserviere(Geld nehmenAusGeld, Geld legenInGeld, int menge, int preis) {
        int mengeMalPreis = MathUtils.myIntMult(menge, preis);
        nehmenAusGeld.reservierenNehmen(mengeMalPreis);
        legenInGeld.reservierenLegen(mengeMalPreis);
    }

    public static float oIfNull(Float value_NL) {
        if (value_NL == null) {
            return 0;
        }
        return value_NL;
    }

    public static float streckeKosten(Vector2 position1, Vector2 position2, float dbBewegungsgeschwindigkeit, float dbEinkommen) {
        if (position1.equals(position2)) {
            return 0;
        }

        float ankathete = position2.x - position1.x;
        float gegenkathete = (position2.y - position1.y)*2;
        float hypothenuse = GeometryUtils.hypothenuse(ankathete, gegenkathete);

        return hypothenuse / dbBewegungsgeschwindigkeit * dbEinkommen;

        /*Vector2 tatsaechlicheBewegungsgeschwindigkeit = new Vector2();
        boolean xRichtungPositiv = ankathete >= 0;

        tatsaechlicheBewegungsgeschwindigkeit.x = (float) (Math.cos(Math.atan(gegenkathete / ankathete))
                * (xRichtungPositiv ? dbBewegungsgeschwindigkeit : (-dbBewegungsgeschwindigkeit)));
        tatsaechlicheBewegungsgeschwindigkeit.y = (float) (Math.sin(Math.atan(gegenkathete / ankathete))
                * (xRichtungPositiv ? dbBewegungsgeschwindigkeit : (-dbBewegungsgeschwindigkeit)));

        return tatsaechlicheBewegungsgeschwindigkeit;*/
    }
}