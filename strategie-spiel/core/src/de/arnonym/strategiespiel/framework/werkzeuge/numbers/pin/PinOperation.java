package de.arnonym.strategiespiel.framework.werkzeuge.numbers.pin;

import de.arnonym.strategiespiel.framework.werkzeuge.numbers.Operation;

public abstract class PinOperation extends Operation<Pin, Number> {
    public final Pin equalOperator1;
    public final Pin equalOperator2;

    public PinOperation(Pin operator1, Number operator2) {
        this(operator1, operator2,
                operator2 instanceof Pin
                        ? Math.max(operator1.digits, ((Pin)operator2).digits)
                        : operator1.digits);
    }

    public PinOperation(Pin operator1, Number operator2, int digits) {
        super(operator1, operator2);

        Pin operatorPin2;
        if (operator2 instanceof Pin) {
            operatorPin2 = (Pin) operator2;
            int d1 = operator1.digits, d2 = operatorPin2.digits;
            long v1, v2;
            if (d1 == d2) {
                v1 = operator1.value;
                v2 = operatorPin2.value;
            } else if (d1 > d2) {
                v1 = operator1.value;
                int diff = d1 - d2;
                v2 = operatorPin2.value * (long) Math.pow(10, diff);
            } else {
                v2 = operatorPin2.value;
                int diff = d2 - d1;
                v1 = operator1.value * (long) Math.pow(10, diff);
            }
            equalOperator1 = new Pin(v1, digits);
            equalOperator2 = new Pin(v2, digits);
        } else {
            operatorPin2 = new Pin(operator2.doubleValue(), operator1.digits);
            equalOperator1 = operator1;
            equalOperator2 = operatorPin2;
        }
    }

    @Override
    public int intValue() {
        return pinValue().intValue(); //todo mehr effizienz wenn in einigen Unterklasse aus double?
    }

    @Override
    public long longValue() {
        return pinValue().longValue(); //todo mehr effizienz wenn in einigen Unterklasse aus double?
    }

    @Override
    public float floatValue() {
        return pinValue().floatValue(); //todo mehr effizienz wenn in einigen Unterklasse aus double?
    }

    @Override
    public double doubleValue() {
        return pinValue().doubleValue();
    }

    public abstract Pin pinValue();

    public Pin pinValue(int digits) { //todo mehr praeizision fuer Rechnung anstatt nachtraeglich nullen hinzuzufuegen
        if (digits < 0 || digits > Long.SIZE - 1) {
            throw new IllegalArgumentException("digits: "+digits);
        }
        Pin n = pinValue();
        if (n.digits == digits) {
            return n;
        }
        if (n.digits > digits) {
            int diff = n.digits - digits;
            return new Pin(n.value / (long) Math.pow(10, diff), digits);
        }
        int diff = n.digits - digits;
        return new Pin(n.value * (long) Math.pow(10, diff), digits);
    }

    public int resultDigits() {
        return equalOperator1.digits;
    }

    @Override
    public Pin add(Number number) {
        return (new Add(pinValue(), number)).pinValue();
    }

    @Override
    public Pin sub(Number number) {
        return (new Sub(pinValue(), number)).pinValue();
    }

    @Override
    public Pin mult(Number number) {
        return (new Mult(pinValue(), number)).pinValue();
    }

    @Override
    public Pin div(Number number) {
        return (new Div(pinValue(), number)).pinValue();
    }
}
