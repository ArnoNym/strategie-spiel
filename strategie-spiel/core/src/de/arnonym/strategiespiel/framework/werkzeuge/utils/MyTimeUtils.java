package de.arnonym.strategiespiel.framework.werkzeuge.utils;

import com.badlogic.gdx.math.MathUtils;

import de.arnonym.strategiespiel.spielSchirm.Enums;
import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenBalance;

public class MyTimeUtils {
    static public final float miliToSec = 1 / 1000f;

    public static float secondsSince(long timeNanos) {
        return MathUtils.nanoToSec * (com.badlogic.gdx.utils.TimeUtils.nanoTime() - timeNanos);
    }

    public static float perSecToPerIngameYear(float value, Enums.EnumValue enumValue) {
        switch (enumValue) {
            case NUMBER: return value / KonstantenBalance.SEKUNDEN_PRO_JAHR;
            case PERCENT: return (float) Math.pow(1 + value, KonstantenBalance.SEKUNDEN_PRO_JAHR) - 1;
            case MULT: return (float) Math.pow(value, KonstantenBalance.SEKUNDEN_PRO_JAHR);
        }
        throw new IllegalStateException("Methode an neues Enum anpassen!");
    }

    public static float perIngameYearToPerSec(float value, Enums.EnumValue enumValue) {
        switch (enumValue) {
            case NUMBER: return value * KonstantenBalance.SEKUNDEN_PRO_JAHR;
            case PERCENT: return (float) Math.pow(1 + value, 1 / KonstantenBalance.SEKUNDEN_PRO_JAHR) - 1;
            case MULT: return (float) Math.pow(value, 1 / KonstantenBalance.SEKUNDEN_PRO_JAHR);
        }
        throw new IllegalStateException("Methode an neues Enum anpassen!");
    }
}
