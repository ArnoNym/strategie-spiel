package de.arnonym.strategiespiel.framework.werkzeuge.interfaces;

import java.util.Collection;

public interface CollectionContainer<O> {
    <COLL extends Collection<O>> COLL toCollection(COLL coll);
}
