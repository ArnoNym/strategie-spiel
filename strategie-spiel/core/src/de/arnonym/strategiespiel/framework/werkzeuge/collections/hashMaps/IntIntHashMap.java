package de.arnonym.strategiespiel.framework.werkzeuge.collections.hashMaps;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by LeonPB on 23.08.2018.
 */

public class IntIntHashMap extends HashMap<Integer, Integer> implements java.io.Serializable {
    //Kann verwendet werden um zB alle noetigen Inputs um ein Output zu erzeugen aufzulisten

    public IntIntHashMap() {
        super();
    }
    public IntIntHashMap(IntIntHashMap intIntHashMap) {
        super();
        for (Entry<Integer, Integer> entry : intIntHashMap.entrySet()) {
            this.put(entry.getKey(), entry.getValue());
        }
    }
    public IntIntHashMap(int[][] intintArray) {
        super();
        for (int[] intArray : intintArray) {
            this.put(intArray[0], intArray[1]);
        }
    }

    public IntIntHashMap kopieren() {
        return new IntIntHashMap(this);
    }

    public float summeAllerValues() {
        float summeAllerValues = 0;
        for (int i : this.keySet()) {
            summeAllerValues += this.get(i);
        }
        return summeAllerValues;
    }

    public void addUpEntrys(IntIntHashMap intIntHashMap) {
        for (Entry<Integer, Integer> entry : intIntHashMap.entrySet()) {
            int key = entry.getKey();
            int value = entry.getValue();

            addUpEntry(key, value);
        }
    }
    public void addUpEntry(int key, int value) {
        if (this.containsKey(key)) {
            int oldValue = this.get(key);
            this.put(key, oldValue + value);
        } else {
            this.put(key, value);
        }
    }

    public void removeValueIsSmallerOrZero() {
        List<Integer> removeList = new ArrayList<Integer>();
        for (int key : this.keySet()) {
            if (this.get(key) <= 0) {
                removeList.add(key);
            }
        }
        for (int key : removeList) {
            this.remove(key);
        }
    }

    public int getNext() {
        for (Integer key : this.keySet()) {
            return key;
        }
        return -1;
    }

    public void valueSumToOne() {
        int oldValueSum = 0;
        for (Integer f : values()) {
            oldValueSum += f;
        }

        for (Map.Entry<Integer, Integer> entry : entrySet()) {
            int value = entry.getValue() / oldValueSum;
            put(entry.getKey(), value);
        }
    }
}
