package de.arnonym.strategiespiel.framework.werkzeuge.functionalInterfaces;

@FunctionalInterface
public interface Method2<O1, O2> {
    void act(O1 o1, O2 o2);
}
