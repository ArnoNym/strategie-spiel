package de.arnonym.strategiespiel.framework.werkzeuge.collections.groups;

import de.arnonym.strategiespiel.framework.ecs.component.Component;
import de.arnonym.strategiespiel.framework.werkzeuge.interfaces.Deletable;
import de.arnonym.strategiespiel.framework.listener.DeletableStoryListener;
import de.arnonym.strategiespiel.framework.listener.EnumStory;
import de.arnonym.strategiespiel.framework.listener.StoryListener;
import de.arnonym.strategiespiel.framework.listener.StoryTeller;
import de.arnonym.strategiespiel.framework.listener.TellerSupport;

public class EcsTwoGroup<C extends Component, V2> extends TwoGroup<C, V2> implements StoryTeller, DeletableStoryListener {
    /** Loescht sich selbst wenn eine der Values geloscht wird. Ermoeglicht es dadurch zB Listen zu
     *  erstellen in denen eine Componente mit einem weiteren Wert gespeichert wird und die dennoch
     *  immer aktuell ist, da sich alle Objekte selbst loeschen */

    private final TellerSupport tellerSupport = new TellerSupport(this);
    private boolean deleted = false;

    public EcsTwoGroup(C c, V2 v2) {
        super(c, v2);

        c.addListener(this);
        addListener(c);

        if (v2 instanceof StoryTeller) {
            ((StoryTeller) v2).addListener(this);
        }
        if (v2 instanceof StoryListener) {
            addListener((StoryListener) v2);
        }
    }

    @Override
    public void delete() {
        if (deleted) return;
        deleted = true;
        v1.delete();
        if (v2 instanceof Deletable) {
            ((Deletable) v2).delete();
        }
    }

    @Override
    public boolean isDeleted() {
        return deleted;
    }

    @Override
    public void react(StoryTeller teller, EnumStory story) {
        if (story.equals(EnumStory.DELETE)) {
            if (teller.equals(v1) || teller.equals(v2)) {
                delete();
            }
        }
    }

    public boolean contains(Object object) {
        return (v1 == null && object == null) || (v1 != null && v1.equals(object))
                || (v2 == null && object == null) || (v2 != null && v2.equals(object));
    }

    @Override
    public void addListener(StoryListener listener) {
        tellerSupport.addListener(listener);
    }

    @Override
    public void removeListener(StoryListener listener) {
        tellerSupport.addListener(listener);
    }
}
