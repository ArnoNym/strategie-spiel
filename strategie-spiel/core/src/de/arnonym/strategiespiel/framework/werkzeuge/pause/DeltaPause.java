package de.arnonym.strategiespiel.framework.werkzeuge.pause;

import java.io.Serializable;
import java.util.concurrent.ThreadLocalRandom;

import de.arnonym.strategiespiel.spielSchirm.Enums;

/**
 * Created by LeonPB on 08.04.2018.
 */

public class DeltaPause extends Pause implements Serializable {
    private float restDauer;
    private boolean vorbei;

    public DeltaPause(float dauer, Enums.EnumPauseStart enumPauseStart) {
        super(dauer, enumPauseStart);
    }

    // KOMPLEXE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    @Override
    public void settStartzeitJetzt() {
        restDauer = getDauer();
    }

    @Override
    public void settStartzeitZufaelligeRestdauer() {
        restDauer = getDauer() * ThreadLocalRandom.current().nextFloat();
    }

    @Override
    public float gettZeitSeitStartzeit() {
        return getDauer() - restDauer;
    }

    @Override
    public void settVorbei() {
        restDauer = 0;
    }

    public boolean issVorbei(float delta, boolean neuePause) {
        restDauer -= delta;
        if (restDauer <= 0) {
            if (neuePause) {
                restDauer = getDauer() + restDauer;
            }
            return vorbei = true;
        }
        return vorbei = false;
    }

    // EINFACHE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    @Override
    public boolean issVorbei() {
        return vorbei;
    }
}
