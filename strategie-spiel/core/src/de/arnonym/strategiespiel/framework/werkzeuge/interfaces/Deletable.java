package de.arnonym.strategiespiel.framework.werkzeuge.interfaces;

public interface Deletable {
    void delete();
    boolean isDeleted();
}
