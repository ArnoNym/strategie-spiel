package de.arnonym.strategiespiel.framework.werkzeuge.textures;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenOptik;
import de.arnonym.strategiespiel.spielSchirm.Enums;

public abstract class Texture {
    public final boolean flipHorizontaly;
    public final boolean flipVerticaly;
    public final Enums.EnumAddTexture enumAddTexture;
    public final float standardDauer_code; /* -1 fuehrt in CompRender dazu, dass die Region
    dauerhaft gezeigt wird (Wenn in CompRender die StandardDauer angefordert wird) */
    public final Vector2 renderMod_code; /* Um wieviel die Position verschoben werden muss, damit
    die TextureRegion an der Richtigen Stelle gezeichnet wird
    code:
        - 0 < X <= unendlich : Fundament Groesse in dessen Mitte sich die Position befindet
        - -unendlich <= X <= 0 : Distanz zwischen Position und der zu rendernden Position
        (unten Links von der Textur) */

    // Breite der Textur um die Position herum
    public final float leftWidth;
    public final float rightWidth;
    public final float topHeight;
    public final float bottomHeight;

    public Texture(boolean flipHorizontaly, boolean flipVerticaly,
                   Enums.EnumAddTexture enumAddTexture, float standardDauer_code,
                   Vector2 renderMod_code) {
        this.flipHorizontaly = flipHorizontaly;
        this.flipVerticaly = flipVerticaly;
        this.enumAddTexture = enumAddTexture;
        this.standardDauer_code = standardDauer_code;
        this.renderMod_code = renderMod_code;

        if (standardDauer_code <= 0 && standardDauer_code != -1) {
            throw new IllegalArgumentException("'Code' nicht beachtet!");
        }

        if (renderMod_code.x > 0 && renderMod_code.y <= 0
                || renderMod_code.x <= 0 && renderMod_code.y > 0) {
            throw new IllegalArgumentException("'Code' nicht beachtet!");
        }

        // erklaerung-002
        float x = renderMod_code.x, y = renderMod_code.y;
        if (x > 0 && y > 0) {
            float halbeGroesse = ((x + y) / 2) / 2; // erklaerung-002
            leftWidth = rightWidth = halbeGroesse * KonstantenOptik.TILE_ISO_WIDTH;
            bottomHeight = halbeGroesse * KonstantenOptik.TILE_ISO_HEIGHT;
            topHeight = gettTopHeight(bottomHeight);
        } else {
            leftWidth = - renderMod_code.x;
            rightWidth = gettRightWidth(leftWidth);
            bottomHeight = - renderMod_code.y;
            topHeight = gettTopHeight(bottomHeight);
        }
    }

    public void start() {

    }

    public abstract TextureRegion get();

    protected abstract float gettRightWidth(float leftWidth);

    protected abstract float gettTopHeight(float bottomHeight);
}
