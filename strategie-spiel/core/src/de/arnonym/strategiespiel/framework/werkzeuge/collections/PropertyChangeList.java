package de.arnonym.strategiespiel.framework.werkzeuge.collections;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.ListIterator;

import de.arnonym.strategiespiel.framework.ecs.component.Component;
import de.arnonym.strategiespiel.framework.ecs.collections.EcsList;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.spielSchirm.worldMap.Zelle;
import de.arnonym.strategiespiel.spielSchirm.Enums;

public class PropertyChangeList<O> extends PropertyChangeCollection<O, List<O>> implements List<O> {
    /**
     * Zur Verallgemeinerung von Listen.
     *
     * Zum Beispiel bei der Verallgemeinerung in {@link RessourcenDepartmentComp} vom Ernten,
     * Pflegen, Abbauen (was alles mit {@link Component}, also der {@link EcsList} funktioniert)
     * UND Anpflanzen (was mit {@link Zelle} fuktioniert, welche {@link Entity} sind, also nicht
     * in die {@link EcsList} passen.
     */

    public PropertyChangeList() {
        this(Enums.EnumDopplung.ALLOW, Enums.EnumNull.ALLOW);
    }
    public PropertyChangeList(Enums.EnumDopplung enumDopplung) {
        this(enumDopplung, Enums.EnumNull.ALLOW);
    }
    public PropertyChangeList(Enums.EnumNull enumNull) {
        this(Enums.EnumDopplung.ALLOW, enumNull);
    }
    public PropertyChangeList(Enums.EnumDopplung enumDopplung, Enums.EnumNull enumNull) {
        this(enumDopplung, enumNull, new ArrayList<>());
    }
    public PropertyChangeList(Enums.EnumDopplung enumDopplung, List<O> list) {
        super(enumDopplung, list);
    }
    public PropertyChangeList(Enums.EnumNull enumNull, List<O> list) {
        super(enumNull, list);
    }
    public PropertyChangeList(Enums.EnumDopplung enumDopplung, Enums.EnumNull enumNull, List<O> list) {
        super(enumDopplung, enumNull, list);
    }

    @Override
    public void add(int i, O o) {
        if (!check(o)) {
            return;
        }
        coll.add(i, o);
        fire_add(o);
    }

    @Override
    public boolean addAll(int i, Collection<? extends O> collection) {
        int s = size();
        for (O o : collection) {
            add(i, o);
            i++;
        }
        return size() > s;
    }

    @Override
    public O remove(int i) {
        O o = get(i);
        remove(o);
        return o;
    }

    @Override
    public O get(int i) {
        return coll.get(i);
    }

    @Override
    public O set(int i, O o) {
        O ret = remove(i);
        add(i, o);
        return ret;
    }

    @Override
    public int indexOf(Object o) {
        return coll.indexOf(o);
    }

    @Override
    public int lastIndexOf(Object o) {
        return coll.lastIndexOf(o);
    }

    @Override
    public ListIterator<O> listIterator() {
        return coll.listIterator();
    }

    @Override
    public ListIterator<O> listIterator(int i) {
        return coll.listIterator(i);
    }

    @Override
    public List<O> subList(int i, int i1) {
        return coll.subList(i, i1);
    }

    public List<O> toList() {
        return new ArrayList<>(coll);
    }
}
