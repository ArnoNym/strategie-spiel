package de.arnonym.strategiespiel.framework.werkzeuge.interfaces;

public interface Activateable {
    boolean activate();
    boolean deactivate();
    boolean isActiv();
}
