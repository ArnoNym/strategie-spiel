package de.arnonym.strategiespiel.framework.werkzeuge;

@FunctionalInterface
public interface Paket<INHALT> {
    INHALT entpacken();
}
