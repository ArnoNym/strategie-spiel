package de.arnonym.strategiespiel.framework.werkzeuge.collections;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import de.arnonym.strategiespiel.framework.ecs.component.Component;
import de.arnonym.strategiespiel.framework.ecs.collections.EcsList;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.spielSchirm.worldMap.Zelle;
import de.arnonym.strategiespiel.spielSchirm.Enums;

public class PropertyChangeCollection<O, COLL extends Collection<O>> implements Collection<O> {
    /**
     * Zur Verallgemeinerung von Listen.
     * <p>
     * Zum Beispiel bei der Verallgemeinerung in {@link RessourcenDepartmentComp} vom Ernten,
     * Pflegen, Abbauen (was alles mit {@link Component}, also der {@link EcsList} funktioniert)
     * UND Anpflanzen (was mit {@link Zelle} fuktioniert, welche {@link Entity} sind, also nicht
     * in die {@link EcsList} passen.
     */

    private final PropertyChangeSupport support = new PropertyChangeSupport(this);
    protected final COLL coll;
    private Enums.EnumDopplung enumDopplung;
    private Enums.EnumNull enumNull;

    // KONSTURKTOREN /////////////////////////////////////////////////////////////////////////////

    public PropertyChangeCollection(Enums.EnumDopplung enumDopplung, COLL coll) {
        this(enumDopplung, Enums.EnumNull.ALLOW, coll);
    }

    public PropertyChangeCollection(Enums.EnumNull enumNull, COLL coll) {
        this(Enums.EnumDopplung.ALLOW, enumNull, coll);
    }

    public PropertyChangeCollection(Enums.EnumDopplung enumDopplung, Enums.EnumNull enumNull, COLL coll) {
        this.enumDopplung = enumDopplung;
        this.enumNull = enumNull;
        this.coll = coll;
    }

    // PROPERTY CHANGE ////////////////////////////////////////////////////////////////////////////

    public void addPropertyChangeListener(PropertyChangeListener pcl) {
        support.addPropertyChangeListener(pcl);
    }

    public void removePropertyChangeListener(PropertyChangeListener pcl) {
        support.removePropertyChangeListener(pcl);
    }

    public void fire_changeTo_empty() {
        // Override
    }

    public void fire_changeTo_notEmpty() {
        // Override
    }

    // IMMITIERE NORMALE LISTE ////////////////////////////////////////////////////////////////////

    @Override
    public boolean add(O o) {
        if (!check(o)) {
            return false;
        }
        if (!coll.add(o)) {
            throw new IllegalStateException();
        }
        fire_add(o);
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends O> collection) {
        boolean changed = false;
        for (O o : collection) {
            changed = changed | add(o);
        }
        return changed;
    }

    public boolean addAll(O[] array) {
        return Collections.addAll(this, array);
    }

    boolean check(O o) {
        if (o == null) {
            switch (enumNull) {
                case IGNORE:
                    return false;
                case EXCEPTION:
                    throw new IllegalArgumentException();
            }
        }
        if (enumDopplung != Enums.EnumDopplung.ALLOW && contains(o)) {
            switch (enumDopplung) {
                case IGNORE:
                    return false;
                case EXCEPTION:
                    throw new IllegalArgumentException();
            }
        }
        return true;
    }

    void fire_add(O o) {
        support.firePropertyChange(Enums.PC_EVENT_ADD, null, o);
        if (size() == 1) {
            fire_changeTo_notEmpty();
        }
    }

    @Override
    public boolean remove(Object o) {
        if (!coll.remove(o)) {
            return false;
        }
        support.firePropertyChange(Enums.PC_EVENT_REMOVE, o, null);
        if (isEmpty()) {
            fire_changeTo_empty();
        }
        return true;
    }

    @Override
    public boolean removeAll(Collection<?> collection) {
        boolean changed = false;
        for (Object o : collection) {
            changed = changed | remove(o);
        }
        return changed;
    }

    @Override
    public int size() {
        return coll.size();
    }

    @Override
    public boolean isEmpty() {
        return coll.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return coll.contains(o);
    }

    @Override
    public Iterator<O> iterator() {
        return coll.iterator();
    }

    @Override
    public O[] toArray() {
        return (O[]) coll.toArray();
    }

    @Override
    public <T> T[] toArray(T[] ts) {
        return coll.toArray(ts);
    }

    @Override
    public boolean containsAll(Collection<?> collection) {
        return coll.containsAll(collection);
    }

    @Override
    public boolean retainAll(Collection<?> collection) {
        throw new IllegalStateException();
    }

    @Override
    public void clear() {
        removeAll(this);
    }

    public List<O> toList() {
        return new ArrayList<>(coll);
    }

    // EINFACHE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public Enums.EnumDopplung getEnumDopplung() {
        return enumDopplung;
    }

    public void setEnumDopplung(Enums.EnumDopplung enumDopplung) {
        this.enumDopplung = enumDopplung;
    }

    public Enums.EnumNull getEnumNull() {
        return enumNull;
    }

    public void setEnumNull(Enums.EnumNull enumNull) {
        this.enumNull = enumNull;
    }
}
