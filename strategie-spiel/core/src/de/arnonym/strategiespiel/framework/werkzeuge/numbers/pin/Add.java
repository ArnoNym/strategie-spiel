package de.arnonym.strategiespiel.framework.werkzeuge.numbers.pin;

public class Add extends PinOperation {

    public Add(Pin operator1, Number operator2) {
        super(operator1, operator2);
    }

    public Add(Pin operator1, Number operator2, int digits) {
        super(operator1, operator2, digits);
    }

    @Override
    public Pin pinValue() {
        return new Pin(equalOperator1.value + equalOperator2.value, resultDigits());
    }
}
