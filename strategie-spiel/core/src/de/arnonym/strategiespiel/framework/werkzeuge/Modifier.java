package de.arnonym.strategiespiel.framework.werkzeuge;

@FunctionalInterface
public interface Modifier<O> {
    void mod(O o);
    default O modRet(O o) {
        mod(o);
        return o;
    }
}
