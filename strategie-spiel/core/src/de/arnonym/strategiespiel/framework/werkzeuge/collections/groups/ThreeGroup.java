package de.arnonym.strategiespiel.framework.werkzeuge.collections.groups;

public class ThreeGroup<V1, V2, V3> extends TwoGroup<V1, V2> {
    public final V3 v3;

    public ThreeGroup(V1 v1, V2 v2, V3 v3) {
        super(v1, v2);
        this.v3 = v3;
    }
}
