package de.arnonym.strategiespiel.framework.werkzeuge;

public class Feedback {
    private String text;

    public Feedback(String orientierungInfos) {
        text = "Feedback "+orientierungInfos;
    }

    public void addText(String addText) {
        text += "\n- "+addText;
    }

    public String getText() {
        return text;
    }
}
