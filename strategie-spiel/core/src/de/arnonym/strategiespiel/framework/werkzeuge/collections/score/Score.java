package de.arnonym.strategiespiel.framework.werkzeuge.collections.score;

public class Score {
    private float value;

    public Score(float value) {
        this.value = value;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }
}
