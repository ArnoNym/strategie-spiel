package de.arnonym.strategiespiel.framework.werkzeuge.collections.hashMaps;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class InefficientMap<K, V> implements Map<K, V> {
    /**
     * Sinn dieser Klasse: Komlexe Sachen wie Listen koennen normalerweise nicht als key verwendet
     * werden, da sobald sie modifiziert werden, sich ihr HashWert aender. Diese List soll auf den
     * HashWert verzichten, wodurch sie natuerlich ineffizienter wird!
     */

    private final List<K> keys = new LinkedList<>();
    private final List<V> values = new ArrayList<>();

    @Override
    public int size() {
        return keys.size();
    }

    @Override
    public boolean isEmpty() {
        return keys.isEmpty();
    }

    @Override
    public boolean containsKey(Object o) {
        return keys.contains(o);
    }

    @Override
    public boolean containsValue(Object o) {
        return values.contains(o);
    }

    @Override
    public V get(Object o) {
        int i = getKeysIndex(o);
        if (i == -1) {
            throw new IllegalArgumentException(o.toString());
        }
        return values.get(i);
    }

    protected int getKeysIndex(Object o) {
        int i = 0;
        Iterator it = keys.iterator();
        while (it.hasNext()) {
            if (it.next().equals(o)) {
                return i;
            }
            i++;
        }
        return -1;
    }

    @Override
    public V put(K k, V v) {
        return put(k, v, !isEmpty());
    }

    public V put(K k, V v, boolean searchForDuplicate) {
        put_or(k, v);

        if (searchForDuplicate) {
            int i = getKeysIndex(k);
            if (i != -1) {
                V retV = values.remove(i);
                values.add(i, v);
                return retV;
            }
        }
        keys.add(k);
        values.add(v);

        return null;
    }

    protected void put_or(K k, V v) {

    }

    @Override
    public void putAll(Map<? extends K, ? extends V> map) {
        putAll(map, true);
    }

    public void putAll(Map<? extends K, ? extends V> map, boolean searchForDuplicates) {
        searchForDuplicates = searchForDuplicates && !isEmpty();
        for (Entry<? extends K, ? extends V> entry : map.entrySet()) {
            put(entry.getKey(), entry.getValue(), searchForDuplicates);
        }
    }

    @Override
    public V remove(Object o) {
        int i = getKeysIndex(o);
        if (i == -1) {
            return null;
        } else {
            remove_or(o);
            keys.remove(i);
            return values.remove(i);
        }
    }

    protected void remove_or(Object o) {

    }

    @Override
    public void clear() {
        keys.clear();
        values.clear();
    }

    @Override
    public Set<K> keySet() {
        return new HashSet<>(keys);
    }

    @Override
    public Collection<V> values() {
        return new ArrayList<>(values);
    }

    @Override
    public Set<Entry<K, V>> entrySet() {
        Set<Entry<K, V>> entrySet = new HashSet<>();
        Iterator<K> keysIt = keys.iterator();
        Iterator<V> valuesIt = values.iterator();
        while (keysIt.hasNext()) {
            entrySet.add(new InefficientMapEntry(keysIt.next(), valuesIt.next()));
        }
        return entrySet;
    }

    private class InefficientMapEntry implements Entry<K, V> {
        private final K key;
        private V value;

        private InefficientMapEntry(K key, V value) {
            this.key = key;
            this.value = value;
        }

        @Override
        public K getKey() {
            return key;
        }

        @Override
        public V getValue() {
            return value;
        }

        @Override
        public V setValue(V v) {
            V oldValue = value;

            value = v;
            put(key, v, true);

            return oldValue;
        }
    }
}