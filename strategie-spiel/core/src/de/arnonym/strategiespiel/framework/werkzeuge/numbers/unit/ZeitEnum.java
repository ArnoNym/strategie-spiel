package de.arnonym.strategiespiel.framework.werkzeuge.numbers.unit;

import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenBalance;

public enum ZeitEnum implements UnitInterface<ZeitEnum> {
    REAL_SEKUNDEN(0, -1),
    INGAME_MONAT(1, (double) KonstantenBalance.SEKUNDEN_PRO_MONAT),
    INGAME_JAHR(2, 12d);

    public int index;
    public double toNextStepMult;

    ZeitEnum(int index, double toNextStepMult) {
        this.index = index;
        this.toNextStepMult = toNextStepMult;
    }

    @Override
    public int index() {
        return index;
    }

    @Override
    public double toNextStepMult() {
        return toNextStepMult;
    }

    @Override
    public ZeitEnum[] getUnitEnums() {
        return ZeitEnum.values();
    }
}
