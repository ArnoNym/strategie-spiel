package de.arnonym.strategiespiel.framework.werkzeuge;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;

public class MyClickListener extends InputListener {
    private boolean klicked = false;

    @Override
    public final boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
        klicked = true;
        return touchDown_or(event, x, y, pointer, button);
    }

    protected boolean touchDown_or(InputEvent event, float x, float y, int pointer, int button) {
        return false;
    }

    @Override
    public final void exit(InputEvent event, float x, float y, int pointer, Actor toActor) {
        if (klicked) return;
        exit_or(event, x, y, pointer, toActor);
    }

    protected void exit_or(InputEvent event, float x, float y, int pointer, Actor toActor) {

    }

    @Override
    public final boolean mouseMoved(InputEvent event, float x, float y) {
        klicked = false;
        return mouseMoved_or(event, x, y);
    }

    protected boolean mouseMoved_or(InputEvent event, float x, float y) {
        return false;
    }
}
