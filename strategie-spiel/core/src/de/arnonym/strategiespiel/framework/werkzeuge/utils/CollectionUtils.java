package de.arnonym.strategiespiel.framework.werkzeuge.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import de.arnonym.strategiespiel.framework.ecs.collections.Matcher;
import de.arnonym.strategiespiel.framework.ecs.collections.System;
import de.arnonym.strategiespiel.framework.listener.StoryTeller;
import de.arnonym.strategiespiel.framework.ecs.component.Comp;
import de.arnonym.strategiespiel.framework.werkzeuge.collections.hashMaps.IntIntHashMap;
import de.arnonym.strategiespiel.spielSchirm.Enums;

public class CollectionUtils {

    public static <V> V[] combine(V[] vs1, V[] vs2, V[] into, Enums.EnumNull enumNull) {
        if (vs1.length + vs2.length > into.length) {
            throw new IllegalArgumentException();
        }
        int i = 0;
        for (V v : vs1) {
            if (check(v, enumNull)) {
                into[i] = v;
                i++;
            }
        }
        for (V v : vs2) {
            if (check(v, enumNull)) {
                into[i] = v;
                i++;
            }
        }
        return into;
    }

    private static <V> boolean check(V v, Enums.EnumNull enumNull) {
        if (v != null) {
            return true;
        }
        switch (enumNull) {
            case EXCEPTION: throw new IllegalStateException();
            case IGNORE: return false;
            case ALLOW: return true;
        }
        throw new IllegalStateException();
    }

    public static StoryTeller[] combine(StoryTeller[] vs1, StoryTeller... vs2) {
        return combine(vs1, vs2, new StoryTeller[vs1.length + vs2.length], Enums.EnumNull.IGNORE);
    }

    public static <O, C extends Collection<O>> C combine(C coll1, C coll2) {
        coll1.addAll(coll2);
        return coll1;
    }

    public static <O, C extends Collection<? super O>> C combine(C coll, O o) {
        coll.add(o);
        return coll;
    }

    public static Matcher[] combineMatcher(Matcher[] matchers1, Matcher... matchers2) {
        List<Matcher> list = new ArrayList<>(Arrays.asList(matchers1));
        list.addAll(Arrays.asList(matchers2));
        return list.toArray(new Matcher[list.size()]);
    }

    public static System[] combine(System[] systems1, System... systems2) {
        List<System> list = new ArrayList<>(Arrays.asList(systems1));
        list.addAll(Arrays.asList(systems2));
        return list.toArray(new System[list.size()]);
    }

    public static Comp[] combineComp(Comp[] components1, Comp... components2) {
        List<Comp> list = new ArrayList<>(Arrays.asList(components1));
        list.addAll(Arrays.asList(components2));
        return list.toArray(new Comp[list.size()]);
    }

    public static IntIntHashMap addiereIntegerHashMaps(IntIntHashMap ihm1, IntIntHashMap ihm2) {
        IntIntHashMap ihm = new IntIntHashMap(ihm1);
        for (Integer key : ihm2.keySet()) {
            if (ihm.get(key) == null) {
                ihm.put(key, ihm2.get(key));
            } else {
                ihm.put(key, ihm.get(key) + ihm2.get(key));
            }
        }
        return ihm;
    }

    public static boolean vergleicheIntegerHashMaps(IntIntHashMap sollIHM, IntIntHashMap pruefenIHM) {
        for (Integer key : sollIHM.keySet()) {
            if (sollIHM.get(key) > pruefenIHM.get(key)) return false;
        }
        return true;
    }

    /*public static List<StatischesObjekt> gebListeZuKollListe(List<Gebaude> gebaudeListe) {
        List<StatischesObjekt> returnListe = new ArrayList();
        returnListe.handle(gebaudeListe);
        return returnListe;
    }*/
    public static boolean arrayEnthaelt(int[] valueArray, int value) {
        for (int i = 0; i<valueArray.length; i++) {
            if (valueArray[i] == value) {
                return true;
            }
        }
        return false;
    }

    /*public static boolean enthaeltKollisionsobjektlisteKlasse(List<StatischesObjekt> statischesObjektListe, Class<?> klasse, int wievielteSuperklasse) {
        if (wievielteSuperklasse > 0) {
            return enthaeltKollisionsobjektlisteKlasse(statischesObjektListe, klasse.getSuperclass(), wievielteSuperklasse - 1);
        }
        for (int s=0; s<=wievielteSuperklasse; s++) {

        }
        for (StatischesObjekt k : statischesObjektListe) {
            if (k.getClass() == klasse) {
                return true;
            }
        }
        return false;
    }

    public static Ressource resscourceEntsprechenderKlasseAusListe(List<Ressource> ressourcenListe, Ressource ressource) {
        for (Ressource r : ressourcenListe) {
            if (r.getClass() == ressource.getClass()) {
                return r;
            }
        }
        return null;
    }*/
}
