package de.arnonym.strategiespiel.framework.werkzeuge.numbers.unit;

public interface UnitInterface<E extends UnitInterface<E>> {
    int index();
    double toNextStepMult();
    E[] getUnitEnums();

    default E finest(Unit<E> u1, Unit<E> u2) {
        if (u1 == null || u2 == null) {
            throw new IllegalArgumentException();
        }
        if (u1.enumE.index() >= u2.enumE.index()) {
            return u1.enumE;
        } else {
            return u2.enumE;
        }
    }
}
