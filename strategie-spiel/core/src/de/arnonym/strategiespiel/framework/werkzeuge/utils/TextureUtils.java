package de.arnonym.strategiespiel.framework.werkzeuge.utils;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

import de.arnonym.strategiespiel.spielSchirm.ui.hud.buttons.icons.Icon;

public class TextureUtils {

    public static Vector2 center(float x, float y, float centerObjektWidth,
                                 float centerObjektHeight, float rahmenWidth, float rahmenHeight) {
        float returnX = x + rahmenWidth / 2 - centerObjektWidth / 2;
        float returnY = y + rahmenHeight / 2 - centerObjektHeight / 2;
        return new Vector2(returnX, returnY);
    }

    /** Skaliert eine Texture gleichmaessig bis sie die max-Werte nicht mehr ueberschreitet */
    public static void zeichneTexturRegion(Batch batch, int x, int y,
                                           TextureRegion worldTextureRegion,
                                           float maxWidth, float maxHeight) {
        float actualWidth = worldTextureRegion.getRegionWidth();
        float actualHeight = worldTextureRegion.getRegionHeight();
        float shouldScaleX = maxWidth / actualWidth;
        float shouldScaleY = maxHeight / actualHeight;
        float scale = Math.min(shouldScaleX, shouldScaleY);
        Vector2 centeredPosition = TextureUtils.center(x, y,
                actualWidth * scale, actualHeight * scale,
                maxWidth, maxHeight);
        zeichneTexturRegion(batch, worldTextureRegion,
                centeredPosition.x, centeredPosition.y,
                scale, scale,
                false);
    }

    //Teilweise aus dem Udacity Kurs kopiert
    public static void zeichneTexturRegion(Batch batch, TextureRegion region, Vector2 position, boolean horizontalSpiegeln) {
        zeichneTexturRegion(batch, region, position.x, position.y, 1, 1, horizontalSpiegeln);
    }
    public static void zeichneTexturRegion(Batch batch, TextureRegion region, Vector2 position, float scaleX, float scaleY, boolean horizontalSpiegeln) {
        zeichneTexturRegion(batch, region, position.x, position.y, scaleX, scaleY, horizontalSpiegeln);
    }
    public static void zeichneTexturRegion(Batch batch, TextureRegion region, float x, float y, float scaleX, float scaleY, boolean horizontalSpiegeln) {
        batch.draw(
                region.getTexture(),
                x,
                y,
                0,
                0,
                region.getRegionWidth(),
                region.getRegionHeight(),
                scaleX,
                scaleY,
                0,
                region.getRegionX(),
                region.getRegionY(),
                region.getRegionWidth(),
                region.getRegionHeight(),
                horizontalSpiegeln,
                false);
    }

    public static void drawTextureRegion(Batch batch, TextureRegion region, Vector2 position) {
        drawTextureRegion(batch, region, position.x, position.y);
    }
    public static void drawTextureRegion(Batch batch, TextureRegion region, float groesseModifikator, Vector2 position) {
        drawTextureRegion(batch, region, groesseModifikator, position.x, position.y);
    }

    public static void drawTextureRegion(Batch batch, TextureRegion region, Vector2 position, Vector2 offset) {
        drawTextureRegion(batch, region, position.x - offset.x, position.y - offset.y);
    }
    public static void drawTextureRegion(Batch batch, TextureRegion region, float groesseModifikator, Vector2 position, Vector2 offset) {
        drawTextureRegion(batch, region, groesseModifikator, position.x - offset.x, position.y - offset.y);
    }

    public static void drawTextureRegion(Batch batch, TextureRegion region, float x, float y) {
        batch.draw(
                region.getTexture(),
                x,
                y,
                0,
                0,
                region.getRegionWidth(),
                region.getRegionHeight(),
                1,
                1,
                0,
                region.getRegionX(),
                region.getRegionY(),
                region.getRegionWidth(),
                region.getRegionHeight(),
                false,
                false);
    }
    public static void drawTextureRegion(Batch batch, TextureRegion region, float groesseModifikator, float x, float y) {
        batch.draw(
                region.getTexture(),
                x,
                y * groesseModifikator, //Damit die Figur auf dem Boden stehen bleibt
                0,
                0,
                (int)(region.getRegionWidth() * groesseModifikator),
                (int)(region.getRegionHeight() * groesseModifikator),
                1,
                1,
                0,
                region.getRegionX(),
                region.getRegionY(),
                region.getRegionWidth(),
                region.getRegionHeight(),
                false,
                false);
    }
}
