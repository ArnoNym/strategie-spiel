package de.arnonym.strategiespiel.framework.werkzeuge.textures;

import com.badlogic.gdx.math.Vector2;

import de.arnonym.strategiespiel.spielSchirm.Enums;

public abstract class IdTexture extends Texture {
    private final int id;

    public IdTexture(int id, boolean flipHorizontaly, boolean flipVerticaly,
                     Enums.EnumAddTexture enumAddTexture, float standardDauer,
                     Vector2 renderMod_code) {
        super(flipHorizontaly, flipVerticaly, enumAddTexture, standardDauer, renderMod_code);
        this.id = id;
    }

    protected int getId() {
        return id;
    }
}
