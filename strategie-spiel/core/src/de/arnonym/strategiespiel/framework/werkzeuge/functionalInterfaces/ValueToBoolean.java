package de.arnonym.strategiespiel.framework.werkzeuge.functionalInterfaces;

public interface ValueToBoolean<V> {
    boolean to(V v);
}
