package de.arnonym.strategiespiel.framework.werkzeuge.numbers.unit;

public class Add<E extends UnitInterface<E>> extends UnitOperation<E> {
    public Add(Unit<E> rawOperator1, Number rawOperator2) {
        super(rawOperator1, rawOperator2);
    }

    public Add(Unit<E> rawOperator1, Unit<E> rawOperator2) {
        super(rawOperator1, rawOperator2);
    }

    public Add(Unit<E> rawOperator1, Number rawOperator2, E e) {
        super(rawOperator1, rawOperator2, e);
    }

    @Override
    public double doubleValue() {
        return equalOperator1 + equalOperator2;
    }
}
