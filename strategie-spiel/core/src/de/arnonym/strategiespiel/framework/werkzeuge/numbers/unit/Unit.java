package de.arnonym.strategiespiel.framework.werkzeuge.numbers.unit;

import de.arnonym.strategiespiel.framework.werkzeuge.numbers.MyNumber;
import de.arnonym.strategiespiel.framework.werkzeuge.numbers.pin.Pin;

public class Unit<E extends UnitInterface<E>> extends MyNumber {
    public final E enumE;
    protected final double value;

    public Unit(double value, E e) {
        this.value = value;
        this.enumE = e;
    }

    public Unit<E> clean() {
        return new Unit<>(0, enumE);
    }

    @Override
    public int intValue() {
        return (int) value;
    }

    public int intValue(E e) {
        return (int) doubleValue(e);
    }

    @Override
    public long longValue() {
        return (long) value;
    }

    public long longValue(E e) {
        return (long) doubleValue(e);
    }

    @Override
    public float floatValue() {
        return (float) value;
    }

    public float floatValue(E e) {
        return (float) doubleValue(e);
    }

    @Override
    public double doubleValue() {
        return value;
    }

    public double doubleValue(E e) {
        int nowIndex = enumE.index();
        int futureIndex = e.index();
        int steps = nowIndex - futureIndex;
        double d = doubleValue();
        if (steps == 0) {
            return d;
        } else if (steps < 0) {
            for (UnitInterface<E> ue : e.getUnitEnums()) {
                d *= ue.toNextStepMult();
            }
        } else {
            UnitInterface<E>[] unitInterfaces = e.getUnitEnums();
            for (int i = steps - 1; i >= 0; i--) {
                d /= unitInterfaces[i].toNextStepMult();
            }
        }
        return d;
    }

    @Override
    public Pin pinValue() {
        return new Pin(doubleValue(), Pin.STANDARD_DIGITS);
    }

    @Override
    public Pin pinValue(int digits) {
        return new Pin(doubleValue(), digits);
    }

    public Pin pinValue(E e) {
        return new Pin(doubleValue(e), Pin.STANDARD_DIGITS);
    }

    public Pin pinValue(E e, int digits) {
        return new Pin(doubleValue(e), digits);
    }

    @Override
    public Unit<E> add(Number number) {
        return (new Add<>(this, number, enumE)).unitValue();
    }

    @Override
    public Unit<E> sub(Number number) {
        return (new Sub<>(this, number, enumE)).unitValue();
    }

    @Override
    public Unit<E> mult(Number number) {
        return (new Mult<>(this, number, enumE)).unitValue();
    }

    @Override
    public Unit<E> div(Number number) {
        return (new Div<>(this, number, enumE)).unitValue();
    }
}
