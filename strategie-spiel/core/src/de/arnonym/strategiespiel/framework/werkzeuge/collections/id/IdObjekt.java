package de.arnonym.strategiespiel.framework.werkzeuge.collections.id;

public interface IdObjekt {
    public abstract int gettId();
}
