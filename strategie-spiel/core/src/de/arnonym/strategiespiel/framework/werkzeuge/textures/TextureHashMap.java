package de.arnonym.strategiespiel.framework.werkzeuge.textures;

import java.util.HashMap;

import de.arnonym.strategiespiel.spielSchirm.Enums;

public class TextureHashMap extends HashMap<Enums.EnumAktion, Texture> {
    /* Sinn dieser HashMap ist, dass grafische Veraenderungen nicht an vielen unterschiedlichen
    Componenten durchgefuehrt werden muessen sondern nur diese HashMap ausgetauscht werden muss */

    public TextureHashMap(Texture standardTexture) {
        put(Enums.EnumAktion.STANDARD_PRIMAERMENGE_VOLL, standardTexture);
    }
}
