package de.arnonym.strategiespiel.framework.werkzeuge;

import java.util.ArrayList;
import java.util.List;

import de.arnonym.strategiespiel.spielSchirm.World;

public class Manager {

    public Manager(World world) {
        if (world.manager != null) {
            throw new IllegalStateException();
        }
    }

    // IDS ////////////////////////////////////////////////////////////////////////////////////////

    private long id = 0;
    private final List<Long> freedIds = new ArrayList<>();

    public long createId() {
        int s = freedIds.size();
        if (s > 0) {
            return freedIds.remove(s - 1);
        }
        return id++;
    }

    public void freeId(long id) {
        if (freedIds.contains(id)) {
            throw new IllegalArgumentException();
        }
        freedIds.add(id);
    }

    /*// ADD ////////////////////////////////////////////////////////////////////////////////////////

    public static void add(Connector connector, Connectable... connectables) {
        for (Connectable connectable : connectables) {
            add(connector.getSystorSupport(), connectable.getSystableSupport());
        }
    }
    public static void add(Connector connector, Connectable connectable) {
        add(connector.getSystorSupport(), connectable.getSystableSupport());
    }
    public static void add(ConnectorSupport connectorSupport,
                           StorySupport<Connectable<?>, Connector> connectableSupport) {
        connectorSupport.add(connectableSupport.getSupported());
        connectableSupport.add(connectorSupport.connector);
    }

    public static void add(Visitor visitor, Visitable... visitables) {
        for (Visitable visitable : visitables) {
            add(visitor.getVisitorSupport(), visitable.getVisitableSupport());
        }
    }
    public static void add(Visitor visitor, Visitable visitable) {
        add(visitor.getVisitorSupport(), visitable.getVisitableSupport());
    }
    /*public static void add(VisitorSupport visitorSupport, VisitableSupport visitableSupport) {
        visitorSupport.add(visitableSupport.visitable);
        visitableSupport.add(visitorSupport.visitor);
    }

    public static void add(StorySupport<Visitable<? extends Visitor>, Visitor> visitorSupport,
                           StorySupport<Visitor, Visitable<? extends Visitor>> visitableSupport) {
        visitorSupport.add(visitableSupport.getSupported());
        visitableSupport.add(visitorSupport.getSupported());
    }

    // NON DELETE REMOVE //////////////////////////////////////////////////////////////////////////

    public static boolean remove(Connector connector, Connectable connectable) {
        return remove(connector.getSystorSupport(), connectable.getSystableSupport());
    }
    public static boolean remove(ConnectorSupport connectorSupport,
                                 StorySupport<Connectable<?>, Connector> connectableSupport) {
        boolean cOrRemoved = connectorSupport.remove(connectableSupport.getSupported());
        boolean cAbleRemoved = connectableSupport.remove(connectorSupport.connector);
        if (cOrRemoved && cAbleRemoved) {
            return true;
        }
        if (!cOrRemoved && !cAbleRemoved) {
            return false;
        }
        throw new IllegalStateException();
    }

    public static boolean remove(Visitor visitor, Visitable visitable) {
        return remove(visitor.getVisitorSupport(), visitable.getVisitableSupport());
    }
    public static boolean remove(VisitorSupport visitorSupport, VisitableSupport visitableSupport) {
        boolean vOrRemoved = visitorSupport.remove(visitableSupport.visitable);
        boolean vAbleRemoved = visitableSupport.remove(visitorSupport.visitor);
        if (vOrRemoved && vAbleRemoved) {
            return true;
        }
        if (!vOrRemoved && !vAbleRemoved) {
            return false;
        }
        throw new IllegalStateException();
    }*/
}
