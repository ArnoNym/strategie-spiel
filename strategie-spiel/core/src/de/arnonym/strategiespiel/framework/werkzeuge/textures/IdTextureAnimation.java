package de.arnonym.strategiespiel.framework.werkzeuge.textures;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

import de.arnonym.strategiespiel.spielSchirm.Assets;
import de.arnonym.strategiespiel.spielSchirm.Enums;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.MyTimeUtils;

public class IdTextureAnimation extends IdTexture {

    private transient Animation animation;
    private long startZeit;

    public IdTextureAnimation(Animation animation, Enums.EnumAddTexture enumAddTexture,
                              float standardDauer, Vector2 renderMod_code) {
        this(animation, false, false, enumAddTexture, standardDauer, renderMod_code);
    }

    public IdTextureAnimation(Animation animation, boolean flipHorizontaly, boolean flipVerticaly,
                              Enums.EnumAddTexture enumAddTexture, float standardDauer,
                              Vector2 renderMod_code) {
        super(Assets.instance.getId(animation), flipHorizontaly, flipVerticaly, enumAddTexture,
                standardDauer, renderMod_code);
        this.animation = animation;
    }

    @Override
    public void start() {
        this.startZeit = com.badlogic.gdx.utils.TimeUtils.nanoTime();
    }

    @Override
    public TextureRegion get() {
        if (animation == null) {
            animation = Assets.instance.getAnimation(getId());
        }
        return (TextureRegion) animation.getKeyFrame(MyTimeUtils.secondsSince(startZeit));
    }

    @Override
    protected float gettRightWidth(float leftWidth) {
        return leftWidth; // Keine Breite oder Hoehe von Animationen gefunden
    }

    @Override
    protected float gettTopHeight(float bottomHeight) {
        return bottomHeight; // Keine Breite oder Hoehe von Animationen gefunden
    }
}
