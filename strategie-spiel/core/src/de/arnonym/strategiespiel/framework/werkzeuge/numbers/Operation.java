package de.arnonym.strategiespiel.framework.werkzeuge.numbers;

import de.arnonym.strategiespiel.framework.werkzeuge.numbers.pin.Pin;

public abstract class Operation<RO1 extends Number, RO2 extends Number> extends MyNumber<RO1> {
    public final RO1 rawOperator1;
    public final RO2 rawOperator2;

    public Operation(RO1 rawOperator1, RO2 rawOperator2) {
        this.rawOperator1 = rawOperator1;
        this.rawOperator2 = rawOperator2;
    }

    public abstract Pin pinValue();

    public abstract Pin pinValue(int digits);
}
