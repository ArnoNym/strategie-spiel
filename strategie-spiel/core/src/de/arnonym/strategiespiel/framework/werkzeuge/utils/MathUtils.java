package de.arnonym.strategiespiel.framework.werkzeuge.utils;

import java.util.Set;

import de.arnonym.strategiespiel.spielSchirm.balancing.Konstanten;
import de.arnonym.strategiespiel.framework.werkzeuge.numbers.pin.Pin;

public class MathUtils {

    // MIN UND MAX ////////////////////////////////////////////////////////////////////////////////

    public static Pin min(Pin... pins) {
        Pin min = pins[0];
        for(Pin n : pins){
            if (min.compareTo(n) < 0) {
                min = n;
            }
        }
        return min;
    }
    public static Pin max(Pin... pins) {
        Pin max = pins[0];
        for(Pin n : pins){
            if (max.compareTo(n) > 0) {
                max = n;
            }
        }
        return max;
    }

    public static int min(Integer... integers) {
        int min = Integer.MAX_VALUE;
        for(int i : integers){
            min = Math.min(min, i);
        }
        return min;
    }
    public static int max(Integer... integers) {
        int max = -Integer.MAX_VALUE;
        for(int i : integers){
            max = Math.max(max, i);
        }
        return max;
    }

    public static float min(Float... floats) {
        float min = 999999999;
        for(float f : floats){
            min = Math.min(min, f);
        }
        return min;
    }
    public static float min(Set<Float> floatSet) {
        float min = 999999999;
        for(float f : floatSet){
            min = Math.min(min, f);
        }
        return min;
    }
    public static float max(Float... floats) {
        float max = -Float.MAX_VALUE;
        for(float f : floats){
            max = Math.max(max, f);
        }
        return max;
    }

    // RUNDEN /////////////////////////////////////////////////////////////////////////////////////

    public static float runden(double zahlRunden, int nachkommastellten) {
        double i = Math.pow(10, nachkommastellten);
        return (float) (Math.round(zahlRunden * i) / i);
    }

    public static int myIntRunden(int intRunden, int nachkommastellten) {
        double zahlRunden = intRunden / Konstanten.NACHKOMMASTELLEN;
        double i = Math.pow(10, nachkommastellten);
        double zahlGerundet = Math.round(zahlRunden * i) / i;
        return (int) (zahlGerundet * Konstanten.NACHKOMMASTELLEN);
    }

    public static int myIntRundenAufGanzeZahl(int intRunden) {
        int nachkommastellten = (int)(Math.log10(Konstanten.NACHKOMMASTELLEN)+1);
        return myIntRunden(intRunden, nachkommastellten);
    }

    public static int myIntRunden(int intRunden, boolean isTeilbar) {
        if (isTeilbar) {
            return intRunden;
        }
        return myIntRundenAufGanzeZahl(intRunden);
    }

    // MULTIPLIKATION UND DIVISION ////////////////////////////////////////////////////////////////

    public static int myIntMult(int int1, int int2) {
        return int1 * int2 / Konstanten.NACHKOMMASTELLEN;
    }

    public static int myIntDiv(int zaehler, int nenner) {
        return (int) ((float) zaehler / nenner * Konstanten.NACHKOMMASTELLEN);
    }

    public static int myIntPercent(int zaehler, int nenner) {
        return (int) ((float) zaehler / nenner);
    }
}
