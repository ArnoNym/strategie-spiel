package de.arnonym.strategiespiel.framework.werkzeuge.pause;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.TimeUtils;

import java.util.concurrent.ThreadLocalRandom;

import de.arnonym.strategiespiel.spielSchirm.Enums;

public class GuiPause extends Pause {
    private long startzeit;

    public GuiPause(float dauer, Enums.EnumPauseStart enumPauseStart) {
        super(dauer, enumPauseStart);
    }

    // KOMPLEXE GETTER UND SETTER /////////////////////////////////////////////////////////////

    @Override
    public void settStartzeitJetzt() {
        startzeit = TimeUtils.nanoTime();
    }

    @Override
    public void settStartzeitZufaelligeRestdauer() {
        startzeit = TimeUtils.nanoTime() - ThreadLocalRandom.current().nextLong()
                * (long) (getDauer() / MathUtils.nanoToSec);
    }

    @Override
    public float gettZeitSeitStartzeit() {
        return TimeUtils.nanoTime() - startzeit;
    }

    @Override
    public void settVorbei() {
        startzeit = -Long.MAX_VALUE;
    }

    @Override
    public boolean issVorbei() {
        return startzeit + (long) (getDauer() / MathUtils.nanoToSec) < TimeUtils.nanoTime();
    }
}
