package de.arnonym.strategiespiel.framework.werkzeuge.pause;

import de.arnonym.strategiespiel.spielSchirm.Enums;

public abstract class Pause {
    private float dauer;

    public Pause(float dauer, Enums.EnumPauseStart enumPauseStart) {
        this.dauer = dauer;
        settStartzeit(enumPauseStart);
    }

    // STARTZEIT //////////////////////////////////////////////////////////////////////////////////

    private void settStartzeit(Enums.EnumPauseStart enumPauseStart) {
        switch (enumPauseStart) {
            case JETZT: settStartzeitJetzt();
            break;
            case JETZT_ZUFAELLIGE_RESTDAUER: settStartzeitZufaelligeRestdauer();
            break;
            case VORBEI: settVorbei();
            break;
            default: throw new IllegalArgumentException();
        }
    }

    public void settStartzeit(boolean zufaelligeRestdauer) {
        if (zufaelligeRestdauer) {
            settStartzeitZufaelligeRestdauer();
        } else {
            settStartzeitJetzt();
        }
    }

    public abstract void settStartzeitJetzt();

    public abstract void settStartzeitZufaelligeRestdauer();

    // KOMPLEXE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public void settDauer(float dauer) { // Kein Neustart
        this.dauer = dauer;
    }

    public void settDauer(float dauer, Enums.EnumPauseStart enumPauseStart) {
        this.dauer = dauer;
        settStartzeit(enumPauseStart);
    }

    public float gettAnteilPauseVergangen() {
        return gettZeitSeitStartzeit() / dauer;
    }

    public abstract float gettZeitSeitStartzeit();

    public abstract void settVorbei();

    public abstract boolean issVorbei();

    // EINFACHE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public float getDauer() {
        return dauer;
    }
}
