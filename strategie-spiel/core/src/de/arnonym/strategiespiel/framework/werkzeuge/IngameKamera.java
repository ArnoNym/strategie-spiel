package de.arnonym.strategiespiel.framework.werkzeuge;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.viewport.Viewport;

import de.arnonym.strategiespiel.spielSchirm.World;
import de.arnonym.strategiespiel.spielSchirm.balancing.Konstanten;
import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenOptik;

/**
 * Created by LeonPB on 07.04.2018.
 */

public class IngameKamera {
    private final World world;
    private final Viewport viewport;
    private final Camera camera;

    private boolean pictureMoved = true;

    public IngameKamera(World world, Viewport worldViewport){
        this.world = world;
        this.viewport = worldViewport;
        this.camera = viewport.getCamera();
    }

    public void update(float delta){
        // TODO Wenn Bildschirm wechselt scrollen pausieren. (Am besten mit Startbildschirm usw einfuegen)
        if (Gdx.input.isKeyPressed(Input.Keys.A)) {
            camera.position.x -= delta * KonstantenOptik.VERFOLGUNGS_KAMERA_BEWEGUNGSGESCHWINDIGKEIT;
            pictureMoved = true;
        }
        if (!Konstanten.DEVELOPER_VISUELL && Gdx.input.getX() < 0 + viewport.getScreenWidth() * (KonstantenOptik.VERFOLGUNGS_KAMERA_MAUS_RAND_SCROLLEN_BREITE_PROZENT)) {
            camera.position.x -= delta * KonstantenOptik.VERFOLGUNGS_KAMERA_BEWEGUNGSGESCHWINDIGKEIT * KonstantenOptik.VERFOLGUNGS_KAMERA_MAUS_RAND_SCROLLEN_BEWEGUNGSGESCHWINDIGKEIT_MODIFIKATOR;
            pictureMoved = true;
        }

        if (Gdx.input.isKeyPressed(Input.Keys.D)) {
            camera.position.x += delta * KonstantenOptik.VERFOLGUNGS_KAMERA_BEWEGUNGSGESCHWINDIGKEIT;
            pictureMoved = true;
        }
        if (!Konstanten.DEVELOPER_VISUELL && Gdx.input.getX() > viewport.getScreenWidth() * (1 - KonstantenOptik.VERFOLGUNGS_KAMERA_MAUS_RAND_SCROLLEN_BREITE_PROZENT)) {
            camera.position.x += delta * KonstantenOptik.VERFOLGUNGS_KAMERA_BEWEGUNGSGESCHWINDIGKEIT * KonstantenOptik.VERFOLGUNGS_KAMERA_MAUS_RAND_SCROLLEN_BEWEGUNGSGESCHWINDIGKEIT_MODIFIKATOR;
            pictureMoved = true;
        }

        if (Gdx.input.isKeyPressed(Input.Keys.W)) {
            camera.position.y += delta * KonstantenOptik.VERFOLGUNGS_KAMERA_BEWEGUNGSGESCHWINDIGKEIT/2;
            pictureMoved = true;
        }
        if (!Konstanten.DEVELOPER_VISUELL && Gdx.input.getY() < 0 + viewport.getScreenHeight() * (KonstantenOptik.VERFOLGUNGS_KAMERA_MAUS_RAND_SCROLLEN_BREITE_PROZENT)) {
            camera.position.y += delta * KonstantenOptik.VERFOLGUNGS_KAMERA_BEWEGUNGSGESCHWINDIGKEIT / 2 * KonstantenOptik.VERFOLGUNGS_KAMERA_MAUS_RAND_SCROLLEN_BEWEGUNGSGESCHWINDIGKEIT_MODIFIKATOR;
            pictureMoved = true;
        }

        if (Gdx.input.isKeyPressed(Input.Keys.S)) {
            camera.position.y -= delta * KonstantenOptik.VERFOLGUNGS_KAMERA_BEWEGUNGSGESCHWINDIGKEIT / 2;
            pictureMoved = true;
        }
        if (!Konstanten.DEVELOPER_VISUELL && Gdx.input.getY()>viewport.getScreenHeight() * (1 - KonstantenOptik.VERFOLGUNGS_KAMERA_MAUS_RAND_SCROLLEN_BREITE_PROZENT)) {
            camera.position.y -= delta * KonstantenOptik.VERFOLGUNGS_KAMERA_BEWEGUNGSGESCHWINDIGKEIT / 2 * KonstantenOptik.VERFOLGUNGS_KAMERA_MAUS_RAND_SCROLLEN_BEWEGUNGSGESCHWINDIGKEIT_MODIFIKATOR;
            pictureMoved = true;
        }

        if (pictureMoved) {
            preufeVisible();
            pictureMoved = false;
        }
    }

    public void mausradScrolled(int amount) {
        float scrollStepsPercent = KonstantenOptik.VERFOLGUNGS_KAMERA_MAUSRAD_SCOLL_SPEED;
        float worldWith = viewport.getWorldWidth() * (1 + scrollStepsPercent * amount) ;
        float worldHeight = viewport.getWorldHeight() * (1 + scrollStepsPercent * amount) ;
        viewport.setWorldSize(worldWith, worldHeight);
        pictureMoved = true;
    }

    public void resize() {
        pictureMoved = true;
    }

    // PRUEFE VISIBLE /////////////////////////////////////////////////////////////////////////////

    public void preufeVisible() {
        float x = camera.position.x;
        float y = camera.position.y;
        float width = viewport.getWorldWidth();
        float heigt = viewport.getWorldHeight();
        float kameraLeft = x - width / 2;
        float kameraRight = x + width / 2;
        float kameraBottom = y - heigt / 2;
        float kameraTop = y + heigt / 2;
        world.getSysRender().preufeVisible(kameraLeft, kameraRight, kameraBottom, kameraTop);
    }

    public boolean preufeVisible(float entityLeft, float entityRight,
                                 float entityBottom, float entityTop) {
        float width = viewport.getWorldWidth();
        float heigt = viewport.getWorldHeight();
        float x = camera.position.x - width/2;
        float y = camera.position.y - heigt/2;
        return (!(x >= entityRight || x + width <= entityLeft
                || y >= entityBottom || y + heigt <= entityTop));
    }

    // KOMPLEXE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public void settPosition(Vector2 position) {
        camera.position.x = position.x;
        camera.position.y = position.y;
    }

    public Vector2 gettPosition() {
        return new Vector2(camera.position.x, camera.position.y);
    }
}
