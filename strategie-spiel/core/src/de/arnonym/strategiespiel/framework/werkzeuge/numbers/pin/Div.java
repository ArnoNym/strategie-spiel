package de.arnonym.strategiespiel.framework.werkzeuge.numbers.pin;

public class Div extends PinOperation {

    public Div(Pin operator1, Number operator2) {
        super(operator1, operator2);
    }

    public Div(Pin operator1, Number operator2, int digits) {
        super(operator1, operator2, digits);
    }

    @Override
    public Pin pinValue() {
        return new Pin(doubleValue(), resultDigits());
    }

    @Override
    public double doubleValue() {
        return equalOperator1.doubleValue() / equalOperator2.doubleValue();
    }
}
