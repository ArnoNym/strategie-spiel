package de.arnonym.strategiespiel.framework.werkzeuge.math;

import com.badlogic.gdx.math.Vector2;

import java.util.Objects;

public class FinalVector2 {
    public final float x;
    public final float y;

    public FinalVector2(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public FinalVector2(Vector2 vector2) {
        this.x = vector2.x;
        this.y = vector2.y;
    }

    public FinalVector2(FinalVector2 finalVector2) {
        this.x = finalVector2.x;
        this.y = finalVector2.y;
    }

    public Vector2 toVector2() {
        return new Vector2(x, y);
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) return false;
        if (!(o instanceof FinalVector2)) return false;
        FinalVector2 other = (FinalVector2) o;
        return x == other.x && y == other.y;
    }

    public boolean equals(Vector2 vector2) {
        return vector2 != null && x == vector2.x && y == vector2.y;
    }
}
