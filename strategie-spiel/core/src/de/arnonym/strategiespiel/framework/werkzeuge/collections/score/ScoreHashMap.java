package de.arnonym.strategiespiel.framework.werkzeuge.collections.score;

import java.util.HashMap;
import java.util.Map;

public class ScoreHashMap<O, S extends Score> extends HashMap<O, S> {

    public ScoreHashMap() {

    }

    public Map.Entry<O, S> gettMaxScoreEntry() {
        float maxScoreValue = - Float.MAX_VALUE;
        Entry<O, S> maxScoreEntry = null;
        for (Entry<O, S> entry : entrySet()) {
            S score = entry.getValue();
            if (score.getValue() > maxScoreValue) {
                maxScoreValue = score.getValue();
                maxScoreEntry = entry;
            }
        }
        if (maxScoreEntry == null) throw new IllegalStateException();
        return maxScoreEntry;
    }

    public Map.Entry<O, S> removeMaxScoreEntry() {
        Entry<O, S> maxScoreEntry = gettMaxScoreEntry();
        remove(maxScoreEntry.getKey());
        return maxScoreEntry;
    }
}
