package de.arnonym.strategiespiel.framework.werkzeuge.utils;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import de.arnonym.strategiespiel.spielSchirm.balancing.KonstantenOptik;

public class GeometryUtils {

    public static int minDistanzZiel(Vector2 start, Vector2[] zielPositionArray) {
        if (zielPositionArray.length == 0) {
            throw new IllegalArgumentException();
        }
        float minDistanz = Float.MAX_VALUE;
        int minDistanzIndex = -1;
        for (int i = 0; i < zielPositionArray.length; i++) {
            float distanz = hypothenuseIsoStrecke(start, zielPositionArray[i]);
            if (distanz < minDistanz) {
                minDistanz = distanz;
                minDistanzIndex = i;
            }
        }
        if (minDistanzIndex == -1) {
            throw new IllegalStateException();
        }
        return minDistanzIndex;
    }

    public static Object minDistanzZiel(Vector2 start, HashMap<Object, Vector2> zielPositionHashMap) {
        float minDistanz = Float.MAX_VALUE;
        Object minDistanzObject = null;
        for (Map.Entry<Object, Vector2> entry : zielPositionHashMap.entrySet()) {
            float distanz = hypothenuseIsoStrecke(start, entry.getValue());
            if (distanz < minDistanz) {
                minDistanz = distanz;
                minDistanzObject = entry.getKey();
            }
        }
        if (minDistanzObject == null && !zielPositionHashMap.isEmpty()) {
            throw new IllegalStateException();
        }
        return minDistanzObject;
    }

    public static Vector2 gettIndizes(Vector2 isoWorldPosition) {
        Vector2 ortoWorldPosition = GeometryUtils.isoToOrto(isoWorldPosition);
        int x = (int) ((ortoWorldPosition.x + 1) / KonstantenOptik.TILE_ORTO_WIDTH_AND_HEIGHT);
        int y = (int) ((ortoWorldPosition.y + 1) / KonstantenOptik.TILE_ORTO_WIDTH_AND_HEIGHT);
        if (ortoWorldPosition.x < 0) {
            x -= 1;
        }
        if (ortoWorldPosition.y < 0) {
            y -= 1;
        }
        return new Vector2(x, y);
    }

    public static ArrayList<Vector2> gettIndizesListe(Vector2 position, Vector2 fundament) {
        Vector2 ortoPosition = isoToOrto(position);
        float width = fundament.x * KonstantenOptik.TILE_ORTO_WIDTH_AND_HEIGHT;
        float height = fundament.y * KonstantenOptik.TILE_ORTO_WIDTH_AND_HEIGHT;
        float left = ortoPosition.x - width / 2 + 1;
        float bottom = ortoPosition.y - height / 2 + 1;

        int leftIndex = (int) (left / KonstantenOptik.TILE_ORTO_WIDTH_AND_HEIGHT);
        int bottomIndex = (int) (bottom / KonstantenOptik.TILE_ORTO_WIDTH_AND_HEIGHT);
        if (left < 0) {
            leftIndex -= 1;
        }
        if (bottom < 0) {
            bottomIndex -= 1;
        }

        ArrayList<Vector2> indizesListe = new ArrayList<>();
        for (int x = 0; x < fundament.x; x++) {
            for (int y = 0; y < fundament.y; y++) {
                indizesListe.add(new Vector2(x + leftIndex, y + bottomIndex));
            }
        }
        return indizesListe;
    }

    public static Vector2 ganzerTile(Vector2 isoPosition, Vector2 blaubpausenMod) {
        // erklaerung-003 und im 'formeln'-Ordner
        Vector2 ortoPosition = GeometryUtils.isoToOrto(isoPosition);

        float xFloatZellen = ortoPosition.x / KonstantenOptik.TILE_ORTO_WIDTH_AND_HEIGHT;
        float yFloatZellen = ortoPosition.y / KonstantenOptik.TILE_ORTO_WIDTH_AND_HEIGHT;

        float xZellen = (int) xFloatZellen; // float ist korrekt (siehe unten)
        float yZellen = (int) yFloatZellen;

        // Die Abfrage damit es im negativen Bereich auch nach 'unten' und nicht in Richtung 0 snapt
        if (xFloatZellen < 0) {
            xZellen -= 1;
        }
        if (yFloatZellen < 0) {
            yZellen -= 1;
        }

        if (blaubpausenMod.x % 2 == 0) {
            /* Machen, dass der Sprung zB bei Feldern mit geradem X von einer Zelle in die andere
            zwischen den Feldern passiert */
            if (xFloatZellen - xZellen >= 0.5) {
                xZellen += 1;
            }
            if (yFloatZellen - yZellen >= 0.5) {
                yZellen += 1;
            }
        } else {
            /* Machen, dass Sachen mit ungeradem x auf den Zellen Raendern stehen. Verschiebung
            nach oben rechts, dasmit das verschieben in die Mitte ueberall gleich funktioniert */
            xZellen += 0.5;
            yZellen += 0.5;
        }

        return ortoToIso(new Vector2(
                xZellen * KonstantenOptik.TILE_ORTO_WIDTH_AND_HEIGHT,
                yZellen * KonstantenOptik.TILE_ORTO_WIDTH_AND_HEIGHT));
    }

    public static Vector2 isoToOrto (Vector2 isoVector) {
        //EcsDokumentation-003
        float isoTilesX = isoVector.x / KonstantenOptik.TILE_ISO_WIDTH;
        float isoTilesY = isoVector.y / KonstantenOptik.TILE_ISO_HEIGHT;

        float ortoX = (isoTilesX - isoTilesY) * KonstantenOptik.TILE_ORTO_WIDTH_AND_HEIGHT;
        float ortoY = (isoTilesX + isoTilesY) * KonstantenOptik.TILE_ORTO_WIDTH_AND_HEIGHT;

        return new Vector2(ortoX, ortoY);
    }

    public static Vector2 ortoToIso (Vector2 ortoVector) {
        //EcsDokumentation-003

        float ortoTilesX = ortoVector.x / KonstantenOptik.TILE_ORTO_WIDTH_AND_HEIGHT;
        float ortoTilesY = ortoVector.y / KonstantenOptik.TILE_ORTO_WIDTH_AND_HEIGHT;

        float isoX = (ortoTilesX + ortoTilesY) * KonstantenOptik.TILE_ISO_WIDTH / 2;
        float isoY = (ortoTilesY - ortoTilesX) * KonstantenOptik.TILE_ISO_HEIGHT / 2;

        return new Vector2(isoX, isoY);
    }

    public static Vector2 isoToOrtoOhneKorrektur (Vector2 punkt) {
        Vector2 tempPunkt = new Vector2(0, 0);
        float tileWidthHalf = KonstantenOptik.TILE_ISO_WIDTH /2;
        float tileHeightHalf = KonstantenOptik.TILE_ISO_HEIGHT /2;
        float tileGroesse = KonstantenOptik.TILE_ORTO_WIDTH_AND_HEIGHT;

        tempPunkt.x = (punkt.x/tileWidthHalf-punkt.y/tileHeightHalf)*tileGroesse/2;
        tempPunkt.y = (punkt.x/tileWidthHalf+punkt.y/tileHeightHalf)*tileGroesse/2;
        return(tempPunkt);
    }

    public static float hypothenuse(float x, float y) {
        return (float) Math.pow(Math.pow(x, 2) + Math.pow(y, 2), 0.5);
    }

    private static float hypothenuse(Vector2 v1, Vector2 v2) {
        float x = v1.x - v2.x;
        float y = v1.y - v2.y;
        return hypothenuse(x, y);
    }

    public static float hypothenuseIsoStrecke(Vector2 v1, Vector2 v2) {
        float x = v1.x - v2.x;
        float y = v1.y - v2.y;
        return hypothenuse(x, y * 2);
    }

    public static Vector2 applyBlaupausenMod(Vector2 position, Vector2 blaubpausenMod) {
        return new Vector2(position.x + KonstantenOptik.TILE_ORTO_UND_ISOXACHSE_DIAGONAL
                * blaubpausenMod.x / 2, position.y);
    }

    public static Vector2 positionUntenLinks(Vector2 positionMitteImViereck, Vector2 breiteUndHoeheDesVierecks) {
        return new Vector2(positionMitteImViereck.x-breiteUndHoeheDesVierecks.x/2, positionMitteImViereck.y-breiteUndHoeheDesVierecks.y/2);
    }
    public static boolean berechneKollisionViereckUndPunkt(Vector2 untenLinksPositionDesVierecks, Vector2 breiteUndHoeheDesVierecks, Vector2 positionDesPunktes){
        Rectangle rectangle = new Rectangle(untenLinksPositionDesVierecks.x, untenLinksPositionDesVierecks.y, breiteUndHoeheDesVierecks.x, breiteUndHoeheDesVierecks.y);
        return rectangle.contains(positionDesPunktes);
    }
    public static boolean berechneKollisionViereckUndPunktMitMitte(Vector2 mittePositionDesVierecks, Vector2 breiteUndHoeheDesVierecks, Vector2 positionDesPunktes){
        Vector2 untenLinksPositionDesVierecks = positionUntenLinks(mittePositionDesVierecks, breiteUndHoeheDesVierecks);
        return berechneKollisionViereckUndPunkt(untenLinksPositionDesVierecks, breiteUndHoeheDesVierecks, positionDesPunktes);
    }
}
