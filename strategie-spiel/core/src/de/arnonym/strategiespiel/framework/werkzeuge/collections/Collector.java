package de.arnonym.strategiespiel.framework.werkzeuge.collections;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import de.arnonym.strategiespiel.framework.werkzeuge.interfaces.CollectionContainer;

public class Collector<O> implements CollectionContainer<O> {
    private final Set<O> set;

    // CONSTRUCTOR ////////////////////////////////////////////////////////////////////////////////

    public Collector(Collector<? extends O> collector, O... objects) {
        this.set = new HashSet<>(collector.set);
        add(objects);
    }

    public Collector(O... objects) {
        this.set = new HashSet<>();
        Collections.addAll(this.set, objects);
    }

    public Collector(O object) {
        this.set = new HashSet<>();
        this.set.add(object);
    }

    public Collector() {
        this.set = new HashSet<>();
    }

    // ADD ////////////////////////////////////////////////////////////////////////////////////////

    private void add(O object, boolean mayNull, boolean mayDouble) {
        if (object == null) {
            if (mayNull) {
                return;
            } else {
                throw new IllegalArgumentException();
            }
        }
        if (!set.add(object) && !mayDouble) {
            throw new IllegalArgumentException();
        }
    }

    //

    public Collector<O> add(Collector<? extends O> objects) {
        set.addAll(objects.getSet());
        return this;
    }

    //

    public Collector<O> add(O... objects) {
        for (O o : objects) {
            add(o);
        }
        return this;
    }

    public Collector<O> add(O object) {
        add(object, false, false);
        return this;
    }

    //

    public Collector<O> addMayNull(O object) {
        add(object, true, false);
        return this;
    }

    //

    public Collector<O> addMayDouble(O object) {
        add(object, false, true);
        return this;
    }

    //

    public Collector<O> addMayNullOrDouble(O object) {
        add(object, true, true);
        return this;
    }

    // COLLECTIONS ////////////////////////////////////////////////////////////////////////////////

    public O[] toArray(O[] arr) {
        return set.toArray(arr);
    }

    public Set<O> getSet() {
        return set;
    }

    @Override
    public <COLL extends Collection<O>> COLL toCollection(COLL coll) {
        coll.addAll(set);
        return coll;
    }
}
