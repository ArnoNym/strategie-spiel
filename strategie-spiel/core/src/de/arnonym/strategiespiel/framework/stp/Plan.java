package de.arnonym.strategiespiel.framework.stp;

import de.arnonym.strategiespiel.framework.stp.stacks.StepStack;
import de.arnonym.strategiespiel.framework.werkzeuge.Feedback;

public abstract class Plan {
    /** StepStack im Constructor fuellen und dann mit getStepStack vom Plan erhalten */

    protected final Feedback feedback = new Feedback(getClass().getSimpleName());
    private StepStack stepStack = new StepStack();

    public Plan() {
    }

    public StepStack getStepStack() {
        return stepStack;
    }

    public Feedback getFeedback() {
        return feedback;
    }
}
