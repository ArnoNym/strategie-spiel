package de.arnonym.strategiespiel.framework.stp;

import de.arnonym.strategiespiel.framework.ecs.component.Comp;
import de.arnonym.strategiespiel.framework.ecs.component.Component;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.framework.ecs.collections.Matcher;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.CollectionUtils;
import de.arnonym.strategiespiel.spielSchirm.ecs.components.comps.rendern.RenderComp;

public abstract class Ai extends Component implements StpObject<Task, Boolean> {
    public final RenderComp renderComp;
    public final StpComp stpComp;
    public final int prioritaetId;

    public Ai(Entity entity, RenderComp renderComp, StpComp stpComp, Comp[] comps,
              int prioritaetId) {
        this(new Matcher[]{}, entity, stpComp, renderComp, comps, prioritaetId);
    }

    public Ai(Matcher[] matchers, Entity entity, StpComp stpComp,
              RenderComp renderComp, Comp[] comps, int prioritaetId) {
        super(matchers, entity,
                CollectionUtils.combine(comps, new Comp[]{stpComp, renderComp}));
        this.renderComp = renderComp;
        this.stpComp = stpComp;
        this.prioritaetId = prioritaetId;
    }

    @Override
    public boolean activate() {
        if (super.activate()) {
            stpComp.connect(this);
            return true;
        }
        return false;
    }

    @Override
    public boolean deactivate() {
        if (super.deactivate()) {
            stpComp.disconnect(this);
            return true;
        }
        return false;
    }

    public Boolean getStatus() {
        return isActiv() && equals(stpComp.getCurrentAi());
    }

    public boolean beenden() {
        if (getStatus()) {
            stpComp.abbrechen();
            return true;
        }
        return false;
    }
}
