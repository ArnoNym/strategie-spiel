package de.arnonym.strategiespiel.framework.stp.stacks;

import de.arnonym.strategiespiel.spielSchirm.umwelt.lebewesen.bewegen.StepGeheZu;
import de.arnonym.strategiespiel.framework.stp.Task;
import de.arnonym.strategiespiel.framework.stp.Plan;
import de.arnonym.strategiespiel.framework.stp.Step;
import de.arnonym.strategiespiel.spielSchirm.StepNGZ;
import de.arnonym.strategiespiel.spielSchirm.Enums;

public class StepStack extends Stack<Step> {
    public StepStack() {

    }

    public Enums.EnumStpStatus handle(StepNGZ step, boolean onTop) {
        StepGeheZu stepGeheZu = step.stepGeheZu;

        Enums.EnumStpStatus status = someHandleLogic(step);

        if (status == Enums.EnumStpStatus.OK) {
            if (onTop) {
                add(step);
                handle(stepGeheZu, true);
            } else {
                handle(stepGeheZu, false);
                add(0, step);
            }
        } else {
            stepGeheZu.setStatus(Enums.EnumStpStatus.FAIL);
            someHandleLogic(stepGeheZu);
        }

        return status;
    }

    private void handle(StepGeheZu stepGeheZu, boolean onTop) {
        if (someHandleLogic(stepGeheZu) == Enums.EnumStpStatus.OK) {
            if (onTop) {
                add(stepGeheZu);
            } else {
                add(0, stepGeheZu);
            }
        }
    }

    public void handle(Task vonTask) {
        if (someHandleLogic(vonTask) == Enums.EnumStpStatus.OK) {
            addAll(vonTask.gettSteps());
        }
    }

    public void handle(Plan vonPlan) {
        addAll(vonPlan.getStepStack().iterate());
    }

    public void beenden() {
        for (Step s : iterate()) {
            if (s.getStatus() == Enums.EnumStpStatus.ERLEDIGT) {
                s.erledigt();
            } else {
                s.setStatus(Enums.EnumStpStatus.FAIL);
                s.fail();
            }
        }
    }
}
