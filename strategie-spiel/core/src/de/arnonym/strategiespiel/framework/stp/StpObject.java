package de.arnonym.strategiespiel.framework.stp;

interface StpObject<VERSUCH, STATUS> {
    VERSUCH versuchen();

    //void anfangen();

    //boolean beenden();

    void delete();

    boolean isDeleted();

    /*default String statusString() {
        return "Status == " + getStatus().getClass().getSimpleName();
    }*/

    //STATUS getStatus();

    //void setStatus(STATUS status);

    /*default boolean isStatus(STATUS status) {
        STATUS s = getStatus();
        if (status == null) {
            return s == null;
        }
        return status.equals(s);
    }*/
}
