package de.arnonym.strategiespiel.framework.stp;

import de.arnonym.strategiespiel.framework.listener.StoryTeller;
import de.arnonym.strategiespiel.spielSchirm.Enums;

/**
 * Created by LeonPB on 25.06.2018.
 */

public abstract class Step extends StpMachineObject {
    /**
     * Steps duerfen nur Variablen uebergeben werden, die in eine diese Katergorien fallen:
     * 1. Alle Felder sind final
     * 2. Aenderungen sind fuer diesen Step unwichtig
     * 3. Aenderungen werden vom Step beobachtet
     * 3.1. Beobachtet durch Pruefungen (zB Kollision von Zellen in StepGeheZu)
     * 3.2. Beobachtet durch PropertyChangeListener (zB Geld in StepTauschen)
     *
     * Legende
     * z == Ziel (zB Baum der gefaellt wird)
     * g == Auftraggeber (zB Dorf zum Faellen von Baumen)
     * a == Akteur (zB Dorfbewohner)
     */

    private boolean angefangen = false;

    public Step(StoryTeller... zgStoryTellers) {
        super(zgStoryTellers); /* Alle Visitables die den Aktuer (a) betreffen
        werden bereits im Task gevisited, im Gegensatz zu den z und g Visitables. */
    }

    // VERSUCHEN //////////////////////////////////////////////////////////////////////////////////

    @Override
    protected void versuchenCheckForExceptions() {
        if (angefangen) {
            throw new IllegalStateException(gettStatusString());
        }
        super.versuchenCheckForExceptions();
    }

    // ANFANGEN ///////////////////////////////////////////////////////////////////////////////////

    public final void anfangen() {
        if (!isVersucht() || angefangen || isMindestensEinmalDurchgefuehrt()) {
            throw new IllegalStateException(gettStatusString());
        }
        anfangen_or();
        angefangen = true;
    }

    protected abstract void anfangen_or();

    // DURCHFUEHREN ///////////////////////////////////////////////////////////////////////////////

    @Override
    protected void durchfuehrenCheckForExceptions() {
        if (!angefangen) {
            throw new IllegalStateException(gettStatusString());
        }
        super.durchfuehrenCheckForExceptions();
    }

    // PREUFEN ////////////////////////////////////////////////////////////////////////////////////

    public Enums.EnumStpStatus pruefenObBeenden() { // Prueft ob er erledigt oder gefailt ist
        Enums.EnumStpStatus stepStatus = Enums.EnumStpStatus.OK;
        if (pruefenObErledigt()) {
            stepStatus = Enums.EnumStpStatus.ERLEDIGT;
        } else if (pruefenObFail()) {
            stepStatus = Enums.EnumStpStatus.FAIL;
        }
        setStatus(stepStatus);
        return stepStatus;
    }

    public abstract boolean pruefenObErledigt();

    public abstract boolean pruefenObFail();

    // FAIL ///////////////////////////////////////////////////////////////////////////////////////

    @Override
    protected final void fail_or() {
        if (isMindestensEinmalDurchgefuehrt()) {
            failNachDurchfuehren();
        } else if (angefangen) {
            failNachAnfangenVorDurchfuehren();
        } else if (isVersucht()) {
            failNachVersuchenVorAnfangen();
        }
    }

    private void failNachVersuchenVorAnfangen() {
        if (!isVersucht() || angefangen || isMindestensEinmalDurchgefuehrt()) {
            throw new IllegalStateException(gettStatusString());
        }
        failNachVersuchenVorAnfangen_or();
    }

    protected abstract void failNachVersuchenVorAnfangen_or();

    private void failNachAnfangenVorDurchfuehren() {
        if (!isVersucht() || !angefangen) {
            throw new IllegalStateException(gettStatusString());
        }
        failNachAnfangenVorDurchfuehren_or();
    }

    protected abstract void failNachAnfangenVorDurchfuehren_or();

    private void failNachDurchfuehren() {
        if (!isVersucht() || !angefangen || !isMindestensEinmalDurchgefuehrt()) {
            throw new IllegalStateException(gettStatusString());
        }
        failNachDurchfuehren_or();
    }

    protected abstract void failNachDurchfuehren_or();

    // ECS IMPLEMENTIERUNG ////////////////////////////////////////////////////////////////////////

    @Override
    public final void delete() {
        Enums.EnumStpStatus status;
        if (pruefenObErledigt()) status = Enums.EnumStpStatus.ERLEDIGT;
        else status = Enums.EnumStpStatus.FAIL;
        setStatus(status);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    protected String gettStatusString() {
        return "Step: " + getClass().getSimpleName()
                + ", EnumStepStatus == " + getStatus().name()
                + ", versucht == " + isVersucht()
                + ", angefangen == " + angefangen
                + ", mindestensEinmalDurchgefuehrt == " + isMindestensEinmalDurchgefuehrt() ;
    }

    public boolean isAngefangen() {
        return angefangen;
    }
}
