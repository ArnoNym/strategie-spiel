package de.arnonym.strategiespiel.framework.stp;

import de.arnonym.strategiespiel.spielSchirm.balancing.Konstanten;
import de.arnonym.strategiespiel.framework.stp.stacks.TaskStack;
import de.arnonym.strategiespiel.framework.stp.Step;
import de.arnonym.strategiespiel.framework.stp.Task;

/**
 * Created by LeonPB on 25.06.2018.
 */

public class StpMachine implements java.io.Serializable {
    private TaskStack taskStack = new TaskStack();
    private Task currentTask = null;

    public StpMachine() {

    }

    // UPDATE /////////////////////////////////////////////////////////////////////////////////////

    public void update(float delta) {
        if (taskStack.isEmpty()) {
            return;
        }

        if (currentTask == null) {
            currentTask = taskStack.get(0);
            if (Konstanten.DEVELOPER_VISUELL_STEP_SYSTEM_OUT) System.out.println(getClass().getSimpleName() +" ::: Task ::: " + currentTask.getClass().getSimpleName() +" ::: Anfangen");
        }

        switch (currentTask.getStatus()) {
            case OK:
                currentTask.durchfuehren(delta);
                return;
            case ERLEDIGT:
                currentTask.erledigt();
                break;
            case FAIL:
                currentTask.fail();
                break;
        }
        if (Konstanten.DEVELOPER_VISUELL_STEP_SYSTEM_OUT) System.out.println(getClass().getSimpleName() +" ::: Task ::: " + currentTask.getClass().getSimpleName() + " ::: Beendet");
        taskStack.remove(currentTask);
        currentTask = null;
    }

    // DELETE /////////////////////////////////////////////////////////////////////////////////////

    public void deleteTasks() {
        for (Task t : taskStack.iterate()) {
            t.delete();
        }
        currentTask = null;
    }

    // INFOS //////////////////////////////////////////////////////////////////////////////////////

    public String gettAktuellerTaskName() {
        if (taskStack.isEmpty()) {
            return "idle";
        }
        return currentTask.getClass().getSimpleName();
    }

    public String gettAktuellerStepName() {
        if (taskStack.isEmpty()) {
            return "idle";
        }
        Step currentStep = currentTask.getCurrentStep();
        if (currentStep == null) {
            return "idle";
        }
        return currentStep.getClass().getSimpleName();
    }

    // SONSTIGES //////////////////////////////////////////////////////////////////////////////////

    public boolean handle(Task task, boolean onTop) {
        return taskStack.handle(task, onTop);
    }

    // EINFACHE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public boolean isEmpty() {
        return taskStack.isEmpty();
    }
}
