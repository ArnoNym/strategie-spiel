package de.arnonym.strategiespiel.framework.stp;

import de.arnonym.strategiespiel.framework.ecs.collections.SetSystem;

public class StpSystem extends SetSystem<StpComp> {
    public StpSystem() {

    }

    @Override
    protected void update_or(float delta) {
        for (StpComp stpComp : compSet) {
            stpComp.update(delta);
        }
    }
}
