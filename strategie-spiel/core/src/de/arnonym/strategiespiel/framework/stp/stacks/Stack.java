package de.arnonym.strategiespiel.framework.stp.stacks;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import de.arnonym.strategiespiel.framework.stp.StpMachineObject;
import de.arnonym.strategiespiel.spielSchirm.Enums;

public abstract class Stack<SMO extends StpMachineObject> {
    private final List<SMO> list = new ArrayList<>();

    public Stack() {

    }

    protected final Enums.EnumStpStatus someHandleLogic(StpMachineObject smo) {
        if (smo == null) {
            throw new IllegalArgumentException();
        }
        Enums.EnumStpStatus status;
        if (smo.isVersucht()) {
            status = smo.getStatus();
        } else {
            status = smo.versuchen();
        }
        switch (status) {
            case ERLEDIGT:
                smo.erledigt();
                break;
            case FAIL:
            case FAIL_BEI_GEHE_ZU:
                smo.fail();
                break;
        }
        return status;
    }

    // IMMITIERT LISTE ////////////////////////////////////////////////////////////////////////////

    // KOMPLEX //

    protected void add(SMO smo) { // nutze handle()
        if (list.contains(smo)) {
            throw new IllegalArgumentException("visitorList.size() == "+list.size());
        }
        list.add(smo);
    }

    protected void add(int i, SMO smo) { // nutze handle()
        if (list.contains(smo)) {
            throw new IllegalArgumentException("visitorList.size() == "+list.size());
        }
        list.add(i, smo);
    }

    protected void addAll(Collection<? extends SMO> collection) {
        list.addAll(collection);
    }

    public SMO gettTop() {
        return list.get(list.size() - 1);
    }

    public SMO gettBottom() {
        return list.get(0);
    }

    public SMO removeTop() {
        return list.remove(list.size() - 1);
    }

    public SMO removeLast() {
        return list.remove(0);
    }

    public List<SMO> iterate() {
        return Collections.unmodifiableList(list);
    }

    // EINFACH //

    public SMO get(int i) {
        return list.get(i);
    }

    public boolean isEmpty() {
        return list.isEmpty();
    }

    public SMO remove(int i) {
        return list.remove(i);
    }

    public boolean remove(SMO smo) {
        return list.remove(smo);
    }

    public int size() { //Weil ich keinen Zugriff auf den stepStack selbst gewaehren mochte
        return list.size();
    }

    public void clear() {
        list.clear();
    }
}
