package de.arnonym.strategiespiel.framework.stp;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import de.arnonym.strategiespiel.framework.werkzeuge.interfaces.Deletable;
import de.arnonym.strategiespiel.framework.listener.EnumStory;
import de.arnonym.strategiespiel.framework.listener.StoryListener;
import de.arnonym.strategiespiel.framework.listener.StoryTeller;
import de.arnonym.strategiespiel.spielSchirm.balancing.Konstanten;
import de.arnonym.strategiespiel.spielSchirm.Enums;

public abstract class StpMachineObject implements PropertyChangeListener, Deletable, StoryListener, StpObject<Enums.EnumStpStatus, Enums.EnumStpStatus> {
    /** WRAPPER = StepMachine fuer Tasks und StepStack eines Tasks fuer Steps */

    /** Spart sehr viel Rechenleistung. StpMachineOpjecte muessen immer sofort versucht werden,
     *  da ansonsten comps die bereits deleted sind weiter verwendet werden! Dies wurd durch
     *  @see this.versuchenCheckForExceptions() todo verlinkugn schoen machen! gewaehrleistet! */
    private StoryTeller[] storyTellers;

    private Enums.EnumStpStatus status = null;
    private boolean versucht = false;
    private boolean mindestensEinmalDurchgefuehrt = false;
    private boolean deleted = false;

    public StpMachineObject(StoryTeller... storyTellers) {
        this.storyTellers = storyTellers;
    }

    // STATUS GEBEND //////////////////////////////////////////////////////////////////////////////

    /** Callen bevor dem WRAPPER hinzugefuegt wird. */
    public final Enums.EnumStpStatus versuchen() {
        versuchenCheckForExceptions();
        status = versuchen_or();
        if (status == Enums.EnumStpStatus.OK) {
            ecs_add();
        }
        versucht = true;
        return status;
    }

    protected void versuchenCheckForExceptions() {
        if (versucht || mindestensEinmalDurchgefuehrt) {
            throw new IllegalStateException(gettStatusString());
        }
    }

    protected abstract Enums.EnumStpStatus versuchen_or();

    /** Callen wenn Object geloescht werden soll. Da es lediglich Status gebend ist findet das
     *  eigentliche loeschen dann mit verzoegerung im Wrapper statt. Deshalb sind auch die Ecs
     *  Methoden ueberschrieben bzw in beenden() verschoben! */
    @Override
    public void delete() {
        if (deleted) return;
        deleted = true;
        if (status == Enums.EnumStpStatus.OK) {
            status = Enums.EnumStpStatus.FAIL;
        }
    }

    // STATUS NEHMEND /////////////////////////////////////////////////////////////////////////////

    /** Aufrufen solange status == OK */
    public final void durchfuehren(float delta) {
        durchfuehrenCheckForExceptions();
        durchfuehren_or(delta);
        mindestensEinmalDurchgefuehrt = true;
    }

    protected void durchfuehrenCheckForExceptions() {
        if (!versucht) {
            throw new IllegalStateException(gettStatusString());
        }
    }

    protected abstract void durchfuehren_or(float delta);

    /** Wird aufgerufen wenn status == ERLEDIGT */
    public final void erledigt() {
        beenden();
        erledigt_or();
    }

    protected abstract void erledigt_or();

    /** Wird aufgerufen wenn status == FAIL */
    public final void fail() {
        beenden();
        fail_or();
    }

    protected abstract void fail_or();

    // ECS ////////////////////////////////////////////////////////////////////////////////////////

    /** @see this.delete() todo verlinkugn schoen machen */
    protected void beenden() {
        ecs_delete();
    }

    private void ecs_add() {
        addPropertyChangeListeners();
        for (StoryTeller sl : storyTellers) {
            sl.addListener(this);
        }
        storyTellers = null;
    }

    /** @see this.delete() todo verlinkugn schoen machen */
    private void ecs_delete() {
        if (storyTellers == null) {
            removePropertyChangeListeners();
        }
    }

    // PROPERTY CHANGE ////////////////////////////////////////////////////////////////////////////

    @Override
    public final void propertyChange(PropertyChangeEvent propertyChangeEvent) {
        Enums.EnumStpStatus oldStatus = status;
        if (propertyChange_or(propertyChangeEvent)) {
            if (Konstanten.DEVELOPER_EXCEPTIONS && oldStatus != status) {
                throw new IllegalStateException("propertyChange_or() may not change 'status'!");
            }
            setStatus(Enums.EnumStpStatus.FAIL);
        }
    }

    protected boolean propertyChange_or(PropertyChangeEvent propertyChangeEvent) {
        return true; // Override
    }

    protected void addPropertyChangeListeners() {
        // Override
    }

    protected void removePropertyChangeListeners() {
        // Override
    }

    // STORY SUPPORT //////////////////////////////////////////////////////////////////////////////

    @Override
    public void react(StoryTeller teller, EnumStory story) {
        if (!status.equals(Enums.EnumStpStatus.OK)) {
            return;
        }
        if (story.equals(EnumStory.DELETE)) {
            delete();
        }
    }

    // GETTER UND SETTER //////////////////////////////////////////////////////////////////////////

    protected String gettStatusString() {
        return "EnumStepStatus == " + getStatus().name() + "versucht == " + isVersucht()
                + ", mindestensEinmalDurchgefuehrt == " + mindestensEinmalDurchgefuehrt ;
    }

    @Override
    public boolean isDeleted() {
        return deleted;
    }

    public Enums.EnumStpStatus getStatus() {
        return status;
    }

    public boolean isStatus(Enums.EnumStpStatus status) {
        return this.status.equals(status);
    }

    public void setStatus(Enums.EnumStpStatus status) {
        this.status = status;
    }

    public boolean isVersucht() {
        return versucht;
    }

    public boolean isMindestensEinmalDurchgefuehrt() {
        return mindestensEinmalDurchgefuehrt;
    }
}
