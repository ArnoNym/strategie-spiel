package de.arnonym.strategiespiel.framework.stp.stacks;

import de.arnonym.strategiespiel.framework.stp.Task;
import de.arnonym.strategiespiel.spielSchirm.Enums;

public class TaskStack extends Stack<Task> {
    public boolean handle(Task task, boolean onTop) {
        Enums.EnumStpStatus status = someHandleLogic(task);
        if (status == Enums.EnumStpStatus.OK) {
            if (onTop) {
                add(task);
            } else {
                add(0, task);
            }
            return true;
        }
        return false;
    }
}
