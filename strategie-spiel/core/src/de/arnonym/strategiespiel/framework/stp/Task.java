package de.arnonym.strategiespiel.framework.stp;

import java.util.List;

import de.arnonym.strategiespiel.framework.listener.StoryTeller;
import de.arnonym.strategiespiel.spielSchirm.balancing.Konstanten;
import de.arnonym.strategiespiel.framework.stp.stacks.StepStack;
import de.arnonym.strategiespiel.spielSchirm.Enums;
import de.arnonym.strategiespiel.framework.werkzeuge.utils.CollectionUtils;

public abstract class Task extends StpMachineObject {
    public final StpComp stpComp;

    private final StepStack stepStack = new StepStack();
    private Step currentStep;

    private String taskName = getClass().getSimpleName();

    public Task(StpComp stpComp, StoryTeller... storyTellers) {
        super(CollectionUtils.combine(storyTellers, stpComp));
        this.stpComp = stpComp;
    }

    // VERSUCHEN //////////////////////////////////////////////////////////////////////////////////

    @Override
    protected Enums.EnumStpStatus versuchen_or() {
        planeAlleSteps(stepStack);
        if (getStatus() == Enums.EnumStpStatus.FAIL) {
            return Enums.EnumStpStatus.FAIL;
        }
        planeNaechstenStep(stepStack);
        if (stepStack.isEmpty() || getStatus() == Enums.EnumStpStatus.FAIL) {
            return Enums.EnumStpStatus.FAIL;
        }
        return Enums.EnumStpStatus.OK;
    }

    // DURCHFUEHREN ///////////////////////////////////////////////////////////////////////////////

    @Override
    protected void durchfuehren_or(float delta) {
        currentStep = pickStep(currentStep);
        if (currentStep != null) {
            currentStep.durchfuehren(delta);
        }
    }

    private Step pickStep(Step currentStep) {
        if (currentStep == null) {
            return nextStep(null);
        }

        Enums.EnumStpStatus currentStepStatus = currentStep.getStatus();
        if (currentStepStatus == Enums.EnumStpStatus.OK) {
            currentStepStatus = currentStep.pruefenObBeenden();
        }
        switch (currentStepStatus) {
            case OK:
                return currentStep;
            case ERLEDIGT:
                if (Konstanten.DEVELOPER_VISUELL_STEP_SYSTEM_OUT) System.out.println("- Step ::: " + currentStep.getClass().getSimpleName() + " ::: " + currentStepStatus.name());
                currentStep.erledigt();
                return nextStep(currentStep);
            case FAIL:
            case FAIL_BEI_GEHE_ZU:
                if (Konstanten.DEVELOPER_VISUELL_STEP_SYSTEM_OUT) System.out.println("- Step ::: " + currentStep.getClass().getSimpleName() + " ::: " + currentStepStatus.name());
                setStatus(Enums.EnumStpStatus.FAIL);
                stepBeendet(currentStep);
                return null;
            default:
                throw new IllegalArgumentException();
        }
    }

    private Step nextStep(Step currentStep) {
        if (currentStep != null) {
            stepStack.remove(currentStep);
            stepBeendet(currentStep);
        }

        planeNaechstenStep(stepStack);

        if (stepStack.isEmpty()) {
            if (currentStep != null) {
                setStatus(currentStep.getStatus());
            } else {
                setStatus(Enums.EnumStpStatus.FAIL);
            }
            return null;
        }

        Step nextStep = stepStack.gettTop();

        if (Konstanten.DEVELOPER_VISUELL_STEP_SYSTEM_OUT) {
            System.out.println("- next Step ::: " + nextStep.getClass().getSimpleName());
        }
        Enums.EnumStpStatus nextStepStatus = nextStep.getStatus();
        switch (nextStepStatus) {
            case ERLEDIGT:
            case FAIL:
                /* Keine Exception, da es den Sonderfall gibt, dass fuer Steps durch
                "Property Change" deltet() gecallt wird. Daraus kann wiederum FAIL oder ERLEDIGT
                resultieren! */
                System.out.println("- next Step ::: " + nextStep.getClass().getSimpleName()
                        + " ist "+nextStepStatus.name()+" vor Anfangen!");
                break;
            case FAIL_BEI_GEHE_ZU:
                throw new IllegalStateException("TaskSize: " + stepStack.size()
                        + "\nStepName: " + nextStep.getClass().getSimpleName()
                        + ", StepStatus == " + nextStep.getStatus().name()
                        + ", Step ist Versucht == " + nextStep.isVersucht()
                        + ", Step ist Angefangen == " + nextStep.isAngefangen()
                        + ", Step ist MindestensEinmalDurchgefuehrt == "
                        + nextStep.isMindestensEinmalDurchgefuehrt());
        }

        nextStep.anfangen();
        stepAngefangen(nextStep);
        return pickStep(nextStep);
    }

    protected void stepAngefangen(Step step) {
        // Override bei Bedarf
    }

    protected void stepBeendet(Step step) {
        // Override bei Bedarf
    }

    // FAIL UND ERLEDIGT ///////////////////////////////////////////////////////////////////////////

    public void fail_or() {
        for (Step s : stepStack.iterate()) {
            if (s.getStatus() == Enums.EnumStpStatus.OK) {
                s.delete(); /* Setzt auch nur StepStatus und ist damit nicht noetig wenn dieser
                bereits nicht OK ist! */
            }
        }
        stepStack.beenden();
    }

    @Override
    public void erledigt_or() {

    }

    // STEPS PLANEN ///////////////////////////////////////////////////////////////////////////////

    public abstract void planeAlleSteps(StepStack stepStack);

    /** Wenn immer neu geplant oder etwas eingeschoben werden soll. Waehrend es gecallt wird wurde
     * der currentStep bereits aus dem stepStack removed aber der pointer selbst noch nicht neu
     * gesetzt. */
    public abstract void planeNaechstenStep(StepStack stepStack);

    // IMMITIERE LISTE ////////////////////////////////////////////////////////////////////////////

    public List<Step> gettSteps() {
        return stepStack.iterate();
    }

    // EINFACHE GETTER UND SETTER /////////////////////////////////////////////////////////////////

    public Step getCurrentStep() {
        return currentStep;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }
}
