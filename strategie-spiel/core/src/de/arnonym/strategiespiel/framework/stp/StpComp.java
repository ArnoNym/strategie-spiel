package de.arnonym.strategiespiel.framework.stp;

import java.util.Map;
import java.util.TreeMap;

import de.arnonym.strategiespiel.framework.ecs.component.Comp;
import de.arnonym.strategiespiel.framework.ecs.entity.Entity;
import de.arnonym.strategiespiel.framework.listener.EnumStory;
import de.arnonym.strategiespiel.framework.listener.StoryTeller;

public class StpComp extends Comp {
    private final StpMachine stpMachine = new StpMachine();
    private final TreeMap<Integer, Ai> aiCompTreeMap = new TreeMap<>();

    private Ai currentAi = null;

    public StpComp(StpSystem stpSystem, Entity entity) {
        super(stpSystem, entity);
    }

    public void update(float delta) {
        if (!isActiv()) throw new IllegalStateException();

        entscheiden();
        stpMachine.update(delta);
    }

    private void entscheiden() {
        if (!stpMachine.isEmpty()) {
            return;
        }

        for (Ai ai : aiCompTreeMap.values()) {
            Task task = ai.versuchen();
            if (task != null) {
                if (stpMachine.handle(task, true)) {
                    setCurrentAi(ai);
                    return;
                }
            }
        }

        setCurrentAi(null);
    }

    public void abbrechen() {
        stpMachine.deleteTasks();
    }

    public boolean connect(Ai ai) {
        if (ai == null) throw new IllegalArgumentException();
        Ai replacedAi = aiCompTreeMap.put(ai.prioritaetId, ai);
        if (replacedAi != null) {
            throw new IllegalArgumentException("Id bereits belegt."
                    + "\nMoeglicheGruende:"
                    + "\n 1. Diese Ai wurde bereits geaddet!"
                    + "\n 2. Eine Ai der selben Klasse wurde bereits geaddet!"
                    + "\n 3. Die id wird von zwei unterschiedlichen Ai Klassen verwendet!"
                    + "\nid: " + ai.prioritaetId + ", newClass: " + ai.getClass().getSimpleName()
                    + ", replacedClass: " + replacedAi.getClass().getSimpleName());
        }
        ai.addListener(this);
        return true;
    }

    public boolean disconnect(Ai ai) {
        if (ai == null) throw new IllegalArgumentException();
        Integer i = null;
        for (Map.Entry<Integer, Ai> entry : aiCompTreeMap.entrySet()) {
            if (entry.getValue().equals(ai)) {
                i = entry.getKey();
                break;
            }
        }
        if (i == null) {
            return false;
        }
        aiCompTreeMap.remove(i);
        ai.removeListener(this);
        return true;
    }

    @Override
    public void react(StoryTeller teller, EnumStory story) {
        if (teller instanceof Ai) {
            if (story.equals(EnumStory.DELETE)) {
                disconnect((Ai) teller);
            }
            return;
        }
        super.react(teller, story);
    }

    // GETTER UND SETTER //////////////////////////////////////////////////////////////////////////

    public String gettAktuellerTaskName() {
        return stpMachine.gettAktuellerTaskName();
    }

    public String gettAktuellerStepName() {
        return stpMachine.gettAktuellerStepName();
    }

    public void setCurrentAi(Ai ai) {
        if (currentAi != null) {
            if (currentAi.equals(ai)) {
                throw new IllegalArgumentException(ai.toString());
            }
            //todo currentAi.setStatus(false);
        }
        currentAi = ai;
        if (ai != null) {
            //todo ai.setStatus(true);
        }
    }

    public Ai getCurrentAi() {
        return currentAi;
    }
}
