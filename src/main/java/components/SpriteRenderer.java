package components;

import engine.Component;
import org.joml.Vector4f;

public class SpriteRenderer extends Component {
    private Vector4f colour;

    public SpriteRenderer(Vector4f colour) {
        this.colour = colour;
    }

    @Override
    public void update(float delta) {

    }

    public Vector4f getColour() {
        return colour;
    }

    public void setColour(Vector4f colour) {
        this.colour = colour;
    }
}
