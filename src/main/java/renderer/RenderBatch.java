package renderer;

import components.SpriteRenderer;

import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL30.*;

public class RenderBatch {
    // Vertex
    // ===========================================
    // Pos              Colour
    // float, float     float, float, float, float
    private static final int POS_SIZE = 2;
    private static final int COLOR_SIZE = 4;

    private static final int VERTEX_SIZE = POS_SIZE + COLOR_SIZE;
    private static final int VERTEX_SIZE_IN_BYTES = VERTEX_SIZE * Float.BYTES;

    private static final int POS_OFFSET = 0;
    private static final int COLOUR_OFFSET = POS_OFFSET + POS_SIZE * Float.BYTES;

    private SpriteRenderer[] sprites;
    private int numSprites; // Sprite currently in our array
    private boolean hasRoom; // Do we have room for more sprites
    private float[] vertices;

    private int vertexArrayObjectID;
    private int vertexBufferObjectID;
    private int maxBatchSize;
    private Shader shader;

    public RenderBatch(int maxBatchSize) {
        shader = new Shader("assets/shaders/default.glsl");
        shader.compileAndLink();
        this.sprites = new SpriteRenderer[maxBatchSize];
        this.maxBatchSize = maxBatchSize;

        vertices = new float[maxBatchSize * VERTEX_SIZE * 4]; // 4 vertices per square

        this.numSprites = 0;
        this.hasRoom = true;
    }

    public void start() {
        // Generate and bind a Vertex Array Object
        vertexArrayObjectID = glGenVertexArrays();
        glBindVertexArray(vertexArrayObjectID);

        // Allocate space for vertices
        vertexBufferObjectID = glGenBuffers();
        glBindBuffer(GL_ARRAY_BUFFER, vertexBufferObjectID);
        glBufferData(
                GL_ARRAY_BUFFER,
                (long) vertices.length * Float.BYTES,
                GL_DYNAMIC_DRAW); // The content of the vertices may be changed in the future

        // Create and upload indices buffer
        int elementBufferObjectId = glGenBuffers();
        int[] indices = generateIndices();
        glBindBuffer(GL_ARRAY_BUFFER, elementBufferObjectId);
        glBufferData(
                GL_ARRAY_BUFFER,
                indices,
                GL_STATIC_DRAW); // Will never get changed

        // Enable buffer attribute pointers
        glVertexAttribPointer(0, POS_SIZE, GL_FLOAT, false, VERTEX_SIZE_IN_BYTES, POS_OFFSET);
        glEnableVertexAttribArray(0);

        glVertexAttribPointer(1, COLOR_SIZE, GL_FLOAT, false, VERTEX_SIZE_IN_BYTES, COLOUR_OFFSET);
        glEnableVertexAttribArray(1);
    }

    private int[] generateIndices() {
        // 6 indices per square because there are 3 per triangle
        int[] elements = new int[6 * maxBatchSize];
        for (int  i = 0; i < maxBatchSize; i++) {
            loadElementIndices(elements, i);
        }
    }

    private void loadElementIndices(int[] elements, int index) {
        int offsetArrayIndex = 6 * index;
        int offset = 4 * index;

        // 3, 2, 0, 0, 2, 1     7, 6, 4, 4, 6, 5
    }
}
