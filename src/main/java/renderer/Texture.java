package renderer;

import org.lwjgl.BufferUtils;

import java.nio.ByteBuffer;
import java.nio.IntBuffer;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.stb.STBImage.stbi_image_free;
import static org.lwjgl.stb.STBImage.stbi_load;

public class Texture {
    private String filepath;
    private int textureId;

    public Texture(String filepath) {
        this.filepath = filepath;

        // Generate texture on GPU
        textureId = glGenTextures();
        glBindTexture(GL_TEXTURE, textureId);

        // Set texture parameters
        // // Repeat image in both directions
        glTexParameteri(
                GL_TEXTURE_2D,
                GL_TEXTURE_WRAP_S, // Left and right direction
                GL_REPEAT);
        glTexParameteri(
                GL_TEXTURE_2D,
                GL_TEXTURE_WRAP_T, // up and down direction
                GL_REPEAT);
        // // When stretching the image, pixelate (make the pixels bigger, dont blend between them)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        // // When shrinking an image, pixelate
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

        IntBuffer width = BufferUtils.createIntBuffer(1);
        IntBuffer height = BufferUtils.createIntBuffer(1);
        IntBuffer channels = BufferUtils.createIntBuffer(1);
        ByteBuffer image = stbi_load(filepath, width, height,
                channels, // Is the colour rgb or rgba
                0); // Tells you many channels are loaded???

        if (image == null) {
            assert false : "ERROR: Could not load image '" + filepath + "' !";
        } else { // Image loaded successfully
            int internalFormat;
            int format;
            if (channels.get(0) == 3) {
                internalFormat = GL_RGB;
                format = GL_RGB;
            } else if (channels.get(0) == 4) {
                internalFormat = GL_RGBA;
                format = GL_RGBA;
            } else {
                assert false : "ERROR: Unknown number of channels '"+channels.get(0)+"' in image '" + filepath + "' !";
                return;
            }
            glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, width.get(0), height.get(0), 0, format,
                    GL_UNSIGNED_BYTE, image); // Upload image to the gpu
        }

        stbi_image_free(image); // After it is uploaded to the gpu we can free the memory
    }

    public void bind() {
        glBindTexture(GL_TEXTURE_2D, textureId);
    }

    public void unbind() {
        glBindTexture(GL_TEXTURE_2D, 0);
    }
}
