package renderer;

import org.joml.*;
import org.lwjgl.BufferUtils;

import java.io.IOException;
import java.nio.FloatBuffer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.function.Consumer;
import java.util.function.Function;

import static org.lwjgl.opengl.GL11.GL_FALSE;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL20.glGetShaderInfoLog;

/**
 * In default.glsl werden vertex und fragement shader in der opengl eigenen Sprache definiert.
 * Diese Klasse versteckt die Shader zur Java Seite hin.
 * Das uebergeben von Werten an die Shader findet in dieser Klasse durch die upload Methoden statt.
 * In glsl die Inputs sind fuer jeden Wert neu, die uniforms bleiben erhalten.
 * Der Output des Vertex Shaders ist der Input des Fragment Shaders.
 *
 * Vertex Shader: Zeichnet Punkte (z.B. Ecken eines Dreiecks) an den uebergebenen Positionen mit den
 * uebergebenen Farben.
 *
 * Fragment Shader: Zeichnet alle Punkte zwischen zwei Vertices (z.B. zwischen den Ekcen eines Dreiecks).
 *
 * Beide werden zusammen gesteckt, kompiliert und der unter liegenden Ebene uebergeben.
 *
 * Untere Ebene: Ist in c programmiert und kommuniziert mit den Grafikkarten. Es wird ueber die OpenGl Methoden
 * kommuniziert. Objekte werden als Pointer zu Speicherorten uebergeben (in Java in int Variablen gespeichert).
 * Diese Speicherorte erhaelt man ebenfalls durch die OpenGL Funktionen.
 */
public class Shader {
    private int shaderProgramId;
    private boolean beingUsed = true;

    private String vertexSource;
    private String fragmentSource;
    private String filepath;

    public Shader(String filepath) {
        this.filepath = filepath;
        try {
            String identifier = "#type";

            String source = new String(Files.readAllBytes(Paths.get(filepath)));
            String[] splitString = source.split("("+identifier+")( )+([a-zA-Z]+)");

            // Find type patterns
            int index = source.indexOf(identifier) + identifier.length() + 1;
            int endOfLine = source.indexOf("\r\n", index); // IMPORTANT: On Linux just look for \n
            String firstPattern = source.substring(index, endOfLine).trim(); // Removes empty spaces etc

            index = source.indexOf(identifier, endOfLine) + identifier.length() + 1;
            endOfLine = source.indexOf("\r\n", index); // IMPORTANT: On Linux just look for \n
            String secondPattern = source.substring(index, endOfLine).trim(); // Removes empty spaces etc

            if (firstPattern.equals("vertex")) {
                vertexSource = splitString[1];
            } else if (firstPattern.equals("fragment")) {
                fragmentSource = splitString[1];
            } else {
                throw new IOException("Unexpected token '"+firstPattern+"'!");
            }

            if (secondPattern.equals("vertex")) {
                vertexSource = splitString[2];
            } else if (secondPattern.equals("fragment")) {
                fragmentSource = splitString[2];
            } else {
                throw new IOException("Unexpected token '"+secondPattern+"'!");
            }
        } catch (IOException e) {
            e.printStackTrace();
            assert false : "ERROR: Could not open file for shade with filepath '"+filepath+"'!";
        }
    }

    public void compileAndLink() {
        int vertexId = compile(GL_VERTEX_SHADER, vertexSource);
        int fragmentId = compile(GL_FRAGMENT_SHADER, fragmentSource);

        // Links shaders and check for errors
        shaderProgramId = glCreateProgram();
        glAttachShader(shaderProgramId, vertexId);
        glAttachShader(shaderProgramId, fragmentId);
        glLinkProgram(shaderProgramId);

        // Check for linking errors
        int success = glGetProgrami(shaderProgramId, GL_LINK_STATUS);
        if (success == GL_FALSE) {
            int length = glGetProgrami(shaderProgramId, GL_INFO_LOG_LENGTH);
            System.out.println("ERROR: Linking of shaders failed!");
            System.out.println(glGetProgramInfoLog(shaderProgramId, length));
            assert false;
        }
    }

    private int compile(int glShaderType, String source) {
        int id = glCreateShader(glShaderType);

        // Pass the shader source to the GPU
        glShaderSource(id, source);
        glCompileShader(id);

        // Check for errors in compilation
        int success = glGetShaderi(id, GL_COMPILE_STATUS);
        if (success == GL_FALSE) {
            int length = glGetShaderi(id, GL_INFO_LOG_LENGTH);
            System.out.println("ERROR: '"+filepath+"'\nShader compilation failed!");
            System.out.println(glGetShaderInfoLog(id, length));
            assert false;
        }

        return id;
    }

    public void use() {
        if (beingUsed) {
            return;
        }
        glUseProgram(shaderProgramId); // Bind shader program
        beingUsed = true;

    }

    public void detach() {
        glUseProgram(0); // Bind nothing
        beingUsed = false;
    }

    public void uploadMat4f(String glslVariableName, Matrix4f matrix) {
        executeUpload(glslVariableName, variableLocation -> {
            int dimensions = 4;
            FloatBuffer buffer = BufferUtils.createFloatBuffer(dimensions * dimensions); // Since it is a 4x4 Matrix
            matrix.get(buffer); // Put matrix content into the buffer (array)

            glUniformMatrix4fv(variableLocation, false, buffer);
        });
    }

    public void uploadMat3f(String glslVariableName, Matrix3f matrix) {
        executeUpload(glslVariableName, variableLocation -> {
            int dimensions = 3;
            FloatBuffer buffer = BufferUtils.createFloatBuffer(dimensions * dimensions);
            matrix.get(buffer);

            glUniformMatrix3fv(variableLocation, false, buffer);
        });
    }

    public void uploadMat2f(String glslVariableName, Matrix2f matrix) {
        executeUpload(glslVariableName, variableLocation -> {
            int dimensions = 2;
            FloatBuffer buffer = BufferUtils.createFloatBuffer(dimensions * dimensions);
            matrix.get(buffer);

            glUniformMatrix2fv(variableLocation, false, buffer);
        });
    }

    public void uploadVec4f(String glslVariableName, Vector4f vec) {
        executeUpload(glslVariableName, variableLocation -> glUniform4f(variableLocation, vec.x, vec.y, vec.z, vec.w));
    }

    public void uploadVec3f(String glslVariableName, Vector3f vec) {
        executeUpload(glslVariableName, variableLocation -> glUniform3f(variableLocation, vec.x, vec.y, vec.z));
    }

    public void uploadVec2f(String glslVariableName, Vector2f vec) {
        executeUpload(glslVariableName, variableLocation -> glUniform2f(variableLocation, vec.x, vec.y));
    }

    public void uploadFloat(String glslVariableName, float value) {
        executeUpload(glslVariableName, variableLocation -> glUniform1f(variableLocation, value));
    }

    public void uploadInt(String glslVariableName, int value) {
        executeUpload(glslVariableName, variableLocation -> glUniform1i(variableLocation, value));
    }

    /**
     *
     * @param glslVariableName name of the sampler2D variable in the glsl file
     * @param slot
     */
    public void uploadTexture(String glslVariableName, int slot) { // Just uploading the imageId
        executeUpload(glslVariableName, variableLocation -> glUniform1i(variableLocation, slot));
    }

    private void executeUpload(String glslVariableName, Consumer<Integer> upload) {
        int variableLocation = glGetUniformLocation(shaderProgramId, glslVariableName);
        use();
        upload.accept(variableLocation);
    }
}
