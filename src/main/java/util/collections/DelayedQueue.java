package util.collections;

import java.util.*;
import java.util.stream.Collectors;

public class DelayedQueue<O> implements Queue<O> {
    private ElementQueue removeElementQueue = new ElementQueue();
    private ElementQueue addElementQueue = new ElementQueue();

    public DelayedQueue() {
    }

    public void update(float delta) {
        if (isEmpty()) return;
        removeElementQueue.cumulatedDelta += delta;
        addElementQueue.cumulatedDelta += delta;
    }

    public Element next() {
        if (removeElementQueue.queue.isEmpty()) {
            return null;
        }

        Element element = removeElementQueue.queue.remove();

        addElementQueue.queue.add(new Element(element.object, addElementQueue.cumulatedDelta));

        if (removeElementQueue.queue.isEmpty()) {
            switchQueues();
        }

        return new Element(element.object, removeElementQueue.cumulatedDelta - element.cumulatedDelta);
    }

    public Pack getPack(int size) {
        if (size < size()) throw new IllegalArgumentException();
        return new Pack(size);
    }

    private void switchQueues() {
        removeElementQueue.cumulatedDelta = addElementQueue.cumulatedDelta;
        removeElementQueue.cumulatedDelta = 0;

        ElementQueue tempQueue = removeElementQueue;
        removeElementQueue = addElementQueue;
        addElementQueue = tempQueue;
    }

    @Override
    public int size() {
        return removeElementQueue.queue.size() + addElementQueue.queue.size();
    }

    @Override
    public boolean isEmpty() {
        return removeElementQueue.queue.isEmpty() && addElementQueue.queue.isEmpty();
    }

    @Override
    public boolean contains(Object object) {
        if (object == null) return false;
        O correctObject;
        try {
            correctObject = (O) object;
        } catch (ClassCastException e) {
            return false;
        }
        return removeElementQueue.contains(correctObject) || addElementQueue.contains(correctObject);
    }

    @Override
    public boolean add(O object) {
        addElementQueue.queue.add(new Element(object, addElementQueue.cumulatedDelta));
        return true;
    }

    @Override
    public boolean remove(Object object) {
        if (object == null) return false;
        O correctObject;
        try {
            correctObject = (O) object;
        } catch (ClassCastException e) {
            return false;
        }
        return addElementQueue.remove(correctObject) || removeElementQueue.remove(correctObject);
    }

    @Override
    public Iterator<O> iterator() {
        return getObjectList().listIterator();
    }

    private List<O> getObjectList() {
        List<O> objectList = new ArrayList<>(size());
        objectList.addAll(removeElementQueue.getObjectList());
        objectList.addAll(addElementQueue.getObjectList());
        return objectList;
    }

    @Override
    public boolean containsAll(Collection<?> collection) {
        return getObjectList().containsAll(collection);
    }

    @Override
    public boolean addAll(Collection<? extends O> collection) {
        boolean ret = false;
        for (O object : collection) {
            ret |= add(object);
        }
        return ret;
    }

    @Override
    public boolean retainAll(Collection<?> collection) {
        throw new IllegalArgumentException(); //TODO
    }

    @Override
    public boolean removeAll(Collection<?> collection) {
        if (collection == null) return false;
        if (collection.isEmpty()) return true;
        boolean ret = false;
        for (Object object : collection) {
            ret = remove(object) || ret;
        }
        return ret;
    }

    @Override
    public void clear() {
        removeElementQueue.queue.clear();
        removeElementQueue.cumulatedDelta = 0;
        addElementQueue.queue.clear();
        removeElementQueue.cumulatedDelta = 0;
    }

    @Override
    public O[] toArray() {
        throw new IllegalArgumentException(); //TODO
    }

    @Override
    public <T> T[] toArray(T[] ts) {
        throw new IllegalArgumentException(); //TODO
    }

    @Override
    public boolean offer(O o) {
        throw new IllegalArgumentException(); //TODO
    }

    @Override
    public O remove() {
        throw new IllegalArgumentException(); //TODO
    }

    @Override
    public O poll() {
        throw new IllegalArgumentException(); //TODO
    }

    @Override
    public O element() {
        throw new IllegalArgumentException(); //TODO
    }

    @Override
    public O peek() {
        throw new IllegalArgumentException(); //TODO
    }

    public class Element {
        public final O object;
        public final double cumulatedDelta;

        private Element(O object, double cumulatedDelta) {
            this.object = object;
            this.cumulatedDelta = cumulatedDelta;
        }

        public boolean isWrapping(Object obj) {
            return object.equals(obj);
        }
    }

    private class ElementQueue {
        private final Queue<Element> queue = new ArrayDeque<>();
        private double cumulatedDelta = 0;

        public boolean remove(O object) {
            Element removeOw = null;
            for (Element ow : queue) {
                if (ow.isWrapping(object)) {
                    removeOw = ow;
                    break;
                }
            }
            if (removeOw == null) {
                return false;
            }
            return queue.remove(removeOw);
        }

        public boolean contains(O object) {
            for (Element ow : queue) {
                if (ow.isWrapping(object)) {
                    return true;
                }
            }
            return false;
        }

        public List<O> getObjectList() {
            return queue.stream().map(element -> element.object).collect(Collectors.toList());
        }
    }

    public class Pack implements Iterable<Element> {
        private final int size;

        private Pack(int size) {
            this.size = size;
        }

        @Override
        public Iterator<Element> iterator() {
            return new PackIterator(size);
        }
    }

    private class PackIterator implements Iterator<Element> {
        private final int size;
        private int i = 0;

        private PackIterator(int size) {
            this.size = size;
        }

        @Override
        public boolean hasNext() {
            return !isEmpty() && i < size;
        }

        @Override
        public Element next() {
            i++;
            return DelayedQueue.this.next();
        }
    }
}
