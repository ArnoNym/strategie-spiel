package util.collections;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class RunList<O> implements Iterable<O> {
    private int current = 0;
    private final List<O> list;

    public RunList() {
        this.list = new ArrayList<>();
    }

    public RunList(List<O> list) {
        this.list = list;
    }

    /**
     * @return Sublist that includes all objects until but not including the current object
     */
    public List<O> sublistUntilCurrent() {
        return list.subList(0, current);
    }

    public void setTo(O object) {
        int index = 0;
        for (O o : list) {
            if (o.equals(object)) {
                setTo(index);
                return;
            }
            index++;
        }
        throw new IllegalArgumentException();
    }

    public void setTo(int index) {
        assert index >= 0 && index < list.size();
        current = index;
    }

    public void toStart() {
        while (hasPrevious()) {
            previous();
        }
    }

    public void toEnd() {
        while (hasNext()) {
            next();
        }
    }

    public O current() {
        return list.get(current);
    }

    public boolean hasNext() {
        return current < list.size() - 1;
    }

    public O next() {
        assert hasNext();
        current++;
        return current();
    }

    public boolean hasPrevious() {
        return current > 0 && !list.isEmpty();
    }

    public O previous() {
        assert hasPrevious();
        current--;
        return current();
    }

    @Override
    public Iterator<O> iterator() {
        return list.iterator();
    }

    public void addAtStart(O o) {
        list.add(0, o);
        current++;
    }

    public void addAtEnd(O o) {
        list.add(list.size(), o);
    }

    public boolean isEmpty() {
        return list.isEmpty();
    }

    public O removeCurrent() {
        if (isEmpty()) {
            throw new IllegalStateException();
        }
        O currentObject = list.remove(current);
        if (current > 0) {
            current--;
        }
        return currentObject;
    }
}
