package util;

public class Time {
    public static final long timeStarted = System.nanoTime();

    public static long getNanoTime() {
        return System.nanoTime();
    }

    public static float getTime() {
        return nanoToSecs(System.nanoTime());
    }

    public static float getGameTime() {
        return nanoToSecs(System.nanoTime() - timeStarted);
    }

    public static float nanoToSecs(long nano) {
        return (float) (nano * 1E-9);
    }

    public static long secsToNano(float secs) {
        return (long) ((long) secs * 1E9);
    }
}
