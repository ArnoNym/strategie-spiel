package engine;

import org.joml.Matrix4f;
import org.joml.Vector2f;
import org.joml.Vector3f;

/**
 * Matrices can be used (in default.glsl) to transform from world coordinates to screen coordinates
 */
public class Camera {
    private Matrix4f projectionMatrix = new Matrix4f(); // How Many Units should our world space have
    private Matrix4f viewMatrix = new Matrix4f(); // How is the camera looking

    private Vector2f position;

    public Camera(Vector2f position) {
        this.position = position;
        adjustProjection();
    }

    public void adjustProjection() {
        projectionMatrix.identity();
        projectionMatrix.ortho(0, 32 * 40, 0, 32 * 21, 0, 100);
    }

    public Matrix4f getViewMatrix() {
        Vector3f cameraFront = new Vector3f(0, 0, -1); // Where is the front
        Vector3f cameraUp = new Vector3f(0, 1, 0); // Where is up
        viewMatrix.identity();
        viewMatrix.lookAt(
                new Vector3f(position.x, position.y, 20), // Where is it located
                cameraFront.add(position.x, position.y, 0), // Where is the camera looking towards
                cameraUp);
        return viewMatrix;
    }

    public Matrix4f getProjectionMatrix() {
        return projectionMatrix;
    }

    public Vector2f getPosition() {
        return position;
    }
}
