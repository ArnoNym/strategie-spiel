package engine;

import org.lwjgl.Version;
import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.opengl.GL;
import util.Time;

import java.util.function.Supplier;

import static org.lwjgl.glfw.Callbacks.glfwFreeCallbacks;
import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.system.MemoryUtil.NULL;

public class Window {
    private static Window window = null;

    public static Window get() {
        if (window == null) {
            window = new Window();
        }
        return window;
    }

    private String title;
    private int width;
    private int height;
    private float red, green, blue, alpha;

    private long glfwWindow; // Number where window is in the memory space

    private Scene scene = null;

    private Window() {
        this.width = 1920;
        this.height = 1080;
        this.title = "Strategiespiel";
        this.red = 1;
        this.green = 1;
        this.blue = 1;
        this.alpha = 1;
    }

    public static void setScene(Supplier<Scene> sceneSupplier) {
        Scene scene = sceneSupplier.get();
        get().scene = scene;
        scene.init();
        scene.start();
    }

    public static Scene getScene() {
        return get().scene;
    }

    public void run() {
        System.out.println("Using LWJGL "+ Version.getVersion() +"!");

        init();

        setScene(LevelEditorScene::new);

        loop();

        // Not necessary. The operating system should do this
        // // Free the memory
        glfwFreeCallbacks(glfwWindow);
        glfwDestroyWindow(glfwWindow);
        // // Terminate GLFW and then free the error callback
        glfwTerminate();
        glfwSetErrorCallback(null).free();
    }

    private void init() {
        // Setup an error callback
        GLFWErrorCallback.createPrint(System.err).set(); // Print errors in console

        // Initialize GLFW
        if (!glfwInit()) {
            throw new IllegalStateException("Unable to initialize GLFW!");
        }

        // Configure GLFW
        glfwDefaultWindowHints();
        glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);
        glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);
        glfwWindowHint(GLFW_MAXIMIZED, GLFW_TRUE); // Window is set to max on creation

        // Create the window
        glfwWindow = glfwCreateWindow(width, height, title, NULL, NULL);
        if (glfwWindow == NULL) {
            throw new IllegalStateException("Failed to create the GLFW window!");
        }

        // Connect GLFW to my listeners
        glfwSetCursorPosCallback(glfwWindow, MouseListener::mousePosCallback);
        glfwSetMouseButtonCallback(glfwWindow, MouseListener::mouseButtonCallback);
        glfwSetScrollCallback(glfwWindow, MouseListener::mouseScrollCallback);
        glfwSetKeyCallback(glfwWindow, KeyListener::keyCallback);

        // Make the OpenGL context current
        glfwMakeContextCurrent(glfwWindow);

        // Enable v-sync
        glfwSwapInterval(1);

        // Make the window visible
        glfwShowWindow(glfwWindow);

        /*
        This line is critical for LWJGL's interoperation with GLFW's OpenGl context, or any context that is managed
        externally. LWJGL detects the context that is current in the current thread, creates the GLCapabilities
        instance and makes the OpenGL bindings available for use.
        */
        GL.createCapabilities();
    }

    private void loop() {
        float startTime = Time.getTime();
        float endTime;
        float delta;

        while (!glfwWindowShouldClose(glfwWindow)) {
            glfwPollEvents(); // Get key events and put them in the key listeners

            // Set background
            glClearColor(red, green, blue, alpha); // Set colour
            glClear(GL_COLOR_BUFFER_BIT); // Flush colour to entire screen

            endTime = Time.getTime();
            delta = endTime - startTime;
            startTime = endTime;

            scene.update(delta);

            glfwSwapBuffers(glfwWindow);
        }
    }
}
