package engine;

import static org.lwjgl.glfw.GLFW.GLFW_PRESS;
import static org.lwjgl.glfw.GLFW.GLFW_RELEASE;

/**
 * Implements the methods from glfw.org/docs/3.3/input_guide.html
 */
public class MouseListener {
    private static MouseListener instance;

    public static MouseListener get() {
        if (instance == null) {
            instance = new MouseListener();
        }
        return instance;
    }

    private double scrollX = 0;
    private double scrollY = 0;
    private double x = 0;
    private double y = 0;
    private double lastX = 0;
    private double lastY = 0;
    private final boolean[] mouseButtonsPressed = new boolean[3]; /* Up to 3 mouse buttons. This leads to gaming mice
    with more buttons being not supported */
    private boolean dragging = false;

    private MouseListener() {
    }

    // update methods used by the deeper layers ========================================================================

    public static void mousePosCallback(long window, double x, double y) {
        get().lastX = get().x;
        get().lastY = get().y;

        get().x = x;
        get().y = y;

        boolean dragging = false;
        for (boolean pressed : get().mouseButtonsPressed) {
            if (pressed) {
                dragging = true;
                break;
            }
        }
        get().dragging = dragging;
    }

    /**
     * @param mods Modifier keys like ctrl or alt
     */
    public static void mouseButtonCallback(long window, int button, int action, int mods) {
        if (!isSupported(button)) {
            return;
        }
        if (action == GLFW_PRESS) {
            get().mouseButtonsPressed[button] = true;
        } else if (action == GLFW_RELEASE) {
            get().mouseButtonsPressed[button] = false;
            get().dragging = false;
        }
    }

    private static boolean isSupported(int button) {
        return button >= get().mouseButtonsPressed.length;
    }

    public static void mouseScrollCallback(long window, double xOffset, double yOffset) {
        get().scrollX = xOffset;
        get().scrollY = yOffset;
    }

    public static void endFrame() {
        get().scrollX = 0;
        get().scrollY = 0;
        get().lastX = get().x;
        get().lastY = get().y;
    }

    // getters =========================================================================================================

    public static float getX() {
        return (float) get().x;
    }

    public static float getY() {
        return (float) get().y;
    }

    public static float getDx() {
        return getX() - (float) get().lastX;
    }

    public static float getDy() {
        return getY() - (float) get().lastY;
    }

    public static float getScrollX() {
        return (float) get().scrollX;
    }

    public static float getScrollY() {
        return (float) get().scrollY;
    }

    public static boolean isDragging() {
        return get().dragging;
    }

    public static boolean mouseButtonDown(int button) {
        if (!isSupported(button)) {
            return false;
        }
        return get().mouseButtonsPressed[button];
    }
}
