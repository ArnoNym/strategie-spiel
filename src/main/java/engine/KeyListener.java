package engine;

import static org.lwjgl.glfw.GLFW.GLFW_PRESS;
import static org.lwjgl.glfw.GLFW.GLFW_RELEASE;

public class KeyListener {
    private static KeyListener instance;

    public static KeyListener get() {
        if (instance == null) {
            instance = new KeyListener();
        }
        return instance;
    }

    private boolean keyPressed[] = new boolean[350]; // Amount of supported keybinding

    private KeyListener() {
    }

    // update methods used by the deeper layers ========================================================================

    public static void keyCallback(long window, int key, int scancode, int action, int mods) {
        if (action == GLFW_PRESS) {
            get().keyPressed[key] = true;
        } else if (action == GLFW_RELEASE) {
            get().keyPressed[key] = false;
        }
    }

    // getters =========================================================================================================

    public static boolean isKeyPressed(int keyCode) {
        return get().keyPressed[keyCode];
    }
}
