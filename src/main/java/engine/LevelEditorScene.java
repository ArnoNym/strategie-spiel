package engine;

import ecs.publish.Component;
import ecs.publish.Entity;
import ecs.publish.Manager;
import engine.ecs.SpriteComponent;
import engine.ecs.SpriteFactory;
import engine.ecs.SpriteSystem;
import org.joml.Vector2f;
import org.lwjgl.BufferUtils;
import renderer.Shader;
import renderer.Texture;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.HashSet;
import java.util.Set;

import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL30.glBindVertexArray;
import static org.lwjgl.opengl.GL30.glGenVertexArrays;

public class LevelEditorScene extends Scene {
    private int positionVertexShaderIndex = 0; // Specified in the vertexShaderSource
    private int colourVertexShaderIndex = 1; // Specified in the vertexShaderSource

    private int vertexId;
    private int fragmentId;
    private int shaderProgram; // Combination of vertex shader and fragment shader

    private float[] vertexArray = {
            // position x, position y, position z, colour red, colour green, colour blue, colour alpha, texture coordinate u, texture coordinate v
            100, 0, 0,         1, 0, 0, 1,    1, 1,
            0, 100, 0,         0, 1, 0, 1,    0, 1,
            100, 100, 0,       0, 0, 1, 1,    1, 1,
            0, 0, 0,           1, 1, 1, 1,    0, 0
    };
    private int[] elementArray = {
            // IMPORTANT: Must be in counter-clockwise order
            2, 1, 0,
            0, 1, 3
    };

    private int vertexArrayObjectId;
    private int vertexBufferObjectId;
    private int elementBufferObjectId;

    private Shader defaultShader;
    private Texture testTexture;

    private Manager manager;

    public LevelEditorScene() {
    }

    @Override
    public void init() {
        manager = new Manager();
        SpriteFactory spriteFactory = new SpriteFactory();
        manager.registerFactory(SpriteComponent.class, spriteFactory);
        Set<Component> components = new HashSet<>();
        components.add(spriteFactory.construct());
        manager.registerEntity(components);

        Set<Class<? extends Component>> componentClasses = new HashSet<>();
        componentClasses.add(SpriteComponent.class);
        manager.registerSystem(componentClasses, new SpriteSystem());

        setCamera(new Camera(new Vector2f()));

        defaultShader = new Shader("assets/shaders/default.glsl");
        defaultShader.compileAndLink();
        testTexture = new Texture("assets/sprites/baumA1.png");

        generateVaoVboEboAndSendToGpu();
    }


    private void generateVaoVboEboAndSendToGpu() {
        vertexArrayObjectId = glGenVertexArrays();
        glBindVertexArray(vertexArrayObjectId); // Everything after is applied to the object

        // Create a float buffer of vertices
        FloatBuffer vertexBuffer = BufferUtils.createFloatBuffer(vertexArray.length);
        vertexBuffer.put(vertexArray).flip();

        // Create VBO upload the vertex buffer
        vertexBufferObjectId = glGenBuffers();
        glBindBuffer(GL_ARRAY_BUFFER, vertexBufferObjectId); // Everything after is applied to the object
        glBufferData(GL_ARRAY_BUFFER, vertexBuffer, GL_STATIC_DRAW); // Sets the buffer. All changes are static

        // Create the indices and upload
        IntBuffer elementBuffer = BufferUtils.createIntBuffer(elementArray.length);
        elementBuffer.put(elementArray).flip();

        elementBufferObjectId = glGenBuffers();
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementBufferObjectId);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, elementBuffer, GL_STATIC_DRAW);

        // Add the vertex attribute pointers. Specifies the order of position and colour in the vertex array
        int positionsSize = 3;
        int colourSize = 4;
        int textureCoordinatesSize = 2;
        int vertexSizeBytes = (positionsSize + colourSize + textureCoordinatesSize) * Float.BYTES;

        glVertexAttribPointer(positionVertexShaderIndex, positionsSize, GL_FLOAT, false, vertexSizeBytes, 0);
        glEnableVertexAttribArray(positionVertexShaderIndex);

        int offsetToTheFirstAttribute = positionsSize * Float.BYTES;
        glVertexAttribPointer(colourVertexShaderIndex, colourSize, GL_FLOAT, false, vertexSizeBytes, offsetToTheFirstAttribute);
        glEnableVertexAttribArray(colourVertexShaderIndex);

        int textureCoordinatesIndex = 2;
        int offsetToTheSecondAttribute = (positionsSize + colourSize) * Float.BYTES;
        glVertexAttribPointer(textureCoordinatesIndex, textureCoordinatesSize, GL_FLOAT, false, vertexSizeBytes, offsetToTheSecondAttribute);
        glEnableVertexAttribArray(textureCoordinatesIndex);
    }

    @Override
    public void update(float delta) {
        getCamera().getPosition().x -= delta * 50;

        defaultShader.use();

        // Upload Texture to shader
        defaultShader.uploadTexture("TEX_SAMPLER", 0);
        glActiveTexture(GL_TEXTURE0); // Activate slot 0
        testTexture.bind(); // Put the texture into the slot

        defaultShader.uploadMat4f("uProjection", getCamera().getProjectionMatrix()); // Names in default.glsl
        defaultShader.uploadMat4f("uView", getCamera().getViewMatrix());

        // Bind the VAO that we are using
        glBindVertexArray(vertexArrayObjectId);

        // Enable the vertex attribute pointers
        glEnableVertexAttribArray(positionVertexShaderIndex);
        glEnableVertexAttribArray(colourVertexShaderIndex);
        glDrawElements(
                GL_TRIANGLES, // Draw as triangles
                elementArray.length, // Number of elements to draw
                GL_UNSIGNED_INT, // Will never be negative
                0); // Starting at

        // Unbind everything
        glDisableVertexAttribArray(positionVertexShaderIndex);
        glDisableVertexAttribArray(colourVertexShaderIndex);
        glBindVertexArray(0); // Bind nothing

        defaultShader.detach();

        manager.update(delta);
    }
}
