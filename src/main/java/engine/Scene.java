package engine;

import java.util.ArrayList;
import java.util.List;

public abstract class Scene {
    private Camera camera;
    private boolean isRunning;
    private List<GameObject> gameObjects = new ArrayList<>();

    public Scene() {
    }

    public abstract void init();

    public void start() {
        for (GameObject gameObject : gameObjects) {
            gameObject.start();
        }
    }

    public void addGameObjectToScene(GameObject gameObject) {
        gameObjects.add(gameObject);
        if (isRunning) {
            gameObject.start();
        }
    }

    public abstract void update(float delta);

    public void setCamera(Camera camera) {
        this.camera = camera;
    }

    public Camera getCamera() {
        return camera;
    }
}
